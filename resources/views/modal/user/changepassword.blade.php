<!-- Middle Modal -->
<style>
        .modal {
          text-align: center;
          padding: 0!important;
        }
        
        .modal:before {
          content: '';
          display: inline-block;
          height: 100%;
          vertical-align: middle;
          margin-right: -4px;
        }
        
        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }
</style>  

<!-- Modal -->
<div class="modal fade" id="modalchange" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>CHANGE PASSWORD</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                        <div class="form-group">
                            <label for="txtoldpassword">Old Password</label>
                            <input id="txtoldpassword" name="txtoldpassword" type="password" class="form-control" placeholder="Old Password">
                        </div>
                        <div class="form-group">
                            <label for="txtnewpassword">New Password</label>
                            <input id="txtnewpassword" name="txtnewpassword" type="password" class="form-control" placeholder="New Password">
                        </div>
                        <div class="form-group">
                            <label for="txtconfirmpassword">Confirm Password</label>
                            <input id="txtconfirmpassword" name="txtconfirmpassword" type="password" class="form-control" placeholder="Confirm Password">
                        </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-8">

                </div>
                <div class="col-md-4">
                    <button id="btnchange" name="btnchange" type="button" class="btn btn-primary btn-block btn-flat">Change Password</button>
                </div>
            </div>
        </div>
    </div>
</div>