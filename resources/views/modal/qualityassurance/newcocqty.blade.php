<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newcocqty" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>NEW COC Quantity</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txtnmstnumber">MST #</label>
                            <input id="txtnmstnumber" name="txtnmstnumber" class="form-control" type="text">
                        </div>
                        <div class="form-group">
                                <label for="txtnqty">Quantity</label>
                                <input id="txtnqty" name="txtnqty" class="form-control" type="number">
                        </div>
                        <div class="form-group">
                                <label for="txtnrejects">Rejects</label>
                                <input id="txtnrejects" name="txtnrejects" class="form-control" type="number">
                        </div>
                        
                    </div>                    

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnnsave" name="btnnsave" type="button" class="btn btn-primary btn-flat">Save Information</button>
                </div>
            </div>
        </div>
    </div>
</div>