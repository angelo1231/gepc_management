@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Customer Information</b></h3>
        </div>
        <div class="box-body">
          <div class="col-md-9">
          </div>
          <div class="col-md-3" style="margin-bottom: 15px;">
              <button id="btnnewcustomer" name="btnnewcustomer" class="btn btn-success btn-block btn-flat"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Customer</strong></button>
          </div> 
          <div class="col-md-12">
              <table id="tblcustomers" class="table">
                  <thead>
                      <tr>
                          <th>#</th>
                          <th>Customer</th>
                          <th>Contact Person</th>
                          <th>Contact Number</th>
                          <th>Email</th>
                          <th>Address</th>
                          <th>TIN</th>
                          <th>Terms</th>
                          <th>Tax Classification</th>
                          <th></th>
                      </tr>
                  </thead>
              </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modals --}}
  @include('modal.customer.newcustomer')
  @include('modal.customer.updatecustomer')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      var tblcustomers;
      $(document).ready(function() {

          tblcustomers = $('#tblcustomers').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: '{{ url("api/customer/customerinformation") }}',
            columns : [
                {data: 'cid', name: 'cid'},
                {data: 'supplier', name: 'customer'},
                {data: 'contactperson', name: 'contactperson'},
                {data: 'contactnumber', name: 'contactnumber'},
                {data: 'email', name: 'email'},
                {data: 'address', name: 'address'},
                {data: 'tin', name: 'tin'},
                {data: 'terms', name: 'terms'},
                {data: 'tax', name: 'tax'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
          });

          SetSelect2();

      });

      $('#btnnewcustomer').on('click', function(){

        @if(in_array(26, $access))

          ClearNewCustomer();
          $('#modalnewcustomer').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif
           
      });

      $('#btnsave').on('click', function(){

        var customer = $('#txtncustomer').val();
        var contactperson = $('#txtncontactperson').val();
        var contactnumber = $('#txtncontactnumber').val();
        var email = $('#txtnemail').val();
        var address = $('#txtnaddress').val();
        var tin = $('#txtntin').val();
        var terms = $('#txtnterms').val();
        var tax = $('#cmbntax').val();

        if(customer==""){
          toastr.error("Please input the customer name.", '', { positionClass: 'toast-top-center' });
          $('#txtncustomer').focus();
        }
        else if(contactperson==""){
          toastr.error('Please input the contact person of the customer.', '', { positionClass: 'toast-top-center' });
          $('#txtncontactperson').focus();
        }
        else if(contactnumber==""){
          toastr.error('Please input the contact number of the customer.', '', { positionClass: 'toast-top-center' });
          $('#txtncontactnumber').focus();
        }
        else if(email==""){
          toastr.error('Please input the email of the customer.', '', { positionClass: 'toast-top-center' });
          $('#txtnemail').focus();
        }
        else if(address==""){
          toastr.error("Please input the customer address.", '', { positionClass: 'toast-top-center' });
          $('#txtnaddress').focus();
        }
        else{

          $.ajax({
            url: '{{ url("api/customer/savecustomer") }}',
            type: 'post',
            data: {
              customer: customer,
              contactperson: contactperson,
              contactnumber: contactnumber,
              email: email,
              address: address, 
              tin: tin, 
              terms: terms,
              tax: tax
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                $("#modalnewcustomer .close").click();
                toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                ReloadCustomers();
                

              }
              else{

                $('#txtncustomer').val('');
                $('#txtncustomer').focus();
                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }

            }
          });

        }

      });

      $(document).on('click', '#btnedit', function(){

        @if(in_array(27, $access))

          var cid = $(this).attr("value");
          ClearUpdate();
          $.ajax({
            url: '{{ url("api/customer/customerprofile") }}',
            type: 'get',
            data: {cid: cid},
            dataType: 'json',
            success: function(response){

              $('#txtucustomer').val(response.customer);
              $('#txtucontactperson').val(response.contactperson);
              $('#txtucontactnumber').val(response.contactnumber);
              $('#txtuemail').val(response.email);
              $('#txtuaddress').val(response.address);
              $('#txtutin').val(response.tin);
              $('#txtuterms').val(response.terms);
              $('#cmbutax').val(response.tax).trigger('change');
              $('#btnupdate').val(cid);

            }
          });

          $('#modalupdatecustomer').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnupdate').on('click', function(){

        var cid = $(this).attr("value");
        var customer =  $('#txtucustomer').val();
        var contactperson = $('#txtucontactperson').val();
        var contactnumber = $('#txtucontactnumber').val();
        var email = $('#txtuemail').val();
        var address =  $('#txtuaddress').val();
        var tin =  $('#txtutin').val();
        var terms =  $('#txtuterms').val();
        var tax = $('#cmbutax').val();

        if(customer==""){
          toastr.error("Please input the customer name.", '', { positionClass: 'toast-top-center' });
          $('#txtucustomer').focus();
        }
        else if(contactperson==""){
          toastr.error('Please input the contact person of the customer.', '', { positionClass: 'toast-top-center' });
          $('#txtucontactperson').focus();
        }
        else if(contactnumber==""){
          toastr.error('Please input the contact number of the customer.', '', { positionClass: 'toast-top-center' });
          $('#txtucontactnumber').focus();
        }
        else if(email==""){
          toastr.error('Please input the email of the customer.', '', { positionClass: 'toast-top-center' });
          $('#txtuemail').focus();
        }
        else if(address==""){
          toastr.error("Please input the customer address.", '', { positionClass: 'toast-top-center' });
          $('#txtuaddress').focus();
        }
        else{

          $.ajax({
            url: '{{ url("api/customer/updatecustomer") }}',
            type: 'post',
            data: {
              cid: cid, 
              customer: customer,
              contactperson: contactperson,
              contactnumber: contactnumber,
              email: email, 
              address: address, 
              tin: tin, 
              terms: terms,
              tax: tax
            },
            dataType: 'json',
            success: function(response){

              $("#modalupdatecustomer .close").click();
              toastr.success(response.message, '', { positionClass: 'toast-top-center' });
              ReloadCustomers();

            }
          });

        }

      });

      function ClearNewCustomer(){

        $('#txtncustomer').val('');
        $('#txtncontactperson').val('');
        $('#txtncontactnumber').val('');
        $('#txtnemail').val('');
        $('#txtnaddress').val('');
        $('#txtntin').val('');
        $('#txtnterms').val('');

      }

      function ClearUpdate(){

        $('#txtucustomer').val('');
        $('#txtucontactperson').val('');
        $('#txtucontactnumber').val('');
        $('#txtuemail').val('');
        $('#txtuaddress').val('');
        $('#txtutin').val('');
        $('#txtuterms').val('');

      }

      function SetSelect2(){

        $('#cmbntax').select2({
          theme: 'bootstrap'
        });

        $('#cmbutax').select2({
          theme: 'bootstrap'
        });

      }

      function ReloadCustomers(){
        tblcustomers.ajax.reload();
      }


  </script>  

@endsection