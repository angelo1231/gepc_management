@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Material Request</b></h3>
          <a href="{{ url("/planning") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="cmbcustomer">Customer</label>
                    <select id="cmbcustomer" name="cmbcustomer" class="form-control">
                    </select>
                </div>
            </div>
            <div class="col-md-9">
            <br>
                <button id="btnnewrequestexcess" name="btnnewrequestexcess" class="btn btn-info btn-flat" style="float:right; margin-top: 7px; margin-bottom: 7px; margin-left: 10px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Request Excess</strong></button>

                <button id="btnnewrequest" name="btnnewrequest" class="btn btn-success btn-flat" style="float:right; margin: 7px 0px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Request</strong></button>
            </div>
            <div class="col-md-12">
                <table id="tblmaterialrequest" class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>MRI #</th>
                            <th>Issuance #</th>
                            <th>Finish Good</th>
                            <th>Issued By</th>
                            <th>Reason</th>
                            <th>Customer</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->\
  {{-- Modal --}}
  @include('modal.materialrequest.newmri')
  @include('modal.materialrequest.materialrequestinfo')
  @include('modal.materialrequest.newmriexcess')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblmaterialrequest;
    var tblexcessso;
    var itemcount = 1;
    var itemarray = [];
    var excessitemcount = 1;
    var excessitemarray = [];

    $(document).ready(function(){

        LoadSelect2();
        LoadCustomers($('#cmbcustomer').attr('id'));

        tblmaterialrequest = $('#tblmaterialrequest').DataTable({
                autoWidth: false,
                ordering: false,
        });

    });

    $('#btnnewrequest').on('click', function(){

        @if(in_array(39, $access))

            ClearNewRequestMaterial();
            GenMRINumber('txtnmrinumber');
            LoadCustomers($('#cmbncustomer').attr('id'));
            LoadRawMaterials($('#cmbnrawitems1').attr('id'));
            LoadRawMaterialsExcess($('#txtnexcessboard1').attr('id'));
            $('#modalnewrequest').modal('toggle');

            //Set
            $('#txtnqtyexcessboard1').select2({
                tags: true,
                tokenSeparators: [',']
                // theme: 'bootstrap'
            });

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

    });

    $('#btnnewrequestexcess').on('click', function(){

        @if(in_array(39, $access))

            ClearNewRequestMaterialExcess();
            GenMRINumber('txtnmrinumberexcess');
            LoadCustomers($('#cmbncustomerexcess').attr('id'));
            LoadRawMaterialWithExcess('cmbnrawitemsexcess1');
            LoadRawMaterialsExcess($('#txtnexcessboardexcess1').attr('id'));

            $('#newmriexcess').modal('toggle');

            //Set
            $('#txtnqtyexcessboardexcess1').select2({
                tags: true,
                tokenSeparators: [',']
                // theme: 'bootstrap'
            });

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

    });

    $('#cmbcustomer').on('change', function(){

        var customer =  $("#cmbcustomer").val();

        tblmaterialrequest.destroy();
        tblmaterialrequest = $('#tblmaterialrequest').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                ordering: false,
                ajax: {
                    type: 'get',
                    url: '{{ url("api/materialrequest/loadmaterialrequestinformation") }}',
                    data: {
                        customer: customer
                    },
                },
                columns : [
                    {data: 'status', name: 'status'},
                    {data: 'mri', name: 'mri'},
                    {data: 'issuancenumber', name: 'issuancenumber'},
                    {data: 'finishgood', name: 'finishgood'},
                    {data: 'issuedby', name: 'issuedby'},
                    {data: 'reason', name: 'reason'},
                    {data: 'customer', name: 'customer'},
                    {data: 'panel', name: 'panel', width: '12%'},
                ]
        });

    });

    $('#cmbncustomer').on('change', function(){

        var cid = $(this).val();
        var id = $('#cmbnfgitems').attr('id');
        LoadCustomerItems(cid, id);
        LoadPurchaseRequestInformation(cid);

    });

    $('#cmbncustomerexcess').on('change', function(){

        var cid = $(this).val();
        var id = $('#cmbnfgitemsexcess').attr('id');
        LoadCustomerItems(cid, id);

    });

    $('#cmbpurchaserequest').on('change', function(){

        var prid = $(this).val();
        LoadPurchaseRequestData(prid);

    });

    $('#cmbnfgitemsexcess').on('change', function(){

        var fgid = $(this).val();
        LoadCustomerFGSalesOrder(fgid);

    });

    $(document).on('select2:select', '[name="txtnqtyexcessboard[]"]', function (e) {
        $(this).append('<option value="'+e.params.data.text+'">' +e.params.data.text + '</option>');
    });

    $('#btnrequest').on('click', function(){

        var mrinumber = $('#txtnmrinumber').val();
        var cid = $('#cmbncustomer').val();
        var fgid = $('#cmbnfgitems').val();
        var prid = $('#cmbpurchaserequest').val();
        var rmid = $('[name="cmbnrawitems[]"]').serializeArray();
        var reqqty = $('[name="txtnrequiredqty[]"]').serializeArray();
        var remarks = $('[name="txtnremarks[]"]').serializeArray();
        var reason = $('#txtnreason').val();

        var hasrmid = false;
        var hasreqqty = false;
        var hasremarks = false;

        var excessboard = new Array();
        var qtyexcessboard = new Array();

        //First Item
        var firstexcess = $('#txtnexcessboard1').serializeArray();
        var firstqtyexcess = $('#txtnqtyexcessboard1').serializeArray();

        if(firstexcess.length==0){

            excessboard.push([{
                value: ''
            }]);
            qtyexcessboard.push([{
                value: ''
            }]);

        }
        else{

            excessboard.push(firstexcess);

            if(firstqtyexcess.length==0){
                qtyexcessboard.push([{
                    value: ''
                }]);
            }
            else{
                qtyexcessboard.push(firstqtyexcess);
            }
        
        }

        //Following Items
        for(var e=0;e<itemarray.length;e++){

            var excess = $('#txtnexcessboard'+itemarray[e]).serializeArray();
            var qtyexcess = $('#txtnqtyexcessboard'+itemarray[e]).serializeArray();

            if(excess.length==0){

                excessboard.push([{
                    value: ''
                }]);
                qtyexcessboard.push([{
                    value: ''
                }]);

            }
            else{

                excessboard.push(excess);

                if(qtyexcess.length==0){
                    qtyexcessboard.push([{
                        value: ''
                    }]);
                }
                else{
                    qtyexcessboard.push(qtyexcess);
                }

            }

        }

        //Validation
        for(var i=0;i<rmid.length;i++){

            if(rmid[i]["value"]==""){
                hasrmid = false;
                break;
            }
            else{
                hasrmid = true
            }

            if(reqqty[i]["value"]==0 || reqqty[i]["value"]<0){
                hasreqqty = false;
                break;
            }
            else{
                hasreqqty = true;
            }

            if(remarks[i]["value"]==""){
                hasremarks = false;
                break;
            }
            else{
                hasremarks = true;
            }

        }

        if(cid==null){
            toastr.error("Please select a customer.", '', { positionClass: 'toast-top-center' });
        }
        else if(fgid==null){
            toastr.error("Please select a finish good information.", '', { positionClass: 'toast-top-center' });
        }
        else if(hasrmid==false){
            toastr.error("Please select a raw material information.", '', { positionClass: 'toast-top-center' });
        }
        else if(hasreqqty==false){
            toastr.error("Please input a required qty.", '', { positionClass: 'toast-top-center' });
        }
        else if(hasremarks==false){
            toastr.error("Please input a remakrs in raw material.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/planning/saverequestmaterial") }}',
                type: 'post',
                data: {
                    mrinumber: mrinumber,
                    cid: cid,
                    fgid: fgid,
                    rmid: rmid,
                    reqqty: reqqty,
                    remarks: remarks,
                    excessboard: excessboard,
                    qtyexcessboard: qtyexcessboard,
                    reason: reason,
                    prid: prid
                },
                dataType: 'json',
                beforeSend: function(){
                    H5_loading.show();
                },
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadMaterialRequest();
                        $("#modalnewrequest .close").click();

                        //Socket Emit
                        socket.emit('planningmrinfo');
                        socket.emit('loadmrcount');
 
                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                },
                complete: function(){
                    H5_loading.hide();
                }
            });

        }

    });

    $('#btnrequestexcess').on('click', function(){

        var mrinumber = $('#txtnmrinumberexcess').val();
        var cid = $('#cmbncustomerexcess').val();
        var fgid = $('#cmbnfgitemsexcess').val();
        var rmid = $('[name="cmbnrawitemsexcess[]"]').serializeArray();
        var reqqty = $('[name="txtnrequiredqtyexcess[]"]').serializeArray();
        var remarks = $('[name="txtnremarksexcess[]"]').serializeArray();
        var soinfo = $('[name="chkexcessso[]"]').serializeArray();
        var reason = $('#txtnreasonexcess').val();

        var hasrmid = false;
        var hasreqqty = false;
        var hasremarks = false;

        var excessboard = new Array();
        var qtyexcessboard = new Array();

        //First Item
        var firstexcess = $('#txtnexcessboardexcess1').serializeArray();
        var firstqtyexcess = $('#txtnqtyexcessboardexcess1').serializeArray();

        if(firstexcess.length==0){

            excessboard.push([{
                value: ''
            }]);
            qtyexcessboard.push([{
                value: ''
            }]);

        }
        else{

            excessboard.push(firstexcess);

            if(firstqtyexcess.length==0){
                qtyexcessboard.push([{
                    value: ''
                }]);
            }
            else{
                qtyexcessboard.push(firstqtyexcess);
            }
        
        }

        //Following Items
        for(var e=0;e<excessitemarray.length;e++){

            var excess = $('#txtnexcessboardexcess1'+excessitemarray[e]).serializeArray();
            var qtyexcess = $('#txtnqtyexcessboardexcess1'+excessitemarray[e]).serializeArray();

            if(excess.length==0){

                excessboard.push([{
                    value: ''
                }]);
                qtyexcessboard.push([{
                    value: ''
                }]);

            }
            else{

                excessboard.push(excess);

                if(qtyexcess.length==0){
                    qtyexcessboard.push([{
                        value: ''
                    }]);
                }
                else{
                    qtyexcessboard.push(qtyexcess);
                }

            }

        }

        //Validation
        for(var i=0;i<rmid.length;i++){

            if(rmid[i]["value"]==""){
                hasrmid = false;
                break;
            }
            else{
                hasrmid = true
            }

            if(reqqty[i]["value"]==0 || reqqty[i]["value"]<0){
                hasreqqty = false;
                break;
            }
            else{
                hasreqqty = true;
            }

            if(remarks[i]["value"]==""){
                hasremarks = false;
                break;
            }
            else{
                hasremarks = true;
            }

        }

        if(cid==null){
            toastr.error("Please select a customer.", '', { positionClass: 'toast-top-center' });
        }
        else if(fgid==null){
            toastr.error("Please select a finish good information.", '', { positionClass: 'toast-top-center' });
        }
        else if(hasrmid==false){
            toastr.error("Please select a raw material information.", '', { positionClass: 'toast-top-center' });
        }
        else if(hasreqqty==false){
            toastr.error("Please input a required qty.", '', { positionClass: 'toast-top-center' });
        }
        else if(hasremarks==false){
            toastr.error("Please input a remakrs in raw material.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/planning/saverequestmaterialexcess") }}',
                type: 'post',
                data: {
                    mrinumber: mrinumber,
                    cid: cid,
                    fgid: fgid,
                    rmid: rmid,
                    reqqty: reqqty,
                    remarks: remarks,
                    excessboard: excessboard,
                    qtyexcessboard: qtyexcessboard,
                    reason: reason,
                    soinfo: soinfo
                },
                dataType: 'json',
                beforeSend: function(){
                    H5_loading.show();
                },
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadMaterialRequest();
                        $('#newmriexcess').modal('toggle');

                        //Socket Emit
                        socket.emit('planningmrinfo');
                        socket.emit('loadmrcount');
 
                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                },
                complete: function(){
                    H5_loading.hide();
                }
            });
        }

    });

    $(document).on('click', '#btninfo', function(){

        var id = $(this).val();
        GetMRI(id);

    });

    $('#btnaddmritem').on('click', function(){

        itemcount += 1;
        $('#nmritems').append('<tr id="'+ itemcount +'"><td><select id="cmbnrawitems'+ itemcount +'" name="cmbnrawitems[]" class="form-control"></select></td><td><input id="txtnrequiredqty'+ itemcount +'" name="txtnrequiredqty[]" type="number" class="form-control" placeholder="0"></td><td><textarea class="form-control" rows="2" id="txtnremarks'+ itemcount +'" name="txtnremarks[]" placeholder="Remarks"></textarea></td><td><select id="txtnexcessboard'+ itemcount +'" name="txtnexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td><td><select id="txtnqtyexcessboard'+ itemcount +'" name="txtnqtyexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td><td><button id="btnremovepritem" name="btnremovepritem" class="btn btn-flat btn-danger" value="'+ itemcount +'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadRawMaterials($('#cmbnrawitems'+itemcount).attr('id'));
        LoadRawMaterialsExcess($('#txtnexcessboard'+itemcount).attr('id'));

        //Set
        $('#txtnqtyexcessboard'+itemcount).select2({
            tags: true,
            tokenSeparators: [',']
            // theme: 'bootstrap'
        });

        itemarray.push(itemcount);

    });

    $('#btnaddmritemexcess').on('click', function(){

        excessitemcount += 1;
        $('#nmritemsexcess').append('<tr id="'+ excessitemcount +'"><td><select id="cmbnrawitemsexcess'+ excessitemcount +'" name="cmbnrawitemsexcess[]" class="form-control"></select></td><td><input id="txtnrequiredqtyexcess'+ excessitemcount +'" name="txtnrequiredqtyexcess[]" type="number" class="form-control" placeholder="0"></td><td><textarea class="form-control" rows="2" id="txtnremarksexcess'+ excessitemcount +'" name="txtnremarksexcess[]" placeholder="Remarks"></textarea></td><td><select id="txtnexcessboardexcess'+ excessitemcount +'" name="txtnexcessboardexcess[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td><td><select id="txtnqtyexcessboardexcess'+ excessitemcount +'" name="txtnqtyexcessboardexcess[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td><td><button id="btnremovepritemexcess" name="btnremovepritemexcess" class="btn btn-flat btn-danger" value="'+ excessitemcount +'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadRawMaterialWithExcess($('#cmbnrawitemsexcess'+excessitemcount).attr('id'));
        LoadRawMaterialsExcess($('#txtnexcessboardexcess'+excessitemcount).attr('id'));

        //Set
        $('#txtnqtyexcessboardexcess'+excessitemcount).select2({
            tags: true,
            tokenSeparators: [',']
            // theme: 'bootstrap'
        });

        excessitemarray.push(excessitemcount);

    });

    $(document).on('click', '#btnremovepritem', function(){

        var id = $(this).val();
        var position = $.inArray(parseInt(id), itemarray);

        if(~position){
            itemarray.splice(position, 1);
            $('#'+id).remove();
        }

    });

    $(document).on('click', '#btnremovepritemexcess', function(){

        var id = $(this).val();
        var position = $.inArray(parseInt(id), excessitemarray);

        if(~position){
            excessitemarray.splice(position, 1);
            $('#'+id).remove();
        }

    });

    function GenMRINumber(id){

        $.ajax({
            url: '{{ url("api/materialrequest/genmrinumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).val(response.batch);

            }
        });

    }

    function GetMRI(id){

        $.ajax({
            url: '{{ url("api/materialrequest/getmri") }}',
            type: 'get',
            data: {
                id: id,
            },
            dataType: 'json',
            success: function(response){

                $('#lblinfo').text('MRI #: ' + response.mrinumber + " |  ISSUANCE #: " + response.issuancenumber);
                $('#tblmrinfocontent').find('tr').remove();
                $('#tblmrinfocontent').append(response.content);

            }
        });

    }

    function LoadRawMaterials(id){

        $.ajax({
            url: '{{ url("api/planning/loadrawmaterials") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Size</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["rmid"] +'">'+  response.data[i]["description"] +'</option>');
                }

            },
            complete: function(){

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadRawMaterialWithExcess(id){

        $.ajax({
            url: '{{ url("api/planning/loadrawmaterialwithexcess") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append(response.content);

            },
            complete: function(){

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadRawMaterialsExcess(id){

        $.ajax({
            url: '{{ url("api/planning/loadrawmaterialsexcess") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="db-'+  response.data[i]["rmid"] +'">'+  response.data[i]["description"] +'</option>');
                }

            },
            complete: function(){

                $('#'+id).select2({
                    tags: true,
                    tokenSeparators: [',']
                    // theme: 'bootstrap'
                });

            }
        });

    }

    function LoadCustomerItems(cid, id){

        $.ajax({
            url: '{{ url("api/planning/loadcustomeritems") }}',
            type: 'get',
            data: {
                cid: cid
            },
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Item</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["fgid"] +'">'+  response.data[i]["item"] +'</option>');
                }

            }
        });

    }

    function ClearNewRequestMaterial(){

        $('#txtnmri').val('');
        $('#txtnrequiredqty1').val('');
        $('#txtnremarks1').val('');
        $('#txtnexcessboard1').val('');
        $('#txtnqtyexcessboard1').val('')
        $('#txtnreason').val('');

        for(var i = 0; i < itemarray.length; i++){

            document.getElementById(itemarray[i].toString()).remove();

        }

        itemarray = [];
   
    }

    function ClearNewRequestMaterialExcess(){

        $('#txtnmrinumberexcess').val('');
        $('#txtnrequiredqtyexcess1').val('');
        $('#txtnremarksexcess1').val('');
        $('#txtnexcessboardexcess1').val('');
        $('#txtnqtyexcessboardexcess1').val('')
        $('#txtnreasonexcess').val('');

        for(var i = 0; i < excessitemarray.length; i++){

            document.getElementById(excessitemarray[i].toString()).remove();

        }

        excessitemarray = [];

    }

    function LoadCustomers(id){

        $.ajax({    
          url: '{{ url("api/planning/loadcustomers") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            $('#'+id).find('option').remove();
            $('#'+id).append(response.content);

          },
          complete: function(){

            $('#'+id).val("All").trigger("change");

          }
        });

    }

    function LoadPurchaseRequestInformation(customer){

        $.ajax({
            url: '{{ url("api/planning/loadpurchaserequestinformation") }}',
            type: 'get',
            data: {
                customer: customer
            },
            dataType: 'json',
            success: function(response){

                $('#cmbpurchaserequest').find('option').remove();
                $('#cmbpurchaserequest').append(response.content);

            }
        });

    }

    function LoadPurchaseRequestData(prid){

        $.ajax({
            url: '{{ url("api/planning/loadpurchaserequestdata") }}',
            type: 'get',
            data: {
                prid: prid
            },
            dataType: 'json',
            success: function(response){

                itemcount = response.itemcount;
                itemarray = response.itemarray;
                $('#cmbnfgitems').val(response.fgid).trigger('change');
                $('#nmritems').find('tr').remove();
                $('#nmritems').append(response.content);


            },
            complete: function(){

                $('[name="cmbnrawitems[]"]').select2({
                    theme: 'bootstrap'
                });

                $('[name="txtnexcessboard[]"]').select2({
                    tags: true,
                    tokenSeparators: [',']
                    // theme: 'bootstrap'
                });

                $('[name="txtnqtyexcessboard[]"]').select2({
                    tags: true,
                    tokenSeparators: [',']
                    // theme: 'bootstrap'
                });

            }
        });

    }

    function LoadCustomerFGSalesOrder(fgid){

        $.ajax({
            url: '{{ url("api/planning/loadcustomerfgsalesorder") }}',
            type: 'get',
            data: {
                fgid: fgid
            },
            dataType: 'json',
            success: function(response){

                $('#tblexcesssocontent').find('tr').remove();
                $('#tblexcesssocontent').append(response.content);

            },
            complete: function(){

                $('#tblexcessso').fadeIn();

            }
        });

    }

    function LoadSelect2(){

        $('#cmbcustomer').select2({
            theme: 'bootstrap'
        });

        $('#cmbncustomer').select2({
            theme: 'bootstrap'
        });

        $('#cmbpurchaserequest').select2({
            theme: 'bootstrap'
        });

        $('#cmbnfgitems').select2({
            theme: 'bootstrap'
        });

        $('#cmbncustomerexcess').select2({
            theme: 'bootstrap'
        });

        $('#cmbnfgitemsexcess').select2({
            theme: 'bootstrap'
        });

    }

    function ReloadMaterialRequest(){

        var customer = $('#cmbcustomer').val();
        if(customer!=null){
          tblmaterialrequest.ajax.reload();
        }

    }

    //Socket Function
    socket.on('reloadplanningmrirequest', function(){
        ReloadMaterialRequest();
    });


  </script>  

@endsection