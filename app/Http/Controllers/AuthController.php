<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auth_Model;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    
    function AuthLogin(Request $request){

        $authinfo = Auth_Model::LoadAuthUserInfo($request->username);

        if(count($authinfo)!=0){

            if(Hash::check($request->password, $authinfo[0]->password)) {
            
                $validation = Auth_Model::AuthLogin($authinfo[0]->usertype);

                if($validation){

                    return json_encode([
                        "success"=>true,
                        "message"=>"User information has no access.",
                        "id"=>$authinfo[0]->id,
                        "name"=>$authinfo[0]->name
                    ]);


                }
                else{

                    return json_encode([
                        "success"=>false,
                        "message"=>"User information has no access."
                    ]);

                }

            }
            else{
    
                return json_encode([
                    "success"=>false,
                    "message"=>"Invalid username or password."
                ]);
    
            }

        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"Invalid username or password."
            ]);

        }

    }

}
