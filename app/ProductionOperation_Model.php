<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductionOperation_Model extends Model
{
    
    public static function LoadJobOrder(){

        $result = DB::table('joborders')
        ->select(
            'joid',
            'jonumber'
        )
        ->where('status', '=', 'Open')
        ->whereRaw('joid NOT IN (SELECT joid FROM jobordermonitoring WHERE status="Open")')
        ->get();

        return $result;

    }

    public static function GetJOFGInformation($data){

        $result = DB::table('joborders')
        ->select(
            'finishgoods.processcode'
        )
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('joborders.joid', '=', $data->joid)
        ->first();

        return $result->processcode;

    }

    public static function GetFirstItemProcess($processcode){

        $result = DB::table('processcode')
        ->select(
            'processid'
        )
        ->where('processcode', '=', $processcode)
        ->orderBy('processorder', 'ASC')
        ->get();

        return $result;

    }

    public static function SaveOperation($data, $issuedby, $currentpid){

        $result = DB::table('jobordermonitoring')
        ->insertGetId([
            "joid"=>$data->joid,
            "operator"=>$issuedby,
            "currentpid"=>$currentpid,
            "status"=>"Open",
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function SaveOperationProcess($mid, $processid, $counter, $targetoutput){

        if($counter==0){

            DB::table('jobordermonitoringaudit')
            ->insert([
                "mid"=>$mid,
                "pid"=>$processid,
                "targetoutput"=>$targetoutput,
                "balance"=>$targetoutput,
                "created_at"=>DB::raw("NOW()"),
                "isshow"=>"1"
            ]);

        }
        else{

            DB::table('jobordermonitoringaudit')
            ->insert([
                "mid"=>$mid,
                "pid"=>$processid,
                "targetoutput"=>"0",
                "balance"=>"0",
                "created_at"=>DB::raw("NOW()")
            ]);

        }

    }

    public static function LoadOperationInformation(){

        $result = DB::table('jobordermonitoring')
        ->select(
            'jobordermonitoring.mid',
            'joborders.jonumber',
            'joborders.targetoutput'
        )
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->where('jobordermonitoring.status', '=', 'Open')
        ->get();

        return $result;

    }

    public static function GetOperationInfo($mid){

        $result = DB::table('jobordermonitoring')
        ->select(
            'jobordermonitoring.mid',
            'joborders.jonumber',
            'jobordermonitoring.operator',
            'jobordermonitoring.joid',
            'jobordermonitoring.currentpid'
        )
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->where('jobordermonitoring.mid', '=', $mid)
        ->first();

        return $result;

    }

    public static function LoadOperationInfo($mid){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'process.process',
            'jobordermonitoringaudit.operator',
            'jobordermonitoringaudit.qty',
            'jobordermonitoringaudit.rejectqty'
        )
        ->join('process', 'process.pid', '=', 'jobordermonitoringaudit.pid')
        ->where('mid', '=', $mid)
        ->get();

        return $result;

    }

    public static function GetGroupProcess($processcode){

        $result = DB::table('processcode')
        ->select(
            'processcode.id',
            'processcode.processcode',
            'processcode.processorder',
            'processcode.processid',
            'process.process'
        )
        ->join('process', 'process.pid', '=', 'processcode.processid')
        ->where('processcode.processcode', '=', $processcode)
        ->orderBy('processcode.processorder', 'ASC')
        ->get();
        
        return $result;

    }

    public static function ValidateMonitorJO($data){

        $result = DB::table('jobordermonitoring')
        ->select(
            DB::raw("COUNT(*) AS 'jom_count'")
        )
        ->where('joid', '=', $data->joid)
        ->where('status', '=', 'Open')
        ->first();

        if($result->jom_count!=0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SaveProcessInformation($data, $issuedby){

        DB::table('jobordermonitoringaudititems')
        ->insert([
            "auditid"=>$data->auditid,
            "operator"=>$issuedby,
            "qty"=>$data->qty,
            "rejectqty"=>$data->qtyreject,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function UpdateMonitorJO($data, $issuedby){

        DB::table('jobordermonitoring')
        ->where('mid', '=', $data->mid)
        ->update([
            "currentpid"=>$data->cpid,
            "operator"=>$issuedby
        ]);

    }

    public static function UpdateMonitorJOClose($data, $issuedby){

        DB::table('jobordermonitoring')
        ->where('mid', '=', $data->mid)
        ->update([
            "status"=>"Close",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetJOFinishGoodsInformation($data){

        $result = DB::table('joborders')
        ->select(
            'finishgoods.processcode'
        )
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('joborders.joid', '=', $data->joid)
        ->first();

        return $result;

    }

    public static function LoadJobOrderProcess($mid, $processcode){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'jobordermonitoringaudit.auditid',
            'jobordermonitoringaudit.pid',
            'process.process',
            DB::raw("(SELECT COUNT(*) FROM jobordermonitoringaudititems WHERE auditid=jobordermonitoringaudit.auditid) AS 'processcount'")
        )
        ->join('process', 'process.pid', '=', 'jobordermonitoringaudit.pid')
        ->where('jobordermonitoringaudit.mid', '=', $mid)
        ->get();

        return $result;

    }

    public static function LoadProcessInfo($data){

        $result = DB::table('process')
        ->select(
            'process'
        )
        ->where('pid', '=', $data->pid)
        ->first();

        return $result;

    }

    public static function LoadJobOrderProcessInfo($data){

        $result = DB::table('jobordermonitoringaudititems')
        ->select(
            'operator',
            'qty',
            'rejectqty',
            DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d %h:%i:%s %p') AS 'date_created'")
        )
        ->where('auditid', '=', $data->auditid)
        ->get();

        return $result;

    }

    public static function LoadMonitoringProcess($mid){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'process.pid',
            'jobordermonitoringaudit.auditid',
            'process.process'
        )
        ->join('process', 'process.pid', '=', 'jobordermonitoringaudit.pid')
        ->where('jobordermonitoringaudit.mid', '=', $mid)
        ->get();

        return $result;

    }

    public static function CheckCurrentProcess($data){

        $result = DB::table('jobordermonitoring')
        ->select(
            DB::raw("
                IF(currentpid!='". $data->pid ."', 'TRUE', 'FALSE') AS 'validation'
            ")
        )
        ->where('mid', '=', $data->mid)
        ->first();

        if($result->validation=="TRUE"){

            return true;

        }
        else{

            return false;

        }

    }

    public static function ChangeCurrentProcess($data){

        DB::table('jobordermonitoring')
        ->where('mid', '=', $data->mid)
        ->update([
            "currentpid"=>$data->pid,
            "updated_at"=>DB::raw("NOW()")
        ]);

        DB::table('jobordermonitoringaudit')
        ->where('auditid', '=', $data->auditid)
        ->update([
            "isshow"=>"1",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadJobOrderForRequest(){

        $result = DB::table('joborders')
        ->select(
            'joid',
            'jorequestnumber'
        )
        ->where('status', '=', 'For Request')
        ->get();

        return $result;

    }

    public static function UpdateCreateJO($data, $tableschema, $issuedby){

        $result = DB::table('joborders')
        ->where('joid', '=', $data->joid)
        ->update([
            "jonumber"=>DB::raw("(SELECT CONCAT('JO', DATE_FORMAT(NOW(), '%m-%y-'),LPAD(AUTO_INCREMENT, 5, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'joborders')"),
            'issuedby'=>$issuedby,
            'style'=>$data->style,
            'inkcolor'=>$data->inkcolor,
            'specialinstruction'=>$data->specialinstruction,
            'deliverydatefrom'=>$data->deldatefrom,
            'deliverydateto'=>$data->deldateto,
            'status'=>'Open',
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function GetJOTargetOutput($joid){

        $result = DB::table('joborders')
        ->select(
            'targetoutput'
        )
        ->where('joid', '=', $joid)
        ->first();

        return $result->targetoutput;

    }

    public static function LoadJORequestInformation($joid){

        $result = DB::table('joborders')
        ->select(
            'customers.customer',
            'pocustomerinformation.ponumber',
            'finishgoods.description',
            'materialrequest.mrinumber',
            'joborders.targetoutput',
            DB::raw("(
                SELECT GROUP_CONCAT(remarks) FROM materialrequestitems WHERE mrid=joborders.requestid
            ) AS 'specialinstruction'")
        )
        ->join('customers', 'customers.cid', '=', 'joborders.cid')
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'joborders.poid')
        ->join('materialrequest', 'materialrequest.requestid', '=', 'joborders.requestid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('joborders.joid', '=', $joid)
        ->first();

        return $result;

    }

}
