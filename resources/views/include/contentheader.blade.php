<section class="content-header">
        <h1>
          <br>
        </h1>
        <ol class="breadcrumb">
          <li><label href="#" id="lbltimer"><i class="fa fa-clock-o"></i> Server Time: </label></li>
        </ol>
</section>
<script>


$(document).ready(function(){

    startTime();
    
});

function startTime() {

    var today = new Date();
    var h = today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();

    //var amPM = 12 < today.getHours() ?  "AM" : "PM";
    var amPM;
    if(today.getHours()>=12){
        amPM = "PM";
    }
    else{
        amPM = "AM";
    }
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('lbltimer').innerHTML = "<i class='fa fa-clock-o'></i> Server Time: "+ h + ":" + m + ":" + s + " " + amPM;
    var t = setTimeout(startTime, 500);

}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

</script>