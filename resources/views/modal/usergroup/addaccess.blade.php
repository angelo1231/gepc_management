<!-- Middle Modal -->
<style>
    .modal {
      text-align: center;
      padding: 0!important;
    }
    
    .modal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }
    
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
</style>  

<!-- Modal -->
<div class="modal fade" id="addaccess" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <strong>Add Access</strong>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        
                <div class="form-group">
                    <label for="cmbuseraccess">Group Access</label>
                    <select id="cmbuseraccess" name="cmbuseraccess" class="form-control form-control-chosen" data-placeholder="Please select..." multiple>
                    </select>
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label><input type="checkbox" id="chkselectall" name="chkselectall">Select All</label>
                    </div>
                </div>

        </div>
        <div class="modal-footer">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">
                <button id="btnnsaveaccess" name="btnnsaveaccess" type="button" class="btn btn-primary btn-block btn-flat">Save</button>
            </div>
        </div>
    </div>
</div>
</div>