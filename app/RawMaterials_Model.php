<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class RawMaterials_Model extends Model
{
   
    public static function LoadSupplier(){

        $result = DB::table('suppliers')
        ->select('sid', 'supplier')
        ->get();

        return $result;

    }

    public static function LoadCurrency(){

        $result = DB::table('currency')
        ->select('currencyid', 'currency')
        ->get();

        return $result;

    }

    public static function LoadFlute(){

        $result = DB::table('rawmaterials')
        ->select(DB::raw('DISTINCT flute'))
        ->where('flute', '!=', '')
        ->get();

        return $result;

    }

    public static function LoadBoardPound($flute){

        $result = DB::table('rawmaterials')
        ->select(DB::raw('DISTINCT boardpound'))
        ->where('flute', '=', $flute)
        ->where('boardpound', '!=', '')
        ->get();

        return $result;

    }

    public static function RawMaterialInformation($sort){

        if($sort=="All"){

            $result = DB::table('rawmaterials')
            ->select(
                'rawmaterials.name',
                'rawmaterials.description',
                'rawmaterials.rmid',
                'rawmaterials.width',
                'rawmaterials.widthsize',
                'rawmaterials.length',
                'rawmaterials.lengthsize',
                'rawmaterials.flute',
                'rawmaterials.boardpound',
                'rawmaterials.price',
                'rawmaterials.stockonhand',
                'currency.currency',
                'rawmaterials.category'
            )
            ->leftjoin('currency', 'currency.currencyid', '=', 'rawmaterials.currencyid')
            ->get();
    
            return $result;

        }
        else{

            $result = DB::table('rawmaterials')
            ->select(
                'rawmaterials.name',
                'rawmaterials.description',
                'rawmaterials.rmid',
                'rawmaterials.width',
                'rawmaterials.widthsize',
                'rawmaterials.length',
                'rawmaterials.lengthsize',
                'rawmaterials.flute',
                'rawmaterials.boardpound',
                'rawmaterials.price',
                'rawmaterials.stockonhand',
                'currency.currency',
                'rawmaterials.category'
            )
            ->leftjoin('currency', 'currency.currencyid', '=', 'rawmaterials.currencyid')
            ->where('rawmaterials.category', '=', $sort)
            ->get();
    
            return $result;

        }

    }

    public static function ValidateRawMaterial($data, $category){

        if($category=="Raw Material"){

            $result = DB::table('rawmaterials')
            ->select(DB::raw('COUNT(*) AS rawmaterial_count'))
            ->where('width', '=', $data->width)
            ->where('widthsize', '=', $data->widthsize)
            ->where('length', '=', $data->length)
            ->where('lengthsize', '=', $data->lengthsize)
            ->where('flute', '=', $data->flute)
            ->where('boardpound', '=', $data->boardpound)
            ->get();
    
            return $result[0]->rawmaterial_count;


        }
        else{

            $result = DB::table('rawmaterials')
            ->select(DB::raw('COUNT(*) AS rawmaterial_count'))
            ->where('name', '=', $data->name)
            ->get();
    
            return $result[0]->rawmaterial_count;

        }


    }

    public static function RMNewItem($data){

        DB::table('rawmaterials')
        ->insert([
            "name"=>$data->name,
            "description"=>$data->description,
            "width"=>$data->width,
            "widthsize"=>$data->widthsize,
            "length"=>$data->length,
            "lengthsize"=>$data->lengthsize,
            "flute"=>$data->flute,
            "boardpound"=>$data->boardpound,
            "price"=>$data->price,
            "currencyid"=>$data->currencyid,
            "category"=>$data->category,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function RawMaterialsProfile($rmid){

        $result = DB::table('rawmaterials')
        ->select(
            'name',
            'description',
            'width',
            'widthsize',
            'length',
            'lengthsize',
            'flute',
            'boardpound',
            'price',
            'currencyid',
            'category'
        )
        ->where('rmid', '=', $rmid)
        ->get();

        return $result;

    }

    public static function RMUpdateItem($data){

        DB::table('rawmaterials')
        ->where('rmid', '=', $data->id)
        ->update([
            "name"=>$data->name,
            "description"=>$data->description,
            "width"=>$data->width,
            "widthsize"=>$data->widthsize,
            "length"=>$data->length,
            "lengthsize"=>$data->lengthsize,
            "flute"=>$data->flute,
            "boardpound"=>$data->boardpound,
            "price"=>$data->price,
            "currencyid"=>$data->currencyid,
            "category"=>$data->category
        ]);

    }

    public static function LoadPONumberIndividual($rmid){

        $result = DB::table('poinformationitems')
        ->select(
            'poinformation.poid',
            'poinformation.ponumber'
        )
        ->leftjoin('poinformation', 'poinformation.poid', '=', 'poinformationitems.poid')
        ->where('poinformationitems.rmid', '=', $rmid)
        ->where('poinformation.remarks', '=', 'Open')
        ->get();

        return $result;

    }

    public static function LoadPORawMaterialProfile($rmid, $poid){

        $result = DB::table('poinformationitems')
        ->select(
            'poinformationitems.itemid',
            'rawmaterials.name',
            'rawmaterials.description',
            'rawmaterials.width',
            'rawmaterials.widthsize',
            'rawmaterials.length',
            'rawmaterials.lengthsize',
            'rawmaterials.flute',
            'rawmaterials.boardpound',       
            'poinformationitems.qty',
            'poinformationitems.delqty',
            'rawmaterials.category'
        )
        ->leftjoin('rawmaterials', 'rawmaterials.rmid', '=', 'poinformationitems.rmid')
        ->where('poinformationitems.rmid', '=', $rmid)
        ->where('poinformationitems.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function GetRMStockOnHand($rmid){

        $result = DB::table('rawmaterials')
        ->select('stockonhand')
        ->where('rmid', '=', $rmid)
        ->get();

        return $result[0]->stockonhand;

    }

    public static function GetPOItemDeliverdQty($itemid){

        $result = DB::table('poinformationitems')
        ->select('delqty')
        ->where('itemid', '=', $itemid)
        ->get();

        return $result[0]->delqty; 

    }

    public static function UpdateRMStockOnHand($rmid, $stockonhand){

        date_default_timezone_set('Asia/Manila');

        DB::table('rawmaterials')
        ->where('rmid', '=', $rmid)
        ->update([
            'stockonhand'=>$stockonhand,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function UpdatePOItem($itemid, $delqty){

        DB::table('poinformationitems')
        ->where('itemid', '=', $itemid)
        ->update([
            'delqty'=>$delqty,
        ]);

    }

    public static function SaveRMAudit($item, $ponumber, $drnumber, $requisitionnumber, $qty, $balance, $issuedby){

        if($ponumber!=""){

            DB::table('rawmaterialaudit')
            ->insert([
                'rmid'=>$item,
                'ponumber'=>$ponumber,
                'drnumber'=>$drnumber,
                'qty'=>$qty,
                'balance'=>$balance,
                'issuedby'=>$issuedby,
                'issueddate'=>DB::raw("NOW()"),
                'issuedtime'=>DB::raw("CURTIME()"),
            ]);

        }
        else{

            DB::table('rawmaterialaudit')
            ->insert([
                'rmid'=>DB::raw("(SELECT rmid FROM materialrequestitems WHERE id=". $item .")"),
                'requisitionnumber'=>$requisitionnumber,
                'qty'=>$qty,
                'balance'=>$balance,
                'issuedby'=>$issuedby,
                'issueddate'=>DB::raw("NOW()"),
                'issuedtime'=>DB::raw("CURTIME()"),
            ]);

        }

    }

    public static function ValidatePOItems($poid){

        $result = DB::table('poinformationitems')
        ->select(DB::raw("IF(qty<=delqty, 'Yes', 'No') AS 'quantity_check'"))
        ->where('poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function UpdatePORemarks($poid){

        DB::table('poinformation')
        ->where('poid', '=', $poid)
        ->update([
            'remarks'=>'Close',
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function LoadPO(){

        $result = DB::table('poinformation')
        ->select('poid', 'ponumber')
        ->where('remarks', '=', 'Open')
        ->orderBy('poid' , 'DESC')
        ->get();

        return $result;

    }

    public static function LoadPOSupplier($poid){

        $result = DB::table('poinformation')
        ->select(
            'suppliers.supplier'
        )
        ->join('suppliers', 'suppliers.sid', '=', 'poinformation.sid')
        ->where('poinformation.poid', '=', $poid)
        ->first();

        return $result->supplier;

    }

    public static function LoadPORawMaterial($poid){

        $result = DB::table('poinformationitems')
        ->select(
            'poinformationitems.itemid',
            'poinformationitems.rmid',
            'rawmaterials.name',
            'rawmaterials.description',
            'rawmaterials.width',
            'rawmaterials.widthsize',
            'rawmaterials.length',
            'rawmaterials.lengthsize',
            'rawmaterials.flute',
            'rawmaterials.boardpound',
            'poinformationitems.qty',
            'poinformationitems.delqty',
            'rawmaterials.category'
        )
        ->leftjoin('rawmaterials', 'rawmaterials.rmid', '=', 'poinformationitems.rmid')
        ->where('poinformationitems.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function LoadRMAuditInformation($start, $end){

        $result = DB::table('rawmaterialaudit')
        ->select(
            'rawmaterials.name',
            'rawmaterials.description',
            'rawmaterials.width',
            'rawmaterials.widthsize',
            'rawmaterials.length',
            'rawmaterials.lengthsize',
            'rawmaterials.flute',
            'rawmaterials.boardpound',
            'rawmaterialaudit.ponumber',
            'rawmaterialaudit.requisitionnumber',
            'rawmaterialaudit.qty',
            'rawmaterialaudit.balance',
            'rawmaterialaudit.issuedby',
            'rawmaterialaudit.issueddate',
            DB::raw("TIME_FORMAT(rawmaterialaudit.issuedtime, '%h:%i:%s %p') AS 'issuedtime'"),
            'rawmaterials.category'
        )
        ->leftjoin('rawmaterials', 'rawmaterials.rmid', '=', 'rawmaterialaudit.rmid')
        ->whereBetween(DB::raw("DATE_FORMAT(rawmaterialaudit.issueddate, '%Y-%m-%d')"), [$start, $end])
        ->get();

        return $result;

    }

    public static function LoadAuditRMInformation($rmid, $start, $end){

        $result = DB::table('rawmaterialaudit')
        ->select(
            'ponumber',
            'drnumber',
            'requisitionnumber',
            'qty',
            'balance',
            'issuedby',
            'issueddate',
            DB::raw("TIME_FORMAT(issuedtime, '%h:%i:%s %p') AS 'issuedtime'")
        )
        ->where('rmid', '=', $rmid)
        ->whereBetween(DB::raw("DATE_FORMAT(issueddate, '%Y-%m-%d')"), [$start, $end])
        ->get();

        return $result;

    }

    public static function ImportRawMaterials($name, $description, $width, $widthsize, $length, $lengthsize, $flute, $boardpound, $price, $currency, $category){

        DB::table('rawmaterials')
        ->insert([
            'name'=>$name,
            'description'=>$description,
            'width'=>$width,
            'widthsize'=>$widthsize,
            'length'=>$length,
            'lengthsize'=>$lengthsize,
            'flute'=>$flute,
            'boardpound'=>$boardpound,
            'price'=>$price,
            'category'=>$category,
            'currencyid'=>DB::raw("(SELECT currencyid FROM currency WHERE currency='". $currency ."')")
        ]);

    }

    public static function GetOverRunQtyItem($itemid){

        $result = DB::table('poinformationitems')
        ->select(
            DB::raw("
                (SELECT ( (((poinformationitems.qty * overrunpercentage) + poinformationitems.qty) - poinformationitems.delqty) ) FROM settings) AS 'overrun' 
            ")
        )
        ->where('itemid', '=', $itemid)
        ->first();

        return $result->overrun;

    }

    public static function GetOverRunItemInfo($overrundata){

        $result = DB::table('poinformationitems')
        ->select(
            DB::raw("
                IF(
                    rawmaterials.`name`='None',
                    CONCAT(
                        IF(
                            rawmaterials.widthsize IS NULL,
                            rawmaterials.width,
                            CONCAT(rawmaterials.width, '.', rawmaterials.widthsize)
                        ),
                        ' x ',
                        IF(
                            rawmaterials.lengthsize IS NULL,
                            rawmaterials.length,
                            CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize)
                        ),
                        ' ',
                        rawmaterials.flute,
                        ' ',
                        rawmaterials.boardpound
                    ),
                    CONCAT(
                        rawmaterials.`name`,
                        ' ',
                        rawmaterials.description
                    )
                ) AS 'item'
            ")
        )
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'poinformationitems.rmid')
        ->whereIn('itemid', $overrundata)
        ->get();

        return $result;

    }

    public static function LoadRMStockOnHand($rmid){

        $result = DB::table('rawmaterials')
        ->select(
            'stockonhand',
            'description'
        )
        ->where('rmid', '=', $rmid)
        ->first();

        return $result;

    }

    public static function SaveRMStockOnHand($rmid, $qty){

        DB::table('rawmaterials')
        ->where('rmid', '=', $rmid)
        ->update([
            "stockonhand"=>$qty,
            "updated_at"=>DB::raw("NOW()")
        ]); 

    }

    public static function SaveRMAuditStock($rmid, $qty, $issuedby){

        DB::table('rawmaterialaudit')
        ->insert([
            'rmid'=>$rmid,
            'qty'=>$qty,
            'balance'=>$qty,
            'issuedby'=>$issuedby,
            'issueddate'=>DB::raw("NOW()"),
            'issuedtime'=>DB::raw("CURTIME()"),
        ]);

    }

    public static function SavePOItemAudit($itemid, $drnumber, $deliverdqty, $rejectqty, $goodqty){

        DB::table('poinformationitemsaudit')
        ->insert([
            "itemid"=>$itemid,
            "drnumber"=>$drnumber,
            "deliveryqty"=>$deliverdqty,
            "rejectqty"=>$rejectqty,
            "goodqty"=>$goodqty,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadMRCount(){

        $result = DB::table('materialrequest')
        ->select(
            DB::raw("COUNT(*) AS 'mrcount'")
        )
        ->where('status', '=', 'Pending')
        ->first();

        return $result->mrcount;

    }

}
