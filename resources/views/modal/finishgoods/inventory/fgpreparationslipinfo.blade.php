<!-- Middle Modal -->
<style>
.modal {
   text-align: center;
   padding: 0!important;
}
    
.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}
    
.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.ui-autocomplete  {
    z-index: 1100 !important;
}

</style>   

<!-- Modal -->
<div class="modal fade" id="fgpreparationslipinfo"  role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  <label id="lblinformation">{ DATA HERE }</label> 
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  
                <div class="modal-body">

                    <div class="container-fluid">

                        <table id="tblpreparationslipitems" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>Partnumber</th>
                                    <th>Partname</th>
                                    <th>Description</th>
                                    <th>Size</th>
                                    <th>Quantity</th>
                                </tr>
                            </thead>
                            <tbody id="tblpreparationslipitemscontent">
                            </tbody>
                        </table>

                        <br>
                        <div class="form-group">
                            <label for="txtremarks">Remarks</label>
                            <input type="text" id="txtremarks" name="txtremarks" class="form-control" placeholder="Remarks for disapproved">
                        </div>

                    </div>
          
                </div>
                
                <div class="modal-footer">

                    <button id="btnapprovepreparationslip" name="btnapprovepreparationslip" class="btn btn-success">Approve</button>
                    <button id="btndisapprovepreparationslip" name="btndisapprovepreparationslip" class="btn btn-primary">Disapprove</button>
                    <button data-dismiss="modal" class="btn btn-danger">Close</button>

                </div>

            </div>
        </div>
       
</div>
    