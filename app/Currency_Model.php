<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Currency_Model extends Model
{
    

    public static function CurrencyInformation(){

        $result = DB::table('currency')
        ->select('currencyid', 'currency', 'rate')
        ->get();

        return $result;

    }

    public static function ValidateCurrency($currency){

    	$result = DB::table('currency')
        ->select(DB::raw('COUNT(*) AS currency_count'))
        ->where('currency', '=', $currency)
        ->get();
        
        return $result[0]->currency_count;

    }

    public static function NewCurrency($data){

    	$result = DB::table('currency')
    	->insert([
    		"currency"=>$data->currency,
    		"rate"=>$data->rate
    	]);

    }

    public static function CurrencyProfile($id){

    	$result = DB::table('currency')
    	->select('currency', 'rate')
    	->where('currencyid', '=', $id)
    	->get();

    	return $result;

    }

    public static function UpdateCurrency($data){

    	$result = DB::table('currency')
        ->where('currencyid', '=', $data->id)
        ->update([
             'currency'=> $data->currency, 
             'rate'=> $data->rate
         ]);

    }



}
