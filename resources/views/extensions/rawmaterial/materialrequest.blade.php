@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Material Request</b></h3>
          <a href="{{ url("/rminventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <br>
                <table id="tblmaterialrequest" class="table">
                    <thead>
                        <tr>
                            <th>Finish Good</th>
                            <th>Issued By</th>
                            <th>Reason</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  @include('modal.materialrequest.approvemri')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblmaterialrequest;
    var balance;

    $(document).ready(function(){

        LoadMaterialRequestInfo();

    });

    $(document).on('click', '#btnapprove', function(){

        var id = $(this).val();
        GetMRIInventory(id);
        GenIssuanceNumber();
        $('#btnaapprove').val(id);

    });

    $(document).on('keyup', '[name="txtaqtyissued[]"]', function(){

        var id = $(this).data('id');
        var qty = $(this).val();
        var orgbalance = $('#txtabalance'+id).data('val');

        if(qty==""){

            $('#txtabalance'+id).val(orgbalance);

        }
        else{

            var bal = orgbalance - qty;
            $('#txtabalance'+id).val(bal);

        }

    });

    $('#btnaapprove').on('click', function(){

        var id = $(this).val();
        // var mrinumber = $('#txtamrinumber').val();
        var issuancenumber = $('#txtaissuancenumber').val();
        var itemid = $('[name="txtaitemid[]"]').serializeArray();
        var qtyrequired = $('[name="txtaqtyrequired[]"]').serializeArray();
        var drnumber = $('[name="txtadrnumber[]"]').serializeArray();
        var qtyissued = $('[name="txtaqtyissued[]"]').serializeArray();
        var balance = $('[name="txtabalance[]"]').serializeArray();

        if(issuancenumber==""){
            toastr.error("Please input the issuance number.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/materialrequest/approvemriinformation") }}',
                type: 'post',
                data: {
                    id: id,
                    issuancenumber: issuancenumber,
                    itemid: itemid,
                    qtyrequired: qtyrequired,
                    drnumber: drnumber,
                    qtyissued: qtyissued,
                    balance: balance
                },
                dataType: 'json',
                beforeSend: function(){
                    H5_loading.show();
                },
                success: function(response){

                    if(response.success){
                        
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadMaterialRequestInfo();
                        $("#modalapprovemri .close").click();

                        //Socket Emit
                        socket.emit('rminvinformation');
                        socket.emit('rminvplanningmri');

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                },
                complete: function(){
                    H5_loading.hide();
                }
            });

        }
            

    });

    $(document).on('click', '#btndisapprove', function(){

        var id = $(this).val();

        $.confirm({
              title: 'Disapprove',
              content: 'Disapprove this material request?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        Disapprove(id);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                } 
            }
        });

    });

    function Disapprove(id){

        $.ajax({
            url: '{{ url("api/materialrequest/disapprovemr") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadMaterialRequestInfo();

                    //Socket Emit
                    socket.emit('rminvinformation');
                    socket.emit('rminvplanningmri');

                }

            }
        });

    }

    function LoadMRINumber(){

        $.ajax({
            url: '{{ url("api/materialrequest/genmrinumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtamrinumber').val(response.batch);

            }
        });

    }

    function GenIssuanceNumber(){

        $.ajax({
            url: '{{ url("api/materialrequest/genissuancenumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtaissuancenumber').val(response.issuancenumber);

            }
        });

    }

    function GetMRIInventory(id){

        $.ajax({
            url: '{{ url("api/materialrequest/getmriinventory") }}',
            type: 'get',
            data: {
                id: id,
            },
            dataType: 'json',
            success: function(response){

                $('#tblapprovemrcontent').find('tr').remove();
                $('#tblapprovemrcontent').append(response.content);

            }
        });

    }

    function LoadMaterialRequestInfo(){

        tblmaterialrequest = $('#tblmaterialrequest').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                ordering: false,
                ajax: {
                    type: 'get',
                    url: '{{ url("api/materialrequest/loadmaterialrequestinformationinv") }}',
                },
                columns : [
                    {data: 'finishgood', name: 'finishgood'},
                    {data: 'issuedby', name: 'issuedby'},
                    {data: 'reason', name: 'reason'},
                    {data: 'panel', name: 'panel', width: '12%'},
                ]
        });

    }

    function ReloadMaterialRequestInfo(){
        
        tblmaterialrequest.ajax.reload();

    }

    //Socket functions
    socket.on('reloadmrplanninginfo', function(){
        ReloadMaterialRequestInfo();
    });

  </script>  

@endsection