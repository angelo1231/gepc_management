<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use App\Purchasing_Model;
use DataTables;
use Illuminate\Support\Collection;
use App\Jobs\POSendEmail;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SettingsController;
use Dompdf\Dompdf;
use Mail;

class PurchasingController extends Controller
{

    private $tableschema;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }
    
    function index($view=null, $action=null, $id=null){

        $access = UserController::UserGroupAccess();

        if($view==null){

            return view('purchasing')
            ->with('access', $access);

        }
        else{

            $datainfo;
            $dataitems;

            if($action!=null && $id!=null){

                $info = Purchasing_Model::POInfo($id);
                $items = Purchasing_Model::POInfoitems($id);
                if($action=="info"){
                    $deliverydates = Purchasing_Model::PODeliveryDatesInfo($id);
                }
                else{
                    $deliverydates = Purchasing_Model::PODeliveryDates($id);
                }

                return view('extensions.purchasing.'.$view)
                ->with('poid', $id)
                ->with('info', $info)
                ->with('items', $items)
                ->with('deliverydates', $deliverydates)
                ->with('access', $access);

            }
            else{

                return view('extensions.purchasing.'.$view)
                ->with('access', $access);

            }
            
        }


    }

    function GenPONumber(){

        $batch = Purchasing_Model::GenPONumber($this->tableschema);

        return json_encode([
            "batch"=>$batch
        ]);

    }

    function LoadSupplier(){

        $supplier = Purchasing_Model::LoadSupplier();
        return json_encode(["data"=>$supplier]);

    }

    function SupplierProfile(Request $request){

        $data = Purchasing_Model::SupplierProfile($request->id);
        return json_encode([
            "supplier"=>$data[0]->supplier,
            "contactperson"=>$data[0]->contactperson,
            "address"=>$data[0]->address,
            "tin"=>$data[0]->tin,
            "terms"=>$data[0]->terms
        ]);

    }

    function LoadItem(Request $request){

       $data = Purchasing_Model::LoadItem($request->id);

       return json_encode([
           "data"=>$data
       ]);

    }

    function RawMaterialProfile(Request $request){

        $data = Purchasing_Model::RawMaterialProfile($request->rmid);
        return json_encode([
            "description"=>$data[0]->description,
            "flute"=>$data[0]->flute,
            "boardpound"=>$data[0]->boardpound,
            "description"=>$data[0]->description,
            "price"=>$data[0]->price
        ]);

    }

    function Currency(Request $request){

        $data = Purchasing_Model::Currency($request->rmid);

        $total = ($data[0]->rate * $request->price) * $request->qty;

        return json_encode([
            "total"=>$total
        ]);

    }

    function SavePOInformation(Request $request){

        $grandtotal = 0.00;

        for($i=0;$i<count($request->total);$i++){ //Grandtotal

            $grandtotal +=  $request->total[$i]["value"];

        }

        $userid = Auth::user()->id;
        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
        $poid = Purchasing_Model::SavePOInformation($grandtotal, $request->sid, $issuedby, $request->attn, $request->delto, $request->delcharge, $request->deldate, $this->tableschema, $userid, $request->note);
        
        for($y=0;$y<count($request->rmid);$y++){

            $data = Purchasing_Model::Currency($request->rmid[$y]["value"]);

            Purchasing_Model::SavePOInformationItems($poid, $request->rmid[$y]["value"], $data[0]->currencyid, $data[0]->rate, $request->price[$y]["value"],$request->qty[$y]["value"], $request->total[$y]["value"]);

            Purchasing_Model::UpdatePriceRM($request->rmid[$y]["value"], $request->price[$y]["value"]);

        }

        Session::flash('message', "Purchase information has been save.");
        return json_encode([
            "success"=>true
        ]);

    }

    function LoadPOInformation(Request $request){

        $po = Purchasing_Model::LoadPOInformation($request->supplier);
        
        $data = array();
        for($i=0;$i<count($po);$i++){

            //Get PO Item Delivery Dates
            $deliverydates = Purchasing_Model::GetItemDeliveryDates($po[$i]->poid);

            $obj = new \stdClass;

            if($po[$i]->remarks=="Close"){
                $obj->status = '<i class="fa fa-check"></i>';   
            }
            else{
                $obj->status = '';   
            }

            $obj->PO = $po[$i]->ponumber;
            $obj->attn = $po[$i]->attentionto;
            $obj->totalqty = $po[$i]->totalqty;
            $obj->deliverydate = $deliverydates;
            $obj->grandtotal = number_format($po[$i]->grandtotal, 2);
            $obj->createdby = $po[$i]->issuedby;
            $obj->createddate = $po[$i]->created_at;    
            $obj->supplier = $po[$i]->supplier;
            $obj->approvedby = $po[$i]->approvedby;
            $obj->approveddate = $po[$i]->approveddate;
            if($po[$i]->approvedby!=""){

                $obj->panel = '<a href="'. url("/purchasing/purchaseorderinfo/info/". $po[$i]->poid) .'"><button id="btninfo" name="btninfo" class="btn btn-info btn-flat" title="View Information" value="'. $po[$i]->poid .'" style="width: 40px;"><i class="fa fa-info"></i></button></a> <a href="'. url("/purchasing/purchaseorderprint/print/". $po[$i]->poid) .'" target="_blank"><button id="btnprint" name="btnprint" class="btn btn-primary btn-flat" title="Print" value="'. $po[$i]->poid .'" style="width: 40px;"><i class="fa fa-print"></i></button></a>';

            }
            else{

                $obj->panel = '<a href="'. url("/purchasing/purchaseorderinfo/info/". $po[$i]->poid) .'"><button id="btninfo" name="btninfo" class="btn btn-info btn-flat" title="View Information" value="'. $po[$i]->poid .'" style="width: 40px;"><i class="fa fa-info"></i></button></a>';

            }

            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'status', 'deliverydate'])->make(true);

    }

    function AutoNewPurchaseOrder(Request $request){

        $data = [];
        $priddata = [];
        $grandtotal = 0.00;
        $userid = Auth::user()->id;
        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        for($y=0;$y<count($request->pritemid);$y++){

            $data[] = [
                "pritemid"=>$request->pritemid[$y]["value"],
                "price"=>$request->price[$y]["value"],
                "papercomid"=>$request->papercom[$y]["value"]
            ];

        }

        //Compute Grand Total
        for($i=0;$i<count($data);$i++){

            $total = Purchasing_Model::GetPRItemTotal($data[$i]["pritemid"], $data[$i]["price"]);

            $grandtotal += $total->total;

        }

        //Save PO Information
        $poid = Purchasing_Model::SavePOInformation($grandtotal, $request->sid, $issuedby, $request->attn, $request->delto, $request->delcharge, $this->tableschema, $userid, $request->note);

        //Save PO Item Information
        for($i=0;$i<count($data);$i++){

            $item = Purchasing_Model::GetPRItemData($data[$i]["pritemid"], $data[$i]["price"]);

            $priddata[] = $item->prid;

            $poitemid = Purchasing_Model::SavePOInformationItems($poid, $item->rmid, $item->currencyid, $item->rate, $data[$i]["price"] , $item->qtyrequired, $item->total, $data[$i]["papercomid"], $data[$i]["pritemid"]);

            //Save Delivery Information
            Purchasing_Model::SavePRItemDeliveryInfo($poitemid, $data[$i]["pritemid"]);

            //Update Price RM
            Purchasing_Model::UpdateRMPrice($item->rmid, $data[$i]["price"]);

        }

        //Update Status Purchase Request
        Purchasing_Model::UpdatePurchaseRequestInfoPurchasing($priddata);

        return json_encode([
            "success"=>true,
            "message"=>"Purchase order information has been saved.",
            "data"=>$data
        ]);

    }

    function GetSupplierName(Request $request){

        $supplier = Purchasing_Model::GetSupplierName($request);

        return json_encode([
            "supplier"=>$supplier
        ]);

    }

    function LoadCountPR(){

        $prcount =  Purchasing_Model::LoadCountPR();

        return json_encode([
            "prcount"=>$prcount
        ]);

    }

    function ApprovePO(Request $request){

        $access = UserController::UserGroupAccess();
        $userid = Auth::user()->id;
        $approvedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        if(in_array(65, $access)){

            Purchasing_Model::ApprovePO($request->poid, $approvedby, $userid);

            return json_encode([
                "success"=>true,
                "message"=>"Purchase order has been approved."
            ]);

        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"You dont have any authority to approve this purchase order."
            ]);

        }

    }

    function POConvertPDF(Request $request){

        $dompdf = new Dompdf();

        $info = Purchasing_Model::POInfo($request->poid);
        $items = Purchasing_Model::POInfoitems($request->poid);
        $deliverydates = Purchasing_Model::PODeliveryDates($request->poid);

        $html = view('pdflayout.purchasing.purchaseorderpdf')
        ->with('poid', $request->poid)
        ->with('info', $info)
        ->with('items', $items)
        ->with('deliverydates', $deliverydates);

        $dompdf->set_option('defaultFont', 'Helvetica');
        $dompdf->set_option('enable_php', true);
        $dompdf->set_option('enable_remote', true);
        $dompdf->setPaper('A4');
        $dompdf->loadHtml($html);
        $dompdf->render();

        //Create Output
        $output = $dompdf->output();

        //Delete File
        if(file_exists(public_path('popdf/'.$request->poid.'.pdf'))){
            unlink(public_path('popdf/'.$request->poid.'.pdf'));
        }

        //Save File
        file_put_contents(public_path('popdf/'.$request->poid.'.pdf'), $output);

        //File Permission
        chmod(public_path('popdf/'.$request->poid.'.pdf'), 0777);

        return json_encode([
            "success"=>true
        ]);       

    }

    function SendPOEmail(Request $request){

        // TEST EMAIL
        // $companyinfo = SettingsController::GetCompanyInfo();
        // $suppileremail = Purchasing_Model::GetSupplierEmail($request->poid);

        // Mail::send("email.poemail", ['address'=>$companyinfo->address, 'number'=>$companyinfo->telnumber . ' / ' . $companyinfo->faxnumber], function($message) use($companyinfo, $suppileremail, $request){

        //     $message->from('goodearth.corporation@gmail.com', 'Good Earth Packaging Corp.');
        //     $message->to($suppileremail, 'GEPC');
        //     $message->subject('Purchase Order');
        //     $message->attach(public_path("popdf/".$request->poid.".pdf"));

        // });


        //Send Email To Item Suppliers
        $jobemail = (new POSendEmail($request->poid));
        //Dispatch Job
        $this->dispatch($jobemail);

        return json_encode([
            "success"=>true
        ]);

    }

    function LoadSelectedItem(Request $request){

        $pritem = Purchasing_Model::LoadSelectedItem($request->prid);

        return json_encode([
            "pritem"=>$pritem
        ]);

    }

    function GetItemInformation(Request $request){

        $iteminfo = Purchasing_Model::GetItemInformation($request->itemid);

        return json_encode([
            "price"=>$iteminfo->price,
            "qty"=>$iteminfo->qty
        ]);

    }

    function UpdateItemInformation(Request $request){

        //Get Rate
        $iteminfo = Purchasing_Model::GetItemInfoRate($request->itemid);

        //Update Item Information
        Purchasing_Model::UpdateItemInformation($request->itemid, $request->price, $request->qty, $iteminfo->rate);
        //Update Grand Total PO Information
        $grandtotal = Purchasing_Model::UpdatePOGrandTotal($request->poid);

        //Update Price RM File
        Purchasing_Model::UpdatePriceRM($iteminfo->rmid, $request->price);

        return json_encode([
            "success"=>true,
            "message"=>"Item information has been update",
            "grandtotal"=>number_format($grandtotal, 2)
        ]);


    }

    function ReloadPOItemInformation(Request $request){

        $content = "";
        $poinfostatus = Purchasing_Model::GetPOInfoStatus($request->poid);
        $items = Purchasing_Model::POInfoitems($request->poid);

        foreach($items as $item){

            $deliverycontent = "";
            $size = "";
            if($item->category=="Raw Material"){

                if($item->widthsize!=""){
                    $size = $size . $item->width . "." . $item->widthsize;
                }else{
                    $size = $size . $item->width;
                }

                if($item->lengthsize!=""){
                    $size = $size . " x " . $item->length . "." . $item->lengthsize;
                }else{
                    $size = $size . " x " . $item->length;
                }

            }
            else if($item->category=="Other"){

                $size = $item->name;

            }

            $delinfo = explode(',', $item->deliveryinfo);

            foreach($delinfo as $del){

                $deliverycontent .= $del . "<br>";

            }

            if($poinfostatus=="False"){
                $content .= '
                    <tr>
                        <td>'. $size .'</td>
                        <td>'. $item->description .'</td>
                        <td>'. $item->flute .'</td>
                        <td>'. $item->boardpound .'</td>
                        <td>'. number_format($item->price, 2) .'</td>
                        <td>'. $item->qty .'</td>
                        <td>'. number_format($item->total, 2) .'</td>
                        <td>'. $item->delqty .'</td>
                        <td>'. $deliverycontent .'</td>
                        <td>
                            <button id="btnedit" name="btnedit" class="btn btn-flat btn-info" value="'. $item->itemid .'"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr> 
                ';
            }
            else{

                $content .= '
                    <tr>
                        <td>'. $size .'</td>
                        <td>'. $item->description .'</td>
                        <td>'. $item->flute .'</td>
                        <td>'. $item->boardpound .'</td>
                        <td>'. number_format($item->price, 2) .'</td>
                        <td>'. $item->qty .'</td>
                        <td>'. number_format($item->total, 2) .'</td>
                        <td>'. $item->delqty .'</td>
                        <td>'. $deliverycontent .'</td>
                        <td>
                        </td>
                    </tr> 
                ';
                
            }

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    //Email Temp
    function Email(){

        $companyinfo = SettingsController::GetCompanyInfo();

        return view('email.poemail')
        ->with('address', $companyinfo->address)
        ->with('number', $companyinfo->telnumber . ' / ' . $companyinfo->faxnumber);

    }


}
