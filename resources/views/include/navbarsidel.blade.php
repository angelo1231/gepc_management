<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGATION</li>

        <li class="treeview">
            <a href="{{ url("/main") }}">
              <i class="fa fa-home"></i> <span>Home</span>
            </a>
        </li>

          @if(in_array(32, $access))
          <li class="treeview">
            <a href="{{ url("/purchasing") }}">
              <i class="fa fa-file-o"></i> <span>Purchasing</span>
            </a>
          </li>
          @endif

          @if(in_array(34, $access))
          <li class="treeview">
            <a href="{{ url("/planning") }}">
              <i class="fa fa-file-o"></i> <span>Planning</span>
            </a>
          </li>
          @endif

          @if(in_array(41, $access))
          <li class="treeview">
            <a href="{{ url("/rminventory") }}">
              <i class="fa fa-cubes"></i> <span>Raw Materials</span>
            </a>  
          </li>
          @endif

          @if(in_array(44, $access))
          <li class="treeview">
              <a href="{{ url("/fginventory") }}">
                <i class="fa fa-cubes"></i> <span>Finished Goods</span>
              </a>
          </li>
          @endif

          @if(in_array(47, $access))
          <li class="treeview">
              <a href="{{ url("/sales") }}">
                <i class="fa fa-money"></i> <span>Sales</span>
              </a>
          </li>
          @endif

          @if(in_array(52, $access))
          <li class="treeview">
              <a href="{{ url("/qualityassurance") }}">
                <i class="fa fa-file-o"></i> <span>Quality Assurance</span>
              </a>
          </li>
          @endif

          @if(in_array(55, $access))
          <li class="treeview">
              <a href="#">
                <i class="fa fa-bars"></i> <span>Production</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                    @if(in_array(56, $access))
                    <li>
                      <a href="{{ url("/production") }}">
                        <i class="fa fa-angle-double-right"></i> <span>Production Tags</span>
                      </a>
                    </li>
                    @endif
                    {{-- @if(in_array(58, $access))
                    <li>
                      <a href="{{ url("/productionoperation") }}"">
                        <i class="fa fa-angle-double-right"></i> <span>Production Operations</span>
                      </a>
                    </li>
                    @endif --}}
                    @if(in_array(64, $access))
                    <li>
                      <a href="{{ url("/productionmonitoring") }}">
                        <i class="fa fa-angle-double-right"></i> <span>Production Monitoring</span>
                      </a>
                    </li>
                    @endif
              </ul>
          </li>
          @endif

          @if(in_array(61, $access))
          <li class="treeview">
              <a href="#">
                <i class="fa fa-bars"></i> <span>Accounting</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                  @if(in_array(62, $access))
                  <li>
                    <a href="{{ url("/deliverysales") }}">
                      <i class="fa fa-angle-double-right"></i> <span>Delivery Sales Information</span>
                    </a>
                  </li>
                  @endif
                  {{-- <li>
                    <a href="#">
                      <i class="fa fa-angle-double-right"></i> <span>Accounting 2</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-angle-double-right"></i> <span>Accounting 3</span>
                    </a>
                  </li> --}}
              </ul>
          </li>
          @endif

          @if(in_array(1, $access))
          <li class="treeview">
              <a href="#">
                <i class="fa fa-cog"></i> <span>Maintenance</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(in_array(2, $access))
                <li>
                  <a href="{{ url("/user") }}"><i class="fa fa-angle-double-right"></i> User Accounts</a>
                </li>
                @endif
                @if(in_array(10, $access))
                <li>
                    <a href="{{ url("/rawmaterials") }}"><i class="fa fa-angle-double-right"></i> Raw Materials</a>
                </li>
                @endif
                @if(in_array(13, $access))
                <li>
                  <a href="{{ url("/finishgoods") }}"><i class="fa fa-angle-double-right"></i> Finished Goods</a>
                </li>
                @endif
                @if(in_array(22, $access))
                <li>
                  <a href="{{ url("/supplier") }}"><i class="fa fa-angle-double-right"></i> Suppliers</a>
                </li>
                @endif
                @if(in_array(25, $access))
                <li>
                  <a href="{{ url("/customer") }}"><i class="fa fa-angle-double-right"></i> Customers</a>
                </li>
                @endif
                @if(in_array(28, $access))
                <li>
                  <a href="{{ url("/currency") }}"><i class="fa fa-angle-double-right"></i> Currency</a>
                </li>
                @endif
                @if(in_array(31, $access))
                <li>
                  <a href="{{ url("/settings") }}"><i class="fa fa-angle-double-right"></i> Settings</a>
                </li>
                @endif
              </ul>
          </li>
          @endif

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>