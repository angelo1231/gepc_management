<?php
namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Supplier_Model extends Model
{

    public static function SupplierInformation(){

        $result = DB::table('suppliers')
        ->select(
            'sid', 
            'supplier', 
            'contactperson',
            'contactnumber',
            'email',
            'address', 
            'tin', 
            'terms', 
            'taxclassification'
        )
        ->get();

        return $result;

    }

    public static function ValidateSupplier($supplier){

        $result = DB::table('suppliers')
        ->select(DB::raw('COUNT(*) AS supplier_count'))
        ->where('supplier', '=', $supplier)
        ->get();
        
        return $result[0]->supplier_count;

    }

    public static function NewSupplier($data){

        $result = DB::table('suppliers')
        ->insert([
            'supplier'=>$data->supplier,
            'contactperson'=>$data->contactperson,
            'contactnumber'=>$data->contactnumber,
            'email'=>$data->email,
            'address'=>$data->address, 
            'tin'=>$data->tin,
            'terms'=>$data->terms,
            'taxclassification'=>$data->tax
        ]);

    }

    public static function SupplierProfile($sid){

        $result = DB::table('suppliers')
        ->select(
            'supplier', 
            'contactperson',
            'contactnumber',
            'email',
            'address', 
            'tin', 
            'terms',
            'taxclassification'
        )
        ->where('sid', '=', $sid)
        ->get();

        return $result;

    }

    public static function UpdateSupplier($data){

       $result = DB::table('suppliers')
       ->where('sid', '=', $data->sid)
       ->update([
            'supplier'=> $data->supplier,
            'contactperson'=>$data->contactperson,
            'contactnumber'=>$data->contactnumber,
            'email'=>$data->email, 
            'address'=> $data->address, 
            'tin'=> $data->tin,
            'terms'=>$data->terms,
            'taxclassification'=>$data->tax
        ]);

    }

}
