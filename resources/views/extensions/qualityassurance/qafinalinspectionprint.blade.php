<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Good Earth Packaging Corp.</title>

        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/print.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/ionicons.min.css') }}">

        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        

    <style>

        .table-bordered > tbody > tr > td.no-line-one {
            border-top: none !important;
            border-bottom: none !important;
            border-right-style: none !important;
        }

        .table-bordered > tbody > tr > td.no-line-two {
            border-top: none !important;
            border-bottom: none !important;
            border-right-style: none !important;
            border-left-style: none !important;
        }

        .table-bordered > tbody > tr > td.no-line-three {
            border-top: none !important;
            border-bottom: none !important;
            border-top-style: none !important;
            border-left-style: none !important;
        }

        .table-bordered > tbody > tr > td.no-line-four {
            border-top: none !important;
            border-bottom: none !important;
        }

        .table-bordered > tbody > tr > td.no-line {
            border-top: none !important;
        }

        .table-bordered{
            border:none;
        }

        @page {
            margin: 0;
        }

    </style>

    </head>

<body>


    <page size="A4">    

        <div class="col-md-12">

            <table class="table table-bordered" style="width: 100%; font-weight: bold; font-size: 10px; margin-bottom: 0px; border-bottom: 0px;">
                <tr>
                    <td colspan="6" style="text-align: center;"><strong>FINAL INSPECTION FROM</strong></td>
                </tr>
                <tr>
                    <td style="width: 20%;">Customer</td>
                    <td style="width: 30%;">{{ $joinformation->customer }}</td>
                    <td style="width: 20%;">Inspection Date</td>
                    <td colspan="3" style="width: 30%">{{ $qafinalinfo->inspectiondate }}</td>
                </tr>
                <tr>
                    <td>Partname</td>
                    <td>{{ $joinformation->partname }}</td>
                    <td>Job Order Qty</td>
                    <td colspan="3">{{ $joinformation->targetoutput }}</td>
                </tr>
                <tr>
                    <td>Partnumber</td>
                    <td>{{ $joinformation->partnumber }}</td>
                    <td style="vertical-align: middle;" rowspan="2">Qutput Qty</td>
                    <td colspan="3" rowspan="2" style="vertical-align: middle;">{{ $qafinalinfo->outputqty }}</td>
                </tr>
                <tr>
                    <td>Job Order #</td>
                    <td>{{ $joinformation->jonumber }}</td>
                    {{-- <td></td>
                    <td></td> --}}
                </tr>
                <tr>
                    <td style="text-align: center;" class="no-line-one">Measuring Tool</td>
                    <td style="text-align: center;" colspan="2" class="no-line-two">Judgement For Visual</td>
                    <td style="text-align: center;" colspan="3" class="no-line-three">General Tolerance</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="no-line-one">
                        @if ($qafinalmeasure->metertape!=0)
                            (<i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>) Meter Tape  
                        @else
                            () Meter Tape
                        @endif
                    </td>
                    <td style="text-align: center;" colspan="2" colspan="2" class="no-line-two">/ - Acceptable</td>
                    <td style="text-align: center; vertical-align: middle;" rowspan="3" colspan="3" class="no-line-three">Based On The Mechanical Drawing</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="no-line-one">
                        @if ($qafinalmeasure->caliper!=0)
                            (<i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>) Caliper  
                        @else
                            () Caliper
                        @endif
                    </td>
                    <td style="text-align: center;" colspan="2" class="no-line-two">X - Not Good/With Abnormality</td>
                </tr>
                <tr>
                    <td style="text-align: center;" class="no-line-one">
                        @if ($qafinalmeasure->meterstick!=0)
                            (<i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>) Meter Stick  
                        @else
                            () Meter Stick
                        @endif
                    </td>
                    <td style="text-align: center;" colspan="2"  class="no-line-two">(- / N/A) - Not Applicable</td>
                </tr>
            </table>
            <table class="table table-bordered" style="width: 100%; margin-top: 0px; font-weight: bold; margin-bottom: 0px; font-size: 10px;">
                <tr>
                    <td colspan="6" style="text-align: center;">Visual Check Legend</td>
                </tr>
                <?php $counter = 0; ?>
                @for ($i = 0; $i < ceil(count($rejecttype) / 3); $i++)
                <tr>
                    <?php 
                        
                        $content = "";
                        $check = "";
                        if ($counter==count($rejecttype)) {
                            
                            $content = '
                                <td style="width: 5%;"></td>
                                <td></td>
                            ';
                            echo $content;

                        }
                        else{

                            if($rejecttype[$counter]->flag!=0){
                                $check = '
                                <i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                                ';
                            }

                            $content = '
                                <td style="width: 5%;">
                                    '. $check .'
                                </td>
                                <td>'. $rejecttype[$counter]->rejectcode .':'. $rejecttype[$counter]->reject .'</td>
                            ';
                            echo $content;
                            $counter += 1;

                        }


                    ?>
                    <?php 
                        
                        $content = "";
                        $check = "";
                        if ($counter==count($rejecttype)) {
                            
                            $content = '
                                <td style="width: 5%;"></td>
                                <td></td>
                            ';
                            echo $content;

                        }
                        else{

                            if($rejecttype[$counter]->flag!=0){
                                $check = '
                                <i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                                ';
                            }

                            $content = '
                                <td style="width: 5%;">
                                    '. $check .'
                                </td>
                                <td>'. $rejecttype[$counter]->rejectcode .':'. $rejecttype[$counter]->reject .'</td>
                            ';
                            echo $content;
                            $counter += 1;

                        }


                    ?>
                    <?php 
                        
                        $content = "";
                        $check = "";
                        if ($counter==count($rejecttype)) {
                            
                            $content = '
                                <td style="width: 5%;"></td>
                                <td></td>
                            ';
                            echo $content;
                            break;

                        }
                        else{

                            if($rejecttype[$counter]->flag!=0){
                                $check = '
                                <i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                                ';
                            }

                            $content = '
                                <td style="width: 5%;">
                                    '. $check .'
                                </td>
                                <td>'. $rejecttype[$counter]->rejectcode .':'. $rejecttype[$counter]->reject .'</td>
                            ';
                            echo $content;
                            $counter += 1;

                        }

                    ?>
                @endfor
                <tr>
                    <td colspan="6" class="no-line-four">Others: {{ $qafinalothers->others }}</td>
                </tr>
            </table>
            <table class="table table-bordered" style="width: 100%; margin-top: 0px; font-weight: bold; margin-bottom: 0px; font-size: 10px;">
                <tr>
                    <td colspan="6" style="text-align: center;">Dimension Check Legend</td>
                </tr>
                <tr>
                    <td style="width: 35.2%; text-align: center;">Requirements</td>
                    <td style="width: 5%;">
                        @if ($qafinalinfo->insideoutside=="ID")
                            <i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        @endif
                    </td>
                    <td style="width: 26.1%;">Inside</td>
                    <td style="width: 5%;">
                        @if ($qafinalinfo->insideoutside=="OD")
                            <i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        @endif
                    </td>
                    <td colspan="2">Outside</td>
                </tr>
            </table>

            <table class="table table-bordered" style="width: 100%; margin-top: 0px; font-weight: bold; margin-bottom: 0px; font-size: 10px;">
                <tr>
                    <td colspan="2" class="no-line" style="text-align: center; width: 35.2%;">Length</td>
                    <td colspan="2" class="no-line" style="text-align: center; width: 31.1%;">Width</td>
                    <td colspan="2" class="no-line" style="text-align: center;">Height</td>
                </tr>
                <tr>
                    <td class="no-line" style="width: 2%;">S</td>
                    <td colspan="1" class="no-line" style="text-align: center; width: 32.1%;">
                        @if ($qafinalinfo->insideoutside=="OD")
                            {{ $OD_length }}
                        @else
                            {{ $ID_length }}
                        @endif
                    </td>
                    <td colspan="2" class="no-line" style="text-align: center; width: 31.1%;">
                        @if ($qafinalinfo->insideoutside=="OD")
                            {{ $OD_width }}
                        @else
                            {{ $ID_width }}
                        @endif
                    </td>
                    <td colspan="2" class="no-line" style="text-align: center;">
                        @if ($qafinalinfo->insideoutside=="OD")
                            {{ $OD_height }}
                        @else
                            {{ $ID_height }}
                        @endif
                    </td>
                </tr>
                <?php $num = 1; ?>
                @foreach ($qafinaldimension as $val)
                <tr>
                    <td>{{ $num }}</td>
                    <?php $num += 1; ?>
                    <td colspan="1" style="text-align: center;">
                        @if (fmod($val->length, 1) !== 0.00)
                            {{ $val->length }} 
                        @else
                            {{ floor($val->length) }}
                        @endif
                    </td>
                    <td colspan="2" style="text-align: center;">
                        @if (fmod($val->width, 1) !== 0.00)
                            {{ $val->width }}
                        @else
                            {{ floor($val->width) }}
                        @endif
                    </td>
                    <td colspan="2" style="text-align: center;">
                        @if (fmod($val->width, 1) !== 0.00)
                            {{ $val->height }}
                        @else
                            {{ floor($val->height) }}
                        @endif
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2">Flute Type: {{ $qafinalinfo->flute }}</td>
                    <td colspan="2">Thickness: {{ $qafinalinfo->thickness }}</td>
                    <td colspan="2">Tolerance: 
                        @if (fmod($qafinalinfo->tolerance, 1) !== 0.00)
                            {{ $qafinalinfo->tolerance }}
                        @else
                            {{ floor($qafinalinfo->tolerance) }}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center;">Final Judgement</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center; vertical-align: middle;">Passed <i class="fa fa-square-o" aria-hidden="true" style="font-size: 12px; margin-left: 10px;"></i></td>
                    <td colspan="2" class="no-line-four" style="text-align: center;">{{ $qafinalinfo->issueds }}</td>
                    <td colspan="2" class="no-line-four" style="text-align: center;">{{ $qafinalinfo->confirms }}</td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center; vertical-align: middle;">Failed <i class="fa fa-square-o" aria-hidden="true" style="font-size: 10px; margin-left: 10px;"></i></td>
                    <td colspan="2" class="no-line" style="text-align: center;">Inspected By</td>
                    <td colspan="2" class="no-line" style="text-align: center;">Confirmed By</td>
                </tr>
            </table>
            <p style="font-size: 8px; float: right;"><strong>FO-QAD-O1-03</strong></p>

        </div>

    </page>


</body>
</html>

<script type="text/javascript">
    // window.onload = function() { window.print(); }
</script>

  
