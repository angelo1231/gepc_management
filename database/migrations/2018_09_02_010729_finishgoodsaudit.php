<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Finishgoodsaudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finishgoodsaudit', function (Blueprint $table) {
            $table->increments('auditid');
            $table->integer('fgid');
            $table->text('jonumber')->nullable();
            $table->text('drnumber')->nullable();
            $table->integer('qty');
            $table->integer('balance');
            $table->text('issuedby')->nullable();
            $table->longtext('remarks')->nullable();
            $table->date('issueddate');
            $table->time('issuedtime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finishgoodsaudit');
    }
}
