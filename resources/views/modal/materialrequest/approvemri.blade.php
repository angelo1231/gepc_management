<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modalapprovemri" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label id="lblapproveinfo">Material Request Information</label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

                <table class="table table-bordered" style="width: 100%;" id="tblapprovemr">
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">Issuance Number</th>
                            <td colspan="7"><input id="txtaissuancenumber" name="txtaissuancenumber" type="text" class="form-control" placeholder="Issuance Number" readonly></td>    
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle;">Raw Material</th>
                            <th style="text-align: center; vertical-align: middle;">Quantity</th>
                            <th style="text-align: center; vertical-align: middle;">Remarks</th>
                            <th style="text-align: center; vertical-align: middle;">Excess Board</th>
                            <th style="text-align: center; vertical-align: middle;">Qty Excess Board</th>
                            <th style="text-align: center; vertical-align: middle;">Delivery Number</th>
                            <th style="text-align: center; vertical-align: middle;">Qty Issued</th>
                            <th style="text-align: center; vertical-align: middle;">Balance</th>
                        </tr>
                        <tbody id="tblapprovemrcontent" style="border: 0;">
                        </tbody>
                </table>

                    {{-- <div class="form-group">
                            <label for="txtaissuancenumber">Issuance Number</label>
                            <input id="txtaissuancenumber" name="txtaissuancenumber" type="text" class="form-control" placeholder="Issuance Number" readonly>
                    </div>
                    <div class="form-group">
                            <label for="txtaqtyrequired">Quantity Required</label>
                            <input id="txtaqtyrequired" name="txtaqtyrequired" type="number" class="form-control" placeholder="Quantity Required" readonly>
                    </div>
                    <div class="form-group">
                            <label for="txtadrnumber">Delivery Number</label>
                            <input id="txtadrnumber" name="txtadrnumber" type="text" class="form-control" placeholder="Delivery Number">
                    </div>
                    <div class="form-group">
                            <label for="txtaqtyissued">Quantity Issued</label>
                            <input id="txtaqtyissued" name="txtaqtyissued" type="number" class="form-control" placeholder="Quantity Issued">
                    </div>
                    <div class="form-group">
                            <label for="txtabalance">Balance</label>
                            <input id="txtabalance" name="txtabalance" type="number" class="form-control" placeholder="Balance" readonly>
                    </div> --}}

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnaapprove" name="btnaapprove" type="button" class="btn btn-primary btn-block btn-flat">Approve</button>
                </div>
            </div>
        </div>
    </div>
</div>