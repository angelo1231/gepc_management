<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="autonewpurchaseorder" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>Purchase Order Information</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="txtcsupplier">Supplier</label>
                            <select id="cmbcsupplier" name="cmbcsupplier" class="form-control">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="txtcponumber">PO #</label>
                            <input type="text" name="txtcponumber" id="txtcponumber" class="form-control" placeholder="Purchase Order Number" readonly>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="txtcdeliverto">Deliver To</label>
                            <input type="text" name="txtcdeliverto" id="txtcdeliverto" class="form-control" placeholder="Delivery To">
                        </div>

                        <div class="form-group">
                            <label for="txtcdeliverycharge">Delivery Charge</label>
                            <input type="number" name="txtcdeliverycharge" id="txtcdeliverycharge" class="form-control" placeholder="Delivery Charge" value="0">
                        </div>

                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtcattentionto">Attention To</label>
                            <input type="text" name="txtcattentionto" id="txtcattentionto" class="form-control" placeholder="Attn">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtcnote">Note</label>
                            <textarea id="txtcnote" name="txtcnote" type="text" rows="3" class="form-control" placeholder="Note"></textarea>
                        </div>
                    </div>

                    <div class="col-md-12">

                        <h4><strong>Items</strong></h4>
                        <table id="tblitems" class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width=20%;">Purchase Request #</th>
                                    <th style="width=20%;">Item Description</th>
                                    <th style="width=20%;">Required Quantity</th>
                                    <th style="width=20%;">Item Price</th>
                                    <th style="width=20%;">Paper Combination</th>
                                    {{-- <th style="width=17%;">Paper Combination Price</th> --}}
                                </tr>
                            </thead>
                            <tbody id="tblitemscontent">
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnccreatepo" name="btnccreatepo" type="button" class="btn btn-primary btn-flat btn-block">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>