@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Production Monitoring</b></h3>
        </div>
        <div class="box-body">

          <div class="col-md-12">
            
            <div class="col-md-6">
              <div class="form-group">
                <label for="txtsearch">Search</label>
                <input type="text" name="txtsearch" id="txtsearch" class="form-control" placeholder="Job Order Number">
              </div>
            </div>
            
          </div>

          <div class="col-md-12">

            @foreach($process as $proc)

              <div class="col-md-12">

                <h4><strong>{{ $proc->process }}</strong></h4>
                <table id="{{ $proc->dataprocess }}" class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Job Order #</th>
                      <th>Item Description</th>
                      <th>Target Output</th>
                      <th>Balance</th>
                    </tr>
                  </thead>
                  <tbody id="content{{ $proc->dataprocess }}">
                    {{-- Content Here --}}
                  </tbody>
                </table>

              </div>

            @endforeach

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modal --}}
  @include('modal.production.productionprocess')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var dataprocess = new Array();
    var spid;
    var smid;

    $(document).ready(function(){

      LoadProcess();

    });

    $('#txtsearch').on('keyup', function(){

      var search = $(this).val();
      LoadProcessJO(search);

    });

    $(document).on('click', '#btnoperation', function(){

      spid = $(this).data("id");
      smid = $(this).val();

      ClearOperationForm();
      GetMonitoringInformation(spid, smid);
      LoadRejectType();
      $('#productionprocess').modal('toggle');

    });

    $('#btnsave').on('click', function(){

      $.confirm({
          title: 'Save',
          content: 'Save this information?',
          type: 'blue',
          buttons: {   
              ok: {
                  text: "Yes",
                  btnClass: 'btn-info',
                  keys: ['enter'],
                  action: function(){

                    Save();

                  }
              },
              cancel: {
                  text: "No",
                  btnClass: 'btn-info',
                  action: function(){
                      
                    

                  }
              } 
          }
      });

    });

    function Save(){

      var qty = $('#txtqty').val();
      var rejectqty = $('#txtrejectqty').val();
      var rejecttype = $('#cmbrejecttype').val();

      if(qty==""){
        toastr.error('Please input the quantity', '', { positionClass: 'toast-top-center' });
      }
      else if(rejectqty==""){
        toastr.error('Please input the reject quantity', '', { positionClass: 'toast-top-center' });
      }
      else if(rejecttype==""){
        toastr.error('Please select a reject type', '', { positionClass: 'toast-top-center' });
      }
      else{

        $.ajax({
          url: '{{ url("api/productionmonitoring/saveoperationinformation") }}',
          type: 'post',
          data: {
            pid: spid,
            mid: smid,
            qty: qty,
            rejectqty: rejectqty,
            rejecttype: rejecttype
          },
          dataType: 'json',
          success: function(response){

            if(response.success){

              toastr.success(response.message, '', { positionClass: 'toast-top-center' });
              $('#productionprocess .close').click();
              LoadProcessJO('');

            }
            else{

              toastr.error(response.message, '', { positionClass: 'toast-top-center' });

            }

          }
        });

      }

    }

    function GetMonitoringInformation(pid, mid){

      $.ajax({
        url: '{{ url("api/productionmonitoring/getmonitoringinformation") }}',
        type: 'get',
        data: {
          pid: pid,
          mid: mid
        },
        dataType: 'json',
        success: function(response){

          $('#pinfo').text('');
          $('#pinfo').text('JO#: ' + response.jonumber + ' Process: ' + response.process);

        }
      });

    }

    function LoadProcess(){

      $.ajax({
        url: '{{ url("api/productionmonitoring/loadprocess") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          dataprocess = response.data;
          LoadProcessJO('');

        }
      });

    }

    function LoadProcessJO(search){

      for(var i=0;i<dataprocess.length;i++){

        GetProcessJO(dataprocess[i]["pid"], dataprocess[i]["dataprocess"], search);

      }

    }

    function GetProcessJO(pid, process, search){

      $.ajax({
        url: '{{ url("api/productionmonitoring/getprocessjo") }}',
        type: 'get',
        data: {
            pid: pid,
            search: search
        },
        dataType: 'json',
        success: function(response){

          $('#content'+process).find('tr').remove();

          if(response.data.length==0){

            $('#content'+process).append('<tr><td style="text-align: center;" colspan="4"><strong>No Data</strong></td></tr>');

          }
          else{

              for(var i=0;i<response.data.length;i++){

                if(response.data[i]["forcoc"]!=0){

                    $('#content'+process).append('<tr><td><button disabled id="btnoperation" name="btnoperation" class="btn btn-link" value="'+ response.data[i]["mid"] +'" data-id="'+ response.data[i]["pid"] +'"><strong>'+response.data[i]["jonumber"]+'</strong></button></td><td><strong>'+response.data[i]["description"]+'</strong></td><td><strong>'+response.data[i]["targetoutput"]+'</strong></td><td><strong>'+response.data[i]["balance"]+'</strong></td></tr>');

                }
                else{

                    $('#content'+process).append('<tr><td><button id="btnoperation" name="btnoperation" class="btn btn-link" value="'+ response.data[i]["mid"] +'" data-id="'+ response.data[i]["pid"] +'"><strong>'+response.data[i]["jonumber"]+'</strong></button></td><td><strong>'+response.data[i]["description"]+'</strong></td><td><strong>'+response.data[i]["targetoutput"]+'</strong></td><td><strong>'+response.data[i]["balance"]+'</strong></td></tr>');

                }
                
              }

          }

        }
      });

    }

    function LoadRejectType(){

      $.ajax({
        url: '{{ url("api/productionmonitoring/loadrejecttype") }}',
        type: 'get',
        dataType: 'json',
        success: function(response) {
          
          $('#cmbrejecttype').find('option').remove();
          $('#cmbrejecttype').append(response.content);

        },
        complete: function(){

          $('#cmbrejecttype').select2({
            theme: 'bootstrap'
          });

        }
      });

    }

    function ClearOperationForm(){

      $('#txtqty').val(0);
      $('#txtrejectqty').val(0);

    }

    //Socket functions
    socket.on('reloadjomonitor', function(){

      LoadProcessJO('');

    });

  </script>  

@endsection