<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductionMonitoring_Model extends Model
{
    
    public static function LoadProcess(){

        $result = DB::table('process')
        ->select(
            'pid',
            'process',
            DB::raw("REPLACE(process, ' ', '') AS 'dataprocess'")
        )
        ->get();

        return $result;

    }

    public static function GetProcessJO($data){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'jobordermonitoring.mid',
            'joborders.jonumber',
            'finishgoods.description',
            'jobordermonitoringaudit.targetoutput',
            // DB::raw("
            //     IFNULL((SELECT (jobordermonitoringaudit.targetoutput - SUM(qty) - SUM(rejectqty)) FROM jobordermonitoringaudititems WHERE auditid=jobordermonitoringaudit.auditid GROUP BY auditid), joborders.targetoutput) AS 'balance'
            // "),
            'jobordermonitoringaudit.balance',
            'jobordermonitoringaudit.pid',
            'process.forcoc'
        )
        ->join('jobordermonitoring', 'jobordermonitoring.mid', '=', 'jobordermonitoringaudit.mid')
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join('process', 'process.pid', '=', 'jobordermonitoringaudit.pid')
        ->where('jobordermonitoringaudit.pid', '=', $data->pid)
        ->where('jobordermonitoringaudit.isshow', '=', 1)
        // ->whereRaw("IFNULL((SELECT (jobordermonitoringaudit.targetoutput - SUM(qty) - SUM(rejectqty)) FROM jobordermonitoringaudititems WHERE auditid=jobordermonitoringaudit.auditid GROUP BY auditid), joborders.targetoutput) > 0")
        ->whereRaw("jobordermonitoringaudit.balance > 0")
        ->get();

        return $result;

    }

    public static function GetProcessJOSearch($data){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'joborders.jonumber',
            'finishgoods.description',
            'jobordermonitoringaudit.targetoutput',
            DB::raw("
                IFNULL((SELECT (SUM(qty) + SUM(rejectqty)) FROM jobordermonitoringaudititems WHERE auditid=jobordermonitoringaudit.auditid GROUP BY auditid), '0') AS 'balance'
            ")
        )
        ->join('jobordermonitoring', 'jobordermonitoring.mid', '=', 'jobordermonitoringaudit.mid')
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('jobordermonitoringaudit.pid', '=', $data->pid)
        ->where('jobordermonitoringaudit.isshow', '=', 1)
        ->whereRaw("joborders.jonumber LIKE '%". $data->search ."%'")
        ->get();

        return $result;

    }

    public static function GetMonitoringInformation($mid){

        $result = DB::table('jobordermonitoring')
        ->select(
            'joborders.jonumber',
            'process.process'
        )
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->join('process', 'process.pid', '=', 'jobordermonitoring.currentpid')
        ->where('jobordermonitoring.mid', '=', $mid)
        ->first();

        return $result;

    }

    public static function GetAuditID($mid, $pid){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'auditid'
        )
        ->where('mid', '=', $mid)
        ->where('pid', '=', $pid)
        ->first();

        return $result->auditid;

    }

    public static function SaveOperationInformation($auditid, $issuedby, $qty, $rejectqty, $rejectype){

        $result = DB::table('jobordermonitoringaudititems')
        ->insertGetId([
            'auditid'=>$auditid,
            'operator'=>$issuedby,
            'qty'=>$qty,
            'rejectqty'=>$rejectqty,
            'rejectid'=>$rejectype,
            'created_at'=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function GetJobOrderFGIDProcess($mid){

        $result = DB::table('jobordermonitoring')
        ->select(
            'finishgoods.processcode'
        )
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('jobordermonitoring.mid', '=', $mid)
        ->first();

        return $result->processcode;

    }

    public static function GetProcess($processcode){

        $result = DB::table('processcode')
        ->select(
            'processorder',
            'processid'
        )
        ->where('processcode', '=', $processcode)
        ->orderBy('processorder')
        ->get();

        return $result;

    }

    public static function UpdateCurrentProcess($mid, $processid){

        DB::table('jobordermonitoring')
        ->where('mid', '=', $mid)
        ->update([
            'currentpid'=>$processid,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function UpdateMonitoringAudit($mid, $processid, $balance){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            DB::raw("(targetoutput + ". $balance .") AS 'targetoutput'"),
            DB::raw("(balance + ". $balance .") AS 'balance'")
        )
        ->where('mid', '=', $mid)
        ->where('pid', '=', $processid)
        ->first();

        DB::table('jobordermonitoringaudit')
        ->where('mid', '=', $mid)
        ->where('pid', '=', $processid)
        ->update([
            'targetoutput'=>$result->targetoutput,
            'balance'=>$result->balance,
            'isshow'=>'1',
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function UpdateProcessTargetOutputBalance($mid, $pid, $balance){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            DB::raw("(targetoutput - ". $balance .") AS 'targetoutput'"),
            DB::raw("(balance - ". $balance .") AS 'balance'")
        )
        ->where('mid', '=', $mid)
        ->where('pid', '=', $pid)
        ->first();

        DB::table('jobordermonitoringaudit')
        ->where('mid', '=', $mid)
        ->where('pid', '=', $pid)
        ->update([
            'targetoutput'=>$result->targetoutput,
            'balance'=>$result->balance,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateBalance($mid, $pid, $balance){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'balance'
        )
        ->where('mid', '=', $mid)
        ->where('pid', '=', $pid)
        ->first();

        if($result->balance<$balance){
            
            return true;

        }   
        else{
           
            return false;

        }

    }

    public static function LoadRejectType(){

        $result = DB::table('qarejecttype')
        ->select(
            'id',
            'reject'
        )
        ->get();

        return $result;

    }

}
