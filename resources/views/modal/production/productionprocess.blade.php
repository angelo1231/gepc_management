<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="productionprocess" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong id="pinfo"></strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txtqty">Quantity</label>
                            <input id="txtqty" name="txtqty" class="form-control" placeholder="Quantity" type="number">
                        </div>
    
                        <div class="form-group">
                            <label for="txtrejectqty">Reject Quantity</label>
                            <input id="txtrejectqty" name="txtrejectqty" class="form-control" placeholder="Reject Quantity" type="number">
                        </div>

                        <div class="form-group">
                          <label for="cmbrejecttype">Reject Type</label>
                          <select name="cmbrejecttype" id="cmbrejecttype" class="form-control">
                          </select>
                        </div>
                            
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                        <div class="col-md-12">
                            <button id="btnsave" name="btnsave" type="button" class="btn btn-success btn-flat">Save</button>
                            <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Close</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>