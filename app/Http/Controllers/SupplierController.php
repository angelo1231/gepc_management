<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Seassion;
use App\Supplier_Model;
use Illuminate\Support\Collection;
use DataTables;
use App\Http\Controllers\UserController;

class SupplierController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    
	function index(){

        $access = UserController::UserGroupAccess();

		return view('suppliers')
        ->with('access', $access);

	}

	function SupplierInformation(){

        $suppliers = Supplier_Model::SupplierInformation();
        $data = array();
        for($i=0;$i<count($suppliers);$i++){
			$obj = new \stdClass;
            $obj->sid = $suppliers[$i]->sid;			
			$obj->supplier = $suppliers[$i]->supplier;
			$obj->contactperson = $suppliers[$i]->contactperson;
			$obj->contactnumber = $suppliers[$i]->contactnumber;
			$obj->email = $suppliers[$i]->email;
            $obj->address = $suppliers[$i]->address;
            $obj->tin = $suppliers[$i]->tin;
			$obj->terms = $suppliers[$i]->terms;
			$obj->tax = $suppliers[$i]->taxclassification;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $suppliers[$i]->sid .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

	}

	function NewSupplier(Request $request){

		$supplier_count = Supplier_Model::ValidateSupplier($request->supplier);

		if($supplier_count == 0){

			Supplier_Model::NewSupplier($request);

			return json_encode(["success"=>true, "message"=>"Supplier has been added."]);

		}
		else{

			return json_encode(["success"=>false, "message"=>"Supplier already exist."]);

		}


	}

	function SupplierProfile(Request $request){

		$sid = $request->sid;

		$data = Supplier_Model::SupplierProfile($sid);

		return json_encode([
			"supplier"=>$data[0]->supplier,
			"contactperson"=>$data[0]->contactperson,
			"contactnumber"=>$data[0]->contactnumber,
			"email"=>$data[0]->email, 
			"address"=>$data[0]->address, 
			"tin"=>$data[0]->tin, 
			"terms"=>$data[0]->terms,
			"tax"=>$data[0]->taxclassification
		]);

	}

	function UpdateSupplier(Request $request){

		Supplier_Model::UpdateSupplier($request);

		return json_encode(["message"=>"Supplier information has been update."]);

	}

}
