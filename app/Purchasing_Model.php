<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Purchasing_Model extends Model
{
    
    public static function LoadSupplier(){

        $result = DB::table('suppliers')
        ->select('sid', 'supplier')
        ->get();

        return $result;

    }

    public static function SupplierProfile($sid){

        $result = DB::table('suppliers')
        ->select(
            'supplier',
            'contactperson',
            'contactnumber',
            'email',
            'address',
            'tin',
            'terms'
        )
        ->where('sid', '=', $sid)
        ->get();

        return $result;

    }

    public static function LoadItem($sid){

        $result = DB::table('rawmaterials')
        ->select(
            'rmid',
            'name',
            'description',
            'width',
            'widthsize',
            'length',
            'lengthsize',
            'flute',
            'boardpound',
            'description',
            'price',
            'category'
        )
        ->where('category', '!=', 'Excess Board')
        ->get();

        return $result;

    }

    public static function RawMaterialProfile($rmid){

        $result = DB::table('rawmaterials')
        ->select(
            'description',
            'flute',
            'boardpound',
            'description',
            'price'
        )
        ->where('rmid', '=', $rmid)
        ->get();

        return $result;

    }

    public static function GenPONumber($tableschema){

        $result = DB::select(DB::raw("
            SELECT CONCAT('PO', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) AS 'batch' FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'poinformation'
        "));

        return $result[0]->batch;

    }

    public static function Padding($batch){

        $result = DB::select(DB::raw('SELECT LPAD('.$batch.', 4, "0") AS pad'));
        
        return $result[0]->pad;

    }

    public static function Currency($rmid){

        $result = DB::table('rawmaterials')
        ->select(
            'rawmaterials.price',
            'rawmaterials.currencyid',
            'currency.rate'
        )
        ->leftjoin('currency', 'currency.currencyid', '=', 'rawmaterials.currencyid')
        ->where('rawmaterials.rmid', '=', $rmid)
        ->get();

        return $result;

    }

    public static function SavePOInformation($grandtotal, $sid, $issuedby, $attn, $delto, $delcharge, $tableschema, $userid, $note){

        $result = DB::table('poinformation')
        ->insertGetId([
            "ponumber"=>DB::raw("(SELECT CONCAT('PO', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'poinformation')"),
            "attentionto"=>$attn,
            "deliveryto"=>$delto,  
            "grandtotal"=>$grandtotal,
            "deliverycharge"=>$delcharge,
            "sid"=>$sid,
            "remarks"=>"Open",
            "iuid"=>$userid,
            "issuedby"=>$issuedby,
            "created_at"=>DB::raw("NOW()"),
            "note"=>$note
        ]);

        return $result;
        
    }

    public static function SavePOInformationItems($poid, $rmid, $currencyid, $rate, $price, $qty, $total, $papercomid, $pritemid){

        $result = DB::table('poinformationitems')
        ->insertGetId([
            "poid"=>$poid,
            "pritemid"=>$pritemid,
            "rmid"=>$rmid,
            "currencyid"=>$currencyid,
            "rate"=>$rate,
            "qty"=>$qty,
            "price"=>$price,
            "total"=>$total,
            "delqty"=>0,
            "papercomid"=>$papercomid
        ]);

        return $result;

    }

    public static function LoadPOInformation($supplier){

        if($supplier=="All"){

            $result = DB::table('poinformation')
            ->select(
                'poinformation.poid',
                'poinformation.ponumber',
                'poinformation.attentionto',
                DB::raw("(SELECT SUM(poinformationitems.qty) AS 'sumqty' FROM poinformationitems WHERE poinformation.poid=poinformationitems.poid) AS 'totalqty'"),
                'poinformation.grandtotal',
                'poinformation.issuedby',
                'poinformation.created_at',
                'suppliers.supplier',
                'poinformation.remarks',
                'poinformation.approvedby',
                'poinformation.approveddate'
            )
            ->leftjoin('suppliers', 'suppliers.sid', '=', 'poinformation.sid')
            ->orderBy('poinformation.poid', 'DESC')
            ->get();

            return $result;

        }
        else{

            $result = DB::table('poinformation')
            ->select(
                'poinformation.poid',
                'poinformation.ponumber',
                'poinformation.attentionto',
                DB::raw("(SELECT SUM(poinformationitems.qty) AS 'sumqty' FROM poinformationitems WHERE poinformation.poid=poinformationitems.poid) AS 'totalqty'"),
                'poinformation.grandtotal',
                'poinformation.issuedby',
                'poinformation.created_at',
                'suppliers.supplier',
                'poinformation.remarks',
                'poinformation.approvedby',
                'poinformation.approveddate'
            )
            ->leftjoin('suppliers', 'suppliers.sid', '=', 'poinformation.sid')
            ->where('poinformation.sid', '=', $supplier)
            ->orderBy('poinformation.poid', 'DESC')
            ->get();

            return $result;

        }

    }

    public static function POInfo($poid){

        $result = DB::table('poinformation')
        ->select(
            'poinformation.poid',
            'poinformation.ponumber',
            'poinformation.attentionto',
            'poinformation.deliveryto',
            DB::raw("(SELECT SUM(poinformationitems.qty) AS 'sumqty' FROM poinformationitems WHERE poinformation.poid=poinformationitems.poid) AS 'totalqty'"),
            'poinformation.deliverycharge',
            'poinformation.grandtotal',
            'poinformation.created_at',
            'poinformation.issuedby',
            'poinformation.approvedby',
            'suppliers.supplier',
            'suppliers.address',
            'suppliers.tin',
            'suppliers.terms',
            DB::raw("iesign.esign AS 'issuedesign'"),
            DB::raw("aesign.esign AS 'approvedesign'"),
            DB::raw("IF(approvedby IS NULL, 'False', 'True') AS 'hasapproved'")
        )
        ->leftjoin('suppliers', 'suppliers.sid', '=', 'poinformation.sid')
        ->join('users as iesign', 'iesign.id', '=', 'poinformation.iuid')
        ->leftjoin('users as aesign', 'aesign.id', '=', 'poinformation.auid')
        ->where('poinformation.poid', '=', $poid)
        ->get();

        return $result;        

    }

    public static function POInfoitems($poid){

        $result = DB::table('poinformationitems')
        ->select(
            'poinformationitems.itemid',
            'rawmaterials.name',
            'rawmaterials.description',
            'rawmaterials.width',
            'rawmaterials.widthsize',
            'rawmaterials.length',
            'rawmaterials.lengthsize',
            'rawmaterials.flute',
            'rawmaterials.boardpound',
            'currency.currency',
            'poinformationitems.rate',
            'poinformationitems.qty',
            'poinformationitems.price',
            'poinformationitems.total',
            'poinformationitems.delqty',
            'rawmaterials.category',
            DB::raw("IFNULL(papercombination.rmpapercombination, '') AS 'rmpapercombination'"),
            DB::raw("
                (SELECT GROUP_CONCAT(CONCAT('Delivery: ',deliverydate, ' Qty: ',deliveryqty)) FROM poinformationitemsdelivery WHERE itemid=poinformationitems.itemid) AS 'deliveryinfo'
            "),
            DB::raw("(poinformationitems.qty - poinformationitems.delqty) AS 'balance'"),
            DB::raw("SUM(poinformationitemsaudit.rejectqty) AS 'totalreject'"),
            DB::raw("SUM(poinformationitemsaudit.deliveryqty) AS 'totaldelqty'")
        )
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'poinformationitems.rmid')
        ->join('currency', 'currency.currencyid', '=', 'poinformationitems.currencyid')
        ->leftjoin('poinformationitemsaudit', 'poinformationitemsaudit.itemid', '=', 'poinformationitems.itemid')
        ->leftjoin('papercombination', 'papercombination.papercomid', '=', 'poinformationitems.papercomid')
        ->where('poinformationitems.poid', '=', $poid)
        ->groupBy('poinformationitems.itemid')
        ->get();

        return $result;

    }

    public static function GetSupplierName($data){

        $result = DB::table('suppliers')
        ->select(
            'supplier'
        )
        ->where('sid', '=', $data->sid)
        ->first();

        return $result->supplier;

    }

    public static function UpdatePurchaseRequestInfoPurchasing($prid){

        DB::table('purchaserequest')
        ->whereIn('prid', $prid)
        ->update([
            "status"=>"Done",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadCountPR(){

        $result = DB::table('purchaserequest')
        ->select(
            DB::raw("COUNT(*) AS 'prcount'")
        )
        ->where('status', '=', 'Pending')
        ->first();

        return $result->prcount;

    }

    public static function ApprovePO($poid, $approvedby, $userid){

        DB::table('poinformation')
        ->where('poid', '=', $poid)
        ->update([
            "auid"=>$userid,
            "approvedby"=>$approvedby,
            "approveddate"=>DB::raw("NOW()")
        ]);

    }

    public static function GetSupplierEmail($poid){

        $result = DB::table('poinformation')
        ->select(
           'suppliers.email' 
        )
        ->join('suppliers', 'suppliers.sid', '=', 'poinformation.sid')
        ->where('poinformation.poid', '=', $poid)
        ->first();

        return $result->email;

    }

    public static function LoadSelectedItem($prid){

        $result = DB::table('purchaserequestitems')
        ->select(
            'purchaserequestitems.itemid',
            'purchaserequest.prid',
            'purchaserequest.prnumber',
            DB::raw("IF(rawmaterials.category!='Raw Material', CONCAT(rawmaterials.name, ' ', rawmaterials.description), CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound)) AS 'description'"),
            'rawmaterials.price',
            'purchaserequestitems.qtyrequired',
            DB::raw("DATE_FORMAT(purchaserequest.created_at, '%Y-%m-%d') AS 'issueddate'"),
            DB::raw("DATE_FORMAT(purchaserequest.created_at, '%h:%i %p') AS 'issuedtime'"),
            'purchaserequest.status',
            'rawmaterials.flute',
            'rawmaterials.boardpound'
        )
        ->join('purchaserequest', 'purchaserequest.prid', '=', 'purchaserequestitems.prid')
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'purchaserequestitems.rmid')
        ->where('purchaserequestitems.prid', '=', $prid)
        ->get();

        return $result;

    }

    public static function GetPRItemTotal($pritemid, $price){

        $result = DB::table('purchaserequestitems')
        ->select(
            'currency.rate',
            'purchaserequestitems.qtyrequired',
            DB::raw("((currency.rate * ". $price .") * purchaserequestitems.qtyrequired) AS 'total'")
        )
        ->where('purchaserequestitems.itemid', '=', $pritemid)
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'purchaserequestitems.rmid')
        ->join('currency', 'currency.currencyid', '=', 'rawmaterials.currencyid')
        ->first();

        return $result;

    }

    public static function GetPRItemData($pritemid, $price){

        $result = DB::table('purchaserequestitems')
        ->select(
            'purchaserequestitems.prid',
            'purchaserequestitems.rmid',
            'currency.currencyid',
            'currency.rate',
            'rawmaterials.price',
            DB::raw("((currency.rate * ". $price .") * purchaserequestitems.qtyrequired) AS 'total'"),
            'purchaserequestitems.qtyrequired'
        )
        ->where('purchaserequestitems.itemid', '=', $pritemid)
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'purchaserequestitems.rmid')
        ->join('currency', 'currency.currencyid', '=', 'rawmaterials.currencyid')
        ->first();
    
        return $result;

    }

    public static function UpdateRMPrice($rmid, $price){

        DB::table('rawmaterials')
        ->where('rmid', '=', $rmid)
        ->update([
            "price"=>$price,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function UpdatePriceRM($rmid, $price){

        DB::table('rawmaterials')
        ->where('rmid', '=', $rmid)
        ->update([
            "price"=>$price,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function SavePaperComPrice($papercomid, $papercomprice, $sid){

        $validationquery = DB::table('papercombinationprice')
        ->select(
            'id',
            DB::raw("COUNT(*) AS 'papercompricecount'")
        )
        ->where('sid', '=', $sid)
        ->where('papercomid', '=', $papercomid)
        ->whereNull('deleted_at')
        ->first();

        if($validationquery->papercompricecount!=0){

            //Update
            DB::table('papercombinationprice')
            ->where('id', '=', $validationquery->id)
            ->update([
                "deleted_at"=>DB::raw("NOW()")
            ]);

            //Insert
            $result = DB::table('papercombinationprice')
            ->insertGetId([
                "papercomid"=>$papercomid,
                "sid"=>$sid,
                "price"=>$papercomprice,
                "created_at"=>DB::raw("NOW()")
            ]);

            return $result;

        }
        else{

            //Insert First Record
            $result = DB::table('papercombinationprice')
            ->insertGetId([
                "papercomid"=>$papercomid,
                "sid"=>$sid,
                "price"=>$papercomprice,
                "created_at"=>DB::raw("NOW()")
            ]);

            return $result;

        }

    }

    public static function GetItemInformation($itemid){

        $result = DB::table('poinformationitems')
        ->select(
            'price',
            'qty'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        return $result;

    }

    public static function GetItemInfoRate($itemid){

        $result = DB::table('poinformationitems')
        ->select(
            'rmid',
            'rate'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        return $result;

    }

    public static function UpdateItemInformation($itemid, $price, $qty, $rate){

        DB::table('poinformationitems')
        ->where('itemid', '=', $itemid)
        ->update([
            "price"=>$price,
            "qty"=>$qty,
            "total"=>($rate * $price) * $qty
        ]);

    }

    public static function UpdatePOGrandTotal($poid){

        $grandtotal = DB::table('poinformationitems')
        ->select(
            DB::raw("SUM(total) AS 'grandtotal'")
        )
        ->where('poid', '=', $poid)
        ->first();

        DB::table('poinformation')
        ->where('poid', '=', $poid)
        ->update([
            "grandtotal"=>$grandtotal->grandtotal
        ]);

        return $grandtotal->grandtotal;

    }

    public static function SavePRItemDeliveryInfo($poitemid, $pritemid){

        DB::select(DB::raw("
            INSERT INTO poinformationitemsdelivery (itemid, deliverydate, deliveryqty, created_at) SELECT '". $poitemid ."', deliverydate, deliveryqty, NOW() FROM purchaserequestitemsdelivery WHERE itemid='". $pritemid ."'
        "));

    }

    public static function GetPOInfoStatus($poid){

        $result = DB::table('poinformation')
        ->select(
            DB::raw("IF(approvedby IS NULL, 'False', 'True') AS 'hasapproved'")
        )
        ->where('poinformation.poid', '=', $poid)
        ->first();

        return $result;

    }

    public static function GetItemDeliveryDates($poid){

        $result = DB::table('poinformationitems')
        ->select(
            DB::raw("GROUP_CONCAT(DISTINCT poinformationitemsdelivery.deliverydate SEPARATOR '<br>') AS 'deliverydate'")
        )
        ->join('poinformationitemsdelivery', 'poinformationitemsdelivery.itemid', '=', 'poinformationitems.itemid')
        ->where('poinformationitems.poid', '=', $poid)
        ->first();

        return $result->deliverydate;

    }

    public static function PODeliveryDates($poid){

        $result = DB::table('poinformationitems')
        ->select(
            '*'
        )
        ->join('poinformationitemsdelivery', 'poinformationitemsdelivery.itemid', '=', 'poinformationitems.itemid')
        ->where('poinformationitems.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function PODeliveryDatesInfo($poid){

        $result = DB::table('poinformationitems')
        ->select(
            DB::raw("GROUP_CONCAT(DISTINCT poinformationitemsdelivery.deliverydate SEPARATOR ', ') AS 'deliverydate'")
        )
        ->join('poinformationitemsdelivery', 'poinformationitemsdelivery.itemid', '=', 'poinformationitems.itemid')
        ->where('poinformationitems.poid', '=', $poid)
        ->first();

        return $result->deliverydate;

    }
    
}
