@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> User Account Information</b></h3>
        </div>
        <div class="box-body">
            <div class="col-md-9">
            </div>
            <div class="col-md-3" style="margin-bottom: 15px;">

                {{-- <button id="btnnewuser" name="btnnewuser" class="btn btn-success btn-block btn-flat" data-toggle="modal" data-target="#modalregister"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New User</strong></button> --}}
                
                <button id="btnnewuser" name="btnnewuser" class="btn btn-success btn-flat" style="float: right;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New User</strong></button>

                <button id="btnusergroup" name="btnusergroup" class="btn btn-primary btn-flat" style="float: right; margin-right: 10px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>User Group</strong></button>


            </div>      
            <div class="col-md-12">
            <table id="tblusers" class="table">
                <thead>
                    <tr>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>MI</th>
                        <th>Username</th>
                        <th>Usertype</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')
  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

  {{-- Modals --}}
  {{-- @include('modal.user.register') --}}
  {{-- @include('modal.user.update') --}}
  @include('modal.user.changepassword')

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblusers;

      $(document).ready(function() {

          //Session Message
          var msg = "{{ Session::get('message') }}";
          if(msg!=""){
            toastr.success(msg, '', { positionClass: 'toast-top-center' });
          }

          //Set
          tblusers = $('#tblusers').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: '{{ url("api/user/userinformation") }}',
            columns : [
                {data: 'firstname', name: 'firstname'},
                {data: 'lastname', name: 'lastname'},
                {data: 'mi', name: 'mi'},
                {data: 'username', name: 'username'},
                {data: 'usertype', name: 'usertype'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
          });
          
          //Load
          LoadSelect2();

      });

      $(document).on('click', '#btnedit', function () { //Button In Datatable
        
        @if(in_array(4, $access))

          var id = $(this).attr("value");
          window.location = '{{ url("user/update/updateuser") }}/' + id;

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif


      });

      $(document).on('click', '#btnchangepassword', function () { //Button In Datatable
        
        @if(in_array(5, $access))

          var id = $(this).attr("value");
          //Set
          ClearChangePassword();
          $('#btnchange').val(id);
          $('#modalchange').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif


      });

      $(document).on('click', '#btnreset', function(){

        @if(in_array(6, $access))

          var id = $(this).attr("value");

          $.confirm({
              title: 'Reset',
              content: 'Reset this account password?',
              type: 'red',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-danger',
                      keys: ['enter'],
                      action: function(){
                        
                         
                         $.ajax({
                            url: '{{ url("api/user/resetuser") }}',
                            type: 'post',
                            dataType: 'json',
                            data: {id: id},
                            success: function(response){

                                if(response.success){
                                  toastr.success("Password has been reset", '', { positionClass: 'toast-top-center' });
                                  download(response.filename, response.password);
                                }
                                else{
                                  toastr.error("Ooops something went wrong.. Please contact your system administrator.", '', { positionClass: 'toast-top-center' });
                                }
                               
                            }
                        });


                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          //Do Nothing
                      }
                  } 
              }
          });

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnchange').on('click', function(){

         var id =  $(this).attr("value");
         var oldpassword = $('#txtoldpassword').val();
         var newpassword = $('#txtnewpassword').val();
         var confirmpassword = $('#txtconfirmpassword').val();

        if(oldpassword==""){

          toastr.error('Please input old password.', '', { positionClass: 'toast-top-center' });

        }
        else if(newpassword==""){

          toastr.error('Please input the new password.', '', { positionClass: 'toast-top-center' });

        }
        else if(confirmpassword==""){

          toastr.error('Please confirm the password.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({

              url: '{{ url("api/user/changepassword") }}',
              type: 'post',
              data: {id: id, oldpassword: oldpassword, newpassword: newpassword, confirmpassword: confirmpassword},
              dataType: 'json',
              success: function(response){

                if(response.success){

                    $("#modalchange .close").click();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }
                else{

                  if(response.type=="oldpassword"){
                    $('#txtoldpassword').val('');
                    $('#txtoldpassword').focus();
                  }
                  else if(response.type=="matchpassword"){
                    $('#txtconfirmpassword').val('');
                    $('#txtconfirmpassword').focus();
                  }
                  toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                }

              }

            });

        }



      });

      $('#btnnewuser').on('click', function(){

        @if(in_array(3, $access))

          window.location = "{{ url('/user/new/newuser') }}";

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnusergroup').on('click', function(){

        @if(in_array(7, $access))

          window.location = "{{ url('/usergroup') }}";

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      function ClearChangePassword(){

        $('#txtoldpassword').val('');
        $('#txtnewpassword').val('');
        $('#txtconfirmpassword').val('');

      }

      function download(filename, text) {

        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);

      }

      function LoadSelect2(){

        $('#cmbusertype').select2({
          theme: 'bootstrap'
        });

        $('#cmbuusertype').select2({
          theme: 'bootstrap'
        });

      }

      function ReloadUsers(){
        tblusers.ajax.reload();
      }

  </script>  

@endsection