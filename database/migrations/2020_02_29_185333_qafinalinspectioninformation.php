<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Qafinalinspectioninformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qafinalinspectioninformation', function (Blueprint $table) {
            $table->increments('qafiid');
            $table->string('finalinspectionnumber', 260); //OD - ID
            $table->integer('joid');
            $table->integer('mid');
            $table->integer('pid');
            $table->integer('auditid');
            $table->decimal('tolerance', 11,2);
            $table->integer('outputqty');
            $table->string('insideoutside', 60); //OD - ID
            $table->string('flute', 60);
            $table->string('thickness', 60);
            $table->text('confirmby');
            $table->datetime('confirmdate');
            $table->text('issuedby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qafinalinspectioninformation');
    }
}
