<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class QualityAssurance_Model extends Model
{

    public static function Padding($batch){

        $result = DB::select(DB::raw('SELECT LPAD('.$batch.', 4, "0") AS pad'));
        
        return $result[0]->pad;

    }

    public static function COCInformations($customer){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'jobordermonitoring.mid',
            'jobordermonitoring.joid',
            'joborders.jonumber',
            'finishgoods.description',
            'jobordermonitoringaudit.targetoutput',
            'jobordermonitoringaudit.balance',
            'jobordermonitoringaudit.pid',
            'process.forcoc'
        )
        ->join('jobordermonitoring', 'jobordermonitoring.mid', '=', 'jobordermonitoringaudit.mid')
        ->join('joborders', 'joborders.joid', '=', 'jobordermonitoring.joid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join('process', 'process.pid', '=', 'jobordermonitoringaudit.pid')
        ->where('jobordermonitoringaudit.isshow', '=', 1)
        ->where('process.forcoc', '=', 1)
        ->whereRaw("jobordermonitoringaudit.balance > 0")
        ->get();

        return $result;

    }

    public static function GetJobOrderInformation($joid){

        $result = DB::table('joborders')
        ->select(
            'joborders.jonumber',
            'finishgoods.partname',
            'finishgoods.partnumber',
            'finishgoods.description',
            'finishgoods.ID_length',
            'finishgoods.ID_width',
            'finishgoods.ID_height',
            'finishgoods.OD_length',
            'finishgoods.OD_width',
            'finishgoods.OD_height',
            'joborders.targetoutput',
            'flute.flute',
            'customers.customer'
        )
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join(DB::raw("(
            SELECT
            materialrequestitems.mrid,
            GROUP_CONCAT(rawmaterials.flute) AS 'flute'
            FROM materialrequestitems
            INNER JOIN rawmaterials ON rawmaterials.rmid=materialrequestitems.rmid
            GROUP BY materialrequestitems.mrid
        ) AS flute"), 'flute.mrid', '=', 'joborders.requestid')
        ->join('customers', 'customers.cid', '=', 'joborders.cid')
        ->where('joborders.joid', '=', $joid)
        ->first();

        return $result;

    }

    public static function LoadRejectType(){

        $result = DB::table('qarejecttype')
        ->select(
            'id',
            'rejectcode',
            'reject'
        )
        ->get();

        return $result;

    }

    public static function GetJOMonitoringInfo($mid, $pid){

        $result = DB::table('jobordermonitoringaudit')
        ->select(
            'targetoutput',
            'balance'
        )
        ->where('mid', '=', $mid)
        ->where('pid', '=', $pid)
        ->first();

        return $result;

    }
    
    public static function SaveFinalInspectionInformation($joid, $mid, $pid, $tolerance, $outputqty, $insideoutside, $flute, $thickness, $issuedby, $auditid, $tableschema){

        $result = DB::table('qafinalinspectioninformation')
        ->insertGetId([
            'finalinspectionnumber'=>DB::raw("(SELECT CONCAT('FI', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'qafinalinspectioninformation')"),
            'joid'=>$joid,
            'mid'=>$mid,
            'pid'=>$pid,
            'auditid'=>$auditid,
            'tolerance'=>$tolerance,
            'outputqty'=>$outputqty,
            'insideoutside'=>$insideoutside,
            'flute'=>$flute,
            'thickness'=>$thickness,
            'issuedby'=>$issuedby,
            'created_at'=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function SaveFIMeasure($qafiid, $mt, $cal, $ms){

        DB::table('qafinalinspectionmeasuring')
        ->insert([
            'qafiid'=>$qafiid,
            'metertape'=>$mt,
            'caliper'=>$cal,
            'meterstick'=>$ms,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function SaveFIVisual($qafiid, $rejecttypeid){

        DB::table('qafinalinspectionvisual')
        ->insert([
            'qafiid'=>$qafiid,
            'rejecttypeid'=>$rejecttypeid,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function SaveFIOthers($qafiid, $others){

        DB::table('qafinalinspectionvisualothers')
        ->insert([
            'qafiid'=>$qafiid,
            'others'=>$others,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function SaveFIDimension($qafiid, $dimensionlength, $dimensionwidth, $dimensionheight, $remarks){

        DB::table('qafinalinspectiondimension')
        ->insert([
            'qafiid'=>$qafiid,
            'length'=>$dimensionlength,
            'width'=>$dimensionwidth,
            'height'=>$dimensionheight,
            'remarks'=>$remarks,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function LoadFinalInspectionInformation($from, $to){

        $result = DB::table('qafinalinspectioninformation')
        ->select(
            'qafinalinspectioninformation.qafiid',
            'qafinalinspectioninformation.finalinspectionnumber',
            'joborders.jonumber',
            'qafinalinspectioninformation.outputqty',
            'customers.customer',
            DB::raw("IFNULL(CONCAT(users.lastname, ', ', users.firstname), '') AS 'validateby'"),
            DB::raw("IFNULL(DATE_FORMAT(qafinalinspectioninformation.confirmdate, '%Y-%m-%d %r'), '') AS 'confirmdate'")
        )
        ->join('joborders', 'joborders.joid', '=', 'qafinalinspectioninformation.joid')
        ->join('customers', 'customers.cid', '=', 'joborders.cid')
        ->leftjoin('users', 'users.id', '=', 'qafinalinspectioninformation.confirmby')
        ->whereRaw("DATE_FORMAT(qafinalinspectioninformation.created_at, '%Y-%m-%d') BETWEEN '". $from ."' AND '". $to ."'")
        ->get();

        return $result;

    }

    public static function GetQAFinalInformation($qafiid){

        $result = DB::table('qafinalinspectioninformation')
        ->select(
            'qafinalinspectioninformation.joid',
            'qafinalinspectioninformation.finalinspectionnumber',
            'qafinalinspectioninformation.outputqty',
            'qafinalinspectioninformation.insideoutside',
            'qafinalinspectioninformation.flute',
            'qafinalinspectioninformation.thickness',
            'qafinalinspectioninformation.tolerance',
            'qafinalinspectioninformation.confirmby',
            DB::raw("DATE_FORMAT(qafinalinspectioninformation.created_at, '%Y-%m-%d') AS 'inspectiondate'"),
            DB::raw("CONCAT(issued.lastname, ', ', issued.firstname) AS 'issueds'"),
            DB::raw("CONCAT(confirm.lastname, ', ', confirm.firstname) AS 'confirms'")
        )
        ->join('users AS issued', 'issued.id', '=', 'qafinalinspectioninformation.issuedby')
        ->leftjoin('users AS confirm', 'confirm.id', '=', 'qafinalinspectioninformation.confirmby')
        ->where('qafinalinspectioninformation.qafiid', '=', $qafiid)
        ->first();

        return $result;

    }

    public static function GetQAFinalMeasure($qafiid){

        $result = DB::table('qafinalinspectionmeasuring')
        ->select(
            'metertape',
            'caliper',
            'meterstick'
        )
        ->where('qafiid', '=', $qafiid)
        ->first();

        return $result;

    }

    public static function GetQAFinalVisual($qafiid){

        $result = DB::table('qafinalinspectionvisual')
        ->select(
            'rejecttypeid'
        )
        ->where('qafiid', '=', $qafiid)
        ->get();

        return $result;

    }

    public static function GetQAFinalOthers($qafiid){

        $result = DB::table('qafinalinspectionvisualothers')
        ->select(
            'others'
        )
        ->where('qafiid', '=', $qafiid)
        ->first();

        return $result;

    }

    public static function GetQAFinalDimension($qafiid){

        $result = DB::table('qafinalinspectiondimension')
        ->select(
            'length',
            'width',
            'height',
            'remarks'
        )
        ->where('qafiid', '=', $qafiid)
        ->get();

        return $result;

    }

    public static function ConfirmFinalInspection($qafiid, $confirmby){

        DB::table('qafinalinspectioninformation')
        ->where('qafiid', '=', $qafiid)
        ->update([
            'confirmby'=>$confirmby,
            'confirmdate'=>DB::raw("NOW()")
        ]);

    }

    public static function GetRejectType($qafiid){

        $result = DB::table('qarejecttype')
        ->select(
            'qarejecttype.id',
            'qarejecttype.rejectcode',
            'qarejecttype.reject',
            DB::raw("IF(qafinalinspectionvisual.rejecttypeid IS NOT NULL, 1, 0) AS 'flag'")
        )
        ->leftjoin('qafinalinspectionvisual', function($join) use ($qafiid){
            $join->on('qafinalinspectionvisual.rejecttypeid', '=', 'qarejecttype.id')
            ->where('qafinalinspectionvisual.qafiid', '=', $qafiid);
        })
        ->get();

        return $result;

    }

    public static function SaveProductionTagInformation($qafiid, $tableschema){

        DB::table('productiontags')
        ->insert([
            'qafiid'=>$qafiid,
            'mstnumber'=>DB::raw("(SELECT CONCAT('MST', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'productiontags')"),
            'created_at'=>DB::raw("NOW()")
        ]);

    }


}
