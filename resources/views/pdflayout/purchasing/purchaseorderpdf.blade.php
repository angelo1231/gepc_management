<html>
<head>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

    {{-- JS --}}
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

</head>
<body>
    
   <div class="contianer">
        
         <table style="border: 2px solid black;">
            <tr>
                <td style="text-align: center;" colspan="5">
                    <img src="{{ public_path('images/GEPC_LOGO.png') }}" style="margin-bottom: -17px; width: 50px; margin-top: 10px;"/>
                    <h4>Blk 25 Lot 29-F Monserrat St. Villa De Toledo Subd. Brgy Balibago Sta Rosa Laguna<br>
                    Tel. No.: (049) 534-9581 * Fax No.: (049) 534-4439
                    </h4>
                    <h1>Purchase Order</h1>
                </td>

            </tr>
            <tr>

                <td colspan="4">
                </td>
                <td style="float: right; border: 2px solid black;">
                   PO# {{ $info[0]->ponumber }}
                </td>
                
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>      
            </tr>
            <tr>
                <td style="border: 2px solid black;" colspan="4">
                    TO:  {{ $info[0]->supplier }}
                </td>
                <td style="float: right; border: 2px solid black;">
                    Date: {{ date('d-M-y', strtotime($info[0]->created_at)) }}
                </td>       
            </tr>
             <tr>
                <td style="border: 2px solid black;" colspan="4">
                    ATTN: {{ $info[0]->attentionto }}
                </td>
                <td style="float: right; border: 2px solid black;">
                    Terms: {{ $info[0]->terms }} days
                </td>       
            </tr>
            <tr>
                <td colspan="4">
                   
                </td>
                <td style="float: right; border-top: 1px solid black; border-left: 2px solid black; border-right: 1px solid black;">
                    {{-- Delivery: {{ $info[0]->deliverydate }} --}}
                </td>       
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>      
            </tr>
            <tr>
                <td style="border: 2px solid black;" colspan="5">
                    Deliver To: {{ $info[0]->deliveryto }}
                </td>      
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>      
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><strong>ITEM #</strong></td>
                <td style="border: 2px solid black; text-align: center;"><strong>DESCRIPTION</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>QUANTITY</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>UNIT PRICE</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>TOTAL COST</strong></td>       
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            <?php $itemcount = 1; ?>
            @foreach($items as $item)
            <?php 

                $size = "";
                if($item->category=="Raw Material"){

                    if($item->widthsize!=""){
                        $size = $size . $item->width . "." . $item->widthsize;
                    }else{
                        $size = $size . $item->width;
                    }

                    if($item->lengthsize!=""){
                        $size = $size . " x " . $item->length . "." . $item->lengthsize;
                    }else{
                        $size = $size . " x " . $item->length;
                    }

                }
                else if($item->category=="Other"){

                    $size = $item->name;

                }
                

            ?>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><strong>{{ $itemcount }}</strong></td>
                <td style="border: 2px solid black; text-align: center;">
                    <strong>
                        {{ $size }} {{ $item->flute }} {{ $item->boardpound }} <br>
                        <?php $deldates = ""; ?>
                        @foreach($deliverydates as $del)
                            @if($item->itemid==$del->itemid)
                                <?php
                                    $deldates .= $del->deliverydate . ' ';
                                ?>
                            @endif
                        @endforeach
                        @if($deldates=="")
                        <span style="font-size: 10px;">Delivery: TBA</span>
                        @else
                        <span style="font-size: 10px;">Delivery: {{ $deldates }}</span>
                        @endif
                        @if($item->rmpapercombination!="")
                        <br>
                        {{ $item->rmpapercombination }}
                        @endif
                    </strong>
                </td>   
                <td style="border: 2px solid black; text-align: center;"><strong>{{ $item->qty }}</strong></td>   
                <td style="border: 2px solid black; text-align: center;">
                    <strong>
                        {{ $item->price }}
                    </strong>
                </td>   
                <td style="border: 2px solid black; text-align: center;"><strong>{{ number_format($item->total, 2) }}</strong></td>       
            </tr>
            <?php $itemcount += 1; ?>
            @endforeach 
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            @if($info[0]->deliverycharge!=0)
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"><strong>Delivery Charge</strong></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>{{ $info[0]->deliverycharge }}</strong></td>       
            </tr>
            @else
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            @endif
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>Total Amount</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>{{ $info[0]->grandtotal }}</strong></td>       
            </tr>
            <tr>
                <td colspan="5">
                    <p>Note:</p>
                    <p>1. Please acknowledge receipt and acceptance of the above order(s) by signing the space below. Kindly fac back within 24 hours to # (049) 534-9581 the signed copy for our file. Should no acknowledgement is received after the said time, it is understood that the details in this Purchase Order is official and binding.</p>
                    <p>2. Unless otherwise previously agreed, all deliveries shall be inspected by our Incoming Quality Assurance personnel using Mil-Std 105 at 1.0% AQL Single Sampling for Normal Inspection.</p>
                </td>
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;" colspan="3">
                    <img src="{{ public_path('esign') }}/{{ $info[0]->issuedesign }}" alt="" width="70" style="margin-top: 10px;">
                    <br>Prepared by: {{ $info[0]->issuedby }}
                </td>
                <td style="border: 2px solid black; text-align: center;" colspan="2">
                    @if($info[0]->approvedesign!="noesignimage.png")
                    <img src="{{ public_path('esign') }}/{{ $info[0]->approvedesign }}" alt="" width="70" style="margin-top: 10px;">
                    @endif
                    <br>Approved by: {{ $info[0]->approvedby }}
                </td>  
            </tr>
            <tr>
                <td colspan="5">
                    <p>Order is accepted and confirmed with conditions appearing hereof:</p>
                </td>
            </tr>
            <tr>
                <td style="border-top: 2px solid black; text-align: center;" colspan="2">Name</td>
                <td style="border-top: 2px solid black; text-align: center;">Signature</td>   
                <td style="border-top: 2px solid black; text-align: center;">Position</td>   
                <td style="border-top: 2px solid black; text-align: center;">Date</td>    
            </tr>
            <tr>
                <td colspan="5"><br></td>       
            </tr>
        </table>
        <p style="float: right; margin-top: -1px; font-size: 12px;">FO-PUR-02-00</p>
        

   </div>

</body>
</html>