@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Production</b></h3>
        </div>
        <div class="box-body">

            <div class="col-md-6">
              <h4><strong>For Printing Tags</strong></h4>
            </div>

            <div class="col-md-6">
              <button class="btn btn-flat btn-primary" style="margin-top: 8px; float: right;" ><i class="fa fa-th-list"></i> Tag History</button>
            </div>

            <div class="col-md-12">

                <br>
                <table id="tblproductiontags" class="table">
                    <thead>
                        <tr>
                          <th>JO #</th>
                          <th>MST #</th>
                          <th>Quantity</th>
                          <th>Reject</th>
                          <th>Issued By</th>
                          <th>Issued Date</th>
                          <th></th>
                        </tr>
                    </thead>
                </table>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblproductiontags;

    $(document).ready(function(){

      LoadMSTNumber();

    });

    $(document).on('click', '#btnprint', function(){

      @if(in_array(57, $access))

        var qafiid = $(this).val();

        $.confirm({
              title: 'Print',
              content: 'Print this MST tag?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        PrintTags(qafiid);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        
                      }
                  } 
              }
        });

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    function PrintTags(qafiid){

        $.ajax({
          url: '{{ url("api/production/updatemststatus") }}',
          type: 'post',
          data: {
            qafiid: qafiid
          },
          dataType: 'json',
          success: function(response){

            if(response.success){

              //Reload
              ReloadMSTNumber();

              //Print Form
              window.open("{{ url('/production/printproductiontag') }}/" + qafiid, '', 'width=1000,height=900');
              
              
            }

          }
        })

    }

    function LoadMSTNumber(){

      tblproductiontags = $('#tblproductiontags').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/production/loadmstnumber") }}',
        },
        columns : [
            {data: 'jonumber', name: 'jonumber'},
            {data: 'mstnumber', name: 'mstnumber'},
            {data: 'qty', name: 'qty'},
            {data: 'reject', name: 'reject'},
            {data: 'issuedby', name: 'issuedby'},
            {data: 'issueddate', name: 'issueddate'},
            {data: 'panel', name: 'panel', width: '12%'},
        ]
      });

    }

    function ReloadMSTNumber(){

      tblproductiontags.ajax.reload();

    }

    //Socket functions
    socket.on('reloadproductiontag', function(){

        ReloadMSTNumber();

    });


  </script>  

@endsection