<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jobordermonitoringaudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobordermonitoringaudit', function (Blueprint $table) {
            $table->increments('auditid');
            $table->integer('mid');
            $table->integer('pid');
            $table->integer('targetoutput');
            $table->integer('balance');
            $table->timestamps();
            $table->integer('isshow')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobordermonitoringaudit');
    }
}
