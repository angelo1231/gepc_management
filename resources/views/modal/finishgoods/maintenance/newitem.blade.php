<!-- Middle Modal -->
<style>
        .modal {
          text-align: center;
          padding: 0!important;
        }
        
        .modal:before {
          content: '';
          display: inline-block;
          height: 100%;
          vertical-align: middle;
          margin-right: -4px;
        }
        
        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }
        </style>   
        <!-- Modal -->
        <div class="modal fade" id="modalnewitem" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <strong>NEW FINISH GOOD</strong>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="container-fluid">
                        <div class="col-md-6">
                                <div class="form-group">
                                        <label for="txtnitemcode">Item Code</label>
                                        <input id="txtnitemcode" name="txtnitemcode" type="text" class="form-control" placeholder="Item Code">
                                </div>
                                <div class="form-group">
                                        <label for="txtnpartnumber">Partnumber</label>
                                        <input id="txtnpartnumber" name="txtnpartnumber" type="text" class="form-control" placeholder="Partnumber">
                                </div>
                                <div class="form-group">
                                        <label for="txtnpartname">Partname</label>
                                        <input id="txtnpartname" name="txtnpartname" type="text" class="form-control" placeholder="Partname">
                                </div>
                                <div class="form-group">
                                        <label for="txtndescription">Description</label>
                                        <input id="txtndescription" name="txtndescription" type="text" class="form-control" placeholder="Description">
                                </div>
                                <div class="form-group">
                                    <label for="cmbncustomer">Customer</label>
                                    <select class="form-control" id="cmbncustomer">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 row">
                                        <label for="cmbnprocess">Process</label>
                                        <select class="form-control" id="cmbnprocess">
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <i style="margin-top: 33px; font-size: 20px;" class="tooltips fa fa-info-circle">
                                            <span id="txtnprocesstip" class="tooltiptext" style="font-size: 12px;">Please Select A Process</span>
                                        </i>
                                    </div>
                                </div>
                        </div>

                        <div class="col-md-6">

                            <div class="col-md-12">
                                <div class="form-group">
                                        <label for="txtnpackingstd">Packing STD</label>
                                        <input id="txtnpackingstd" name="txtnpackingstd" type="text" class="form-control" placeholder="Packing Standard">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                        <label for="txtnstyle">Style</label>
                                        <input id="txtnstyle" name="txtnstyle" type="text" class="form-control" placeholder="Style" readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                        <label for="txtninkcolor">Ink Color</label>
                                        <input id="txtninkcolor" name="txtninkcolor" type="text" class="form-control" placeholder="Ink Color" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                        <label>I.D Size</label>
                                        <input id="txtnidwidth" name="txtnidwidth" type="text" class="form-control" placeholder="Width">
                                        <br>
                                        <input id="txtnidlength" name="txtnidlength" type="text" class="form-control" placeholder="Length">
                                        <br>
                                        <input id="txtnidheight" name="txtnidheight" type="text" class="form-control" placeholder="Height">
                                </div>

                                <div class="form-group">
                                    <label id="lblncurrency" for="cmbncurrency">Currency</label>
                                    <select class="form-control" id="cmbncurrency" name="cmbncurrency">
                                    </select>
                                    
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>O.D Size</label>
                                    <input id="txtnodwidth" name="txtnodwidth" type="text" class="form-control" placeholder="Width">
                                    <br>
                                    <input id="txtnodlength" name="txtnodlength" type="text" class="form-control" placeholder="Length">
                                    <br>
                                    <input id="txtnodheight" name="txtnodheight" type="text" class="form-control" placeholder="Height">
                                    
                                    <div class="form-group" style="margin-top: 15px;">
                                        <label id="lblnprice" for="txtnprice">Price</label>
                                        <input id="txtnprice" name="txtnprice" type="number" class="form-control" placeholder="Price">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <label for="nattachment">Drawing Attachment</label>
                            <input type="file" name="nattachment" id="nattachment">
                            <img src="{{ asset('images/noimgattachment.png') }}" id="imgnattachment" style="margin: 10px 24%;" width="400px">
                        </div>

                      </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-9">
        
                        </div>
                        <div class="col-md-3">
                            <button id="btnsave" name="btnsave" type="button" class="btn btn-primary btn-block btn-flat">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>