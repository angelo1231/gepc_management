<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Delivery_Model extends Model
{
    
    public static function LoadCustomer(){

        $result = DB::table('customers')
        ->select(
            'cid',
            'customer'
        )
        ->get();

        return $result;

    }

    public static function LoadCustomerItems($cid){

        $result = DB::table('finishgoods')
        ->select(
            'fgid',
            DB::raw("description AS 'item'")
        )
        ->where('cid', '=', $cid)
        ->get();

        return $result;

    }

    public static function GetFGProfile($fgid){

        $result = DB::table('finishgoods')
        ->select(
            '*'
        )
        ->where('fgid', '=', $fgid)
        ->get();

        return $result;

    }

    public static function GetCurrency($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'finishgoods.price',
            'currency.rate'
        )
        ->leftjoin('currency', 'currency.currencyid', '=', 'finishgoods.currencyid')
        ->where('finishgoods.fgid', '=', $fgid)
        ->get();

        return $result;

    }

    public static function SaveCustomerPOInformation($ponumber, $cid, $grandtotal, $remarks, $deliverydate, $issuedby, $isopenpo){

        $result = DB::table('pocustomerinformation')
        ->insertGetId([
            "ponumber"=>$ponumber,
            "cid"=>$cid,
            "grandtotal"=>$grandtotal,
            "remarks"=>$remarks,
            "deliverydate"=>$deliverydate,
            "issuedby"=>$issuedby,
            "status"=>"Open",
            "created_at"=>DB::raw("NOW()"),
            'is_openpo'=>$isopenpo
        ]);

        return $result;

    }

    public static function Currency($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'currency.currencyid',
            'currency.rate',
            'finishgoods.price'
        )
        ->leftjoin('currency', 'currency.currencyid', '=', 'finishgoods.currencyid')
        ->where('finishgoods.fgid', '=', $fgid)
        ->get();

        return $result;

    }

    public static function SaveCustomerPOInformationItems($poid, $fgid, $currencyid, $rate, $qty, $price, $total){

        DB::table('pocustomerinformationitems')
        ->insert([
            "poid"=>$poid,
            "fgid"=>$fgid,
            "currencyid"=>$currencyid,
            "rate"=>$rate,
            "qty"=>$qty,
            "price"=>$price,
            "total"=>$total,
            "delqty"=>"0"
        ]);

    }

    public static function ValidatePurchaseOrderNumber($ponumber){

        $result = DB::table('pocustomerinformation')
        ->select(
            DB::raw("COUNT(*) AS 'ponumber_count'")
        )
        ->where('ponumber', '=', $ponumber)
        ->get();

        if($result[0]->ponumber_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function LoadCustomerPO($customer){

        if($customer=="All"){

            $result = DB::table('pocustomerinformation')
            ->select(
                'pocustomerinformation.status',
                'pocustomerinformation.poid',
                'pocustomerinformation.ponumber',
                DB::raw("GROUP_CONCAT(finishgoods.description SEPARATOR '<br>') AS 'item'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.qty SEPARATOR '<br>') AS 'qty'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.delqty SEPARATOR '<br>') AS 'served'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.qty - pocustomerinformationitems.delqty SEPARATOR '<br>') AS 'unserved'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.price SEPARATOR '<br>') AS 'price'"),
                DB::raw("GROUP_CONCAT((pocustomerinformationitems.rate * pocustomerinformationitems.price) * (pocustomerinformationitems.qty - pocustomerinformationitems.delqty) SEPARATOR '<br>') AS 'pobalamount'"),
                'customers.customer'
            )
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.poid', '=', 'pocustomerinformation.poid')
            ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
            ->join('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
            ->groupBy('pocustomerinformation.poid')
            ->orderBy('pocustomerinformation.poid', 'DESC')
            ->get();

            return $result;

        }
        else{

            $result = DB::table('pocustomerinformation')
            ->select(
                'pocustomerinformation.status',
                'pocustomerinformation.poid',
                'pocustomerinformation.ponumber',
                DB::raw("GROUP_CONCAT(finishgoods.description SEPARATOR '<br>') AS 'item'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.qty SEPARATOR '<br>') AS 'qty'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.delqty SEPARATOR '<br>') AS 'served'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.qty - pocustomerinformationitems.delqty SEPARATOR '<br>') AS 'unserved'"),
                DB::raw("GROUP_CONCAT(pocustomerinformationitems.price SEPARATOR '<br>') AS 'price'"),
                DB::raw("GROUP_CONCAT((pocustomerinformationitems.rate * pocustomerinformationitems.price) * (pocustomerinformationitems.qty - pocustomerinformationitems.delqty) SEPARATOR '<br>') AS 'pobalamount'"),
                'customers.customer'
            )
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.poid', '=', 'pocustomerinformation.poid')
            ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
            ->join('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
            ->where('pocustomerinformation.cid', '=', $customer)
            ->groupBy('pocustomerinformation.poid')
            ->orderBy('pocustomerinformation.poid', 'DESC')
            ->get();

            return $result;

        }

    }

    public static function CustomerPOProfile($poid){

        $result = DB::table('pocustomerinformation')
        ->select(
            'customers.customer',
            'pocustomerinformation.ponumber',
            'pocustomerinformation.grandtotal',
            'pocustomerinformation.remarks',
            'pocustomerinformation.deliverydate',
            'pocustomerinformation.is_openpo'
        )
        ->leftjoin('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
        ->where('pocustomerinformation.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function CustomerPOItemsProfile($poid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'pocustomerinformationitems.itemid',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'finishgoods.ID_length',
            'finishgoods.ID_width',
            'finishgoods.ID_height',
            'finishgoods.OD_length',
            'finishgoods.OD_width',
            'finishgoods.OD_height',
            'pocustomerinformationitems.price',
            'pocustomerinformationitems.qty',
            'pocustomerinformationitems.total',
            'pocustomerinformationitems.delqty'
        )
        ->leftjoin('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('pocustomerinformationitems.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function GetPOCustomer($cid){

        $result = DB::table('pocustomerinformation')
        ->select(
            'poid',
            'ponumber'
        )
        ->where('cid', '=', $cid)
        ->get();

        return $result;

    }

    public static function GetPOItems($poid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'pocustomerinformationitems.itemid', 
            DB::raw("CONCAT(finishgoods.partnumber, ' ', finishgoods.partname, ' ', finishgoods.description) AS 'finishgood'")
        )
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('pocustomerinformationitems.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function LoadItemInformation($itemid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            DB::raw("CONCAT(finishgoods.partnumber, ' ', finishgoods.partname, ' ', finishgoods.description) AS 'finishgood'"),
            DB::raw("CONCAT('W: ', finishgoods.ID_width, ' L: ', finishgoods.ID_length, ' H: ', finishgoods.ID_height) AS 'idsize'"),
            DB::raw("CONCAT('W: ', finishgoods.OD_width, ' L: ', finishgoods.OD_length, ' H: ', finishgoods.OD_height) AS 'odsize'"),
            'pocustomerinformationitems.price',
            'finishgoods.stockonhand'
        )
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('pocustomerinformationitems.itemid', '=', $itemid)
        ->first();

        return $result;

    }

    public static function LoadCurrency($itemid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'rate',
            'price'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        return $result;

    }

    public static function SaveDeliveryInformation($drnumber, $deldate, $grandtotal, $cid, $issuedby){

        //Get Tax Classification
        $taxclassification = DB::table('customers')
        ->select(
            DB::raw("LCASE(REPLACE(taxclassification, ' ', '')) AS 'taxclassification'")
        )
        ->where('cid', '=', $cid)
        ->first();

        //Save Delivery Information
        $result = DB::table('deliveryinformation')
        ->insertGetId([
            "drnumber"=>$drnumber, 
            "grandtotal"=>$grandtotal,
            "deliverydate"=>$deldate,
            "cid"=>$cid,
            "taxclassification"=>DB::raw("(SELECT taxclassification FROM customers WHERE cid='". $cid ."')"),
            "tax"=>DB::raw("(SELECT ".$taxclassification->taxclassification." FROM settings)"),
            "status"=>"Open",
            "issuedby"=>$issuedby,
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function SaveDeliveryInformationItems($did, $poid, $itemid, $qty, $price, $total){

        DB::table('deliveryinformationitems')
        ->insert([
            "did"=>$did, 
            "poid"=>$poid,
            "pocusitemid"=>$itemid,
            "fgid"=>DB::raw("
                (SELECT 
                fgid 
                FROM pocustomerinformationitems 
                WHERE itemid='". $itemid ."')
            "),
            "currencyid"=>DB::raw("
                (SELECT
                currency.currencyid
                FROM 
                pocustomerinformationitems
                INNER JOIN finishgoods ON finishgoods.fgid=pocustomerinformationitems.fgid
                INNER JOIN currency ON currency.currencyid=finishgoods.currencyid
                WHERE pocustomerinformationitems.itemid='". $itemid ."')
            "),
            "rate"=>DB::raw("
                (SELECT
                currency.rate
                FROM 
                pocustomerinformationitems
                INNER JOIN finishgoods ON finishgoods.fgid=pocustomerinformationitems.fgid
                INNER JOIN currency ON currency.currencyid=finishgoods.currencyid
                WHERE pocustomerinformationitems.itemid='". $itemid ."') 
            "),
            "price"=>$price,
            "qty"=>$qty,
            "total"=>$total
        ]);

    }

    public static function LoadDeliveryInformation($data){

        if($data->cid=="All"){

            $result = DB::table('deliveryinformation')
            ->select(
                'deliveryinformation.did',
                'deliveryinformation.drnumber',
                DB::raw("(SELECT SUM(qty) FROM deliveryinformationitems WHERE did=deliveryinformation.did) AS 'totalqty'"),
                'deliveryinformation.grandtotal',
                'deliveryinformation.deliverydate',
                'deliveryinformation.issuedby',
                DB::raw("DATE_FORMAT(deliveryinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
                'customers.customer'
            )
            ->join('customers', 'customers.cid', '=', 'deliveryinformation.cid')
            ->where('deliveryinformation.status', '=', 'Open')
            ->get();
    
            return $result;

        }
        else{

            $result = DB::table('deliveryinformation')
            ->select(
                'deliveryinformation.did',
                'deliveryinformation.drnumber',
                DB::raw("(SELECT SUM(qty) FROM deliveryinformationitems WHERE did=deliveryinformation.did) AS 'totalqty'"),
                'deliveryinformation.grandtotal',
                'deliveryinformation.deliverydate',
                'deliveryinformation.issuedby',
                DB::raw("DATE_FORMAT(deliveryinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
                'customers.customer'
            )
            ->join('customers', 'customers.cid', '=', 'deliveryinformation.cid')
            ->where('deliveryinformation.cid', '=', $data->cid)
            ->where('deliveryinformation.status', '=', 'Open')
            ->get();
    
            return $result;

        }

    }

    public static function ValidateDeliveryNumber($drnumber){

        $result = DB::table('deliveryinformation')
        ->select(
            DB::raw("COUNT(*) AS 'dr_count'")
        )
        ->where('drnumber', '=', $drnumber)
        ->first();

        if($result->dr_count==1){

            return true;

        }
        else{

            return false;

        }


    }

    public static function ValidateStockOnHand($itemid, $qty){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'finishgoods.stockonhand'
        )
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('pocustomerinformationitems.itemid', '=', $itemid)
        ->first();

        if($result->stockonhand<$qty){
            return true;
        }
        else{
            return false;
        }

    }

    public static function GetFGInformation($itemid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'finishgoods.stockonhand',
            DB::raw("CONCAT(finishgoods.partnumber, ' ', finishgoods.partname, ' ', finishgoods.description) AS 'finishgood'")
        )
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('pocustomerinformationitems.itemid', '=', $itemid)
        ->first();

        return $result;

    }

    public static function GetDeliveryItemInformation($did){

        $result = DB::table('deliveryinformationitems')
        ->select(
            'deliveryinformationitems.itemid',
            'deliveryinformation.drnumber',
            'pocustomerinformation.ponumber',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'deliveryinformationitems.qty',
            'deliveryinformationitems.price',
            'deliveryinformationitems.total'  
        )
        ->join('deliveryinformation', 'deliveryinformation.did', '=', 'deliveryinformationitems.did')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'deliveryinformationitems.fgid')
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'deliveryinformationitems.poid')
        ->where('deliveryinformationitems.did', '=', $did)
        ->get();

        return $result;


    }

    public static function CreateInvoice($data, $issuedby){

        DB::table('invoiceinformation')
        ->insert([
            'invoicenumber'=>$data->invoicenumber,
            'did'=>$data->did,
            'cid'=>DB::raw("(SELECT cid FROM deliveryinformation WHERE did='".$data->did."')"),
            'issuedby'=>$issuedby,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function CloseDeliveryInformation($data){

        DB::table('deliveryinformation')
        ->where('did', '=', $data->did)
        ->update([
            'status'=>'Close',
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function SaveSalesOrderDelivery($deliveryqty, $deliverydate, $itemid, $issuedby, $tableschema){

        DB::table('pocustomerinformationitemdelivery')
        ->insert([
            'salesnumber'=>DB::raw("(SELECT CONCAT('SO', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'pocustomerinformationitemdelivery')"),
            'itemid'=>$itemid,
            'deliveryqty'=>$deliveryqty,
            'deliverydate'=>$deliverydate,
            'status'=>"Open",
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function LoadItemDeliveryInformation($itemid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'delid',
            'salesnumber',
            'deliveryqty',
            'deliverydate',
            DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') AS 'created_at'"),
            'status'
        )
        ->where('itemid', '=', $itemid)
        ->whereNull('deleted_at')
        ->get();

        return $result;

    }

    public static function GetPOCustomerID($poid){

        $result = DB::table('pocustomerinformation')
        ->select(
            'cid'
        )
        ->where('poid', '=', $poid)
        ->first();

        return $result->cid;

    }

    public static function UpdatePOInformation($poid, $total){

        $total = DB::table('pocustomerinformation')
        ->select(
            DB::raw("(grandtotal + ". $total .") AS 'grandtotal'")
        )
        ->where('poid', '=', $poid)
        ->first();

        DB::table('pocustomerinformation')
        ->where('poid', '=', $poid)
        ->update([
            "grandtotal"=>$total->grandtotal
        ]);

        return $total->grandtotal;

    }

    public static function SavePreparationSlipInformation($cid, $issuedby, $tableschema){

        $result = DB::table('deliverypreparationsinformation')
        ->insertGetId([
            'preparationnumber'=>DB::raw("(SELECT CONCAT('PS', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'deliverypreparationsinformation')"),
            'cid'=>$cid,
            'issuedby'=>$issuedby,
            'status'=>'Open',
            'created_at'=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function SavePreparationSlipInformationItems($preparationid, $poid, $itemid, $qty){

        DB::table('deliverypreparationsinformationitems')
        ->insert([
            'preparationid'=>$preparationid,
            'poid'=>$poid,
            'pocusitemid'=>$itemid,
            'fgid'=>DB::raw("
                (SELECT 
                fgid 
                FROM pocustomerinformationitems 
                WHERE itemid='". $itemid ."')
            "),
            'qty'=>$qty
        ]);

    }

    public static function LoadPreparationSlipInformation($data){

        if($data->cid=="All"){

            $result = DB::table('deliverypreparationsinformation')
            ->select(
                'deliverypreparationsinformation.preparationid',
                'deliverypreparationsinformation.preparationnumber',
                DB::raw("(SELECT SUM(qty) FROM deliverypreparationsinformationitems WHERE preparationid=deliverypreparationsinformation.preparationid) AS 'totalqty'"),
                'deliverypreparationsinformation.issuedby',
                DB::raw("DATE_FORMAT(deliverypreparationsinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
                'customers.customer',
                'deliverypreparationsinformation.status'
            )
            ->join('customers', 'customers.cid', '=', 'deliverypreparationsinformation.cid')
            ->where('deliverypreparationsinformation.status', '!=', 'Close')
            ->get();
    
            return $result;

        }
        else{

            $result = DB::table('deliverypreparationsinformation')
            ->select(
                'deliverypreparationsinformation.preparationid',
                'deliverypreparationsinformation.preparationnumber',
                DB::raw("(SELECT SUM(qty) FROM deliverypreparationsinformationitems WHERE preparationid=deliverypreparationsinformation.preparationid) AS 'totalqty'"),
                'deliverypreparationsinformation.issuedby',
                DB::raw("DATE_FORMAT(deliverypreparationsinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
                'customers.customer',
                'deliverypreparationsinformation.status'
            )
            ->join('customers', 'customers.cid', '=', 'deliverypreparationsinformation.cid')
            ->where('deliverypreparationsinformation.cid', '=', $data->cid)
            ->where('deliverypreparationsinformation.status', '!=', 'Close')
            ->get();
    
            return $result;

        }

    }

    public static function GetPreparationInfo($preparationid){

        //SQL Statement
        // SELECT 
        // deliverypreparationsinformation.cid,
        // customers.taxclassification,
        // (SELECT
        // CAST(
        // ( (currency.rate * finishgoods.price) * deliverypreparationsinformationitems.qty  )
        // AS DECIMAL(8,2))
        // FROM deliverypreparationsinformationitems
        // INNER JOIN finishgoods ON finishgoods.fgid=deliverypreparationsinformationitems.fgid
        // INNER JOIN currency ON currency.currencyid=finishgoods.currencyid
        // WHERE deliverypreparationsinformationitems.preparationid=2) AS 'grandtotal'
        // FROM deliverypreparationsinformation
        // INNER JOIN customers ON customers.cid=deliverypreparationsinformation.cid
        // WHERE deliverypreparationsinformation.preparationid=2;

        $result = DB::table('deliverypreparationsinformation')
        ->select(
            'deliverypreparationsinformation.cid',
            'customers.taxclassification',
            DB::raw("
                (SELECT
                CAST(
                ( (currency.rate * finishgoods.price) * deliverypreparationsinformationitems.qty  )
                AS DECIMAL(8,2))
                FROM deliverypreparationsinformationitems
                INNER JOIN finishgoods ON finishgoods.fgid=deliverypreparationsinformationitems.fgid
                INNER JOIN currency ON currency.currencyid=finishgoods.currencyid
                WHERE deliverypreparationsinformationitems.preparationid='". $preparationid ."') AS 'grandtotal'
            ")
        )
        ->join('customers', 'customers.cid', '=', 'deliverypreparationsinformation.cid')
        ->where('deliverypreparationsinformation.preparationid', '=', $preparationid)
        ->first();

        return $result;

    }

    public static function CreateDRInfo($preparationinfo, $deliverynumber, $deliverydate, $issuedby){

        //Get Tax Classification
        $taxclassification = DB::table('customers')
        ->select(
            DB::raw("LCASE(REPLACE(taxclassification, ' ', '')) AS 'taxclassification'")
        )
        ->where('cid', '=', $preparationinfo->cid)
        ->first();

        //Save Delivery Information
        $result = DB::table('deliveryinformation')
        ->insertGetId([
            "drnumber"=>$deliverynumber, 
            "grandtotal"=>$preparationinfo->grandtotal,
            "deliverydate"=>$deliverydate,
            "cid"=>$preparationinfo->cid,
            "taxclassification"=>DB::raw("(SELECT taxclassification FROM customers WHERE cid='". $preparationinfo->cid ."')"),
            "tax"=>DB::raw("(SELECT ".$taxclassification->taxclassification." FROM settings)"),
            "status"=>"Open",
            "issuedby"=>$issuedby,
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;
        
    }

    public static function GetPreparationItems($preparationid){

        //SQL Statement
        // SELECT
        // deliverypreparationsinformationitems.poid,
        // deliverypreparationsinformationitems.pocusitemid,
        // deliverypreparationsinformationitems.fgid,
        // currency.currencyid,
        // currency.rate,
        // deliverypreparationsinformationitems.qty,
        // finishgoods.price,
        // CAST(( (currency.rate * finishgoods.price) * deliverypreparationsinformationitems.qty ) AS DECIMAL(8,2)) AS 'total'
        // FROM deliverypreparationsinformationitems
        // INNER JOIN finishgoods ON finishgoods.fgid=deliverypreparationsinformationitems.fgid
        // INNER JOIN currency ON currency.currencyid=finishgoods.currencyid
        // WHERE deliverypreparationsinformationitems.preparationid=2

        $result = DB::table('deliverypreparationsinformationitems')
        ->select(
            'deliverypreparationsinformationitems.poid',
            'deliverypreparationsinformationitems.pocusitemid',
            'deliverypreparationsinformationitems.fgid',
            'currency.currencyid',
            'currency.rate',
            'deliverypreparationsinformationitems.qty',
            'finishgoods.price',
            DB::raw("
                CAST(( (currency.rate * finishgoods.price) * deliverypreparationsinformationitems.qty ) AS DECIMAL(8,2)) AS 'total'
            ")
        )
        ->join('finishgoods', 'finishgoods.fgid', '=', 'deliverypreparationsinformationitems.fgid')
        ->join('currency', 'currency.currencyid', '=', 'finishgoods.currencyid')
        ->where('deliverypreparationsinformationitems.preparationid', '=', $preparationid)
        ->get();

        return $result;

    }

    public static function CreateDRItems($did, $poid, $itemid, $fgid, $currencyid, $rate, $qty, $price, $total){

        DB::table('deliveryinformationitems')
        ->insert([
            "did"=>$did, 
            "poid"=>$poid,
            "pocusitemid"=>$itemid,
            "fgid"=>$fgid,
            "currencyid"=>$currencyid,
            "rate"=>$rate,
            "price"=>$price,
            "qty"=>$qty,
            "total"=>$total
        ]);

    }

    public static function UpdatePreparationSlip($preparationid){

        DB::table('deliverypreparationsinformation')
        ->where('preparationid', '=', $preparationid)
        ->update([
            "status"=>"Close",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateQuantitySO($itemid, $deliveryqty){

        $totaldeliveryso = $deliveryqty;

        $resultitemdel = DB::table('pocustomerinformationitems')
        ->select(
            'qty'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        $resultso = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'deliveryqty'
        )
        ->where('itemid', '=', $itemid)
        ->get();

        if(count($resultso)!=0){ //Check If There Is Data SO

            foreach($resultso as $val){

                $totaldeliveryso += $val->deliveryqty;

            }

        }

        if($totaldeliveryso>$resultitemdel->qty){
            return true;
        }
        else{
            return false;
        }

    }

    public static function LoadDelIDDeliveryInfo($delid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'deliveryqty'
        )
        ->where('delid', '=', $delid)
        ->first();

        return $result->deliveryqty;

    }

    public static function UpdateDelQtyInfo($delid, $deliveryqty){

        DB::table('pocustomerinformationitemdelivery')
        ->where('delid', '=', $delid)
        ->update([
            'deliveryqty'=>$deliveryqty
        ]);

    }

    public static function ValidateUpdateQuantitySO($delid, $deliveryqty, $itemid){

        $totaldeliveryso = $deliveryqty;

        $resultitemdel = DB::table('pocustomerinformationitems')
        ->select(
            'qty'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        $resultso = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'deliveryqty'
        )
        ->where('itemid', '=', $itemid)
        ->where('delid', '!=', $delid)
        ->get();

        if(count($resultso)!=0){ //Check If There Is Data SO

            foreach($resultso as $val){

                $totaldeliveryso += $val->deliveryqty;

            }

        }

        if($totaldeliveryso>$resultitemdel->qty){
            return true;
        }
        else{
            return false;
        }

    }

    public static function ValidatePOItem($poid, $fgid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            DB::raw("COUNT(*) AS 'pocusitemcount'")
        )
        ->where('poid', '=', $poid)
        ->where('fgid', '=', $fgid)
        ->first();

        if($result->pocusitemcount!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function UpdateQuantityItem($itemid, $qty){

        DB::table('pocustomerinformationitems')
        ->where('itemid', '=', $itemid)
        ->update([
            "qty"=>$qty
        ]);

    }

}
