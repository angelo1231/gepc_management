<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="updatepapercombination" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>UPDATE PAPER COMBINATION</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">
    
                        <div class="form-group">
                            <label for="txturmpapercombination">RM Paper Combination</label>
                            <input type="text" id="txturmpapercombination" name="txturmpapercombination" class="form-control" placeholder="RM Paper Combination">
                        </div>

                        <div class="form-group">
                            <label for="cmbuflute">Flute</label>
                            <select id="cmbuflute" name="cmbuflute" class="form-control">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="cmbuboardpound">Board Pound</label>
                            <select id="cmbuboardpound" name="cmbuboardpound" class="form-control">
                            </select>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btnupdatepapercombination" name="btnupdatepapercombination" class="btn btn-success">Update Information</button>
                        <button data-dismiss="modal" class="btn btn-danger">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>