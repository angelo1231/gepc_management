<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PaperCombination_Model extends Model
{
    
    public static function LoadFlute(){

        $result = DB::table('rawmaterials')
        ->select(
            'flute'
        )
        ->groupBy('flute')
        ->get();

        return $result;

    }

    public static function LoadBoardPound($flute){

        $result = DB::table('rawmaterials')
        ->select(
            'boardpound'
        )
        ->where('flute', '=', $flute)
        ->groupBy('boardpound')
        ->get();

        return $result;

    }

    public static function ValidatePaperCombination($flute, $boardpound, $rmpapercombination){

        $result = DB::table('papercombination')
        ->select(
            DB::raw("COUNT(*) AS 'papercomcount'")
        )
        ->where('flute', '=', $flute)
        ->where('boardpound', '=', $boardpound)
        ->where('rmpapercombination', '=', $rmpapercombination)
        ->first();

        if($result->papercomcount!="0"){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SavePaperCombination($flute, $boardpound, $rmpapercombination){

        DB::table('papercombination')
        ->insert([
            "rmpapercombination"=>$rmpapercombination,
            "flute"=>$flute,
            "boardpound"=>$boardpound,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadPaperCombination(){

        $result = DB::table('papercombination')
        ->select(
            'papercomid',
            'rmpapercombination',
            'flute',
            'boardpound',
            'created_at',
            'updated_at'
        )
        ->get();

        return $result;

    }

    public static function LoadPaperCombinationInfo($papercomid){

        $result = DB::table('papercombination')
        ->select(
            'rmpapercombination',
            'flute',
            'boardpound'
        )
        ->where('papercomid', '=', $papercomid)
        ->first();

        return $result;

    }

    public static function ValidateUpdatePaperCombination($papercomid, $rmpapercombination, $flute, $boardpound){

        $result = DB::table('papercombination')
        ->select(
            'papercomid',
            DB::raw("COUNT(*) AS 'papercomcount'")
        )
        ->where('rmpapercombination', '=', $rmpapercombination)
        ->where('flute', '=', $flute)
        ->where('boardpound', '=', $boardpound)
        ->first();

        if($result->papercomcount!=0){

            if($papercomid==$result->papercomid){
                return false;
            }
            else{
                return true;
            }

        }
        else{

            return false;

        }

    }

    public static function UpdatePaperCombination($papercomid, $rmpapercombination, $flute, $boardpound){

        DB::table('papercombination')
        ->where('papercomid', '=', $papercomid)
        ->update([
            'rmpapercombination'=>$rmpapercombination,
            'flute'=>$flute,
            'boardpound'=>$boardpound,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

}
