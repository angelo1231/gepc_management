-- MySQL dump 10.13  Distrib 5.7.26, for Win64 (x86_64)
--
-- Host: localhost    Database: db_accounting_management
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access`
--

DROP TABLE IF EXISTS `access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access` (
  `accessid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`accessid`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access`
--

LOCK TABLES `access` WRITE;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` VALUES (1,'Maintenance',NULL,NULL),(2,'User Account Information',NULL,NULL),(3,'New User Information',NULL,NULL),(4,'Edit User Information',NULL,NULL),(5,'Change User Password',NULL,NULL),(6,'Reset User Password',NULL,NULL),(7,'User Group Information',NULL,NULL),(8,'New User Group Information',NULL,NULL),(9,'Add User Group Access',NULL,NULL),(10,'Raw Material 201 Information',NULL,NULL),(11,'New Raw Material Information',NULL,NULL),(12,'Edit Raw Material Information',NULL,NULL),(13,'FInish Goods 201 Information',NULL,NULL),(14,'New FInish Goods Information',NULL,NULL),(15,'Edit Finish Goods Information',NULL,NULL),(16,'Process Information',NULL,NULL),(17,'New Process Information',NULL,NULL),(18,'Edit Process Information',NULL,NULL),(19,'Process Code Information',NULL,NULL),(20,'New Process Code Information',NULL,NULL),(21,'Edit Process Code Information',NULL,NULL),(22,'Supplier 201 Information',NULL,NULL),(23,'New Supplier Information',NULL,NULL),(24,'Edit Supplier Information',NULL,NULL),(25,'Customer 201 Information',NULL,NULL),(26,'New Customer Information',NULL,NULL),(27,'Edit Customer Information',NULL,NULL),(28,'Currency 201 Information',NULL,NULL),(29,'New Currency Information',NULL,NULL),(30,'Edit Currency Information',NULL,NULL),(31,'Settings',NULL,NULL),(32,'Purchasing',NULL,NULL),(33,'New Purchase Order',NULL,NULL),(34,'Planning',NULL,NULL),(35,'New Job Order',NULL,NULL),(36,'Purchase Request Planning Info',NULL,NULL),(37,'New Purchase Request Planning',NULL,NULL),(38,'Material Request Planning',NULL,NULL),(39,'New Material Request Planning',NULL,NULL),(40,'Customer Purchase Order Planning Info',NULL,NULL),(41,'Raw Material Inventory',NULL,NULL),(42,'Raw Material Request Approval Inventory',NULL,NULL),(43,'Raw Material Delivery Info Inventory',NULL,NULL),(44,'Finish Goods Inventory',NULL,NULL),(45,'Finish Goods Production',NULL,NULL),(46,'Finish Goods Delivery',NULL,NULL),(47,'Sales',NULL,NULL),(48,'New Sales Information',NULL,NULL),(49,'Sales Customer Purchase Order Info',NULL,NULL),(50,'Sales New Customer Purchase Order Info',NULL,NULL),(52,'Quality Assurance',NULL,NULL),(53,'New COC',NULL,NULL),(54,'COC Information',NULL,NULL),(55,'Production',NULL,NULL),(56,'Production Tag',NULL,NULL),(57,'Production Tag Print',NULL,NULL),(58,'Production Operation',NULL,NULL),(59,'New Prodcution Operation',NULL,NULL),(60,'Production Operation Information',NULL,NULL),(61,'Accounting',NULL,NULL),(62,'Accounting Delivery Sales Information',NULL,NULL),(63,'Purchasing Purchase Request ',NULL,NULL),(64,'Production Monitoring',NULL,NULL),(65,'Purchase Order Approver',NULL,NULL),(66,'Update FG Stock',NULL,NULL),(67,'Update RM Stock',NULL,NULL),(68,'Planning Notification',NULL,NULL),(69,'Quality Assurance Reject Information',NULL,NULL),(70,'Quality Assurance New Reject Type',NULL,NULL),(71,'Quality Assurance Update Reject Type',NULL,NULL);
/*!40000 ALTER TABLE `access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `currencyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'American Dollars',53.25),(2,'PH Peso',1.00);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactperson` text COLLATE utf8mb4_unicode_ci,
  `contactnumber` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` text COLLATE utf8mb4_unicode_ci,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `taxclassification` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Aikawa Philippines Inc.',NULL,NULL,NULL,'FPIP Sto. Tomas Batangas','006-035-150-000','30 Days','Vat Exempt'),(2,'B/E Aerospace B.V.',NULL,NULL,NULL,'FPIP Sto. Tomas Batangas','289-439-550-000','30 Days','Vat Exempt'),(3,'Continental Temic Electronics Phils, Inc.',NULL,NULL,NULL,'No.16 Ringroad LISP II SEZ Brgy. La Mesa Calamba Laguna','234-474-332','30 Days','Vat Exempt'),(4,'Denso Philippines Corporation',NULL,NULL,NULL,'109 Unity Ave. CIP, canlubang, Laguna','004-706-120-000','30 Days','Vat Exempt'),(5,'Device Dynamic Asia',NULL,NULL,NULL,'Batino, Calamba Laguna','204-770-285-000','30 Days','Vat Exempt'),(6,'Godo Philippines Inc.',NULL,NULL,NULL,'City Land Tower Shaw Blvd, Mandaluyong City','217-776-769-000','60 Days','Vatable Sales'),(7,'Inoac Philippines Corp.',NULL,NULL,NULL,'RGC Compound Brgy. Pittland Cabuyao Laguna','005-240-769-000','30 Days','Vatable Sales'),(8,'Integrated Micro-Electronics Inc.',NULL,NULL,NULL,'Laguna Technopark SEPZ Binan Laguna','000-409-747-000','120 Days','Vat Exempt'),(9,'JSJ Packaging Services',NULL,NULL,NULL,'Poblacion 2 Marilao Bulacan','128-136-211-000','30 Days','Vatable Sales'),(10,'Littlefuse Philippines Inc.',NULL,NULL,NULL,'Lima Technology Center Malvar, Batangas','005-196-916-000','60 Days','Vat Exempt'),(11,'Maccaferri Philippines Manufacturing Inc.',NULL,NULL,NULL,'Blk,3 Lot.3&4 Star Avenue LIIP Mamplasan City of Binan','008-382-415-000','30 Days','Vat Exempt'),(12,'Masuma Food Industry Inc.',NULL,NULL,NULL,'Bagsakan Road SEZ FTI Complex, Taguig City','245-660-529-000','30 Days','Vatable Sales'),(13,'Mitsuba Philippines Corporation',NULL,NULL,NULL,'Lima Technology Center, Malvar, Batangas','005-065-700-000','30 Days','Vat Exempt'),(14,'Nep Logistics Inc.',NULL,NULL,NULL,'Laguna Technopark Binan Laguna','204-562-495-000','30 Days','Vat Exempt'),(15,'Nihon Houzai Laguna Corporation',NULL,NULL,NULL,'L3281 SEPZ Laguna Technopark, City of Binan, Laguna','231-496-735-000','30 Days','Vat Exempt'),(16,'NxtRev Technology Inc.',NULL,NULL,NULL,'B 12 L 15 Molino III, Bacoor Cavite','007-646-638-000','30 Days','Zero Rated'),(17,'Okuda Phils.',NULL,NULL,NULL,'FPIP Tanauan City, Batangas','009-099-635-000','30 Days','Vat Exempt'),(18,'Philippines TRC Inc.',NULL,NULL,NULL,'Lima Technology Center, Malvar Batangas','005-739-755-000','30 Days','Vat Exempt'),(19,'Sohbi Kohgei Phils. Inc.',NULL,NULL,NULL,'Lima Technology Center Malvar, Batangas','005-739-706-000','30 Days','Vat Exempt'),(20,'Terumo Philippines Corporation',NULL,NULL,NULL,'Laguna Technopark Binan Laguna','005-648-877-000','30 Days','Vat Exempt'),(21,'Testech Incorporated',NULL,NULL,NULL,'SEZ Brgy.Batino Calamba City','208-003-322-000','30 Days','Vat Exempt'),(22,'Thermos Vaccumtech Philippines Inc.',NULL,NULL,NULL,'FPIP Sto. Tomas Batangas','008-855-485-000','30 Days','Vat Exempt'),(23,'Tsuchiya Kogyo (Phils.) Inc.',NULL,NULL,NULL,'L9 B 1, LISP III Sto. Tomas Batangas','000-666-637-000','30 Days','Vat Exempt'),(24,'Vishay (Phils) Inc.',NULL,NULL,NULL,'FTI SEZ Bagsakan RD FTI Taguig City','004-641-513-000','30 Days','Vat Exempt'),(25,'Zama Precision Industy Mfg Phils. Inc.',NULL,NULL,NULL,'FPIP SEZ 2 Brgy. Sta. Anastacia Sto. Tomas Batangas','008-699-372-000','30 Days','Vat Exempt'),(26,'Techno Moldplas (Phils) Inc.',NULL,NULL,NULL,'Lot C4 7B Tagaytay Bridge drive Carmelray Industrial Park II Brgy Punta Calamba City Laguna','008-598-886-000','30 Days','Zero Rated'),(27,'ShinHeung Electro-Digital Inc.',NULL,NULL,NULL,'Lot 5 Blk 3 CPIP Brgy. batino Calamba Laguna','211-675-728-000','30 Days','Zero Rated'),(28,'Armstrong Weston Asia Inc.',NULL,NULL,NULL,'Bldg. 5B Paronama Compound Lot 3 Blk 10 A Bo. Anastacia Sto. Tomas Batangas',NULL,'30 days','Zero Rated'),(29,'Worth BI Enterprise',NULL,NULL,NULL,'Bel Air Sta. Rosa Laguna','147-522-231-000','30 Days','Zero Rated'),(43,'Nihon Houzai Laguna Corporation',NULL,NULL,NULL,'CPIP Batino Calamba Laguna','231-496-735-000','30 Days','Vat Exempt'),(44,'Angeal Corp','Angeal Del Mundo','09989505579','delmundoangealgabriel@gmail.com','#38 Vasra Village Diliman QC',NULL,NULL,'Vat Exempt');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveryinformation`
--

DROP TABLE IF EXISTS `deliveryinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveryinformation` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sinumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grandtotal` decimal(8,2) NOT NULL,
  `cid` int(11) NOT NULL,
  `taxclassification` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliverydate` date NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveryinformation`
--

LOCK TABLES `deliveryinformation` WRITE;
/*!40000 ALTER TABLE `deliveryinformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveryinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliveryinformationitems`
--

DROP TABLE IF EXISTS `deliveryinformationitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliveryinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `did` int(11) NOT NULL,
  `poid` int(11) NOT NULL,
  `pocusitemid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `currencyid` int(11) NOT NULL,
  `rate` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliveryinformationitems`
--

LOCK TABLES `deliveryinformationitems` WRITE;
/*!40000 ALTER TABLE `deliveryinformationitems` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveryinformationitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverypreparationsinformation`
--

DROP TABLE IF EXISTS `deliverypreparationsinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverypreparationsinformation` (
  `preparationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preparationnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approvedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approveddate` datetime DEFAULT NULL,
  `disapprovedby` text COLLATE utf8mb4_unicode_ci,
  `disapproveddate` datetime DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`preparationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverypreparationsinformation`
--

LOCK TABLES `deliverypreparationsinformation` WRITE;
/*!40000 ALTER TABLE `deliverypreparationsinformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverypreparationsinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverypreparationsinformationitems`
--

DROP TABLE IF EXISTS `deliverypreparationsinformationitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverypreparationsinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preparationid` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poid` int(11) NOT NULL,
  `pocusitemid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverypreparationsinformationitems`
--

LOCK TABLES `deliverypreparationsinformationitems` WRITE;
/*!40000 ALTER TABLE `deliverypreparationsinformationitems` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverypreparationsinformationitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finishgoods`
--

DROP TABLE IF EXISTS `finishgoods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finishgoods` (
  `fgid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partnumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `ID_length` text COLLATE utf8mb4_unicode_ci,
  `ID_width` text COLLATE utf8mb4_unicode_ci,
  `ID_height` text COLLATE utf8mb4_unicode_ci,
  `OD_length` text COLLATE utf8mb4_unicode_ci,
  `OD_width` text COLLATE utf8mb4_unicode_ci,
  `OD_height` text COLLATE utf8mb4_unicode_ci,
  `currencyid` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `stockonhand` int(11) DEFAULT NULL,
  `packingstd` text COLLATE utf8mb4_unicode_ci,
  `cid` int(11) NOT NULL,
  `processcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fgid`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finishgoods`
--

LOCK TABLES `finishgoods` WRITE;
/*!40000 ALTER TABLE `finishgoods` DISABLE KEYS */;
INSERT INTO `finishgoods` VALUES (1,'PAC-52-00','PH989004-3310','TOP SHEET PAD','PH989004-3310 TOP SHEET PAD',NULL,NULL,NULL,'430','260','0',2,4.50,0,'50pcs',4,'B',NULL,NULL,NULL),(2,'PAC-48-00','PH989004-3330','(INNER) SMALL BOX','PH989004-3330 (INNER) SMALL BOX',NULL,NULL,NULL,'182','182','125',2,13.86,10,'10pcs',4,'G',NULL,NULL,'2019-08-27 20:32:43'),(3,'PAC-59-01','PH989004-4280','INNER BOX','PH989004-4280 INNER BOX','605','390','305','609','394','311',2,40.50,0,'10pcs',4,'G',NULL,NULL,NULL),(4,'PAC-69-00','PH989004-4930','INNER BOX','PH989004-4930 INNER BOX',NULL,NULL,NULL,'565','386','165',2,42.57,0,'10pcs',4,'G',NULL,NULL,NULL),(5,'PAC-02-00','PH989246-0070','INNER BOX','PH989246-0070 INNER BOX',NULL,NULL,NULL,'520','440','230',2,54.56,0,'10pcs',4,'D',NULL,NULL,NULL),(6,'PAC-85-00','PH989246-0430','INNER BOX','PH989246-0430 INNER BOX',NULL,NULL,NULL,'175','175','120',2,9.74,0,'10pcs',4,'G',NULL,NULL,NULL),(7,'PAC-110-00','PH989246-0440','INNER BOX','PH989246-0440 INNER BOX','514','434','147','520','440','155',2,52.97,0,'16pcs',4,'D',NULL,NULL,NULL),(8,'PAC-122-00','PH989246-0490','INNER BOX','PH989246-0490 INNER BOX',NULL,NULL,NULL,'631','291','260',2,56.43,0,'10pcs',4,'D',NULL,NULL,NULL),(9,'PAC-99-00','PH989247-0020','INNER BOX','PH989247-0020 INNER BOX',NULL,NULL,NULL,'1050','848','500',2,242.50,0,'5pcs',4,'E',NULL,NULL,NULL),(10,'PAC-111-00','PH989247-0110','MASTER BOX','PH989247-0110 MASTER BOX','1051','891','638','1060','900','650',2,362.29,0,'5pcs',4,'E',NULL,NULL,NULL),(11,'PAC-141-00','PH989247-0150','MASTER BOX','PH989247-0150 MASTER BOX','1125','960','480','1135','968','490',2,311.69,0,'5pcs',4,'E',NULL,NULL,NULL),(12,'PAC-86-00','PH989701-0100','MASTER BOX','PH989701-0100 MASTER BOX','1052','892','468','1060','900','480',2,201.85,0,'5pcs',4,'E',NULL,NULL,NULL),(13,'PAC-138-00','PH989701-0250','T9AA MASTER BOX','PH989701-0250 T9AA MASTER BOX',NULL,NULL,NULL,'1170','880','550',2,195.14,0,'5pcs',4,'E',NULL,NULL,NULL),(14,'PAC-109-00','PH989702-0220','INNER BOX','PH989702-0220 INNER BOX',NULL,NULL,NULL,'435','268','178',2,25.13,0,'10pcs',4,'B',NULL,NULL,NULL),(15,'PAC-105-00','PH989702-0350','INNER BOX','PH989702-0350 INNER BOX',NULL,NULL,NULL,'437','267','85',2,21.29,0,'10pcs',4,'B',NULL,NULL,NULL),(16,'PAC-114-00','PH989702-0420','INNER BOX','PH989702-0420 INNER BOX',NULL,NULL,NULL,'420','151','215',2,25.45,0,'10pcs',4,'C',NULL,NULL,NULL),(17,'PAC-129-00','PH989702-0470','YLA INNER BOX','PH989702-0470 YLA INNER BOX',NULL,NULL,NULL,'552','325','168',2,23.43,0,'10pcs',4,'G',NULL,NULL,NULL),(18,'PAC-126-00','PH989702-0480','INNER CARTON BOX','PH989702-0480 INNER CARTON BOX',NULL,NULL,NULL,'390','245','107',2,17.82,0,'10pcs',4,'B',NULL,NULL,NULL),(19,'PAC-133-00','PH989702-0520','T9AA INNER BOX','PH989702-0520 T9AA INNER BOX',NULL,NULL,NULL,'715','428','258',2,47.52,0,'10pcs',4,'G',NULL,NULL,NULL),(20,'PAC-150-00','PH989702-0541','D87A INNER BOX','PH989702-0541 D87A INNER BOX',NULL,NULL,NULL,'622','284','170',2,27.45,0,'10pcs',4,'G',NULL,NULL,NULL),(21,'PAC-143-00','PH989702-0550','T9AA SPARE INNER BOX','PH989702-0550 T9AA SPARE INNER BOX',NULL,NULL,NULL,'428','285','258',2,40.15,0,'10pcs',4,'D',NULL,NULL,NULL),(22,'PAC-144-01','PH989702-0570','INNER BOX','PH989702-0570 INNER BOX',NULL,NULL,NULL,'357','547','77',2,47.38,0,'10pcs',4,'C',NULL,NULL,NULL),(23,'PAC-160-00','PH989702-0650','HONDA INNER BOX','PH989702-0650 HONDA INNER BOX',NULL,NULL,NULL,'615','410','265',2,51.37,0,'10pcs',4,'G',NULL,NULL,NULL),(24,'PAC-157-00','PH989702-0700','INNER BOX','PH989702-0700 INNER BOX',NULL,NULL,NULL,'42','42','68',2,6.75,0,'10pcs',4,'C',NULL,NULL,NULL),(25,'PAC-165-00','PH989702-0750','HONDA INNER BOX','PH989702-0750 HONDA INNER BOX',NULL,NULL,NULL,'520','440','258',2,51.98,0,'10pcs',4,'G',NULL,NULL,NULL),(26,'PAC-87-00','PH989704-0020','COVER','PH989704-0020 COVER',NULL,NULL,NULL,'1076','916','100',2,64.57,0,'5pcs',4,'F',NULL,NULL,NULL),(27,'PAC-98-00','PH989704-0030','COVER','PH989704-0030 COVER',NULL,NULL,NULL,'1068','866','100',2,81.07,0,'5pcs',4,'F',NULL,NULL,NULL),(28,'PAC-137-00','PH989704-0390','T9AA MASTER BOX COVER','PH989704-0390 T9AA MASTER BOX COVER',NULL,NULL,NULL,'1185','895','100',2,68.59,0,'5pcs',4,'F',NULL,NULL,NULL),(29,'PAC-163-00','PH989705-0330','DANPLA STICK W/ CUSHION','PH989705-0330 DANPLA STICK W/ CUSHION',NULL,NULL,NULL,'422','18','0',2,12.70,0,'30pcs',4,'B',NULL,NULL,NULL),(30,'PAC-08-01','PH989918-0010','SLIP SHEET','PH989918-0010 SLIP SHEET',NULL,NULL,NULL,'1060','1100','0',2,63.74,0,'20pcs',4,'M',NULL,NULL,NULL),(31,'PAC-79-00','PH989933-0340','INSERT','PH989933-0340 INSERT',NULL,NULL,NULL,'200','209','0',2,2.14,0,'10pcs',4,'B',NULL,NULL,NULL),(32,'PAC-102-00','PH989933-0500','PARTITION SET','PH989933-0500 PARTITION SET',NULL,NULL,NULL,'512','434','220',2,24.86,0,'8sets',4,'B',NULL,NULL,NULL),(33,'PAC-123-00','PH989933-0650','INSERT','PH989933-0650 INSERT',NULL,NULL,NULL,'420','280','260',2,28.93,0,'10pcs',4,'B',NULL,NULL,NULL),(34,'PAC-124-00','PH989933-0660','INSERT','PH989933-0660 INSERT',NULL,NULL,NULL,'630','130','260',2,29.54,0,'10pcs',4,'B',NULL,NULL,NULL),(35,'PAC-130-00','PH989933-0710','YLA PARTITION SET','PH989933-0710 YLA PARTITION SET',NULL,NULL,NULL,'544','320','158',2,22.00,0,'10sets',4,'B',NULL,NULL,NULL),(36,'PAC-140-00','PH989933-0720','PARTITION SET','PH989933-0720 PARTITION SET',NULL,NULL,NULL,'700','520','220',2,102.08,0,'10sets',4,'B',NULL,NULL,NULL),(37,'PAC-127-00','PH989933-0730','PARTITION SET','PH989933-0730 PARTITION SET',NULL,NULL,NULL,'380','239-5','98',2,19.44,0,'10sets',4,'B',NULL,NULL,NULL),(38,'PAC-125-00','PH989933-0740','PARTITION SET','PH989933-0740 PARTITION SET',NULL,NULL,NULL,'432','260','156',2,22.00,0,'10sets',4,'B',NULL,NULL,NULL),(39,'PAC-134-00','PH989933-0760','T9AA PARTITION SET','PH989933-0760 T9AA PARTITION SET',NULL,NULL,NULL,'706','424','246',2,42.68,0,'10sets',4,'B',NULL,NULL,NULL),(40,'PAC-135-00','PH989933-0770','T9AA CARTON INSERT','PH989933-0770 T9AA CARTON INSERT',NULL,NULL,NULL,'428','140','258',2,9.85,0,'10pcs',4,'B',NULL,NULL,NULL),(41,'PAC-153-00','PH989933-0831','D87A PARTITION SET','PH989933-0831 D87A PARTITION SET',NULL,NULL,NULL,'614','276','163',2,20.30,0,'10sets',4,'B',NULL,NULL,NULL),(42,'PAC-151-00','PH989933-0841','D87A CARTON TOP INSERT','PH989933-0841 D87A CARTON TOP INSERT',NULL,NULL,NULL,'276','39','10',2,3.14,0,'20pcs',4,'B',NULL,NULL,NULL),(43,'PAC-142-00','PH989933-0850','T9AA SPARE PARTITION','PH989933-0850 T9AA SPARE PARTITION',NULL,NULL,NULL,'420','280','140',2,22.44,0,'1set',4,'B',NULL,NULL,NULL),(44,'PAC-145-01','PH989933-0860','INSERT','PH989933-0860 INSERT',NULL,NULL,NULL,'189','120','50',2,5.89,0,'10pcs',4,'B',NULL,NULL,NULL),(45,'PAC-158-00','PH989933-0920','T9AA PARTITION SET','PH989933-0920 T9AA PARTITION SET',NULL,NULL,NULL,'691','424','246',2,41.94,0,'10sets',4,'B',NULL,NULL,NULL),(46,'PAC-166-00','PH989933-1000','HONDA PARTITION','PH989933-1000 HONDA PARTITION',NULL,NULL,NULL,'510','430','246',2,37.13,0,'10sets',4,'B',NULL,NULL,NULL),(47,'PAC-168-00','PH989933-1040','PARTITION SET','PH989933-1040 PARTITION SET',NULL,NULL,NULL,'610','405','252',2,46.37,0,'10sets',4,'B',NULL,NULL,NULL),(48,'PAC-80-00','PH989938-0130','TOP PAD','PH989938-0130 TOP PAD',NULL,NULL,NULL,'139','130','35',2,2.90,0,'10pcs',4,'B',NULL,NULL,NULL),(49,'PAC-128-00','PH989938-0520','TOP PAD','PH989938-0520 TOP PAD',NULL,NULL,NULL,'340','380','0',2,4.36,0,'50pcs',4,'B',NULL,NULL,NULL),(50,'PAC-136-00','PH989938-0530','T9AA TOP SHEET PAD','PH989938-0530 T9AA TOP SHEET PAD',NULL,NULL,NULL,'1158','872','0',2,23.98,0,'10pcs',4,'I',NULL,NULL,NULL),(51,'PAC-167-00','PH989939-0140','HONDA BOTTOM INSERT','PH989939-0140 HONDA BOTTOM INSERT',NULL,NULL,NULL,'510','67','43',2,5.12,0,'20pcs',4,'B',NULL,NULL,NULL),(52,'PAC-155-00','PH989939-0860','D87A INSERT 1','PH989939-0860 D87A INSERT 1',NULL,NULL,NULL,'450','160','260',2,16.94,0,'10pcs',4,'B',NULL,NULL,NULL),(53,'PAC-154-00','PH989939-0870','D87A INSERT 2','PH989939-0870 D87A INSERT 2',NULL,NULL,NULL,'620','160','160',2,19.09,0,'10pcs',4,'B',NULL,NULL,NULL),(54,'PAC-159-00','PH989939-0940','HONDA PARTITION SET','PH989939-0940 HONDA PARTITION SET',NULL,NULL,NULL,'610','405','252',2,46.37,0,'10sets',4,'B',NULL,NULL,NULL);
/*!40000 ALTER TABLE `finishgoods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finishgoodsaudit`
--

DROP TABLE IF EXISTS `finishgoodsaudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finishgoodsaudit` (
  `auditid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fgid` int(11) NOT NULL,
  `jonumber` text COLLATE utf8mb4_unicode_ci,
  `drnumber` text COLLATE utf8mb4_unicode_ci,
  `qty` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `issueddate` date NOT NULL,
  `issuedtime` time NOT NULL,
  PRIMARY KEY (`auditid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finishgoodsaudit`
--

LOCK TABLES `finishgoodsaudit` WRITE;
/*!40000 ALTER TABLE `finishgoodsaudit` DISABLE KEYS */;
INSERT INTO `finishgoodsaudit` VALUES (1,1,'2019020001',NULL,50,550,'Del Mundo, Angelo Gabriel B',NULL,'2019-02-24','11:22:55'),(2,1,NULL,'123',99,451,'Del Mundo, Angelo Gabriel B',NULL,'2019-03-09','21:34:59'),(3,1,NULL,'123',100,351,'Del Mundo, Angelo Gabriel B',NULL,'2019-03-09','21:34:59'),(4,1,NULL,'123',99,252,'Del Mundo, Angelo Gabriel B',NULL,'2019-03-09','21:39:31'),(5,1,NULL,'123',100,152,'Del Mundo, Angelo Gabriel B',NULL,'2019-03-09','21:39:31'),(6,2,'JO201907180002',NULL,10,10,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-27','13:32:43');
/*!40000 ALTER TABLE `finishgoodsaudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoiceinformation`
--

DROP TABLE IF EXISTS `invoiceinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoiceinformation` (
  `invoiceid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoicenumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`invoiceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoiceinformation`
--

LOCK TABLES `invoiceinformation` WRITE;
/*!40000 ALTER TABLE `invoiceinformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoiceinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobordermonitoring`
--

DROP TABLE IF EXISTS `jobordermonitoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobordermonitoring` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `currentpid` int(11) NOT NULL DEFAULT '0',
  `operator` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobordermonitoring`
--

LOCK TABLES `jobordermonitoring` WRITE;
/*!40000 ALTER TABLE `jobordermonitoring` DISABLE KEYS */;
INSERT INTO `jobordermonitoring` VALUES (1,1,1,'Del Mundo, Angelo Gabriel B','Open','2019-08-21 19:59:32',NULL);
/*!40000 ALTER TABLE `jobordermonitoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobordermonitoringaudit`
--

DROP TABLE IF EXISTS `jobordermonitoringaudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobordermonitoringaudit` (
  `auditid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `targetoutput` int(11) NOT NULL DEFAULT '0',
  `balance` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isshow` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`auditid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobordermonitoringaudit`
--

LOCK TABLES `jobordermonitoringaudit` WRITE;
/*!40000 ALTER TABLE `jobordermonitoringaudit` DISABLE KEYS */;
INSERT INTO `jobordermonitoringaudit` VALUES (1,1,1,50,50,'2019-08-21 19:59:32',NULL,1),(2,1,3,0,0,'2019-08-21 19:59:32',NULL,0),(3,1,8,0,0,'2019-08-21 19:59:32',NULL,0),(4,1,9,0,0,'2019-08-21 19:59:32',NULL,0);
/*!40000 ALTER TABLE `jobordermonitoringaudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobordermonitoringaudititems`
--

DROP TABLE IF EXISTS `jobordermonitoringaudititems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobordermonitoringaudititems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auditid` int(11) NOT NULL,
  `operator` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `rejectqty` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobordermonitoringaudititems`
--

LOCK TABLES `jobordermonitoringaudititems` WRITE;
/*!40000 ALTER TABLE `jobordermonitoringaudititems` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobordermonitoringaudititems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joborderrminfo`
--

DROP TABLE IF EXISTS `joborderrminfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborderrminfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `rmid` int(11) NOT NULL,
  `rmstockonhand` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborderrminfo`
--

LOCK TABLES `joborderrminfo` WRITE;
/*!40000 ALTER TABLE `joborderrminfo` DISABLE KEYS */;
INSERT INTO `joborderrminfo` VALUES (1,1,2,280,'2019-07-19 03:46:31',NULL),(2,1,3,40,'2019-07-19 03:46:31',NULL),(7,6,1,41,'2019-08-27 00:14:02',NULL);
/*!40000 ALTER TABLE `joborderrminfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joborders`
--

DROP TABLE IF EXISTS `joborders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborders` (
  `joid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jorequestnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jonumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `requestid` int(11) NOT NULL,
  `rmstock` int(11) NOT NULL,
  `style` text COLLATE utf8mb4_unicode_ci,
  `inkcolor` text COLLATE utf8mb4_unicode_ci,
  `specialinstruction` text COLLATE utf8mb4_unicode_ci,
  `targetoutput` int(11) NOT NULL,
  `qtyreceive` int(11) NOT NULL DEFAULT '0',
  `deliverydatefrom` date NOT NULL,
  `deliverydateto` date NOT NULL,
  `cid` int(11) NOT NULL,
  `requestby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `requestdate` datetime DEFAULT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`joid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborders`
--

LOCK TABLES `joborders` WRITE;
/*!40000 ALTER TABLE `joborders` DISABLE KEYS */;
INSERT INTO `joborders` VALUES (1,'JOR258122923','JO201907180002',1,1,4,0,'None','None','SAMP',50,10,'2019-07-01','2019-07-31',4,'Del Mundo, Angelo Gabriel B','2019-07-18 20:46:31','Del Mundo, Angelo Gabriel B','Open','2019-07-19 03:46:31','2019-08-27 20:32:43'),(6,'JOR120883282',NULL,1,1,7,0,NULL,NULL,NULL,50,0,'0000-00-00','0000-00-00',4,'Del Mundo, Angelo Gabriel B','2019-08-26 17:14:02',NULL,'For Request','2019-08-27 00:14:02',NULL);
/*!40000 ALTER TABLE `joborders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joborderssoinfo`
--

DROP TABLE IF EXISTS `joborderssoinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `joborderssoinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `delid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joborderssoinfo`
--

LOCK TABLES `joborderssoinfo` WRITE;
/*!40000 ALTER TABLE `joborderssoinfo` DISABLE KEYS */;
INSERT INTO `joborderssoinfo` VALUES (1,1,1),(2,6,3);
/*!40000 ALTER TABLE `joborderssoinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materialrequest`
--

DROP TABLE IF EXISTS `materialrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materialrequest` (
  `requestid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mrinumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issuancenumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rmissuedby` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`requestid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materialrequest`
--

LOCK TABLES `materialrequest` WRITE;
/*!40000 ALTER TABLE `materialrequest` DISABLE KEYS */;
INSERT INTO `materialrequest` VALUES (1,'MR201907160001',NULL,4,2,'SAMPLE','Del Mundo, Angelo Gabriel B',NULL,'Disapproved','2019-07-17 05:22:29','2019-08-21 18:51:26'),(2,'MR201907160002',NULL,4,3,'SAMP','Del Mundo, Angelo Gabriel B',NULL,'Pending','2019-07-17 05:24:28',NULL),(3,'MR201907180003',NULL,4,2,'SAMP LOADING','Del Mundo, Angelo Gabriel B',NULL,'Pending','2019-07-19 03:42:16',NULL),(4,'MR201907180004','ISN201907180845',4,2,'SAMPLE','Del Mundo, Angelo Gabriel B','Del Mundo, Angelo Gabriel B','Done','2019-07-19 03:45:35','2019-07-19 03:46:31'),(5,'MR201908200005','ISN201908211239',4,3,'SAMPLE','Del Mundo, Angelo Gabriel B','Del Mundo, Angelo Gabriel B','JO Creation','2019-08-21 00:56:02','2019-08-21 19:39:12'),(6,'MR201908210006',NULL,4,2,'1','Del Mundo, Angelo Gabriel B',NULL,'Disapproved','2019-08-21 18:33:24','2019-08-21 18:51:53'),(7,'MR201908260007','ISN201908260504',4,2,'FOR ME','Del Mundo, Angelo Gabriel B','Del Mundo, Angelo Gabriel B','Done','2019-08-27 00:02:57','2019-08-27 00:14:02');
/*!40000 ALTER TABLE `materialrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materialrequestitems`
--

DROP TABLE IF EXISTS `materialrequestitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materialrequestitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mrid` int(11) DEFAULT NULL,
  `drnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rmid` int(11) NOT NULL,
  `qtyrequired` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `qtyissued` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materialrequestitems`
--

LOCK TABLES `materialrequestitems` WRITE;
/*!40000 ALTER TABLE `materialrequestitems` DISABLE KEYS */;
INSERT INTO `materialrequestitems` VALUES (1,1,NULL,2,10,'SAMP',NULL,NULL,'2019-07-17 05:22:29',NULL),(2,1,NULL,3,20,'SAMMP',NULL,NULL,'2019-07-17 05:22:29',NULL),(3,2,NULL,2,10,'S',NULL,NULL,'2019-07-17 05:24:28',NULL),(4,2,NULL,3,10,'S',NULL,NULL,'2019-07-17 05:24:28',NULL),(5,3,NULL,3,10,'SSS',NULL,NULL,'2019-07-19 03:42:16',NULL),(6,4,'1231',2,10,'SAMP',10,280,'2019-07-19 03:45:35','2019-07-19 03:46:07'),(7,4,'2222',3,10,'SAMP',10,40,'2019-07-19 03:45:36','2019-07-19 03:46:08'),(8,5,'asdasd',2,1,'SAMPLE',1,0,'2019-08-21 00:56:02','2019-08-21 19:39:12'),(9,6,NULL,2,2,'2',NULL,NULL,'2019-08-21 18:33:24',NULL),(10,6,NULL,37,2,'2',NULL,NULL,'2019-08-21 18:33:25',NULL),(11,6,NULL,43,2,'2',NULL,NULL,'2019-08-21 18:33:25',NULL),(12,7,'12312',1,20,'CUT 5',20,41,'2019-08-27 00:02:58','2019-08-27 00:04:25');
/*!40000 ALTER TABLE `materialrequestitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materialrequestitemsexcess`
--

DROP TABLE IF EXISTS `materialrequestitemsexcess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materialrequestitemsexcess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mritemid` int(11) NOT NULL,
  `excessrmid` int(11) NOT NULL,
  `qtyexcessboard` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materialrequestitemsexcess`
--

LOCK TABLES `materialrequestitemsexcess` WRITE;
/*!40000 ALTER TABLE `materialrequestitemsexcess` DISABLE KEYS */;
INSERT INTO `materialrequestitemsexcess` VALUES (1,2,4,10,'2019-07-17 05:22:29',NULL),(2,3,4,10,'2019-07-17 05:24:28',NULL),(3,4,1,10,'2019-07-17 05:24:28',NULL),(4,5,1,10,'2019-07-19 03:42:16',NULL),(5,9,9,1,'2019-08-21 18:33:25',NULL),(6,10,10,1,'2019-08-21 18:33:25',NULL);
/*!40000 ALTER TABLE `materialrequestitemsexcess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (4,'2018_07_11_113230_suppliers',1),(5,'2018_07_11_113657_customers',1),(6,'2018_07_12_065828_currency',1),(15,'2018_07_14_191651_fginventory',4),(27,'2018_07_23_233654_poinformation',6),(28,'2018_07_23_234325_poinformationitems',6),(29,'2018_07_28_194750_rawmaterialaudit',7),(30,'2018_07_19_212128_rawmaterials',8),(31,'2018_08_08_050804_joborders',9),(32,'2018_08_11_044153_materialrequest',9),(33,'2018_08_18_174059_purchaserequest',10),(34,'2018_08_25_051555_pocustomerinformation',11),(36,'2018_08_25_052640_pocustomerinformationitems',12),(37,'2018_09_02_010729_finishgoodsaudit',13),(38,'2018_09_02_025251_qajoinformation',14),(39,'2018_09_02_025348_qajoinformationitems',14),(40,'2018_09_02_033514_qaitemdimension',14),(41,'2018_09_02_045107_qaitemvisual',14),(42,'2018_10_18_033710_deliveryinformation',15),(43,'2018_10_18_034239_deliveryinformationitems',15),(44,'2018_07_14_223953_processcode',16),(45,'2018_11_24_070447_jobordermonitoring',17),(46,'2018_11_24_073202_jobordermonitoringaudit',17),(47,'2018_12_29_100838_invoiceinformation',18),(48,'2019_01_06_045939_settings',18),(49,'2019_02_02_020302_jobordermonitoringaudititems',19),(50,'2019_02_09_012349_usersrestrictions',20),(51,'2019_02_25_015540_proceestyle',21),(52,'2019_03_09_114215_usergroups',22),(53,'2019_03_09_114517_useraccess',22),(54,'2019_03_09_114719_access',22),(55,'2019_04_12_120602_pocustomerinformationitemdelivery',23),(56,'2019_04_15_112613_deliverypreparationinformation',24),(57,'2019_04_15_112642_deliverypreparationinformationitems',24),(58,'2019_05_12_180413_papercombination',25),(59,'2019_05_12_181101_papercombinationprice',25),(61,'2019_07_06_231240_settingseditaccess',27),(62,'2019_07_07_004024_pocustomerinformationitemdeliverydeleted',28),(63,'2019_07_06_225716_materialrequestitems',29),(64,'2019_07_14_050936_joborderrminfo',30),(65,'2019_07_15_225228_materialrequestitemsexcess',31),(66,'2019_07_27_042129_purchaserequestitems',32),(67,'2019_07_27_042244_purchaserequestitemsdelivery',32),(68,'2019_07_30_013257_poinformationitemsdelivery',33),(69,'2019_08_27_003516_qarejecttype',34);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `papercombination`
--

DROP TABLE IF EXISTS `papercombination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `papercombination` (
  `papercomid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rmpapercombination` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flute` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `boardpound` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`papercomid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `papercombination`
--

LOCK TABLES `papercombination` WRITE;
/*!40000 ALTER TABLE `papercombination` DISABLE KEYS */;
INSERT INTO `papercombination` VALUES (1,'TL140 x CM125 x TL140','B','200','2019-05-12 20:13:51','2019-05-14 03:10:50'),(2,'TL140 x CM125 x TL140','C','150','2019-05-14 01:45:39','2019-05-14 03:10:48');
/*!40000 ALTER TABLE `papercombination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `papercombinationprice`
--

DROP TABLE IF EXISTS `papercombinationprice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `papercombinationprice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `papercomid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `papercombinationprice`
--

LOCK TABLES `papercombinationprice` WRITE;
/*!40000 ALTER TABLE `papercombinationprice` DISABLE KEYS */;
INSERT INTO `papercombinationprice` VALUES (1,1,23,10.00,'2019-05-17 04:45:56',NULL,'2019-05-17 04:45:56'),(2,1,23,20.00,'2019-05-17 04:45:56',NULL,'2019-05-17 04:51:11'),(3,1,23,30.00,'2019-05-17 04:51:11',NULL,'2019-05-17 04:56:58'),(4,1,23,20.00,'2019-05-17 04:56:58',NULL,'2019-06-07 20:26:31'),(5,1,23,20.00,'2019-06-07 20:26:31',NULL,NULL),(6,1,19,20.00,'2019-06-28 04:07:30',NULL,'2019-06-28 04:10:16'),(7,1,19,20.00,'2019-06-28 04:10:16',NULL,NULL);
/*!40000 ALTER TABLE `papercombinationprice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pocustomerinformation`
--

DROP TABLE IF EXISTS `pocustomerinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pocustomerinformation` (
  `poid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ponumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `grandtotal` decimal(10,2) NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `deliverydate` date DEFAULT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_openpo` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`poid`),
  UNIQUE KEY `pocustomerinformation_ponumber_unique` (`ponumber`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pocustomerinformation`
--

LOCK TABLES `pocustomerinformation` WRITE;
/*!40000 ALTER TABLE `pocustomerinformation` DISABLE KEYS */;
INSERT INTO `pocustomerinformation` VALUES (1,'13213',4,1386.00,NULL,NULL,'Del Mundo, Angelo Gabriel B','Open',1,'2019-07-19 03:43:59',NULL);
/*!40000 ALTER TABLE `pocustomerinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pocustomerinformationitemdelivery`
--

DROP TABLE IF EXISTS `pocustomerinformationitemdelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pocustomerinformationitemdelivery` (
  `delid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salesnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemid` int(11) NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `deliverydate` date NOT NULL,
  `prid` int(11) DEFAULT '0',
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`delid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pocustomerinformationitemdelivery`
--

LOCK TABLES `pocustomerinformationitemdelivery` WRITE;
/*!40000 ALTER TABLE `pocustomerinformationitemdelivery` DISABLE KEYS */;
INSERT INTO `pocustomerinformationitemdelivery` VALUES (1,'SO201907180001',1,50,'2019-07-31',1,'Close','2019-07-19 03:44:16','2019-07-24 00:52:27',NULL),(2,'SO201907180002',1,50,'2019-07-31',55,'Close','2019-07-19 03:44:28','2019-08-08 01:18:09',NULL),(3,'SO201908260003',1,50,'2019-08-31',0,'Open','2019-08-27 00:13:33',NULL,NULL);
/*!40000 ALTER TABLE `pocustomerinformationitemdelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pocustomerinformationitemdeliverydeleted`
--

DROP TABLE IF EXISTS `pocustomerinformationitemdeliverydeleted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pocustomerinformationitemdeliverydeleted` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delid` int(11) NOT NULL,
  `reason` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `authuid` int(11) NOT NULL,
  `authname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deluid` int(11) NOT NULL,
  `delname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pocustomerinformationitemdeliverydeleted`
--

LOCK TABLES `pocustomerinformationitemdeliverydeleted` WRITE;
/*!40000 ALTER TABLE `pocustomerinformationitemdeliverydeleted` DISABLE KEYS */;
/*!40000 ALTER TABLE `pocustomerinformationitemdeliverydeleted` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pocustomerinformationitems`
--

DROP TABLE IF EXISTS `pocustomerinformationitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pocustomerinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `currencyid` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `delqty` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pocustomerinformationitems`
--

LOCK TABLES `pocustomerinformationitems` WRITE;
/*!40000 ALTER TABLE `pocustomerinformationitems` DISABLE KEYS */;
INSERT INTO `pocustomerinformationitems` VALUES (1,1,2,2,1.00,13.86,200,1386.00,0);
/*!40000 ALTER TABLE `pocustomerinformationitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poinformation`
--

DROP TABLE IF EXISTS `poinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poinformation` (
  `poid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ponumber` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attentionto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `grandtotal` decimal(10,2) NOT NULL,
  `deliverycharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sid` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `iuid` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `auid` int(11) NOT NULL DEFAULT '0',
  `approvedby` text COLLATE utf8mb4_unicode_ci,
  `approveddate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`poid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poinformation`
--

LOCK TABLES `poinformation` WRITE;
/*!40000 ALTER TABLE `poinformation` DISABLE KEYS */;
INSERT INTO `poinformation` VALUES (8,'PO201907290008','Gelo','Gelo',5447.20,0.00,23,'Open','SAMPLE NOTE',6,'Del Mundo, Angelo Gabriel B',0,'',NULL,'2019-07-30 03:15:35',NULL),(9,'PO201908070009','GELO','GELO',3706.00,0.00,23,'Pending','GELO',6,'Del Mundo, Angelo Gabriel B',6,'Del Mundo, Angelo Gabriel B','2019-08-17 11:51:34','2019-08-08 01:25:53',NULL),(10,'PO201908210010','GELO','GELO',87060.00,0.00,23,'Open','GELO',6,'Del Mundo, Angelo Gabriel B',6,'Del Mundo, Angelo Gabriel B','2019-08-21 12:18:48','2019-08-21 19:17:46',NULL),(11,'PO201908260011','GELO','G',5500.00,0.00,23,'Open','GELO',6,'Del Mundo, Angelo Gabriel B',6,'Del Mundo, Angelo Gabriel B','2019-08-26 11:09:41','2019-08-26 18:09:18',NULL);
/*!40000 ALTER TABLE `poinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poinformationitems`
--

DROP TABLE IF EXISTS `poinformationitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poid` int(11) NOT NULL,
  `pritemid` int(11) DEFAULT NULL,
  `rmid` int(11) NOT NULL,
  `papercomid` int(11) DEFAULT '0',
  `currencyid` int(11) NOT NULL,
  `rate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `delqty` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poinformationitems`
--

LOCK TABLES `poinformationitems` WRITE;
/*!40000 ALTER TABLE `poinformationitems` DISABLE KEYS */;
INSERT INTO `poinformationitems` VALUES (14,8,64,3,0,2,'1.00',120,37.06,4447.20,26),(15,8,65,4,0,2,'1.00',20,50.00,1000.00,20),(16,9,66,3,0,2,'1.00',100,37.06,3706.00,0),(17,10,67,3,0,2,'1.00',1000,37.06,37060.00,0),(18,10,68,4,0,2,'1.00',1000,50.00,50000.00,0),(19,11,69,1,0,2,'1.00',100,55.00,5500.00,60);
/*!40000 ALTER TABLE `poinformationitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poinformationitemsaudit`
--

DROP TABLE IF EXISTS `poinformationitemsaudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poinformationitemsaudit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `drnumber` int(11) NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `rejectqty` int(11) NOT NULL,
  `goodqty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poinformationitemsaudit`
--

LOCK TABLES `poinformationitemsaudit` WRITE;
/*!40000 ALTER TABLE `poinformationitemsaudit` DISABLE KEYS */;
INSERT INTO `poinformationitemsaudit` VALUES (1,14,61236,2,0,2,'2019-08-17 17:39:05',NULL),(2,15,61236,2,0,2,'2019-08-17 17:39:05',NULL),(3,14,12121,1,0,1,'2019-08-17 17:39:39',NULL),(4,15,12121,1,0,1,'2019-08-17 17:39:39',NULL),(5,19,756767,10,5,5,'2019-08-26 18:13:20',NULL),(6,19,54556,10,5,5,'2019-08-26 18:15:54',NULL),(7,14,56465,20,5,15,'2019-08-26 18:40:36',NULL),(8,15,56465,17,0,17,'2019-08-26 18:40:36',NULL),(9,19,123123,50,0,50,'2019-08-27 00:03:50',NULL);
/*!40000 ALTER TABLE `poinformationitemsaudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poinformationitemsdelivery`
--

DROP TABLE IF EXISTS `poinformationitemsdelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poinformationitemsdelivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `deliverydate` date NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poinformationitemsdelivery`
--

LOCK TABLES `poinformationitemsdelivery` WRITE;
/*!40000 ALTER TABLE `poinformationitemsdelivery` DISABLE KEYS */;
INSERT INTO `poinformationitemsdelivery` VALUES (3,14,'2019-07-30',100,'2019-07-30 03:15:35',NULL),(4,15,'2019-07-31',30,'2019-07-30 03:15:35',NULL),(5,14,'2019-07-29',20,'2019-07-30 03:32:09',NULL),(6,16,'2019-08-21',100,'2019-08-08 01:25:53',NULL),(7,19,'2019-08-29',100,'2019-08-26 18:09:19',NULL);
/*!40000 ALTER TABLE `poinformationitemsdelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process`
--

DROP TABLE IF EXISTS `process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `process` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process`
--

LOCK TABLES `process` WRITE;
/*!40000 ALTER TABLE `process` DISABLE KEYS */;
INSERT INTO `process` VALUES (1,'Creasing','2018-10-23 22:14:04',NULL),(2,'Printing','2018-10-23 22:14:17',NULL),(3,'Slotting','2018-10-23 22:14:27',NULL),(4,'Stitching','2018-10-23 22:14:37',NULL),(5,'Die Cutting','2018-10-23 22:15:02',NULL),(6,'Eyeletting','2018-10-23 22:15:14',NULL),(7,'Assembly','2018-10-23 22:15:21',NULL),(8,'Production Inspection','2018-10-23 22:15:33',NULL),(9,'Bundling Packing','2018-10-23 22:15:46',NULL);
/*!40000 ALTER TABLE `process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processcode`
--

DROP TABLE IF EXISTS `processcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `processcode` varchar(10) NOT NULL,
  `processorder` int(11) NOT NULL,
  `processid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processcode`
--

LOCK TABLES `processcode` WRITE;
/*!40000 ALTER TABLE `processcode` DISABLE KEYS */;
INSERT INTO `processcode` VALUES (1,'A',1,1),(2,'A',2,8),(3,'A',3,9),(4,'B',1,5),(5,'B',2,8),(6,'B',3,9),(7,'C',1,2),(8,'C',2,5),(9,'C',3,8),(10,'C',4,9),(11,'D',1,1),(12,'D',2,5),(13,'D',3,8),(14,'D',4,9),(15,'E',1,1),(16,'E',2,5),(17,'E',3,7),(18,'E',4,8),(19,'E',5,9),(20,'F',1,1),(21,'F',2,7),(22,'F',3,8),(23,'F',4,9),(24,'G',1,1),(25,'G',2,3),(26,'G',3,8),(27,'G',4,9),(28,'H',1,1),(29,'H',2,3),(30,'H',3,7),(31,'H',4,8),(32,'H',5,9),(33,'I',1,1),(34,'I',2,2),(35,'I',3,7),(36,'I',4,8),(37,'I',5,9),(38,'J',1,1),(45,'K',1,1),(46,'K',2,2),(47,'K',3,5),(48,'K',4,7),(49,'K',5,8),(50,'K',6,9),(51,'J',2,2),(52,'J',3,5),(54,'J',4,8),(55,'J',5,9),(56,'L',1,1),(57,'L',2,2),(58,'L',3,3),(59,'L',4,7),(60,'L',5,8),(61,'L',6,7),(66,'M',1,1),(67,'M',2,3);
/*!40000 ALTER TABLE `processcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processstyle`
--

DROP TABLE IF EXISTS `processstyle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processstyle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `processcode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `style` text COLLATE utf8mb4_unicode_ci,
  `inkcolor` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processstyle`
--

LOCK TABLES `processstyle` WRITE;
/*!40000 ALTER TABLE `processstyle` DISABLE KEYS */;
INSERT INTO `processstyle` VALUES (1,'M','RSC','Black'),(2,'L','DIECUT','White');
/*!40000 ALTER TABLE `processstyle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaserequest`
--

DROP TABLE IF EXISTS `purchaserequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchaserequest` (
  `prid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rmid` int(11) NOT NULL DEFAULT '0',
  `qtyrequired` int(11) NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prid`),
  UNIQUE KEY `purchaserequest_prnumber_unique` (`prnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaserequest`
--

LOCK TABLES `purchaserequest` WRITE;
/*!40000 ALTER TABLE `purchaserequest` DISABLE KEYS */;
INSERT INTO `purchaserequest` VALUES (54,'PR201907270054',0,0,'ssss','Del Mundo, Angelo Gabriel B','Done','2019-07-27 19:38:27','2019-07-30 03:15:35'),(55,'PR201908070055',0,0,'SAMPLE','Del Mundo, Angelo Gabriel B','Done','2019-08-08 01:17:44','2019-08-08 01:25:53'),(56,'PR201908210056',0,0,'SAMPLE','Del Mundo, Angelo Gabriel B','Done','2019-08-21 19:16:30','2019-08-21 19:17:47'),(57,'PR201908260057',0,0,'SAMPLE','Del Mundo, Angelo Gabriel B','Done','2019-08-26 18:08:31','2019-08-26 18:09:19');
/*!40000 ALTER TABLE `purchaserequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaserequestitems`
--

DROP TABLE IF EXISTS `purchaserequestitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchaserequestitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prid` int(11) DEFAULT NULL,
  `rmid` int(11) NOT NULL,
  `qtyrequired` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaserequestitems`
--

LOCK TABLES `purchaserequestitems` WRITE;
/*!40000 ALTER TABLE `purchaserequestitems` DISABLE KEYS */;
INSERT INTO `purchaserequestitems` VALUES (64,54,3,30,'2019-07-27 19:38:54',NULL),(65,54,4,20,'2019-07-27 19:38:54',NULL),(66,55,3,100,'2019-08-08 01:17:59',NULL),(67,56,3,1000,'2019-08-21 19:16:57',NULL),(68,56,4,1000,'2019-08-21 19:16:57',NULL),(69,57,1,100,'2019-08-26 18:08:44',NULL);
/*!40000 ALTER TABLE `purchaserequestitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaserequestitemsdelivery`
--

DROP TABLE IF EXISTS `purchaserequestitemsdelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchaserequestitemsdelivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `deliverydate` date NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaserequestitemsdelivery`
--

LOCK TABLES `purchaserequestitemsdelivery` WRITE;
/*!40000 ALTER TABLE `purchaserequestitemsdelivery` DISABLE KEYS */;
INSERT INTO `purchaserequestitemsdelivery` VALUES (12,65,'2019-07-31',30,'2019-07-27 19:39:05',NULL),(13,64,'2019-07-30',100,'2019-07-27 19:39:16',NULL),(14,66,'2019-08-21',100,'2019-08-08 01:18:06',NULL),(15,69,'2019-08-29',100,'2019-08-26 18:08:50',NULL);
/*!40000 ALTER TABLE `purchaserequestitemsdelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qaitemdimension`
--

DROP TABLE IF EXISTS `qaitemdimension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qaitemdimension` (
  `dimid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`dimid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qaitemdimension`
--

LOCK TABLES `qaitemdimension` WRITE;
/*!40000 ALTER TABLE `qaitemdimension` DISABLE KEYS */;
/*!40000 ALTER TABLE `qaitemdimension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qaitemvisual`
--

DROP TABLE IF EXISTS `qaitemvisual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qaitemvisual` (
  `visualid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `v15` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v12` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v13` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v8` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v17` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v18` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v21` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v19` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `v1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `others` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`visualid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qaitemvisual`
--

LOCK TABLES `qaitemvisual` WRITE;
/*!40000 ALTER TABLE `qaitemvisual` DISABLE KEYS */;
/*!40000 ALTER TABLE `qaitemvisual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qajoinformation`
--

DROP TABLE IF EXISTS `qajoinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qajoinformation` (
  `qaid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `widthtolerancep` int(11) NOT NULL,
  `widthtolerancen` int(11) NOT NULL,
  `lengthtolerancep` int(11) NOT NULL,
  `lengthtolerancen` int(11) NOT NULL,
  `heighttolerancep` int(11) NOT NULL,
  `heighttolerancen` int(11) NOT NULL,
  `insideoutside` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`qaid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qajoinformation`
--

LOCK TABLES `qajoinformation` WRITE;
/*!40000 ALTER TABLE `qajoinformation` DISABLE KEYS */;
INSERT INTO `qajoinformation` VALUES (1,1,1,1,1,1,1,1,'Inside',4,'Del Mundo, Angelo Gabriel B','2019-08-21 20:00:27',NULL);
/*!40000 ALTER TABLE `qajoinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qajoinformationitems`
--

DROP TABLE IF EXISTS `qajoinformationitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qajoinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qaid` int(11) NOT NULL,
  `mstnumber` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `qtyreject` int(11) NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `issueddate` date NOT NULL,
  `issuedtime` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qajoinformationitems`
--

LOCK TABLES `qajoinformationitems` WRITE;
/*!40000 ALTER TABLE `qajoinformationitems` DISABLE KEYS */;
INSERT INTO `qajoinformationitems` VALUES (1,1,'2019080001',10,0,'Done','Del Mundo, Angelo Gabriel B','2019-08-27','13:14:24');
/*!40000 ALTER TABLE `qajoinformationitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qarejecttype`
--

DROP TABLE IF EXISTS `qarejecttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qarejecttype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qarejecttype`
--

LOCK TABLES `qarejecttype` WRITE;
/*!40000 ALTER TABLE `qarejecttype` DISABLE KEYS */;
INSERT INTO `qarejecttype` VALUES (1,'SAMPLE',6,'2019-08-27 01:39:47','2019-08-27 01:59:11');
/*!40000 ALTER TABLE `qarejecttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rawmaterialaudit`
--

DROP TABLE IF EXISTS `rawmaterialaudit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rawmaterialaudit` (
  `auditid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rmid` int(11) NOT NULL,
  `ponumber` text COLLATE utf8mb4_unicode_ci,
  `drnumber` text COLLATE utf8mb4_unicode_ci,
  `requisitionnumber` text COLLATE utf8mb4_unicode_ci,
  `qty` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `issueddate` date NOT NULL,
  `issuedtime` time NOT NULL,
  PRIMARY KEY (`auditid`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rawmaterialaudit`
--

LOCK TABLES `rawmaterialaudit` WRITE;
/*!40000 ALTER TABLE `rawmaterialaudit` DISABLE KEYS */;
INSERT INTO `rawmaterialaudit` VALUES (1,2,'PO201905100001','1',NULL,100,100,'Del Mundo, Angelo Gabriel B','','2019-05-10','21:57:16'),(2,2,'PO201905100001','2',NULL,110,210,'Del Mundo, Angelo Gabriel B','','2019-05-10','21:58:11'),(3,2,'PO201905100001','3',NULL,110,320,'Del Mundo, Angelo Gabriel B','','2019-05-10','22:02:55'),(4,3,'PO201905110002','9210063',NULL,100,100,'Del Mundo, Angelo Gabriel B','','2019-05-11','11:01:52'),(5,3,NULL,NULL,NULL,100,0,'Del Mundo, Angelo Gabriel B','','2019-05-11','11:02:28'),(6,3,'PO201905110002','1231',NULL,20,20,'Del Mundo, Angelo Gabriel B','','2019-05-11','11:04:50'),(7,3,NULL,NULL,NULL,10,10,'Del Mundo, Angelo Gabriel B','','2019-05-11','13:13:01'),(8,3,NULL,NULL,NULL,100,100,'Del Mundo, Angelo Gabriel B',NULL,'2019-06-23','10:15:52'),(9,3,NULL,NULL,NULL,50,50,'Del Mundo, Angelo Gabriel B',NULL,'2019-06-23','10:16:11'),(10,1,NULL,NULL,'MR201907110012',10,310,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-11','17:48:44'),(11,2,NULL,NULL,'MR201907110012',10,300,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-11','17:53:43'),(12,2,'PO201907110001','ghh',NULL,10,310,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-11','17:55:18'),(13,1,NULL,NULL,NULL,500,500,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-11','18:52:24'),(14,1,NULL,NULL,'MR201907110019',9,491,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-11','18:53:13'),(15,2,NULL,NULL,'MR201907110019',10,300,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-11','18:53:13'),(16,2,NULL,NULL,'MR201907140020',10,290,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-14','15:42:13'),(17,1,NULL,NULL,'MR201907140020',10,481,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-14','15:42:14'),(18,2,NULL,NULL,'MR201907180004',10,280,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-18','20:46:08'),(19,3,NULL,NULL,'MR201907180004',10,40,'Del Mundo, Angelo Gabriel B',NULL,'2019-07-18','20:46:08'),(20,3,'PO201907290008','61236',NULL,2,2,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:30:22'),(21,3,'PO201907290008','61236',NULL,2,4,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:30:27'),(22,3,'PO201907290008','61236',NULL,2,6,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:36:58'),(23,3,'PO201907290008','61236',NULL,2,8,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:37:02'),(24,3,'PO201907290008','61236',NULL,2,10,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:39:05'),(25,4,'PO201907290008','61236',NULL,2,2,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:39:05'),(26,3,'PO201907290008','12121',NULL,1,11,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:39:39'),(27,4,'PO201907290008','12121',NULL,1,3,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-17','10:39:39'),(28,2,NULL,NULL,NULL,1,1,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-21','12:39:00'),(29,2,NULL,NULL,'MR201908200005',1,0,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-21','12:39:12'),(30,1,NULL,NULL,NULL,1,1,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','11:06:17'),(31,1,'PO201908260011','756767',NULL,10,6,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','11:13:20'),(32,1,'PO201908260011','54556',NULL,5,11,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','11:15:54'),(33,3,'PO201907290008','56465',NULL,15,26,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','11:40:36'),(34,4,'PO201907290008','56465',NULL,17,20,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','11:40:36'),(35,1,'PO201908260011','123123',NULL,50,61,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','17:03:50'),(36,1,NULL,NULL,'MR201908260007',20,41,'Del Mundo, Angelo Gabriel B',NULL,'2019-08-26','17:04:25');
/*!40000 ALTER TABLE `rawmaterialaudit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rawmaterials`
--

DROP TABLE IF EXISTS `rawmaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rawmaterials` (
  `rmid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `width` text COLLATE utf8mb4_unicode_ci,
  `widthsize` text COLLATE utf8mb4_unicode_ci,
  `length` text COLLATE utf8mb4_unicode_ci,
  `lengthsize` text COLLATE utf8mb4_unicode_ci,
  `flute` text COLLATE utf8mb4_unicode_ci,
  `boardpound` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(10,2) DEFAULT NULL,
  `stockonhand` int(11) NOT NULL DEFAULT '0',
  `currencyid` int(11) DEFAULT NULL,
  `category` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rmid`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rawmaterials`
--

LOCK TABLES `rawmaterials` WRITE;
/*!40000 ALTER TABLE `rawmaterials` DISABLE KEYS */;
INSERT INTO `rawmaterials` VALUES (1,NULL,'100 x 100',NULL,'100',NULL,'100',NULL,'B','100',55.00,41,2,'Raw Material','2019-04-22 03:45:44','2019-08-27 00:04:25'),(2,NULL,'Corr. Board 131 SQNMB','Supplier Code: GoodE-0080','624','','1484','','C','175#',200.00,0,2,'Raw Material','2019-06-03 07:57:52','2019-08-21 19:39:12'),(3,NULL,'Corr. Board CB-300# 58 x 49\"','CB-300# 58\" X 49\"','58','','49','','CB','300#',37.06,26,2,'Raw Material','2019-06-27 03:35:51','2019-08-26 18:40:35'),(4,NULL,'Corr. Board CB-300# 42 x 48.5\"','CB-300# 42\" x 48.5\"','42','','48.5','','CB','300#',50.00,20,2,'Raw Material','2019-06-27 03:36:56','2019-08-26 18:40:36'),(5,NULL,'Corr. Board CB-350# 51\" x 74.25\"','CB-350# 51\" x 74.25\"','51','','74.25','','CB','350',81.40,0,2,'Raw Material','2019-07-01 10:57:07','2019-07-24 03:11:37'),(6,NULL,'Corr. Board CB- 350# 51\" x 84.5\"','CB- 350# 51\" x 84.5\"','51','','84.5','','CB','350',71.53,0,2,'Raw Material','2019-07-01 10:58:11','2019-07-24 03:11:37'),(7,NULL,'Corr. Board E- 200# 45\" x 42.5\"','E- 200# 45\" x 42.5\"','45','','42.5','','E','200#',27.89,3140,2,'Raw Material','2019-07-02 00:41:04','2019-07-02 00:51:01'),(8,NULL,'Corr. Board C-175# 40\" x 42\"','C-175# 40\" x 42\"','40','','42','','C','175#',21.23,611,2,'Raw Material','2019-07-02 06:43:51','2019-07-02 07:36:51'),(9,NULL,NULL,'100 x 100','100','','100','','C','175#',0.00,10,1,'Excess Board','2019-07-16 14:55:05','2019-07-18 05:30:09'),(10,NULL,NULL,'51\" x 33.5\" CB 350#','51\"','','33','5\"CB350#','CB','350',0.00,0,1,'Excess Board','2019-07-18 05:39:27','2019-07-23 03:25:32'),(11,NULL,'Corr. Board B-175# 39\" x 43\"','B-175# 39\" x 43\"','39','\"','43','\"','B','175',20.73,0,2,'Raw Material','2019-07-22 09:36:05','2019-07-23 02:38:48'),(12,NULL,'Corr. Board B-175# 39\" x 39\"','B-175# 39\" x 39\"','39','\"','39','\"','B','175',18.80,0,2,'Raw Material','2019-07-22 09:39:33','2019-07-23 02:38:48'),(13,NULL,'Corr. Board B-175# 49\" x 60\"','B-175# 49\" x 60\"','49','\"','60','\"','B','175',36.34,0,2,'Raw Material','2019-07-22 09:46:29','2019-07-23 02:38:48'),(14,NULL,'Corr. Board C-175# 36\" x 68.5\"','C-175# 36\" x 68.5\"','36','\"','68.5','\"','C','175#',30.48,0,2,'Raw Material','2019-07-22 09:55:41','2019-07-23 02:38:48'),(15,NULL,'Corr. Board C-175# 36\" x 66\"','C-175# 36\" x 66\"','36','\"','66','\"','C','175#',29.37,0,2,'Raw Material','2019-07-22 09:57:30','2019-07-23 02:38:47'),(16,NULL,'Corr. Board C-175# 36\" x 46\"','C-175# 36\" x 46\"','36','\"','46','\"','C','175#',20.47,0,2,'Raw Material','2019-07-22 10:03:33','2019-07-23 02:38:47'),(17,NULL,'Corr. Board C-175# 46\" x 44\"','C-175# 46\" x 44\"','46','\"','44','\"','C','175#',25.02,0,2,'Raw Material','2019-07-22 10:04:27','2019-07-23 02:38:47'),(18,NULL,'Corr. Board C-175# 37\" x 43\"','C-175# 37\" x 43\"','37','\"','43','\"','C','175#',19.67,0,2,'Raw Material','2019-07-22 10:08:31','2019-07-23 02:38:47'),(19,NULL,'Corr. Board C-175# 41\"x 39\"','C-175# 41\"x 39\"','41','\"','39','\"','C','175#',19.77,0,2,'Raw Material','2019-07-22 10:09:35','2019-07-23 02:38:46'),(20,NULL,'Corr. Board B-175# 47\"x39\"','B-175# 47\"x39\"','47','\"','39','\"','B','175',22.66,0,2,'Raw Material','2019-07-22 10:14:37','2019-07-23 02:38:46'),(21,NULL,'Corr. Board B-175# 55\" x 43\"','B-175# 55\" x 43\"','55','\"','43','\"','B','175',29.33,0,2,'Raw Material','2019-07-23 00:51:08','2019-07-23 02:38:46'),(22,NULL,'Corr. Board B-175# 40\" x 34\"','B-175# 40\" x 34\"','40','\"','34','\"','B','175',16.81,0,2,'Raw Material','2019-07-23 00:54:59','2019-07-23 02:38:46'),(23,NULL,'Corr. Board C-175# 40\" x 36\"','C-175# 40\" x 36\"','40','\"','36','\"','B','175',17.80,0,2,'Raw Material','2019-07-23 00:57:02','2019-07-23 02:38:45'),(24,NULL,'Corr. Board B-175# 68\" x 63\"','B-175# 68\" x 63\"','68','\"','63','\"','B','175',52.06,0,2,'Raw Material','2019-07-23 05:52:17','2019-07-23 06:24:59'),(25,NULL,'Corr. Board C-175# 46\" x 80.5\"','C-175# 46\" x 80.5\"','46','\"','80.5','\"','C','175#',26.01,0,2,'Raw Material','2019-07-23 05:54:43','2019-07-23 06:24:59'),(26,NULL,'Corr. Board E-200# 46\" x 42.5\"','E-200# 46\" x 42.5\"','46','\"','42.5','\"','E','200#',27.70,0,2,'Raw Material','2019-07-23 05:59:36','2019-07-23 06:24:58'),(27,NULL,'Corr. Board E-200# 36\" x 51\"','E-200# 36\" x 51\"','36','\"','51','\"','E','200#',45.00,0,2,'Raw Material','2019-07-23 06:00:35','2019-07-23 06:24:59'),(28,NULL,'Corr. Board C-175# 38\" x 74.5\"','C-175# 38\" x 74.5\"','38','\"','74.5','\"','C','175#',34.40,0,2,'Raw Material','2019-07-23 07:42:06','2019-07-23 08:37:56'),(29,NULL,'Corr. Board C-175# 42\" x 68.5\"','C-175# 42\" x 68.5\"','42','\"','68.5','\"','C','175#',34.96,0,2,'Raw Material','2019-07-23 07:50:30','2019-07-23 08:37:54'),(30,NULL,'Corr. Board C-175# 46\" x 77\"','C-175# 46\" x 77\"','46','\"','77','\"','C','175#',43.05,0,2,'Raw Material','2019-07-23 07:51:40','2019-07-23 08:37:54'),(31,NULL,'Corr. Board C-175# 46\" x 48\"','C-175# 46\" x 48\"','46','\"','48','\"','C','175#',26.83,0,2,'Raw Material','2019-07-23 07:52:28','2019-07-23 08:37:56'),(32,NULL,'Corr. Board C-175# 48\" x 77.25\"','Corr. Board C-175# 48\" x 77.25\"','48','\"','77.25','\"','C','175#',45.06,0,2,'Raw Material','2019-07-23 07:53:43','2019-07-23 08:37:55'),(33,NULL,'Corr. Board C-175# 50\" x 37\"','C-175# 50\" x 37\"','50','\"','37','\"','C','175#',22.48,0,2,'Raw Material','2019-07-23 07:54:39','2019-07-23 08:37:55'),(34,NULL,'Corr. Board C-175# 50\" x 58\"','C-175# 50\" x 58\"','50','\"','58','\"','C','175#',35.24,0,2,'Raw Material','2019-07-23 07:55:42','2019-07-23 08:37:55'),(35,NULL,'Corr. Board C-175# 54\" x 77.25\"','C-175# 54\" x 77.25\"','54','\"','77.25','\"','C','175#',50.70,0,2,'Raw Material','2019-07-23 07:56:37','2019-07-23 08:37:55'),(36,NULL,'Corr. Board C-175# 56\" x 36.5\"','C-175# 56\" x 36.5\"','56','\"','36.5','\"','C','175#',24.84,0,2,'Raw Material','2019-07-23 07:57:30','2019-07-23 08:37:54'),(37,NULL,'Corr. Board C-175# 58\" x 43\"','C-175# 58\" x 43\"','58','\"','43','\"','C','175#',30.31,0,2,'Raw Material','2019-07-23 07:58:41','2019-07-23 08:37:54'),(38,NULL,'Corr. Board CB-300# 58\" x 45.5\"','CB-300# 58\" x 45.5\"','58','\"','45.5','\"','CB','300#',43.35,0,2,'Raw Material','2019-07-23 09:15:21','2019-07-23 09:32:50'),(39,NULL,'Corr. Board CB-300# 58\" x 39\"','CB-300# 58\" x 39\"','58','\"','39','\"','CB','300#',41.16,0,2,'Raw Material','2019-07-23 09:17:13','2019-07-23 09:32:50'),(40,NULL,'Corr. Board CB-300# 40\" x 69\"','CB-300# 40\" x 69\"','40','\"','69','\"','CB','300#',50.22,0,2,'Raw Material','2019-07-23 09:18:51','2019-07-23 09:32:50'),(41,NULL,'Corr. Board CB-350# 45\" x 51\"','CB-350# 45\" x 51\"','45','\"','51','\"','CB','350#',48.02,0,2,'Raw Material','2019-07-23 09:22:44','2019-07-23 09:32:50'),(42,NULL,'Corr. Board C-175# 61\" x 71\"','C-175# 61\" x 71\"','61','\"','71','\"','C','175#',52.03,0,2,'Raw Material','2019-07-23 09:41:27','2019-07-23 09:58:17'),(43,NULL,'Corr. Board CB-300# 38\" x 70\"','CB-300# 38\" x 70\"','38','\"','70','\"','CB','300#',48.40,0,2,'Raw Material','2019-07-23 09:42:44','2019-07-23 09:58:17'),(44,NULL,'Corr. Board CB-300# 51\" x 64\"','CB-300# 51\" x 64\"','51','\"','64','\"','CB','300#',99.93,0,2,'Raw Material','2019-07-23 09:43:48','2019-07-23 09:58:16'),(45,NULL,'Corr. Board CB-300# 65 \" x 84.5\"','CB-300# 65 \" x 84.5\"','65','\"','84.5','\"','CB','300#',65.44,0,2,'Raw Material','2019-07-23 09:45:59','2019-07-23 09:58:16'),(46,NULL,'Corr. Board CB-350# 65\" x 84.5\"','CB-350# 65\" x 84.5\"','65','\"','84.5','\"','CB','350#',65.44,0,2,'Raw Material','2019-07-23 09:47:04',NULL),(47,NULL,'Corr. Board CB-350# 41\" x 84.5\"','CB-350# 41\" x 84.5\"','41','\"','84.5','\"','CB','350#',59.39,0,2,'Raw Material','2019-07-23 09:48:27','2019-07-23 09:58:16');
/*!40000 ALTER TABLE `rawmaterials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vatexempt` decimal(10,2) NOT NULL,
  `vatablesales` decimal(10,2) NOT NULL,
  `zerorated` decimal(10,2) NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `telnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faxnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overrunpercentage` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,0.30,0.20,0.00,'Lot 8 Blk 25 , Verona Street, Villa de Toledo Subd 4026 Sta. Rosa, Laguna','(049) 534-9581','(049) 534-4439',0.05);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settingseditaccess`
--

DROP TABLE IF EXISTS `settingseditaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settingseditaccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ugroupid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settingseditaccess`
--

LOCK TABLES `settingseditaccess` WRITE;
/*!40000 ALTER TABLE `settingseditaccess` DISABLE KEYS */;
INSERT INTO `settingseditaccess` VALUES (2,1,'2019-07-07 00:33:06',NULL);
/*!40000 ALTER TABLE `settingseditaccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactperson` text COLLATE utf8mb4_unicode_ci,
  `contactnumber` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` text COLLATE utf8mb4_unicode_ci,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `taxclassification` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sid`),
  UNIQUE KEY `suppliers_supplier_unique` (`supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Twinpack Container Corporation',NULL,NULL,NULL,'Unit1209 Le Gran Condo., 45 Eisenhower St., San Juan City, 1504','005-515-235-000','90 days','Vatable Sales'),(2,'Packagemakers Inc.',NULL,NULL,NULL,'495 Boni Avenue, Mandaluyong City','000-058-227-000','90 Days','Vatable Sales'),(3,'Packlink industrial Corp.',NULL,NULL,NULL,'18 Dizon Compd. G. Marcelo St., Brgy. Maysan, Valenzuela City','008-298-466-000','90 Days','Vatable Sales'),(4,'Triple Star Packaging Corp.',NULL,NULL,NULL,'Canlubang Ind\'l. Estate Pittland, Cabuyao Laguna','006-600-682-000','90 Days','Vatable Sales'),(5,'Ubros Enterprises',NULL,NULL,NULL,'Blk 68 Lot 19 Phase 3 Main Road,Brgy. Loma Binan Laguna','714-811-445-000','30 Days','Vat Exempt'),(6,'Inoac Philippines Corp.',NULL,NULL,NULL,'RGC Compound, Brgy. Pittland Cabuayo, Laguna','005-240-769-000','30 Days','Vatable Sales'),(7,'Richtech Industrial Supply Company',NULL,NULL,NULL,'Blk.5 Lot.9 Chicago St. Landmark Subd., Parian Calamba City','228-953-827-000','30 Days','Vatable Sales'),(8,'E Copy Corporation',NULL,NULL,NULL,'65 Gil Puyat Avenue Palanan Makati City, Philippines','219-274-001-000','30 Days','Vatable Sales'),(9,'Nappco',NULL,NULL,NULL,'34 Narciso St., Bo. Canumay, Valenzuela City','000-297-204-000','60 Days','Vatable Sales'),(10,'Precisionfoam Company Inc.',NULL,NULL,NULL,'Coats manila Bay Compound Lopez Jaeza St., Marikina City','008-113-324-000','30 Days','Vatable Sales'),(11,'ECD Industrial Supply Corp.',NULL,NULL,NULL,'Purok 1, National Highway, Dita Sta. Rosa City, Laguna','009-339-717-000','30 Days','Vatable Sales'),(12,'C.Dioko Construction Supply Corp.',NULL,NULL,NULL,'National Hi-way, Brgy.Dita , Sta Rosa Laguna','209-617-512-000','30 Days','Vatable Sales'),(13,'JSJ Packaging Services',NULL,NULL,NULL,'Main Road, Constantino Subd., Poblacion 2, Marilao, Bulacan','128-136-211-000','30 Days','Vatable Sales'),(14,'Consolisated Adhesives, Inc.',NULL,NULL,NULL,'7 Caballero St., Pozorrubio, Pangasinan / 139-D Aurora Blvd.,San Juan, Metro Manila','000-251-906-000','30 Days','Vatable Sales'),(15,'Five Star Europepaper Systems, Inc.',NULL,NULL,NULL,'#068 Purok 3, Brgy. Langkiwa, Binan Laguna','006-721-089-000','30 Days','Vatable Sales'),(16,'MCR Industries, Inc.',NULL,NULL,NULL,'495 boni Avenue, Mandaluyong City PC 3119','000-058-523-000','30 days','Vatable Sales'),(17,'Super Die Cut Enterprises',NULL,NULL,NULL,'8 Anak Bayan Street, Paltok 1, Quezon City','103-965-957-000','30 Days','Vatable Sales'),(18,'Marion Flexographics Services',NULL,NULL,NULL,'1009 JP Rizal Street Poblacion, Makati City','129-435-751-000','30 Days','Vatable Sales'),(19,'Brusmick Place Balibago Gasoline Service Station Inc.',NULL,NULL,NULL,'Lot 1 Balibago Rd.,Cor.RSBS Blvd., Balibago, Sta. Rosa City Laguna','006-940-152-000','15 Days','Vatable Sales'),(20,'1198 Enterprises',NULL,NULL,NULL,'D-5015 ME, National Highway, Balibago, Sta. Rosa City, Laguna','195-800-422-000','30 Days','Vat Exempt'),(23,'Angeal Corp','Angelo Gabriel B Del Mundo','9210063','delmundoangelo15@gmail.com','#38 Vasra Village Diliman QC',NULL,NULL,'Vat Exempt');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useraccess`
--

DROP TABLE IF EXISTS `useraccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccess` (
  `uaccessid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ugroupid` int(11) NOT NULL,
  `accessid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uaccessid`)
) ENGINE=InnoDB AUTO_INCREMENT=670 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccess`
--

LOCK TABLES `useraccess` WRITE;
/*!40000 ALTER TABLE `useraccess` DISABLE KEYS */;
INSERT INTO `useraccess` VALUES (600,1,1,'2019-08-27 01:46:29',NULL),(601,1,2,'2019-08-27 01:46:29',NULL),(602,1,3,'2019-08-27 01:46:29',NULL),(603,1,4,'2019-08-27 01:46:29',NULL),(604,1,5,'2019-08-27 01:46:29',NULL),(605,1,6,'2019-08-27 01:46:29',NULL),(606,1,7,'2019-08-27 01:46:29',NULL),(607,1,8,'2019-08-27 01:46:29',NULL),(608,1,9,'2019-08-27 01:46:29',NULL),(609,1,10,'2019-08-27 01:46:29',NULL),(610,1,11,'2019-08-27 01:46:29',NULL),(611,1,12,'2019-08-27 01:46:29',NULL),(612,1,13,'2019-08-27 01:46:29',NULL),(613,1,14,'2019-08-27 01:46:29',NULL),(614,1,15,'2019-08-27 01:46:29',NULL),(615,1,16,'2019-08-27 01:46:29',NULL),(616,1,17,'2019-08-27 01:46:29',NULL),(617,1,18,'2019-08-27 01:46:29',NULL),(618,1,19,'2019-08-27 01:46:29',NULL),(619,1,20,'2019-08-27 01:46:29',NULL),(620,1,21,'2019-08-27 01:46:29',NULL),(621,1,22,'2019-08-27 01:46:29',NULL),(622,1,23,'2019-08-27 01:46:29',NULL),(623,1,24,'2019-08-27 01:46:29',NULL),(624,1,25,'2019-08-27 01:46:29',NULL),(625,1,26,'2019-08-27 01:46:29',NULL),(626,1,27,'2019-08-27 01:46:29',NULL),(627,1,28,'2019-08-27 01:46:29',NULL),(628,1,29,'2019-08-27 01:46:29',NULL),(629,1,30,'2019-08-27 01:46:29',NULL),(630,1,31,'2019-08-27 01:46:29',NULL),(631,1,32,'2019-08-27 01:46:29',NULL),(632,1,33,'2019-08-27 01:46:29',NULL),(633,1,34,'2019-08-27 01:46:29',NULL),(634,1,35,'2019-08-27 01:46:29',NULL),(635,1,36,'2019-08-27 01:46:29',NULL),(636,1,37,'2019-08-27 01:46:29',NULL),(637,1,38,'2019-08-27 01:46:29',NULL),(638,1,39,'2019-08-27 01:46:29',NULL),(639,1,40,'2019-08-27 01:46:29',NULL),(640,1,41,'2019-08-27 01:46:29',NULL),(641,1,42,'2019-08-27 01:46:29',NULL),(642,1,43,'2019-08-27 01:46:29',NULL),(643,1,44,'2019-08-27 01:46:29',NULL),(644,1,45,'2019-08-27 01:46:29',NULL),(645,1,46,'2019-08-27 01:46:29',NULL),(646,1,47,'2019-08-27 01:46:29',NULL),(647,1,48,'2019-08-27 01:46:29',NULL),(648,1,49,'2019-08-27 01:46:29',NULL),(649,1,50,'2019-08-27 01:46:29',NULL),(650,1,52,'2019-08-27 01:46:29',NULL),(651,1,53,'2019-08-27 01:46:29',NULL),(652,1,54,'2019-08-27 01:46:29',NULL),(653,1,55,'2019-08-27 01:46:29',NULL),(654,1,56,'2019-08-27 01:46:29',NULL),(655,1,57,'2019-08-27 01:46:29',NULL),(656,1,58,'2019-08-27 01:46:29',NULL),(657,1,59,'2019-08-27 01:46:29',NULL),(658,1,60,'2019-08-27 01:46:29',NULL),(659,1,61,'2019-08-27 01:46:29',NULL),(660,1,62,'2019-08-27 01:46:29',NULL),(661,1,63,'2019-08-27 01:46:29',NULL),(662,1,64,'2019-08-27 01:46:29',NULL),(663,1,65,'2019-08-27 01:46:29',NULL),(664,1,66,'2019-08-27 01:46:29',NULL),(665,1,67,'2019-08-27 01:46:29',NULL),(666,1,68,'2019-08-27 01:46:29',NULL),(667,1,69,'2019-08-27 01:46:29',NULL),(668,1,70,'2019-08-27 01:46:29',NULL),(669,1,71,'2019-08-27 01:46:29',NULL);
/*!40000 ALTER TABLE `useraccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `ugroupid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usergroup` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ugroupid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
INSERT INTO `usergroups` VALUES (1,'Administrator','2019-03-09 12:31:39',NULL),(2,'Purchasing','2019-03-09 16:30:46',NULL),(3,'Planning','2019-03-10 01:58:50',NULL);
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` int(255) NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'noimage.jpg',
  `esign` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'Angelo Gabriel','Del Mundo','B','angel1231','$2y$10$IlYeUynFcD662dePaLkTKO7QClJKbEnYt6/r8r1peJOJ/aj.mRHqq',1,'noimage.jpg','1043884448.png','fX1YQdMCCRsMkX2aIsLVnbE4ujTOrtxe9D4mIPww0P9u5r8VOAj6NhYfpHD5','2018-07-08 12:41:46','2018-07-15 02:06:22'),(19,'Raphy','Del Mundo','B','rap1231','$2y$10$CNTWDIOjesq1ZX6O.OwVPeh8qOV24AQQ6ZvI0fBtV.pcXkzrF7Nl6',3,'noimage.jpg','noesignimage.png','IJ7nYJ61d2uyOeH2kbNA8ojZYnFagXM52WyfU9aVGMjlwJsXZzvnnUOLc9HN','2019-03-10 03:14:52','2019-03-16 03:02:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersrestrictions`
--

DROP TABLE IF EXISTS `usersrestrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersrestrictions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `purchasing` int(11) NOT NULL,
  `planning` int(11) NOT NULL,
  `rawmaterial` int(11) NOT NULL,
  `finishgoods` int(11) NOT NULL,
  `sales` int(11) NOT NULL,
  `qualityassurance` int(11) NOT NULL,
  `production` int(11) NOT NULL,
  `accounting` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersrestrictions`
--

LOCK TABLES `usersrestrictions` WRITE;
/*!40000 ALTER TABLE `usersrestrictions` DISABLE KEYS */;
INSERT INTO `usersrestrictions` VALUES (1,17,1,1,1,1,1,1,1,1,'2019-02-09 03:07:37','2019-02-10 00:38:14'),(2,9,1,0,0,1,0,0,1,0,'2019-02-10 00:45:09','2019-02-10 00:45:15'),(3,15,0,0,0,0,0,0,0,0,'2019-02-25 11:13:06','2019-02-25 11:14:22'),(4,18,0,0,0,0,0,0,0,0,'2019-02-25 11:15:22',NULL),(5,19,0,0,0,0,0,0,0,0,'2019-03-10 03:14:52','2019-03-16 03:02:22');
/*!40000 ALTER TABLE `usersrestrictions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-03  5:46:08
