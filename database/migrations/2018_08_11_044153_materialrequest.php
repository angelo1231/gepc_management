<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Materialrequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialrequest', function (Blueprint $table) {
            $table->increments('requestid');
            $table->string('mrinumber', '60')->unique();
            $table->string('issuancenumber', '60')->unique();
            $table->integer('prid');
            $table->integer('cid');
            $table->integer('fgid');
            $table->text('rmissuedby')->nullable();
            $table->text('reason')->nullable();
            $table->text('issuedby');
            $table->text('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialrequest');
    }
}
