<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Materialrequestitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialrequestitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mrid');
            $table->string('drnumber', 60)->nullable();
            $table->integer('rmid');
            $table->integer('qtyrequired');
            $table->text('remarks')->nullable();
            $table->integer('qtyissued')->nullable();
            $table->integer('balance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialrequestitems');       
    }
}
