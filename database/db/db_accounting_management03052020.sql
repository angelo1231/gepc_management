-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.26-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_accounting_management.access
CREATE TABLE IF NOT EXISTS `access` (
  `accessid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`accessid`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.access: ~70 rows (approximately)
DELETE FROM `access`;
/*!40000 ALTER TABLE `access` DISABLE KEYS */;
INSERT INTO `access` (`accessid`, `access`, `created_at`, `updated_at`) VALUES
	(1, 'Maintenance', NULL, NULL),
	(2, 'User Account Information', NULL, NULL),
	(3, 'New User Information', NULL, NULL),
	(4, 'Edit User Information', NULL, NULL),
	(5, 'Change User Password', NULL, NULL),
	(6, 'Reset User Password', NULL, NULL),
	(7, 'User Group Information', NULL, NULL),
	(8, 'New User Group Information', NULL, NULL),
	(9, 'Add User Group Access', NULL, NULL),
	(10, 'Raw Material 201 Information', NULL, NULL),
	(11, 'New Raw Material Information', NULL, NULL),
	(12, 'Edit Raw Material Information', NULL, NULL),
	(13, 'FInish Goods 201 Information', NULL, NULL),
	(14, 'New FInish Goods Information', NULL, NULL),
	(15, 'Edit Finish Goods Information', NULL, NULL),
	(16, 'Process Information', NULL, NULL),
	(17, 'New Process Information', NULL, NULL),
	(18, 'Edit Process Information', NULL, NULL),
	(19, 'Process Code Information', NULL, NULL),
	(20, 'New Process Code Information', NULL, NULL),
	(21, 'Edit Process Code Information', NULL, NULL),
	(22, 'Supplier 201 Information', NULL, NULL),
	(23, 'New Supplier Information', NULL, NULL),
	(24, 'Edit Supplier Information', NULL, NULL),
	(25, 'Customer 201 Information', NULL, NULL),
	(26, 'New Customer Information', NULL, NULL),
	(27, 'Edit Customer Information', NULL, NULL),
	(28, 'Currency 201 Information', NULL, NULL),
	(29, 'New Currency Information', NULL, NULL),
	(30, 'Edit Currency Information', NULL, NULL),
	(31, 'Settings', NULL, NULL),
	(32, 'Purchasing', NULL, NULL),
	(33, 'New Purchase Order', NULL, NULL),
	(34, 'Planning', NULL, NULL),
	(35, 'New Job Order', NULL, NULL),
	(36, 'Purchase Request Planning Info', NULL, NULL),
	(37, 'New Purchase Request Planning', NULL, NULL),
	(38, 'Material Request Planning', NULL, NULL),
	(39, 'New Material Request Planning', NULL, NULL),
	(40, 'Customer Purchase Order Planning Info', NULL, NULL),
	(41, 'Raw Material Inventory', NULL, NULL),
	(42, 'Raw Material Request Approval Inventory', NULL, NULL),
	(43, 'Raw Material Delivery Info Inventory', NULL, NULL),
	(44, 'Finish Goods Inventory', NULL, NULL),
	(45, 'Finish Goods Production', NULL, NULL),
	(46, 'Finish Goods Delivery', NULL, NULL),
	(47, 'Sales', NULL, NULL),
	(48, 'New Sales Information', NULL, NULL),
	(49, 'Sales Customer Purchase Order Info', NULL, NULL),
	(50, 'Sales New Customer Purchase Order Info', NULL, NULL),
	(52, 'Quality Assurance', NULL, NULL),
	(53, 'New Final Inspection', NULL, NULL),
	(54, 'COC Information', NULL, NULL),
	(55, 'Production', NULL, NULL),
	(56, 'Production Tag', NULL, NULL),
	(57, 'Production Tag Print', NULL, NULL),
	(58, 'Production Operation', NULL, NULL),
	(59, 'New Prodcution Operation', NULL, NULL),
	(60, 'Production Operation Information', NULL, NULL),
	(61, 'Accounting', NULL, NULL),
	(62, 'Accounting Delivery Sales Information', NULL, NULL),
	(63, 'Purchasing Purchase Request ', NULL, NULL),
	(64, 'Production Monitoring', NULL, NULL),
	(65, 'Purchase Order Approver', NULL, NULL),
	(66, 'Update FG Stocks', NULL, NULL),
	(67, 'Update RM Stocks', NULL, NULL),
	(68, 'Planning Notification', NULL, NULL),
	(69, 'Quality Assurance Reject Information', NULL, NULL),
	(70, 'Quality Assurance New Reject Type', NULL, NULL),
	(71, 'Quality Assurance Update Reject Type', NULL, NULL);
/*!40000 ALTER TABLE `access` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.currency
CREATE TABLE IF NOT EXISTS `currency` (
  `currencyid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`currencyid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.currency: ~2 rows (approximately)
DELETE FROM `currency`;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`currencyid`, `currency`, `rate`) VALUES
	(1, 'American Dollars', 53.25),
	(2, 'PH Peso', 1.00);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactperson` text COLLATE utf8mb4_unicode_ci,
  `contactnumber` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` text COLLATE utf8mb4_unicode_ci,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `taxclassification` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.customers: ~32 rows (approximately)
DELETE FROM `customers`;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`cid`, `customer`, `contactperson`, `contactnumber`, `email`, `address`, `tin`, `terms`, `taxclassification`) VALUES
	(1, 'Aikawa Philippines Inc.', NULL, NULL, NULL, 'FPIP Sto. Tomas Batangas', '006-035-150-000', '30 Days', 'Vat Exempt'),
	(2, 'B/E Aerospace B.V.', NULL, NULL, NULL, 'FPIP Sto. Tomas Batangas', '289-439-550-000', '30 Days', 'Vat Exempt'),
	(3, 'Continental Temic Electronics Phils, Inc.', NULL, NULL, NULL, 'No.16 Ringroad LISP II SEZ Brgy. La Mesa Calamba Laguna', '234-474-332', '30 Days', 'Vat Exempt'),
	(4, 'Denso Philippines Corporation', NULL, NULL, NULL, '109 Unity Ave. CIP, canlubang, Laguna', '004-706-120-000', '30 Days', 'Vat Exempt'),
	(5, 'Device Dynamic Asia', NULL, NULL, NULL, 'Batino, Calamba Laguna', '204-770-285-000', '30 Days', 'Vat Exempt'),
	(6, 'Godo Philippines Inc.', NULL, NULL, NULL, 'City Land Tower Shaw Blvd, Mandaluyong City', '217-776-769-000', '60 Days', 'Vatable Sales'),
	(7, 'Inoac Philippines Corp.', NULL, NULL, NULL, 'RGC Compound Brgy. Pittland Cabuyao Laguna', '005-240-769-000', '30 Days', 'Vatable Sales'),
	(8, 'Integrated Micro-Electronics Inc.', NULL, NULL, NULL, 'Laguna Technopark SEPZ Binan Laguna', '000-409-747-000', '120 Days', 'Vat Exempt'),
	(9, 'JSJ Packaging Services', NULL, NULL, NULL, 'Poblacion 2 Marilao Bulacan', '128-136-211-000', '30 Days', 'Vatable Sales'),
	(10, 'Littlefuse Philippines Inc.', NULL, NULL, NULL, 'Lima Technology Center Malvar, Batangas', '005-196-916-000', '60 Days', 'Vat Exempt'),
	(11, 'Maccaferri Philippines Manufacturing Inc.', NULL, NULL, NULL, 'Blk,3 Lot.3&4 Star Avenue LIIP Mamplasan City of Binan', '008-382-415-000', '30 Days', 'Vat Exempt'),
	(12, 'Masuma Food Industry Inc.', NULL, NULL, NULL, 'Bagsakan Road SEZ FTI Complex, Taguig City', '245-660-529-000', '30 Days', 'Vatable Sales'),
	(13, 'Mitsuba Philippines Corporation', NULL, NULL, NULL, 'Lima Technology Center, Malvar, Batangas', '005-065-700-000', '30 Days', 'Vat Exempt'),
	(14, 'Nep Logistics Inc.', NULL, NULL, NULL, 'Laguna Technopark Binan Laguna', '204-562-495-000', '30 Days', 'Vat Exempt'),
	(15, 'Nihon Houzai Laguna Corporation', NULL, NULL, NULL, 'L3281 SEPZ Laguna Technopark, City of Binan, Laguna', '231-496-735-000', '30 Days', 'Vat Exempt'),
	(16, 'NxtRev Technology Inc.', NULL, NULL, NULL, 'B 12 L 15 Molino III, Bacoor Cavite', '007-646-638-000', '30 Days', 'Zero Rated'),
	(17, 'Okuda Phils.', NULL, NULL, NULL, 'FPIP Tanauan City, Batangas', '009-099-635-000', '30 Days', 'Vat Exempt'),
	(18, 'Philippines TRC Inc.', NULL, NULL, NULL, 'Lima Technology Center, Malvar Batangas', '005-739-755-000', '30 Days', 'Vat Exempt'),
	(19, 'Sohbi Kohgei Phils. Inc.', NULL, NULL, NULL, 'Lima Technology Center Malvar, Batangas', '005-739-706-000', '30 Days', 'Vat Exempt'),
	(20, 'Terumo Philippines Corporation', NULL, NULL, NULL, 'Laguna Technopark Binan Laguna', '005-648-877-000', '30 Days', 'Vat Exempt'),
	(21, 'Testech Incorporated', NULL, NULL, NULL, 'SEZ Brgy.Batino Calamba City', '208-003-322-000', '30 Days', 'Vat Exempt'),
	(22, 'Thermos Vaccumtech Philippines Inc.', NULL, NULL, NULL, 'FPIP Sto. Tomas Batangas', '008-855-485-000', '30 Days', 'Vat Exempt'),
	(23, 'Tsuchiya Kogyo (Phils.) Inc.', NULL, NULL, NULL, 'L9 B 1, LISP III Sto. Tomas Batangas', '000-666-637-000', '30 Days', 'Vat Exempt'),
	(24, 'Vishay (Phils) Inc.', NULL, NULL, NULL, 'FTI SEZ Bagsakan RD FTI Taguig City', '004-641-513-000', '30 Days', 'Vat Exempt'),
	(25, 'Zama Precision Industy Mfg Phils. Inc.', NULL, NULL, NULL, 'FPIP SEZ 2 Brgy. Sta. Anastacia Sto. Tomas Batangas', '008-699-372-000', '30 Days', 'Vat Exempt'),
	(26, 'Techno Moldplas (Phils) Inc.', NULL, NULL, NULL, 'Lot C4 7B Tagaytay Bridge drive Carmelray Industrial Park II Brgy Punta Calamba City Laguna', '008-598-886-000', '30 Days', 'Zero Rated'),
	(27, 'ShinHeung Electro-Digital Inc.', NULL, NULL, NULL, 'Lot 5 Blk 3 CPIP Brgy. batino Calamba Laguna', '211-675-728-000', '30 Days', 'Zero Rated'),
	(28, 'Armstrong Weston Asia Inc.', NULL, NULL, NULL, 'Bldg. 5B Paronama Compound Lot 3 Blk 10 A Bo. Anastacia Sto. Tomas Batangas', NULL, '30 days', 'Zero Rated'),
	(29, 'Worth BI Enterprise', NULL, NULL, NULL, 'Bel Air Sta. Rosa Laguna', '147-522-231-000', '30 Days', 'Zero Rated'),
	(43, 'Nihon Houzai Laguna Corporation', NULL, NULL, NULL, 'CPIP Batino Calamba Laguna', '231-496-735-000', '30 Days', 'Vat Exempt'),
	(44, 'Angeal Corp', 'Angeal Del Mundo', '09989505579', 'delmundoangealgabriel@gmail.com', '#38 Vasra Village Diliman QC', NULL, NULL, 'Vat Exempt'),
	(45, 'Zenith Technology Inc.', 'Kristine Arambulo', 'n/a', 'purchasing@zenitech-group.com', 'Bldg 2, Print Town Complex LIIP Avenue Mamplasan, Biñan City, Laguna', '006-906-838-000', '30 days', 'Zero Rated');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.deliveryinformation
CREATE TABLE IF NOT EXISTS `deliveryinformation` (
  `did` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sinumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grandtotal` decimal(8,2) NOT NULL,
  `cid` int(11) NOT NULL,
  `taxclassification` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliverydate` date NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.deliveryinformation: ~0 rows (approximately)
DELETE FROM `deliveryinformation`;
/*!40000 ALTER TABLE `deliveryinformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveryinformation` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.deliveryinformationitems
CREATE TABLE IF NOT EXISTS `deliveryinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `did` int(11) NOT NULL,
  `poid` int(11) NOT NULL,
  `pocusitemid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `currencyid` int(11) NOT NULL,
  `rate` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.deliveryinformationitems: ~0 rows (approximately)
DELETE FROM `deliveryinformationitems`;
/*!40000 ALTER TABLE `deliveryinformationitems` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveryinformationitems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.deliverypreparationsinformation
CREATE TABLE IF NOT EXISTS `deliverypreparationsinformation` (
  `preparationid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preparationnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approvedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approveddate` datetime DEFAULT NULL,
  `disapprovedby` text COLLATE utf8mb4_unicode_ci,
  `disapproveddate` datetime DEFAULT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`preparationid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.deliverypreparationsinformation: ~0 rows (approximately)
DELETE FROM `deliverypreparationsinformation`;
/*!40000 ALTER TABLE `deliverypreparationsinformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverypreparationsinformation` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.deliverypreparationsinformationitems
CREATE TABLE IF NOT EXISTS `deliverypreparationsinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preparationid` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poid` int(11) NOT NULL,
  `pocusitemid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.deliverypreparationsinformationitems: ~0 rows (approximately)
DELETE FROM `deliverypreparationsinformationitems`;
/*!40000 ALTER TABLE `deliverypreparationsinformationitems` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverypreparationsinformationitems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.finishgoods
CREATE TABLE IF NOT EXISTS `finishgoods` (
  `fgid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partnumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `ID_length` text COLLATE utf8mb4_unicode_ci,
  `ID_width` text COLLATE utf8mb4_unicode_ci,
  `ID_height` text COLLATE utf8mb4_unicode_ci,
  `OD_length` text COLLATE utf8mb4_unicode_ci,
  `OD_width` text COLLATE utf8mb4_unicode_ci,
  `OD_height` text COLLATE utf8mb4_unicode_ci,
  `currencyid` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `stockonhand` int(11) DEFAULT NULL,
  `packingstd` text COLLATE utf8mb4_unicode_ci,
  `cid` int(11) NOT NULL,
  `processcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fgid`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.finishgoods: ~156 rows (approximately)
DELETE FROM `finishgoods`;
/*!40000 ALTER TABLE `finishgoods` DISABLE KEYS */;
INSERT INTO `finishgoods` (`fgid`, `itemcode`, `partnumber`, `partname`, `description`, `ID_length`, `ID_width`, `ID_height`, `OD_length`, `OD_width`, `OD_height`, `currencyid`, `price`, `stockonhand`, `packingstd`, `cid`, `processcode`, `attachment`, `created_at`, `updated_at`) VALUES
	(1, 'PAC-52-00', 'PH989004-3310', 'TOP SHEET PAD', 'PH989004-3310 TOP SHEET PAD', NULL, NULL, NULL, '430', '260', '0', 2, 4.50, 97, '50pcs', 4, 'B', NULL, NULL, '2019-06-04 05:27:09'),
	(2, 'PAC-48-00', 'PH989004-3330', '(INNER) SMALL BOX', 'PH989004-3330 (INNER) SMALL BOX', NULL, NULL, NULL, '182', '182', '125', 2, 13.86, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(3, 'PAC-59-01', 'PH989004-4280', 'INNER BOX', 'PH989004-4280 INNER BOX', '605', '390', '305', '609', '394', '311', 2, 40.50, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(4, 'PAC-69-00', 'PH989004-4930', 'INNER BOX', 'PH989004-4930 INNER BOX', NULL, NULL, NULL, '565', '386', '165', 2, 42.57, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(5, 'PAC-02-00', 'PH989246-0070', 'INNER BOX', 'PH989246-0070 INNER BOX', NULL, NULL, NULL, '520', '440', '230', 2, 54.56, 0, '10pcs', 4, 'D', NULL, NULL, NULL),
	(6, 'PAC-85-00', 'PH989246-0430', 'INNER BOX', 'PH989246-0430 INNER BOX', NULL, NULL, NULL, '175', '175', '120', 2, 9.74, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(7, 'PAC-110-00', 'PH989246-0440', 'INNER BOX', 'PH989246-0440 INNER BOX', '514', '434', '147', '520', '440', '155', 2, 52.97, 0, '16pcs', 4, 'D', NULL, NULL, NULL),
	(8, 'PAC-122-00', 'PH989246-0490', 'INNER BOX', 'PH989246-0490 INNER BOX', NULL, NULL, NULL, '631', '291', '260', 2, 56.43, 596, '10pcs', 4, 'D', NULL, NULL, '2019-08-27 02:29:09'),
	(9, 'PAC-99-00', 'PH989247-0020', 'INNER BOX', 'PH989247-0020 INNER BOX', NULL, NULL, NULL, '1050', '848', '500', 2, 242.50, 0, '5pcs', 4, 'E', NULL, NULL, NULL),
	(10, 'PAC-111-00', 'PH989247-0110', 'MASTER BOX', 'PH989247-0110 MASTER BOX', '1051', '891', '638', '1060', '900', '650', 2, 362.29, 0, '5pcs', 4, 'E', NULL, NULL, NULL),
	(11, 'PAC-141-00', 'PH989247-0150', 'MASTER BOX', 'PH989247-0150 MASTER BOX', '1125', '960', '480', '1135', '968', '490', 2, 311.69, 0, '5pcs', 4, 'E', NULL, NULL, NULL),
	(12, 'PAC-86-00', 'PH989701-0100', 'MASTER BOX', 'PH989701-0100 MASTER BOX', '1052', '892', '468', '1060', '900', '480', 2, 201.85, 0, '5pcs', 4, 'E', NULL, NULL, NULL),
	(13, 'PAC-138-00', 'PH989701-0250', 'T9AA MASTER BOX', 'PH989701-0250 T9AA MASTER BOX', NULL, NULL, NULL, '1170', '880', '550', 2, 195.14, 0, '5pcs', 4, 'E', NULL, NULL, NULL),
	(14, 'PAC-109-00', 'PH989702-0220', 'INNER BOX', 'PH989702-0220 INNER BOX', NULL, NULL, NULL, '435', '268', '178', 2, 25.13, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(15, 'PAC-105-00', 'PH989702-0350', 'INNER BOX', 'PH989702-0350 INNER BOX', NULL, NULL, NULL, '437', '267', '85', 2, 21.29, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(16, 'PAC-114-00', 'PH989702-0420', 'INNER BOX', 'PH989702-0420 INNER BOX', NULL, NULL, NULL, '420', '151', '215', 2, 25.45, 0, '10pcs', 4, 'C', NULL, NULL, NULL),
	(17, 'PAC-129-00', 'PH989702-0470', 'YLA INNER BOX', 'PH989702-0470 YLA INNER BOX', NULL, NULL, NULL, '552', '325', '168', 2, 23.43, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(18, 'PAC-126-00', 'PH989702-0480', 'INNER CARTON BOX', 'PH989702-0480 INNER CARTON BOX', NULL, NULL, NULL, '390', '245', '107', 2, 17.82, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(19, 'PAC-133-00', 'PH989702-0520', 'T9AA INNER BOX', 'PH989702-0520 T9AA INNER BOX', NULL, NULL, NULL, '715', '428', '258', 2, 47.52, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(20, 'PAC-150-00', 'PH989702-0541', 'D87A INNER BOX', 'PH989702-0541 D87A INNER BOX', NULL, NULL, NULL, '622', '284', '170', 2, 27.45, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(21, 'PAC-143-00', 'PH989702-0550', 'T9AA SPARE INNER BOX', 'PH989702-0550 T9AA SPARE INNER BOX', NULL, NULL, NULL, '428', '285', '258', 2, 40.15, 0, '10pcs', 4, 'D', NULL, NULL, NULL),
	(22, 'PAC-144-01', 'PH989702-0570', 'INNER BOX', 'PH989702-0570 INNER BOX', NULL, NULL, NULL, '357', '547', '77', 2, 47.38, 0, '10pcs', 4, 'C', NULL, NULL, NULL),
	(23, 'PAC-160-00', 'PH989702-0650', 'HONDA INNER BOX', 'PH989702-0650 HONDA INNER BOX', NULL, NULL, NULL, '615', '410', '265', 2, 51.37, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(24, 'PAC-157-00', 'PH989702-0700', 'INNER BOX', 'PH989702-0700 INNER BOX', NULL, NULL, NULL, '42', '42', '68', 2, 6.75, 0, '10pcs', 4, 'C', NULL, NULL, NULL),
	(25, 'PAC-165-00', 'PH989702-0750', 'HONDA INNER BOX', 'PH989702-0750 HONDA INNER BOX', NULL, NULL, NULL, '520', '440', '258', 2, 51.98, 0, '10pcs', 4, 'G', NULL, NULL, NULL),
	(26, 'PAC-87-00', 'PH989704-0020', 'COVER', 'PH989704-0020 COVER', NULL, NULL, NULL, '1076', '916', '100', 2, 64.57, 0, '5pcs', 4, 'F', NULL, NULL, NULL),
	(27, 'PAC-98-00', 'PH989704-0030', 'COVER', 'PH989704-0030 COVER', NULL, NULL, NULL, '1068', '866', '100', 2, 81.07, 0, '5pcs', 4, 'F', NULL, NULL, NULL),
	(28, 'PAC-137-00', 'PH989704-0390', 'T9AA MASTER BOX COVER', 'PH989704-0390 T9AA MASTER BOX COVER', NULL, NULL, NULL, '1185', '895', '100', 2, 68.59, 0, '5pcs', 4, 'F', NULL, NULL, NULL),
	(29, 'PAC-163-00', 'PH989705-0330', 'DANPLA STICK W/ CUSHION', 'PH989705-0330 DANPLA STICK W/ CUSHION', NULL, NULL, NULL, '422', '18', '0', 2, 12.70, 0, '30pcs', 4, 'B', NULL, NULL, NULL),
	(30, 'PAC-08-01', 'PH989918-0010', 'SLIP SHEET', 'PH989918-0010 SLIP SHEET', NULL, NULL, NULL, '1060', '1100', '0', 2, 63.74, 0, '20pcs', 4, 'M', NULL, NULL, NULL),
	(31, 'PAC-79-00', 'PH989933-0340', 'INSERT', 'PH989933-0340 INSERT', NULL, NULL, NULL, '200', '209', '0', 2, 2.14, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(32, 'PAC-102-00', 'PH989933-0500', 'PARTITION SET', 'PH989933-0500 PARTITION SET', NULL, NULL, NULL, '512', '434', '220', 2, 24.86, 0, '8sets', 4, 'B', NULL, NULL, NULL),
	(33, 'PAC-123-00', 'PH989933-0650', 'INSERT', 'PH989933-0650 INSERT', NULL, NULL, NULL, '420', '280', '260', 2, 28.93, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(34, 'PAC-124-00', 'PH989933-0660', 'INSERT', 'PH989933-0660 INSERT', NULL, NULL, NULL, '630', '130', '260', 2, 29.54, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(35, 'PAC-130-00', 'PH989933-0710', 'YLA PARTITION SET', 'PH989933-0710 YLA PARTITION SET', NULL, NULL, NULL, '544', '320', '158', 2, 22.00, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(36, 'PAC-140-00', 'PH989933-0720', 'PARTITION SET', 'PH989933-0720 PARTITION SET', NULL, NULL, NULL, '700', '520', '220', 2, 102.08, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(37, 'PAC-127-00', 'PH989933-0730', 'PARTITION SET', 'PH989933-0730 PARTITION SET', NULL, NULL, NULL, '380', '239-5', '98', 2, 19.44, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(38, 'PAC-125-00', 'PH989933-0740', 'PARTITION SET', 'PH989933-0740 PARTITION SET', NULL, NULL, NULL, '432', '260', '156', 2, 22.00, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(39, 'PAC-134-00', 'PH989933-0760', 'T9AA PARTITION SET', 'PH989933-0760 T9AA PARTITION SET', NULL, NULL, NULL, '706', '424', '246', 2, 42.68, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(40, 'PAC-135-00', 'PH989933-0770', 'T9AA CARTON INSERT', 'PH989933-0770 T9AA CARTON INSERT', NULL, NULL, NULL, '428', '140', '258', 2, 9.85, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(41, 'PAC-153-00', 'PH989933-0831', 'D87A PARTITION SET', 'PH989933-0831 D87A PARTITION SET', NULL, NULL, NULL, '614', '276', '163', 2, 20.30, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(42, 'PAC-151-00', 'PH989933-0841', 'D87A CARTON TOP INSERT', 'PH989933-0841 D87A CARTON TOP INSERT', NULL, NULL, NULL, '276', '39', '10', 2, 3.14, 0, '20pcs', 4, 'B', NULL, NULL, NULL),
	(43, 'PAC-142-00', 'PH989933-0850', 'T9AA SPARE PARTITION', 'PH989933-0850 T9AA SPARE PARTITION', NULL, NULL, NULL, '420', '280', '140', 2, 22.44, 0, '1set', 4, 'B', NULL, NULL, NULL),
	(44, 'PAC-145-01', 'PH989933-0860', 'INSERT', 'PH989933-0860 INSERT', NULL, NULL, NULL, '189', '120', '50', 2, 5.89, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(45, 'PAC-158-00', 'PH989933-0920', 'T9AA PARTITION SET', 'PH989933-0920 T9AA PARTITION SET', NULL, NULL, NULL, '691', '424', '246', 2, 41.94, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(46, 'PAC-166-00', 'PH989933-1000', 'HONDA PARTITION', 'PH989933-1000 HONDA PARTITION', NULL, NULL, NULL, '510', '430', '246', 2, 37.13, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(47, 'PAC-168-00', 'PH989933-1040', 'PARTITION SET', 'PH989933-1040 PARTITION SET', NULL, NULL, NULL, '610', '405', '252', 2, 46.37, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(48, 'PAC-80-00', 'PH989938-0130', 'TOP PAD', 'PH989938-0130 TOP PAD', NULL, NULL, NULL, '139', '130', '35', 2, 2.90, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(49, 'PAC-128-00', 'PH989938-0520', 'TOP PAD', 'PH989938-0520 TOP PAD', NULL, NULL, NULL, '340', '380', '0', 2, 4.36, 0, '50pcs', 4, 'B', NULL, NULL, NULL),
	(50, 'PAC-136-00', 'PH989938-0530', 'T9AA TOP SHEET PAD', 'PH989938-0530 T9AA TOP SHEET PAD', NULL, NULL, NULL, '1158', '872', '0', 2, 23.98, 0, '10pcs', 4, 'I', NULL, NULL, NULL),
	(51, 'PAC-167-00', 'PH989939-0140', 'HONDA BOTTOM INSERT', 'PH989939-0140 HONDA BOTTOM INSERT', NULL, NULL, NULL, '510', '67', '43', 2, 5.12, 0, '20pcs', 4, 'B', NULL, NULL, NULL),
	(52, 'PAC-155-00', 'PH989939-0860', 'D87A INSERT 1', 'PH989939-0860 D87A INSERT 1', NULL, NULL, NULL, '450', '160', '260', 2, 16.94, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(53, 'PAC-154-00', 'PH989939-0870', 'D87A INSERT 2', 'PH989939-0870 D87A INSERT 2', NULL, NULL, NULL, '620', '160', '160', 2, 19.09, 0, '10pcs', 4, 'B', NULL, NULL, NULL),
	(54, 'PAC-159-00', 'PH989939-0940', 'HONDA PARTITION SET', 'PH989939-0940 HONDA PARTITION SET', NULL, NULL, NULL, '610', '405', '252', 2, 46.37, 0, '10sets', 4, 'B', NULL, NULL, NULL),
	(59, 'MIT-04-02', 'FB001', 'HS Box', 'FB001 HS Box', NULL, NULL, NULL, '470', '235', '235', 2, 31.95, 340, '10pcs', 13, 'H', NULL, NULL, '2019-08-26 22:53:10'),
	(60, 'MIT-07-01', 'FB014', 'ETC Box New', 'FB014 ETC Box New', NULL, NULL, NULL, '453', '318', '133', 2, 35.64, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(61, 'MIT-32-03', 'FB026', 'W02A Master Box', 'FB026 W02A Master Box', NULL, NULL, NULL, '530', '220', '155', 2, 28.62, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(62, 'MIT-29-02', 'FB028', 'X61G Master Box', 'FB028 X61G Master Box', NULL, NULL, NULL, '520', '327', '155', 2, 40.05, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(63, 'MIT-40-00', 'FB031', 'MD5 Master Box', 'FB031 MD5 Master Box', NULL, NULL, NULL, '480', '462', '90', 2, 33.68, 0, '10pcs', 13, 'D', NULL, NULL, NULL),
	(64, 'MIT-54-00', 'FB033', 'WR15 Carton Box', 'FB033 WR15 Carton Box', NULL, NULL, NULL, '515', '333', '110', 2, 33.38, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(65, 'MIT-69-00', 'FB033-1', 'WR15 Open Box', 'FB033-1 WR15 Open Box', NULL, NULL, NULL, '508', '330', '95', 2, 18.85, 0, '25 pcs', 13, 'B', NULL, NULL, NULL),
	(66, 'MIT-50-01', 'FB035', 'P42M Master Box', 'FB035 P42M Master Box', NULL, NULL, NULL, '520', '327', '185', 2, 29.37, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(67, 'MIT-57-00', 'FB037', 'WR14 Master Box', 'FB037 WR14 Master Box', NULL, NULL, NULL, '465', '346', '92', 2, 27.80, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(68, 'MIT-71-00', 'FB042', 'WR12 Master Box', 'FB042 WR12 Master Box', NULL, NULL, NULL, '550', '373', '95', 2, 33.90, 0, '10pcs', 13, 'H', NULL, NULL, NULL),
	(69, 'MIT-24-00', 'FI009', 'Inner Divider', 'FI009 Inner Divider', NULL, NULL, NULL, '2136', '177', '0', 2, 15.20, 0, '50pcs', 13, 'B', NULL, NULL, NULL),
	(70, 'MIT-45-00', 'FP001-2', 'HS Pad', 'FP001-2 HS Pad', NULL, NULL, NULL, '460', '225', '0', 2, 5.50, 0, '50pcs', 13, 'A', NULL, NULL, NULL),
	(71, 'MIT-21-01', 'FP010', 'Standard Pad', 'FP010 Standard Pad', NULL, NULL, NULL, '436', '173', '0', 2, 2.45, 0, '50pcs', 13, 'B', NULL, NULL, NULL),
	(72, 'MIT-19-00', 'FP014', 'ETC Pad', 'FP014 ETC Pad', NULL, NULL, NULL, '445', '310', '0', 2, 4.68, 0, '50pcs', 13, 'A', NULL, NULL, NULL),
	(73, 'MIT-11-01', 'FP024', 'JCI Big Pad', 'FP024 JCI Big Pad', NULL, NULL, NULL, '507', '319', '0', 2, 3.90, 0, '50pcs', 13, 'A', NULL, NULL, NULL),
	(74, 'MIT-53-01', 'FP033', 'WR15 Pad', 'FP033 WR15 Pad', NULL, NULL, NULL, '330', '510', '0', 2, 5.20, 0, '50pcs', 13, 'A', NULL, NULL, NULL),
	(75, 'MIT-72-01', 'FP042', 'WR12 Layer Pad', 'FP042 WR12 Layer Pad', NULL, NULL, NULL, '368', '538', '0', 2, 6.55, 0, '50pcs', 13, 'A', NULL, NULL, NULL),
	(76, 'MIT-01-00', 'FR014', 'ETC Partition Set', 'FR014 ETC Partition Set', NULL, NULL, NULL, '445', '312', '56', 2, 11.26, 0, '50 sets', 13, 'B', NULL, NULL, NULL),
	(77, 'MIT-63-00', 'FR022-1', 'Imasen Divider 18SNP', 'FR022-1 Imasen Divider 18SNP', NULL, NULL, NULL, '1202', '508', '0', 2, 18.33, 0, '10 pcs', 13, 'B', NULL, NULL, NULL),
	(78, 'MIT-34-01', 'FR026', 'W02A Partition Set', 'FR026 W02A Partition Set', NULL, NULL, NULL, '520', '210', '140', 2, 8.90, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(79, 'MIT-28-00', 'FR028', 'X61G Partition', 'FR028 X61G Partition', NULL, NULL, NULL, '507', '319', '60', 2, 20.30, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(80, 'MIT-49-01', 'FR028-2', 'WM22-C Partition', 'FR028-2 WM22-C Partition', NULL, NULL, NULL, '508', '320', '134', 2, 28.60, 0, '5 sets', 13, 'B', NULL, NULL, NULL),
	(81, 'MIT-67-00', 'FR028-3', 'WM22-C Partition 6SNP', 'FR028-3WM22-C Partition 6SNP', NULL, NULL, NULL, '508', '320', '134', 2, 28.60, 0, '5 sets', 13, 'B', NULL, NULL, NULL),
	(82, 'MIT-41-00', 'FR031', 'MD5 Partition Set', 'FR031 MD5 Partition Set', NULL, NULL, NULL, '470', '456', '80', 2, 21.93, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(83, 'MIT-52-01', 'FR033-1', 'WR15 Partition Set', 'FR033-1 WR15 Partition Set', NULL, NULL, NULL, '510', '332', '100', 2, 26.00, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(84, 'MIT-68-00', 'FR033-2', ' WR15 Partition w/ Kraft Tape', 'FR033-2 WR15 Partition w/ Kraft Tape', NULL, NULL, NULL, '500', '325', '90', 2, 26.45, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(85, 'MIT-51-01', 'FR035-1', 'P42M Partition Set', 'FR035-1 P42M Partition Set', NULL, NULL, NULL, '510', '320', '162', 2, 42.52, 0, '5 sets', 13, 'B', NULL, NULL, NULL),
	(86, 'MIT-58-00', 'FR037', 'WR14 Partition Set', 'FR037 WR14 Partition Set', NULL, NULL, NULL, '457', '340', '76', 2, 23.67, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(87, 'MIT-70-00', 'FR042', 'WR12 Partition Set', 'FR042 WR12 Partition Set', NULL, NULL, NULL, '544', '369', '80', 2, 18.70, 0, '10 sets', 13, 'B', NULL, NULL, NULL),
	(90, 'IMI 20002-R2000021', '20002-R2000021', 'HID Project Top', '20002-R2000021 HID Project Top', NULL, NULL, NULL, '164mm', '114mm', '41mm', 2, 6.00, 0, '50pcs/bundle', 8, 'B', NULL, NULL, NULL),
	(141, 'Littel H12439', 'H12439', 'Switch Box #125', 'P/N H12439 Switch Box #125', '4.970"', '1.640"', '1.785"', NULL, NULL, NULL, 2, 15.70, 0, '20pcs/bundle', 10, 'B', NULL, '2019-07-01 17:45:48', NULL),
	(142, 'Aik S-00018972', 'S-00018972', 'Sumitomo Carton Box', 'S-00018972 Sumitomo Carton Box', NULL, NULL, NULL, '492mm', '394mm', '297mm', 2, 53.50, 0, '10pcs/ bundle', 1, 'D', NULL, '2019-07-23 01:21:27', NULL),
	(143, 'Zen- Sep B', 'N/A', 'Separator B', 'Separator B 285mm x 130mm', NULL, NULL, NULL, '285mm', '130mm', '0mm', 2, 0.90, 0, '50pcs', 45, 'B', NULL, '2019-07-23 23:44:01', NULL),
	(144, 'Aik S-00018973', 'S-00018973', 'Sumitomo Carton Pad', 'S-00018973 Sumitomo Carton Pad', NULL, NULL, NULL, '480mm', '380mm', '0mm', 2, 8.75, 0, '50pcs', 1, 'B', NULL, NULL, NULL),
	(145, 'Aik S-00018974', 'S-00018974', 'Sumitomo Partition', 'S-00018974 Sumitomo Partition', NULL, NULL, NULL, '484mm', '386mm', '135mm', 2, 20.04, 0, '10sets', 1, 'B', NULL, NULL, NULL),
	(146, 'Aik S-00020255', 'S-00020255', 'APM 023 Box', 'S-00020255 APM 023 Box', NULL, NULL, NULL, '525mm', '322mm', '266mm', 2, 41.80, 0, '10pcs', 1, 'F', NULL, NULL, NULL),
	(147, 'Aik S-00020256', 'S-00020256', ' APM 023 Pad', 'S-00020256 APM 023 Pad', NULL, NULL, NULL, '316mm', '519mm', '0mm', 2, 5.80, 0, '50pcs', 1, 'B', NULL, NULL, NULL),
	(148, 'Aik S-00025452', 'S-00025452', 'Sumitomo Partition Sets', 'S-00025452 Sumitomo Partition Sets', NULL, NULL, NULL, '484mm', '386mm', '135mm', 2, 32.25, 0, '10sets', 1, 'B', NULL, NULL, NULL),
	(149, 'Aik S-00025998', 'S-00025998', 'Sumitomo Partition Assy', 'S-00025998 Sumitomo Partition Assy 6358-6025/6024', NULL, NULL, NULL, '484mm', '387mm', '64mm', 2, 16.95, 0, '10sets', 1, 'B', NULL, NULL, NULL),
	(150, 'Aik S-00026098', 'S-00026098', 'Sumitomo Partition A', 'S-00026098 Sumitomo Partition A', NULL, NULL, NULL, '484mm', '387mm', '68mm', 2, 27.30, 0, '10sets', 1, 'B', NULL, NULL, NULL),
	(205, '20002-R2000022', '20002-R2000022', 'Master Box ', 'Master Box P/N 20002-R2000022', NULL, NULL, NULL, '745mm', '245mm', '282mm', 2, 51.46, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(206, '20012-R2000005', '20012-R2000005', 'Cor Box', 'Cor Box P/N 20012-R2000005', NULL, NULL, NULL, '558mm', '360mm', '217mm', 2, 51.40, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(207, '20012-R2000007', '20012-R2000007', 'Belimo Carton Box', 'Belimo Carton Box P/N 20012-R2000007', NULL, NULL, NULL, '516mm', '360mm', '215mm', 2, 56.85, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(208, '20016-2000003', '20016-2000003', 'Die Cut/Pad Die C', '20016-2000003 Die Cut/Pad Die C P/N', NULL, NULL, NULL, '310mm', '410mm', '0mm', 2, 5.53, 0, '100pcs', 8, 'B', NULL, NULL, NULL),
	(209, '20016-2000004', '20016-2000004', 'Pad Die Cut', 'Pad Die Cut P/N 20016-2000004', NULL, NULL, NULL, '312mm', '416mm', '0mm', 2, 5.70, 0, '100pcs', 8, 'B', NULL, NULL, NULL),
	(210, '20016-2000005', '20016-2000005', 'Pad CC', 'Pad CC P/N 20016-2000005', NULL, NULL, NULL, '283mm', '388mm', '0mm', 2, 4.65, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(211, '20016-2000011', '20016-2000011', 'Pad w/ Die Cut', '20016-2000011 Pad w/ Die-Cut P/N', NULL, NULL, NULL, '312mm', '416mm', '0mm', 2, 4.76, 0, '100pcs', 8, 'B', NULL, NULL, NULL),
	(212, '20016-2000017', '20016-2000017', 'Pad w/ Die Cut', '20016-2000017 Pad w/ Die Cut P/N', NULL, NULL, NULL, '340mm', '432mm', '0mm', 2, 14.90, 0, '100pcs', 8, 'B', NULL, NULL, NULL),
	(213, '20016-2000020', '20016-2000020', 'Partition', 'Partition P/N 20016-2000020', NULL, NULL, NULL, '385mm', '285mm', '31mm', 2, 10.45, 0, '10sets', 8, 'B', NULL, NULL, NULL),
	(214, '20016-R2000007', '20016-R2000007', 'Master Box', 'Master Box P/N 20016-R2000007', NULL, NULL, NULL, '590mm', '323mm', '152mm', 2, 41.77, 0, '10pcs', 8, 'H', NULL, NULL, NULL),
	(215, '20018-20000010', '20018-20000010', 'Jideco Box', '20018-20000010 Jideco Box P/N', NULL, NULL, NULL, '635mm', '302mm', '277mm', 2, 59.59, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(216, '20027-R2000103', '20027-R2000103', 'Carton Box CC CTN-3171EB', 'Carton Box CC CTN-3171EB P/N 20027-R2000103', NULL, NULL, NULL, '265mm', '275mm', '118mm', 2, 53.10, 0, '50pcs', 8, 'C', NULL, NULL, NULL),
	(217, '20027-R2000110', '20027-R2000110', 'PC-CH1P-K PACKIN', '20027-R2000110 PC-CH1P-K PACKIN P/N', NULL, NULL, NULL, '385mm', '210mm', '96mm', 2, 25.25, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(218, '20027-R2000114', '20027-R2000114', 'PC-TE82 PLA Carton Box', '20027-R2000114 PC-TE82 PLA Carton Box P/N', NULL, NULL, NULL, '185mm', '195mm', '68mm', 2, 16.10, 0, '50pcs', 8, 'C', NULL, NULL, NULL),
	(219, '20045-R1000887', '20045-R1000887', 'Siemens Master B', '20045-R1000887 Siemens Master B P/N', NULL, NULL, NULL, '644mm', '453mm', '365mm', 2, 95.65, 0, '5pcs', 8, 'D', NULL, NULL, NULL),
	(220, '20268+10014313', '20268+10014313', 'Led Master Box', '20889-R2000025 Led Master Box P/N', NULL, NULL, NULL, '530mm', '470mm', '300mm', 2, 85.55, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(221, '20371-R2000021', '20371-R2000021', 'Camera Gift Box', '20371-R2000021 Camera Gift Box P/N', NULL, NULL, NULL, '176mm', '113mm', '70mm', 2, 38.60, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(222, '20371-R2000022', '20371-R2000022', 'Camera Insert Box lef', 'Camera Insert Box left P/N 20371-R2000022', NULL, NULL, NULL, '109mm', '56mm', '65mm', 2, 8.60, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(223, '20371-R2000023', '20371-R2000023', 'Camera Insert Box Right ', 'Camera Insert Box Right P/N 20371-R2000023', NULL, NULL, NULL, '371mm', '56mm', '65mm', 2, 8.60, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(224, '20546-R2000002', '20546-R2000002', 'Master Box ', 'Master Box P/N 20546-R2000002', NULL, NULL, NULL, '344mm', '255mm', '218mm', 2, 37.00, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(225, '20546-R2000003', '20546-R2000003', 'Slave Box', 'Slave Box P/N 20546-R2000003', NULL, NULL, NULL, '324mm', '236mm', '95mm', 2, 20.50, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(226, '20546-R2000004', '20546-R2000004', 'Partition ', 'Partition P/N 20546-R2000004', NULL, NULL, NULL, '314mm', '228mm', '78mm', 2, 23.00, 0, '10sets', 8, 'B', NULL, NULL, NULL),
	(227, '20546-R2000005', '20546-R2000005', 'ZFLS PAD', '20546-R2000005 ZFLS PAD P/N', NULL, NULL, NULL, '228mm', '314mm', '0mm', 2, 3.50, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(228, '20572-R2000003', '20572-R2000003', 'Slave Box', '20572-R2000003 P/N Slave Box', NULL, NULL, NULL, '530mm', '342mm', '120mm', 2, 72.55, 0, '10pcs', 8, 'C', NULL, NULL, NULL),
	(229, '20572-R2000004', '20572-R2000004', 'Ficosa Pad', '20572-R2000004 Ficosa PadD P/N', NULL, NULL, NULL, '520mm', '338mm', '0mm', 2, 7.50, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(230, '20572-R2000005', '20572-R2000005', 'Partition', 'Partition P/N 20572-R2000005', NULL, NULL, NULL, '520mm', '338mm', '108mm', 2, 50.45, 0, '5sets', 8, 'B', NULL, NULL, NULL),
	(231, '20572-R2000012', '20572-R2000012', 'Master Box', 'Master Box P/N 20572-R2000012', NULL, NULL, NULL, '550mm', '365mm', '268mm', 2, 72.55, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(232, '20572-R2000013', '20572-R2000013', 'Ficosa Individual', '20572-R2000013 Ficosa Individual P/N', NULL, NULL, NULL, '530mm', '342mm', '120mm', 2, 33.85, 0, '10pcs', 8, 'C', NULL, NULL, NULL),
	(233, '20616-R2000007', '20616-R2000007', 'MPC 1Q Master Box', '20616-R2000007 MPC 1Q Master Box P/N', NULL, NULL, NULL, '550mm', '365mm', '230mm', 2, 52.50, 0, '10pcs', 8, 'H', NULL, NULL, NULL),
	(234, '20616-R2000014', '20616-R2000014', 'MPC 1Q Filler Separator', '20616-R2000014 MPC 1Q Filler Separator P/N', NULL, NULL, NULL, '136mm', '85mm', '0mm', 2, 3.45, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(235, '20616-R2000016', '20616-R2000016', 'Pad Insert', 'Pad Insert P/N 20616-R2000016', NULL, NULL, NULL, '166mm', '110mm', '63mm', 2, 6.00, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(236, '20616-R2000017', '20616-R2000017', 'MPC 1Q Insert', '20616-R2000017 MPC 1Q INSERT P/N', NULL, NULL, NULL, '166mm', '110mm', '65mm', 2, 5.70, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(237, '20681-20000288', '20681-20000288', 'Master Box', 'Master Box P/N 20681-20000288', NULL, NULL, NULL, '575mm', '230mm', '242mm', 2, 44.35, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(238, '20681-20000289', '20681-20000289', 'Slave Box', 'Slave Box P/N 20681-20000289', NULL, NULL, NULL, '557mm', '215mm', '42mm', 2, 22.94, 0, '50pcs', 8, 'C', NULL, NULL, NULL),
	(239, '20809-R1000581', '20809-R1000581', 'Car Orbit Main White', 'Car Orbit Main White (5CR39)  P/N 20809-R1000581', NULL, NULL, NULL, '268mm', '134mm', '74mm', 2, 12.70, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(240, '20809-R1000829', '20809-R1000829', 'Carton Box 538x218x205', 'Carton Box 538x218x205(5CR0152)  P/N 20809-R1000829', NULL, NULL, NULL, '221mm', '650mm', '205mm', 2, 44.90, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(241, '20809-R1001078', '20809-R1001078', 'Carton Box 5CR00', '20809-R1001078 Carton Box 5CR00 P/N', NULL, NULL, NULL, '420mm', '288mm', '480mm', 2, 56.80, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(242, '20809-R2000041', '20809-R2000041', 'CAR RSC D376 D.W.', 'CAR RSC D376 D.W.(5CRNOVA2P14) P/N 20809-R20000041', NULL, NULL, NULL, '433mm', '578mm', '652mm', 2, 170.00, 0, '5pcs', 8, 'D', NULL, NULL, NULL),
	(243, '20829-R2000003/R4600056', '20829-R2000003/R4600056', 'BOSCH MPC 2 MAST', '20829-R2000003 BOSCH MPC 2 MAST P/N', NULL, NULL, NULL, '680mm', '293mm', '143mm', 2, 59.40, 0, '10pcs', 8, 'H', NULL, NULL, NULL),
	(244, '20889-R2000017', '20889-R2000017', 'Luco Pd Partition', '20889-R2000017 Luco Pd Partition P/N', NULL, NULL, NULL, '525mm', '525mm', '165mm', 2, 57.40, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(245, '21250-R2000007', '21250-R2000007', 'Pick Up Plate For Led Tray', 'Pick Up Plate For Led Tray P/N', NULL, NULL, NULL, '516mm', '460mm', '275mm', 2, 28.50, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(246, '21250-R2000010', '21250-R2000010', 'Led Master Box', 'Led Master Box P/N 21250-R2000010  (530X470X300)', NULL, NULL, NULL, '192mm', '51mm', '90.5mm', 2, 106.80, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(247, '21343-N2000009', '21343-N2000009', 'Pad 530X350X', 'Pad 530x350OD P/N 21343-N2000009', NULL, NULL, NULL, '163.5mm', '36mm', '83mm', 2, 12.05, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(248, '21343-R1000567', '21343-R1000567', 'Product Box Rear Insert', 'Product Box Rear Insert P/N 21343-R1000567', NULL, NULL, NULL, '192mm', '35mm', '90.5mm', 2, 6.50, 0, '10pcs', 8, 'B', NULL, NULL, NULL),
	(249, '21343-R1000568', '21343-R1000568', 'Corrugated Insert', 'Corrugated Insert P/N 21343-R1000568', NULL, NULL, NULL, '89mm', '192mm', '0mm', 2, 1.95, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(250, '21343-R2000008', '21343-R2000008', 'Product Box Rear Insert', 'Product Box P/N 21343-R2000008', NULL, NULL, NULL, '284mm', '202mm', '161mm', 2, 54.50, 0, '25pcs', 8, 'C', NULL, NULL, NULL),
	(251, '21343-R2000011', '21343-R2000011', 'Product Box Top & Bottom Insert', 'Product Box Top & Bottom Insert P/N 21343-R2000011', NULL, NULL, NULL, '350mm', '32mm', '192mm', 2, 6.55, 0, '50pcs', 8, 'B', NULL, NULL, NULL),
	(252, '21343-R2000012', '21343-R2000012', 'Master Shipping Carton', '21343-R2000012 Master Shipping Carton P/N', NULL, NULL, NULL, '302mm', '336mm', '430mm', 2, 112.60, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(253, 'R4000105', 'R4000105', 'Short Partition', 'R4000105 Short Partition P/N ', NULL, NULL, NULL, '295mm', '130mm', '0mm', 2, 2.30, 0, '100sets', 8, 'B', NULL, NULL, NULL),
	(254, 'R4001982', 'R4001982', 'Slave Box W/ New IMI L', 'R4001982 Slave Box W/ New IMI L P/N  R4001982', NULL, NULL, NULL, '594mm', '224mm', '73mm', 2, 22.03, 0, '50pcs', 8, 'C', NULL, NULL, NULL),
	(255, 'R4002024/R4600059', 'R4002024/R4600059', 'Master Box', 'R4002024 Master Box P/N R4002024/R4600059', NULL, NULL, NULL, '679mm', '206mm', '223mm', 2, 25.58, 0, '10pcs', 8, 'D', NULL, NULL, NULL),
	(256, '20045-R1000885', '20045-R1000885', 'Box Master', '20045-R1000885 Box Master P/N', NULL, NULL, NULL, '685mm', '176mm', '270mm', 2, 37.15, 0, '50pcs', 8, 'D', NULL, NULL, NULL),
	(257, 'IMI 20027-R2000105', '20027-R2000105', 'CTN-TOBL- P Box Plain White', 'CTN-TOBL- P Box Plain White P/N 20027-R2000105', NULL, NULL, NULL, '258mm', '85mm', '55mm', 2, 11.85, 0, '100pcs', 8, 'P', NULL, '2019-07-29 01:29:01', NULL),
	(258, 'IMI 21191-R2000021', '21191-R2000021', 'Carton KR BC 518x310x308mm', 'Carton KR BC  P/N 21191-R2000021', NULL, NULL, NULL, '518mm', '310mm', '308mm', 2, 167.20, 0, '10pcs', 8, 'D', NULL, '2019-07-29 01:33:08', NULL),
	(259, 'IMI 21191-R2000015', '21191-R2000015', 'Eroad Insert 2 E Flute 275 x151 KL', '21191-R2000015 Eroad Insert 2', NULL, NULL, NULL, '151mm', '275mm', '0mm', 2, 5.50, 0, '25pcs', 8, 'B', NULL, '2019-07-29 03:32:40', NULL),
	(260, 'IMI 21191-R2000014', '21191-R2000014', 'Pad Insert 1 E Flute 384 x 189KL', 'Pad Insert 1 21191-R2000014', NULL, NULL, NULL, '189mm', '384mm', '0mm', 2, 10.00, 0, '25pcs', 8, 'B', NULL, '2019-07-31 19:56:42', NULL),
	(261, 'IMI 21191-R2000013', '21191-R2000013', 'Carton Gift E 161x288x70mm KL', '21191-R2000013 Carton Gift', NULL, NULL, NULL, '161mm', '288mm', '70mm', 2, 45.00, 0, '25pcs', 8, 'B', NULL, '2019-07-31 19:58:42', NULL),
	(262, 'Godo TD C 013', 'TD C 013', 'Inner Carton Box No. 2', 'TD C 013 Inner Carton Box No. 2', NULL, NULL, NULL, '255mm', '155mm', '110mm', 2, 16.30, 0, '50pcs', 6, 'B', NULL, '2019-07-31 20:02:36', NULL),
	(263, 'Nihon LAF331-000', 'LAF331-000', 'Pad 71x183 PH', 'LAF331-000 Pad', NULL, NULL, NULL, '183mm', '71mm', '0mm', 1, 0.02, 0, '50pcs', 15, 'B', NULL, '2019-07-31 20:05:54', NULL),
	(264, 'Nihon LW3380001', 'LW3380001', 'Cassette Spacer 7100', 'LW3380001 Cassette Spacer 7100', NULL, NULL, NULL, '50mm', '326mm', '0mm', 1, 0.03, 0, '50pcs', 15, 'B', NULL, '2019-07-31 20:08:41', NULL),
	(265, 'Littel 580773', '580-773', 'RSC', 'Netherland Box', NULL, NULL, NULL, '805mm', '475mm', '423mm', 2, 153.30, 0, '10pcs', 10, 'D', NULL, '2019-08-27 00:43:10', NULL),
	(266, 'Littel 580773S', '580-773S', 'RSC', '580-773S Shipping Box Small', NULL, NULL, NULL, '746mm', '371mm', '300mm', 2, 67.90, 0, '10pcs', 10, 'D', NULL, '2019-08-27 00:44:34', NULL);
/*!40000 ALTER TABLE `finishgoods` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.finishgoodsaudit
CREATE TABLE IF NOT EXISTS `finishgoodsaudit` (
  `auditid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fgid` int(11) NOT NULL,
  `jonumber` text COLLATE utf8mb4_unicode_ci,
  `drnumber` text COLLATE utf8mb4_unicode_ci,
  `qty` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `issueddate` date NOT NULL,
  `issuedtime` time NOT NULL,
  PRIMARY KEY (`auditid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.finishgoodsaudit: ~3 rows (approximately)
DELETE FROM `finishgoodsaudit`;
/*!40000 ALTER TABLE `finishgoodsaudit` DISABLE KEYS */;
INSERT INTO `finishgoodsaudit` (`auditid`, `fgid`, `jonumber`, `drnumber`, `qty`, `balance`, `issuedby`, `remarks`, `issueddate`, `issuedtime`) VALUES
	(1, 56, NULL, NULL, 9900, 9900, 'Vergara, Jay R P', NULL, '2019-07-01', '04:01:45'),
	(2, 59, 'JO201908210002', NULL, 340, 340, 'Vergara, Jay R P', NULL, '2019-08-27', '05:53:10'),
	(3, 8, 'JO08-19-00003', NULL, 596, 596, 'Pagkalinawan, Mary Grace ', NULL, '2019-08-27', '09:29:09');
/*!40000 ALTER TABLE `finishgoodsaudit` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.invoiceinformation
CREATE TABLE IF NOT EXISTS `invoiceinformation` (
  `invoiceid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoicenumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`invoiceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.invoiceinformation: ~0 rows (approximately)
DELETE FROM `invoiceinformation`;
/*!40000 ALTER TABLE `invoiceinformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoiceinformation` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.jobordermonitoring
CREATE TABLE IF NOT EXISTS `jobordermonitoring` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `currentpid` int(11) NOT NULL DEFAULT '0',
  `operator` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.jobordermonitoring: ~0 rows (approximately)
DELETE FROM `jobordermonitoring`;
/*!40000 ALTER TABLE `jobordermonitoring` DISABLE KEYS */;
INSERT INTO `jobordermonitoring` (`mid`, `joid`, `currentpid`, `operator`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 8, 'Del Mundo, Angelo Gabriel B', 'Open', '2019-10-18 00:37:14', '2020-02-26 20:40:30');
/*!40000 ALTER TABLE `jobordermonitoring` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.jobordermonitoringaudit
CREATE TABLE IF NOT EXISTS `jobordermonitoringaudit` (
  `auditid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `targetoutput` int(11) NOT NULL DEFAULT '0',
  `balance` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isshow` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`auditid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.jobordermonitoringaudit: ~5 rows (approximately)
DELETE FROM `jobordermonitoringaudit`;
/*!40000 ALTER TABLE `jobordermonitoringaudit` DISABLE KEYS */;
INSERT INTO `jobordermonitoringaudit` (`auditid`, `mid`, `pid`, `targetoutput`, `balance`, `created_at`, `updated_at`, `isshow`) VALUES
	(1, 1, 1, 1965, 1965, '2019-10-18 00:37:14', '2019-10-18 00:38:51', 1),
	(2, 1, 5, 0, 0, '2019-10-18 00:37:14', '2019-10-18 00:39:21', 1),
	(3, 1, 7, 20, 20, '2019-10-18 00:37:14', '2020-02-26 20:40:30', 1),
	(4, 1, 8, 10, 10, '2019-10-18 00:37:14', '2020-02-26 20:40:30', 1),
	(5, 1, 9, 0, 0, '2019-10-18 00:37:14', NULL, 0);
/*!40000 ALTER TABLE `jobordermonitoringaudit` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.jobordermonitoringaudititems
CREATE TABLE IF NOT EXISTS `jobordermonitoringaudititems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `auditid` int(11) NOT NULL,
  `operator` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `rejectqty` int(11) NOT NULL,
  `rejectid` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.jobordermonitoringaudititems: ~4 rows (approximately)
DELETE FROM `jobordermonitoringaudititems`;
/*!40000 ALTER TABLE `jobordermonitoringaudititems` DISABLE KEYS */;
INSERT INTO `jobordermonitoringaudititems` (`id`, `auditid`, `operator`, `qty`, `rejectqty`, `rejectid`, `created_at`) VALUES
	(1, 1, 'Del Mundo, Angelo Gabriel B', 10, 0, 0, '2019-10-18 07:37:57'),
	(2, 1, 'Del Mundo, Angelo Gabriel B', 20, 5, 1, '2019-10-18 07:38:51'),
	(3, 2, 'Del Mundo, Angelo Gabriel B', 30, 0, 0, '2019-10-18 07:39:21'),
	(4, 3, 'Del Mundo, Angelo Gabriel B', 10, 0, 0, '2020-02-26 20:40:30');
/*!40000 ALTER TABLE `jobordermonitoringaudititems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.joborderrminfo
CREATE TABLE IF NOT EXISTS `joborderrminfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `rmid` int(11) NOT NULL,
  `rmstockonhand` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.joborderrminfo: ~3 rows (approximately)
DELETE FROM `joborderrminfo`;
/*!40000 ALTER TABLE `joborderrminfo` DISABLE KEYS */;
INSERT INTO `joborderrminfo` (`id`, `joid`, `rmid`, `rmstockonhand`, `created_at`, `updated_at`) VALUES
	(1, 1, 31, 2, '2019-10-18 00:28:20', NULL),
	(2, 2, 1, 9, '2019-10-28 18:03:49', NULL),
	(3, 3, 9, 7, '2019-10-28 18:25:08', NULL),
	(4, 4, 1, 8, '2019-10-28 20:07:45', NULL);
/*!40000 ALTER TABLE `joborderrminfo` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.joborders
CREATE TABLE IF NOT EXISTS `joborders` (
  `joid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jorequestnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jonumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poid` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `requestid` int(11) NOT NULL,
  `delid` int(11) NOT NULL,
  `rmstock` int(11) NOT NULL,
  `style` text COLLATE utf8mb4_unicode_ci,
  `inkcolor` text COLLATE utf8mb4_unicode_ci,
  `specialinstruction` text COLLATE utf8mb4_unicode_ci,
  `targetoutput` int(11) NOT NULL,
  `qtyreceive` int(11) NOT NULL DEFAULT '0',
  `deliverydatefrom` date NOT NULL,
  `deliverydateto` date NOT NULL,
  `cid` int(11) NOT NULL,
  `requestby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `requestdate` datetime DEFAULT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`joid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.joborders: ~3 rows (approximately)
DELETE FROM `joborders`;
/*!40000 ALTER TABLE `joborders` DISABLE KEYS */;
INSERT INTO `joborders` (`joid`, `jorequestnumber`, `jonumber`, `poid`, `itemid`, `requestid`, `delid`, `rmstock`, `style`, `inkcolor`, `specialinstruction`, `targetoutput`, `qtyreceive`, `deliverydatefrom`, `deliverydateto`, `cid`, `requestby`, `requestdate`, `issuedby`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'JOR319290173', 'JO10-19-00002', 1, 1, 1, 0, 0, 'NOne', 'NONE', 'Cutting:  LENGTH FIRST:   \nL = 12.5" + 12.5" + 12.5"\nW = 23 + 23"', 2000, 0, '2019-10-18', '2019-10-26', 8, 'Quia, Rommel ', '2019-10-18 07:28:20', 'Del Mundo, Angelo Gabriel B', 'Open', '2019-10-18 00:28:20', '2019-10-18 00:37:09'),
	(2, 'JOR059580522', NULL, 1, 1, 3, 0, 0, NULL, NULL, NULL, 63, 0, '0000-00-00', '0000-00-00', 8, 'Del Mundo, Angelo Gabriel B', '2019-10-28 18:03:49', NULL, 'For Request', '2019-10-28 18:03:49', NULL),
	(3, 'JOR991909395', NULL, 1, 1, 6, 0, 0, NULL, NULL, NULL, 1, 0, '0000-00-00', '0000-00-00', 8, 'Del Mundo, Angelo Gabriel B', '2019-10-28 18:25:08', NULL, 'For Request', '2019-10-28 18:25:08', NULL),
	(4, 'JOR717566417', NULL, 1, 1, 5, 0, 0, NULL, NULL, NULL, 1, 0, '0000-00-00', '0000-00-00', 8, 'Del Mundo, Angelo Gabriel B', '2019-10-28 20:07:45', NULL, 'For Request', '2019-10-28 20:07:45', NULL);
/*!40000 ALTER TABLE `joborders` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.joborderssoinfo
CREATE TABLE IF NOT EXISTS `joborderssoinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `delid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.joborderssoinfo: ~0 rows (approximately)
DELETE FROM `joborderssoinfo`;
/*!40000 ALTER TABLE `joborderssoinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `joborderssoinfo` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.materialrequest
CREATE TABLE IF NOT EXISTS `materialrequest` (
  `requestid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mrinumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issuancenumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `rmid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prid` int(11) NOT NULL DEFAULT '0',
  `qtyrequired` int(11) NOT NULL,
  `drnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qtyissued` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `rmissuedby` text COLLATE utf8mb4_unicode_ci,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`requestid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.materialrequest: ~4 rows (approximately)
DELETE FROM `materialrequest`;
/*!40000 ALTER TABLE `materialrequest` DISABLE KEYS */;
INSERT INTO `materialrequest` (`requestid`, `mrinumber`, `issuancenumber`, `cid`, `fgid`, `rmid`, `prid`, `qtyrequired`, `drnumber`, `qtyissued`, `balance`, `rmissuedby`, `reason`, `issuedby`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'MR201910180001', 'ISN201910180726', 8, 253, '', 1, 0, NULL, NULL, NULL, 'Del Mundo, Angelo Gabriel B', 'Cutting:  LENGTH FIRST:   \nL = 12.5" + 12.5" + 12.5"\nW = 23 + 23"\nExcess:  46 x 10"', 'Quia, Rommel ', 'Done', '2019-10-18 00:26:02', '2019-10-18 00:28:20'),
	(3, 'MR201910280003', 'ISN201910280603', 8, 253, '', 0, 0, NULL, NULL, NULL, 'Del Mundo, Angelo Gabriel B', 'SAMPLE', 'Del Mundo, Angelo Gabriel B', 'Done', '2019-10-28 17:38:57', '2019-10-28 18:03:49'),
	(4, 'MR201910280004', NULL, 8, 253, '', 0, 0, NULL, NULL, NULL, NULL, 'sss', 'Del Mundo, Angelo Gabriel B', 'Pending', '2019-10-28 17:43:56', NULL),
	(5, 'MR201910280005', 'ISN201910280632', 8, 253, '', 0, 0, NULL, NULL, NULL, 'Del Mundo, Angelo Gabriel B', 'slhd', 'Del Mundo, Angelo Gabriel B', 'Done', '2019-10-28 18:02:53', '2019-10-28 20:07:45'),
	(6, 'MR201910280006', 'ISN201910280624', 8, 253, '', 0, 0, NULL, NULL, NULL, 'Del Mundo, Angelo Gabriel B', NULL, 'Del Mundo, Angelo Gabriel B', 'Done', '2019-10-28 18:10:39', '2019-10-28 18:25:09');
/*!40000 ALTER TABLE `materialrequest` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.materialrequestitems
CREATE TABLE IF NOT EXISTS `materialrequestitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mrid` int(11) DEFAULT NULL,
  `drnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rmid` int(11) NOT NULL,
  `qtyrequired` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci,
  `qtyissued` int(11) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.materialrequestitems: ~4 rows (approximately)
DELETE FROM `materialrequestitems`;
/*!40000 ALTER TABLE `materialrequestitems` DISABLE KEYS */;
INSERT INTO `materialrequestitems` (`id`, `mrid`, `drnumber`, `rmid`, `qtyrequired`, `remarks`, `qtyissued`, `balance`, `created_at`, `updated_at`) VALUES
	(1, 1, 'DR2543', 31, 83, 'Cutting:  LENGTH FIRST:   \r\nL = 12.5" + 12.5" + 12.5"\r\nW = 23 + 23"', 83, 2, '2019-10-18 00:26:02', '2019-10-18 00:26:27'),
	(3, 3, '1asd', 1, 1, 'samp', 1, 9, '2019-10-28 17:38:58', '2019-10-28 18:03:26'),
	(4, 4, NULL, 1, 1, '1', NULL, NULL, '2019-10-28 17:43:56', NULL),
	(5, 5, '1', 1, 2, '2', 1, 8, '2019-10-28 18:02:53', '2019-10-28 18:32:06'),
	(6, 6, '1', 9, 1, '1', 1, 7, '2019-10-28 18:10:39', '2019-10-28 18:24:48');
/*!40000 ALTER TABLE `materialrequestitems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.materialrequestitemsexcess
CREATE TABLE IF NOT EXISTS `materialrequestitemsexcess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mritemid` int(11) NOT NULL,
  `excessrmid` int(11) NOT NULL,
  `qtyexcessboard` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.materialrequestitemsexcess: ~0 rows (approximately)
DELETE FROM `materialrequestitemsexcess`;
/*!40000 ALTER TABLE `materialrequestitemsexcess` DISABLE KEYS */;
INSERT INTO `materialrequestitemsexcess` (`id`, `mritemid`, `excessrmid`, `qtyexcessboard`, `created_at`, `updated_at`) VALUES
	(1, 1, 84, 83, '2019-10-18 00:26:02', '2019-10-18 00:33:31');
/*!40000 ALTER TABLE `materialrequestitemsexcess` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.migrations: ~39 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(4, '2018_07_11_113230_suppliers', 1),
	(5, '2018_07_11_113657_customers', 1),
	(6, '2018_07_12_065828_currency', 1),
	(15, '2018_07_14_191651_fginventory', 4),
	(27, '2018_07_23_233654_poinformation', 6),
	(28, '2018_07_23_234325_poinformationitems', 6),
	(29, '2018_07_28_194750_rawmaterialaudit', 7),
	(30, '2018_07_19_212128_rawmaterials', 8),
	(31, '2018_08_08_050804_joborders', 9),
	(32, '2018_08_11_044153_materialrequest', 9),
	(33, '2018_08_18_174059_purchaserequest', 10),
	(34, '2018_08_25_051555_pocustomerinformation', 11),
	(36, '2018_08_25_052640_pocustomerinformationitems', 12),
	(37, '2018_09_02_010729_finishgoodsaudit', 13),
	(38, '2018_09_02_025251_qajoinformation', 14),
	(39, '2018_09_02_025348_qajoinformationitems', 14),
	(40, '2018_09_02_033514_qaitemdimension', 14),
	(41, '2018_09_02_045107_qaitemvisual', 14),
	(42, '2018_10_18_033710_deliveryinformation', 15),
	(43, '2018_10_18_034239_deliveryinformationitems', 15),
	(44, '2018_07_14_223953_processcode', 16),
	(45, '2018_11_24_070447_jobordermonitoring', 17),
	(46, '2018_11_24_073202_jobordermonitoringaudit', 17),
	(47, '2018_12_29_100838_invoiceinformation', 18),
	(48, '2019_01_06_045939_settings', 18),
	(49, '2019_02_02_020302_jobordermonitoringaudititems', 19),
	(50, '2019_02_09_012349_usersrestrictions', 20),
	(51, '2019_02_25_015540_proceestyle', 21),
	(52, '2019_03_09_114215_usergroups', 22),
	(53, '2019_03_09_114517_useraccess', 22),
	(54, '2019_03_09_114719_access', 22),
	(55, '2019_04_12_120602_pocustomerinformationitemdelivery', 23),
	(56, '2019_04_15_112613_deliverypreparationinformation', 24),
	(57, '2019_04_15_112642_deliverypreparationinformationitems', 24),
	(58, '2020_02_29_185333_qafinalinspectioninformation', 25),
	(59, '2020_02_29_185941_qafinalinspectionmeasuring', 25),
	(60, '2020_02_29_190220_qafinalinspectionvisual', 25),
	(61, '2020_02_29_190639_qafinalinspectionvisualothers', 25),
	(62, '2020_02_29_190841_qafinalinspectiondimension', 25);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.papercombination
CREATE TABLE IF NOT EXISTS `papercombination` (
  `papercomid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rmpapercombination` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flute` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `boardpound` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`papercomid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.papercombination: ~14 rows (approximately)
DELETE FROM `papercombination`;
/*!40000 ALTER TABLE `papercombination` DISABLE KEYS */;
INSERT INTO `papercombination` (`papercomid`, `rmpapercombination`, `flute`, `boardpound`, `created_at`, `updated_at`) VALUES
	(1, '175/175/175', 'C', '175#', '2019-06-04 04:21:43', NULL),
	(2, 'TL150-115/115/115/115', 'CB', '300#', '2019-06-26 22:19:11', NULL),
	(3, 'TL140-115-115-115-TL140', 'CB', '350#', '2019-07-01 03:59:42', '2019-07-23 02:23:48'),
	(4, 'TL150-115-TL150', 'B', '175#', '2019-07-22 02:38:17', '2019-07-23 20:00:35'),
	(5, 'TL150-115-TL150', 'C', '175#', '2019-07-22 02:56:14', NULL),
	(6, 'TL150-150-TL150', 'E', '200#', '2019-07-22 23:18:14', NULL),
	(7, 'TL150-115-115-115-115', 'CB', '350#', '2019-07-23 02:23:37', NULL),
	(8, 'TL140-115-TL140', 'C', '175#', '2019-07-23 02:54:59', NULL),
	(9, 'TL140-115-115-115-115', 'CB', '300#', '2019-07-23 02:55:38', NULL),
	(10, '115-115-115-115-115', 'CB', 'NT', '2019-07-23 20:13:45', NULL),
	(11, 'TL140-115-TL140', 'B', '175#', '2019-07-23 22:47:35', NULL),
	(12, 'TL150-115-115-115-TL150', 'CB', '300#', '2019-08-14 03:50:53', NULL),
	(13, '200-150-200', 'B', 'ECT32', '2019-08-14 03:54:44', NULL),
	(14, 'TL150-115-115-115-TL150', 'CB', '350#', '2019-08-14 20:35:06', NULL);
/*!40000 ALTER TABLE `papercombination` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.papercombinationprice
CREATE TABLE IF NOT EXISTS `papercombinationprice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `papercomid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.papercombinationprice: ~56 rows (approximately)
DELETE FROM `papercombinationprice`;
/*!40000 ALTER TABLE `papercombinationprice` DISABLE KEYS */;
INSERT INTO `papercombinationprice` (`id`, `papercomid`, `sid`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 3, 23, 0.00, '2019-07-17 22:28:35', NULL, NULL),
	(2, 4, 1, 0.00, '2019-07-22 19:38:45', NULL, '2019-07-22 19:38:45'),
	(3, 4, 1, 0.00, '2019-07-22 19:38:45', NULL, '2019-07-22 19:38:46'),
	(4, 4, 1, 0.00, '2019-07-22 19:38:46', NULL, '2019-07-22 19:38:46'),
	(5, 5, 1, 0.00, '2019-07-22 19:38:46', NULL, '2019-07-22 19:38:46'),
	(6, 4, 1, 0.00, '2019-07-22 19:38:46', NULL, '2019-07-22 19:38:48'),
	(7, 5, 1, 0.00, '2019-07-22 19:38:47', NULL, '2019-07-22 19:38:47'),
	(8, 5, 1, 0.00, '2019-07-22 19:38:47', NULL, '2019-07-22 19:38:47'),
	(9, 5, 1, 0.00, '2019-07-22 19:38:47', NULL, '2019-07-22 19:38:47'),
	(10, 5, 1, 0.00, '2019-07-22 19:38:47', NULL, '2019-07-22 19:38:48'),
	(11, 5, 1, 0.00, '2019-07-22 19:38:48', NULL, '2019-07-23 20:21:33'),
	(12, 4, 1, 0.00, '2019-07-22 19:38:48', NULL, '2019-07-22 19:38:48'),
	(13, 4, 1, 0.00, '2019-07-22 19:38:48', NULL, '2019-07-22 19:38:48'),
	(14, 4, 1, 0.00, '2019-07-22 19:38:48', NULL, '2019-07-23 20:03:03'),
	(15, 6, 4, 0.00, '2019-07-22 23:24:58', NULL, '2019-07-22 23:24:59'),
	(16, 5, 4, 0.00, '2019-07-22 23:24:59', NULL, '2019-07-23 01:37:53'),
	(17, 6, 4, 0.00, '2019-07-22 23:24:59', NULL, NULL),
	(18, 4, 4, 0.00, '2019-07-22 23:24:59', NULL, NULL),
	(19, 5, 4, 0.00, '2019-07-23 01:37:53', NULL, '2019-07-23 01:37:54'),
	(20, 5, 4, 0.00, '2019-07-23 01:37:54', NULL, '2019-07-23 01:37:54'),
	(21, 5, 4, 0.00, '2019-07-23 01:37:54', NULL, '2019-07-23 01:37:54'),
	(22, 5, 4, 0.00, '2019-07-23 01:37:54', NULL, '2019-07-23 01:37:55'),
	(23, 5, 4, 0.00, '2019-07-23 01:37:55', NULL, '2019-07-23 01:37:55'),
	(24, 5, 4, 0.00, '2019-07-23 01:37:55', NULL, '2019-07-23 01:37:55'),
	(25, 5, 4, 0.00, '2019-07-23 01:37:55', NULL, '2019-07-23 01:37:55'),
	(26, 5, 4, 0.00, '2019-07-23 01:37:55', NULL, '2019-07-23 01:37:56'),
	(27, 5, 4, 0.00, '2019-07-23 01:37:56', NULL, '2019-07-23 01:37:56'),
	(28, 5, 4, 0.00, '2019-07-23 01:37:56', NULL, NULL),
	(29, 2, 2, 0.00, '2019-07-23 02:32:50', NULL, '2019-07-23 02:32:50'),
	(30, 7, 2, 0.00, '2019-07-23 02:32:50', NULL, NULL),
	(31, 2, 2, 0.00, '2019-07-23 02:32:50', NULL, '2019-07-23 02:32:50'),
	(32, 2, 2, 0.00, '2019-07-23 02:32:50', NULL, NULL),
	(33, 9, 3, 0.00, '2019-07-23 02:58:16', NULL, '2019-07-23 02:58:16'),
	(34, 3, 3, 0.00, '2019-07-23 02:58:16', NULL, '2019-07-23 22:42:35'),
	(35, 9, 3, 0.00, '2019-07-23 02:58:16', NULL, '2019-07-23 02:58:17'),
	(36, 9, 3, 0.00, '2019-07-23 02:58:17', NULL, NULL),
	(37, 8, 3, 0.00, '2019-07-23 02:58:17', NULL, '2019-07-23 22:39:55'),
	(38, 4, 1, 0.00, '2019-07-23 20:03:03', NULL, '2019-07-23 20:03:03'),
	(39, 4, 1, 0.00, '2019-07-23 20:03:03', NULL, '2019-07-23 20:21:34'),
	(40, 5, 1, 0.00, '2019-07-23 20:21:33', NULL, '2019-07-23 20:21:33'),
	(41, 5, 1, 0.00, '2019-07-23 20:21:33', NULL, '2019-07-23 20:21:34'),
	(42, 10, 1, 0.00, '2019-07-23 20:21:34', NULL, NULL),
	(43, 5, 1, 0.00, '2019-07-23 20:21:34', NULL, '2019-07-23 20:21:34'),
	(44, 4, 1, 0.00, '2019-07-23 20:21:34', NULL, '2019-07-23 20:21:35'),
	(45, 5, 1, 0.00, '2019-07-23 20:21:34', NULL, NULL),
	(46, 4, 1, 0.00, '2019-07-23 20:21:35', NULL, '2019-07-23 20:25:32'),
	(47, 4, 1, 0.00, '2019-07-23 20:25:32', NULL, NULL),
	(48, 8, 3, 0.00, '2019-07-23 22:39:55', NULL, '2019-07-23 23:01:33'),
	(49, 3, 3, 0.00, '2019-07-23 22:42:35', NULL, '2019-07-23 23:01:32'),
	(50, 11, 3, 0.00, '2019-07-23 22:50:56', NULL, NULL),
	(51, 3, 3, 0.00, '2019-07-23 23:01:32', NULL, '2019-07-23 23:01:33'),
	(52, 3, 3, 0.00, '2019-07-23 23:01:33', NULL, '2019-07-23 23:01:33'),
	(53, 3, 3, 0.00, '2019-07-23 23:01:33', NULL, NULL),
	(54, 8, 3, 0.00, '2019-07-23 23:01:33', NULL, '2019-07-23 23:01:34'),
	(55, 8, 3, 0.00, '2019-07-23 23:01:34', NULL, '2019-07-23 23:01:34'),
	(56, 8, 3, 0.00, '2019-07-23 23:01:34', NULL, NULL);
/*!40000 ALTER TABLE `papercombinationprice` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.pocustomerinformation
CREATE TABLE IF NOT EXISTS `pocustomerinformation` (
  `poid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ponumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` int(11) NOT NULL,
  `grandtotal` decimal(10,2) NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `deliverydate` date DEFAULT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_openpo` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`poid`),
  UNIQUE KEY `pocustomerinformation_ponumber_unique` (`ponumber`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.pocustomerinformation: ~0 rows (approximately)
DELETE FROM `pocustomerinformation`;
/*!40000 ALTER TABLE `pocustomerinformation` DISABLE KEYS */;
INSERT INTO `pocustomerinformation` (`poid`, `ponumber`, `cid`, `grandtotal`, `remarks`, `deliverydate`, `issuedby`, `status`, `is_openpo`, `created_at`, `updated_at`) VALUES
	(1, '4500237412', 8, 10494.90, 'TEST', NULL, 'Deoda, Gerlie S', 'Open', 1, '2019-10-18 00:16:45', NULL);
/*!40000 ALTER TABLE `pocustomerinformation` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.pocustomerinformationitemdelivery
CREATE TABLE IF NOT EXISTS `pocustomerinformationitemdelivery` (
  `delid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salesnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `itemid` int(11) NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `deliverydate` date NOT NULL,
  `prid` int(11) NOT NULL DEFAULT '0',
  `mrid` int(11) NOT NULL DEFAULT '0',
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`delid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.pocustomerinformationitemdelivery: ~8 rows (approximately)
DELETE FROM `pocustomerinformationitemdelivery`;
/*!40000 ALTER TABLE `pocustomerinformationitemdelivery` DISABLE KEYS */;
INSERT INTO `pocustomerinformationitemdelivery` (`delid`, `salesnumber`, `itemid`, `deliveryqty`, `deliverydate`, `prid`, `mrid`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'SO201910180001', 1, 2000, '2019-11-20', 1, 0, 'Close', '2019-10-18 00:17:17', '2019-10-18 00:21:16', NULL),
	(2, 'SO201910180002', 1, 2500, '2019-11-21', 0, 2, 'Close', '2019-10-18 00:17:31', '2019-10-18 00:32:39', NULL),
	(3, 'SO201910280003', 1, 63, '2019-10-30', 0, 3, 'Close', '2019-10-28 17:37:25', '2019-10-28 17:38:58', NULL),
	(4, 'SO201910280004', 1, 1, '2019-10-31', 0, 4, 'Close', '2019-10-28 17:43:25', '2019-10-28 17:43:56', NULL),
	(5, 'SO201910280005', 1, 1, '2019-10-31', 0, 5, 'Close', '2019-10-28 18:01:32', '2019-10-28 18:02:53', NULL),
	(6, 'SO201910280006', 1, 1, '2019-11-14', 0, 6, 'Close', '2019-10-28 18:10:05', '2019-10-28 18:10:39', NULL),
	(7, 'SO202002050007', 1, 100, '2020-02-27', 4, 0, 'Close', '2020-02-05 18:47:27', '2020-02-05 18:51:47', NULL),
	(8, 'SO202002050008', 1, 100, '2020-02-20', 7, 0, 'Close', '2020-02-05 18:57:12', '2020-02-05 19:26:23', NULL);
/*!40000 ALTER TABLE `pocustomerinformationitemdelivery` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.pocustomerinformationitemdeliverydeleted
CREATE TABLE IF NOT EXISTS `pocustomerinformationitemdeliverydeleted` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `delid` int(11) NOT NULL,
  `reason` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `authuid` int(11) NOT NULL,
  `authname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deluid` int(11) NOT NULL,
  `delname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.pocustomerinformationitemdeliverydeleted: ~0 rows (approximately)
DELETE FROM `pocustomerinformationitemdeliverydeleted`;
/*!40000 ALTER TABLE `pocustomerinformationitemdeliverydeleted` DISABLE KEYS */;
/*!40000 ALTER TABLE `pocustomerinformationitemdeliverydeleted` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.pocustomerinformationitems
CREATE TABLE IF NOT EXISTS `pocustomerinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poid` int(11) NOT NULL,
  `fgid` int(11) NOT NULL,
  `currencyid` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `delqty` int(11) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.pocustomerinformationitems: ~0 rows (approximately)
DELETE FROM `pocustomerinformationitems`;
/*!40000 ALTER TABLE `pocustomerinformationitems` DISABLE KEYS */;
INSERT INTO `pocustomerinformationitems` (`itemid`, `poid`, `fgid`, `currencyid`, `rate`, `price`, `qty`, `total`, `delqty`) VALUES
	(1, 1, 253, 2, 1.00, 2.30, 7000, 10494.90, 0);
/*!40000 ALTER TABLE `pocustomerinformationitems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.poinformation
CREATE TABLE IF NOT EXISTS `poinformation` (
  `poid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ponumber` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attentionto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliveryto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `grandtotal` decimal(10,2) NOT NULL,
  `deliverycharge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sid` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `iuid` int(11) NOT NULL DEFAULT '0',
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `auid` int(11) NOT NULL DEFAULT '0',
  `approvedby` text COLLATE utf8mb4_unicode_ci,
  `approveddate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`poid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.poinformation: ~0 rows (approximately)
DELETE FROM `poinformation`;
/*!40000 ALTER TABLE `poinformation` DISABLE KEYS */;
INSERT INTO `poinformation` (`poid`, `ponumber`, `attentionto`, `deliveryto`, `grandtotal`, `deliverycharge`, `sid`, `remarks`, `note`, `iuid`, `issuedby`, `auid`, `approvedby`, `approveddate`, `created_at`, `updated_at`) VALUES
	(1, 'PO201910180001', 'GEPC', 'GEPC', 2280.55, 0.00, 4, 'Close', 'TEST', 1, 'Del Mundo, Angelo Gabriel B', 1, 'Del Mundo, Angelo Gabriel B', '2019-10-18 07:22:10', '2019-10-18 00:21:57', '2019-10-18 00:22:37');
/*!40000 ALTER TABLE `poinformation` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.poinformationitems
CREATE TABLE IF NOT EXISTS `poinformationitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poid` int(11) NOT NULL,
  `pritemid` int(11) NOT NULL DEFAULT '0',
  `prid` int(11) NOT NULL DEFAULT '0',
  `rmid` int(11) NOT NULL,
  `currencyid` int(11) NOT NULL,
  `rate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `papercomid` int(11) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `delqty` int(11) NOT NULL,
  `rejectqty` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.poinformationitems: ~0 rows (approximately)
DELETE FROM `poinformationitems`;
/*!40000 ALTER TABLE `poinformationitems` DISABLE KEYS */;
INSERT INTO `poinformationitems` (`itemid`, `poid`, `pritemid`, `prid`, `rmid`, `currencyid`, `rate`, `qty`, `papercomid`, `price`, `total`, `delqty`, `rejectqty`) VALUES
	(1, 1, 1, 0, 31, 2, '1.00', 85, 0, 26.83, 2280.55, 85, 0);
/*!40000 ALTER TABLE `poinformationitems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.poinformationitemsaudit
CREATE TABLE IF NOT EXISTS `poinformationitemsaudit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `drnumber` int(11) NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `rejectqty` int(11) NOT NULL,
  `goodqty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table db_accounting_management.poinformationitemsaudit: ~8 rows (approximately)
DELETE FROM `poinformationitemsaudit`;
/*!40000 ALTER TABLE `poinformationitemsaudit` DISABLE KEYS */;
INSERT INTO `poinformationitemsaudit` (`id`, `itemid`, `drnumber`, `deliveryqty`, `rejectqty`, `goodqty`, `created_at`, `updated_at`) VALUES
	(1, 49, 0, 200, 0, 200, '2019-08-20 21:07:39', NULL),
	(2, 50, 0, 305, 5, 300, '2019-08-20 21:07:39', NULL),
	(3, 59, 0, 193, 3, 190, '2019-08-27 01:18:12', NULL),
	(4, 60, 0, 95, 0, 95, '2019-08-27 01:18:12', NULL),
	(5, 61, 0, 130, 0, 130, '2019-08-27 01:19:34', NULL),
	(6, 62, 0, 300, 0, 300, '2019-08-27 01:19:34', NULL),
	(7, 59, 0, 3, 0, 3, '2019-08-27 01:20:25', NULL),
	(8, 1, 0, 85, 0, 85, '2019-10-18 00:22:37', NULL);
/*!40000 ALTER TABLE `poinformationitemsaudit` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.poinformationitemsdelivery
CREATE TABLE IF NOT EXISTS `poinformationitemsdelivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `deliverydate` date NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.poinformationitemsdelivery: ~0 rows (approximately)
DELETE FROM `poinformationitemsdelivery`;
/*!40000 ALTER TABLE `poinformationitemsdelivery` DISABLE KEYS */;
INSERT INTO `poinformationitemsdelivery` (`id`, `itemid`, `deliverydate`, `deliveryqty`, `created_at`, `updated_at`) VALUES
	(1, 1, '2019-10-18', 84, '2019-10-18 00:21:57', NULL);
/*!40000 ALTER TABLE `poinformationitemsdelivery` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.process
CREATE TABLE IF NOT EXISTS `process` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `process` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forcoc` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.process: ~9 rows (approximately)
DELETE FROM `process`;
/*!40000 ALTER TABLE `process` DISABLE KEYS */;
INSERT INTO `process` (`pid`, `process`, `forcoc`, `created_at`, `updated_at`) VALUES
	(1, 'Creasing', 0, '2018-10-23 15:14:04', NULL),
	(2, 'Printing', 0, '2018-10-23 15:14:17', NULL),
	(3, 'Slotting', 0, '2018-10-23 15:14:27', NULL),
	(4, 'Stitching', 0, '2018-10-23 15:14:37', NULL),
	(5, 'Die Cutting', 0, '2018-10-23 15:15:02', NULL),
	(6, 'Eyeletting', 0, '2018-10-23 15:15:14', NULL),
	(7, 'Assembly', 0, '2018-10-23 15:15:21', NULL),
	(8, 'Production Inspection', 1, '2018-10-23 15:15:33', '2020-02-26 20:33:50'),
	(9, 'Bundling Packing', 0, '2018-10-23 15:15:46', NULL);
/*!40000 ALTER TABLE `process` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.processcode
CREATE TABLE IF NOT EXISTS `processcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `processcode` varchar(10) NOT NULL,
  `processorder` int(11) NOT NULL,
  `processid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

-- Dumping data for table db_accounting_management.processcode: ~81 rows (approximately)
DELETE FROM `processcode`;
/*!40000 ALTER TABLE `processcode` DISABLE KEYS */;
INSERT INTO `processcode` (`id`, `processcode`, `processorder`, `processid`) VALUES
	(1, 'A', 1, 1),
	(2, 'A', 2, 8),
	(3, 'A', 3, 9),
	(4, 'B', 1, 1),
	(5, 'B', 2, 5),
	(8, 'C', 1, 1),
	(13, 'B', 3, 7),
	(14, 'B', 4, 8),
	(15, 'B', 5, 9),
	(16, 'C', 2, 2),
	(17, 'C', 3, 5),
	(18, 'C', 4, 7),
	(19, 'C', 5, 8),
	(20, 'C', 6, 9),
	(21, 'D', 1, 1),
	(22, 'D', 2, 2),
	(23, 'D', 3, 3),
	(24, 'D', 4, 7),
	(25, 'D', 5, 8),
	(26, 'D', 6, 9),
	(27, 'E', 1, 1),
	(28, 'E', 2, 2),
	(29, 'E', 3, 3),
	(30, 'E', 4, 4),
	(31, 'E', 5, 8),
	(32, 'E', 6, 9),
	(33, 'F', 1, 1),
	(34, 'F', 2, 3),
	(35, 'F', 3, 4),
	(36, 'F', 4, 8),
	(37, 'F', 5, 9),
	(38, 'G', 1, 1),
	(39, 'G', 2, 5),
	(40, 'G', 3, 2),
	(41, 'G', 4, 7),
	(42, 'G', 5, 8),
	(43, 'G', 6, 9),
	(44, 'H', 1, 1),
	(45, 'H', 2, 3),
	(46, 'H', 3, 7),
	(47, 'H', 4, 8),
	(48, 'H', 5, 9),
	(49, 'I', 1, 1),
	(50, 'I', 2, 7),
	(51, 'I', 3, 8),
	(52, 'I', 4, 9),
	(53, 'J', 1, 1),
	(54, 'J', 2, 4),
	(55, 'J', 3, 8),
	(56, 'J', 4, 9),
	(57, 'K', 1, 1),
	(58, 'K', 2, 3),
	(59, 'K', 3, 8),
	(60, 'K', 4, 9),
	(61, 'L', 1, 1),
	(62, 'L', 2, 5),
	(63, 'L', 3, 4),
	(64, 'L', 4, 8),
	(65, 'L', 5, 9),
	(66, 'M', 1, 1),
	(67, 'M', 2, 5),
	(68, 'M', 3, 6),
	(69, 'M', 4, 7),
	(70, 'M', 5, 8),
	(71, 'M', 6, 9),
	(72, 'N', 1, 1),
	(73, 'N', 2, 5),
	(74, 'N', 3, 2),
	(75, 'N', 4, 4),
	(76, 'N', 5, 8),
	(77, 'N', 6, 9),
	(78, 'O', 1, 1),
	(79, 'O', 2, 2),
	(80, 'O', 3, 5),
	(81, 'O', 4, 4),
	(82, 'O', 5, 8),
	(83, 'O', 6, 9),
	(84, 'P', 1, 5),
	(85, 'P', 2, 7),
	(86, 'P', 3, 8),
	(87, 'P', 4, 9);
/*!40000 ALTER TABLE `processcode` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.processstyle
CREATE TABLE IF NOT EXISTS `processstyle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `processcode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `style` text COLLATE utf8mb4_unicode_ci,
  `inkcolor` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.processstyle: ~18 rows (approximately)
DELETE FROM `processstyle`;
/*!40000 ALTER TABLE `processstyle` DISABLE KEYS */;
INSERT INTO `processstyle` (`id`, `processcode`, `style`, `inkcolor`) VALUES
	(1, 'M', 'RSC', 'Black'),
	(2, 'L', 'DIECUT', 'White'),
	(3, 'A', 'none', 'none'),
	(4, 'B', 'none', 'none'),
	(5, 'C', 'none', 'none'),
	(6, 'D', 'none', 'none'),
	(7, 'E', 'none', 'none'),
	(8, 'F', 'none', 'none'),
	(9, 'G', 'none', 'none'),
	(10, 'H', 'none', 'none'),
	(11, 'I', 'none', 'none'),
	(12, 'J', 'none', 'none'),
	(13, 'K', 'none', 'none'),
	(14, 'L', 'none', 'none'),
	(15, 'M', 'none', 'none'),
	(16, 'N', 'none', 'none'),
	(17, 'O', 'none', 'none'),
	(18, 'P', 'none', 'none');
/*!40000 ALTER TABLE `processstyle` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.purchaserequest
CREATE TABLE IF NOT EXISTS `purchaserequest` (
  `prid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prnumber` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rmid` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qtyrequired` int(11) NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prid`),
  UNIQUE KEY `purchaserequest_prnumber_unique` (`prnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.purchaserequest: ~5 rows (approximately)
DELETE FROM `purchaserequest`;
/*!40000 ALTER TABLE `purchaserequest` DISABLE KEYS */;
INSERT INTO `purchaserequest` (`prid`, `prnumber`, `rmid`, `qtyrequired`, `reason`, `issuedby`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'PR201910180001', '', 0, 'IMI- R4000105', 'Quia, Rommel ', 'Done', '2019-10-18 00:20:07', '2019-10-18 00:21:57'),
	(2, 'PR202002050002', '', 0, 'SAMPLE AGAIN', 'Del Mundo, Angelo Gabriel B', 'Pending', '2020-02-05 18:47:48', NULL),
	(4, 'PR202002050004', '', 0, 'dasdas', 'Del Mundo, Angelo Gabriel B', 'Pending', '2020-02-05 18:51:28', NULL),
	(6, 'PR202002050006', '', 0, NULL, 'Del Mundo, Angelo Gabriel B', '', '2020-02-05 19:11:29', NULL),
	(7, 'PR202002050007', '', 0, 'GELO', 'Del Mundo, Angelo Gabriel B', 'Pending', '2020-02-05 19:16:58', NULL);
/*!40000 ALTER TABLE `purchaserequest` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.purchaserequestitems
CREATE TABLE IF NOT EXISTS `purchaserequestitems` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prid` int(11) DEFAULT NULL,
  `rmid` int(11) NOT NULL,
  `qtyrequired` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.purchaserequestitems: ~0 rows (approximately)
DELETE FROM `purchaserequestitems`;
/*!40000 ALTER TABLE `purchaserequestitems` DISABLE KEYS */;
INSERT INTO `purchaserequestitems` (`itemid`, `prid`, `rmid`, `qtyrequired`, `created_at`, `updated_at`) VALUES
	(1, 1, 31, 85, '2019-10-18 00:20:56', NULL),
	(2, 7, 1, 100, '2020-02-05 19:17:25', NULL);
/*!40000 ALTER TABLE `purchaserequestitems` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.purchaserequestitemsdelivery
CREATE TABLE IF NOT EXISTS `purchaserequestitemsdelivery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemid` int(11) NOT NULL,
  `deliverydate` date NOT NULL,
  `deliveryqty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.purchaserequestitemsdelivery: ~0 rows (approximately)
DELETE FROM `purchaserequestitemsdelivery`;
/*!40000 ALTER TABLE `purchaserequestitemsdelivery` DISABLE KEYS */;
INSERT INTO `purchaserequestitemsdelivery` (`id`, `itemid`, `deliverydate`, `deliveryqty`, `created_at`, `updated_at`) VALUES
	(1, 1, '2019-10-18', 84, '2019-10-18 00:21:11', NULL),
	(2, 2, '2020-02-13', 100, '2020-02-05 19:26:12', NULL);
/*!40000 ALTER TABLE `purchaserequestitemsdelivery` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.qafinalinspectiondimension
CREATE TABLE IF NOT EXISTS `qafinalinspectiondimension` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qafiid` int(11) NOT NULL,
  `length` decimal(11,2) NOT NULL,
  `width` decimal(11,2) NOT NULL,
  `height` decimal(11,2) NOT NULL,
  `remarks` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.qafinalinspectiondimension: ~0 rows (approximately)
DELETE FROM `qafinalinspectiondimension`;
/*!40000 ALTER TABLE `qafinalinspectiondimension` DISABLE KEYS */;
/*!40000 ALTER TABLE `qafinalinspectiondimension` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.qafinalinspectioninformation
CREATE TABLE IF NOT EXISTS `qafinalinspectioninformation` (
  `qafiid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `joid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `tolerance` decimal(11,2) NOT NULL,
  `outputqty` int(11) NOT NULL,
  `insideoutside` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flute` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thickness` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmdate` datetime NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`qafiid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.qafinalinspectioninformation: ~0 rows (approximately)
DELETE FROM `qafinalinspectioninformation`;
/*!40000 ALTER TABLE `qafinalinspectioninformation` DISABLE KEYS */;
/*!40000 ALTER TABLE `qafinalinspectioninformation` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.qafinalinspectionmeasuring
CREATE TABLE IF NOT EXISTS `qafinalinspectionmeasuring` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qafiid` int(11) NOT NULL,
  `metertape` int(11) NOT NULL DEFAULT '0',
  `caliper` int(11) NOT NULL DEFAULT '0',
  `meterstick` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.qafinalinspectionmeasuring: ~0 rows (approximately)
DELETE FROM `qafinalinspectionmeasuring`;
/*!40000 ALTER TABLE `qafinalinspectionmeasuring` DISABLE KEYS */;
/*!40000 ALTER TABLE `qafinalinspectionmeasuring` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.qafinalinspectionvisual
CREATE TABLE IF NOT EXISTS `qafinalinspectionvisual` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qafiid` int(11) NOT NULL,
  `rejecttypeid` int(11) NOT NULL,
  `value` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.qafinalinspectionvisual: ~0 rows (approximately)
DELETE FROM `qafinalinspectionvisual`;
/*!40000 ALTER TABLE `qafinalinspectionvisual` DISABLE KEYS */;
/*!40000 ALTER TABLE `qafinalinspectionvisual` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.qafinalinspectionvisualothers
CREATE TABLE IF NOT EXISTS `qafinalinspectionvisualothers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `qafiid` int(11) NOT NULL,
  `others` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.qafinalinspectionvisualothers: ~0 rows (approximately)
DELETE FROM `qafinalinspectionvisualothers`;
/*!40000 ALTER TABLE `qafinalinspectionvisualothers` DISABLE KEYS */;
/*!40000 ALTER TABLE `qafinalinspectionvisualothers` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.qarejecttype
CREATE TABLE IF NOT EXISTS `qarejecttype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rejectcode` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdby` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.qarejecttype: ~24 rows (approximately)
DELETE FROM `qarejecttype`;
/*!40000 ALTER TABLE `qarejecttype` DISABLE KEYS */;
INSERT INTO `qarejecttype` (`id`, `rejectcode`, `reject`, `createdby`, `created_at`, `updated_at`) VALUES
	(1, 'V1', 'Damaged', 14, '2019-08-27 01:07:56', '2020-02-24 19:20:48'),
	(2, 'V2', 'Dent', 14, '2019-08-27 01:09:16', '2020-02-24 19:21:01'),
	(3, 'V3', 'Crumpled', 1, '2020-02-24 19:21:26', NULL),
	(4, 'V4', 'Weak Flute glue', 1, '2020-02-24 19:51:45', NULL),
	(5, 'V5', 'Dirt', 1, '2020-02-24 19:52:07', NULL),
	(6, 'V6', 'Stain', 1, '2020-02-24 19:52:21', NULL),
	(7, 'V7', 'Warp / Deformed', 1, '2020-02-24 19:52:36', NULL),
	(8, 'V8', 'Crushed Flute', 1, '2020-02-24 19:52:50', NULL),
	(9, 'V9', 'Misaligned Board', 1, '2020-02-24 19:53:03', NULL),
	(10, 'V10', 'Wrong Orientation', 1, '2020-02-24 19:53:23', NULL),
	(11, 'V11', 'Wrong Flute Direction', 1, '2020-02-24 19:53:46', NULL),
	(12, 'V12', 'Double Image', 1, '2020-02-24 19:54:09', NULL),
	(13, 'V13', 'Wrong dimension', 1, '2020-02-24 19:54:25', NULL),
	(14, 'V14', 'Detached / Incomplete parts', 1, '2020-02-24 19:54:53', NULL),
	(15, 'V15', 'Incomplete / No print', 1, '2020-02-24 19:55:13', NULL),
	(16, 'V16', 'Smeared print', 1, '2020-02-24 19:55:27', NULL),
	(17, 'V17', 'Illegible print', 1, '2020-02-24 19:56:03', NULL),
	(18, 'V18', 'Faded printing', 1, '2020-02-24 19:56:21', NULL),
	(19, 'V19', 'Over / Under slot', 1, '2020-02-24 19:56:37', NULL),
	(20, 'V20', 'Poor / uncut parts', 1, '2020-02-24 19:56:58', NULL),
	(21, 'V21', 'Cut / Crack parts', 1, '2020-02-24 19:57:17', '2020-02-24 19:58:31'),
	(22, 'V22', 'Excess Glue', 1, '2020-02-24 19:58:46', NULL),
	(23, 'V23', 'Weak Glue Joint Tab', 1, '2020-02-24 19:59:01', NULL),
	(24, 'V24', 'Misaligned joint', 1, '2020-02-24 19:59:17', NULL);
/*!40000 ALTER TABLE `qarejecttype` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.rawmaterialaudit
CREATE TABLE IF NOT EXISTS `rawmaterialaudit` (
  `auditid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rmid` int(11) NOT NULL,
  `ponumber` text COLLATE utf8mb4_unicode_ci,
  `drnumber` text COLLATE utf8mb4_unicode_ci,
  `requisitionnumber` text COLLATE utf8mb4_unicode_ci,
  `qty` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `issuedby` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci,
  `issueddate` date NOT NULL,
  `issuedtime` time NOT NULL,
  PRIMARY KEY (`auditid`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.rawmaterialaudit: ~65 rows (approximately)
DELETE FROM `rawmaterialaudit`;
/*!40000 ALTER TABLE `rawmaterialaudit` DISABLE KEYS */;
INSERT INTO `rawmaterialaudit` (`auditid`, `rmid`, `ponumber`, `drnumber`, `requisitionnumber`, `qty`, `balance`, `issuedby`, `remarks`, `issueddate`, `issuedtime`) VALUES
	(1, 7, NULL, NULL, NULL, 3840, 3840, 'Vergara, Jay R P', NULL, '2019-07-02', '00:42:13'),
	(2, 7, NULL, NULL, NULL, 700, 3140, 'Vergara, Jay R P', NULL, '2019-07-02', '00:51:01'),
	(3, 8, NULL, NULL, NULL, 1031, 1031, 'Vergara, Jay R P', NULL, '2019-07-02', '07:36:28'),
	(4, 8, NULL, NULL, NULL, 420, 611, 'Vergara, Jay R P', NULL, '2019-07-02', '07:36:51'),
	(5, 9, NULL, NULL, NULL, 10, 10, 'Vergara, Jay R P', NULL, '2019-07-18', '05:30:09'),
	(6, 5, 'PO201907180001', 'SAMPLE01', NULL, 1000, 1000, 'Vergara, Jay R P', NULL, '2019-07-18', '05:30:56'),
	(7, 5, NULL, NULL, 'MR201907180014', 1000, 0, 'Vergara, Jay R P', NULL, '2019-07-18', '05:45:29'),
	(8, 10, NULL, NULL, NULL, 1000, 1000, 'Vergara, Jay R P', NULL, '2019-07-18', '05:45:48'),
	(9, 10, NULL, NULL, NULL, 0, 0, 'Vergara, Jay R P', NULL, '2019-07-23', '03:25:32'),
	(10, 34, 'PO201907230003', '140887', NULL, 979, 979, 'Avila, Ronald ', NULL, '2019-07-25', '05:28:52'),
	(11, 34, 'PO201907230003', '140887', NULL, 979, 1958, 'Avila, Ronald ', NULL, '2019-07-25', '05:29:13'),
	(12, 34, 'PO201907230003', '140887', NULL, 979, 2937, 'Avila, Ronald ', NULL, '2019-07-25', '05:29:16'),
	(13, 21, 'PO201908090004', '284742', NULL, 1460, 1460, 'Avila, Ronald ', NULL, '2019-08-15', '01:08:58'),
	(14, 13, 'PO201908090004', '284743', NULL, 1417, 1334, 'Avila, Ronald ', NULL, '2019-08-15', '01:11:12'),
	(15, 23, 'PO201908090004', '284744', NULL, 980, 980, 'Avila, Ronald ', NULL, '2019-08-15', '01:25:10'),
	(16, 20, 'PO201908090004', '284745', NULL, 1300, 1300, 'Avila, Ronald ', NULL, '2019-08-15', '01:25:58'),
	(17, 15, 'PO201908090004', '285419', NULL, 380, 380, 'Avila, Ronald ', NULL, '2019-08-15', '01:27:22'),
	(18, 42, 'PO201908140005', '285420', NULL, 1020, 1004, 'Avila, Ronald ', NULL, '2019-08-15', '01:35:17'),
	(19, 14, 'PO201908090004', '285421', NULL, 1000, 1000, 'Avila, Ronald ', NULL, '2019-08-15', '01:36:10'),
	(20, 25, 'PO201908150009', '141215', NULL, 297, 297, 'Avila, Ronald ', NULL, '2019-08-15', '05:53:16'),
	(21, 82, 'PO201908150014', '141216', NULL, 993, 993, 'Avila, Ronald ', NULL, '2019-08-15', '05:54:14'),
	(22, 30, 'PO201908150010', '141217', NULL, 977, 977, 'Avila, Ronald ', NULL, '2019-08-15', '05:54:57'),
	(23, 24, 'PO201908150015', '141168', NULL, 1364, 1364, 'Avila, Ronald ', NULL, '2019-08-15', '05:56:01'),
	(24, 35, 'PO201908150010', '141169', NULL, 556, 556, 'Avila, Ronald ', NULL, '2019-08-15', '05:56:41'),
	(25, 32, 'PO201908150010', '141177', NULL, 980, 980, 'Avila, Ronald ', NULL, '2019-08-15', '05:57:55'),
	(26, 35, 'PO201908150010', '141177', NULL, 362, 918, 'Avila, Ronald ', NULL, '2019-08-15', '05:59:21'),
	(27, 25, 'PO201908150009', '141178', NULL, 683, 980, 'Avila, Ronald ', NULL, '2019-08-15', '05:59:58'),
	(28, 18, 'PO201908090004', '141178', NULL, 683, 683, 'Avila, Ronald ', NULL, '2019-08-15', '06:01:04'),
	(29, 17, 'PO201908090004', '285402', NULL, 1620, 1620, 'Avila, Ronald ', NULL, '2019-08-15', '06:01:46'),
	(30, 12, 'PO201908090004', '285403', NULL, 940, 940, 'Avila, Ronald ', NULL, '2019-08-15', '06:02:16'),
	(31, 16, 'PO201908090004', '285404', NULL, 1000, 1000, 'Avila, Ronald ', NULL, '2019-08-15', '06:02:57'),
	(32, 40, 'PO201908150014', '141289', NULL, 984, 984, 'Avila, Ronald ', NULL, '2019-08-15', '06:03:53'),
	(33, 81, 'PO201908150014', '141289', NULL, 655, 655, 'Avila, Ronald ', NULL, '2019-08-15', '06:04:25'),
	(34, 36, 'PO201908150010', '141358', NULL, 906, 906, 'Avila, Ronald ', NULL, '2019-08-15', '06:05:25'),
	(35, 70, 'PO201908140007', '286159', NULL, 3304, 3304, 'Avila, Ronald ', NULL, '2019-08-15', '06:06:22'),
	(36, 75, 'PO201908150011', '141396', NULL, 1485, 1485, 'Avila, Ronald ', NULL, '2019-08-15', '06:07:06'),
	(37, 34, 'PO201908150010', '141397', NULL, 961, 3898, 'Avila, Ronald ', NULL, '2019-08-15', '06:08:12'),
	(38, 69, 'PO201908140006', '286371', NULL, 2040, 2040, 'Avila, Ronald ', NULL, '2019-08-15', '06:14:09'),
	(39, 68, 'PO201908140006', '286374', NULL, 650, 650, 'Avila, Ronald ', NULL, '2019-08-15', '06:17:24'),
	(40, 70, 'PO201908140007', '286372', NULL, 1460, 4764, 'Avila, Ronald ', NULL, '2019-08-15', '06:18:19'),
	(41, 35, NULL, NULL, 'MR201908150001', 300, 618, 'Avila, Ronald ', NULL, '2019-08-15', '06:27:56'),
	(42, 3, NULL, NULL, 'MR201908170003', 1, -1, 'Del Mundo, Angelo Gabriel B', NULL, '2019-08-17', '02:20:41'),
	(43, 43, 'PO201908150012', 'SAMPLE', NULL, 200, 200, 'Vergara, Jay R P', NULL, '2019-08-21', '04:07:38'),
	(44, 76, 'PO201908150012', 'SAMPLE', NULL, 305, 300, 'Vergara, Jay R P', NULL, '2019-08-21', '04:07:39'),
	(45, 1, NULL, NULL, NULL, 100, 100, 'Vergara, Jay R P', NULL, '2019-08-21', '04:35:00'),
	(46, 1, NULL, NULL, 'MR201908210002', 100, 0, 'Vergara, Jay R P', NULL, '2019-08-21', '04:43:51'),
	(47, 78, 'PO201908270016', 'Sample01', NULL, 190, 190, 'Avila, Ronald ', NULL, '2019-08-27', '08:18:12'),
	(48, 79, 'PO201908270016', 'Sample01', NULL, 95, 95, 'Avila, Ronald ', NULL, '2019-08-27', '08:18:12'),
	(49, 35, 'PO201908270017', 'Sample02', NULL, 130, 748, 'Avila, Ronald ', NULL, '2019-08-27', '08:19:34'),
	(50, 35, 'PO201908270017', 'Sample02', NULL, 300, 1048, 'Avila, Ronald ', NULL, '2019-08-27', '08:19:34'),
	(51, 78, 'PO201908270016', 'Sample02', NULL, 3, 193, 'Avila, Ronald ', NULL, '2019-08-27', '08:20:25'),
	(52, 35, NULL, NULL, 'MR201908270003', 300, 748, 'Avila, Ronald ', NULL, '2019-08-27', '08:27:32'),
	(53, 83, NULL, NULL, NULL, 300, 300, 'Avila, Ronald ', NULL, '2019-08-27', '08:29:07'),
	(54, 31, 'PO201910180001', 'DR25245', NULL, 85, 85, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-18', '07:22:37'),
	(55, 31, NULL, NULL, 'MR201910180001', 83, 2, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-18', '07:26:27'),
	(56, 84, NULL, NULL, NULL, 83, 83, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-18', '07:33:31'),
	(57, 84, NULL, NULL, 'MR201910180002', 83, 0, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-18', '07:33:53'),
	(58, 1, NULL, NULL, NULL, 1, 1, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '17:40:25'),
	(59, 1, NULL, NULL, 'MR201910280003', 1, 0, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '17:40:43'),
	(60, 1, NULL, NULL, NULL, 10, 10, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '18:02:01'),
	(61, 1, NULL, NULL, 'MR201910280003', 1, 9, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '18:03:26'),
	(62, 9, NULL, NULL, 'MR201910280006', 1, 9, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '18:16:26'),
	(63, 9, NULL, NULL, 'MR201910280006', 1, 8, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '18:19:28'),
	(64, 9, NULL, NULL, 'MR201910280006', 1, 7, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '18:24:48'),
	(65, 1, NULL, NULL, 'MR201910280005', 1, 8, 'Del Mundo, Angelo Gabriel B', NULL, '2019-10-28', '18:32:06');
/*!40000 ALTER TABLE `rawmaterialaudit` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.rawmaterials
CREATE TABLE IF NOT EXISTS `rawmaterials` (
  `rmid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `itemcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `width` text COLLATE utf8mb4_unicode_ci,
  `widthsize` text COLLATE utf8mb4_unicode_ci,
  `length` text COLLATE utf8mb4_unicode_ci,
  `lengthsize` text COLLATE utf8mb4_unicode_ci,
  `flute` text COLLATE utf8mb4_unicode_ci,
  `boardpound` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(10,2) DEFAULT NULL,
  `stockonhand` int(11) NOT NULL DEFAULT '0',
  `currencyid` int(11) DEFAULT NULL,
  `category` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`rmid`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.rawmaterials: ~84 rows (approximately)
DELETE FROM `rawmaterials`;
/*!40000 ALTER TABLE `rawmaterials` DISABLE KEYS */;
INSERT INTO `rawmaterials` (`rmid`, `itemcode`, `name`, `description`, `width`, `widthsize`, `length`, `lengthsize`, `flute`, `boardpound`, `price`, `stockonhand`, `currencyid`, `category`, `created_at`, `updated_at`) VALUES
	(1, NULL, '100 x 100', NULL, '100', NULL, '100', NULL, 'B', '100', 50.00, 8, 2, 'Raw Material', '2019-04-21 20:45:44', '2019-10-28 18:32:06'),
	(2, NULL, 'Corr. Board 624 mm x 1484 mm', '624 mm x 1484 mm', '624', NULL, '1484', NULL, 'C', '175#', 10.06, 0, 2, 'Raw Material', '2019-06-03 00:57:52', '2019-07-29 06:00:13'),
	(3, NULL, 'Corr. Board CB-300# 58 x 49"', 'CB-300# 58" X 49"', '58', NULL, '49', NULL, 'CB', '300#', 55.00, -1, 2, 'Raw Material', '2019-06-26 20:35:51', '2019-08-16 19:20:41'),
	(4, NULL, 'Corr. Board CB-300# 42 x 48.5"', 'CB-300# 42" x 48.5"', '42', NULL, '48', '50', 'CB', '300#', 37.06, 0, 2, 'Raw Material', '2019-06-26 20:36:56', '2019-07-29 06:00:13'),
	(5, NULL, 'Corr. Board CB-350# 51" x 74.25"', 'CB-350# 51" x 74.25"', '51', NULL, '74.25', NULL, 'CB', '350#', 71.53, 0, 2, 'Raw Material', '2019-07-01 03:57:07', '2019-07-17 22:45:29'),
	(6, NULL, 'Corr. Board CB- 350# 51" x 84.5"', 'CB- 350# 51" x 84.5"', '51', NULL, '84', '5', 'CB', '350#', 81.40, 0, 2, 'Raw Material', '2019-07-01 03:58:11', NULL),
	(7, NULL, 'Corr. Board E- 200# 45" x 42.5"', 'E- 200# 45" x 42.5"', '45', NULL, '42', '5', 'E', '200#', 27.89, 3140, 2, 'Raw Material', '2019-07-01 17:41:04', '2019-07-01 17:51:01'),
	(8, NULL, 'Corr. Board C-175# 40" x 42"', 'C-175# 40" x 42"', '40', NULL, '42', NULL, 'C', '175#', 21.23, 611, 2, 'Raw Material', '2019-07-01 23:43:51', '2019-07-02 00:36:51'),
	(9, NULL, NULL, '100 x 100', '100', '', '100', '', 'C', '175#', 0.00, 7, 1, 'Excess Board', '2019-07-16 07:55:05', '2019-10-28 18:24:48'),
	(10, NULL, NULL, '51" x 33.5" CB 350#', '51"', '', '33', '5"CB350#', 'CB', '350', 0.00, 0, 1, 'Excess Board', '2019-07-17 22:39:27', '2019-07-22 20:25:32'),
	(11, NULL, 'Corr. Board B-175# 39" x 43"', 'B-175# 39" x 43"', '39', NULL, '43', NULL, 'B', '175#', 20.73, 0, 2, 'Raw Material', '2019-07-22 02:36:05', '2019-08-09 01:01:08'),
	(12, NULL, 'Corr. Board B-175# 39" x 39"', 'B-175# 39" x 39"', '39', NULL, '39', NULL, 'B', '175#', 18.80, 940, 2, 'Raw Material', '2019-07-22 02:39:33', '2019-08-14 23:02:15'),
	(13, NULL, 'Corr. Board B-175# 49" x 60"', 'B-175# 49" x 60"', '49', NULL, '60', NULL, 'B', '175#', 36.34, 1334, 2, 'Raw Material', '2019-07-22 02:46:29', '2019-08-14 18:11:12'),
	(14, NULL, 'Corr. Board C-175# 36" x 68.5"', 'C-175# 36" x 68.5"', '36', NULL, '68.5', NULL, 'C', '175#', 30.48, 1000, 2, 'Raw Material', '2019-07-22 02:55:41', '2019-08-14 18:36:10'),
	(15, NULL, 'Corr. Board C-175# 36" x 66"', 'C-175# 36" x 66"', '36', NULL, '66', NULL, 'C', '175#', 29.37, 380, 2, 'Raw Material', '2019-07-22 02:57:30', '2019-08-14 18:27:22'),
	(16, NULL, 'Corr. Board C-175# 36" x 46"', 'C-175# 36" x 46"', '36', NULL, '46', NULL, 'C', '175#', 20.47, 1000, 2, 'Raw Material', '2019-07-22 03:03:33', '2019-08-14 23:02:57'),
	(17, NULL, 'Corr. Board C-175# 46" x 44"', 'C-175# 46" x 44"', '46', NULL, '44', NULL, 'C', '175#', 25.02, 1620, 2, 'Raw Material', '2019-07-22 03:04:27', '2019-08-14 23:01:46'),
	(18, NULL, 'Corr. Board C-175# 37" x 43"', 'C-175# 37" x 43"', '37', NULL, '43', NULL, 'C', '175#', 19.67, 683, 2, 'Raw Material', '2019-07-22 03:08:31', '2019-08-14 23:01:04'),
	(19, NULL, 'Corr. Board C-175# 41"x 39"', 'C-175# 41"x 39"', '41', NULL, '39', NULL, 'C', '175#', 19.77, 0, 2, 'Raw Material', '2019-07-22 03:09:35', '2019-08-09 01:01:08'),
	(20, NULL, 'Corr. Board B-175# 47"x39"', 'B-175# 47"x39"', '47', NULL, '39', NULL, 'B', '175#', 22.66, 1300, 2, 'Raw Material', '2019-07-22 03:14:37', '2019-08-14 18:25:58'),
	(21, NULL, 'Corr. Board B-175# 55" x 43"', 'B-175# 55" x 43"', '55', NULL, '43', NULL, 'B', '175#', 29.33, 1460, 2, 'Raw Material', '2019-07-22 17:51:08', '2019-08-14 18:08:58'),
	(22, NULL, 'Corr. Board B-175# 40" x 34"', 'B-175# 40" x 34"', '40', NULL, '34', NULL, 'B', '175#', 16.81, 0, 2, 'Raw Material', '2019-07-22 17:54:59', '2019-08-09 01:01:08'),
	(23, NULL, 'Corr. Board C-175# 40" x 36"', 'C-175# 40" x 36"', '40', NULL, '36', NULL, 'B', '175#', 17.80, 980, 2, 'Raw Material', '2019-07-22 17:57:02', '2019-08-14 18:25:10'),
	(24, NULL, 'Corr. Board B-175# 68" x 63"', 'B-175# 68" x 63"', '68', NULL, '63', NULL, 'B', '175#', 52.06, 1364, 2, 'Raw Material', '2019-07-22 22:52:17', '2019-08-14 22:56:01'),
	(25, NULL, 'Corr. Board C-175# 46" x 80.5"', 'C-175# 46" x 80.5"', '46', NULL, '80', '50', 'C', '175#', 45.00, 980, 2, 'Raw Material', '2019-07-22 22:54:43', '2019-08-14 22:59:58'),
	(26, NULL, 'Corr. Board E-200# 46" x 42.5"', 'E-200# 46" x 42.5"', '46', NULL, '42', '50', 'E', '200#', 27.70, 0, 2, 'Raw Material', '2019-07-22 22:59:36', '2019-08-14 20:22:19'),
	(27, NULL, 'Corr. Board E-200# 36" x 51"', 'E-200# 36" x 51"', '36', NULL, '51', NULL, 'E', '200#', 26.01, 0, 2, 'Raw Material', '2019-07-22 23:00:35', '2019-08-14 20:22:19'),
	(28, NULL, 'Corr. Board C-175# 38" x 74.5"', 'C-175# 38" x 74.5"', '38', NULL, '74', '50', 'C', '175#', 34.40, 0, 2, 'Raw Material', '2019-07-23 00:42:06', '2019-08-14 20:26:25'),
	(29, NULL, 'Corr. Board C-175# 42" x 68.5"', 'C-175# 42" x 68.5"', '42', NULL, '68', '50', 'C', '175#', 34.96, 0, 2, 'Raw Material', '2019-07-23 00:50:30', '2019-08-14 20:26:25'),
	(30, NULL, 'Corr. Board C-175# 46" x 77"', 'C-175# 46" x 77"', '46', NULL, '77', NULL, 'C', '175#', 43.05, 977, 2, 'Raw Material', '2019-07-23 00:51:40', '2019-08-14 22:54:57'),
	(31, NULL, 'Corr. Board C-175# 46" x 48"', 'C-175# 46" x 48"', '46', NULL, '48', NULL, 'C', '175#', 26.83, 2, 2, 'Raw Material', '2019-07-23 00:52:28', '2019-10-18 00:26:27'),
	(32, NULL, 'Corr. Board C-175# 48" x 77.25"', 'C-175# 48" x 77.25"', '48', NULL, '77', '25', 'C', '175#', 45.06, 980, 2, 'Raw Material', '2019-07-23 00:53:43', '2019-08-14 22:57:55'),
	(33, NULL, 'Corr. Board C-175# 50" x 37"', 'C-175# 50" x 37"', '50', NULL, '37', NULL, 'C', '175#', 22.48, 0, 2, 'Raw Material', '2019-07-23 00:54:39', '2019-08-14 20:26:26'),
	(34, NULL, 'Corr. Board C-175# 50" x 58"', 'C-175# 50" x 58"', '50', NULL, '58', NULL, 'C', '175#', 35.24, 3898, 2, 'Raw Material', '2019-07-23 00:55:42', '2019-08-14 23:08:11'),
	(35, NULL, 'Corr. Board C-175# 54" x 77.25"', 'C-175# 54" x 77.25"', '54', NULL, '77', '25', 'C', '175#', 50.70, 748, 2, 'Raw Material', '2019-07-23 00:56:37', '2019-08-27 01:27:32'),
	(36, NULL, 'Corr. Board C-175# 56" x 36.5"', 'C-175# 56" x 36.5"', '56', NULL, '36', '50', 'C', '175#', 24.84, 906, 2, 'Raw Material', '2019-07-23 00:57:30', '2019-08-14 23:05:25'),
	(37, NULL, 'Corr. Board C-175# 58" x 43"', 'C-175# 58" x 43"', '58', NULL, '43', NULL, 'C', '175#', 30.31, 0, 2, 'Raw Material', '2019-07-23 00:58:41', '2019-08-14 20:26:26'),
	(38, NULL, 'Corr. Board CB-300# 58" x 45.5"', 'CB-300# 58" x 45.5"', '58', NULL, '45', '50', 'CB', '300#', 48.02, 0, 2, 'Raw Material', '2019-07-23 02:15:21', '2019-07-23 18:11:31'),
	(39, NULL, 'Corr. Board CB-300# 58" x 39"', 'CB-300# 58" x 39"', '58', NULL, '39', NULL, 'CB', '300#', 41.16, 0, 2, 'Raw Material', '2019-07-23 02:17:13', '2019-07-23 02:32:50'),
	(40, NULL, 'Corr. Board CB-300# 40" x 69"', 'CB-300# 40" x 69"', '40', NULL, '69', NULL, 'CB', '300#', 50.22, 984, 2, 'Raw Material', '2019-07-23 02:18:51', '2019-08-14 23:03:53'),
	(41, NULL, 'Corr. Board CB-350# 45" x 51"', 'CB-350# 45" x 51"', '45', NULL, '51', NULL, 'CB', '350#', 43.35, 0, 2, 'Raw Material', '2019-07-23 02:22:44', '2019-07-23 18:12:12'),
	(42, NULL, 'Corr. Board C-175# 61" x 71"', 'C-175# 61" x 71"', '61', NULL, '71', NULL, 'C', '175#', 53.54, 1004, 2, 'Raw Material', '2019-07-23 02:41:27', '2019-08-14 18:35:17'),
	(43, NULL, 'Corr. Board CB-300# 38" x 70"', 'CB-300# 38" x 70"', '38', NULL, '70', NULL, 'CB', '300#', 48.40, 200, 2, 'Raw Material', '2019-07-23 02:42:44', '2019-08-20 21:07:38'),
	(44, NULL, 'Corr. Board CB-300# 51" x 64"', 'CB-300# 51" x 64"', '51', '"', '64', '"', 'CB', '300#', 59.39, 0, 2, 'Raw Material', '2019-07-23 02:43:48', '2019-07-23 18:13:30'),
	(45, NULL, 'Corr. Board CB-300# 65 " x 84.5"', 'CB-300# 65 " x 84.5"', '65', NULL, '84', '50', 'CB', '300#', 99.93, 0, 2, 'Raw Material', '2019-07-23 02:45:59', '2019-07-23 18:14:09'),
	(46, NULL, 'Corr. Board CB-350# 65" x 84.5"', 'CB-350# 65" x 84.5"', '65', NULL, '84', '50', 'CB', '350#', 65.44, 0, 2, 'Raw Material', '2019-07-23 02:47:04', NULL),
	(47, NULL, 'Corr. Board CB-350# 41" x 84.5"', 'CB-350# 41" x 84.5"', '41', NULL, '84', '50', 'CB', '350#', 65.44, 0, 2, 'Raw Material', '2019-07-23 02:48:27', '2019-07-23 18:14:45'),
	(48, NULL, 'Corr. Board B-175# 40" x 52"', 'B-175# 40" x 52"', '40', NULL, '52', NULL, 'B', '175#', 25.71, 0, 2, 'Raw Material', '2019-07-23 19:42:04', '2019-08-01 02:49:05'),
	(49, NULL, 'Corr. Board B-175# 52" x 38"', 'B-175# 52" x 38"', '52', NULL, '38', NULL, 'B', '175#', 24.43, 0, 2, 'Raw Material', '2019-07-23 19:57:04', '2019-08-01 02:49:05'),
	(50, NULL, 'Corr. Board B-175# 44" x 36"', 'B-175# 44" x 36"', '44', NULL, '36', NULL, 'B', '175#', 19.58, 0, 2, 'Raw Material', '2019-07-23 20:05:32', '2019-07-23 20:21:35'),
	(51, NULL, 'Corr. Board B-175# 62"x 30"', 'B-175# 62"x 30"', '62', NULL, '30', NULL, 'B', '175#', 22.99, 0, 2, 'Raw Material', '2019-07-23 20:06:24', '2019-08-14 04:18:56'),
	(52, NULL, 'Corr. Board C-175# 41" x 42"', 'C-175# 41" x 42"', '41', NULL, '42', NULL, 'C', '175#', 21.29, 0, 2, 'Raw Material', '2019-07-23 20:07:26', '2019-07-23 20:21:34'),
	(53, NULL, 'Corr. Board C-175# 49" x 44"', 'C-175# 49" x 44"', '49', NULL, '44', NULL, 'C', '175#', 26.65, 0, 2, 'Raw Material', '2019-07-23 20:09:26', '2019-07-23 20:21:34'),
	(54, NULL, 'Corr. Board CBNT 39" x 57"', 'CBNT 39" x 57"', '39', NULL, '57', NULL, 'CB', 'NT', 39.37, 0, 2, 'Raw Material', '2019-07-23 20:10:57', '2019-07-23 20:21:34'),
	(55, NULL, 'Corr. Board C-175# 49" x 43.5"', 'C-175# 49" x 43.5"', '49', NULL, '43', '50', 'C', '175#', 26.35, 0, 2, 'Raw Material', '2019-07-23 20:13:12', '2019-07-23 20:21:33'),
	(56, NULL, 'Corr. Board B-175# 48" x 46.5"', 'B-175# 48" x 46.5"', '48', NULL, '46', '50', 'B', '175#', 27.59, 0, 2, 'Raw Material', '2019-07-23 20:24:08', '2019-08-09 00:57:34'),
	(57, NULL, 'Corr. Board C-175# 45" x 57"', 'C-175# 45" x 57"', '45', NULL, '57', NULL, 'C', '175#', 34.20, 0, 2, 'Raw Material', '2019-07-23 20:28:13', '2019-07-23 22:39:55'),
	(58, NULL, 'Corr. Board CB-350# 48" x 72.5 "', 'CB-350# 48" x 72.5 "', '48', NULL, '72', '50', 'CB', '350#', 71.29, 0, 2, 'Raw Material', '2019-07-23 20:45:48', '2019-07-23 22:42:35'),
	(59, NULL, 'Corr. Board C-175# 41" x 47"', 'C-175# 41" x 47"', '41', NULL, '47', NULL, 'C', '175#', 23.82, 0, 2, 'Raw Material', '2019-07-23 20:49:00', '2019-08-14 03:47:56'),
	(60, NULL, 'Corr. Board C-175# 41" x 58"', 'C-175# 41" x 58"', '41', NULL, '58', NULL, 'C', '175#', 29.39, 0, 2, 'Raw Material', '2019-07-23 20:50:14', '2019-08-14 03:47:56'),
	(61, NULL, 'Corr. Board C-175# 41" x 53"', 'C-175# 41" x 53"', '41', NULL, '53', NULL, 'C', '175#', 26.86, 0, 2, 'Raw Material', '2019-07-23 20:51:18', '2019-08-14 03:47:57'),
	(62, NULL, 'Corr. Board CB-350# 41" x 58"', 'CB-350# 41" x 58"', '41', NULL, '58', NULL, 'CB', '350#', 44.92, 0, 2, 'Raw Material', '2019-07-23 20:52:38', '2019-07-23 23:01:33'),
	(63, NULL, 'Corr. Board CB-350# 43" x 51"', 'CB-350# 43" x 51"', '43', NULL, '51', NULL, 'CB', '350#', 41.42, 0, 2, 'Raw Material', '2019-07-23 20:53:39', '2019-07-23 23:01:33'),
	(64, NULL, 'Corr. Board CB-350# 45 " x 79"', 'CB-350# 45 " x 79"', '45', NULL, '79', NULL, 'CB', '350#', 67.15, 0, 2, 'Raw Material', '2019-07-23 20:54:39', '2019-07-23 23:01:33'),
	(65, NULL, 'Corr. Board CB-300# 51" x 58.5"', 'CB-300# 51" x 58.5"', '51', NULL, '58', '5', 'CB', '300#', 54.28, 0, 2, 'Raw Material', '2019-08-06 03:38:55', '2019-08-06 03:44:16'),
	(66, NULL, 'Corr. Board CB-300# 65" x 84"', 'CB-300# 65" x 84"', '65', NULL, '84', NULL, 'CB', '300#', 99.34, 0, 2, 'Raw Material', '2019-08-08 23:53:15', '2019-08-09 00:57:34'),
	(67, NULL, 'Corr. Board B-175# 40" x 36"', 'B-175# 40" x 36"', '40', NULL, '36', NULL, 'B', '175#', 17.80, 0, 2, 'Raw Material', '2019-08-09 00:00:04', NULL),
	(68, NULL, 'Corr. Board CB-300# 40" x 57"', 'CB-300# 40" x 57"', '40', NULL, '57', NULL, 'CB', '300#', 41.48, 650, 2, 'Raw Material', '2019-08-09 02:53:47', '2019-08-14 23:17:24'),
	(69, NULL, 'Corr. Board C-175# 46" x 42.5"', 'C-175# 46" x 42.5"', '46', NULL, '42', '5', 'C', '175#', 24.17, 2040, 2, 'Raw Material', '2019-08-09 02:54:49', '2019-08-14 23:14:09'),
	(70, NULL, 'Corr. Board B-ECT32 55" x 36"', 'B-ECT32 55" x 36"', '55', NULL, '36', NULL, 'B', 'ECT32', 31.63, 4764, 2, 'Raw Material', '2019-08-14 03:53:23', '2019-08-14 23:18:19'),
	(71, NULL, 'Corr. Board B-175# 55" x 57"', 'B-175# 55" x 57"', '55', NULL, '57', NULL, 'B', '175#', 38.75, 0, 2, 'Raw Material', '2019-08-14 03:58:22', '2019-08-14 04:18:56'),
	(72, NULL, 'Corr. Board B-175# 47" x 38"', 'B-175# 47" x 38"', '47', NULL, '38', NULL, 'B', '175#', 22.08, 0, 2, 'Raw Material', '2019-08-14 03:59:16', '2019-08-14 04:18:56'),
	(73, NULL, 'Corr. Board C-175# 43" x 52"', 'C-175# 43" x 52"', '43', NULL, '52', NULL, 'C', '175#', 27.64, 0, 2, 'Raw Material', '2019-08-14 04:00:40', '2019-08-14 04:18:57'),
	(74, NULL, 'Corr. Board C-175# 48" x 68.5"', NULL, '48', NULL, '68', '5', 'C', '175#', 40.64, 0, 2, 'Raw Material', '2019-08-14 04:01:37', '2019-08-14 04:18:57'),
	(75, NULL, 'Corr. Board B-175# 48" x 38"', 'B-175# 48" x 38"', '48', NULL, '38', NULL, 'B', '175#', 22.17, 1485, 2, 'Raw Material', '2019-08-14 20:02:18', '2019-08-14 23:07:06'),
	(76, NULL, 'Corr. Board CB-300# 52" x 64"', 'CB-300# 52" x 64"', '52', NULL, '64', NULL, 'CB', '300#', 60.55, 300, 2, 'Raw Material', '2019-08-14 20:04:49', '2019-08-20 21:07:39'),
	(77, NULL, 'Corr. Board CB-300# 66" x 84.5"', 'CB-300# 66" x 84.5"', '66', NULL, '84', '5', 'CB', '300#', 101.47, 0, 2, 'Raw Material', '2019-08-14 20:06:06', '2019-08-14 20:29:17'),
	(78, NULL, 'Corr. Board CB-350# 42" x 58"', 'CB-350# 42" x 58"', '42', NULL, '58', NULL, 'CB', '350#', 46.69, 193, 2, 'Raw Material', '2019-08-14 20:09:43', '2019-08-27 01:20:25'),
	(79, NULL, 'Corr. Board CB-350# 44" x 51"', 'CB-350# 44" x 51"', '44', NULL, '51', NULL, 'CB', '350#', 43.01, 95, 2, 'Raw Material', '2019-08-14 20:10:47', '2019-08-27 01:18:12'),
	(80, NULL, 'Corr. Board CB-350# 42" x 84.5"', 'CB-350# 42" x 84.5"', '42', NULL, '84', '5', 'CB', '350#', 68.02, 0, 2, 'Raw Material', '2019-08-14 20:11:56', '2019-08-14 20:36:36'),
	(81, NULL, 'Corr. Board CB-350# 46" x 79"', 'CB-350# 46" x 79"', '46', NULL, '79', NULL, 'CB', '350#', 69.65, 655, 2, 'Raw Material', '2019-08-14 20:13:20', '2019-08-14 23:04:25'),
	(82, NULL, 'Corr. Board C-175# 46" x 57"', 'C-175# 46" x 57"', '46', NULL, '57', NULL, 'C', '175#', 31.86, 993, 2, 'Raw Material', '2019-08-14 20:14:09', '2019-08-14 22:54:14'),
	(83, NULL, NULL, '10" x 77.25" C-175#', '10"', '', '77', '25"C-175#', 'C', '175#', 0.00, 300, 1, 'Excess Board', '2019-08-14 23:26:09', '2019-08-27 01:29:07'),
	(84, NULL, NULL, '46 x 10', '46', '', '10', '', 'C', '175#', 0.00, 0, 1, 'Excess Board', '2019-10-18 00:26:02', '2019-10-18 00:33:53');
/*!40000 ALTER TABLE `rawmaterials` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vatexempt` decimal(10,2) NOT NULL,
  `vatablesales` decimal(10,2) NOT NULL,
  `zerorated` decimal(10,2) NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `telnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faxnumber` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overrunpercentage` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.settings: ~0 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `vatexempt`, `vatablesales`, `zerorated`, `address`, `telnumber`, `faxnumber`, `overrunpercentage`) VALUES
	(1, 0.30, 0.20, 0.00, 'Lot 8 Blk 25 , Verona Street, Villa de Toledo Subd 4026 Sta. Rosa, Laguna', '(049) 534-9581', '(049) 534-4439', 0.10);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.settingseditaccess
CREATE TABLE IF NOT EXISTS `settingseditaccess` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ugroupid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.settingseditaccess: ~0 rows (approximately)
DELETE FROM `settingseditaccess`;
/*!40000 ALTER TABLE `settingseditaccess` DISABLE KEYS */;
INSERT INTO `settingseditaccess` (`id`, `ugroupid`, `created_at`, `updated_at`) VALUES
	(1, 1, '2019-07-17 20:05:28', NULL);
/*!40000 ALTER TABLE `settingseditaccess` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.suppliers
CREATE TABLE IF NOT EXISTS `suppliers` (
  `sid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactperson` text COLLATE utf8mb4_unicode_ci,
  `contactnumber` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` text COLLATE utf8mb4_unicode_ci,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `taxclassification` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sid`),
  UNIQUE KEY `suppliers_supplier_unique` (`supplier`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.suppliers: ~21 rows (approximately)
DELETE FROM `suppliers`;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` (`sid`, `supplier`, `contactperson`, `contactnumber`, `email`, `address`, `tin`, `terms`, `taxclassification`) VALUES
	(1, 'Twinpack Container Corporation', NULL, NULL, NULL, 'Unit1209 Le Gran Condo., 45 Eisenhower St., San Juan City, 1504', '005-515-235-000', '90 days', 'Vatable Sales'),
	(2, 'Packagemakers Inc.', NULL, NULL, NULL, '495 Boni Avenue, Mandaluyong City', '000-058-227-000', '90 Days', 'Vatable Sales'),
	(3, 'Packlink industrial Corp.', NULL, NULL, NULL, '18 Dizon Compd. G. Marcelo St., Brgy. Maysan, Valenzuela City', '008-298-466-000', '90 Days', 'Vatable Sales'),
	(4, 'Triple Star Packaging Corp.', NULL, NULL, NULL, 'Canlubang Ind\'l. Estate Pittland, Cabuyao Laguna', '006-600-682-000', '90 Days', 'Vatable Sales'),
	(5, 'Ubros Enterprises', NULL, NULL, NULL, 'Blk 68 Lot 19 Phase 3 Main Road,Brgy. Loma Binan Laguna', '714-811-445-000', '30 Days', 'Vat Exempt'),
	(6, 'Inoac Philippines Corp.', NULL, NULL, NULL, 'RGC Compound, Brgy. Pittland Cabuayo, Laguna', '005-240-769-000', '30 Days', 'Vatable Sales'),
	(7, 'Richtech Industrial Supply Company', NULL, NULL, NULL, 'Blk.5 Lot.9 Chicago St. Landmark Subd., Parian Calamba City', '228-953-827-000', '30 Days', 'Vatable Sales'),
	(8, 'E Copy Corporation', NULL, NULL, NULL, '65 Gil Puyat Avenue Palanan Makati City, Philippines', '219-274-001-000', '30 Days', 'Vatable Sales'),
	(9, 'Nappco', NULL, NULL, NULL, '34 Narciso St., Bo. Canumay, Valenzuela City', '000-297-204-000', '60 Days', 'Vatable Sales'),
	(10, 'Precisionfoam Company Inc.', NULL, NULL, NULL, 'Coats manila Bay Compound Lopez Jaeza St., Marikina City', '008-113-324-000', '30 Days', 'Vatable Sales'),
	(11, 'ECD Industrial Supply Corp.', NULL, NULL, NULL, 'Purok 1, National Highway, Dita Sta. Rosa City, Laguna', '009-339-717-000', '30 Days', 'Vatable Sales'),
	(12, 'C.Dioko Construction Supply Corp.', NULL, NULL, NULL, 'National Hi-way, Brgy.Dita , Sta Rosa Laguna', '209-617-512-000', '30 Days', 'Vatable Sales'),
	(13, 'JSJ Packaging Services', NULL, NULL, NULL, 'Main Road, Constantino Subd., Poblacion 2, Marilao, Bulacan', '128-136-211-000', '30 Days', 'Vatable Sales'),
	(14, 'Consolisated Adhesives, Inc.', NULL, NULL, NULL, '7 Caballero St., Pozorrubio, Pangasinan / 139-D Aurora Blvd.,San Juan, Metro Manila', '000-251-906-000', '30 Days', 'Vatable Sales'),
	(15, 'Five Star Europepaper Systems, Inc.', NULL, NULL, NULL, '#068 Purok 3, Brgy. Langkiwa, Binan Laguna', '006-721-089-000', '30 Days', 'Vatable Sales'),
	(16, 'MCR Industries, Inc.', NULL, NULL, NULL, '495 boni Avenue, Mandaluyong City PC 3119', '000-058-523-000', '30 days', 'Vatable Sales'),
	(17, 'Super Die Cut Enterprises', NULL, NULL, NULL, '8 Anak Bayan Street, Paltok 1, Quezon City', '103-965-957-000', '30 Days', 'Vatable Sales'),
	(18, 'Marion Flexographics Services', NULL, NULL, NULL, '1009 JP Rizal Street Poblacion, Makati City', '129-435-751-000', '30 Days', 'Vatable Sales'),
	(19, 'Brusmick Place Balibago Gasoline Service Station Inc.', NULL, NULL, NULL, 'Lot 1 Balibago Rd.,Cor.RSBS Blvd., Balibago, Sta. Rosa City Laguna', '006-940-152-000', '15 Days', 'Vatable Sales'),
	(20, '1198 Enterprises', NULL, NULL, NULL, 'D-5015 ME, National Highway, Balibago, Sta. Rosa City, Laguna', '195-800-422-000', '30 Days', 'Vat Exempt'),
	(23, 'Angeal Corp', 'Angelo Gabriel B Del Mundo', '9210063', 'delmundoangelo15@gmail.com', '#38 Vasra Village Diliman QC', NULL, NULL, 'Vat Exempt');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.useraccess
CREATE TABLE IF NOT EXISTS `useraccess` (
  `uaccessid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ugroupid` int(11) NOT NULL,
  `accessid` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uaccessid`)
) ENGINE=InnoDB AUTO_INCREMENT=677 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.useraccess: ~129 rows (approximately)
DELETE FROM `useraccess`;
/*!40000 ALTER TABLE `useraccess` DISABLE KEYS */;
INSERT INTO `useraccess` (`uaccessid`, `ugroupid`, `accessid`, `created_at`, `updated_at`) VALUES
	(461, 2, 10, '2019-06-02 19:52:07', NULL),
	(462, 2, 11, '2019-06-02 19:52:07', NULL),
	(463, 2, 12, '2019-06-02 19:52:07', NULL),
	(464, 2, 22, '2019-06-02 19:52:07', NULL),
	(465, 2, 23, '2019-06-02 19:52:07', NULL),
	(466, 2, 24, '2019-06-02 19:52:07', NULL),
	(467, 2, 32, '2019-06-02 19:52:07', NULL),
	(468, 2, 33, '2019-06-02 19:52:07', NULL),
	(469, 2, 36, '2019-06-02 19:52:07', NULL),
	(470, 2, 37, '2019-06-02 19:52:07', NULL),
	(471, 2, 41, '2019-06-02 19:52:07', NULL),
	(472, 2, 63, '2019-06-02 19:52:07', NULL),
	(489, 3, 10, '2019-06-02 20:50:10', NULL),
	(490, 3, 11, '2019-06-02 20:50:10', NULL),
	(491, 3, 12, '2019-06-02 20:50:10', NULL),
	(492, 3, 34, '2019-06-02 20:50:10', NULL),
	(493, 3, 35, '2019-06-02 20:50:10', NULL),
	(494, 3, 36, '2019-06-02 20:50:10', NULL),
	(495, 3, 37, '2019-06-02 20:50:10', NULL),
	(496, 3, 38, '2019-06-02 20:50:10', NULL),
	(497, 3, 39, '2019-06-02 20:50:10', NULL),
	(498, 3, 40, '2019-06-02 20:50:10', NULL),
	(499, 3, 41, '2019-06-02 20:50:10', NULL),
	(566, 5, 41, '2019-06-27 01:46:55', NULL),
	(567, 5, 42, '2019-06-27 01:46:55', NULL),
	(568, 5, 43, '2019-06-27 01:46:55', NULL),
	(569, 4, 13, '2019-07-22 02:11:05', NULL),
	(570, 4, 14, '2019-07-22 02:11:05', NULL),
	(571, 4, 15, '2019-07-22 02:11:05', NULL),
	(572, 4, 25, '2019-07-22 02:11:05', NULL),
	(573, 4, 26, '2019-07-22 02:11:05', NULL),
	(574, 4, 27, '2019-07-22 02:11:05', NULL),
	(575, 4, 40, '2019-07-22 02:11:05', NULL),
	(576, 4, 44, '2019-07-22 02:11:05', NULL),
	(577, 4, 46, '2019-07-22 02:11:05', NULL),
	(578, 4, 47, '2019-07-22 02:11:05', NULL),
	(579, 4, 48, '2019-07-22 02:11:05', NULL),
	(580, 4, 49, '2019-07-22 02:11:05', NULL),
	(581, 4, 50, '2019-07-22 02:11:05', NULL),
	(582, 4, 62, '2019-07-22 02:11:05', NULL),
	(583, 6, 45, '2019-08-26 20:19:36', NULL),
	(584, 6, 55, '2019-08-26 20:19:36', NULL),
	(585, 6, 56, '2019-08-26 20:19:36', NULL),
	(586, 6, 57, '2019-08-26 20:19:36', NULL),
	(587, 6, 58, '2019-08-26 20:19:36', NULL),
	(588, 6, 59, '2019-08-26 20:19:36', NULL),
	(589, 6, 60, '2019-08-26 20:19:36', NULL),
	(590, 6, 64, '2019-08-26 20:19:36', NULL),
	(593, 8, 44, '2019-08-26 20:25:29', NULL),
	(594, 8, 45, '2019-08-26 20:25:29', NULL),
	(595, 8, 46, '2019-08-26 20:25:29', NULL),
	(599, 7, 55, '2019-08-26 20:50:04', NULL),
	(600, 7, 56, '2019-08-26 20:50:04', NULL),
	(601, 7, 57, '2019-08-26 20:50:04', NULL),
	(602, 9, 52, '2019-08-27 01:06:29', NULL),
	(603, 9, 53, '2019-08-27 01:06:29', NULL),
	(604, 9, 54, '2019-08-27 01:06:29', NULL),
	(605, 9, 69, '2019-08-27 01:06:29', NULL),
	(606, 9, 70, '2019-08-27 01:06:29', NULL),
	(607, 9, 71, '2019-08-27 01:06:29', NULL),
	(608, 1, 1, '2019-10-28 17:28:25', NULL),
	(609, 1, 2, '2019-10-28 17:28:25', NULL),
	(610, 1, 3, '2019-10-28 17:28:25', NULL),
	(611, 1, 4, '2019-10-28 17:28:25', NULL),
	(612, 1, 5, '2019-10-28 17:28:25', NULL),
	(613, 1, 6, '2019-10-28 17:28:25', NULL),
	(614, 1, 7, '2019-10-28 17:28:25', NULL),
	(615, 1, 8, '2019-10-28 17:28:25', NULL),
	(616, 1, 9, '2019-10-28 17:28:25', NULL),
	(617, 1, 10, '2019-10-28 17:28:25', NULL),
	(618, 1, 11, '2019-10-28 17:28:25', NULL),
	(619, 1, 12, '2019-10-28 17:28:25', NULL),
	(620, 1, 13, '2019-10-28 17:28:25', NULL),
	(621, 1, 14, '2019-10-28 17:28:25', NULL),
	(622, 1, 15, '2019-10-28 17:28:25', NULL),
	(623, 1, 16, '2019-10-28 17:28:25', NULL),
	(624, 1, 17, '2019-10-28 17:28:25', NULL),
	(625, 1, 18, '2019-10-28 17:28:25', NULL),
	(626, 1, 19, '2019-10-28 17:28:25', NULL),
	(627, 1, 20, '2019-10-28 17:28:25', NULL),
	(628, 1, 21, '2019-10-28 17:28:25', NULL),
	(629, 1, 22, '2019-10-28 17:28:25', NULL),
	(630, 1, 23, '2019-10-28 17:28:25', NULL),
	(631, 1, 24, '2019-10-28 17:28:25', NULL),
	(632, 1, 25, '2019-10-28 17:28:25', NULL),
	(633, 1, 26, '2019-10-28 17:28:25', NULL),
	(634, 1, 27, '2019-10-28 17:28:25', NULL),
	(635, 1, 28, '2019-10-28 17:28:25', NULL),
	(636, 1, 29, '2019-10-28 17:28:25', NULL),
	(637, 1, 30, '2019-10-28 17:28:25', NULL),
	(638, 1, 31, '2019-10-28 17:28:25', NULL),
	(639, 1, 32, '2019-10-28 17:28:25', NULL),
	(640, 1, 33, '2019-10-28 17:28:25', NULL),
	(641, 1, 34, '2019-10-28 17:28:25', NULL),
	(642, 1, 35, '2019-10-28 17:28:25', NULL),
	(643, 1, 36, '2019-10-28 17:28:25', NULL),
	(644, 1, 37, '2019-10-28 17:28:25', NULL),
	(645, 1, 38, '2019-10-28 17:28:25', NULL),
	(646, 1, 39, '2019-10-28 17:28:25', NULL),
	(647, 1, 40, '2019-10-28 17:28:25', NULL),
	(648, 1, 41, '2019-10-28 17:28:25', NULL),
	(649, 1, 42, '2019-10-28 17:28:25', NULL),
	(650, 1, 43, '2019-10-28 17:28:25', NULL),
	(651, 1, 44, '2019-10-28 17:28:25', NULL),
	(652, 1, 45, '2019-10-28 17:28:25', NULL),
	(653, 1, 46, '2019-10-28 17:28:25', NULL),
	(654, 1, 47, '2019-10-28 17:28:25', NULL),
	(655, 1, 48, '2019-10-28 17:28:25', NULL),
	(656, 1, 49, '2019-10-28 17:28:25', NULL),
	(657, 1, 50, '2019-10-28 17:28:25', NULL),
	(658, 1, 52, '2019-10-28 17:28:25', NULL),
	(659, 1, 53, '2019-10-28 17:28:25', NULL),
	(660, 1, 54, '2019-10-28 17:28:25', NULL),
	(661, 1, 55, '2019-10-28 17:28:25', NULL),
	(662, 1, 56, '2019-10-28 17:28:25', NULL),
	(663, 1, 57, '2019-10-28 17:28:25', NULL),
	(664, 1, 58, '2019-10-28 17:28:25', NULL),
	(665, 1, 59, '2019-10-28 17:28:25', NULL),
	(666, 1, 60, '2019-10-28 17:28:25', NULL),
	(667, 1, 61, '2019-10-28 17:28:25', NULL),
	(668, 1, 62, '2019-10-28 17:28:25', NULL),
	(669, 1, 63, '2019-10-28 17:28:25', NULL),
	(670, 1, 64, '2019-10-28 17:28:25', NULL),
	(671, 1, 65, '2019-10-28 17:28:25', NULL),
	(672, 1, 66, '2019-10-28 17:28:25', NULL),
	(673, 1, 67, '2019-10-28 17:28:25', NULL),
	(674, 1, 69, '2019-10-28 17:28:25', NULL),
	(675, 1, 70, '2019-10-28 17:28:25', NULL),
	(676, 1, 71, '2019-10-28 17:28:25', NULL);
/*!40000 ALTER TABLE `useraccess` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.usergroups
CREATE TABLE IF NOT EXISTS `usergroups` (
  `ugroupid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usergroup` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ugroupid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.usergroups: ~9 rows (approximately)
DELETE FROM `usergroups`;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
INSERT INTO `usergroups` (`ugroupid`, `usergroup`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', '2019-03-09 04:31:39', NULL),
	(2, 'Purchasing', '2019-03-09 08:30:46', NULL),
	(3, 'Planning', '2019-03-09 17:58:50', NULL),
	(4, 'Sales', '2019-06-02 19:53:51', NULL),
	(5, 'Raw Materials', '2019-06-27 01:45:02', NULL),
	(6, 'Production', '2019-08-26 20:17:18', NULL),
	(7, 'Production Tag', '2019-08-26 20:19:47', NULL),
	(8, 'FG Warehouse', '2019-08-26 20:20:31', NULL),
	(9, 'Quality Assurance', '2019-08-26 20:43:49', NULL);
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` int(255) NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'noimage.jpg',
  `esign` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'noesignimage.png',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.users: ~14 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `firstname`, `lastname`, `mi`, `username`, `password`, `usertype`, `image`, `esign`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Angelo Gabriel', 'Del Mundo', 'B', 'Administrator', '$2y$10$Y/z3YIqDAapPFGsQy0erJOuKm.eFEB8vOcvE.qhuGc4T0TnS0S.N6', 1, 'noimage.jpg', 'noesignimage.png', 'hCooYsZX1zJVJRc77FEujw0DAGbfCRDND1d3EYm9GwkSfjwnbvm6yeOPcDa4', '2018-07-08 05:41:46', '2019-10-29 00:26:31'),
	(2, 'Jay R', 'Vergara', 'P', 'jayraccounting', '$2y$10$GFY4ZIQ/V1HrKc5xhm5lX.drPRboh9qSrYcaZc3eje0AbF.2WoEZa', 1, 'noimage.jpg', 'noesignimage.png', 'rdCnpP6oFe3ONHzTve3qVHkWe2DRescLvh1eVYrJ0ybtw2qxybGg51A2xwZM', '2019-04-21 22:31:01', NULL),
	(3, 'Jose', 'Reyes', NULL, 'jose', '$2y$10$Gw.IRU98R2qxTc9PGuT2Ae/CpAaopXTGl9kByKhPb.GtqiFJVHWna', 6, 'noimage.jpg', 'noesignimage.png', '4CZ5W4RascuKECcdhQNMBPPUWTi0OmMp4uTs4xfDXGX3HGT8dHTVFGkXfXLy', '2019-05-31 18:30:01', '2019-08-26 23:05:10'),
	(4, 'Michelle', 'Abadines', NULL, 'kc', '$2y$10$gELXJ/aNaONwP3ba/7Y8pO3XFFLzgFhd6u93O.NAamw.GRzJSHD7i', 1, 'noimage.jpg', 'noesignimage.png', 'E05sQFE2nRzABfdiuO7p8XDvEpf7c3aMaZ4IvDvVzhWT0nEIX92gDUTJwnhY', '2019-06-02 16:45:50', '2019-06-03 00:44:30'),
	(5, 'Rommel', 'Quia', NULL, 'rommel', '$2y$10$yqjq4sTzhPiJIRyqoBpC3ue2s.uzhLks7FXStAZYm81tIaGwXL5..', 1, 'noimage.jpg', 'noesignimage.png', 'V2AnHltAay36XOWWyxgDmqbXOV4vI4XnS9nYOwgBcJ9rKE3Dpy2yZC4uDKpn', '2019-06-02 17:04:10', '2019-08-27 00:37:05'),
	(6, 'Marites', 'Estoista', NULL, 'tesspurchasing', '$2y$10$H4jiE6B2nydGfLGHhNODt.GrXKYNWKucVuD1To5SVT28FZAP.iwU.', 2, 'noimage.jpg', 'noesignimage.png', 'C8YxMDu3KsY2I5NqotbqMpQZAbm1nT17fZeKWRMLURqchFBYPc08pmmwqBJL', '2019-06-02 19:53:37', NULL),
	(7, 'Gerlie', 'Deoda', 'S', 'gerliesales', '$2y$10$FI8W/vgU.InC45YfGn1kgO7an2YDxQbb9ZmDG9laivddSvzfsaIIC', 4, 'noimage.jpg', 'noesignimage.png', 'OZrKxNEVfTE0IvaYV4VlZc5PqyZjRQj1ZW8X7yj6GzS1wrdhf3lGlaQIScGw', '2019-06-02 19:57:25', NULL),
	(8, 'Ronald', 'Avila', NULL, 'ronaldrm', '$2y$10$Nu94TzLFwHa3xdzV7q25Ou8Csd7.ebzZEEF7bv10lpuGhOO6XGm/2', 5, 'noimage.jpg', 'noesignimage.png', 'xsrwzadt9Mr4L2MLDZFkR5WJuex3wQ0roYxv30jivfd1jOq5o2elogC8QZXp', '2019-06-27 01:48:02', '2019-07-24 22:25:09'),
	(9, 'Ma. Asuncion', 'Pontejos', 'B.', 'ma.asuncionpontejos', '$2y$10$4XohbacjQuSAE1isBz8IeOqnVQhN.PyV1KywRKspoqt5YqMlfF/NG', 1, 'noimage.jpg', 'noesignimage.png', '1Ej39frVscVWaDPjcJykLbf9HcOKG3Y5ZAvZtkrVCHDLwxA5N0sQH2OZ9iwe', '2019-07-22 23:26:43', NULL),
	(10, 'Orlando', 'Dela Cruz', NULL, 'orlyproduction', '$2y$10$pau8fNZ1.5Zhug38kNbmEe2mljBdolIz880U5L6LB5dFzJVd4adBG', 6, 'noimage.jpg', 'noesignimage.png', 'AYHd3i6rt2jU2i6fsVAGdwh9bk9RvN73ZQ5UOTtKGlwP9luEDGRGeXLsmktq', '2019-08-26 20:26:27', NULL),
	(11, 'Joan', 'Ruiz', NULL, 'joantag', '$2y$10$cUjgf40PzNZv0m8kemIXgegoiHkOPfg0NNufrIdwqwYlUTYNFCpkG', 7, 'noimage.jpg', 'noesignimage.png', 'PwUuqp4XwFDCJVgPSbp4lcg5od1uVrKdkCadg8psvXgnTMaHVGtnUuhvX9Q6', '2019-08-26 20:26:54', NULL),
	(12, 'Mary Grace', 'Pagkalinawan', NULL, 'gracefg', '$2y$10$sYLkeLW6E3oCcPwgQIJS5uEMdfjbWLMY4I4a8FRRWdAjObKINo9Py', 8, 'noimage.jpg', 'noesignimage.png', '7K6w59Vp0E3FksmCMYkdnKCtFrw4mVG8RkVMRVAWG9coRmA0WCPNdTDoKDKG', '2019-08-26 20:28:04', NULL),
	(13, 'Myrna', 'Borbe', NULL, 'myrnaqa', '$2y$10$g14.bpsYytc1TAhYmC2kT.QGuf9gWN4Ka4PlQ7UdYQcpWEnPbWlGm', 9, 'noimage.jpg', 'noesignimage.png', 'Lw1o32PwxdhhL441MWuaFhgh10NQXHKsaETFGNqlldj0wZxYFZObWgNmwnPz', '2019-08-26 20:45:50', NULL),
	(14, 'Jeffrey', 'Almanzor', NULL, 'jeffreyqa', '$2y$10$/UP4rdg4sfOPubm5SYtVFeEUYnMqJqd9wTRutldl.NY4VqawR7Y.2', 9, 'noimage.jpg', 'noesignimage.png', '4SNJI7k1j3rVhAe4QPQJurESbOCJtMIVIFbYRIha63ro4FJhuDEDs5RBU8sI', '2019-08-26 20:46:26', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table db_accounting_management.usersrestrictions
CREATE TABLE IF NOT EXISTS `usersrestrictions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `purchasing` int(11) NOT NULL,
  `planning` int(11) NOT NULL,
  `rawmaterial` int(11) NOT NULL,
  `finishgoods` int(11) NOT NULL,
  `sales` int(11) NOT NULL,
  `qualityassurance` int(11) NOT NULL,
  `production` int(11) NOT NULL,
  `accounting` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_accounting_management.usersrestrictions: ~11 rows (approximately)
DELETE FROM `usersrestrictions`;
/*!40000 ALTER TABLE `usersrestrictions` DISABLE KEYS */;
INSERT INTO `usersrestrictions` (`id`, `uid`, `purchasing`, `planning`, `rawmaterial`, `finishgoods`, `sales`, `qualityassurance`, `production`, `accounting`, `created_at`, `updated_at`) VALUES
	(1, 17, 1, 1, 1, 1, 1, 1, 1, 1, '2019-02-08 19:07:37', '2019-02-09 16:38:14'),
	(2, 9, 1, 0, 0, 1, 0, 0, 1, 0, '2019-02-09 16:45:09', '2019-02-09 16:45:15'),
	(3, 15, 0, 0, 0, 0, 0, 0, 0, 0, '2019-02-25 03:13:06', '2019-02-25 03:14:22'),
	(4, 18, 0, 0, 0, 0, 0, 0, 0, 0, '2019-02-25 03:15:22', NULL),
	(5, 19, 0, 0, 0, 0, 0, 0, 0, 0, '2019-03-09 19:14:52', '2019-03-15 20:02:22'),
	(6, 2, 0, 0, 0, 0, 0, 0, 0, 0, '2019-04-21 22:31:03', NULL),
	(7, 4, 0, 0, 0, 0, 0, 0, 0, 0, '2019-06-02 16:49:28', '2019-06-02 16:52:54'),
	(8, 7, 0, 0, 0, 0, 0, 0, 0, 0, '2019-06-02 20:17:18', NULL),
	(9, 6, 0, 0, 0, 0, 0, 0, 0, 0, '2019-06-26 20:41:19', NULL),
	(10, 3, 0, 0, 0, 0, 0, 0, 0, 0, '2019-08-26 23:05:03', '2019-08-26 23:05:10'),
	(11, 5, 0, 0, 0, 0, 0, 0, 0, 0, '2019-08-26 23:05:19', '2019-08-27 00:37:05');
/*!40000 ALTER TABLE `usersrestrictions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
