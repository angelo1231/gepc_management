<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Materialrequestitemsexcess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialrequestitemsexcess', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mritemid');
            $table->integer('excessrmid');
            $table->integer('qtyexcessboard');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialrequestitemsexcess');
    }
}
