<!-- Middle Modal -->
<style>
        .modal {
          text-align: center;
          padding: 0!important;
        }
        
        .modal:before {
          content: '';
          display: inline-block;
          height: 100%;
          vertical-align: middle;
          margin-right: -4px;
        }
        
        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }
        </style>   
        <!-- Modal -->
        <div class="modal fade" id="modalupdatecustomer" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <strong>CUSTOMER INFORMATION</strong>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                    
                            <div class="form-group">
                                    <label for="txtucustomer">Supplier</label>
                                    <input id="txtucustomer" name="txtucustomer" type="text" class="form-control" placeholder="Customer">
                            </div>
                            <div class="form-group">
                                <label for="txtucontactperson">Contact Person</label>
                                <input id="txtucontactperson" name="txtucontactperson" type="text" class="form-control" placeholder="Contact Person">
                            </div>
                            <div class="form-group">
                                    <label for="txtucontactnumber">Contact Number</label>
                                    <input id="txtucontactnumber" name="txtucontactnumber" type="text" class="form-control" placeholder="Contact Number">
                            </div>
                            <div class="form-group">
                                    <label for="txtuemail">Email</label>
                                    <input id="txtuemail" name="txtuemail" type="text" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                    <label for="txtuaddress">Address</label>
                                    <input id="txtuaddress" name="txtuaddress" type="text" class="form-control" placeholder="Address">
                            </div>
                            <div class="form-group">
                                    <label for="txtutin">TIN (Optional)</label>
                                    <input id="txtutin" name="txtutin" type="text" class="form-control" placeholder="TIN">
                            </div>
                             <div class="form-group">
                                    <label for="txtuterms">Terms (Optional)</label>
                                    <input id="txtuterms" name="txtuterms" type="text" class="form-control" placeholder="Terms">
                            </div>
                            <div class="form-group">
                                <label for="cmbutax">Tax Classification</label>
                                <select name="cmbutax" id="cmbutax" class="form-control">
                                  <option value="Vat Exempt">Vat Exempt</option>
                                  <option value="Vatable Sales">Vatable Sales</option>
                                  <option value="Zero Rated">Zero Rated</option>
                                </select>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-9">
        
                        </div>
                        <div class="col-md-3">
                            <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-block btn-flat">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>