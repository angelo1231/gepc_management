<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="productionprocessinfo" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong id="pinfo"></strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Operator</th>
                                <th>Quantity</th>
                                <th>Quantity Reject</th>
                                <th>Issued Date</th>
                              </tr>
                            </thead>
                            <tbody id="processinfocontent">
                            </tbody>
                          </table>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>