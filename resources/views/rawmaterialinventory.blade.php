@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Raw Material Inventory</b></h3>
        </div>
        <div class="box-body">
          <div class="col-md-6">

              <button name="btnsortall" class="btn btn-success btn-flat" value="All" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>All</strong></button>

              <button name="btnsortall" class="btn btn-primary btn-flat" value="Raw Material" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Raw Material</strong></button>

              <button name="btnsortall" class="btn btn-primary btn-flat" value="Excess Board" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Excess Board</strong></button>

              <button name="btnsortall" class="btn btn-info btn-flat" value="Other" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Others</strong></button>

              <button name="btnsortall" class="btn btn-danger btn-flat" value="Fix Asset" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Fix Asset</strong></button>



          </div>
          <div class="col-md-6">
            <div class="form-group">

              <button id="btnmaterialrequest" name="btnmaterialrequest" class="btn btn-primary btn-flat" style="float:right; margin: 7px 0px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Material Request</strong></button>

              <button id="btnaudit" name="btnaudit" class="btn btn-primary btn-flat" style="float:right; margin: 7px 2px; 0px 7px;"><i class="fa fa-info-circle" style="font-size:12px;"></i> <strong>Audit Information</strong></button>

              <button id="btnexcess" name="btnexcess" class="btn btn-success btn-flat" style="float:right; margin: 7px 2px; 0px 7px;"><i class="fa fa-clipboard" style="font-size:12px;"></i> <strong>Excess</strong></button>

              <button id="btndel" name="btndel" class="btn btn-success btn-flat" style="float:right; margin: 7px 2px; 0px 7px;"><i class="fa fa-truck" style="font-size:12px;"></i> <strong>Delivery Informations</strong></button>

            </div>
          </div>
          <div class="col-md-12"> 
            <br>
            <table id="tblrawmaterial" class="table">
                <thead>
                    <tr>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Size</th>
                      <th>Flute</th>
                      <th>Board Pound</th>
                      <th>Currency</th>
                      <th>Price</th>
                      <th>Stocks</th>
                      <th>Category</th>
                      <th></th>
                    </tr>
                </thead>
            </table>
          </div>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
              <button id="btnexcel" class="btn btn-info btn-flat" style="float: right;"><i class="fa fa-file-excel-o"></i> Excel</button>
              <button id="btnprint" name="btnprint" class="btn btn-info btn-flat" style="float: right; margin-right: 10px;"><i class="fa fa-print"></i> Print</button>
          </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modals --}}
  @include('modal.rawmaterials.inventory.individualdelivery')
  @include('modal.rawmaterials.inventory.individualaudit')
  @include('modal.rawmaterials.inventory.rmupdatestocks')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>
    
    //Variables
    var delrmid;
    var tblrawmaterials;
    var ssort = "All";
    var srmid;

    $(document).ready(function(){

        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
          toastr.success(msg, '', { positionClass: 'toast-top-center' });
        }

        //Set Datatable
        tblrawmaterials = $('#tblrawmaterial').DataTable({
            autoWidth: false,
            ordering: false,
        });

        //Set Datepicker
        $("#txtdatefrom").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        $("#txtdateto").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });   

        //Load
        LoadRawMaterialInformation("All");
        LoadSelect2();
        LoadMRCount();

        // tblrawmaterials.columns(5).visible(false);
        // tblrawmaterials.columns(6).visible(false);    

    });

    $('#btnmaterialrequest').on('click', function(){

      @if(in_array(42, $access))

        window.location = "{{ url('/rminventory/materialrequest') }}";

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $('#btnaudit').on('click', function(){

      window.location = "{{ url('/rminventory/rmaudit') }}";

    });

    $('#btndel').on('click', function(){

      @if(in_array(43, $access))

        window.location = "{{ url('/rminventory/delivery') }}";

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $(document).on('click', '#btndelivery', function(){

      @if(in_array(43, $access))

        delrmid = $(this).val();
        ClearDelivery();
        LoadDeliveryIndividual($('#cmbdponumber').attr('id'), delrmid);
        $('#modaldelivery').modal('toggle');

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $('#btnexcess').on('click', function(){

      window.location = "{{ url('/rminventory/excess') }}";

    });

    $('#cmbdponumber').on('change', function(){

      var poid = $(this).val();
      ClearDelivery();
      $.ajax({
        url: '{{ url("api/rawmaterialinv/loadpoitemprofile") }}',
        type: 'get',
        data: {rmid: delrmid, poid: poid},
        dataType: 'json',
        success: function(response){

          var size = "";

          if(response.category=="Raw Material"){

              if(response.widthsize!=null){
                  size = size + response.width + "." + response.widthsize;
              }
              else{
                  size = size + response.width;
              }

              if(response.lengthsize!=null){
                  size = size + " x " + response.length + "." + response.lengthsize;
              }
              else{
                  size = size + " x " + response.length;
              }

          }

          $('#txtdname').val(response.name);
          $('#txtddescription').val(response.description);
          $('#txtdsize').val(size);
          $('#txtdflute').val(response.flute);
          $('#txtdqty').val(response.qty);
          $('#txtdboardpound').val(response.boardpound);
          $('#txtddelqty').val(response.delqty);
          $('#btnsavedelivery').val(response.itemid)

        }
      });

    });

    $('#btnsavedelivery').on('click', function(){

      var poid = $('#cmbdponumber').val();
      var ponumber = $('#cmbdponumber option:selected').text();
      var deliverdqty = $('#txtddeliveryqty').val();
      var requiredqty = parseInt($('#txtdqty').val()) - parseInt($('#txtddelqty').val());
      var itemid = $(this).val();

      if(ponumber==null){
        toastr.error("Please select a purchase order number.", '', { positionClass: 'toast-top-center' });
      }
      else if(deliverdqty < 0){
        toastr.error("Please input a non negative number in the deliverd quantity.", '', { positionClass: 'toast-top-center' });
      }
      else if(deliverdqty==0){
        toastr.error("Please input a greater than zero in the deliverd quantity.", '', { positionClass: 'toast-top-center' });
      }
      else{

        if(deliverdqty > requiredqty){
          toastr.error("The delivery is over the limit please put the exact quantity. You just need " + requiredqty, '', { positionClass: 'toast-top-center' });
        }
        else{

          $.ajax({
            url: '{{ url("api/rawmaterialinv/savedeliveryinformationinv") }}',
            type: 'post',
            data: {
              poid: poid, 
              rmid: delrmid, 
              itemid: itemid, 
              ponumber: ponumber, 
              deliverdqty: deliverdqty
            },
            dataType: 'json',
            success: function(response){
              
              if(response.success){

                toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                $("#modaldelivery .close").click();
                LoadRawMaterialInformation(ssort);                

                //Socket Emit
                socket.emit('rminvinformation');

              }

            }
          });

        }

      }
      

    });

    $('#btnprint').on('click', function(){

      window.open("{{ url('/rminventory/printrm') }}/"+ssort, '_blank');

    });

    $('#btnexcel').on('click', function(){

      window.open("{{ url('api/rawmaterialinv/downloadexcel') }}/"+ssort, '_blank');

    });

    $(document).on('click', '#btnrmauditinfo', function(){

      var id = $(this).val();

      $.ajax({
        url: '{{ url("api/rawmaterialinv/rminformation") }}',
        type: 'get',
        data: {
          id: id
        },
        dataType: 'json',
        success: function(response){

          if(response.category=="Raw Material"){
            $('#lblinformation').text("Audit (Size: "+ response.size +" Flute: "+ response.flute +" boardpound: "+ response.boardpound +")");
          }
          else if(response.category=="Other"){
            $('#lblinformation').text("Audit (Name: "+ response.name +" Description: "+ response.description +")");
          }
          
          $('#btngenerate').val(id);
          $('#content').hide();
          LoadStartEndDate();

        }
      });

      

    });

    $('[name="btnsortall"]').on('click', function(){

      ssort = $(this).val();
      LoadRawMaterialInformation(ssort);

    });

    $('#btngenerate').on('click', function(){

      var id = $('#btngenerate').val();
      var datefrom = $('#txtdatefrom').val();
      var dateto = $('#txtdateto').val();

      $.ajax({
        url: '{{ url("api/rawmaterialinv/generateauditrm") }}',
        type: 'get',
        data: {
          id: id,
          datefrom: datefrom,
          dateto: dateto
        },
        beforeSend: function(){
          $('#content').hide();
        },
        success: function(response){

          $('#content').html(response);

        },
        complete: function(){
          $('#content').fadeIn("slow");
        }
      });

    });

    $(document).on('click', '#btnupdatestock', function(){

      srmid = $(this).val();

      ClearAuth();
      $('#auth').modal('toggle');

    });

    $(document).on('click', '#btnauthlogin', function(){

        var validation = AuthLogin();

        validation.done(function(data){

            if(data.success){

                authinfo = {
                    id: data.id,
                    name: data.name
                };

                $('#auth').modal('toggle');
                setTimeout(function(){ 

                  ClearUpdateRMStock();
                  LoadRMStockOnHand(srmid);
                  $('#rmupdatestocks').modal('toggle');

                }, 500);

            }

        });

    });

    $('#btnsaveupdatestocks').on('click', function(){

          $.confirm({
              title: 'Save',
              content: 'Save this stock on hand information of the item?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveRMStockOnHand(srmid);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    function LoadRMStockOnHand(rmid){

      $.ajax({
        url: '{{ url("api/rawmaterialinv/loadrmstockonhand") }}',
        type: 'get',
        data: {
          rmid: rmid
        },
        dataType: 'json',
        success: function(response){

          $('#lblinformationstock').text('');
          $('#lblinformationstock').text(response.description);
          $('#txtuqtystock').val(response.stockonhand);

        }
      });

    }

    function SaveRMStockOnHand(rmid){

      var qty = $('#txtuqtystock').val();
      var remarks = $('#txturemarksstock').val();

      //Validation
      if(qty==""){

          toastr.error("Please input a quantity.", '', { positionClass: 'toast-top-center' });


      }
      else if(remarks==""){

          toastr.error("Please input the remarks.", '', { positionClass: 'toast-top-center' });

      }
      else{

        $.ajax({
          url: '{{ url("api/rawmaterialinv/savermstockonhand") }}',
          type: 'post',
          data: {
            rmid: rmid,
            qty: qty,
            remarks: remarks
          },
          dataType: 'json',
          success: function(response){

            if(response.success){

              $('#rmupdatestocks .close').click();
              LoadRawMaterialInformation(ssort);
              toastr.success(response.message, '', { positionClass: 'toast-top-center' });


            }

          }
        });

      }

    }

    function LoadStartEndDate(){

        $.ajax({
            url: '{{ url("api/rawmaterialinv/loadstartenddate") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtdatefrom').val(response.datefrom);
                $('#txtdateto').val(response.dateto);

            }
        });

    }

   function LoadSupplier(id){

      $.ajax({
        url: '{{ url("api/rawmaterialinv/loadsupplier") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          $('#'+id).find('option').remove();
          $('#'+id).append('<option value="" disabled selected>Select a Supplier</option>');
          for (var i = 0; i < response.data.length; i++) {
              $('#'+id).append('<option value="'+  response.data[i]["sid"] +'">'+  response.data[i]["supplier"] +'</option>');
          }

        }
      });

    }

    function LoadDeliveryIndividual(id, rmid){

      $.ajax({
        url: '{{ url("api/rawmaterialinv/loadpodeliveryinv") }}',
        type: 'get',
        data: {rmid: rmid},
        dataType: 'json',
        success: function(response){

          $('#'+id).find('option').remove();
          $('#'+id).append('<option value="" disabled selected>Select a Purchase Order Number</option>');
          for (var i = 0; i < response.data.length; i++) {
              $('#'+id).append('<option value="'+  response.data[i]["poid"] +'">'+  response.data[i]["ponumber"] +'</option>');
          }

        }
      });

    }

    function LoadSelect2(){

      $('#cmbsupplier').select2({
        theme: 'bootstrap'
      });

      $('#cmbdponumber').select2({
        theme: 'bootstrap'
      });

    }

    function ClearDelivery(){

      $('#txtdname').val('');
      $('#txtddescription').val('');
      $('#txtdsize').val('');
      $('#txtdflute').val('');
      $('#txtdqty').val('');
      $('#txtdboardpound').val('');
      $('#txtddelqty').val('');
      $('#txtddeliveryqty').val('0');

    }

    function LoadRawMaterialInformation(sort){

      tblrawmaterials.destroy();
      tblrawmaterials = $('#tblrawmaterial').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/rawmaterialinv/rawmaterialsinformation") }}',
            data: {
              sort: sort
            },
        },
        columns : [
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'size', name: 'size'},
            {data: 'flute', name: 'flute'},
            {data: 'boardpound', name: 'boardpound'},
            {data: 'currency', name: 'currency'},
            {data: 'price', name: 'price'},
            {data: 'stocks', name: 'stocks'},
            {data: 'category', name: 'category'},
            {data: 'panel', name: 'panel', width: '12%'},
        ]
      });

    }

    // function ReloadRawMaterials(){

    //   var supplier = $('#cmbsupplier').val();
    //   if(supplier!=null){
    //     tblrawmaterials.ajax.reload();
    //   }

    // }

    function LoadMRCount(){

      $.ajax({
        url: '{{ url("api/rawmaterialinv/loadmrcount") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          if(response.mrcount!=0){

            $('#btnmaterialrequest').text('');
            $('#btnmaterialrequest').append('<i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Material Request <span id="prcount" class="dot">'+ response.mrcount +'</span></strong>');

          }
          else{

            $('#btnmaterialrequest').text('');
            $('#btnmaterialrequest').append('<i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Material Request</strong>');

          }

        }
      });

    }

    function ClearUpdateRMStock(){

      $('#txtuqtystock').val('');
      $('#txturemarksstock').val('');

    }

    //Socket functions
    socket.on('reloadrminvinformation', function(){

      LoadRawMaterialInformation("All");      

    });

    socket.on('reloadmrcount', function(){

      LoadMRCount();     

    });


  </script>  

@endsection