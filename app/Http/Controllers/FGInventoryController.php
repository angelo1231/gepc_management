<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use App\FinishGoods_Model;
use DataTables;
use Illuminate\Support\Collection;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use App\Exports\FinishGoods\FinishGoodExport;
use App\Http\Controllers\UserController;

class FGInventoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index($view=null, $action=null, $id=null){
     
        $access = UserController::UserGroupAccess();

        if($view!=null){

            if($action!=null && $id!=null){

                if($action=="printfg"){

                    $fgdata = FinishGoods_Model::LoadFinsihGoodsInformation($id);
                    return view('extensions.finishgoods.'.$view)->with('fgdata', $fgdata);

                }

            }
            else{

                return view('extensions.finishgoods.'.$view)
                ->with('access', $access);

            }

        }
        else{
            
            return view('finishgoodsinventory')
            ->with('access', $access);

        }         

    }

    function LoadCustomers(){

        $customers = FinishGoods_Model::LoadCustomers();

        return json_encode([
            "data"=>$customers
        ]);

    }

    function LoadFinsihGoodsInformation(Request $request){

        //Get User Access
        $access = UserController::UserGroupAccess();

        $finishgoods = FinishGoods_Model::LoadFinsihGoodsInformation($request->customer);

        $data = array();
        for($i=0;$i<count($finishgoods);$i++){
            
            $obj = new \stdClass;

            $obj->partnumber = $finishgoods[$i]->partnumber;
            $obj->partname = $finishgoods[$i]->partname;
            $obj->description = $finishgoods[$i]->description;
            $obj->size = '<p><strong>I.D</strong></p>
                          <p>W:'. $finishgoods[$i]->ID_width .' L:'. $finishgoods[$i]->ID_length .' H:'.  $finishgoods[$i]->ID_height .'</p>
                          <p><strong>O.D</strong></p>
                          <p>W:'. $finishgoods[$i]->OD_width .' L:'. $finishgoods[$i]->OD_length .' H:'.  $finishgoods[$i]->OD_height .'</p>';
            $obj->packingstd = $finishgoods[$i]->packingstd;
            $obj->currency = $finishgoods[$i]->currency;
            $obj->price = $finishgoods[$i]->price;
            $obj->stockonhand = $finishgoods[$i]->stockonhand;
            
            if(in_array(66, $access)){

                $obj->panel = '<button id="btninventory" name="btninventory" class="btn btn-flat btn-info" title="Production" value="'. $finishgoods[$i]->fgid .'"><i class="fa fa-dropbox"></i></button> <button id="btndelivery" name="btndelivery" class="btn btn-flat btn-info" title="Delivery"><i class="fa fa-truck"></i></button> <button id="btnaudittrail" name="btnaudittrail" class="btn btn-flat btn-info" title="Audit Trail" data-toggle="modal" data-target="#modalaudit" value="'.  $finishgoods[$i]->fgid .'"><i class="fa fa-info"></i></button> <button id="btnupdatestocks" name="btnupdatestocks" class="btn btn-flat btn-info" value="'. $finishgoods[$i]->fgid .'" title="Update Stocks"><i class="fa fa-list-ul"></i></button>';

            }
            else{

                $obj->panel = '<button id="btninventory" name="btninventory" class="btn btn-flat btn-info" title="Production" value="'. $finishgoods[$i]->fgid .'"><i class="fa fa-dropbox"></i></button> <button id="btndelivery" name="btndelivery" class="btn btn-flat btn-info" title="Delivery"><i class="fa fa-truck"></i></button> <button id="btnaudittrail" name="btnaudittrail" class="btn btn-flat btn-info" title="Audit Trail" data-toggle="modal" data-target="#modalaudit" value="'.  $finishgoods[$i]->fgid .'"><i class="fa fa-info"></i></button>';

            }

            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'size'])->make(true);

    }

    function LoadJobOrders(){

       $joborders = FinishGoods_Model::LoadJobOrders();

       return json_encode([
        "data"=>$joborders
       ]);

    }

    function LoadJobOrderInformation(Request $request){

        $joborderinformation = FinishGoods_Model::LoadJobOrderInformation($request->mstid);
        
        $rmdescription = "";
        $rminfowidth = explode(',', $joborderinformation[0]->width);
        $rminfolength = explode(',', $joborderinformation[0]->length);
        $rminfoflute = explode(',', $joborderinformation[0]->flute);
        $rminfoboardpound = explode(',', $joborderinformation[0]->boardpound);

        for($i=0;$i<count($rminfowidth);$i++){

            $rmdescription .= $rminfowidth[$i] . " x " . $rminfolength[$i] . " " . $rminfoflute[$i] . " " . $rminfoboardpound[$i] . ", ";

        }

        return json_encode([
            "jonumber"=>$joborderinformation[0]->jonumber,
            "partnumber"=>$joborderinformation[0]->partnumber,
            "partname"=>$joborderinformation[0]->partname,
            "description"=>$joborderinformation[0]->description,
            "id_size"=>$joborderinformation[0]->id_size,
            "od_size"=>$joborderinformation[0]->od_size,
            "rmdescription"=>$rmdescription,
            "targetoutput"=>$joborderinformation[0]->targetoutput,
            "qtyreceive"=>$joborderinformation[0]->qtyreceive,
            "qty"=>$joborderinformation[0]->qty
        ]);

    }

    function SaveProductionInformation(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
        for($i=0;$i<count($request->datamst);$i++){

            if($request->dataqty[$i]["value"]!=0){

                $joid = FinishGoods_Model::GetJOID($request->datajo[$i]["value"]);

                $fgid = FinishGoods_Model::GetFGID($joid);
                $stockonhand = FinishGoods_Model::GetFGStockOnHand($fgid);
                $receiveqty = FinishGoods_Model::GetReceiveQuantity($joid);

                $stockonhand += $request->dataqty[$i]["value"];
                $receiveqty += $request->dataqty[$i]["value"];

                FinishGoods_Model::UpdateFGStockOnHand($fgid, $stockonhand);
                FinishGoods_Model::UpdateJOQuantityReceive($joid, $receiveqty);
                FinishGoods_Model::UpdateMSTStatus($request->datamst[$i]["value"]);
                FinishGoods_Model::SaveFGAudit($fgid, $joid, $request->dataqty[$i]["value"], $stockonhand, $issuedby);

            }

        }

        Session::flash('message', "Production information has been save.");
        return json_encode(["success"=>true]);

    }

    function LoadFGInfo(Request $request){

        $fginfo = FinishGoods_Model::LoadFGInfo($request->fgid);

        return json_encode([
            "partnumber"=>$fginfo->partnumber,
            "partname"=>$fginfo->partname,
            "description"=>$fginfo->description
        ]);

    }

    function GenerateAudit(Request $request){

        $audit = FinishGoods_Model::GenerateAudit($request);

        return View::make('extensions.finishgoods.views.fgaudittable')->with('audit', $audit)->render();

    }

    function LoadFGAuditInformation(Request $request){

        $audit = FinishGoods_Model::LoadFGAuditInformation($request);

        return View::make('extensions.finishgoods.views.fgauditmodaltable')->with('audit', $audit)->render();

    }

    function LoadStartEndDate(){

        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->format('Y-m-d');

        return json_encode(["datefrom"=>$start, "dateto"=>$end]);

    }

    function LoadFGJO(Request $request){

        $jonumber = FinishGoods_Model::LoadFGJO($request->fgid);

        return json_encode([
            "data"=>$jonumber
        ]);

    }

    function SaveFGIndividualProduction(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        $stockonhand = FinishGoods_Model::GetFGStockOnHand($request->fgid);
        $receiveqty = FinishGoods_Model::GetReceiveQuantity($request->jonumber);

        $stockonhand += $request->qty;
        $receiveqty += $request->qty;

        FinishGoods_Model::UpdateFGStockOnHand($request->fgid, $stockonhand);
        FinishGoods_Model::UpdateJOQuantityReceive($request->jonumber, $receiveqty);
        FinishGoods_Model::SaveFGAudit($request->fgid, $request->jonumber, $request->qty, $stockonhand, $issuedby);

        return json_encode([
            "success"=>true,
            "message"=>"Production information has been save."
        ]);

    }

    function LoadMSTNumbers(){

        $mstnumbers = FinishGoods_Model::LoadMSTNumbers();

        return json_encode([
            "data"=>$mstnumbers
        ]);

    }

    function LoadDRNumbers()
    {
        
        $drnumber = FinishGoods_Model::LoadDRNumbers();

        return json_encode([
            "data"=>$drnumber
        ]);

    }

    function LoadDRItems(Request $request){

        $dritems = FinishGoods_Model::LoadDRItems($request);

        return json_encode([
            "data"=>$dritems
        ]);

    }

    function ValidateDelivery(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        for($i=0;$i<count($request->datapo);$i++){

            $fgid = FinishGoods_Model::GetFGIDPOCustomer($request->datacusitemid[$i]["value"]);
            $delqty = FinishGoods_Model::GetPOItemCustomerDelQty($request->datacusitemid[$i]["value"]);
            $stockonhand  = FinishGoods_Model::GetFGStockOnHand($fgid);
            
            $stockonhand = $stockonhand - $request->dataqty[$i]["value"];
            $delqty = $delqty + $request->dataqty[$i]["value"];

            FinishGoods_Model::UpdateFGStockOnHand($fgid, $stockonhand);
            FinishGoods_Model::UpdateCustomerPOItems($request->datacusitemid[$i]["value"], $delqty);
            FinishGoods_Model::SaveFGAuditDR($fgid, $request->did, $request->dataqty[$i]["value"], $stockonhand, $issuedby);
     
        }

        FinishGoods_Model::UpdateDeliveryInformation($request->did);

        Session::flash('message', "Delivery information has been validate.");
        return json_encode([
            "success"=>true
        ]);

    }

    function DownloadExcel($cid){

        $fgdata = FinishGoods_Model::LoadFinsihGoodsInformation($cid);

        $fgdata_array[] = array();

        foreach($fgdata as $data){

            $idsize = "";
            $odsize = "";
            $stockonhand = "";

            $idsize = "IDSize: W: " . $data->ID_width . " L: " . $data->ID_length . " H: " . $data->ID_height;
            $odsize = "ODSize: W: " . $data->OD_width . " L: " . $data->OD_length . " H: " . $data->OD_height;

            if($data->stockonhand=="0"){
                $stockonhand = "0";
            }
            else{
                $stockonhand = $data->stockonhand;
            }

            $fgdata_array[] = array(
                'Partnumber'=>$data->partnumber,
                'Partname'=>$data->partname,
                'Description'=>$data->description,
                'Size'=>$idsize . " " . $odsize,
                'Currency'=>$data->currency,
                'Price'=>$data->price,
                'Stocks'=>$stockonhand
            );

        }

        $customer = $fgdata[0]->customer;
        return Excel::download(new FinishGoodExport($fgdata_array), $customer . ".xlsx");

    }

    function LoadPreparationSlipInformation(){

        $preparationslip = FinishGoods_Model::LoadPreparationSlipInformation();

        $data = array();
        for($i=0;$i<count($preparationslip);$i++){
            
            $obj = new \stdClass;

            $obj->psnumber = $preparationslip[$i]->preparationnumber;
            $obj->totalqty = $preparationslip[$i]->totalqty;
            $obj->issuedby = $preparationslip[$i]->issuedby;
            $obj->createdat = $preparationslip[$i]->created_at;
            $obj->customer = $preparationslip[$i]->customer;
            $obj->panel = '<button id="btnpreparationinfo" name="btnpreparationinfo" class="btn btn-flat btn-info" title="Preparation Slip Information" value="'. $preparationslip[$i]->preparationid .'"><i class="fa fa-info"></i></button>';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadPreparationSlipItems(Request $request){

        $preparationslip = FinishGoods_Model::GetPreparationSlipNumber($request->preparationid);
        $preparationitems = FinishGoods_Model::LoadPreparationSlipItems($request->preparationid);

        return json_encode([
            "preparationnumber"=>$preparationslip,
            "preparationitems"=>$preparationitems
        ]);

    }

    function ApprovePreparationSlip(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        FinishGoods_Model::ApprovePreparationSlip($request->preparationid, $issuedby);

        return json_encode([
            "success"=>true,
            "message"=>"You have successfully approve the preparation slip."
        ]);

    }

    function DisapprovePreparationSlip(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        FinishGoods_Model::DisapprovePreparationSlip($request->preparationid, $issuedby, $request->remarks);

        return json_encode([
            "success"=>true,
            "message"=>"You have successfully disapprove the preparation slip."
        ]);

    }

    function LoadCountPS(){

        $pscount =  FinishGoods_Model::LoadCountPS();

        return json_encode([
            "pscount"=>$pscount
        ]);

    }

    function LoadFGStockOnHand(Request $request){

        $fginfo = FinishGoods_Model::LoadFGStockOnHand($request->fgid);

        return json_encode([
            "description"=>$fginfo->description,
            "stockonhand"=>$fginfo->stockonhand
        ]);

    }

    function SaveStockOnHandInformation(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        FinishGoods_Model::SaveStockOnHandInformation($request->fgid, $request->qty);
        FinishGoods_Model::SaveFGAuditStock($request->fgid, $request->qty, $request->remarks, $issuedby);

        return json_encode([
            "success"=>true,
            "message"=>"Finish good stock on hand information has been update."
        ]);

    }

}
