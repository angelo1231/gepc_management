<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pocustomerinformationitemdeliverydeleted extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pocustomerinformationitemdeliverydeleted', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delid');
            $table->longtext('reason');
            $table->integer('authuid');
            $table->text('authname');
            $table->integer('deluid');
            $table->text('delname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pocustomerinformationitemdeliverydeleted');
    }
}
