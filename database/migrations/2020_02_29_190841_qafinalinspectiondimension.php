<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Qafinalinspectiondimension extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qafinalinspectiondimension', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qafiid');
            $table->decimal('length', 11,2);
            $table->decimal('width', 11,2);
            $table->decimal('height', 11,2);
            $table->integer('remarks')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qafinalinspectiondimension');
    }
}
