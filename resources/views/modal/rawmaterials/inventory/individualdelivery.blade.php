<!-- Middle Modal -->
<style>
.modal {
   text-align: center;
   padding: 0!important;
}
    
.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}
    
.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.ui-autocomplete  {
    z-index: 1100 !important;
}

</style>   

<!-- Modal -->
<div class="modal fade" id="modaldelivery"  role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  <strong>DELIVERY</strong>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="col-md-6">

                                <div class="form-group">
                                    <label for="cmbdponumber">Purchase Order Number</label>
                                    <select class="form-control" id="cmbdponumber" style="width: 320px;">
                                    </select>
                                </div>
              
                                <div class="form-group">
                                    
                                    <label for="txtdname">Name</label>
                                    <input id="txtdname" name="txtdname" type="text" class="form-control" placeholder="Size" readonly>
                             
                                </div>

                                <div class="form-group">
                                        
                                    <label for="txtddescription">Description</label>
                                    <input id="txtddescription" name="txtddescription" type="text" class="form-control" placeholder="Description" readonly>
                             
                                </div>

                                <div class="form-group">
                                    
                                    <label for="txtdsize">Size</label>
                                    <input id="txtdsize" name="txtdsize" type="text" class="form-control" placeholder="Size" readonly>
                             
                                </div>
            
                                <div class="form-group">
                                    
                                    <label for="txtdflute">Flute</label>
                                    <input id="txtdflute" name="txtdflute" type="text" class="form-control" placeholder="Flute" readonly>
                             
                                </div>
            
                                <div class="form-group">
                                    
                                    <label for="txtdqty">Quantity</label>
                                    <input id="txtdqty" name="txtdqty" type="text" class="form-control" placeholder="Quantity" readonly>
                             
                                </div>
                      
                            </div>
        
                            <div class="col-md-6">
        
                                    <div class="form-group">
                                    
                                        </br>
                                        </br>
                                        </br>
                                 
                                    </div>
        
                                    <div class="form-group">
                                        
                                        <label for="txtdboardpound">Board Pound</label>
                                        <input id="txtdboardpound" name="txtdboardpound" type="text" class="form-control" placeholder="Board Pound" readonly>
                                 
                                    </div>
                
                                    <div class="form-group">
                                        
                                        <label for="txtddelqty">Deliverd Quantity</label>
                                        <input id="txtddelqty" name="txtddelqty" type="text" class="form-control" placeholder="Deliverd Quantity" readonly>
                                 
                                    </div>
                
                                    <div class="form-group">
                                        
                                        <label for="txtddeliveryqty">Delivery Quantity</label>
                                        <input id="txtddeliveryqty" name="txtddeliveryqty" type="number" value="0" class="form-control" placeholder="Delivery Quantity">
                                 
                                    </div>
                                  
                            </div>
                        </div>
          
                </div>
                
                <div class="modal-footer">
                    <div class="col-md-9">
    
                    </div>
                    <div class="col-md-3">
                        <button id="btnsavedelivery" name="btnsavedelivery" type="button" class="btn btn-primary btn-block btn-flat">Save Information</button>
                    </div>
                </div>

            </div>
        </div>
       
</div>
