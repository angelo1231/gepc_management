<?php

namespace App\Exports\Delivery;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DeliveryExport implements FromCollection, WithHeadings
{
    use Exportable;
    private $data;

    public function __construct($data)
    {
         $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Delivery Number',
            'Purchase Order Number',
            'Partnumber',
            'Partname',
            'Description',
            'Qty',
            'Price',
            'Total'
        ];
    }

}
