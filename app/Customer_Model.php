<?php
namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Customer_Model extends Model
{

    public static function CustomerInformation(){

        $result = DB::table('customers')
        ->select(
            'cid', 
            'customer',
            'contactperson',
            'contactnumber',
            'email', 
            'address', 
            'tin', 
            'terms', 
            'taxclassification'
        )
        ->get();

        return $result;

    }

    public static function ValidateCustomer($customer){
     
        $result = DB::table('customers')
        ->select(DB::raw('COUNT(*) AS customer_count'))
        ->where('customer', '=', $customer)
        ->get();
        
        return $result[0]->customer_count;

    }

    public static function NewCustomer($data){

        $result = DB::table('customers')
        ->insert([
            'customer'=>$data->customer,
            'contactperson'=>$data->contactperson,
            'contactnumber'=>$data->contactnumber,
            'email'=>$data->email,
            'address'=>$data->address, 
            'tin'=>$data->tin,
            'terms'=>$data->terms,
            'taxclassification'=>$data->tax
        ]);

    }

    public static function CustomerProfile($cid){

        $result = DB::table('customers')
        ->select(
            'customer',
            'contactperson',
            'contactnumber',
            'email', 
            'address', 
            'tin', 
            'terms',
            'taxclassification'
        )
        ->where('cid', '=', $cid)
        ->get();

        return $result;

    }

    public static function UpdateCustomer($data){

        $result = DB::table('customers')
        ->where('cid', '=', $data->cid)
        ->update([
             'customer'=> $data->customer,
             'contactperson'=>$data->contactperson,
             'contactnumber'=>$data->contactnumber,
             'email'=>$data->email, 
             'address'=> $data->address, 
             'tin'=> $data->tin,
             'terms'=>$data->terms,
             'taxclassification'=>$data->tax
         ]);

    }

}
