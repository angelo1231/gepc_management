<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['preventbackhistory']], function () {

    Route::group(['middleware' => 'checklogin'] , function(){
        Route::get('/', 'LoginController@index');
        Route::get('/login', 'LoginController@index')->name('login');
    });
    
    Route::group(['middleware' => 'checkuser'] , function(){
        Route::get('/main', 'MainController@index');
        Route::get('/user', 'UserController@index');
        Route::get('/user/{action?}/{view?}/{id?}', 'UserController@index');
        Route::get('/usergroup', 'UserGroupController@index');
        Route::get('/profile', 'ProfileController@index');
        Route::get('/supplier', 'SupplierController@index');
        Route::get('/customer', 'CustomerController@index');
        Route::get('/currency', 'CurrencyController@index');
        Route::get('/process/{view?}', 'ProcessController@index');
        Route::get('/finishgoods', 'FGFileController@index');
        Route::get('/rawmaterials', 'RMFileController@index');
        Route::get('/purchasing/{view?}/{action?}/{id?}', 'PurchasingController@index');
        Route::get('/rminventory/printrm/{sort?}', 'RMInventoryController@PrintRM');
        Route::get('/rminventory/{view?}/{action?}/{id?}', 'RMInventoryController@index');
        Route::get('/planning/{view?}/{action?}/{id?}', 'PlanningController@index');
        Route::get('/fginventory/{view?}/{action?}/{id?}', 'FGInventoryController@index');
        Route::get('/sales/{view?}/{action?}/{id?}', 'DeliveryController@index');
        Route::get('/qualityassurance/{view?}/{action?}/{id?}', 'QualityAssuranceController@index');
        Route::get('/qafinalinspection/new/{joid?}/{mid?}/{pid?}', 'QualityAssuranceController@NewFinalInspection');
        Route::get('/qafinalinspection/info/{qafiid?}', 'QualityAssuranceController@QAFinalInformationProfile');
        Route::get('/production/{view?}/{id?}', 'ProductionController@index');
        Route::get('/productionmonitoring', 'ProductionMonitoringController@index');
        Route::get('/productionoperation/{view?}/{id?}', 'ProductionOperationController@index');
        Route::get('/settings', 'SettingsController@index');
        Route::get('/deliverysales', 'DeliverySalesController@index');
        Route::get('/papercombination', 'PaperCombinationController@index');
        Route::get('/qainfo/rejecttype', 'QualityAssuranceRejectTypeController@index');
    });

});

//Email Temp
Route::get('/email', 'PurchasingController@Email');

