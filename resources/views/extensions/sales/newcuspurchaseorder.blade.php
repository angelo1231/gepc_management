@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> New Purchase Order</b></h3>
          <a href="{{ url("/sales/cuspurchaseorder") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-3">

                <div class="form-group">
                    <label for="cmbcustomer">Customer</label>
                    <select id="cmbcustomer" name="cmbcustomer" class="form-control">
                    </select>
                </div>

                <div class="form-group">
                    <label for="txtponumber">Purchase Order #</label>
                    <input id="txtponumber" name="txtponumber" type="text" class="form-control" placeholder="Purchase Order Number">
                </div>

                {{-- <div class="form-group">
                    <label for="txtdeldate">Delivery Date</label>
                    <input id="txtdeldate" name="txtdeldate" type="text" class="form-control" placeholder="Delivery Date">
                </div> --}}
                <div class="checkbox">
                    <label><input id="chkopenpo" name="chkopenpo" type="checkbox"><strong>Open PO</strong></label>
                </div>

            </div>
            <div class="col-md-9">

                <div class="form-group">
                    <label for="txtremarks">Remarks</label>
                    <textarea id="txtremarks" name="txtremarks" class="form-control" rows="4" style="width: 400px;"></textarea>
                </div>

            </div>

            <div class="col-md-12">

                <table id="tblitems" class="table" style="margin-top: 10px;">
                        <tr>
                            <th style="width: 250px;">Item</th>
                            <th>Partnumber</th>
                            <th>Partname</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th><button id="btnadd" name="btnadd" class="btn btn-flat btn-primary" disabled><i class="fa fa-plus"></i></button></th>
                        </tr>
                        <tr>
                            <td>
                                <select id="cmbitems1" name="cmbitems[]" class="form-control" disabled></select>
                            </td>
                            <td>
                                <input id="txtpartnumber1" name="txtpartnumber[]" class="form-control" type="text" placeholder="Partnumber" readonly>
                            </td>
                            <td>
                                <input id="txtpartname1" name="txtpartname[]" class="form-control" type="text" placeholder="Partname" readonly>
                            </td>
                            <td>
                                <input id="txtdescription1" name="txtdescription[]" class="form-control" type="text" placeholder="Description" readonly>
                            </td>
                            <td>
                                <textarea id="txtsize1" name="txtsize[]" class="form-control" rows="4" placeholder="Size" readonly></textarea>
                            </td>
                            <td>
                                <input id="txtprice1" name="txtprice[]" class="form-control" type="text" placeholder="Price" readonly>
                            </td>
                            <td>
                                <input id="txtqty1" name="txtqty[]" class="form-control" type="text" placeholder="Quantity" disabled>
                            </td>
                            <td>
                                <input id="txttotal1" name="txttotal[]" class="form-control" type="text" placeholder="Total" readonly>
                            </td>
                            <td></td>
                        </tr>
                </table>
                <label id="lblgrandtotal" style="float: right; margin-right: 65px;">Grand Total: 0</label>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
            <button id="btnsavepo" name="btnsavepo" class="btn btn-primary btn-flat" style="float:right;"><i class="fa fa-floppy-o" style="font-size: 12px;"></i><strong> Save Information</strong></button>
                
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var itemcount = 1;
    var grandtotal = 0;
    var itemarray = new Array();
    var customer;
    var isopenpo = 0;

    $(document).ready(function(){

        //Load
        LoadCustomers($('#cmbcustomer').attr('id'));

        // $("#txtdeldate").datepicker({ 
        //     dateFormat: 'yy-mm-dd' 
        // });

        //Set Select2
        $('#cmbcustomer').select2({
            theme: 'bootstrap'
        })

    });

    $('#cmbcustomer').on('change', function(){

        customer = $(this).val();
        
        //Enable
        $('#btnadd').attr('disabled', false);
        $('#cmbitems1').attr('disabled', false);
        $('#txtqty1').attr('disabled', false);

        RemoveAll();
        LoadCustomerItems(customer, $('#cmbitems'+itemcount).attr('id'));

    });

    $('#btnadd').on('click', function(){

        itemcount += 1;
        itemarray.push(itemcount);
        $('#tblitems').append('<tr id="'+itemcount+'"><td><select id="cmbitems'+itemcount+'" name="cmbitems[]" class="form-control"></select></td><td><input id="txtpartnumber'+itemcount+'" name="txtpartnumber[]" class="form-control" type="text" placeholder="Partnumber" readonly></td><td><input id="txtpartname'+itemcount+'" name="txtpartname[]" class="form-control" type="text" placeholder="Partname" readonly></td><td><input id="txtdescription'+itemcount+'" name="txtdescription[]" class="form-control" type="text" placeholder="Description" readonly></td><td><textarea id="txtsize'+itemcount+'" name="txtsize[]" class="form-control" rows="4" placeholder="Size" readonly></textarea></td><td><input id="txtprice'+itemcount+'" name="txtprice[]" class="form-control" type="text" placeholder="Price" readonly></td><td><input id="txtqty'+itemcount+'" name="txtqty[]" class="form-control" type="text" placeholder="Quantity" readonly></td><td><input id="txttotal'+itemcount+'" name="txttotal[]" class="form-control" type="text" placeholder="Total" readonly></td><td><button id="btnremove" name="btnremove" class="btn btn-flat btn-danger" value="'+itemcount+'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadCustomerItems(customer, $('#cmbitems'+itemcount).attr('id'));

    });

    $(document).on('click', '#btnremove', function(){

        var item = $(this).val();
        var position = $.inArray(parseInt(item), itemarray);

        if(~position){
            itemarray.splice(position, 1);
            $('#'+item).remove();
            GrandTotal();
        }

    });

    $(document).on('change', '[name="cmbitems[]"]', function(){

        var id = $(this).attr('id');
        var fgid = $(this).val();

        split_string_id = id.split(/(\d+)/);
        $('#txttotal'+split_string_id[1]).val('');

        var data = $("[name='cmbitems[]']").serializeArray();
        var count = 0;
        for(var i=0;i<data.length;i++){ //Validation
           if(data[i].value==fgid){
              count ++;
           }
        }

        if(count==1){

            $.ajax({
                url: '{{ url("api/sales/newpurchaseorder/getfgprofile") }}',
                type: 'get',
                data: {
                    fgid: fgid
                },
                dataType: 'json',
                success: function(response){

                    $('#txtpartnumber'+split_string_id[1]).val(response.partnumber);
                    $('#txtpartname'+split_string_id[1]).val(response.partname);
                    $('#txtdescription'+split_string_id[1]).val(response.description);
                    $('#txtsize'+split_string_id[1]).val(response.idsize + '\n' + response.odsize);
                    $('#txtprice'+split_string_id[1]).val(response.price);
                    $('#txtqty'+split_string_id[1]).prop('readonly', false);
                    $('#txtqty'+split_string_id[1]).val('0');     

                }
            });

        }
        else{

            toastr.error("You have already select this item please select another item.", '', { positionClass: 'toast-top-center' });

            $('#txtpartnumber'+split_string_id[1]).val('');
            $('#txtpartname'+split_string_id[1]).val('');
            $('#txtdescription'+split_string_id[1]).val('');
            $('#txtsize'+split_string_id[1]).val('');
            $('#txtprice'+split_string_id[1]).val('');
            $('#txtqty'+split_string_id[1]).prop('readonly', true);
            $('#txtqty'+split_string_id[1]).val('');
            GrandTotal();

        }
        
    });

    $(document).on('keyup', '[name="txtqty[]"]', function(){

        var id = $(this).attr('id');
        var qty = $(this).val();

        split_string_id = id.split(/(\d+)/);
        var fgid = $('#cmbitems'+split_string_id[1]).val();

        $.ajax({
            url: '{{ url("api/sales/newpurchaseorder/getcurrency") }}',
            type: 'get',
            data: {
                fgid: fgid,
                qty: qty
            },
            dataType: 'json',
            success: function(response){

                $('#txttotal'+split_string_id[1]).val(response.total);
                GrandTotal();

            }
        });
       
    });

    $('#btnsavepo').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this purchase order information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SavePOInformation();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    $('#chkopenpo').on('click', function(){

        if($(this).prop('checked')){
            isopenpo = 1;
        }
        else{
            isopenpo = 0;
        }

    });

    function SavePOInformation(){

        var validateblank = false;
        var validatenegative = false;
        var cid = $('#cmbcustomer').val();
        var ponumber = $('#txtponumber').val()
        // var deliverydate = $('#txtdeldate').val();
        var remarks = $('#txtremarks').val();
        var fgid = $('[name="cmbitems[]"]').serializeArray();
        var qty = $('[name="txtqty[]"]').serializeArray();
        var total = $('[name="txttotal[]"]').serializeArray();

        //Validation
        for(var i=0;i<qty.length;i++){
            
            if(qty[i].value==""){
                validateblank = true;
                break;
            }

            if(qty[i].value<=0){
                validatenegative = true;
                break;
            }

        }

        if(cid==null){
            toastr.error("Please select a customer.", '', { positionClass: 'toast-top-center' });
        }
        else if(ponumber==""){
            toastr.error("Please input the purchase order number.", '', { positionClass: 'toast-top-center' });
        }
        // else if(deliverydate==""){
        //     toastr.error("Please select the delivery date.", '', { positionClass: 'toast-top-center' });
        // }
        else{

            if(!validateblank){

                if(!validatenegative){

                    $.ajax({
                        url: '{{ url("api/sales/newpurchaseorder/savepurchaseorder") }}',
                        type: 'post',
                        data: {
                            cid: cid,
                            ponumber: ponumber,
                            // deliverydate: deliverydate,
                            remarks: remarks,
                            grandtotal: grandtotal,
                            fgid: fgid,
                            qty: qty,
                            total: total,
                            isopenpo: isopenpo
                        },
                        dataType: 'json',
                        beforeSend: function(){
                            H5_loading.show();
                        },
                        success: function(response){

                            if(response.success){

                                H5_loading.hide();
                                window.location = "{{ url('/sales/cuspurchaseorder') }}";

                                //Socket Emit
                                socket.emit('cuspurchaseorder');

                            }
                            else{
                                H5_loading.hide();
                                toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                                $('#txtponumber').val('');
                                $('#txtponumber').focus();
                            }

                        }
                    });

                }
                else{
                    toastr.error("Please dont input negative numbers or equal to zero quantity.", '', { positionClass: 'toast-top-center' });
                }

            }
            else{
                toastr.error("Please dont leave any blank information.", '', { positionClass: 'toast-top-center' });
            }

        }

    }

    function RemoveAll(){

        for(var i = 0; i < itemarray.length; i++){

            $('#'+itemarray[i].toString()).remove();

        }

        //Set
        itemcount = 1;
        itemarray = [];
        $('#txtpartnumber1').val('');
        $('#txtpartname1').val('');
        $('#txtdescription1').val('');
        $('#txtsize1').val('');
        $('#txtprice1').val('');
        $('#txtqty1').val('0');
        $('#txttotal1').val('');        

    }

    function LoadCustomerItems(customer, id){

        $.ajax({
            url: '{{ url("api/sales/newpurchaseorder/loadcustomeritems") }}',
            type: 'get',
            data: {
                customer: customer
            },
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a item</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["fgid"] +'">'+  response.data[i]["item"] +'</option>');
                }

                 $('#'+id).select2({
                     theme: 'bootstrap'
                 })

            }
        });

    }

    function LoadCustomers(id){

        $.ajax({
            url: '{{ url("api/sales/newpurchaseorder/loadcustomer") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Customer</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                }

            }
        });

    }

    function GrandTotal(){

        grandtotal = 0;        
        var data  = document.getElementsByName("txttotal[]");
        for(var i = 0; i < data.length; i++){
           
           if(data[i].value!=""){
            grandtotal += parseFloat(data[i].value);
           }
           
        }

        grandtotal.toFixed(2);
        $('#lblgrandtotal').text('Grand Total: '+grandtotal.toString());

    }

  </script>  

@endsection