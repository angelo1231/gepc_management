<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Login API
Route::post('login/loginuser', 'LoginController@Login');
Route::post('login/logoutuser', 'LoginController@Logout');

//Users API
Route::get('user/userinformation', 'UserController@UserInformation');
Route::get('user/userprofile', 'UserController@UserProfile');
Route::post('user/updateprofile', 'UserController@UpdateProfile');
Route::post('user/changepassword', 'UserController@ChangePassword');
Route::post('user/registeruser', 'UserController@RegisterUser');
Route::post('user/resetuser', 'UserController@ResetPassword');
Route::get('user/loadusergroup', 'UserController@LoadUserGroup');
Route::get('user/getusertype', 'UserController@GetUsertype');

//Profile API
Route::post('profile/uploadimage', 'ProfileController@UploadImage');
Route::post('profile/updateinformation', 'ProfileController@UpdateInformation');
Route::post('profile/changepassword', 'ProfileController@ChangePassword');
Route::post('profile/uploadesign', 'ProfileController@UploadESign');

//Suppliers API
Route::get('supplier/supplierinformation', 'SupplierController@SupplierInformation');
Route::post('supplier/newsupplier', 'SupplierController@NewSupplier');
Route::get('supplier/supplierprofile', 'SupplierController@SupplierProfile');
Route::post('supplier/updatesupplier', 'SupplierController@UpdateSupplier');

//Customers API
Route::get('customer/customerinformation', 'CustomerController@CustomerInformation');
Route::post('customer/savecustomer', 'CustomerController@SaveCustomer');
Route::get('customer/customerprofile', 'CustomerController@CustomerProfile');
Route::post('customer/updatecustomer', 'CustomerController@UpdateCustomer');

//Currency API
Route::get('currency/currencyinformation', 'CurrencyController@CurrencyInformation');
Route::post('currency/newcurrency', 'CurrencyController@NewCurrency');
Route::get('currency/currencyprofile', 'CurrencyController@CurrencyProfile');
Route::post('currency/updatecurrency', 'CurrencyController@UpdateCurrency');

//Process API
Route::get('process/processinformation', 'ProcessController@ProcessInformation');
Route::post('process/saveprocess', 'ProcessController@SaveProcess');
Route::get('process/processprofile', 'ProcessController@ProcessProfile');
Route::post('process/updateprocess', 'ProcessController@UpdateProcess');
Route::get('groupprocess/loadprocess', 'ProcessController@LoadProcess');
Route::post('groupprocess/savegroupprocess', 'ProcessController@SaveGroupProcess');
Route::get('groupprocess/loadgroupprocess', 'ProcessController@LoadGroupProcess');
Route::get('groupprocess/loadgroupprocessinformation', 'ProcessController@LoadGroupProcessInformation');
Route::post('groupprocess/deletegroupprocess', 'ProcessController@DeleteGroupProcess');
Route::post('groupprocess/updategroupprocessinformation', 'ProcessController@UpdateGroupProcessInformation');

//Finsihgoods API
Route::get('finishgoods/finishgoodsinformation', 'FGFileController@FinishGoodsInformation');
Route::post('finishgoods/newitem', 'FGFileController@FGNewItem');
Route::get('finishgoods/finishgoodsprofile', 'FGFileController@FinishGoodsProfile');
Route::post('finishgoods/updateitem', 'FGFileController@FGUpdateItem');
Route::get('finishgoods/loadcustomer', 'FGFileController@LoadCustomer');
Route::get('finishgoods/loadprocess', 'FGFileController@LoadProcess');
Route::get('finishgoods/loadcurrency', 'FGFileController@LoadCurrency');
Route::get('finishgoods/loadcodeprocess', 'FGFileController@LoadCodeProcess');
Route::get('finishgoods/downloadexceltemp', 'FGFileController@DownloadExcelTemp');
Route::post('finishgoods/importcustomerfg', 'FGFileController@ImportCustomerFG');
Route::post('finishgoods/uploadattachment', 'FGFileController@UploadAttachment');
Route::post('finishgoods/updateattachment', 'FGFileController@UpdateAttachment');

//RawMaterial File API
Route::get('rawmaterial/loadsupplier', 'RMFileController@LoadSupplier');
Route::get('rawmaterial/loadcurrency', 'RMFileController@LoadCurrency');
Route::get('rawmaterial/loadflute', 'RMFileController@LoadFlute');
Route::get('rawmaterial/loadboardpound', 'RMFileController@LoadBoardPound');
Route::post('rawmaterial/newitem', 'RMFileController@RMNewItem');
Route::get('rawmaterial/rawmaterialsinformation', 'RMFileController@RawMaterialInformation');
Route::get('rawmaterial/rawmaterialsprofile', 'RMFileController@RawMaterialsProfile');
Route::get('rawmaterial/updateitem', 'RMFileController@RMUpdateItem');
Route::get('rawmaterial/downloadexceltemp', 'RMFileController@DownloadExcelTemp');
Route::post('rawmaterial/importrawmaterials', 'RMFileController@ImportRawMaterials');

//Purchasing API
Route::get('purchasing/genponumber', 'PurchasingController@GenPONumber');
Route::get('purchasing/loadsupplier', 'PurchasingController@LoadSupplier');
Route::get('purchasing/supplierprofile', 'PurchasingController@SupplierProfile');
Route::get('purchasing/loaditem', 'PurchasingController@LoadItem');
Route::get('purchasing/rawmaterialprofile', 'PurchasingController@RawMaterialProfile');
Route::get('purchasing/currency', 'PurchasingController@Currency');
Route::post('purchasing/savepoinfo', 'PurchasingController@SavePOInformation');
Route::get('purchasing/loadpoinformation', 'PurchasingController@LoadPOInformation');
Route::post('purchasing/autonewpurchaseorder', 'PurchasingController@AutoNewPurchaseOrder');
Route::get('purchasing/getsuppliername', 'PurchasingController@GetSupplierName');
Route::get('purchasing/loadcountpr', 'PurchasingController@LoadCountPR');
Route::post('purchasing/approvepo', 'PurchasingController@ApprovePO');
Route::get('purchasing/loadselecteditem', 'PurchasingController@LoadSelectedItem');
Route::get('purchasing/getiteminformation', 'PurchasingController@GetItemInformation');
Route::post('purchasing/updateiteminformation', 'PurchasingController@UpdateItemInformation');
Route::get('purchasing/reloadpoiteminformation', 'PurchasingController@ReloadPOItemInformation');

//Purchasing Email API
Route::post('purchasing/poconvertpdf', 'PurchasingController@POConvertPDF');
Route::post('purchasing/sendpoemail', 'PurchasingController@SendPOEmail');


//RawMaterial Inventory API
Route::get('rawmaterialinv/loadsupplier', 'RMInventoryController@LoadSupplier');
Route::get('rawmaterialinv/rawmaterialsinformation', 'RMInventoryController@RawMaterialInformation');
Route::get('rawmaterialinv/loadpodeliveryinv', 'RMInventoryController@LoadPONumberIndividual');
Route::get('rawmaterialinv/loadpoitemprofile', 'RMInventoryController@LoadPORawMaterialProfile');
Route::post('rawmaterialinv/savedeliveryinformationinv', 'RMInventoryController@SaveDeliveryInformationIndividual');
Route::get('rawmaterialinv/loadpo', 'RMInventoryController@LoadPO');
Route::get('rawmaterialinv/loadporawmaterial', 'RMInventoryController@LoadPORawMaterial');
Route::post('rawmaterialinv/savepodelivery', 'RMInventoryController@SavePODelivery');
Route::get('rawmaterialinv/downloadexcel/{sort?}', 'RMInventoryController@DownloadExcel');
Route::get('rawmaterialinv/loadstartenddate', 'RMInventoryController@LoadStartEndDate');
Route::get('rawmaterialinv/generateaudit', 'RMInventoryController@LoadAuditInformation');
Route::get('rawmaterialinv/generateauditrm', 'RMInventoryController@LoadAuditRMInformation');
Route::get('rawmaterialinv/rminformation', 'RMInventoryController@RMInformation');
Route::get('rawmaterialinv/loadrmstockonhand', 'RMInventoryController@LoadRMStockOnHand');
Route::post('rawmaterialinv/savermstockonhand', 'RMInventoryController@SaveRMStockOnHand');
Route::get('rawmaterialinv/loadmrcount', 'RMInventoryController@LoadMRCount');

//Planning API
Route::get('planning/loadcustomers', 'PlanningController@LoadCustomers');
Route::get('planning/loadcustomeritems', 'PlanningController@LoadCustomerItems');
Route::get('planning/loadrawmaterials', 'PlanningController@LoadRawMaterials');
Route::post('planning/saverequestmaterial', 'PlanningController@SaveRequestMaterial');
Route::get('planning/cuspurchaseorder/loadcustomers', 'PlanningController@LoadCustomers');
Route::get('planning/cuspurchaseorder/loadcustomerpo', 'PlanningController@LoadCustomerPO');
Route::get('planning/genjonumber', 'PlanningController@GenJONumber');
Route::post('planning/createjoborder', 'PlanningController@CreateJobOrder');
Route::get('planning/loadjoborderinformation', 'PlanningController@LoadJobOrderInformation');
Route::get('planning/getdeliveryqtysalesorder', 'PlanningController@GetDeliveryQtySalesOrder');
Route::get('planning/loadrawmaterialsexcess', 'PlanningController@LoadRawMaterialsExcess');
Route::get('planning/cuspurchaseorder/loaditemsonumber', 'PlanningController@LoadItemSONumber');
Route::get('planning/cuspurchaseorder/loadquantityreqso', 'PlanningController@LoadQuantityReqSO');
Route::post('planning/cuspurchaseorder/savepurchaserequest', 'PlanningController@SavePurchaseRequest');
Route::get('planning/cuspurchaseorder/loadsoiteminformation', 'PlanningController@LoadSOItemInformation');
Route::post('planning/cuspurchaseorder/deletesoinformation', 'PlanningController@DeleteSOInformation');
Route::post('planning/cuspurchaseorder/savepurchaserequestindividual', 'PlanningController@SavePurchaseRequestIndividual');
Route::post('planning/cuspurchaseorder/createnewprs', 'PlanningController@CreateNewPRS');
Route::post('planning/cuspurchaseorder/cancelnewprs', 'PlanningController@CancelNewPRS');
Route::post('planning/cuspurchaseorder/savenewprsitem', 'PlanningController@SaveNewPRSItem');
Route::post('planning/cuspurchaseorder/deletepritem', 'PlanningController@DeletePRItem');
Route::post('planning/cuspurchaseorder/savedelpriteminfo', 'PlanningController@SaveDelPRItemInfo');
Route::post('planning/cuspurchaseorder/deletedeldeliveryinfo', 'PlanningController@DeleteDelDeliveryInfo');
Route::get('planning/loadpurchaserequestinformation', 'PlanningController@LoadPurchaseRequestInformation');
Route::get('planning/loadpurchaserequestdata', 'PlanningController@LoadPurchaseRequestData');
Route::get('planning/loadmaterialrequestinfo', 'PlanningController@LoadMaterialRequestInfo');
Route::get('planning/loadmaterialrequestdata', 'PlanningController@LoadMaterialRequestData');
Route::get('planning/loadcustomerfgsalesorder', 'PlanningController@LoadCustomerFGSalesOrder');
Route::get('planning/loadrawmaterialwithexcess', 'PlanningController@LoadRawMaterialWithExcess');
Route::post('planning/saverequestmaterialexcess', 'PlanningController@SaveRequestMaterialExcess');
Route::get('planning/loadmrforjobordercreation', 'PlanningController@LoadMRForJobOrderCreation');

//Material Request API
Route::get('materialrequest/genmrinumber', 'MaterialRequestController@GenMRINumber');
Route::get('materialrequest/loadmaterialrequestinformation', 'MaterialRequestController@LoadMaterialRequestInformation');
Route::get('materialrequest/loadmaterialrequestinformationinv', 'MaterialRequestController@LoadMaterialRequestInformationINV');
Route::get('materialrequest/getmri', 'MaterialRequestController@GetMRI');
Route::get('materialrequest/getrawmaterialbalance', 'MaterialRequestController@GetRawMaterialBalance');
Route::post('materialrequest/approvemriinformation', 'MaterialRequestController@ApproveMRIInformation');
Route::get('materialrequest/genissuancenumber', 'MaterialRequestController@GenIssuanceNumber');
Route::get('materialrequest/getmriinventory', 'MaterialRequestController@GetMRIInventory');
Route::post('materialrequest/disapprovemr', 'MaterialRequestController@DisapproveMR');

//Purchase Request API
Route::get('purchaserequest/genpurchaserequestnumber', 'PurchaseRequestController@GenPurchaseRequestNumber');
Route::get('purchaserequest/loadrawmaterials', 'PurchaseRequestController@LoadRawMaterials');
Route::post('purchaserequest/savepr', 'PurchaseRequestController@SavePR');
Route::get('purchaserequest/loadpurchaserequestinformation', 'PurchaseRequestController@LoadPurchaseRequestInformation');
Route::get('purchaserequest/loadpurchaserequestinfopurchasing', 'PurchaseRequestController@LoadPurchaseRequestInfoPurchasing');
Route::post('purchaserequest/updatepurchaserequestinfopurchasing', 'PurchaseRequestController@UpdatePurchaseRequestInfoPurchasing');
Route::get('purchaserequest/loadpurchaserequestsuppliers', 'PurchaseRequestController@LoadPurchaseRequestSuppliers');
Route::get('purchaserequest/loadsuppliers', 'PurchaseRequestController@LoadSuppliers');
Route::get('purchaserequest/loadflutepapercombination', 'PurchaseRequestController@LoadFlutePaperCombination');
Route::get('purchaserequest/loadpapercomprice', 'PurchaseRequestController@LoadPaperComPrice');

//Sales API
Route::get('sales/newpurchaseorder/loadcustomer', 'DeliveryController@LoadCustomer');
Route::get('sales/newpurchaseorder/loadcustomeritems', 'DeliveryController@LoadCustomerItems');
Route::get('sales/newpurchaseorder/getfgprofile', 'DeliveryController@GetFGProfile');
Route::get('sales/newpurchaseorder/getcurrency', 'DeliveryController@GetCurrency');
Route::post('sales/newpurchaseorder/savepurchaseorder', 'DeliveryController@SavePurchaseOrder');
Route::get('sales/cuspurchaseorder/loadcustomer', 'DeliveryController@LoadCustomer');
Route::get('sales/cuspurchaseorder/loadcustomerpo', 'DeliveryController@LoadCustomerPO');
Route::get('sales/getpocustomer', 'DeliveryController@GetPOCustomer');
Route::get('sales/getpoitems', 'DeliveryController@GetPOItems');
Route::get('sales/loaditeminformation', 'DeliveryController@LoadItemInformation');
Route::get('sales/currency', 'DeliveryController@Currency');
Route::post('sales/savedeliveryinformation', 'DeliveryController@SaveDeliveryInformation');
Route::get('sales/loaddeliveryinformation', 'DeliveryController@LoadDeliveryInformation');
Route::get('sales/validatedeliverynumber', 'DeliveryController@ValidateDeliveryNumber');
Route::get('sales/downloadexcel/{id?}', 'DeliveryController@DownloadExcel');
Route::post('sales/createinvoice', 'DeliveryController@CreateInvoice');
Route::get('sales/loadpreparationslipinformation', 'DeliveryController@LoadPreparationSlipInformation');
Route::post('sales/cuspurchaseorderinfo/savesalesorderdelivery', 'DeliveryController@SaveSalesOrderDelivery');
Route::get('sales/cuspurchaseorderinfo/loaditemdeliveryinformation', 'DeliveryController@LoadItemDeliveryInformation');
Route::get('sales/cuspurchaseorderinfo/loadcustomeritems', 'DeliveryController@LoadCustomerItems');
Route::get('sales/cuspurchaseorderinfo/getfgprofile', 'DeliveryController@GetFGProfile');
Route::get('sales/cuspurchaseorderinfo/getcurrency', 'DeliveryController@GetCurrency');
Route::post('sales/cuspurchaseorderinfo/addopenpoitems', 'DeliveryController@AddOpenPOItems');
Route::get('sales/cuspurchaseorderinfo/reloadpoitems', 'DeliveryController@ReloadPOItems');
Route::get('sales/newpreparationslip/loadcustomer', 'DeliveryController@LoadCustomer');
Route::get('sales/newpreparationslip/getpocustomer', 'DeliveryController@GetPOCustomer');
Route::get('sales/newpreparationslip/getpoitems', 'DeliveryController@GetPOItems');
Route::get('sales/newpreparationslip/loaditeminformation', 'DeliveryController@LoadItemInformation');
Route::post('sales/newpreparationslip/savepreparationslipinformation', 'DeliveryController@SavePreparationSlipInformation');
Route::post('sales/createdr', 'DeliveryController@CreateDR');
Route::get('sales/cuspurchaseorderinfo/loaddeliddeliveryinfo', 'DeliveryController@LoadDelIDDeliveryInfo');
Route::post('sales/cuspurchaseorderinfo/updatedelqtyinfo', 'DeliveryController@UpdateDelQtyInfo');
Route::post('sales/cuspurchaseorderinfo/updatequantityitem', 'DeliveryController@UpdateQuantityItem');

//Finish Goods Inventory API
Route::get('finishgoodsinv/loadfinishgoodsinformation', 'FGInventoryController@LoadFinsihGoodsInformation');
Route::get('finishgoodsinv/loadcustomers', 'FGInventoryController@LoadCustomers');
Route::get('finishgoodsinv/loadjoborderinformation', 'FGInventoryController@LoadJobOrderInformation');
Route::post('finishgoodsinv/saveproductioninformation', 'FGInventoryController@SaveProductionInformation');
Route::get('finishgoodsinv/loadfginfo', 'FGInventoryController@LoadFGInfo');
Route::get('finishgoodsinv/loadfgauditinformation', 'FGInventoryController@LoadFGAuditInformation');
Route::get('finishgoodsinv/loadstartenddate', 'FGInventoryController@LoadStartEndDate');
Route::get('finishgoodsinv/generateaudit', 'FGInventoryController@GenerateAudit');
Route::get('finishgoodsinv/loadfgjo', 'FGInventoryController@LoadFGJO');
Route::post('finishgoodsinv/savefgindividualproduction', 'FGInventoryController@SaveFGIndividualProduction');
Route::get('finishgoodsinv/loadmstnumbers', 'FGInventoryController@LoadMSTNumbers');
Route::get('finishgoodsinv/loaddrnumbers', 'FGInventoryController@LoadDRNumbers');
Route::get('finishgoodsinv/loaddritems', 'FGInventoryController@LoadDRItems');
Route::post('finishgoodsinv/validatedelivery', 'FGInventoryController@ValidateDelivery');
Route::get('finishgoodsinv/downloadexcel/{id?}', 'FGInventoryController@DownloadExcel');
Route::get('finishgoodsinv/loadcountps', 'FGInventoryController@LoadCountPS');
Route::get('finishgoodsinv/loadfgstockonhand', 'FGInventoryController@LoadFGStockOnHand');
Route::post('finishgoodsinv/savestockonhandinformation', 'FGInventoryController@SaveStockOnHandInformation');

//Finish Goods Inventory Preparation Slip API
Route::get('finishgoodsinv/loadpreparationslipinformation', 'FGInventoryController@LoadPreparationSlipInformation');
Route::get('finishgoodsinv/loadpreparationslipitems', 'FGInventoryController@LoadPreparationSlipItems');
Route::post('finishgoodsinv/approvepreparationslip', 'FGInventoryController@ApprovePreparationSlip');
Route::post('finishgoodsinv/disapprovepreparationslip', 'FGInventoryController@DisapprovePreparationSlip');

//Quality Assurance API
Route::get('qualityassurance/cocinformations', 'QualityAssuranceController@COCInformations');
Route::get('qualityassurance/loadfinalinspectioninformation', 'QualityAssuranceController@LoadFinalInspectionInformation');
Route::post('qualityassurance/savefinalinspection', 'QualityAssuranceController@SaveFinalInspection');
Route::post('qualityassurance/confirmfinalinspection', 'QualityAssuranceController@ConfirmFinalInspection');

//Productin API
Route::get('production/loadmstnumber', 'ProductionController@LoadMSTNumber');
Route::post('production/updatemststatus', 'ProductionController@UpdateMSTStatus');

//Production Monitoring
Route::get('productionmonitoring/loadprocess', 'ProductionMonitoringController@LoadProcess');
Route::get('productionmonitoring/getprocessjo', 'ProductionMonitoringController@GetProcessJO');
Route::get('productionmonitoring/getmonitoringinformation', 'ProductionMonitoringController@GetMonitoringInformation');
Route::post('productionmonitoring/saveoperationinformation', 'ProductionMonitoringController@SaveOperationInformation');
Route::get('productionmonitoring/loadrejecttype', 'ProductionMonitoringController@LoadRejectType');

//Production Operation
Route::get('productionoperation/loadjoborder', 'ProductionOperationController@LoadJobOrder');
Route::post('productionoperation/saveoperation', 'ProductionOperationController@SaveOperation');
Route::get('productionoperation/loadjoborderforrequest', 'ProductionOperationController@LoadJobOrderForRequest');
Route::get('productionoperation/loadoperationinformation', 'ProductionOperationController@LoadOperationInformation');
Route::post('productionoperation/updatecreatejo', 'ProductionOperationController@UpdateCreateJO');
Route::get('productionoperationinfo/getprocesscode', 'ProductionOperationController@GetProcessCode');
Route::get('productionoperationinfo/getgroupprocess', 'ProductionOperationController@GetGroupProcess');
Route::post('productionoperationinfo/saveprocessinformation', 'ProductionOperationController@SaveProcessInformation');
Route::get('productionoperationinfo/loadjoborderprocess', 'ProductionOperationController@LoadJobOrderProcess');
Route::get('productionoperationinfo/loadprocessinfo', 'ProductionOperationController@LoadProcessInfo');
Route::get('productionoperationinfo/loadjoborderprocessinfo', 'ProductionOperationController@LoadJobOrderProcessInfo');
Route::get('productionoperationinfo/loadmonitoringprocess', 'ProductionOperationController@LoadMonitoringProcess');
Route::get('productionoperationinfo/checkcurrentprocess', 'ProductionOperationController@CheckCurrentProcess');
Route::post('productionoperationinfo/changecurrentprocess', 'ProductionOperationController@ChangeCurrentProcess');
Route::get('productionoperation/loadjorequestinformation', 'ProductionOperationController@LoadJORequestInformation');

//Settings API
Route::get('settings/loadsettings', 'SettingsController@LoadSettings');
Route::post('settings/savetaxclassification', 'SettingsController@SaveTaxClassification');
Route::get('settings/loadcompanyinfo', 'SettingsController@LoadCompanyInfo');
Route::post('settings/savecompanyinfo', 'SettingsController@SaveCompanyInfo');
Route::get('settings/loadrawmaterialsettings', 'SettingsController@LoadRawMaterialSettings');
Route::post('settings/saverawmaterial', 'SettingsController@SaveRawMaterial');
Route::get('settings/loadusergroup', 'SettingsController@LoadUserGroup');
Route::post('settings/saveeditaccess', 'SettingsController@SaveEditAccess');
Route::get('settings/loadeditaccessinformation', 'SettingsController@LoadEditAccessInformation');
Route::get('settings/loadeditaccessprofile', 'SettingsController@LoadEditAccessProfile');
Route::post('settings/updateeditaccess', 'SettingsController@UpdateEditAccess');
Route::post('settings/deleteeditaccess', 'SettingsController@DeleteEditAccess');

//Delivery Sales API
Route::get('deliverysales/loadcustomer', 'DeliverySalesController@LoadCustomer');
Route::get('deliverysales/loaddeliverysalesinformation', 'DeliverySalesController@LoadDeliverySalesInformation');
Route::get('deliverysales/loadsalessummary', 'DeliverySalesController@LoadSalesSummary');
Route::get('deliverysales/loadsalespercustomer', 'DeliverySalesController@LoadSalesPerCustomer');

//User Group API
Route::post('usergroup/saveusergroup', 'UserGroupController@SaveUserGroup');
Route::get('usergroup/loadusergroupinformation', 'UserGroupController@LoadUserGroupInformation');
Route::get('usergroup/loadaccess', 'UserGroupController@LoadAccess');
Route::post('usergroup/saveusergroupaccess', 'UserGroupController@SaveUserGroupAccess');
Route::get('usergroup/getusergroupaccess', 'UserGroupController@GetUserGroupAccess');

//Main API
Route::get('main/loadpoforapproval', 'MainController@LoadPOForApproval');

//Paper Combination API
Route::get('papercombination/loadflute', 'PaperCombinationController@LoadFlute');
Route::get('papercombination/loadboardpound', 'PaperCombinationController@LoadBoardPound');
Route::post('papercombination/savepapercombination', 'PaperCombinationController@SavePaperCombination');
Route::get('papercombination/loadpapercombination', 'PaperCombinationController@LoadPaperCombination');
Route::get('papercombination/loadpapercombinationinfo', 'PaperCombinationController@LoadPaperCombinationInfo');
Route::post('papercombination/updatepapercombination', 'PaperCombinationController@UpdatePaperCombination');

//Notification API
Route::get('notification/notifplanningmr', 'NotifController@NotifPlanningMR');

//Auth API
Route::post('auth/authlogin', 'AuthController@AuthLogin');

//Excess RM API
Route::get('rmexcess/loadexcessinformation', 'RMExcessController@LoadExcessInformation');
Route::post('rmexcess/receiveexcess', 'RMExcessController@ReceiveExcess');

//Quality Assurance Reject Type API
Route::get('qainfo/rejecttype/loadrejecttype', 'QualityAssuranceRejectTypeController@LoadRejectType');
Route::post('qainfo/rejecttype/saverejecttype', 'QualityAssuranceRejectTypeController@SaveRejectType');
Route::get('qainfo/rejecttype/loadrejecttypeinfo', 'QualityAssuranceRejectTypeController@LoadRejectTypeInfo');
Route::post('qainfo/rejecttype/updaterejecttype', 'QualityAssuranceRejectTypeController@UpdateRejectType');
