<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Profile_Model extends Model
{
    
	public static function UploadImage($id, $path){

		DB::table('users')
		->where('id', '=', $id)
		->update(["image"=>$path]);

	}

	public static function UpdateInformation($id, $data){

		DB::table('users')
		->where('id', '=', $id)
		->update([
			"firstname"=>$data->firstname,
			"lastname"=>$data->lastname,
			"mi"=>$data->mi
		]);

	}

	public static function OldPassword($id){

        $result = DB::table('users')
        ->select('password')
        ->where('id', '=', $id)
        ->get();

        return $result[0]->password;

	}
	
	public static function GetUsertype($ugroupid){

		$result = DB::table('usergroups')
		->select(
			'usergroup'
		)
		->where('ugroupid', '=', $ugroupid)
		->first();

		return $result->usergroup;

	}

	public static function UpdateUserESign($userid, $filename){

		DB::table('users')
		->where('id', '=', $userid)
		->update([
			"esign"=>$filename
		]);

	}

}
