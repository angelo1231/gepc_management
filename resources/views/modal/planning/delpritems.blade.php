<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="delpritems" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>Delivery</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txtdelpritemdeldate">Delivery Date</label>
                            <input id="txtdelpritemdeldate" name="txtdelpritemdeldate" class="form-control" placeholder="Delivery Date" type="text">
                        </div>

                        <div class="form-group">
                            <label for="txtdelpritemdelqty">Quantity Required</label>
                            <input id="txtdelpritemdelqty" name="txtdelpritemdelqty" class="form-control" placeholder="Delivery Quantity" type="number">
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btnsavedelpritem" name="btnsavedelpritem" type="button" class="btn btn-success btn-flat">Save</button>
                        <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>