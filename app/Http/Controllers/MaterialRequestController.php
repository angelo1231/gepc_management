<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\MaterialRequest_Model;
use App\RawMaterials_Model;
use Session;
use DataTables;
use Illuminate\Support\Collection;

class MaterialRequestController extends Controller
{

    private $tableschema;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }
    
    function GenMRINumber(){

        $batch = MaterialRequest_Model::GenMRINumber($this->tableschema);

        return json_encode([
            "batch"=>$batch
        ]);

    }

    function LoadMaterialRequestInformation(Request $request){

        $material_request = MaterialRequest_Model::LoadMaterialRequestInformation($request->customer);

        $data = array();
        for($i=0;$i<count($material_request);$i++){
            
            $obj = new \stdClass;

            if($material_request[$i]->status=="Done" || $material_request[$i]->status=="JO Creation"){
                $obj->status = '<a href="#"><i class="fa fa-check"></i></a>';
            }
            else if($material_request[$i]->status=="Disapproved"){
                $obj->status = '<a href="#" style="color: red;"><i class="fa fa-times"></i></a>';
            }
            else{
                $obj->status = '';
            }
            
            $obj->mri = $material_request[$i]->mrinumber;
            $obj->issuancenumber = $material_request[$i]->issuancenumber;
            $obj->finishgood = $material_request[$i]->finishgood;
            $obj->issuedby = $material_request[$i]->issuedby;
            $obj->reason = $material_request[$i]->reason;
            $obj->customer = $material_request[$i]->customer;
            $obj->panel = '<button id="btninfo" name="btninfo" class="btn btn-info btn-flat" title="Information" value="'. $material_request[$i]->requestid .'" style="width: 40px;" data-toggle="modal" data-target="#modalinformation"><i class="fa fa-info-circle"></i></button>';
        
            $data[] = $obj;
            
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'status'])->make(true);

    }

    function LoadMaterialRequestInformationINV(){

        $material_request = MaterialRequest_Model::LoadMaterialRequestInformationINV();

        $data = array();
        for($i=0;$i<count($material_request);$i++){
            
            $obj = new \stdClass;

            $obj->finishgood = $material_request[$i]->finishgood;
            $obj->issuedby = $material_request[$i]->issuedby;
            $obj->reason = $material_request[$i]->reason;
            $obj->panel = '<button id="btnapprove" name="btnapprove" class="btn btn-success btn-flat" title="Approve" value="'. $material_request[$i]->requestid .'" style="width: 40px;" data-toggle="modal" data-target="#modalapprovemri"><i class="fa fa-thumbs-o-up"></i></button> <button id="btndisapprove" name="btndisapprove" class="btn btn-danger btn-flat" title="Disapprove" value="'. $material_request[$i]->requestid .'" style="width: 40px;"><i class="fa fa-thumbs-o-down"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function GetMRI(Request $request){

        $content = "";
        $mri = MaterialRequest_Model::GetMRI($request->id);
        $mriitem = MaterialRequest_Model::GetMRIItem($request->id);

        foreach($mriitem as $val){

            $content .= '
            <tr>
                <td>'. $val->rawmaterial .'</td>
                <td>'. $val->qtyrequired .'</td>
                <td>'. $val->remarks .'</td>
                <td>'. $val->excessboard .'</td>
                <td>'. $val->qtyexcessboard .'</td>
                <td>'. $val->qtyissued .'</td>
                <td>'. $val->balance .'</td>
                <td>'. $val->rmissuedby .'</td>
            </tr>
            ';

        }

        return json_encode([
            "mrinumber"=>$mri[0]->mrinumber,
            "issuancenumber"=>$mri[0]->issuancenumber,
            "content"=>$content
        ]);

    }

    function GetRawMaterialBalance(Request $request){

        $data = MaterialRequest_Model::GetRMID($request->id);
        $rmid = explode(',', $data);

        $balance = MaterialRequest_Model::GetRawMaterialBalance($rmid);

        return json_encode([
            "balance"=>$balance
        ]);

    }

    function ApproveMRIInformation(Request $request){


        //Validation Quantity
        for($q=0;$q<count($request->qtyissued);$q++){

            if($request->qtyissued[$q]["value"]<=0){
                
                return json_encode([
                    "success"=>false,
                    "message"=>"Please input quantity issued."
                ]);

            }

        }

        //Validation Balance
        for($q=0;$q<count($request->balance);$q++){

            if($request->balance[$q]["value"]<0){
                
                return json_encode([
                    "success"=>false,
                    "message"=>"Insufficient stocks."
                ]);

            }

        }

        //Validation Delivery Number
        for($q=0;$q<count($request->drnumber);$q++){

            if($request->drnumber[$q]["value"]==""){
                
                return json_encode([
                    "success"=>false,
                    "message"=>"Please input the delivery number of the item."
                ]);

            }

        }

        //Begin Process
        $status = "";
        $issuedby = Auth::user()->lastname . ', ' . Auth::user()->firstname . ' ' . Auth::user()->mi;
        $mrinumber = MaterialRequest_Model::GetMRINumber($request->id);
        $category = MaterialRequest_Model::GetCategory($request->id);

        if($category=="Raw Material" || $category=="Excess Board"){
            $status = "JO Creation";
        }
        else{
            $status = "Done";
        }

        MaterialRequest_Model::UpdateMRIInformationINV($request->id, $request->issuancenumber, $issuedby, $status);

        for($i=0;$i<count($request->itemid);$i++){

            $stockonhand = MaterialRequest_Model::GetRMStockOnHand($request->itemid[$i]["value"], $request->qtyissued[$i]["value"]);

            //Update MRI Items Info
            MaterialRequest_Model::ApproveMRIInformation($request->itemid[$i]["value"], $request->drnumber[$i]["value"], $request->qtyissued[$i]["value"], $stockonhand);

            //Update RM Stock
            MaterialRequest_Model::UpdateRMStocks($request->itemid[$i]["value"], $stockonhand);

            //Save Audit
            RawMaterials_Model::SaveRMAudit($request->itemid[$i]["value"], "", "", $mrinumber, $request->qtyissued[$i]["value"], $stockonhand, $issuedby);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Material Request information has been update."
        ]);


    }

    function GenIssuanceNumber(){

        $issuancenumber = MaterialRequest_Model::GenIssuanceNumber();

        return json_encode([
            "issuancenumber"=>$issuancenumber
        ]);

    }

    function GetMRIInventory(Request $request){

        $content = "";
        $mriitem = MaterialRequest_Model::GetMRIItem($request->id);

        foreach($mriitem as $val){

            $balance = MaterialRequest_Model::GetRMBalance($val->rmid);

            $content .= '
            <tr>
                <td style="text-align: center; vertical-align: middle;"><input id="txtaitemid" name="txtaitemid[]" type="hidden" value="'. $val->id .'">'. $val->rawmaterial .'</td>
                <td style="text-align: center; vertical-align: middle;"><input id="txtaqtyrequired" name="txtaqtyrequired[]" type="hidden" value="'. $val->qtyrequired .'">'. $val->qtyrequired .'</td>
                <td style="text-align: center; vertical-align: middle;">'. $val->remarks .'</td>
                <td style="text-align: center; vertical-align: middle;">'. $val->excessboard .'</td>
                <td style="text-align: center; vertical-align: middle;">'. $val->qtyexcessboard .'</td>
                <td style="text-align: center; vertical-align: middle;"><input type="text" id="txtadrnumber" name="txtadrnumber[]" class="form-control" placeholder="Delivery Number"></td>
                <td style="text-align: center; vertical-align: middle;"><input type="number" id="txtaqtyissued" name="txtaqtyissued[]" class="form-control" value="0" data-id="'. $val->id .'"></td>
                <td style="text-align: center; vertical-align: middle;"><input type="text" id="txtabalance'. $val->id .'" name="txtabalance[]" class="form-control" value="'. $balance .'" data-val="'. $balance .'" readonly></td>
            </tr>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function DisapproveMR(Request $request){

        MaterialRequest_Model::DisapproveMR($request->id);

        return json_encode([
            "success"=>true,
            "message"=>"Material request information has been disapproved."
        ]);        


    }

}
