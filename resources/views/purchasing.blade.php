@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Purchasing</b></h3>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="cmbsupplier">Supplier</label>
                    <select class="form-control" id="cmbsupplier">
                    </select>
                </div>
            </div>
            <div class="col-md-9">
                    {{-- <button id="btnnewpo" name="btnnewpo" class="btn btn-success btn-flat" style="float:right; margin: 26px 0px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Purchase Order</strong></button> --}}
                    <button id="btnpr" name="btnpr" class="btn btn-primary btn-flat" style="float:right; margin: 26px 10px 26px; 0px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Purchase Request <span id="prcount" class="dot"></span></strong></button>
            </div>
            <div class="col-md-12">
                <table id="tblpo" class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>PO#</th>
                            <th>Attention</th>
                            <th>Total Quantity</th>
                            <th>Delivery Date</th>
                            <th>Grand Total</th>
                            <th>Created By</th>
                            <th>Created Date</th>
                            <th>Approved By</th>
                            <th>Approved Date</th>
                            <th>Supplier</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblpo;

      $(document).ready(function() {
     
          var msg = "{{ Session::get('message') }}";
          if(msg!=""){
            toastr.success(msg, '', { positionClass: 'toast-top-center' });
          }

          //Load
          LoadSelect2();
          LoadSupplier($('#cmbsupplier').attr("id"));
          LoadCountPR();

          //Set
          tblpo = $('#tblpo').DataTable({
                autoWidth: false,
                ordering: false,
          });

          tblpo.columns(7).visible(false);

      });

      $('#cmbsupplier').on('change', function(){

        var supplier = $(this).val();
        
        tblpo.destroy();
        tblpo = $('#tblpo').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/purchasing/loadpoinformation") }}',
                data: {
                  supplier: supplier
                },
            },
            columns : [
                {data: 'status', name: 'status'},
                {data: 'PO', name: 'PO'},
                {data: 'attn', name: 'attn'},
                {data: 'totalqty', name: 'totalqty'},
                {data: 'deliverydate', name: 'deliverydate'},
                {data: 'grandtotal', name: 'grandtotal'},
                {data: 'createdby', name: 'createdby'},
                {data: 'createddate', name: 'createddate'},
                {data: 'approvedby', name: 'approvedby'},
                {data: 'approveddate', name: 'approveddate'},
                {data: 'supplier', name: 'supplier'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

        if(supplier=="All"){
          tblpo.columns(7).visible(true); 
        }
        else{
          tblpo.columns(7).visible(false); 
        }

      });

      $('#btnnewpo').on('click', function(){

        @if(in_array(33, $access))

          window.location = "{{ url('/purchasing/newpurchaseorder') }}";

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnpr').on('click', function(){

        @if(in_array(63, $access))

          window.location = "{{ url('/purchasing/purchaserequest') }}";

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      function LoadCountPR(){

        $.ajax({
          url: '{{ url("api/purchasing/loadcountpr") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            if(response.prcount!=0){

              $('#btnpr').text('');
              $('#btnpr').append('<i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Purchase Request <span id="prcount" class="dot">'+ response.prcount +'</span></strong>');

            }
            else{

              $('#btnpr').text('');
              $('#btnpr').append('<i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Purchase Request</strong>');

            }

          }
        });

      }

      function LoadSupplier(id){

            $.ajax({
              url: '{{ url("api/purchasing/loadsupplier") }}',
              type: 'get',
              dataType: 'json',
              success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Supplier</option>');
                $('#'+id).append('<option value="All">All</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["sid"] +'">'+  response.data[i]["supplier"] +'</option>');
                }

              },
              complete: function(){

                $('#'+id).val("All").trigger("change");

              }
            });

      }

      function LoadSelect2(){

        $('#cmbsupplier').select2({
          theme: 'bootstrap'
        })

      }

      function ReloadRawMaterials(){

        var supplier = $('#cmbsupplier').val();
        if(supplier!=null){
          tblpo.ajax.reload();
        }

      }

      //Socket Function
      socket.on('reloadprcount', function(){
          LoadCountPR();
      });

  </script>  

@endsection