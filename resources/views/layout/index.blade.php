<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{ asset("images/GEPC_LOGO.png") }}">
        <title>Good Earth Packaging Corp.</title>

        {{-- CSS --}}
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href=" {{ asset('css/toastr.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">
        <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery-confirm.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/placeholderselect.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/datatable.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/loading.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select2-bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('css/tooltips.css') }}">
        <link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
        <link rel="stylesheet" href="{{ asset('css/dot.css') }}">
       
        {{-- JS --}}
        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/toastr.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/datatable.min.js') }}"></script>
        <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
        <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
        <script src="{{ asset('dist/js/app.min.js') }}"></script>
        <script src="{{ asset('dist/js/demo.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-confirm.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/loading.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/chosen.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>

        {{-- Register Service Worker --}}
        {{-- <script type="text/javascript" src="{{ asset('js/registersw.js') }}"></script> --}}

        {{-- Socket JS --}}
        <script type="text/javascript" src="{{ asset('js/socket.io.js') }}"></script> 


    </head>
    
    @yield('body')

    {{-- Load Socket Server --}}
    <script>
        //Socket
        var ipaddress = "{{ $_SERVER['SERVER_ADDR'] }}";
        var socket = io.connect('http://'+ipaddress+':8000');
    </script>

    {{-- Modal --}}
    @include('modal.auth.auth')

    @yield('script')

    {{-- Notification JS --}}
    <script>

        //Notification Sockets
        socket.on('notifplanning', function(prid){

            for(var i=0;i<prid.data.length;i++){

                $.ajax({
                    url: '{{ url("api/notification/notifplanningmr") }}',
                    type: 'get',
                    data: {
                        prid: prid.data[i]['prid']
                    },
                    dataType: 'json',
                    success: function(response){

                        toastr.info('Purchase Request #: ' + response.prdata[0]['prnumber'] + ' has been approved.', 0, { positionClass: 'toast-top-center' });                    

                    }
                });

            }

        });

        socket.on('notifproductionjorequest', function(jorequestnumber){

            toastr.info('Production you have a pending job order request # ' + jorequestnumber.data['jorequestnumber'] + '', 0, { positionClass: 'toast-top-center' });            

        });

    </script>

    {{-- Auth JS --}}
    <script>
        
        function AuthLogin(){

            var authusername = document.getElementById('txtauthusername').value;
            var authpassword = document.getElementById('txtauthpassword').value;

            //Validation
            if(authusername==""){

                toastr.error('Please input the username.', 0, { positionClass: 'toast-top-center' });                 

            }
            else if(authpassword==""){

                toastr.error('Please input the password.', 0, { positionClass: 'toast-top-center' });                 

            }
            else{

                return $.ajax({
                    url: '{{ url("api/auth/authlogin") }}',
                    type: 'post',
                    data: {
                        username: authusername,
                        password: authpassword
                    },
                    dataType: 'json',
                    success: function(response){

                        if(response.success){


                        }
                        else{

                            toastr.error(response.message, 0, { positionClass: 'toast-top-center' });                 

                        }

                    }
                });

            }

        }

        function ClearAuth(){

            document.getElementById('txtauthusername').value = '';
            document.getElementById('txtauthpassword').value = '';

        }

    </script>

</html>