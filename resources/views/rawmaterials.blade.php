@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Raw Material Information</b></h3>
        </div>
        <div class="box-body">
            <div class="col-md-6">

                <button name="btnsortall" class="btn btn-success btn-flat" value="All" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>All</strong></button>

                <button name="btnsortall" class="btn btn-primary btn-flat" value="Raw Material" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Raw Material</strong></button>

                <button name="btnsortall" class="btn btn-info btn-flat" value="Other" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Others</strong></button>

                <button name="btnsortall" class="btn btn-danger btn-flat" value="Fix Asset" style="margin: 7px 0px;"><i class="fa fa-bars" style="font-size:12px;"></i> <strong>Fix Asset</strong></button>


            </div>
            <div class="col-md-6">

                <button id="btnnewitem" name="btnnewitem" class="btn btn-success btn-flat" data-toggle="modal" data-target="#modalnewitem" style="float:right; margin: 7px 0px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Item</strong></button>

                <button id="btnpapercombination" name="btnpapercombination" class="btn btn-info btn-flat" style="float:right; margin: 7px 10px 0px 0px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Paper Combination</strong></button>

                <button id="btnimport" name="btnimport" class="btn btn-primary btn-flat" style="float:right; margin: 7px 10px;"><i class="fa fa-sign-in" style="font-size:12px;"></i> <strong>Import</strong></button>

            </div>
          <div class="col-md-12">
            <br>
            <table id="tblrawmaterial" class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Size</th>
                        <th>Flute</th>
                        <th>Board Pound</th>
                        <th>Currency</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  @include('modal.rawmaterials.maintenance.newitem')
  @include('modal.rawmaterials.maintenance.updateitem')
  @include('modal.rawmaterials.maintenance.importitem')


  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblrawmaterial;
    var uname = "";
    var udescription = "";
    var uwidth = "";
    var uwidthsize = "";
    var ulength = "";
    var ulengthsize = "";
    var uflute = "";
    var uboardpound = "";

    $(document).ready(function() {

      //Set
      tblrawmaterial = $('#tblrawmaterial').DataTable({
        autoWidth: false,
        ordering: false,
      });

      // tblrawmaterial.columns(5).visible(false);
      // tblrawmaterial.columns(6).visible(false);      

      //Load
      LoadRawMaterials("All");
      LoadSelect2();    

    });

    $('[name="btnsortall"]').on('click', function(){

      var sort = $(this).val();
      LoadRawMaterials(sort);

    });

    $('#btnnewitem').on('click', function(){

      @if(in_array(11, $access))

        LoadCurrency($('#cmbncurrency').attr('id'));
        LoadFlute($('#txtnflute').attr('id'))

        ClearNewItem();

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $('#txtnflute').on('keyup', function(){

      LoadBoardPound($(this).val(), $('#txtnboardpound').attr('id'));

    });

    $('#txtuflute').on('keyup', function(){

      LoadBoardPound($(this).val(), $('#txtuboardpound').attr('id'));

    });

    $('#btnsave').on('click', function(){

      var name = $('#txtnname').val();
      var description = $('#txtndescription').val();
      var width = $('#txtnwidth').val();
      var widthsize = $('#txtnwidthsize').val();
      var length = $('#txtnlength').val();
      var lengthsize = $('#txtnlengthsize').val();
      var flute = $('#txtnflute').val();
      var boardpound = $('#txtnboardpound').val();
      var currencyid = $('#cmbncurrency').val();
      var price = $('#txtnprice').val();
      var category = $('#cmbncategory').val();
      
      if(name==""){

        toastr.error("Please input the name of the item.", '', { positionClass: 'toast-top-center' });
        $('#txtnname').focus();

      }
      else if(category==null){

         toastr.error("Please select a category.", '', { positionClass: 'toast-top-center' });
         $('#cmbncategory').focus();

      }
      else{


          $.ajax({
            url: '{{ url("api/rawmaterial/newitem") }}',
            type: 'post',
            data: {
              name: name,
              description: description,
              width: width,
              widthsize: widthsize,
              length: length,
              lengthsize: lengthsize,
              flute: flute,
              boardpound: boardpound,
              currencyid: currencyid,
              price: price,
              category: category
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                  toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                  $("#modalnewitem .close").click();
                  ReloadRawMaterials();

              }
              else{

                $('#txtnwidth').val('');
                $('#txtnwidthsize').val('');
                $('#txtnlength').val('');
                $('#txtnlengthsize').val('');
                $('#txtnflute').val('');
                $('#txtnboardpound').val('');
                $('#txtnwidth').focus();
                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }

            }
          });

      }

    });

    $(document).on('click', '#btnedit', function(){

      @if(in_array(12, $access))

        var id = $(this).val();

        ClearUpdateItem();

        // $('#ucurrency').show();
        // $('#uprice').show();

        // $('#ucurrency').hide();
        // $('#uprice').hide();
        
        $.ajax({
            url: '{{ url("api/rawmaterial/rawmaterialsprofile") }}',
            type: 'get',
            data: {id: id},
            dataType: 'json',
            success: function(response){
 
                LoadInformation($('#cmbucurrency').attr('id'), response);
                $('#btnupdate').val(id);

            }
        });

        $('#modalupdateitem').modal('toggle');

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $('#btnimport').on('click', function(){

      $('#modalimportitem').modal('toggle');

    });

    $('#btnimporttemp').on('click', function(){

      window.open("{{ url('api/rawmaterial/downloadexceltemp') }}", '_blank');

    });

    $('#btnimportdata').on('click', function(){

        $.confirm({
            title: 'Import',
            content: 'Import this selected file?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){

                      ImportRMData();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                      
                    }
                } 
            }
        });

    });

    $('#btnpapercombination').on('click', function(){

      window.location = "{{ url('/papercombination') }}";

    });

    function ImportRMData(){

      var file = $('#file').val();

      if(file==""){

        toastr.error("Please select a file to import.", '', { positionClass: 'toast-top-center' });

      }
      else{

          var form = new FormData(); 
          form.append("file", $("#file")[0].files[0]);

          $.ajax({
              url: '{{ url("api/rawmaterial/importrawmaterials") }}',
              type: 'post',
              data: form,
              dataType: 'json',
              contentType: false,
              cache: false,
              processData: false,
              success: function(response){

                  if(response.success){

                      ReloadRawMaterials();
                      $('#modalimportitem .close').click();
                      toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                  }

               }
           });

      }

    }

    function LoadInformation(currency, data){

      $.ajax({
          url: '{{ url("api/rawmaterial/loadcurrency") }}',
          type: 'get',
          dataType: 'json',
          beforeSend: function(){
              H5_loading.show();
          },
          success: function(response){

            $('#'+currency).find('option').remove();
            $('#'+currency).append('<option value="" disabled selected>Select a Currency</option>');
            for (var i = 0; i < response.data.length; i++) {
                $('#'+currency).append('<option value="'+  response.data[i]["currencyid"] +'">'+  response.data[i]["currency"] +'</option>');
            }

            LoadFlute($('#txtuflute').attr('id'))
            LoadBoardPound(data.flute, $('#txtuboardpound').attr('id'));
            RawMaterialProfile(data);
            $('#cmbucategory').val(data.category).trigger('change');

          },
          complete: function(){

            H5_loading.hide();

          }
      });

    }

    $('#btnupdate').on('click', function(){

      var id = $(this).val();
      var name = $('#txtuname').val();
      var description = $('#txtudescription').val();
      var width = $('#txtuwidth').val();
      var widthsize = $('#txtuwidthsize').val();
      var length = $('#txtulength').val();
      var lengthsize = $('#txtulengthsize').val();
      var flute = $('#txtuflute').val();
      var boardpound = $('#txtuboardpound').val();
      var currencyid = $('#cmbucurrency').val();
      var price = $('#txtuprice').val();
      var category = $('#cmbucategory').val();

      if(name==""){

        toastr.error("Please input the name of the item.", '', { positionClass: 'toast-top-center' });
        $('#txtuname').focus();

      }
      else if(category==""){

        toastr.error("Please select a category.", '', { positionClass: 'toast-top-center' });
        $('#cmbucategory').focus();

      }
      else{

          $.ajax({
              url: '{{ url("api/rawmaterial/updateitem") }}',
              type: 'get',
              data: {
                id: id,
                name: name,
                description: description,
                width: width,
                widthsize: widthsize,
                length: length,
                lengthsize: lengthsize,
                flute: flute,
                boardpound: boardpound,
                currencyid: currencyid,
                price: price,
                category: category,
              },
              dataType: 'json',
              success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    $("#modalupdateitem .close").click();
                    ReloadRawMaterials();

                }

              }
          });

      }

    });

    function RawMaterialProfile(data){

      $('#txtuname').val(data.name);
      $('#txtudescription').val(data.description);
      $('#txtuwidth').val(data.width);
      $('#txtuwidthsize').val(data.widthsize);
      $('#txtulength').val(data.length);
      $('#txtulengthsize').val(data.lengthsize);
      $('#txtuflute').val(data.flute);
      $('#txtuboardpound').val(data.boardpound);
      $('#txtuprice').val(data.price);
      $('#cmbusupplier').val(data.sid).trigger('change');
      $('#cmbucurrency').val(data.currencyid).trigger('change');
      $('#cmbucategory').val(data.category).trigger('change');

      uname = data.name;
      udescription = data.description;
      uwidth = data.width;
      uwidthsize = data.widthsize;
      ulength = data.length;
      ulengthsize = data.lengthsize;
      uflute = data.flute;
      uboardpound = data.boardpound;

    }

    function LoadFlute(id){

      $.ajax({
        url: '{{ url("api/rawmaterial/loadflute") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          var data = [];
          for (var i = 0; i < response.data.length; i++) {
            data.push(response.data[i]["flute"]);
          }

          if(data.length!=0){
            $('#'+id).autocomplete({
              source: data
            });
          }

        }
      });

    }

    function LoadBoardPound(flute, id){
 
      $.ajax({
        url: '{{ url("api/rawmaterial/loadboardpound") }}',
        type: 'get',
        data: {flute: flute},
        dataType: 'json',
        success: function(response){

          var data = [];
          for (var i = 0; i < response.data.length; i++) {
            data.push(response.data[i]["boardpound"]);
          }

          if(data.length!=0){

            $('#'+id).autocomplete({
              source: data
            });

          }

        }
      });


    }

    function LoadCurrency(id){

      $.ajax({
          url: '{{ url("api/rawmaterial/loadcurrency") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            $('#'+id).find('option').remove();
            $('#'+id).append('<option value="" disabled selected>Select a Currency</option>');
            for (var i = 0; i < response.data.length; i++) {
                $('#'+id).append('<option value="'+  response.data[i]["currencyid"] +'">'+  response.data[i]["currency"] +'</option>');
            }

          }
      });

    }

    function ClearNewItem(){

      $('#txtnname').val('');
      $('#txtndescription').val('');
      $('#txtnwidth').val('');
      $('#txtnwidthsize').val('');
      $('#txtnlength').val('');
      $('#txtnlengthsize').val('');
      $('#txtnflute').val('');
      $('#txtnboardpound').val('');
      $('#txtnprice').val('');
      $('#cmbncategory').val('');

    }

    function ClearUpdateItem(){

      $('#txtuname').val('');
      $('#txtudescription').val('');
      $('#txtuwidth').val('');
      $('#txtuwidthsize').val('');
      $('#txtulength').val('');
      $('#txtulengthsize').val('');
      $('#txtuflute').val('');
      $('#txtuboardpound').val('');
      $('#txtuprice').val('');
      $('#cmbucategory').val('');

    }

    function LoadSelect2(){

      $('#cmbncategory').select2({
        theme: 'bootstrap'
      });

      $('#cmbucategory').select2({
        theme: 'bootstrap'
      });
  
      $('#cmbncurrency').select2({
        theme: 'bootstrap'
      });
            
       $('#cmbusupplier').select2({
        theme: 'bootstrap'
       });
            
      $('#cmbucurrency').select2({
        theme: 'bootstrap'
      });

      $('#cmbimportsupplier').select2({
        theme: 'bootstrap'
      });
      
    }

    function ClearData(){

      uname = "";
      udescription = "";
      uwidth = "";
      uwidthsize = "";
      ulength = "";
      ulengthsize = "";
      uflute = "";
      uboardpound = "";

    }

    function LoadRawMaterials(sort){

      tblrawmaterial.destroy();
      tblrawmaterial = $('#tblrawmaterial').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/rawmaterial/rawmaterialsinformation") }}',
            data: {
              sort: sort
            },
        },
        columns : [
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'size', name: 'size'},
            {data: 'flute', name: 'flute'},
            {data: 'boardpound', name: 'boardpound'},
            {data: 'currency', name: 'currency'},
            {data: 'price', name: 'price'},
            {data: 'category', name: 'category'},
            {data: 'panel', name: 'panel', width: '12%'},
        ]
      });

    }

    function ReloadRawMaterials(){

        tblrawmaterial.ajax.reload();

    }

  </script>  

@endsection