<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Purchasing_Model;
use App\Http\Controllers\SettingsController;

class POSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    //Variables
    public $poid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($poid)
    {
        
        $this->poid = $poid;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $companyinfo = SettingsController::GetCompanyInfo();
        $suppileremail = Purchasing_Model::GetSupplierEmail($this->poid);

        Mail::send("email.poemail", ['address'=>$companyinfo->address, 'number'=>$companyinfo->telnumber . ' / ' . $companyinfo->faxnumber], function($message) use($companyinfo, $suppileremail){

            $message->from('goodearth.corporation@gmail.com', 'Good Earth Packaging Corp.');
            $message->to($suppileremail, 'GEPC');
            $message->subject('Purchase Order');
            $message->attach(public_path("popdf/".$this->poid.".pdf"));

        });

    }
}
