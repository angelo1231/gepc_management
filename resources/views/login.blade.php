@extends('layout.index')

@section('body')

  <body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <img src="{{ asset("images/GEPC_LOGO.png") }}" height="170px">
      <br>
      <b>GOOD EARTH PACKAGING CORP. </b>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Enterprise Resource Planning System</p>
      <form id="frmlogin" name="frmlogin" action="{{ url('api/login/loginuser') }}" method="post">
        @csrf
        <div class="form-group has-feedback">
          <input id="txtusername" name="txtusername" type="text" class="form-control" placeholder="Username" autocomplete="off">
          <i class="fa fa-user form-control-feedback" style="font-size: 17px;"></i>
        </div>
        <div class="form-group has-feedback">
          <input id="txtpassword" name="txtpassword" type="password" class="form-control" placeholder="Password">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">

            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button id="btnlogin" name="btnlogin" type="button" class="btn btn-primary btn-block btn-flat">Login</button>
          </div>
          <!-- /.col -->
        </div>
      </form>  

    </div>

  </div>
  </body>

@endsection

@section('script')
<script>

  $(document).ready(function() {
     
      var msg = "{{ Session::get('message') }}";
      if(msg!=""){
        toastr.error(msg, '', { positionClass: 'toast-top-center' });
      }

      $('#txtusername').focus();

  });

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  $('#btnlogin').on('click', function() {
            
      Login();      

  });

  $('#txtpassword').bind('keypress', function(e) {

    if(e.keyCode==13){

        Login();

    }

  });

  function Login(){

      var username = $('#txtusername').val();
      var password = $('#txtpassword').val();

      if(username == ""){
        toastr.error('Please input your username', '', { positionClass: 'toast-top-center' });
      }
      else if(password == ""){
        toastr.error('Please input your password', '', { positionClass: 'toast-top-center' });
      }
      else{

          $('#frmlogin').submit();

      } 

  }

</script>
@endsection


