<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Good Earth Packaging Corp.</title>

        <link rel="stylesheet" href="{{ asset('css/print.css') }}">

    </head>

<body>


    <page size="A4">    

        <table style="width:100%; border: 2px solid black; border-collapse: collapse; font-size: 10px;">
            <tr>
                <td colspan="2">
                    <img src="{{ asset('images/GEPC_LOGO_RPT.jpg') }}" style="width: 200px;"/>
                </td>
                <td colspan="9" style="text-align: center; border: 2px solid black;">
                    <strong>JOB ORDER</strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black;">Customer:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->customer }}</td>
                <td colspan="3" style="border: 2px solid black;">JO NO.:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->jonumber }}</td>
            </tr>
            <tr>
            <tr>
                <td colspan="2" style="border: 2px solid black;">Product Code:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->partnumber }}</td>
                <td colspan="3" style="border: 2px solid black;">Date Issued:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->issueddate }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black;">Model/MPN:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->partname }}</td>
                <td colspan="3" style="border: 2px solid black;">PO NO.:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->ponumber }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black;">Product Description:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->description }}</td>
                <td colspan="3" style="border: 2px solid black;">PO Qty.:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->poqty }} PCS</td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black;">FG Size(Inches):</td>
                <td colspan="3" style="border: 2px solid black;">L{{ $poinformation[0]->OD_length }}MM X W{{ $poinformation[0]->OD_width }}MM X H{{ $poinformation[0]->OD_height }}MM OD</td>
                <td colspan="3" style="border: 2px solid black;">Delivery Date:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->deliverydate }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black;">FG Qty. (PCS):</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->stockonhand }} PCS</td>
                <td colspan="3" style="border: 2px solid black;">Packing STD:</td>
                <td colspan="3" style="border: 2px solid black;">{{ $poinformation[0]->packingstd }}</td>
            </tr>
            <tr>
                <td colspan="11" style="text-align: center; border: 2px solid black;"><strong>RM WAREHOUSE</strong></td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black; text-align: center;"><strong>Material</strong></td>
                <td colspan="1" style="border: 2px solid black; text-align: center;"><strong>Qty</strong></td>
                <td colspan="2" style="border: 2px solid black; text-align: center;"><strong>Issued By/Date</strong></td>
                <td colspan="2" style="border: 2px solid black; text-align: center;"><strong>Material Lot No.</strong></td>
                <td colspan="2" style="border: 2px solid black; text-align: center;"><strong>Time Received</strong></td>
                <td colspan="2" style="border: 2px solid black; text-align: center;"><strong>Received By/Date</strong></td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">Size (Inches)</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->rawmaterial }}</td>
                <td colspan="1" rowspan="7" style="border: 2px solid black; text-align: center;">{{ $poinformation[0]->qtyissued }} PCS</td>
                <td colspan="2" rowspan="7" style="border: 2px solid black; text-align: center;">{{ $poinformation[0]->issuedby }}</td>
                <td colspan="2" rowspan="7" style="border: 2px solid black; text-align: center;">DR# {{ $poinformation[0]->drnumber }}</td>
                <td colspan="2" rowspan="7" style="border: 2px solid black; text-align: center;"></td>
                <td colspan="2" rowspan="7" style="border: 2px solid black; text-align: center;"></td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">Flute</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->flute }}</td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">Board(LBS-TEST)</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->boardpound }}</td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">Style</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->style }}</td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">Ink Color</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->inkcolor }}</td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">RM On Stock</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->balance }} PCS</td>
            </tr>
            <tr>
                <td style="border: 2px solid black;">Target Output</td>
                <td style="border: 2px solid black;">{{ $poinformation[0]->targetoutput }} PCS</td>
            </tr>
            <tr>
                <td colspan="2" style="border: 2px solid black; text-align: center; text-decoration: underline;"><strong>Special Instruction</strong></td>
                <td colspan="9" style="border: 2px solid black;">{{ $poinformation[0]->specialinstruction }}</td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">
                    Prepared By: ____________________________________________
                </td>
                <td colspan="5" style="text-align: center;">
                    Checked By: __________________________________
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;">Planner</td>
                <td colspan="5" style="text-align: center;">Planning Staff/Head</td>
            </tr>
            <tr>
                <td colspan="11" style="text-align: center; border: 2px solid black;"><strong>PRODUCTION</strong></td>
            </tr>
            <tr>
                <td style="text-align: center; border: 2px solid black;" rowspan="2">No.</td>
                <td style="text-align: center; border: 2px solid black;" rowspan="2">Process</td>
                <td style="text-align: center; border: 2px solid black;" rowspan="2">M/C Code</td>
                <td style="text-align: center; border: 2px solid black;" rowspan="2">Operator</td>
                <td style="text-align: center; border: 2px solid black;" colspan="2" rowspan="1">Plan</td>
                <td style="text-align: center; border: 2px solid black;" rowspan="2">Remarks</td>
                <td style="text-align: center; border: 2px solid black;" colspan="2" rowspan="1">Actual</td>
                <td style="text-align: center; border: 2px solid black;" colspan="2" rowspan="1">Defects</td>
            </tr>
            <tr>
                <td style="text-align: center; border: 2px solid black;">Date:Time</td>
                <td style="text-align: center; border: 2px solid black;">Qty(pcs)</td>
                <td style="text-align: center; border: 2px solid black;">Date:Time</td>
                <td style="text-align: center; border: 2px solid black;">Qty(pcs)</td> 
                <td style="text-align: center; border: 2px solid black;">Qty(pcs)</td>
                <td style="text-align: center; border: 2px solid black;">Type</td>        
            </tr>
            {{-- Process Start Here --}}
            <?php $processcount = 1; ?>
            @foreach($fgprocess as $proc)

            <tr>
                <td style="text-align: center; border: 2px solid black; height: 60px;">{{ $processcount }}</td>
                <td style="text-align: center; border: 2px solid black;">{{ $proc->process }}</td>
                <td style="text-align: center; border: 2px solid black;"></td>
                <td style="text-align: center; border: 2px solid black;"></td>
                <td style="text-align: center; border: 2px solid black;">{{ $poinformation[0]->issueddate }}</td>
                <td style="text-align: center; border: 2px solid black;">{{ $poinformation[0]->targetoutput }} PCS</td>
                <td style="text-align: center; border: 2px solid black;"></td>
                <td style="text-align: center; border: 2px solid black;"><?php echo $proc->created_at; ?></td>
                <td style="text-align: center; border: 2px solid black;"><?php echo $proc->qty; ?></td>
                <td style="text-align: center; border: 2px solid black;"><?php echo $proc->rejectqty; ?></td>
                <td style="text-align: center; border: 2px solid black;"><?php echo $proc->rejecttype; ?></td>
            </tr>
            <?php $processcount += 1; ?>
            @endforeach
            {{-- Process End Here --}}
            <tr>
                <td style="text-align: center; border: 2px solid black;">{{ $processcount }}</td>
                <td style="text-align: center; border: 2px solid black;">Production</td>
                <td style="text-align: center; border: 2px solid black;">Inspected By</td>
                <td style="text-align: center; border: 2px solid black;" colspan="3"></td>
                <td style="text-align: center; border: 2px solid black;">Date</td>
                <td style="text-align: center; border: 2px solid black;" colspan="4"></td>
            </tr>
            <?php $processcount++; ?>
            <tr>
                <td style="text-align: center; border: 2px solid black;">{{ $processcount }}</td>
                <td style="text-align: center; border: 2px solid black;">FG Warehouse</td>
                <td style="text-align: center; border: 2px solid black;">Received By</td>
                <td style="text-align: center; border: 2px solid black;" colspan="3"></td>
                <td style="text-align: center; border: 2px solid black;">Date</td>
                <td style="text-align: center; border: 2px solid black;" colspan="4"></td>
            </tr>
        </table>
        <p style="float: right; margin-top: -1px; font-size: 10px;">FO-PROD-02-02</p>    
        
    </page>


</body>
</html>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

  
