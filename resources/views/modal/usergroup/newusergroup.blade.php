<!-- Middle Modal -->
<style>
    .modal {
      text-align: center;
      padding: 0!important;
    }
    
    .modal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }
    
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
</style>  

<!-- Modal -->
<div class="modal fade" id="newusergroup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <strong>NEW USER GROUP</strong>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        
                <div class="form-group">
                    <label for="txtnusergroup">User Group</label>
                    <input id="txtnusergroup" name="txtnusergroup" type="text" class="form-control" placeholder="User Group">
                </div>

        </div>
        <div class="modal-footer">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">
                <button id="btnnsaveusergroup" name="btnnsaveusergroup" type="button" class="btn btn-primary btn-block btn-flat">Save</button>
            </div>
        </div>
    </div>
</div>
</div>