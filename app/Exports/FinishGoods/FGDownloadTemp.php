<?php

namespace App\Exports\FinishGoods;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FGDownloadTemp implements WithHeadings
{
    
    use Exportable;

    public function headings(): array
    {
        return [
            'itemcode',
            'partnumber',
            'partname',
            'description',
            'ID_length',
            'ID_width',
            'ID_height',
            'OD_length',
            'OD_width',
            'OD_height',
            'currency',
            'price',
            'packingstd',
            'processcode'
        ];
    }


}
