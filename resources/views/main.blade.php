@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Home</b></h3>
        </div>
        <div class="box-body">

          <div class="col-md-12">

            @if(in_array(65, $access))

              <div class="col-md-6">
                  <h5><strong>Purchase Order For Approval</strong></h5>
                  <table id="tblpo" class="table">
                      <thead>
                          <tr>
                              <th>PO#</th>
                              <th>Created By</th>
                              <th>Created Date</th>
                              <th></th>
                          </tr>
                      </thead>
                  </table>
              </div>

            @endif

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

        //Variables
        var tblpo;

        $(document).ready(function() {
     
            var msg = "{{ Session::get('message') }}";
            if(msg!=""){
              toastr.success(msg, '', { positionClass: 'toast-top-center' });
            }

            //Load
            @if(in_array(65, $access))
              LoadPOForApproval();
            @endif

        });

        function LoadPOForApproval(){

          tblpo = $('#tblpo').DataTable({
              processing: true,
              serverSide: true,
              autoWidth: false,
              ordering: false,
              ajax: {
                  type: 'get',
                  url: '{{ url("api/main/loadpoforapproval") }}'
              },
              columns : [
                  {data: 'PO', name: 'PO'},
                  {data: 'createdby', name: 'createdby'},
                  {data: 'createddate', name: 'createddate'}
              ]
          });

        }

  </script>  

@endsection