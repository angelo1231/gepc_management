@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> User Group Information</b></h3>
        </div>
        <div class="box-body">
            <div class="col-md-9">
            </div>
            <div class="col-md-3" style="margin-bottom: 15px;">
                
                <button id="btnnewusergroup" name="btnnewusergroup" class="btn btn-success btn-flat" style="float: right;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New User Group</strong></button>

            </div>      
            <div class="col-md-12">
            <table id="tblusergroup" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User Group</th>
                        <th>Access</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')
  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

  {{-- Modals --}}
  @include('modal.usergroup.newusergroup')
  @include('modal.usergroup.addaccess')

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblusergroup;
    var susergroupid;

    $(document).ready(function(){

        //Load
        LoadUserGroupInformation();

    });

    $('#btnnewusergroup').on('click', function(){

        //Clear
        ClearNewUserGroup();

        //Toggle Modal
        $('#newusergroup').modal('toggle');

    });

    $('#btnnsaveusergroup').on('click', function(){

        SaveUserGroup();

    });

    $(document).on('click', '#btnaddaccess', function(){

        susergroupid = $(this).val();
        LoadAccess('cmbuseraccess', susergroupid);
        $('#addaccess').modal('toggle');
        
    });

    $('#chkselectall').on('click', function(){

        if($(this).is(":checked")){
            
            SelectAll('cmbuseraccess');

        }
        else if($(this).is(":not(:checked)")){
           
            DeSelectAll('cmbuseraccess');

        }

    });

    $('#btnnsaveaccess').on('click', function(){

        var access = $('#cmbuseraccess').val();
        SaveGroupAccess(access);

    });

    function SaveGroupAccess(access){

        $.ajax({
            url: '{{ url("api/usergroup/saveusergroupaccess") }}',
            type: 'post',
            data: {
                access: access,
                ugroupid: susergroupid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });  
                    $('#addaccess .close').click();
                    ReloadUserGroupInformation();                    

                }

            }
        });

    }

    function SelectAll(id){

        $('#'+id+' option').prop('selected', true);  
        $('#'+id).trigger('chosen:updated');

    }

    function DeSelectAll(id){

        $('#'+id+' option:selected').prop('selected', false);
        $('#'+id).trigger('chosen:updated');

    }

    function LoadAccess(id, usergroupid){

        $.ajax({
            url: '{{ url("api/usergroup/loadaccess") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                //Clear Selected
                $('#'+id+' option').prop('selected', false).trigger('chosen:updated');
                $('#'+id).find('option').remove();
                for(var i=0;i<response.data.length;i++){
                    $('#'+id).append('<option value="'+ response.data[i]["accessid"] +'">'+ response.data[i]["access"] +'</option>');
                }

                $('#'+id).chosen({
                    allow_single_deselect: true,
                    width: '100%'
                });

                SetUserGroupAccess(id);

            }
        });

    }

    function SetUserGroupAccess(id){

        $.ajax({
            url: '{{ url("api/usergroup/getusergroupaccess") }}',
            type: 'get',
            data: {
                ugroupid: susergroupid
            },
            dataType: 'json',
            success: function(response){

                $("#"+id).val(response.access).trigger("chosen:updated");

            }
        });

    }

    function SaveUserGroup(){

        var usergroup = $('#txtnusergroup').val();

        if(usergroup==""){

            toastr.error('Please input the user group name.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/usergroup/saveusergroup") }}',
                type: 'post',
                data: {
                    usergroup: usergroup
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newusergroup .close').click();
                        ReloadUserGroupInformation();

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                        $('#txtnusergroup').focus();

                    }

                }
            });

        }

    }

    function LoadUserGroupInformation(){

        tblusergroup = $('#tblusergroup').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/usergroup/loadusergroupinformation") }}'
          },
          columns : [
              {data: 'id', name: 'id'},
              {data: 'usergroup', name: 'usergroup'},
              {data: 'access', name: 'access'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
        });

    }

    function ReloadUserGroupInformation(){

        tblusergroup.ajax.reload();

    }

    function ClearNewUserGroup(){

        $('#txtnusergroup').val('');

    }

  </script>  

@endsection