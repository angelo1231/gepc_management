<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Session;
use App\Http\Controllers\UserController;
use App\Main_Model;
use DataTables;
use Illuminate\Support\Collection;

class MainController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        $access = UserController::UserGroupAccess();

        return view('main')
        ->with('access', $access);

    }

    function LoadPOForApproval(){

        $poforapproval = Main_Model::LoadPOForApproval();

        $data = array();
        for($i=0;$i<count($poforapproval);$i++){
            
            $obj = new \stdClass;

            $obj->PO = '<a href="'. url("/purchasing/purchaseorderinfo/info/". $poforapproval[$i]->poid) .'">'.$poforapproval[$i]->ponumber.'</a>';
            $obj->createdby = $poforapproval[$i]->issuedby;
            $obj->createddate = $poforapproval[$i]->created_at;

            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns(['PO'])
        ->make(true);

    }

}  