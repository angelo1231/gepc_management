<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Poinformationitemsaudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poinformationitemsaudit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('itemid');
            $table->string('drnumber', 60);
            $table->integer('deliverdqty');
            $table->integer('rejectqty');
            $table->integer('goodqty');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poinformationitemsaudit');        
    }
}
