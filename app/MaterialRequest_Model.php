<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class MaterialRequest_Model extends Model
{
    
    public static function GenMRINumber($tableschema){

        $result = DB::select(DB::raw("
            SELECT CONCAT('MR', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) AS 'batch' FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'materialrequest'
        "));

        return $result[0]->batch;

    }

    public static function Padding($batch){

        $result = DB::select(DB::raw('SELECT LPAD('.$batch.', 4, "0") AS pad'));
        
        return $result[0]->pad;

    }


    public static function LoadMaterialRequestInformation($cid){

        if($cid=="All"){

            $result = DB::table('materialrequest')
            ->select(
               'materialrequest.requestid',
               'materialrequest.mrinumber',
               'materialrequest.issuancenumber',
               DB::raw("description AS 'finishgood'"),
               'materialrequest.issuedby',
               'materialrequest.reason',
               'customers.customer',
               'materialrequest.status'
            )
            ->join('finishgoods', 'finishgoods.fgid', '=', 'materialrequest.fgid')
            ->join('customers', 'customers.cid', '=', 'materialrequest.cid')
            ->orderBy('materialrequest.requestid', 'DESC')
            ->get();

            return $result;

        }
        else{

            $result = DB::table('materialrequest')
            ->select(
               'materialrequest.requestid',
               'materialrequest.mrinumber',
               'materialrequest.issuancenumber',
               DB::raw("description AS 'finishgood'"),
               'materialrequest.issuedby',
               'materialrequest.reason',
               'customers.customer',
               'materialrequest.status'
            )
            ->join('finishgoods', 'finishgoods.fgid', '=', 'materialrequest.fgid')
            ->join('customers', 'customers.cid', '=', 'materialrequest.cid')
            ->where('materialrequest.cid', '=', $cid)
            ->orderBy('materialrequest.requestid', 'DESC')
            ->get();

            return $result;

        }



    }

    public static function LoadMaterialRequestInformationINV(){

        // IF(rawmaterials.category!='Raw Material', CONCAT(rawmaterials.name, ' ', rawmaterials.description), CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound)) AS rawmaterial,
        $result = DB::select(
            DB::raw("
            SELECT
            materialrequest.requestid,
            finishgoods.description AS finishgood,
            materialrequest.issuedby,
            materialrequest.reason,
            customers.customer,
            materialrequest.status
            FROM materialrequest
            LEFT JOIN finishgoods ON finishgoods.fgid=materialrequest.fgid
            LEFT JOIN customers ON customers.cid=materialrequest.cid
            WHERE status='Pending'
            ")
        );

        return $result;

    }

    public static function GetMRI($id){

        $result = DB::table('materialrequest')
        ->select(
            'mrinumber', 
            DB::raw("IFNULL(issuancenumber, 'None') AS 'issuancenumber'")
        )
        ->where('requestid', '=', $id)
        ->get();

        return $result;

    }

    public static function GetMRIItem($id){

        $result = DB::table('materialrequestitems')
        ->select(
            'materialrequestitems.id',
            'materialrequestitems.rmid',
            DB::raw("IF(rawmaterials.category='Others', CONCAT(rawmaterials.name, ' ', rawmaterials.description), CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound)) AS 'rawmaterial'"),
            'materialrequestitems.qtyrequired',
            'materialrequestitems.remarks',
            DB::raw("
                (SELECT
                GROUP_CONCAT(IF(category!='Excess Board', CONCAT(name, ' ', description), CONCAT(IF(widthsize!='', CONCAT(width,'.',widthsize), width), ' x ', IF(lengthsize!='', CONCAT(length, '.', lengthsize), length), ' ', flute, ' ', boardpound)) SEPARATOR '<br>')
                FROM materialrequestitemsexcess
                INNER JOIN rawmaterials ON rawmaterials.rmid=materialrequestitemsexcess.excessrmid
                WHERE materialrequestitemsexcess.mritemid=materialrequestitems.id               
                GROUP BY materialrequestitemsexcess.mritemid) AS 'excessboard'
            "),
            DB::raw("
                (SELECT
                GROUP_CONCAT(materialrequestitemsexcess.qtyexcessboard SEPARATOR '<br>')
                FROM materialrequestitemsexcess
                INNER JOIN rawmaterials ON rawmaterials.rmid=materialrequestitemsexcess.excessrmid
                WHERE materialrequestitemsexcess.mritemid=materialrequestitems.id
                GROUP BY materialrequestitemsexcess.mritemid) AS 'qtyexcessboard'
            "),
            DB::raw("IFNULL(materialrequestitems.qtyissued, 0) AS 'qtyissued'"),
            DB::raw("(materialrequestitems.qtyrequired - IFNULL(materialrequestitems.qtyissued, 0)) AS 'balance'"),
            DB::raw("IFNULL((SELECT rmissuedby FROM materialrequest WHERE requestid=materialrequestitems.mrid), 'None') AS 'rmissuedby'")
        )
        ->where('materialrequestitems.mrid', '=', $id)
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'materialrequestitems.rmid')
        ->get();

        return $result;

    }

    public static function GetRMID($id){

        $result = DB::table('materialrequest')
        ->select('rmid')
        ->where('requestid', '=', $id)
        ->get();

        return $result[0]->rmid;

    }

    public static function GetRawMaterialBalance($rmid){

        $result = DB::table('rawmaterials')
        ->select(DB::raw("SUM(stockonhand) AS balance"))
        ->whereIn('rmid', $rmid)
        ->get();

        return $result[0]->balance;

    }

    public static function ApproveMRIInformation($itemid, $drnumber, $qtyissued, $stockonhand){

        DB::table('materialrequestitems')
        ->where('id', '=', $itemid)
        ->update([
            "drnumber"=>$drnumber,
            "qtyissued"=>$qtyissued,
            "balance"=>$stockonhand,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetRMStockOnHand($itemid, $qtyissued){

        $result = DB::table('rawmaterials')
        ->select(
            DB::raw("(stockonhand - ". $qtyissued .") AS 'stockonhand'")
        )
        ->whereRaw("rmid = (SELECT rmid FROM materialrequestitems WHERE id=". $itemid .")")
        ->get();

        return $result[0]->stockonhand;

    }

    public static function UpdateRMStocks($itemid, $stockonhand){

        DB::table('rawmaterials')
        ->whereRaw("rmid = (SELECT rmid FROM materialrequestitems WHERE id=". $itemid .")")
        ->update([
            "stockonhand"=>$stockonhand,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetMRINumber($id){

        $result = DB::table('materialrequest')
        ->select(
            'mrinumber'
        )
        ->where('requestid', '=', $id)
        ->first();

        return $result->mrinumber;

    }

    public static function GetCategory($id){

        $result = DB::table('rawmaterials')
        ->select(
            'category'
        )
        ->whereRaw("rmid IN ((SELECT rmid FROM materialrequestitems WHERE mrid=".$id."))")
        ->get();


        foreach($result as $val){

            if($val->category=="Raw Material"){
                $cat = "Raw Material";
                break;
            }
            else if($val->category=="Excess Board"){
                $cat = "Excess Board";
                break;
            }
            else{
                $cat = "Other";
            }

        }

        return $cat;

    }

    public static function GenIssuanceNumber(){

        $result = DB::select(
            DB::raw("
                SELECT CONCAT('ISN', DATE_FORMAT(NOW(), '%Y%m%d%h%i')) AS 'issuancenumber'; 
            ")
        );

        return $result[0]->issuancenumber;

    }

    public static function GetRMBalance($rmid){

        $result = DB::table('rawmaterials')
        ->select(
            'stockonhand'
        )
        ->where('rmid', '=', $rmid)
        ->first();

        return $result->stockonhand;

    }

    public static function UpdateMRIInformationINV($id, $issuancenumber, $issuedby, $status){

        DB::table('materialrequest')
        ->where('requestid', '=', $id)
        ->update([
            "issuancenumber"=>$issuancenumber,
            "rmissuedby"=>$issuedby,
            "status"=>$status,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DisapproveMR($id){

        DB::table('materialrequest')
        ->where('requestid', '=', $id)
        ->update([
            "status"=>'Disapproved',
            "updated_at"=>DB::raw("NOW()")
        ]);        

    }


}
