<!-- Middle Modal -->
<style>
.modal {
   text-align: center;
   padding: 0!important;
}
    
.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}
    
.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.ui-autocomplete  {
    z-index: 1100 !important;
}

</style>   

<!-- Modal -->
<div class="modal fade" id="rmupdatestocks"  role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  <label id="lblinformationstock">{ DATA HERE }</label> 
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  
                <div class="modal-body">

                    <div class="container-fluid">

                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="txtuqtystock">Quantity</label>
                                <input type="number" id="txtuqtystock" name="txtuqtystock" class="form-control" placeholder="Quantity">
                            </div>
                            <div class="form-group">
                                <label for="txturemarksstock">Remarks</label>
                                <textarea name="txturemarksstock" id="txturemarksstock" cols="30" rows="5" class="form-control"></textarea>
                            </div>

                        </div>

                    </div>
          
                </div>
                
                <div class="modal-footer">

                    <button id="btnsaveupdatestocks" name="btnsaveupdatestocks" class="btn btn-success">Save</button>
                    <button data-dismiss="modal" class="btn btn-danger">Close</button>

                </div>

            </div>
        </div>
       
</div>
    