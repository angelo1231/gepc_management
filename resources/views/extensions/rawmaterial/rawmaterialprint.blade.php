<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Good Earth Packaging Corp.</title>

        <link rel="stylesheet" href="{{ asset('css/print.css') }}">

    </head>

<body>


    <page size="A4">    
        <table style="width:100%; border: 2px solid black;">
            <tr>
                <td colspan="7" style="border: 2px solid black;"><h2 style="text-align: center;">Inventory Report</h2></td>
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;">Size</td>
                <td style="border: 2px solid black; text-align: center;">Description</td>
                <td style="border: 2px solid black; text-align: center;">Flute</td>
                <td style="border: 2px solid black; text-align: center;">Board Pounds</td>
                <td style="border: 2px solid black; text-align: center;">Currency</td>
                <td style="border: 2px solid black; text-align: center;">Price</td>
                <td style="border: 2px solid black; text-align: center;">Stocks</td>
            </tr>
            @foreach($rmdata as $data)
            <?php

                    $size = "";
                    if($data->widthsize!=""){
                        $size = $size . $data->width . "." . $data->widthsize;
                    }
                    else{
                        $size = $size . $data->width;
                    }

                    if($data->lengthsize!=""){
                        $size = $size . " x " . $data->length . "." . $data->lengthsize;
                    }
                    else{
                        $size = $size . " x " . $data->length;
                    }

            ?>
            <tr>
                <td style="border: 2px solid black; text-align: center;">{{ $size }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->description }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->flute }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->boardpound }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->currency }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->price }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->stockonhand }}</td>
            </tr>
            @endforeach
        </table>
    </page>


</body>
</html>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

  
