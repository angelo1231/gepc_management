<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="finishgoodproduction" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label id="lblpinformation">{ DATA HERE }</label> 
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
              <div class="container-fluid">

                <div class="col-md-12">

                  <div class="col-md-12 row">
                    
                    <div class="form-group">
                      <label for="cmbpjonumber">Job Order Number</label>
                      <select name="cmbpjonumber" id="cmbpjonumber" class="form-control"></select>
                    </div>

                    <div class="form-group">
                      <label for="txtpqty">Quantity</label>
                      <input id="txtpqty" name="txtpqty" type="number" class="form-control">
                    </div>

                  </div>

                </div>

              </div>
                    
            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnpsave" name="btnpsave" type="button" class="btn btn-primary btn-flat">Save Information</button>
                </div>
            </div>
        </div>
    </div>
</div>