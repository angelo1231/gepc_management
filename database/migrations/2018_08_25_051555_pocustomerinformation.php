<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pocustomerinformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pocustomerinformation', function (Blueprint $table) {
            $table->increments('poid');
            $table->string('ponumber', '60')->unique();
            $table->integer('cid');
            $table->decimal('grandtotal', 10, 2);
            $table->longtext('remarks')->nullable();
            $table->date('deliverydate');
            $table->text('issuedby');
            $table->text('status');
            $table->integer('is_openpo')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pocustomerinformation');
    }
}
