<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use App\Planning_Model;
use DataTables;
use Illuminate\Support\Collection;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\UserController;
use Illuminate\Support\Str;

class PlanningController extends Controller
{

    private $tableschema;
    private $generatelength = 9;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }
    
    function index($view=null, $action=null, $id=null){
        
        $access = UserController::UserGroupAccess();

        if($view==null){

            return view('planning')
            ->with('access', $access);

        }
        else{

            if($action!=null && $id!=null){

                if($action=="info"){

                    $poinformation = Planning_Model::CustomerPOProfile($id);
                    $poinformationitems = Planning_Model::CustomerPOItemsProfile($id);
    
                    return view('extensions.planning.'.$view)
                    ->with('poinformation', $poinformation)
                    ->with('poinformationitems', $poinformationitems)
                    ->with('access', $access);

                }
                else if($action=="print"){

                    $poinformation = Planning_Model::LoadCustomerPOInformation($id);
                    // $fgprocess = Planning_Model::GetFGProcess($poinformation[0]->processcode);
                    $fgprocess = Planning_Model::GetFGProcess($id);

                    return view('extensions.planning.'.$view)
                    ->with('poinformation', $poinformation)
                    ->with('fgprocess', $fgprocess);

                }
                


            }
            else{

                return view('extensions.planning.'.$view)
                ->with('access', $access);

            }

        }

        
    }

    function LoadCustomers(){

        $content = "";
        $customers = Planning_Model::LoadCustomers();

        $content .= '
           <option value="" disabled selected>Select a Customer</option>
           <option value="All">All</option>
        ';
        foreach($customers as $val){

            $content .= '<option value="'. $val->cid .'">'. $val->customer .'</option>';

        }


        return json_encode([
            "content"=>$content
        ]);

    }

    function LoadCustomerItems(Request $request){

        $customer_items = Planning_Model::LoadCustomerItems($request->cid);
        
        return json_encode([
            "data"=>$customer_items
        ]);

    }

    function LoadRawMaterials(){

        $rawmaterials = Planning_Model::LoadRawMaterials();

        return json_encode([
            "data"=>$rawmaterials
        ]);

    }

    function SaveRequestMaterial(Request $request){

        $cid = "";
        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        //Validation
        for($v=0;$v<count($request->rmid);$v++){

            for($l=0;$l<count($request->excessboard[$v]);$l++){

                $valexcess = $request->excessboard[$v][$l];
                $valqtyexcess = $request->qtyexcessboard[$v][$l];

                if($valexcess["value"]!=null){

                    if($valqtyexcess["value"]==null){

                        return json_encode([
                            "success"=>false,
                            "message"=>"Excess board must be equal to the quantity excess board."
                        ]);

                    }
                    else{
   
                        if(count($request->excessboard[$v])!=count($request->qtyexcessboard[$v])){

                            return json_encode([
                                "success"=>false,
                                "message"=>"Excess board must be equal to the quantity excess board."
                            ]);
        
                        }                    

                    }



                }

            }

        }

        //Process
        if($request->cid=="All"){

            $cid = Planning_Model::GetFGIDCustomer($request->fgid);

        }
        else{
            $cid = $request->cid;
        }

        $mrid = Planning_Model::SaveRequestMaterial($request->mrinumber, $cid, $request->fgid, $request->reason, $issuedby, $request->prid);

        for($i=0;$i<count($request->rmid);$i++){

            $mritemid = Planning_Model::SaveRequestMaterialItem($mrid, $request->rmid[$i]["value"], $request->reqqty[$i]["value"], $request->remarks[$i]["value"]);

            //Excess Board
            for($y=0;$y<count($request->excessboard[$i]);$y++){

                $dataexcess = $request->excessboard[$i][$y];
                $dataqtyexcess = $request->qtyexcessboard[$i][$y];

                if($dataexcess["value"]!=null){
                    
                    if(Str::contains($dataexcess["value"], ['db'])){

                        $rmid = explode('-', $dataexcess["value"]);
                        Planning_Model::SaveExcessBoardItem($mritemid, $rmid[1], $dataqtyexcess["value"]);

                    }
                    else{

                        //Validation
                        $validation = Planning_Model::ValidateExcessBoard($dataexcess["value"]);

                        if($validation){
                            
                            $excestingrmid = Planning_Model::GetExcessBoardRMID($dataexcess["value"]);

                            //Save Item Excess Board
                            Planning_Model::SaveExcessBoardItem($mritemid, $excestingrmid, $dataqtyexcess["value"]);

                        }
                        else{

                            $stripped = str_replace(' ', '', $dataexcess["value"]);
                            $newrmdata = explode('x', $stripped);
    
                            //Width Length
                            $width = "";
                            $widthsize = "";
                            $length = "";
                            $lengthsize = "";
                            $description = $dataexcess["value"];
                            if(Str::contains($newrmdata[0], ['.'])){
    
                                $wsizedata = explode('.', $newrmdata[0]);
    
                                $width = $wsizedata[0];
                                $widthsize = $wsizedata[1];
    
                            }
                            else{
    
                                $width = $newrmdata[0];
    
                            }
    
                            if(Str::contains($newrmdata[1], ['.'])){
    
                                $lsizedata = explode('.', $newrmdata[1]);
    
                                $length = $lsizedata[0];
                                $lengthsize = $lsizedata[1];
    
                            }
                            else{
    
                                $length = $newrmdata[1];                            
    
                            }
    
                            //Save New Excess Board
                            $newrmid = Planning_Model::SaveNewExccessBoard($request->rmid[$i]["value"], $width, $widthsize, $length, $lengthsize, $description);
    
                            //Save Item Excess Board
                            Planning_Model::SaveExcessBoardItem($mritemid, $newrmid, $dataqtyexcess["value"]);

                        }

                    }


                }

            }

        }

        return json_encode([
            "success"=>true,
            "message"=>"Material Request has been save."
        ]);

    }

    function LoadCustomerPO(Request $request){

        $po = Planning_Model::LoadCustomerPO($request->customer);
        
        $data = array();
        for($i=0;$i<count($po);$i++){
            
            $obj = new \stdClass;

            $obj->ponumber = $po[$i]->ponumber;
            $obj->itemdescription = $po[$i]->description;
            $obj->qty = $po[$i]->qty;
            $obj->sonumber = $po[$i]->salesnumber;
            $obj->deliveryqty = $po[$i]->deliveryqty;
            $obj->deliverydate = $po[$i]->deliverydate;
            $obj->customer = $po[$i]->customer;
            if($po[$i]->salesnumber!="None"){
                // $obj->panel = '<a href="'. url("/planning/cuspurchaseorderinfo/info/". $po[$i]->poid) .'"><button id="btninfo" name="btninfo" class="btn btn-flat btn-info"><i class="fa fa-info"></i></button></a>';
                $obj->panel = '
                    <button id="btnprrequest" name="btnprrequest" class="btn btn-flat btn-info" title="Request Material" value="'. $po[$i]->itemid .'"><i class="fa fa-cog" aria-hidden="true"></i></button>
                    <button id="btndelso" name="btndelso" class="btn btn-flat btn-danger" title="Delete Sales Order" value="'. $po[$i]->itemid .'"><i class="fa fa-trash" aria-hidden="true"></i></button>
                ';
            }
            else{
                $obj->panel = '';
            }

            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns([
            'sonumber',
            'deliveryqty',
            'deliverydate',
            'panel'
        ])
        ->make(true);

    }

    function GenJONumber(){

        $batch = Planning_Model::GenJONumber($this->tableschema);

        return json_encode([
            "batch"=>$batch
        ]);

    }

    function LoadMaterialRequestInfo(Request $request){

        $content = "";
        $materialrequestinfo = Planning_Model::LoadMaterialRequestInfo($request->customer);

        $content .= '
            <option value="" disabled selected>Select a Material Request Number</option>
        ';
        foreach($materialrequestinfo as $val){

            $content .= '
                <option value="'. $val->requestid .'">'. $val->mrinumber .'</option>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function CreateJobOrder(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
        $jorequestnumber = "JOR" . $this->GenerateJORequestNumber($this->generatelength);

        //Get PRID Request
        $prid = Planning_Model::GetMaterialRequestPRID($request->materialrequest);

        //Get Request Data
        $requestdata = Planning_Model::LoadMaterialRequestData($prid, $request->materialrequest);

        //Save JO Information
        $joid = Planning_Model::CreateJobOrder($request->materialrequest, $request->targetoutput, $jorequestnumber, $issuedby, $requestdata->poid, $requestdata->itemid, $requestdata->cid);
        $rmiddata = Planning_Model::GetRMMaterialRequest($request->materialrequest);

        foreach($rmiddata as $val){

            //Get RM Stock On Hand
            $stockonhand = Planning_Model::GetRMStock($val->rmid);

            //Save JO RM Information
            Planning_Model::SaveJobOrderRMInfo($joid, $val->rmid, $stockonhand);

        }

        //Update Material Request
        Planning_Model::UpdateMaterialRequest($request->materialrequest);

        return json_encode([
            "success"=>true,
            "message"=>"Job order information has been created.",
            "jorequestnumber"=>$jorequestnumber
        ]);


    }

    function LoadJobOrderInformation(Request $request){

        $joinformation = Planning_Model::LoadJobOrderInformation($request->customer);

        $data = array();
        for($i=0;$i<count($joinformation);$i++){
            
            $obj = new \stdClass;

            if($joinformation[$i]->status=="Close"){
                $obj->status = '<i class="fa fa-check"></i>';
            }
            else{
                $obj->status = '';
            }

            $obj->jorequestnumber = $joinformation[$i]->jorequestnumber;
            $obj->jonumber = $joinformation[$i]->jonumber;
            $obj->ponumber = $joinformation[$i]->ponumber;
            $obj->finishgood = $joinformation[$i]->finishgood;
            $obj->rawmaterial = $joinformation[$i]->rawmaterial;
            $obj->targetoutput = $joinformation[$i]->targetoutput;    
            // $obj->deliverydate = $joinformation[$i]->deliverydate;
            $obj->requestdate = $joinformation[$i]->requestdate;
            $obj->requestby = $joinformation[$i]->requestby;
            $obj->qtyreceive = $joinformation[$i]->qtyreceive;
            $obj->customer = $joinformation[$i]->customer;
            // $obj->panel ='<button id="btninfo" name="btninfo" class="btn btn-flat btn-info"><i class="fa fa-info"></i></button> <button id="btnprintjo" name="btnprintjo" class="btn btn-flat btn-primary"><i class="fa fa-print"></i></button>';
            if($joinformation[$i]->status=="For Request"){
                $obj->panel ='';
            }
            else{
                $obj->panel ='<a href="'. url("/planning/joborderprint/print/". $joinformation[$i]->joid) .'" target="_blank" id="btnprintjo" class="btn btn-flat btn-primary"><i class="fa fa-print"></i></a>';
            }

            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns([
            'status', 
            'panel',
            'rawmaterial'
        ])
        ->make(true);

    }

    function GetDeliveryQtySalesOrder(Request $request){

        $targetoutput = 0;
        $delid = array();

        for($i=0;$i<count($request->delid);$i++){

            $delid[] = $request->delid[$i]["value"];

        }

        $deliveryqty = Planning_Model::GetDeliveryQtySalesOrder($delid);

        foreach($deliveryqty as $val){

            $targetoutput += $val->deliveryqty;

        }

        return json_encode([
            "targetoutput"=>$targetoutput
        ]);

    }

    function LoadItemSONumber(Request $request){

        $salesorderinfo = Planning_Model::LoadItemSONumber($request->itemid);

        return json_encode([
            "salesorderinfo"=>$salesorderinfo
        ]);

    }

    function LoadQuantityReqSO(Request $request){

        $delid = array();

        for($i=0;$i<count($request->sodata);$i++){

            $delid[] = $request->sodata[$i]["value"];

        }

        $sodelqty = Planning_Model::LoadQuantityReqSO($delid);

        return json_encode([
            "sodelqty"=>$sodelqty
        ]);

    }

    function SavePurchaseRequest(Request $request){

        //Validation For Raw Material Data
        $hasnoraw = Planning_Model::ValidateRawMaterialDataPR($request->prid);

        if($hasnoraw){

            return json_encode([
                "success"=>false,
                "message"=>"Please add a raw material to purchase."
            ]);

        }
        else{

            //Validate Delivery Info For Every Items
            $items = Planning_Model::GetPurchaseRequestItems($request->prid);

            foreach($items as $val){

                $hasnodelivery = Planning_Model::ValidatePurchaseRequestItemDelivery($val->itemid);
                if($hasnodelivery){

                    return json_encode([
                        "success"=>false,
                        "message"=>"Please add a raw material to delivery information."
                    ]);
        
                }

            }

            //Save Purchase Order Information
            $delid = [];

            if(isset($request->sodata)){
    
                for($i=0;$i<count($request->sodata);$i++){
    
                    $delid[] = $request->sodata[$i]["value"];
        
                }
    
            }
            else{
    
                return json_encode([
                    "success"=>false,
                    "message"=>"Please check a sales order information."
                ]);
    
            }
    
            $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
    
            //Update Purchase Request
            Planning_Model::SavePR($request);
    
            //Update Sales Order Number (Close)
            if(count($delid)!=0){
                Planning_Model::UpdateSO($request->prid, $delid);
            }
    
            return json_encode([
                "success"=>true,
                "message"=>"Purchase request has been save."
            ]);

        }

    }

    function GenerateJORequestNumber($length){

        $result = '';

        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;

    }

    function LoadSOItemInformation(Request $request){

        $content = "";
        $soiteminfo = Planning_Model::LoadSOItemInformation($request->itemid);

        foreach($soiteminfo as $val){

            $content .= '
            <tr> <td><div class="checkbox"><label><input id="chkddelid" name="chkddelid[]" type="checkbox" value="'. $val->delid .'"></label></div></td> <td style="vertical-align: middle;">'. $val->salesnumber .'</td> <td style="vertical-align: middle;">'. $val->deliveryqty .'</td> <td style="vertical-align: middle;">'. $val->deliverydate .'</td> </tr>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function DeleteSOInformation(Request $request){

        $userid = Auth::user()->id;
        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        for($i=0;$i<count($request->sodata);$i++) { 
            
            //Update Customer SO Information
            Planning_Model::UpdateCustomerSOInformation($request->sodata[$i]["value"]);

            //Save Delete SO Information
            Planning_Model::DeleteSOInformation($request->sodata[$i]["value"], $request->reason, $request->authinfo['id'], $request->authinfo['name'], $userid, $issuedby);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Sales order information has been deleted."
        ]);

    }

    function SavePurchaseRequestIndividual(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        //Save Purchase Request
        $prid = Planning_Model::SavePR($request);

        return json_encode([
            "success"=>true,
            "message"=>"Purchase request has been save."
        ]);

    }

    function LoadRawMaterialsExcess(){

        $rawmaterials = Planning_Model::LoadRawMaterialsExcess();

        return json_encode([
            "data"=>$rawmaterials
        ]);

    }

    function CreateNewPRS(){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        $prid = Planning_Model::CreateNewPRS($issuedby, $this->tableschema);
        $prnumber = Planning_Model::GetPRNumber($prid);

        return json_encode([
            "prid"=>$prid,
            "prnumber"=>$prnumber
        ]);

    }

    function CancelNewPRS(Request $request){

        Planning_Model::DeleteItemInfoDelivery($request->prid);
        Planning_Model::DeletePRInfo($request->prid);
        Planning_Model::DeletePRItem($request->prid);
        Planning_Model::UpdateSOPurchaseRequest($request->prid);

        return json_encode([
            "success"=>true,
            "message"=>"Purchase request cancel."
        ]);

    }

    function SaveNewPRSItem(Request $request){

        $content = "";

        for($i=0;$i<count($request->rmid);$i++){

            Planning_Model::SaveNewPRSItem($request->prid, $request->rmid[$i]["value"], $request->qtyrequired[$i]["value"]);

        }

        $priteminfo = Planning_Model::GetPRItemInfo($request->prid);
        $prsoitemsavecount = count($priteminfo);

        foreach($priteminfo as $val){

            $deliteminfocontent = "";            
            $deliteminfo = Planning_Model::GetDelItemInfo($val->itemid);

            if(count($deliteminfo)!=0){

                foreach($deliteminfo as $del){
                
                    $deliteminfocontent .= '
                        Date: '. $del->deliverydate .' Qty: '. $del->deliveryqty .' <button id="btndeldeliveryinfo" name="btndeldeliveryinfo" class="btn btn-link" value="'. $del->id .'" data-itemid="'. $val->itemid .'"><i class="fa fa-times" aria-hidden="true"></i></button><br>
                    ';
    
                }

            }
            else{

                $deliteminfocontent = "None";

            }


            $content .= '
            <tr id="prsosaveitem'. $val->itemid .'"> 
                <td>
                    '. $val->rawmaterial .'
                </td> 
                <td>
                   '. $val->qtyrequired .'
                </td> 
                <td id="deliteminfo'. $val->itemid .'">
                    '. $deliteminfocontent .'
                </td> 
                <td style="text-align: center;">
                    <button id="btndelprsoitem" name="btndelprsoitem" class="btn btn-flat btn-primary" title="Delivery" value="'. $val->itemid .'"><i class="fa fa-edit"></i></button>
                    <button id="btnremoveprsoitem" name="btnremoveprsoitem" class="btn btn-flat btn-danger" value="'. $val->itemid .'" title="Remove Item"><i class="fa fa-trash"></i></button>
                </td> 
            </tr>
            ';

        }

        return json_encode([
            "success"=>true,
            "content"=>$content,
            "prsoitemsavecount"=>$prsoitemsavecount
        ]);

    }


    function DeletePRItem(Request $request){

        Planning_Model::DeletePRItemIndi($request->itemid);
        Planning_Model::DeleteDelPRItemIndi($request->itemid);

        $priteminfo = Planning_Model::GetPRItemInfo($request->prid);
        $prsoitemsavecount = count($priteminfo);

        return json_encode([
            "success"=>true,
            "prsoitemsavecount"=>$prsoitemsavecount
        ]);

    }

    function SaveDelPRItemInfo(Request $request){

        $deliteminfocontent = "";

        Planning_Model::SaveDelPRItemInfo($request->itemid, $request->deldate, $request->delqty);
        $deliteminfo = Planning_Model::GetDelItemInfo($request->itemid);

        foreach($deliteminfo as $del){
        
            $deliteminfocontent .= '
                Date: '. $del->deliverydate .' Qty: '. $del->deliveryqty .' <button id="btndeldeliveryinfo" name="btndeldeliveryinfo" class="btn btn-link" value="'. $del->id .'" data-itemid="'. $request->itemid .'"><i class="fa fa-times" aria-hidden="true"></i></button><br>
            ';

        }

        return json_encode([
            "success"=>true,
            "content"=>$deliteminfocontent
        ]);

    }

    function DeleteDelDeliveryInfo(Request $request){

        $deliteminfocontent = "";

        Planning_Model::DeleteDelDeliveryInfo($request->id);
        $deliteminfo = Planning_Model::GetDelItemInfo($request->itemid);

        if(count($deliteminfo)!=0){

            foreach($deliteminfo as $del){
        
                $deliteminfocontent .= '
                    Date: '. $del->deliverydate .' Qty: '. $del->deliveryqty .' <button id="btndeldeliveryinfo" name="btndeldeliveryinfo" class="btn btn-link" value="'. $del->id .'" data-itemid="'. $request->itemid .'"><i class="fa fa-times" aria-hidden="true"></i></button><br>
                ';
    
            }

        }
        else{

            $deliteminfocontent = 'None';

        }

        return json_encode([
            "success"=>true,
            "content"=>$deliteminfocontent
        ]);

    }

    function LoadPurchaseRequestInformation(Request $request){

        $content = "";
        $purchaserequestinfo = Planning_Model::LoadPurchaseRequestInformation($request->customer);

        $content .= '
            <option value="" disabled selected>Select a Purchase Request Number</option>
        ';
        foreach($purchaserequestinfo as $val){

            $content .= '
                <option value="'. $val->prid .'">'. $val->prnumber .' - '. $val->ponumber .'</option>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function LoadPurchaseRequestData(Request $request){

        $content = "";
        $itemcount = 0;
        $itemarray = array();
        $contentexcess = "";

        $purchaserequestdata = Planning_Model::LoadPurchaseRequestData($request->prid);
        $purchaserequestitemdata = Planning_Model::LoadPurchaseRequestItemData($request->prid);
        $rawmaterials = Planning_Model::LoadRawMaterials();
        $rawmaterialsexcess = Planning_Model::LoadRawMaterialsExcess();

        foreach($rawmaterialsexcess as $excess){

            $contentexcess .= '
                <option value="db-'. $excess->rmid .'">'.  $excess->description .'</option>
            ';

        }

        foreach($purchaserequestitemdata as $val){

            $rmcontent = "";
            $rmcontent .= '
                <option value="" disabled selected>Select a Size</option>
            ';
            foreach($rawmaterials as $raw){

                if($val->rmid==$raw->rmid){

                    $rmcontent .= '
                        <option value="'. $raw->rmid .'" selected>'. $raw->description .'</option>
                    ';

                }
                else{

                    $rmcontent .= '
                        <option value="'. $raw->rmid .'">'. $raw->description .'</option>
                    ';

                }

            }

            $itemcount += 1;
            if($itemcount==1){

                $content .= '
                    <tr id="'. $itemcount .'">
                        <td>
                            <select id="cmbnrawitems'. $itemcount .'" name="cmbnrawitems[]" class="form-control">
                                '. $rmcontent .'
                            </select>
                        </td>
                        <td><input id="txtnrequiredqty'. $itemcount .'" name="txtnrequiredqty[]" type="number" class="form-control" placeholder="0"></td>
                        <td><textarea class="form-control" rows="2" id="txtnremarks'. $itemcount .'" name="txtnremarks[]" placeholder="Remarks"></textarea></td>
                        <td>
                            <select id="txtnexcessboard'. $itemcount .'" name="txtnexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;">
                                '. $contentexcess .'
                            </select>
                        </td>
                        <td><select id="txtnqtyexcessboard'. $itemcount .'" name="txtnqtyexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td>
                        <td></td>
                    </tr>
                ';

            }
            else{

                $content .= '
                    <tr id="'. $itemcount .'">
                        <td>
                            <select id="cmbnrawitems'. $itemcount .'" name="cmbnrawitems[]" class="form-control">
                                '. $rmcontent .'
                            </select>
                        </td>
                        <td><input id="txtnrequiredqty'. $itemcount .'" name="txtnrequiredqty[]" type="number" class="form-control" placeholder="0"></td>
                        <td><textarea class="form-control" rows="2" id="txtnremarks'. $itemcount .'" name="txtnremarks[]" placeholder="Remarks"></textarea></td>
                        <td>
                            <select id="txtnexcessboard'. $itemcount .'" name="txtnexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;">
                                '. $contentexcess .'
                            </select>
                        </td>
                        <td><select id="txtnqtyexcessboard'. $itemcount .'" name="txtnqtyexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td>
                        <td><button id="btnremovepritem" name="btnremovepritem" class="btn btn-flat btn-danger" value="'. $itemcount .'"><i class="fa fa-trash"></i></button></td>
                    </tr>
                ';

                $itemarray[] = $itemcount;

            }

        }

        return json_encode([
            "fgid"=>$purchaserequestdata->fgid,
            "itemcount"=>$itemcount,
            "itemarray"=>$itemarray,
            "content"=>$content
        ]);

    }

    function LoadMaterialRequestData(Request $request){

        $prid = Planning_Model::GetMaterialRequestPRID($request->requestid);

        $requestdata = Planning_Model::LoadMaterialRequestData($prid, $request->requestid);

        return json_encode([
            "ponumber"=>$requestdata->ponumber,
            "poitem"=>$requestdata->description,
            "targetoutput"=>$requestdata->targetoutput
        ]);

    }

    function LoadCustomerFGSalesOrder(Request $request){

        $content = "";
        $fgsoinfo = Planning_Model::LoadCustomerFGSalesOrder($request->fgid);

        if(count($fgsoinfo)!=0){

            foreach($fgsoinfo as $val){

                $content .= '
                    <tr>
                        <td style="text-align: center;">
                            <label><input id="chkexcessso" name="chkexcessso[]" type="checkbox" value="'. $val->delid .'"></label>
                        </td>
                        <td>'. $val->ponumber .'</td>
                        <td>'. $val->salesnumber .'</td>
                        <td>'. $val->deliveryqty .'</td>
                        <td>'. $val->deliverydate .'</td>
                    </tr>
                ';
    
            }

        }
        else{

            $content = '
                <tr>
                    <td colspan="5" style="text-align: center;">No Sales Order Information.</td>
                </tr>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function LoadRawMaterialWithExcess(){

        $content = "";
        $rawmaterialexcessinfo = Planning_Model::LoadRawMaterialWithExcess();

        $content .= '
            <option value="" disabled selected>Select a Size</option>
        ';
        foreach($rawmaterialexcessinfo as $val){

            $content .= '
                <option value="'.  $val->rmid .'">'.  $val->description .'</option>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function SaveRequestMaterialExcess(Request $request){

        $cid = "";
        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        //Validation
        for($v=0;$v<count($request->rmid);$v++){

            for($l=0;$l<count($request->excessboard[$v]);$l++){

                $valexcess = $request->excessboard[$v][$l];
                $valqtyexcess = $request->qtyexcessboard[$v][$l];

                if($valexcess["value"]!=null){

                    if($valqtyexcess["value"]==null){

                        return json_encode([
                            "success"=>false,
                            "message"=>"Excess board must be equal to the quantity excess board."
                        ]);

                    }
                    else{
   
                        if(count($request->excessboard[$v])!=count($request->qtyexcessboard[$v])){

                            return json_encode([
                                "success"=>false,
                                "message"=>"Excess board must be equal to the quantity excess board."
                            ]);
        
                        }                    

                    }



                }

            }

        }

        //Process
        if($request->cid=="All"){

            $cid = Planning_Model::GetFGIDCustomer($request->fgid);
        
        }
        else{
            $cid = $request->cid;
        }


        $sodata = [];
        for($e=0;$e<count($request->soinfo);$e++){

            $sodata[] = $request->soinfo[$e]["value"];

        }

        $mrid = Planning_Model::SaveRequestMaterial($request->mrinumber, $cid, $request->fgid, $request->reason, $issuedby, '0');

        for($i=0;$i<count($request->rmid);$i++){

            $mritemid = Planning_Model::SaveRequestMaterialItem($mrid, $request->rmid[$i]["value"], $request->reqqty[$i]["value"], $request->remarks[$i]["value"]);

            //Excess Board
            for($y=0;$y<count($request->excessboard[$i]);$y++){

                $dataexcess = $request->excessboard[$i][$y];
                $dataqtyexcess = $request->qtyexcessboard[$i][$y];

                if($dataexcess["value"]!=null){
                    
                    if(Str::contains($dataexcess["value"], ['db'])){

                        $rmid = explode('-', $dataexcess["value"]);
                        Planning_Model::SaveExcessBoardItem($mritemid, $rmid[1], $dataqtyexcess["value"]);

                    }
                    else{

                        //Validation
                        $validation = Planning_Model::ValidateExcessBoard($dataexcess["value"]);

                        if($validation){
                            
                            $excestingrmid = Planning_Model::GetExcessBoardRMID($dataexcess["value"]);

                            //Save Item Excess Board
                            Planning_Model::SaveExcessBoardItem($mritemid, $excestingrmid, $dataqtyexcess["value"]);

                        }
                        else{

                            $stripped = str_replace(' ', '', $dataexcess["value"]);
                            $newrmdata = explode('x', $stripped);
    
                            //Width Length
                            $width = "";
                            $widthsize = "";
                            $length = "";
                            $lengthsize = "";
                            $description = $dataexcess["value"];
                            if(Str::contains($newrmdata[0], ['.'])){
    
                                $wsizedata = explode('.', $newrmdata[0]);
    
                                $width = $wsizedata[0];
                                $widthsize = $wsizedata[1];
    
                            }
                            else{
    
                                $width = $newrmdata[0];
    
                            }
    
                            if(Str::contains($newrmdata[1], ['.'])){
    
                                $lsizedata = explode('.', $newrmdata[1]);
    
                                $length = $lsizedata[0];
                                $lengthsize = $lsizedata[1];
    
                            }
                            else{
    
                                $length = $newrmdata[1];                            
    
                            }
    
                            //Save New Excess Board
                            $newrmid = Planning_Model::SaveNewExccessBoard($request->rmid[$i]["value"], $width, $widthsize, $length, $lengthsize, $description);
    
                            //Save Item Excess Board
                            Planning_Model::SaveExcessBoardItem($mritemid, $newrmid, $dataqtyexcess["value"]);

                        }

                    }


                }

            }

        }

        //Attach MRID In Sales Order Information
        Planning_Model::UpdateCustomerSOInfo($mrid, $sodata);

        return json_encode([
            "success"=>true,
            "message"=>"Material Request has been save."
        ]);

    }

    function LoadMRForJobOrderCreation(){

        $mrforcreation = Planning_Model::LoadMRForJobOrderCreation();

        $data = array();
        foreach($mrforcreation as $val){

            $obj = new \stdClass;
            $obj->customer = $val->customer;
            $obj->materialnumber = $val->mrinumber;
            $obj->ponumber = $val->ponumber;
            $obj->poitem = $val->description;
            $obj->targetoutput = number_format($val->deliveryqty, 2);
            $obj->panel = '<button id="btncrtjo" name="btncrtjo" class="btn btn-info btn-flat" value="'. $val->requestid .'" data-output="'. $val->deliveryqty .'"><i class="fa fa-check" aria-hidden="true"></i>
            </button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns([ 
            'panel'
        ])
        ->make(true);

    }

}
