<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RMExcess_Model extends Model
{
    
    public static function LoadExcessInformation(){

        $result = DB::table('materialrequestitemsexcess')
        ->select(
            'materialrequestitemsexcess.id',
            DB::raw("CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound) AS 'rmexcess'"),
            'materialrequestitemsexcess.qtyexcessboard'
        )
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'materialrequestitemsexcess.excessrmid')
        ->whereNull('materialrequestitemsexcess.updated_at')
        ->get();

        return $result;

    }

    public static function GetRMStockOnHand($id){

        $result = DB::table('rawmaterials')
        ->select(
            'stockonhand'
        )
        ->whereRaw("
            rmid=(
                SELECT
                excessrmid
                FROM materialrequestitemsexcess
                WHERE id=". $id ."
            )
        ")
        ->first();

        return $result->stockonhand;

    }

    public static function GetExcessQty($id){

        $result = DB::table('materialrequestitemsexcess')
        ->select(
            'qtyexcessboard'
        )
        ->where('id', '=', $id)
        ->first();

        return $result->qtyexcessboard;

    }

    public static function SaveRMAudit($id, $qty, $balance, $issuedby){

        DB::table('rawmaterialaudit')
        ->insert([
            'rmid'=>DB::raw("(SELECT rmid FROM rawmaterials WHERE rmid=(SELECT excessrmid FROM materialrequestitemsexcess WHERE id=". $id ."))"),
            'qty'=>$qty,
            'balance'=>$balance,
            'issuedby'=>$issuedby,
            'issueddate'=>DB::raw("NOW()"),
            'issuedtime'=>DB::raw("CURTIME()"),
        ]);

    }

    public static function UpdateRMStockOnHand($id, $stockonhand){

        DB::table('rawmaterials')
        ->whereRaw("
            rmid=(
                SELECT
                excessrmid
                FROM materialrequestitemsexcess
                WHERE id=". $id ."                
            )
        ")
        ->update([
            'stockonhand'=>$stockonhand,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function UpdateExcessInformation($id){

        DB::table('materialrequestitemsexcess')
        ->where('id', '=', $id)
        ->update([
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

}
