<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Suppliers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('sid');
            $table->string('supplier', '60')->unique();
            $table->text('contactperson');
            $table->text('contactnumber');
            $table->string('email', '60');
            $table->longtext('address');
            $table->text('tin')->nullable();
            $table->text('terms')->nullable();
            $table->string('taxclassification', '60');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
