@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Production Operation</b></h3>
        </div>
        <div class="box-body">

            <div class="col-md-12">
                <br>
                <button id="btnnewoperation" name="btnnewoperation" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Operation</button>

                <button id="btnrequestjo" name="btnrequestjo" class="btn btn-flat btn-primary" style="float: right; margin-bottom: 10px; margin-right: 10px;"><i class="fa fa-file"></i> New Job Order</button>


            </div>
            <div class="col-md-12">
                <br>
                <table id="tbloperations" class="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Job Order #</th>
                            <th>Target Output</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  @include('modal.production.newproductionoperation')
  @include('modal.production.newjoborder')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tbloperations;
    var sjorequestnumber;

    $(document).ready(function(){

        //Message
        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
          toastr.success(msg, '', { positionClass: 'toast-top-center' });
        }

        //Set
        $("#txtdeldatefrom").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        $("#txtdeldateto").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        //Load
        LoadOperationInformation();

    });

    $('#btnnewoperation').on('click', function(){

        @if(in_array(59, $access))

            $('#newproductionoperation').modal('toggle');
            LoadJobOrder();

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif


    });

    $('#btnncreate').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveOperation();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    $(document).on('click', '#btnoperationprofile', function(){

        @if(in_array(60, $access))

            var mid = $(this).val();
            window.location = "{{ url('/productionoperation/productionoperationinfo') }}/" + mid;

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

    });

    $('#btnrequestjo').on('click', function(){

        $('#divjocontent').hide();
        $('#newjoborderprod').modal('toggle');
        LoadJobOrderForRequest();
        GenJONumber();
        ClearNewJobOrder();

    });

    $('#cmbjornumber').on('change', function(){

        sjorequestnumber = $(this).val();

        LoadJORequestInformation(sjorequestnumber);
        $('#divjocontent').fadeIn();
    
    });

    $('#btncreate').on('click', function(){

        $.confirm({
              title: 'Create',
              content: 'Create this job order information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        Create();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    function LoadJORequestInformation(jorequestnumber){

        $.ajax({
            url: '{{ url("api/productionoperation/loadjorequestinformation") }}',
            type: 'get',
            data: {
                joid: jorequestnumber
            },
            dataType: 'json',
            success: function(response){

                $('#txtjocustomer').val(response.customer);
                $('#txtjoponumber').val(response.ponumber);
                $('#txtjoitem').val(response.description);
                $('#txtmrnumber').val(response.mrinumber);
                $('#txtjotargetoutput').val(response.targetoutput);
                $('#txtspecialinstruction').val(response.specialinstruction);

            }
        });

    }

    function Create(){

        var joid = $('#cmbjornumber').val();
        var style = $('#txtstyle').val();
        var inkcolor = $('#txtinkcolor').val();
        var specialinstruction = $('#txtspecialinstruction').val();
        var deldatefrom = $('#txtdeldatefrom').val();
        var deldateto = $('#txtdeldateto').val();

        if(inkcolor==""){
          $('#txtinkcolor').val('None');
          inkcolor = "None";
        }

        //Validation
        if(style==""){
          toastr.error("Please input the style.", '', { positionClass: 'toast-top-center' });
        }
        else if(deldatefrom==""){
            toastr.error("Please select a delivery date from.", '', { positionClass: 'toast-top-center' });
        }
        else if(deldateto==""){
            toastr.error("Please select delivery date to.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/productionoperation/updatecreatejo") }}',
                type: 'post',
                data: {
                    joid: joid,
                    style: style,
                    inkcolor: inkcolor,
                    specialinstruction: specialinstruction,
                    deldatefrom: deldatefrom,
                    deldateto: deldateto
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newjoborderprod .close').click();

                    }

                }
            });

        }

    }

    function SaveOperation(){

        var joid = $('#txtnjonumber').val();
        
        if(joid==null || joid==""){

            toastr.error("Please select a job order number.", '', { positionClass: 'toast-top-center' });
            $('#txtnjonumber').focus();

        }
        else{

            $.ajax({
                url: '{{ url("api/productionoperation/saveoperation") }}',
                type: 'post',
                data: {
                    joid: joid
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newproductionoperation .close').click();
                        ReloadOperationInformation();

                        //Socket Emit
                        socket.emit('monitoringjo');

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    }

    function LoadJobOrder(){

        $.ajax({
            url: '{{ url("api/productionoperation/loadjoborder") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtnjonumber').find('option').remove();
                $('#txtnjonumber').append('<option value="" disabled selected>Select a Job Order Number</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#txtnjonumber').append('<option value="'+response.data[i]["joid"]+'">'+response.data[i]["jonumber"]+'</option>');

                }

                $('#txtnjonumber').select2({
                    theme: 'bootstrap'
                });  

            }
        });

    }

    function LoadJobOrderForRequest(){

        $.ajax({
            url: '{{ url("api/productionoperation/loadjoborderforrequest") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbjornumber').find('option').remove();
                $('#cmbjornumber').append('<option value="" disabled selected>Select a Job Order Request Number</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#cmbjornumber').append('<option value="'+response.data[i]["joid"]+'">'+response.data[i]["jorequestnumber"]+'</option>');

                }

                $('#cmbjornumber').select2({
                    theme: 'bootstrap'
                });  

            }
        });

    }

    function GenJONumber(){

        $.ajax({
            url: '{{ url("api/planning/genjonumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtjonum').val(response.batch);

            }
        });

    }

    function ClearNewJobOrder(){

        $('#txtstyle').val('');
        $('#txtinkcolor').val('');
        $('#txtspecialinstruction').val('');
        $('#txtdeldatefrom').val('');
        $('#txtdeldateto').val('');

    }

    function LoadOperationInformation(){

        tbloperations = $('#tbloperations').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/productionoperation/loadoperationinformation") }}'
        },
        columns : [
            {data: 'id', name: 'id'},
            {data: 'jonumber', name: 'jonumber'},
            {data: 'targetoutput', name: 'targetoutput'},
            {data: 'panel', name: 'panel', width: '12%'},
        ]
      });

    }

    function ReloadOperationInformation(){

        tbloperations.ajax.reload();

    }

  </script>  

@endsection