<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\QualityAssurance_Model;
use DataTables;
use Illuminate\Support\Collection;
use App\Http\Controllers\UserController;

class QualityAssuranceController extends Controller
{
    
    private $tableschema;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }

    function index($view=null, $action=null, $id=null){

        $access = UserController::UserGroupAccess();

        if($view!=null && $id!=null){

            if($action=="info"){

                $joborder = QualityAssurance_Model::LoadJobOrderNumber($id);

                return view('extensions.qualityassurance.'.$view)
                ->with('id', $id)
                ->with('joborder', $joborder)
                ->with('access', $access);


            }
            else if($action=="printfi"){

                $qafinalinfo = QualityAssurance_Model::GetQAFinalInformation($id);
                $joinformation = QualityAssurance_Model::GetJobOrderInformation($qafinalinfo->joid);
                $qafinaldimension = QualityAssurance_Model::GetQAFinalDimension($id);        
                $qafinalmeasure = QualityAssurance_Model::GetQAFinalMeasure($id);
                $qafinalvisual = QualityAssurance_Model::GetQAFinalVisual($id);
                $qafinalothers = QualityAssurance_Model::GetQAFinalOthers($id);
                $rejecttype = QualityAssurance_Model::GetRejectType($id);

                $ID_length = preg_replace("/[^0-9]./", "", $joinformation->ID_length);
                $ID_width = preg_replace("/[^0-9]./", "", $joinformation->ID_width);
                $ID_height = preg_replace("/[^0-9]./", "", $joinformation->ID_height);
                $OD_length = preg_replace("/[^0-9]./", "", $joinformation->OD_length);
                $OD_width = preg_replace("/[^0-9]./", "", $joinformation->OD_width);
                $OD_height = preg_replace("/[^0-9]./", "", $joinformation->OD_height);

                return view('extensions.qualityassurance.'.$view)
                ->with('qafinalinfo', $qafinalinfo)
                ->with('joinformation', $joinformation)
                ->with('qafinalmeasure', $qafinalmeasure)
                ->with('qafinalvisual', $qafinalvisual)
                ->with('qafinalothers', $qafinalothers)
                ->with('qafinaldimension', $qafinaldimension)
                ->with(compact(
                    'ID_length',
                    'ID_width',
                    'ID_height',
                    'OD_length',
                    'OD_width',
                    'OD_height'
                ))
                ->with('rejecttype', $rejecttype);

            }
            else if($action="printcoc"){

                return view('extensions.qualityassurance.'.$view);

            }
            
    
        }
        else{
    
            return view('qualityassurance')
            ->with('access', $access);
    
        }


    }

    function NewFinalInspection($joid=null, $mid=null, $pid=null){

        $access = UserController::UserGroupAccess();
        $rejecttype = QualityAssurance_Model::LoadRejectType();
        $joinformation = QualityAssurance_Model::GetJobOrderInformation($joid);
        $jomonitoringinfo = QualityAssurance_Model::GetJOMonitoringInfo($mid, $pid);

        $ID_length = preg_replace("/[^0-9]./", "", $joinformation->ID_length);
        $ID_width = preg_replace("/[^0-9]./", "", $joinformation->ID_width);
        $ID_height = preg_replace("/[^0-9]./", "", $joinformation->ID_height);
        $OD_length = preg_replace("/[^0-9]./", "", $joinformation->OD_length);
        $OD_width = preg_replace("/[^0-9]./", "", $joinformation->OD_width);
        $OD_height = preg_replace("/[^0-9]./", "", $joinformation->OD_height);

        return view('extensions.qualityassurance.qanewfinalinspection')
        ->with('joinformation', $joinformation)
        ->with('jomonitoringinfo', $jomonitoringinfo)
        ->with('rejecttype', $rejecttype)
        ->with(compact(
            'ID_length',
            'ID_width',
            'ID_height',
            'OD_length',
            'OD_width',
            'OD_height'
        ))
        ->with('joid', $joid)
        ->with('mid', $mid)
        ->with('pid', $pid)
        ->with('access', $access);

    }

    function COCInformations(Request $request){

        $coc = QualityAssurance_Model::COCInformations($request->customer);

        $data = array();

        foreach($coc as $val){

            $obj = new \stdClass;

            $obj->jonumber = '
                <a href="'. url('qafinalinspection/new') .'/'. $val->joid .'/'. $val->mid .'/'. $val->pid .'">'. $val->jonumber .'</a>
            ';
            $obj->itemdescription = $val->description;
            $obj->targetoutput = $val->targetoutput;
            $obj->balance = $val->balance;

            $data[] = $obj;

        }

        $info = new Collection($data);
        
        return Datatables::of($info)
        ->rawColumns(['jonumber'])
        ->make(true);

    }

    function SaveFinalInspection(Request $request){

        //Save Final Inspection Information
        $qafiid = QualityAssurance_Model::SaveFinalInspectionInformation($request->joid, $request->mid, $request->pid, $request->tolerance, $request->outputqty, $request->insideoutside, $request->flute, $request->thickness, Auth::user()->id, $request->auditid, $this->tableschema);

        //Save Measure
        QualityAssurance_Model::SaveFIMeasure($qafiid, $request->mt, $request->cal, $request->ms);

        //Save Visual
        for($i=0;$i<count($request->visual);$i++){

            QualityAssurance_Model::SaveFIVisual($qafiid, $request->visual[$i]["value"]);

        }

        //Save Others
        QualityAssurance_Model::SaveFIOthers($qafiid, $request->others);

        //Save Dimension
        for($i=0;$i<count($request->dimensionlength);$i++){

            QualityAssurance_Model::SaveFIDimension($qafiid, $request->dimensionlength[$i]["value"], $request->dimensionwidth[$i]["value"], $request->dimensionheight[$i]["value"], $request->remarks[$i]["value"]);

        }

        //Save For Tags
        QualityAssurance_Model::SaveProductionTagInformation($qafiid, $this->tableschema);

        return json_encode([
            "success"=>true
        ]);


    }

    function LoadFinalInspectionInformation(Request $request){

        $fiinformation = QualityAssurance_Model::LoadFinalInspectionInformation($request->from, $request->to);

        $data = array();

        foreach($fiinformation as $val){

            $obj = new \stdClass;

            $obj->finumber = '
                <a href="'. url('qafinalinspection/info') .'/'. $val->qafiid .'">'. $val->finalinspectionnumber .'</a>
            ';
            $obj->jonumber = $val->jonumber;
            $obj->qty = $val->outputqty;
            $obj->customer = $val->customer;
            $obj->validateby = $val->validateby;
            $obj->confirmdate = $val->confirmdate;

            $data[] = $obj;

        }

        $info = new Collection($data);
        
        return Datatables::of($info)
        ->rawColumns(['finumber'])
        ->make(true);

    }

    function QAFinalInformationProfile($qafiid=null){

        $access = UserController::UserGroupAccess();
        $rejecttype = QualityAssurance_Model::LoadRejectType();
        $qafinalinfo = QualityAssurance_Model::GetQAFinalInformation($qafiid);
        $joinformation = QualityAssurance_Model::GetJobOrderInformation($qafinalinfo->joid);
        $qafinalmeasure = QualityAssurance_Model::GetQAFinalMeasure($qafiid);
        $qafinalvisual = QualityAssurance_Model::GetQAFinalVisual($qafiid);
        $qafinalothers = QualityAssurance_Model::GetQAFinalOthers($qafiid);
        $qafinaldimension = QualityAssurance_Model::GetQAFinalDimension($qafiid);

        $ID_length = preg_replace("/[^0-9]./", "", $joinformation->ID_length);
        $ID_width = preg_replace("/[^0-9]./", "", $joinformation->ID_width);
        $ID_height = preg_replace("/[^0-9]./", "", $joinformation->ID_height);
        $OD_length = preg_replace("/[^0-9]./", "", $joinformation->OD_length);
        $OD_width = preg_replace("/[^0-9]./", "", $joinformation->OD_width);
        $OD_height = preg_replace("/[^0-9]./", "", $joinformation->OD_height);

        return view('extensions.qualityassurance.qafinalinformationprofile')
        ->with('qafiid', $qafiid)
        ->with('qafinalinfo', $qafinalinfo)
        ->with('joinformation', $joinformation)
        ->with('qafinalmeasure', $qafinalmeasure)
        ->with('qafinalvisual', $qafinalvisual)
        ->with('qafinalothers', $qafinalothers)
        ->with('qafinaldimension', $qafinaldimension)
        ->with('rejecttype', $rejecttype)
        ->with(compact(
            'ID_length',
            'ID_width',
            'ID_height',
            'OD_length',
            'OD_width',
            'OD_height'
        ))
        ->with('access', $access);

    }

    function ConfirmFinalInspection(Request $request){

        QualityAssurance_Model::ConfirmFinalInspection($request->qafiid, Auth::user()->id);

        return json_encode([
            "success"=>true,
            "message"=>"Final inspection information has been confirm."
        ]);

    }


}
