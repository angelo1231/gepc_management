<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('cid');
            $table->string('customer', '60')->unique();
            $table->text('contactperson');
            $table->text('contactnumber');
            $table->string('email', '60');
            $table->longtext('address');
            $table->text('tin')->nullable();
            $table->text('terms')->nullable();
            $table->string('taxclassification', '60');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
