<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Deliverypreparationinformationitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliverypreparationsinformationitems', function (Blueprint $table) {
            $table->increments('itemid');
            $table->string('preparationid', 60);
            $table->integer('poid');
            $table->integer('pocusitemid');
            $table->integer('fgid');
            $table->integer('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliverypreparationsinformationitems');
    }
}
