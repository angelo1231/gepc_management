<?php
namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class User_Model extends Model
{

    public static function ValidateUsername($username){

        $result = DB::table('users')
        ->select(DB::raw('COUNT(*) AS username_count'))
        ->where('username', '=', $username)
        ->get();
        
        return $result[0]->username_count;
 
    }

    public static function UserInformation($id){

        $result = DB::table('users')
        ->select(
            'users.id', 
            'users.firstname', 
            'users.lastname', 
            'users.mi', 
            'users.username', 
            DB::raw("usergroups.usergroup AS 'usertype'")
        )
        ->join('usergroups', 'usergroups.ugroupid', '=', 'users.usertype')
        ->where('users.id', '!=', $id)
        ->get();

        return $result;

    }

    public static function UserProfile($id){

        $result = DB::table('users')
        ->select(
            'firstname', 
            'lastname',
            'mi', 
            'username',
            'password',
            'usertype'
        )
        ->where('id', '=', $id)
        ->get();

        return $result; 

    }

    public static function UserRestriction($id){

        $result = DB::table('usersrestrictions')
        ->select(
            'id',
            'purchasing',
            'planning',
            'rawmaterial',
            'finishgoods',
            'sales',
            'qualityassurance',
            'production',
            'accounting'
        )
        ->where('uid', '=', $id)
        ->get();

        return $result;

    }

    public static function UpdateProfile($data){

       DB::table('users')
       ->where('id', '=', $data->uid)
       ->update([
            "firstname"=>$data->firstname,
            "lastname"=>$data->lastname,
            "mi"=>$data->mi,
            "usertype"=>$data->usertype,
            "updated_at"=>DB::raw("NOW()")
        ]);

        DB::table('usersrestrictions')
        ->where('uid', '=', $data->uid)
        ->update([
            "purchasing"=>$data->purchasing,
            "planning"=>$data->planning,
            "rawmaterial"=>$data->rawmaterial,
            "finishgoods"=>$data->finishgoods,
            "sales"=>$data->sales,
            "qualityassurance"=>$data->qualityassurance,
            "production"=>$data->production,
            "accounting"=>$data->accounting,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function OldPassword($id){

        $result = DB::table('users')
        ->select('password')
        ->where('id', '=', $id)
        ->get();

        return $result[0]->password;

    }

    public static function RegisterUser($data){

        $result = DB::table('users')
        ->insertGetId([
            "firstname"=>$data->firstname,
            "lastname"=>$data->lastname,
            "mi"=>$data->mi,
            "username"=>$data->username,
            "password"=>$data->password,
            "usertype"=>$data->usertype,
            "image"=>"noimage.jpg",
            "esign"=>"noesignimage.png",
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateUserRestriction($uid){

        $result = DB::table('usersrestrictions')
        ->select(
            DB::raw("COUNT(*) AS 'restriction_count' ")
        )
        ->where('uid', '=', $uid)
        ->first();

        if($result->restriction_count==0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function AddUserRestriction($uid){

        DB::table('usersrestrictions')
        ->insert([
            "uid"=>$uid,
            "purchasing"=>"0",
            "planning"=>"0",
            "rawmaterial"=>"0",
            "finishgoods"=>"0",
            "sales"=>"0",
            "qualityassurance"=>"0",
            "production"=>"0",
            "accounting"=>"0",
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadUserGroup(){

        $result = DB::table('usergroups')
        ->select(
            'ugroupid',
            'usergroup'
        )
        ->get();

        return $result;

    }

    public static function UserGroupAccess($ugroupid){

        $result = DB::table('useraccess')
        ->select(
            DB::raw("GROUP_CONCAT(accessid) AS 'access'")
        )
        ->where('ugroupid', '=', $ugroupid)
        ->groupBy('ugroupid')
        ->get();

        return $result;

    }

    public static function GetUsertype($ugroupid){

        $result = DB::table('usergroups')
		->select(
			'usergroup'
		)
		->where('ugroupid', '=', $ugroupid)
		->first();

		return $result->usergroup;

    }

}
