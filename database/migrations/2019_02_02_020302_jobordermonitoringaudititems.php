<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jobordermonitoringaudititems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobordermonitoringaudititems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('auditid');
            $table->text('operator');
            $table->integer('qty');
            $table->integer('rejectqty');
            $table->integer('rejectid');
            $table->datetime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobordermonitoringaudititems');
    }
}
