<!-- Middle Modal -->
<style>
        .modal {
          text-align: center;
          padding: 0!important;
        }
        
        .modal:before {
          content: '';
          display: inline-block;
          height: 100%;
          vertical-align: middle;
          margin-right: -4px;
        }
        
        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }

        .ui-autocomplete  {
            z-index: 1100 !important;
        }
        </style>   
        <!-- Modal -->
        <div class="modal fade" id="modalnewitem" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <strong>NEW RAW MATERIAL</strong>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="container-fluid">
                        <div class="col-md-6">
                                <div class="col-md-12" id="nname">
                                        <div class="form-group">
                                                <label for="txtnname">Name</label>
                                                <input id="txtnname" name="txtnname" type="text" class="form-control" placeholder="Name">
                                        </div>
                                </div>
                                <div class="col-md-12" id="ndescription">
                                        <div class="form-group">
                                                <label for="txtndescription">Description</label>
                                                <textarea id="txtndescription" name="txtndescription" type="text" rows="4" class="form-control" placeholder="Description"></textarea>
                                        </div>
                                </div>
                                <div class="col-md-6" id="nwidth">
                                        <div class="form-group">
                                                <label for="txtnwidth">Width</label>
                                                <input id="txtnwidth" name="txtnwidth" type="text" class="form-control" placeholder="Width">
                                        </div>
                                </div>
                                <div class="col-md-6" id="nwidthsize">
                                        <div class="form-group">
                                                <label for="txtnwidthsize"></br></label>
                                                <input id="txtnwidthsize" name="txtnwidthsize" type="text" class="form-control">
                                        </div>
                                </div>
                                <div class="col-md-6" id="nlength">
                                        <div class="form-group">
                                                <label for="txtnlength">Length</label>
                                                <input id="txtnlength" name="txtnlength" type="text" class="form-control" placeholder="Length">
                                        </div>
                                </div>
                                <div class="col-md-6" id="nlengthsize">
                                        <div class="form-group">
                                                <label for="txtnlengthsize"></br></label>
                                                <input id="txtnlengthsize" name="txtnlengthsize" type="text" class="form-control">
                                        </div>
                                </div>
                                
                        </div>
                        <div class="col-md-6">
                                <div class="col-md-12" id="nflute">
                                        <div class="form-group">
                                                <label for="txtnflute">Flute</label>
                                                <input id="txtnflute" name="txtnflute" type="text" class="form-control" placeholder="Flute">
                                        </div>
                                </div>
                                <div class="col-md-12" id="nboardpound">
                                        <div class="form-group">
                                                <label for="txtnboardpound">Board Pound</label>
                                                <input id="txtnboardpound" name="txtnboardpound" type="text" class="form-control" placeholder="Board Pound">
                                        </div>
                                </div>
                                <div class="col-md-6" id="ncurrency">
                                        <div class="form-group">
                                                <label for="cmbncurrency">Currency</label>
                                                <select class="form-control" id="cmbncurrency">
                                                </select>
                                        </div>
                                </div>
                                <div class="col-md-6" id="nprice">
                                        <div class="form-group">
                                        <label id="lblnprice" for="txtnprice">Price</label>
                                        <input id="txtnprice" name="txtnprice" type="number" class="form-control" placeholder="Price">
                                        </div>
                                </div>
                                <div class="col-md-12">
                                        <div class="form-group">
                                                <label for="cmbncategory">Category</label>
                                                <select class="form-control" id="cmbncategory">
                                                        <option value="" disabled selected>Selec a Category</option>
                                                        <option value="Raw Material">Raw Material</option>
                                                        <option value="Other">Other</option>
                                                        <option value="Fix Asset">Fix Asset</option>
                                                </select>
                                        </div>
                                </div>

                        </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-9">
        
                        </div>
                        <div class="col-md-3">
                            <button id="btnsave" name="btnsave" type="button" class="btn btn-primary btn-block btn-flat">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>