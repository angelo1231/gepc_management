<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newjoborder" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>REQUEST JOB ORDER</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="cmbncustomer">Customer</label>
                            <select name="cmbncustomer" id="cmbncustomer" class="form-control">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="cmbnmaterialrequest">Material Request Number</label>
                            <select name="cmbnmaterialrequest" id="cmbnmaterialrequest" class="form-control">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="txtnponumber">Purchase Order Number</label>
                            <input type="text" id="txtnponumber" name="txtnponumber" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label for="txtnpoitem">Purchase Order Items</label>
                            <input type="text" id="txtnpoitem" name="txtnpoitem" class="form-control" readonly>
                        </div>

                        <div class="form-group">
                            <label for="txtntargetoutput">Target Output</label>
                            <input id="txtntargetoutput" name="txtntargetoutput" class="form-control" placeholder="Target Output" type="number">
                        </div>
    
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btncreatejo" name="btncreatejo" type="button" class="btn btn-primary btn-flat">Request</button>
                        <button id="btnclose" name="btnclose" type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>