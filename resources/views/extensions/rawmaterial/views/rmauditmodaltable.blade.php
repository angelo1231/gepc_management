<div class="col-md-12 row">
<table id="tblaudit" class="table">
        <thead>
            <tr>
              <th>PO#</th>
              <th>DR#</th>
              <th>MR#</th>
              <th>Quantity</th>
              <th>Balance</th>
              <th>Issued By</th>
              <th>Issued Date</th>
              <th>Issued Time</th>
            </tr>
        </thead>
        <tbody>
            @foreach($audit as $data)
                <tr>
                    <th>{{ $data->ponumber }}</th>
                    <th>{{ $data->drnumber }}</th>
                    <th>{{ $data->requisitionnumber }}</th>
                    <th>{{ $data->qty }}</th>
                    <th>{{ $data->balance }}</th>
                    <th>{{ $data->issuedby }}</th>
                    <th>{{ $data->issueddate }}</th>
                    <th>{{ $data->issuedtime }}</th>
                </tr>
            @endforeach
        </tbody>
</table>
</div>
<script>

    $(document).ready(function(){

        $('#tblaudit').DataTable({
            autoWidth: false,
            ordering: false,
        });
           
    });

</script>