<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="delso" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>DELETE SALES ORDER</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <table class="table table-bordered" id="tblplanningso" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Sales Order #</td>
                                    <td>Delivery Quantity</td>
                                    <td>Delivery Date</td>
                                </tr>
                            </thead>
                            <tbody id="tblplanningsocontent">

                            </tbody>
                        </table>

                        <div class="form-group">
                            <label for="txtdelreason">Reason</label>
                            <textarea class="form-control" rows="3" id="txtdelreason" name="txtdelreason" placeholder="Your Reason For Deletion"></textarea>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btndeleteso" name="btndeleteso" type="button" class="btn btn-primary btn-flat">Delete</button>
                        <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>