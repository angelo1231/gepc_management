<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="cocitemdimension" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>COC Item Dimensions</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12 row">
                         
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a data-toggle="tab" href="#dimensions">Dimensions</a></li>
                            <li><a data-toggle="tab" href="#visual">Visual</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="dimensions" class="tab-pane fade in active">
                               
                                <div class="col-md-12">
                                    <button id="btniadddimensions" name="btniadddimensions" class="btn btn-flat btn-primary" style="margin-top: 10px; margin-bottom: 10px; float: right;"><i class="fa fa-plus"></i></button>
                                </div>
                                

                                <div id="newdimension" class="col-md-12">

                                    <div class="col-md-12">
                                        <table style="margin-top: 10px; margin-bottom: 15px; margin-left: 17px; width: 100%;">
                                            <tr>
                                                <td>   
                                                    <label><input id="radidsize" name="radsize" type="radio" checked></label>
                                                </td>
                                                <td>ID Size</td>
                                                <td id="idsize"></td>
                                                <td>
                                                    <label><input id="radodsize" name="radsize" type="radio"></label>
                                                </td>
                                                <td>OD Size</td>
                                                <td id="odsize"></td>
                                            </tr>
                                        </table>
                                    </div>
                                    
                                    <div class="col-md-12">

                                        <div class="form-group col-md-12">
                                            <label for="txtiwidth">Width</label>
                                            <input id="txtiwidth" name="txtiwidth" type="number" class="form-control" placeholder="Width">
                                        </div>
    
                                        <div class="form-group col-md-12">
                                            <label for="txtilength">Length</label>
                                            <input id="txtilength" name="txtilength" type="number" class="form-control" placeholder="Length">
                                        </div>
    
                                        <div class="form-group col-md-12">
                                            <label for="txtiheight">Height</label>
                                            <input id="txtiheight" name="txtiheight" type="number" class="form-control" placeholder="Height">
                                        </div>

                                        <div class="form-group col-md-12">
                                            <label for="txtiremarks">Remarks</label>
                                            <input id="txtiremarks" name="txtiremarks" type="text" class="form-control" placeholder="Remarks" readonly>
                                        </div>
        
        
                                    </div>

                                    <div class="col-md-12">
                                        <button id="btnsavedimension" name="btnsavedimension" style="float: right; margin-right: 15px;" class="btn btn-flat btn-info"><i class="fa fa-save"></i> Save Information</button>
                                    </div>

                                </div>


                                <div id="tbldimensions" class="col-md-12">

                                    <table id="tblcocdimension" class="table">
                                        <thead>
                                            <tr>
                                                <th>Width</th>
                                                <th>Length</th>
                                                <th>Height</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>

                                </div>

                            </div>
                            <div id="visual" class="tab-pane fade">
                                
                                <div class="col-md-12">

                                    <br>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="optradiov15">V-15 No Dent/Damage Part</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov15ok" name="optradiov15" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov15ng" name="optradiov15" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov15na" name="optradiov15" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov12">V-12 No Weak Flute Glue</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov12ok" name="optradiov12" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov12ng" name="optradiov12" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov12na" name="optradiov12" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov13">V-13 No Misaligned Board</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov13ok" name="optradiov13" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov13ng" name="optradiov13" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov13na" name="optradiov13" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov2">V-2 No Rough Crack Edges</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov2ok" name="optradiov2" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov2ng" name="optradiov2" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov2na" name="optradiov2" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov8">V-8 No Double Image</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov8ok" name="optradiov8" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov8ng" name="optradiov8" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov8na" name="optradiov8" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov17">V-17 No Smeared Print</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov17ok" name="optradiov17" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov17ng" name="optradiov17" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov17na" name="optradiov17" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov18">V-18 No Weak Glue Joint Tab</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov18ok" name="optradiov18" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov18ng" name="optradiov18" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov18na" name="optradiov18" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov21">V-21 No Misalingned Gluing</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov21ok" name="optradiov21" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov21ng" name="optradiov21" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov21na" name="optradiov21" value="N/A">N/A</label>
                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="optradiov19">V-19 No Excess Glue</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov19ok" name="optradiov19" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov19ng" name="optradiov19" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov19na" name="optradiov19" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                            <label for="optradiov1">V-1 No Paper Discoloration</label><br>
                                            <label class="radio-inline"><input type="radio" id="optradiov1ok" name="optradiov1" value="OK">OK</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov1ng" name="optradiov1" value="NG">NG</label>
                                            <label class="radio-inline"><input type="radio" id="optradiov1na" name="optradiov1" value="N/A">N/A</label>
                                        </div>

                                        <div class="form-group">
                                                <label for="txtvothers">Others</label>
                                                <textarea class="form-control" rows="5" id="txtvothers" name="txtvothers"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtvflute">Flute</label>
                                            <input id="txtvflute" name="txtvflute" type="text" placeholder="Flute" class="form-control" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="txtvthickness">Thickness</label>
                                            <input id="txtvthickness" name="txtvthickness" type="text" placeholder="Thickness" class="form-control" readonly>
                                        </div>

                                        <div class="form-group">
                                            <br>
                                            <button id="btnsavevisual" name="btnsavevisual" class="btn btn-flat btn-info" style="float: right;"><i class="fa fa-save"></i> Save Information</button>
                                        </div>

                                    </div>
                                   
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>