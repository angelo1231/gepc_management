<!-- Middle Modal -->
<style>
.modal {
   text-align: center;
   padding: 0!important;
}
    
.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}
    
.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.ui-autocomplete  {
    z-index: 1100 !important;
}

</style>   

<!-- Modal -->
<div class="modal fade" id="modalaudit"  role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  <label id="lblinformation">{ DATA HERE }</label> 
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="col-md-4">

                                <div class="form-group">
                                    <label for="txtdatefrom">Date From</label>
                                    <input id="txtdatefrom" name="txtdatefrom" type="text" class="form-control" placeholder="Date From" style="width: 250px;">
        
                                </div>
                      
                        </div>
        
                        <div class="col-md-4">
        
                             <div class="form-group">
                                        
                                <label for="txtdateto">Date To</label>
                                <input id="txtdateto" name="txtdateto" type="text" class="form-control" placeholder="Date To" style="width: 250px;">
                                 
                            </div>
                                  
                        </div>
                        <div class="col-md-4">
        
                             <div class="form-group">
                                
                                <br>
                                <button id="btngenerate" name="btngenerate" class="btn btn-success" style="margin-top: 5px;">Generate</button>
                                 
                            </div>
                                  
                        </div>
                        <div class="col-md-12" id="content" style="margin-top: 10px;">
                        </div>
                    </div>
          
                </div>
                
                <div class="modal-footer">
                </div>

            </div>
        </div>
       
</div>
    