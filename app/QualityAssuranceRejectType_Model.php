<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class QualityAssuranceRejectType_Model extends Model
{
    
    public static function LoadRejectType(){

        $result = DB::table('qarejecttype')
        ->select(
            'qarejecttype.id',
            'qarejecttype.rejectcode',
            'qarejecttype.reject',
            DB::raw("CONCAT(users.lastname, ', ', users.firstname, ' ', users.mi) AS 'createdby'"),
            'qarejecttype.created_at'
        )
        ->join('users', 'users.id', '=', 'qarejecttype.createdby')
        ->get();

        return $result;

    }

    public static function ValRejectType($rejecttype){

        $result = DB::table('qarejecttype')
        ->select(
            DB::raw("COUNT(*) AS 'rejecttypecount'")
        )
        ->where('reject', '=', $rejecttype)
        ->first();

        if($result->rejecttypecount!=0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SaveRejectType($rejecttype, $rejectcode, $userid){

        DB::table('qarejecttype')
        ->insert([
            "rejectcode"=>$rejectcode,
            "reject"=>$rejecttype,
            "createdby"=>$userid,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadRejectTypeInfo($id){

        $result = DB::table('qarejecttype')
        ->select(
            'rejectcode',
            'reject'
        )
        ->where('id', '=', $id)
        ->first();

        return $result;

    }

    public static function UpdateRejectType($id, $rejecttype, $rejectcode){

        DB::table('qarejecttype')
        ->where('id', '=', $id)
        ->update([
            "rejectcode"=>$rejectcode,
            "reject"=>$rejecttype,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

}
