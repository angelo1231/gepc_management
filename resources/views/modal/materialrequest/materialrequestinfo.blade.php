<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modalinformation" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label id="lblinfo"><strong></strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <table class="table table-bordered" style="width: 100%;" id="tblmrinfo">
                <thead>
                  <tr>
                    <th>Raw Material</th>
                    <th>Quantity</th>
                    <th>Remarks</th>
                    <th>Excess Board</th>
                    <th>Qty Excess Board</th>
                    <th>Qty Issued</th>
                    <th>Balance</th>
                    <th>Issued By</th>
                  </tr>
                </thead>
                <tbody id="tblmrinfocontent">
                </tbody>
              </table>
                    {{-- <div class="form-group">
                            <label for="txtiqtyissued">Quantity Issued</label>
                            <input id="txtiqtyissued" name="txtiqtyissued" type="text" class="form-control" placeholder="Quantity Issued" readonly>
                    </div>
                    <div class="form-group">
                            <label for="txtibalance">Balance</label>
                            <input id="txtibalance" name="txtibalance" type="number" class="form-control" placeholder="Balance" readonly>
                    </div>
                    <div class="form-group">
                            <label for="txtirmissuedby">Issued By</label>
                            <input id="txtirmissuedby" name="txtirmissuedby" type="text" class="form-control" placeholder="Issued By" readonly>
                    </div> --}}

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    {{-- <button id="btnsave" name="btnsave" type="button" class="btn btn-primary btn-block btn-flat">Save</button> --}}
                </div>
            </div>
        </div>
    </div>
</div>