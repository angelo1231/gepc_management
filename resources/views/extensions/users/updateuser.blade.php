@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Update User Information</b></h3>
          <a href="{{ url('/user') }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#userinfo">User Information</a></li>
                </ul>
                  
                <div class="tab-content">
                    {{-- User Information --}}
                    <div id="userinfo" class="tab-pane fade in active">
                      
                        <div class="col-md-12 row">
                            <div class="col-md-6">
                                <br>
                                <div class="form-group">
                                    <label for="txtfirstname">Firstname</label>
                                    <input id="txtfirstname" name="txtfirstname" type="text" class="form-control" placeholder="Firstname">
                                </div>
                                <div class="form-group">
                                    <label for="txtmi">MI (Optional)</label>
                                    <input id="txtmi" name="txtmi" type="text" class="form-control" placeholder="Middle Initial">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <br>
                                <div class="form-group">
                                    <label for="txtlastname">Lastname</label>
                                    <input id="txtlastname" name="txtlastname" type="text" class="form-control" placeholder="Lastname">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="txtusername">Username</label>
                                    <input id="txtusername" name="txtusername" type="text" class="form-control" placeholder="Username" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="cmbusertype">Usertype</label>
                                    <select class="form-control" id="cmbusertype">
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="txtpassword">Password</label>
                                    <input id="txtpassword" name="txtpassword" type="password" class="form-control" placeholder="Password" readonly>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 row">
                            <button id="btnupdateuser" name="btnupdateuser" class="btn btn-flat btn-success" style="float: right;">Update Information</button>
                        </div>


                    </div>

                </div>

            </div>


        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var uid = '{{ $uid }}';

    $(document).ready(function(){

        //Set
        LoadSelect2();
        LoadUserGroup();
        SetUserInformation();

    });
    
    $('#btnupdateuser').on('click', function(){

        UpdateUserInformation();

    });

    function UpdateUserInformation(){

        var firstname = $('#txtfirstname').val();
        var lastname = $('#txtlastname').val();
        var mi = $('#txtmi').val();
        var usertype = $('#cmbusertype').val();

        if(firstname==""){
            toastr.error('Please input the firstname.', '', { positionClass: 'toast-top-center' });
        }
        else if(lastname==""){
            toastr.error('Please input the lastname.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/user/updateprofile") }}',
                type: 'post',
                data: {
                    uid: uid,
                    firstname: firstname,
                    lastname: lastname,
                    mi: mi,
                    usertype: usertype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        window.location = '{{ url("/user") }}';

                    }

                }
            });

        }

    }

    function SetUserInformation(){

        $('#txtfirstname').val("{{ $userinfo[0]->firstname }}");
        $('#txtlastname').val("{{ $userinfo[0]->lastname }}");
        $('#txtmi').val("{{ $userinfo[0]->mi }}");
        $('#txtusername').val("{{ $userinfo[0]->username }}");
        $('#txtpassword').val("{{ $userinfo[0]->password }}");
    }

    function LoadUserGroup(){

        $.ajax({
            url: '{{ url("api/user/loadusergroup") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbusertype').find('option').remove();
                $('#cmbusertype').append('<option value="">Please select a usertype</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbusertype').append('<option value="'+ response.data[i]["ugroupid"] +'">'+ response.data[i]["usergroup"] +'</option>');
                }

                $('#cmbusertype').val("{{ $userinfo[0]->usertype }}").trigger("change");

            }
        });

    }

    function LoadSelect2(){

        $('#cmbusertype').select2({
            theme: 'bootstrap'
        });

    }

  </script>  

@endsection