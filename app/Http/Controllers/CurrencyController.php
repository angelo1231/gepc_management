<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Seassion;
use App\Currency_Model;
use Illuminate\Support\Collection;
use DataTables;
use App\Http\Controllers\UserController;


class CurrencyController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
   
	function index(){

        $access = UserController::UserGroupAccess();

		return view('currency')
        ->with('access', $access);

	}

	function CurrencyInformation(){

		$currency = Currency_Model::CurrencyInformation();
        $data = array();
        for($i=0;$i<count($currency);$i++){
            $obj = new \stdClass;
            $obj->currency = $currency[$i]->currency;
            $obj->rate = $currency[$i]->rate;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $currency[$i]->currencyid .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);
		
	}

	function NewCurrency(Request $request){

		$currency_count = Currency_Model::ValidateCurrency($request->currency);

		if($currency_count==0){

			Currency_Model::NewCurrency($request);
			return json_encode(["success"=>true, "message"=>"Currency has been added."]);

		}
		else{

			return json_encode(["success"=>false, "message"=>"Currency already exist."]);

		}

	}

	function CurrencyProfile(Request $request){

		$data = Currency_Model::CurrencyProfile($request->id);

		return json_encode(["currency"=>$data[0]->currency, "rate"=>$data[0]->rate]);

	}

	function UpdateCurrency(Request $request){

		Currency_Model::UpdateCurrency($request);
		return json_encode(["message"=>"Currency information has been update."]);

	}

}
