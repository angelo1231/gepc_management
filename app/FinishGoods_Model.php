<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class FinishGoods_Model extends Model
{
    
    public static function LoadCustomers(){

        $result = DB::table('customers')
        ->select('cid', 'customer')
        ->get();

        return $result;

    }

    public static function LoadProcess(){

        $result = DB::table('processcode')
        ->select(
            DB::raw("DISTINCT processcode")
        )
        ->get();

        return $result;
        
    }

    public static function LoadCurrency(){

        $result = DB::table('currency')
        ->select('currencyid', 'currency')
        ->get();

        return $result;

    }

    public static function FinishGoodsInformation($cid){

        $result = DB::table('finishgoods')
        ->select(
            'finishgoods.fgid',
            'finishgoods.itemcode',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'finishgoods.ID_length',
            'finishgoods.ID_width',
            'finishgoods.ID_height',
            'finishgoods.OD_length',
            'finishgoods.OD_width',
            'finishgoods.OD_height',
            'finishgoods.price',
            'finishgoods.processcode',
            'currency.currency',
            'finishgoods.packingstd'
        )
        ->leftJoin('currency', 'currency.currencyid', '=', 'finishgoods.currencyid')
        ->where('finishgoods.cid', '=', $cid)
        ->get();

        return $result;

    }

    public static function FGNewItem($data){

       $result = DB::table('finishgoods')
        ->insertGetId([
            "itemcode"=>$data->itemcode,
            "partnumber"=>$data->partnumber,
            "partname"=>$data->partname, 
            "description"=> $data->description, 
            "ID_length"=>$data->idlength,
            "ID_width"=>$data->idwidth,
            "ID_height"=>$data->idheight,
            "OD_length"=>$data->odlength ,
            "OD_width"=>$data->odwidth,
            "OD_height"=>$data->odheight,
            "currencyid"=> $data->currencyid,
            "price"=> $data->price,
            "stockonhand"=>0,
            "packingstd"=>$data->packingstd,
            "cid"=>$data->cid,
            "processcode"=>$data->processcode,
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function FinishGoodsProfile($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'fgid',
            'itemcode',
            'partnumber',
            'partname',
            'description',
            'ID_length',
            'ID_width',
            'ID_height',
            'OD_length',
            'OD_width',
            'OD_height',
            'price',
            'processcode',
            'currencyid',
            'cid',
            'packingstd',
            'attachment'
        )
        ->where('finishgoods.fgid', '=', $fgid)
        ->get();

        return $result;

    }

    public static function FGUpdateItem($data){

        DB::table('finishgoods')
        ->where('fgid', '=', $data->fgid)
        ->update([
            "itemcode"=>$data->itemcode,
            "partnumber"=>$data->partnumber,
            "partname"=>$data->partname, 
            "description"=> $data->description, 
            "ID_length"=>$data->idlength,
            "ID_width"=>$data->idwidth,
            "ID_height"=>$data->idheight,
            "OD_length"=>$data->odlength ,
            "OD_width"=>$data->odwidth,
            "OD_height"=>$data->odheight,
            "currencyid"=> $data->currencyid,
            "price"=> $data->price,
            "cid"=>$data->cid,
            "processcode"=>$data->processcode,
            "packingstd"=>$data->packingstd
        ]);
        
    }

    public static function LoadFinsihGoodsInformation($customer){

        $result = DB::table('finishgoods')
        ->select(
            'finishgoods.fgid',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'finishgoods.ID_length',
            'finishgoods.ID_width',
            'finishgoods.ID_height',
            'finishgoods.OD_length',
            'finishgoods.OD_width',
            'finishgoods.OD_height',
            'finishgoods.packingstd',
            'currency.currency',
            'finishgoods.price',
            'finishgoods.stockonhand',
            'customers.customer'
        )
        ->leftjoin('currency', 'currency.currencyid', '=', 'finishgoods.currencyid')
        ->join('customers', 'customers.cid', '=', 'finishgoods.cid')
        ->where('finishgoods.cid', '=', $customer)
        ->get();
    
        return $result;

    }

    public static function LoadJobOrders(){

        $result = DB::table('joborders')
        ->select(
            'joid',
            'jonumber'
        )
        ->where('status', '=', 'Open')
        ->get();

        return $result;

    }

    public static function LoadJobOrderInformation($mstid){

        $result = DB::table('qajoinformationitems')
        ->select(
            'joborders.jonumber',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            DB::raw("CONCAT('W:', finishgoods.ID_width, ' L:', finishgoods.ID_length, ' H:', finishgoods.ID_height) AS 'id_size'"),
            DB::raw("CONCAT('W:', finishgoods.OD_width, ' L:', finishgoods.OD_length, ' H:', finishgoods.OD_height) AS 'od_size'"),
            DB::raw("GROUP_CONCAT(IF(rawmaterials.widthsize IS NULL, rawmaterials.width, CONCAT(rawmaterials.width, '.', rawmaterials.widthsize))) AS 'width'"),
            DB::raw("GROUP_CONCAT(IF(rawmaterials.lengthsize IS NULL, rawmaterials.length, CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize))) AS 'length'"),
            DB::raw("GROUP_CONCAT(rawmaterials.flute) AS 'flute'"),
            DB::raw("GROUP_CONCAT(rawmaterials.boardpound) AS 'boardpound'"),
            'joborders.targetoutput',
            'joborders.qtyreceive',
            'qajoinformationitems.qty'
        )
        ->join('qajoinformation', 'qajoinformation.qaid', '=', 'qajoinformationitems.qaid')
        ->join('joborders', 'joborders.joid', '=', 'qajoinformation.joid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join('materialrequest', 'materialrequest.requestid', '=', 'joborders.requestid')
        ->join('materialrequestitems', 'materialrequestitems.mrid', '=', 'joborders.requestid')
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'materialrequestitems.rmid')
        ->where('qajoinformationitems.itemid', '=', $mstid)
        ->get();

        return $result;

    }

    public static function GetFGID($joid){

        $result = DB::table('joborders')
        ->select(
            'finishgoods.fgid'
        )
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('joborders.joid', '=', $joid)
        ->get();

        return $result[0]->fgid;

    }

    public static function GetFGStockOnHand($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'stockonhand'
        )
        ->where('fgid', '=', $fgid)
        ->get();

        return $result[0]->stockonhand;

    }

    public static function GetReceiveQuantity($joid){

        $result = DB::table('joborders')
        ->select(
            'qtyreceive'
        )
        ->where('joid', '=', $joid)
        ->get();

        return $result[0]->qtyreceive;

    }

    public static function UpdateFGStockOnHand($fgid, $stockonhand){

        DB::table('finishgoods')
        ->where('fgid', '=', $fgid)
        ->update([
            "stockonhand"=>$stockonhand,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function UpdateJOQuantityReceive($joid, $receiveqty){

        DB::table('joborders')
        ->where('joid', '=', $joid)
        ->update([
            "qtyreceive"=>$receiveqty,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function SaveFGAudit($fgid, $joid, $qty, $stockonhand, $issuedby){

        DB::table('finishgoodsaudit')
        ->insert([
            "fgid"=>$fgid,
            "jonumber"=>DB::raw("(SELECT jonumber FROM joborders WHERE joid='".$joid."')"),
            "qty"=>$qty,
            "balance"=>$stockonhand,
            "issuedby"=>$issuedby,
            "issueddate"=>DB::raw("NOW()"),
            "issuedtime"=>DB::raw("CURTIME()")
        ]);

    }

    public static function LoadFGInfo($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'partnumber',
            'partname',
            'description'
        )
        ->where('fgid', '=', $fgid)
        ->first();

        return $result;

    }

    public static function GenerateAudit($data){

        $result = DB::table('finishgoodsaudit')
        ->select(
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            DB::raw("CONCAT('W: ', finishgoods.ID_width, ' L: ', finishgoods.ID_length, ' H: ', finishgoods.ID_height) AS 'idsize'"),
            DB::raw("CONCAT('W: ', finishgoods.OD_width, ' L: ', finishgoods.OD_length, ' H: ', finishgoods.OD_height) AS 'odsize'"),
            'finishgoodsaudit.jonumber',
            'finishgoodsaudit.drnumber',
            'finishgoodsaudit.qty',
            'finishgoodsaudit.balance',
            'finishgoodsaudit.issuedby',
            'finishgoodsaudit.issueddate',
            DB::raw("TIME_FORMAT(finishgoodsaudit.issuedtime, '%h:%i:%s %p') AS 'issuedtime'")
        )
        ->leftjoin('finishgoods', 'finishgoods.fgid', '=', 'finishgoodsaudit.fgid')
        ->whereBetween(DB::raw("DATE_FORMAT(finishgoodsaudit.issueddate, '%Y-%m-%d')"), [$data->datefrom, $data->dateto])
        ->get();

        return $result;

    }

    public static function LoadFGAuditInformation($data){

        $result = DB::table('finishgoodsaudit')
        ->select(
            'jonumber',
            'drnumber',
            'qty',
            'balance',
            'issuedby',
            'issueddate',
            DB::raw("TIME_FORMAT(issuedtime, '%h:%i:%s %p') AS 'issuedtime'")
        )
        ->where('fgid', '=', $data->fgid)
        ->whereBetween(DB::raw("DATE_FORMAT(issueddate, '%Y-%m-%d')"), [$data->from, $data->to])
        ->get();

        return $result;

    }

    public static function LoadFGJO($fgid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'joborders.joid', 
            'joborders.jonumber'
        )
        ->join('joborders', 'joborders.itemid', '=', 'pocustomerinformationitems.itemid')
        ->where('pocustomerinformationitems.fgid', '=', $fgid)
        ->get();

        return $result;

    }

    public static function LoadMSTNumbers(){

        $result = DB::table('qajoinformationitems')
        ->select(
            'qajoinformationitems.itemid',
            'qajoinformationitems.mstnumber'    
        )
        ->where('status', '=', 'Inventory')
        ->get();

        return $result;

    }

    public static function GetJOID($jonumber){

        $result = DB::table('joborders')
        ->select(
            'joid'
        )
        ->where('jonumber', '=', $jonumber)
        ->first();

        return $result->joid;

    }

    public static function UpdateMSTStatus($id){

        DB::table('qajoinformationitems')
        ->where('itemid', '=', $id)
        ->update([
            'status'=>"Done"
        ]);

    }

    public static function LoadDRNumbers(){

        $result = DB::table('deliveryinformation')
        ->select(
            'did',
            'drnumber'
        )
        ->where('status', '=', 'Open')
        ->get();

        return $result;

    }

    public static function LoadDRItems($data){

        $result = DB::table('deliveryinformationitems')
        ->select(
            'pocustomerinformation.ponumber',
            'deliveryinformationitems.pocusitemid',
            DB::raw("CONCAT(finishgoods.partnumber, ' ', finishgoods.partname, ' ', finishgoods.description) AS 'itemdescription'"),
            DB::raw("CONCAT('W: ', finishgoods.ID_width, ' L: ', finishgoods.ID_length, ' H: ', finishgoods.ID_height) AS 'idsize'"),
            DB::raw("CONCAT('W: ', finishgoods.OD_width, ' L: ', finishgoods.OD_length, ' H: ', finishgoods.OD_height) AS 'odsize'"),
            'deliveryinformationitems.qty'
        )
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'deliveryinformationitems.poid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'deliveryinformationitems.fgid')
        ->where('deliveryinformationitems.did', '=', $data->did)
        ->get();

        return $result;

    }

    public static function GetFGIDPOCustomer($itemid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'fgid'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        return $result->fgid;

    }

    public static function GetPOItemCustomerDelQty($itemid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'delqty'
        )
        ->where('itemid', '=', $itemid)
        ->first();

        return $result->delqty;

    }

    public static function UpdateCustomerPOItems($itemid, $delqty){

        DB::table('pocustomerinformationitems')
        ->where('itemid', '=', $itemid)
        ->update([
            "delqty"=>$delqty
        ]);

    }

    public static function SaveFGAuditDR($fgid, $did, $qty, $stockonhand, $issuedby){

        DB::table('finishgoodsaudit')
        ->insert([
            "fgid"=>$fgid,
            "drnumber"=>DB::raw("(SELECT drnumber FROM deliveryinformation WHERE did='".$did."')"),
            "qty"=>$qty,
            "balance"=>$stockonhand,
            "issuedby"=>$issuedby,
            "issueddate"=>DB::raw("NOW()"),
            "issuedtime"=>DB::raw("CURTIME()")
        ]);

    }

    public static function UpdateDeliveryInformation($did){

        DB::table('deliveryinformation')
        ->where('did', '=', $did)
        ->update([
            "status"=>"Close",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadCodeProcess($data){

        $result = DB::table('processcode')
        ->select(
            'process.process'
        )
        ->join('process', 'process.pid', '=', 'processcode.processid')
        ->where('processcode.processcode', '=', $data->processcode)
        ->orderBy('processcode.processorder', 'ASC')
        ->get();

        return $result;

    }

    public static function GetProcessType($processcode){

        // SQL Statement
        // SELECT 
        // processcode.processcode,
        // MAX(IFNULL(processstyle.style, 'None')) AS 'style',
        // MAX(IFNULL(processstyle.inkcolor, 'None')) AS 'inkcolor'
        // FROM processcode
        // LEFT JOIN processstyle ON processstyle.processcode=processcode.processcode 
        // WHERE processcode.processcode='A' 
        // GROUP BY processcode.processcode;

        $result = DB::table('processcode')
        ->select(
            'processcode.processcode',
            DB::raw("MAX(IFNULL(processstyle.style, 'None')) AS 'style'"),
            DB::raw("MAX(IFNULL(processstyle.inkcolor, 'None')) AS 'inkcolor'")
        )
        ->leftjoin('processstyle', 'processstyle.processcode', '=', 'processcode.processcode')
        ->where('processcode.processcode', '=', $processcode)
        ->groupBy('processcode.processcode')
        ->get();

        return $result;

    }

    public static function ImportFinishGoods($cid, $itemcode, $partnumber, $partname, $description, $id_length, $id_width, $id_height, $od_length, $od_width, $od_height, $currency, $price, $packingstd, $processcode){

        DB::table('finishgoods')
        ->insert([
            "itemcode"=>$itemcode,
            "partnumber"=>$partnumber,
            "partname"=>$partname,
            "description"=>$description,
            "ID_length"=>$id_length,
            "ID_width"=>$id_width,
            "ID_height"=>$id_height,
            "OD_length"=>$od_length,
            "OD_width"=>$od_width,
            "OD_height"=>$od_height,
            "currencyid"=>DB::raw("(SELECT currencyid FROM currency WHERE currency='". $currency ."')"),
            "price"=>$price,
            "packingstd"=>$packingstd,
            "processcode"=>$processcode,
            "cid"=>$cid,
            "stockonhand"=>"0"
        ]);

    }

    public static function UpdateFGAttachment($fgid, $filename){

        DB::table('finishgoods')
        ->where('fgid', '=', $fgid)
        ->update([
            "attachment"=>$filename
        ]);

    }

    public static function LoadPreparationSlipInformation(){

        $result = DB::table('deliverypreparationsinformation')
        ->select(
            'deliverypreparationsinformation.preparationid',
            'deliverypreparationsinformation.preparationnumber',
            DB::raw("(SELECT SUM(qty) FROM deliverypreparationsinformationitems WHERE preparationid=deliverypreparationsinformation.preparationid) AS 'totalqty'"),
            'deliverypreparationsinformation.issuedby',
            DB::raw("DATE_FORMAT(deliverypreparationsinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
            'customers.customer'
        )
        ->join('customers', 'customers.cid', '=', 'deliverypreparationsinformation.cid')
        ->where('deliverypreparationsinformation.status', '=', 'Open')
        ->get();

        return $result;

    }

    public static function LoadPreparationSlipItems($preparationid){

        $result = DB::table('deliverypreparationsinformationitems')
        ->select(
            'deliverypreparationsinformationitems.itemid',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            DB::raw("CONCAT('W: ', finishgoods.ID_width, ' L: ', finishgoods.ID_length, ' H: ', finishgoods.ID_height) AS 'idsize'"),
            DB::raw("CONCAT('W: ', finishgoods.OD_width, ' L: ', finishgoods.OD_length, ' H: ', finishgoods.OD_height) AS 'odsize'"),
            'deliverypreparationsinformationitems.qty'
        )
        ->join('finishgoods', 'finishgoods.fgid', '=', 'deliverypreparationsinformationitems.fgid')
        ->where('deliverypreparationsinformationitems.preparationid', '=', $preparationid)
        ->get();

        return $result;

    }

    public static function GetPreparationSlipNumber($preparationid){

        $result = DB::table('deliverypreparationsinformation')
        ->select(
            'preparationnumber'
        )
        ->where('preparationid', '=', $preparationid)
        ->first();

        return $result->preparationnumber;

    }

    public static function ApprovePreparationSlip($preparationid, $issuedby){

        DB::table('deliverypreparationsinformation')
        ->where('preparationid', '=', $preparationid)
        ->update([
            "approvedby"=>$issuedby,
            "approveddate"=>DB::raw("NOW()"),
            "status"=>"Approved"
        ]);

    }

    public static function DisapprovePreparationSlip($preparationid, $issuedby, $remarks){

        DB::table('deliverypreparationsinformation')
        ->where('preparationid', '=', $preparationid)
        ->update([
            "disapprovedby"=>$issuedby,
            "disapproveddate"=>DB::raw("NOW()"),
            "status"=>"Disapproved",
            "remarks"=>$remarks
        ]);        

    }

    public static function LoadCountPS(){

        $result = DB::table('deliverypreparationsinformation')
        ->select(
            DB::raw("COUNT(*) AS 'pscount'")
        )
        ->where('status', '=', 'Open')
        ->first();

        return $result->pscount;

    }

    public static function LoadFGStockOnHand($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'description',
            'stockonhand'
        )
        ->where('fgid', '=', $fgid)
        ->first();

        return $result;

    }

    public static function SaveStockOnHandInformation($fgid, $qty){

        DB::table('finishgoods')
        ->where('fgid', '=', $fgid)
        ->update([
            "stockonhand"=>$qty,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function SaveFGAuditStock($fgid, $qty, $remarks, $issuedby){

        DB::table('finishgoodsaudit')
        ->insert([
            "fgid"=>$fgid,
            "qty"=>$qty,
            "balance"=>$qty,
            "issuedby"=>$issuedby,
            "remarks"=>$remarks,
            "issueddate"=>DB::raw("NOW()"),
            "issuedtime"=>DB::raw("CURTIME()")
        ]);

    }

}
