<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usersrestrictions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersrestrictions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid');
            $table->integer('purchasing');
            $table->integer('planning');
            $table->integer('rawmaterial');
            $table->integer('finishgoods');
            $table->integer('sales');
            $table->integer('qualityassurance');
            $table->integer('production');
            $table->integer('accounting');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersrestrictions');        
    }
}
