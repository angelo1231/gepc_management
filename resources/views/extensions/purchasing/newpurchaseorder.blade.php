@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> New Purchase Order</b></h3>
          <a href="{{ url("/purchasing") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-12">
                
                <article class="col-md-6">
                    <div class="form-group">
                        
                            <label for="txtponumber">PO #</label>
                            <input id="txtponumber" name="txtponumber" type="text" class="form-control" placeholder="Purchase Order Number" style="width: 400px;" readonly>
                     
                    </div>
                    <div class="form-group">
                        
                        <label for="cmbsupplier">Supplier</label>
                        <br>
                        <select class="form-control" id="cmbsupplier" style="width: 400px;">
                        </select>

                    </div>
                    {{-- <div class="form-group">
                        
                            <label for="txtaddress">Address</label>
                            <input id="txtaddress" name="txtaddress" type="text" class="form-control" placeholder="Address">
                     
                    </div> --}}
                    <div class="form-group">
                        
                        <label for="txtattn">Attention To</label>
                        <input id="txtattn" name="txtattn" type="text" class="form-control" placeholder="Attn">
                 
                    </div>
                    <div class="form-group">
                        
                            <label for="txtdelto">Deliver To</label>
                            <input id="txtdelto" name="txtdelto" type="text" class="form-control" placeholder="Deliver To">
        
                    </div>
                </article>
                <article class="col-md-6">
                    <div class="form-group">
                        
                        <br>
                        <br>
                        <br>
    
                    </div>  
                    <div class="form-group">
                        
                        <label for="txtdelto">Delivery Date</label>
                        <input id="txtdeldate" name="txtdeldate" type="text" class="form-control" placeholder="Delivery Date" style="width: 320px;">
    
                    </div>  
                    <div class="form-group">
                        
                            <label for="txttin">TIN</label>
                            <input id="txttin" name="txttin" type="text" class="form-control" placeholder="TIN" style="width: 320px;">
        
                    </div>
                    <div class="form-group">
                        
                            <label for="txtterms">Terms</label>
                            <input id="txtterms" name="txtterms" type="text" class="form-control" placeholder="Terms" style="width: 320px;">
        
                    </div>

                </article>

                <article class="col-md-12">
                    <div class="form-group">
                        <label for="txtnote">Note</label>
                        <textarea id="txtnote" name="txtnote" type="text" rows="3" class="form-control" placeholder="Note"></textarea>
                    </div>
                </article>

                <article class="col-md-12">
                    <table id="tblitems" class="table" style="margin-top: 10px;">
                        <tr>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Flute</th>
                            <th>Board Pound</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th><button id="btnadd" name="btnadd" type="button" class="btn btn-primary btn-flat" style="float: right; margin-left: 5px;" disabled><i class="fa fa-plus"></i></button></th>
                        </tr>
                        <tr>
                            <td><select class="form-control" id="cmbitem1" name="cmbitem[]" style="width: 260px;"></select></td>
                            <td><input id="txtdescription1" name="txtdescription[]" type="text" class="form-control" placeholder="Description" readonly></td>
                            <td><input id="txtflute1" name="txtflute[]" type="text" class="form-control" placeholder="Size" readonly></td>
                            <td><input id="txtboardpound1" name="txtboardpound[]" type="text" class="form-control" placeholder="Board Pound" readonly></td>
                            <td><input id="txtprice1" name="txtprice[]" type="text" class="form-control" placeholder="Price"></td>
                            <td><input id="txtqty1" name="txtqty[]" type="number" class="form-control" placeholder="Quantity" readonly></td>
                            <td><input id="txttotal1" name="txttotal[]" type="number" class="form-control" placeholder="Total" readonly></td>
                            <td></td>
                        </tr>
                    </table>
                       
                    <label for="txtdelprice" style="float: left; margin-top: 5px; margin-left: 10px;">Delivery Charge </label>
                    <input id="txtdelprice" name="txtdelprice" type="number" class="form-control" placeholder="Terms" value="0" style="width: 150px; float: left; margin-left: 10px;">
        
                    <label id="lblgrandtotal" style="float: right; margin-right: 72px;">
                        Grand Total: 0
                    </label>
                </article>    
                   
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">

            <button id="btnsavepo" name="btnsavepo" class="btn btn-primary btn-flat" style="float:right;"><i class="fa fa-floppy-o" style="font-size: 12px;"></i><strong> Save Information</strong></button>
                
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var itemcount = 1;
    var itemarray = new Array();
    var table;
    var grandtotal = 0;

    $(document).ready(function (){

        var table = $('#tblitems');
        $("#txtdeldate").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        $('#cmbsupplier').select2({
            theme: "bootstrap"
        });
        $('#cmbitem1').select2({
            theme: "bootstrap"
        });

        LoadSupplier($('#cmbsupplier').attr('id'));
        GeneratePONumber();

    });

    $('#btnadd').on('click', function(){

         itemcount += 1;
         LoadSize();
         itemarray.push(itemcount);     
         $('#tblitems').append('<tr id="item'+itemcount+'"><td><select class="form-control" id="cmbitem'+itemcount+'" name="cmbitem[]" style="width: 260px;"></select></td><td><input id="txtdescription'+itemcount+'" name="txtdescription[]" type="text" class="form-control" placeholder="Description" readonly></td><td><input id="txtflute'+itemcount+'" name="txtflute[]" type="text" class="form-control" placeholder="Size" readonly></td><td><input id="txtboardpound'+itemcount+'" name="txtboardpound[]" type="text" class="form-control" placeholder="Board Pound" readonly></td><td><input id="txtprice'+itemcount+'" name="txtprice[]" type="text" class="form-control" placeholder="Price"></td><td><input id="txtqty'+itemcount+'" name="txtqty[]" type="number" class="form-control" placeholder="Quantity" readonly></td><td><input id="txttotal'+itemcount+'" name="txttotal[]" type="number" class="form-control" placeholder="Total" readonly></td><td><button id="btnremove" name="btnremove" class="btn btn-info btn-flat" value="'+ itemcount +'"><i class="fa fa-trash"></i></button></td></tr>');     

    });

    $(document).on('click', '#btnremove', function(){

        var val = $(this).val();
        var position = $.inArray(parseInt(val), itemarray);

        if(~position){
            itemarray.splice(position, 1);
            document.getElementById("item"+val).remove();
            GrandTotal();
        }

    });

    function LoadSupplier(id){

     $.ajax({
        url: '{{ url("api/purchasing/loadsupplier") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          $('#'+id).find('option').remove();
          $('#'+id).append('<option value="" disabled selected>Select a Supplier</option>');
          for (var i = 0; i < response.data.length; i++) {
              $('#'+id).append('<option value="'+  response.data[i]["sid"] +'">'+  response.data[i]["supplier"] +'</option>');
          }

        }
      });

    }

    $('#cmbsupplier').on('change', function(){

        var id = $(this).val();
        $('#lblgrandtotal').text('Grand Total: 0');
        RemoveAll();

        $.ajax({
            url: '{{ url("api/purchasing/supplierprofile") }}',
            type: 'get',
            data: {id: id},
            dataType: 'json',
            success: function(response){

                $('#txttin').val(response.tin);
                $('#txtterms').val(response.terms);
                $('#txtattn').val(response.contactperson);
                LoadSize();
                $('#btnadd').prop('disabled', false);

            }
        });

    });

    function LoadSize(){

        $.ajax({
            url: '{{ url("api/purchasing/loaditem") }}',
            type: 'get',
            dataType: 'json',
            beforeSend: function(){
                $('#btnadd').prop('disabled', true);
            },
            success: function(response){

                $('#cmbitem'+itemcount).find('option').remove();
                $('#cmbitem'+itemcount).append('<option value="" disabled selected>Select a Item</option>');
                for (var i = 0; i < response.data.length; i++) {

                    var item = "";

                    if(response.data[i].category=="Raw Material"){

                        if(response.data[i]["widthsize"]!=null){
                            item += response.data[i]["width"] + "." + response.data[i]["widthsize"];
                        }
                        else{
                            item += response.data[i]["width"];
                        }

                        if(response.data[i]["lengthsize"]!=null){
                            item += " x " + response.data[i]["length"] + "." + response.data[i]["lengthsize"];
                        }
                        else{
                            item += " x " + response.data[i]["length"];
                        }

                    }
                    else if(response.data[i].category=="Other"){

                        item = response.data[i].name;

                    }
                    

                    $('#cmbitem'+itemcount).append('<option value="'+  response.data[i]["rmid"] +'">'+ item +'</option>');

                }

                $('#cmbitem'+itemcount).select2({
                    theme: "bootstrap"
                });

            },
            complete: function(){
                $('#btnadd').prop('disabled', false);
            }
        });

    }

    $(document).on('change', '[name="cmbitem[]"]', function(){

        var id = $(this).attr('id');
        var rmid = $(this).val();

        split_string_id = id.split(/(\d+)/);
        
        var data = $("[name='cmbitem[]']").serializeArray();
        var val = $(this).val();
        var count = 0;
        for(var i=0;i<data.length;i++){ //Validation
           if(data[i].value==val){
              count ++;
           }
        }

       if(count==1){

            $.ajax({
                url: '{{ url("api/purchasing/rawmaterialprofile") }}',
                type: 'get',
                data: {rmid: rmid},
                dataType: 'json',
                success: function(response){

                    $('#txtdescription'+split_string_id[1]).val(response.description);
                    $('#txtflute'+split_string_id[1]).val(response.flute);
                    $('#txtboardpound'+split_string_id[1]).val(response.boardpound);
                    $('#txtprice'+split_string_id[1]).val(response.price);
                    if(response.price!=null){
                        $('#txtqty'+split_string_id[1]).prop('readonly', false);
                        $('#txtqty'+split_string_id[1]).val('');
                        $('#txttotal'+split_string_id[1]).val('');
                        GrandTotal();
                    }
                    else{
                        $('#txtqty'+split_string_id[1]).prop('readonly', true);
                        $('#txtqty'+split_string_id[1]).val('');
                        $('#txttotal'+split_string_id[1]).val('');
                        GrandTotal();
                    }
                

                }
            });

       }
       else{

            toastr.error("You have already select this item please select another item.", '', { positionClass: 'toast-top-center' });
            $('#txtdescription'+split_string_id[1]).val('');
            $('#txtflute'+split_string_id[1]).val('');
            $('#txtboardpound'+split_string_id[1]).val('');
            $('#txtprice'+split_string_id[1]).val('');
            $('#txtqty'+split_string_id[1]).val('');
            $('#txttotal'+split_string_id[1]).val('');
            $('#txtqty'+split_string_id[1]).prop('readonly', true);
            GrandTotal();

       }

        
        
    });

    $(document).on('keyup', '[name="txtqty[]"]', function(){

        var id = $(this).attr("id");
        split_string_id = id.split(/(\d+)/);

        var rmid = $('#cmbitem'+split_string_id[1]).val();
        var price = $('#txtprice'+split_string_id[1]).val();
        var qty = $(this).val();

        Currency(rmid, price, qty);

    });

    $(document).on('change', '[name="txtqty[]"]', function(){

        var id = $(this).attr("id");
        split_string_id = id.split(/(\d+)/);

        var rmid = $('#cmbitem'+split_string_id[1]).val();
        var price = $('#txtprice'+split_string_id[1]).val();
        var qty = $(this).val();

        Currency(rmid, price, qty);

    });

    $('#btnsavepo').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this purchase order information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SavePOInformation();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });
        
    });

    function SavePOInformation(){

        //Validation
        var validateblank = false;
        var validatenegative = false;
        var sid = $('#cmbsupplier').val();
        var attn = $('#txtattn').val();
        var delto = $('#txtdelto').val();
        var deldate = $('#txtdeldate').val();
        var note = $('#txtnote').val();
        var datarmid = $('[name="cmbitem[]"]').serializeArray();
        var dataqty = $('[name="txtqty[]"]').serializeArray();
        var dataprice = $('[name="txtprice[]"]').serializeArray();
        var datatotal = $('[name="txttotal[]"]').serializeArray();
        var delcharge = $('#txtdelprice').val();

        for(var i=0;i<dataqty.length;i++){
            
            if(dataqty[i].value==""){
                validateblank = true;
                break;
            }

            if(dataqty[i].value<=0){
                validatenegative = true;
                break;
            }

        }

        if(sid==null){
            toastr.error("Please select a supplier.", '', { positionClass: 'toast-top-center' });
        }
        else if(attn==""){
            toastr.error("Please input the name of the recipient.", '', { positionClass: 'toast-top-center' });
        }
        else if(delto==""){
            toastr.error("Please input the address of the delivery.", '', { positionClass: 'toast-top-center' });
        }
        else if(deldate==""){
            toastr.error("Please select the delivery date.", '', { positionClass: 'toast-top-center' });
        }
        else if(delcharge < 0){
            toastr.error("Please input a non negative number in the delivery charge.", '', { positionClass: 'toast-top-center' });
        }
        else{

            if(!validateblank){

                if(!validatenegative){
                
                    $.ajax({
                        url: '{{ url("api/purchasing/savepoinfo") }}',
                        type: 'post',
                        data: {
                            sid: sid, 
                            attn: attn, 
                            delto: delto, 
                            delcharge: delcharge, 
                            deldate: deldate, 
                            rmid: datarmid, 
                            qty: dataqty, 
                            price: dataprice,
                            total: datatotal,
                            note: note
                        },
                        dataType: 'json',
                        beforeSend: function(){
                            H5_loading.show();
                        },
                        complete: function(){
                            H5_loading.hide();
                            window.location = "{{ url('/purchasing') }}";
                        }
                    });

                }
                else{
                    toastr.error("Please dont input negative numbers or equal to zero quantity.", '', { positionClass: 'toast-top-center' });
                }

            }
            else{

                toastr.error("Please dont leave any blank information.", '', { positionClass: 'toast-top-center' });

            }

        }

    }

    function Currency(rmid, price, qty){

        $.ajax({
            url: '{{ url("api/purchasing/currency") }}',
            type: 'get',
            data: {rmid: rmid, price: price, qty: qty},
            dataType: 'json',
            success: function(response){

                $('#txttotal'+split_string_id[1]).val(response.total);
                GrandTotal();
    
            }
        });

    }

    function GeneratePONumber(){

        $.ajax({
            url: '{{ url("api/purchasing/genponumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

               $('#txtponumber').val(response.batch);
                

            }
        });

    }

    function GrandTotal(){

        grandtotal = 0;
        var data  = document.getElementsByName("txttotal[]");
        for(var i = 0; i < data.length; i++){
           
           if(data[i].value!=""){
            grandtotal += parseFloat(data[i].value);
           }
           
        }

        grandtotal.toFixed(2);
        $('#lblgrandtotal').text('Grand Total: '+grandtotal.toString());

    }

    function RemoveAll(){

        for(var i = 0; i < itemarray.length; i++){

            document.getElementById("item"+itemarray[i].toString()).remove();

        }

        //Set
        itemcount = 1;
        itemarray = [];
        $('#txtdescription1').val('');
        $('#txtflute1').val('');
        $('#txtboardpound1').val('');
        $('#txtprice1').val('');
        $('#txtqty1').val('');
        $('#txttotal1').val('');
        $('#txtattn').val('');
        $('#txtdelto').val('');
        $('#txtdelprice').val('0');
        $('#txtdeldate').val('');

    }

  </script>  

@endsection