@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Production</b></h3>
          <a href="{{ url("/fginventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

          <div class="col-md-12">
            <h4><strong>Production Receive</strong></h4>
            <br>
            <div class="col-md-12 row"> 

              <table id="tblproduction" class="table">
                <thead>
                  <tr>
                    <th>MST #</th>
                    <th>Job Order #</th>
                    <th>Item Description</th>
                    <th>Material Description</th>
                    <th>Target Output</th>
                    <th>Quantity Receive</th>
                    <th>Quantity</th>
                    <th><button id="btnadditem" name="btnadditem" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button></th>
                  </tr>
                </thead>
                <tbody id="productionitem">
                  <tr>
                    <td><select class="form-control" id="cmbmst1" name="cmbmst[]" style="width: 150px;"></select></td>
                    <td><input class="form-control" id="txtjo1" name="txtjo[]" readonly></td>
                    <td><textarea id="txtitemdesc1" name="txtitemdesc[]" class="form-control" rows="3" style="width: 200px;" readonly></textarea></td>
                    <td><textarea id="txtmaterialdesc1" name="txtmaterialdesc[]" class="form-control" rows="3" style="width: 200px;" readonly></textarea></td>
                    <td><input id="txttargetoutput1" name="txttargetoutput[]" class="form-control" type="text" readonly></td>
                    <td><input id="txtreceiveqty1" name="txtreceiveqty[]" class="form-control" type="text" readonly></td>
                    <td><input id="txtqty1" name="txtqty[]" class="form-control" type="number" readonly></td>
                  </tr>
                </tbody>
              </table>   

            </div>      

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button id="btnsaveproduction" name="btnsaveproduction" class="btn btn-primary btn-flat" style="float:right;"><i class="fa fa-floppy-o" style="font-size: 12px;"></i><strong> Save Information</strong></button>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var itemcount = 1;
    var itemarray = new Array();

    $(document).ready(function(){

      LoadMSTNumber($('#cmbmst1').attr('id'));

    });

    $('#btnadditem').on('click', function(){

      itemcount += 1;
      itemarray.push(itemcount);
      $('#productionitem').append('<tr id="'+itemcount+'"><td><select class="form-control" id="cmbmst'+itemcount+'" name="cmbmst[]" style="width: 150px;"></select></td><td><input class="form-control" id="txtjo'+itemcount+'" name="txtjo[]" readonly></td><td><textarea id="txtitemdesc'+itemcount+'" name="txtitemdesc[]" class="form-control" rows="3" style="width: 200px;" readonly></textarea></td><td><textarea id="txtmaterialdesc'+itemcount+'" name="txtmaterialdesc[]" class="form-control" rows="3" style="width: 200px;" readonly></textarea></td><td><input id="txttargetoutput'+itemcount+'" name="txttargetoutput[]" class="form-control" type="text" readonly></td><td><input id="txtreceiveqty'+itemcount+'" name="txtreceiveqty[]" class="form-control" type="text" readonly></td><td><input id="txtqty'+itemcount+'" name="txtqty[]" class="form-control" type="number" readonly></td><td><button id="btnremoveitem" name="btnremoveitem" class="btn btn-flat btn-info" value="'+itemcount+'"><i class="fa fa-trash"></i></button></td></tr>');
      LoadMSTNumberAdd();

    });

    $(document).on('click', '#btnremoveitem', function(){

      var val = $(this).val();
      var position = $.inArray(parseInt(val), itemarray);

      if(~position){
          itemarray.splice(position, 1);
          $('#'+val).remove();
      }

    });

    $(document).on('change', '[name="cmbmst[]"]', function(){

      var id = $(this).attr('id');
      var mstid = $(this).val();

      split_string_id = id.split(/(\d+)/);

      var data = $("[name='cmbmst[]']").serializeArray();
      var count = 0;
      for(var i=0;i<data.length;i++){ //Validation
         if(data[i].value==mstid){
            count ++;
         }
      }

      if(count==1){

        $.ajax({
          url: '{{ url("api/finishgoodsinv/loadjoborderinformation") }}',
          type: 'get',
          data: {
            mstid: mstid
          },
          dataType: 'json',
          success: function(response){

            $('#txtjo'+split_string_id[1]).val(response.jonumber);
            $('#txtitemdesc'+split_string_id[1]).text(response.description + ' ID Size: ' + response.id_size + ' OD Size: ' + response.od_size);
            $('#txtmaterialdesc'+split_string_id[1]).text(response.rmdescription);
            $('#txttargetoutput'+split_string_id[1]).val(response.targetoutput);
            $('#txtreceiveqty'+split_string_id[1]).val(response.qtyreceive);
            $('#txtqty'+split_string_id[1]).prop('readonly', false);
            $('#txtqty'+split_string_id[1]).val(response.qty);

          }
        });

      }
      else{

        toastr.error("You have already select this MST number please select another MST number.", '', { positionClass: 'toast-top-center' });

        $('#txtjo'+split_string_id[1]).val('');
        $('#txtitemdesc'+split_string_id[1]).text('');
        $('#txtmaterialdesc'+split_string_id[1]).text('');
        $('#txttargetoutput'+split_string_id[1]).val('');
        $('#txtreceiveqty'+split_string_id[1]).val('');
        $('#txtqty'+split_string_id[1]).prop('readonly', true);
        $('#txtqty'+split_string_id[1]).val('');

      }

    });

    $('#btnsaveproduction').on('click', function(){

      $.confirm({
            title: 'Save',
            content: 'Save this production information?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){
                      
                      SaveProductionInformation();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                      
                    }
                } 
            }
        });

    });

    function SaveProductionInformation(){

      var validateblank = false;
      var validatenegative = false;
      var datamst = $('[name="cmbmst[]"]').serializeArray();
      var datajo = $('[name="txtjo[]"]').serializeArray();
      var dataqty = $('[name="txtqty[]"]').serializeArray();

      for(var i=0;i<dataqty.length;i++){
            
          if(dataqty[i].value==""){
              validateblank = true;
              break;
          }

          if(dataqty[i].value<=0){
              validatenegative = true;
              break;
          }

      }

      if(!validateblank){

        if(!validatenegative){

          $.ajax({
            url: '{{ url("api/finishgoodsinv/saveproductioninformation") }}',
            type: 'post',
            data: {
              datamst: datamst,
              datajo: datajo,
              dataqty: dataqty
            },
            dataType: 'json',
            beforeSend: function(){
              H5_loading.show();
            },
            complete: function(){

                H5_loading.hide();

                //Socket Emit
                socket.emit('fginvinformation');

                window.location = "{{ url('/fginventory') }}";
        
            }
          });

        }
        else{

          toastr.error("Please dont input negative numbers or equal to zero quantity.", '', { positionClass: 'toast-top-center' });

        }

      }
      else{

        toastr.error("Please dont leave any blank information.", '', { positionClass: 'toast-top-center' });

      }

    }

    function LoadMSTNumber(id){

      $.ajax({
        url: '{{ url("api/finishgoodsinv/loadmstnumbers") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          $('#'+id).find('option').remove();
          $('#'+id).append('<option value="" disabled selected>Select a MST #</option>');
          for(var i=0;i<response.data.length;i++){

            $('#'+id).append('<option value="'+  response.data[i]["itemid"] +'">'+  response.data[i]["mstnumber"] +'</option>');

          }

          $('#'+id).select2({
            theme: "bootstrap"
          });

        }
      });

    }

    function LoadMSTNumberAdd(){

      $.ajax({
        url: '{{ url("api/finishgoodsinv/loadmstnumbers") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          $('#cmbmst'+itemcount).find('option').remove();
          $('#cmbmst'+itemcount).append('<option value="" disabled selected>Select a MST #</option>');
          for(var i=0;i<response.data.length;i++){

            $('#cmbmst'+itemcount).append('<option value="'+  response.data[i]["itemid"] +'">'+  response.data[i]["mstnumber"] +'</option>');

          }

          $('#cmbmst'+itemcount).select2({
            theme: "bootstrap"
          });

        }
      });


    }

  </script>  

@endsection