<?php

namespace App\Imports\FinishGoods;

use App\FinishGoods_Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FinishGoodsImport implements ToModel, WithHeadingRow
{

    private $cid;

    public function __construct($cid)
    {
         $this->cid = $cid;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        FinishGoods_Model::ImportFinishGoods($this->cid, $row['itemcode'], $row['partnumber'], $row['partname'], $row['description'], $row['id_length'], $row['id_width'], $row['id_height'], $row['od_length'], $row['od_width'], $row['od_height'], $row['currency'], $row['price'], $row['packingstd'], $row['processcode']);

    }
}
