<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PaperCombination_Model;
use DataTables;
use Illuminate\Support\Collection;

class PaperCombinationController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    function index(){

        $access = UserController::UserGroupAccess();

        return view('papercombination')
        ->with('access', $access);

    }

    function LoadFlute(){

        $flute = PaperCombination_Model::LoadFlute();

        return json_encode([
            "data"=>$flute
        ]);

    }

    function LoadBoardPound(Request $request){

        $boardpound = PaperCombination_Model::LoadBoardPound($request->flute);

        return json_encode([
            "data"=>$boardpound
        ]);

    }

    function SavePaperCombination(Request $request){

        //Validation
        $validation = PaperCombination_Model::ValidatePaperCombination($request->flute, $request->boardpound, $request->rmpapercombination);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Paper combination information already exist."
            ]);

        }
        else{

            PaperCombination_Model::SavePaperCombination($request->flute, $request->boardpound, $request->rmpapercombination);

            return json_encode([
                "success"=>true,
                "message"=>"Paper combination information has been save."
            ]);

        }   

    }

    function LoadPaperCombination(){

        $papercombination = PaperCombination_Model::LoadPaperCombination();

        $data = array();
        for($i=0;$i<count($papercombination);$i++){
            
            $obj = new \stdClass;

            $obj->rmpapercombination = $papercombination[$i]->rmpapercombination;
            $obj->flute = $papercombination[$i]->flute;
            $obj->boardpound = $papercombination[$i]->boardpound;
            $obj->createdat = $papercombination[$i]->created_at;
            $obj->updatedat = $papercombination[$i]->updated_at;   
            $obj->panel = '<button id="btnupdate" name="btnupdate" class="btn btn-flat btn-primary" title="Edit RM Paper Combination" value="'. $papercombination[$i]->papercomid .'"><i class="fa fa-edit"></i></button>';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns([
            'panel'
        ])
        ->make(true);

    }

    function LoadPaperCombinationInfo(Request $request){

        $papercominfo = PaperCombination_Model::LoadPaperCombinationInfo($request->papercomid);

        return json_encode([
            "rmpapercombination"=>$papercominfo->rmpapercombination,
            "flute"=>$papercominfo->flute,
            "boardpound"=>$papercominfo->boardpound
        ]);

    }

    function UpdatePaperCombination(Request $request){

        $validation = PaperCombination_Model::ValidateUpdatePaperCombination($request->papercomid, $request->rmpapercombination, $request->flute, $request->boardpound);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Paper combination information already exist."
            ]);

        }
        else{

            PaperCombination_Model::UpdatePaperCombination($request->papercomid, $request->rmpapercombination, $request->flute, $request->boardpound);

            return json_encode([
                "success"=>true,
                "message"=>"Paper combination information has been update."
            ]);

        }


    }

}
