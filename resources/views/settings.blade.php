@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Settings</b></h3>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="pill" href="#companyinfo"><strong>Company Information</strong></a></li>
                    <li><a data-toggle="pill" href="#tax"><strong>Tax Classification</strong></a></li>
                    <li><a data-toggle="pill" href="#rawmaterial"><strong>Raw Material</strong></a></li>
                    <li><a data-toggle="pill" href="#editaccess"><strong>Edit Access</strong></a></li>
                </ul>
                  
                <div class="tab-content">

                    <div id="companyinfo" class="tab-pane fade in active">
                        <br>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="txtcompanyaddress">Address</label>
                                <input type="text" id="txtcompanyaddress" name="txtcompanyaddress" class="form-control" placeholder="Company Address">
                            </div>

                            <div class="form-group">
                                <label for="txtcompanytelnumber">Telephone Number</label>
                                <input type="text" id="txtcompanytelnumber" name="txtcompanytelnumber" class="form-control" placeholder="Company Telephone Number">
                            </div>

                            <div class="form-group">
                                <label for="txtcompanyfaxnumber">Fax Number</label>
                                <input type="text" id="txtcompanyfaxnumber" name="txtcompanyfaxnumber" class="form-control" placeholder="Company Fax Number">
                            </div>

                            <button id="btnsavecompanyinfo" name="btnsavecompanyinfo" class="btn btn-flat btn-success" style="float: right;">Save Information</button>

                        </div>
                    </div>

                    <div id="tax" class="tab-pane">
                        
                        <br>
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <label for="txtvatexempt">Vat Exempt</label>
                                <input type="number" id="txtvatexempt" name="txtvatexempt" class="form-control" placeholder="Vat Exempt">
                            </div>

                            <div class="form-group">
                                <label for="txtvatablesales">Vatable Sales</label>
                                <input type="number" id="txtvatablesales" name="txtvatablesales" class="form-control" placeholder="Vatable Sales">
                            </div>

                            <div class="form-group">
                                <label for="txtzerorated">Zero Rated</label>
                                <input type="number" id="txtzerorated" name="txtzerorated" class="form-control" placeholder="Zero Rated">
                            </div>

                            <button id="btnsavetaxclassification" name="btnsavetaxclassification" class="btn btn-flat btn-success" style="float: right;">Save Information</button>

                        </div>

                    </div>

                    <div id="rawmaterial" class="tab-pane">
                        
                        <br>
                        <div class="col-md-12">
                            
                            <div class="form-group">
                                <label for="txtoverrunpercent">Overrun Percentage</label>
                                <input type="number" id="txtoverrunpercent" name="txtoverrunpercent" class="form-control" placeholder="Overrun Percentage">
                            </div>

                            <button id="btnsaveraw" name="btnsaveraw" class="btn btn-flat btn-success" style="float: right;">Save Information</button>

                        </div>

                    </div>

                    <div id="editaccess" class="tab-pane">
                        
                        <br>
                        <div class="col-md-12">
                            
                            <button id="btnaddeditaccess" name="btnaddeditaccess" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-plus"></i> New Edit Access</button>
                            <br>
                            <br>
                            <br>
                            <table class="table" id="tbleditaccess">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Group</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modal --}}
  @include('modal.settings.neweditaccess')
  @include('modal.settings.updateeditaccess')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tbleditaccess;
    var seditaccessid;

    $(document).ready(function(){

        //Load
        LoadSettings();
        LoadCompanyInfo();
        LoadRawMaterialSettings();
        LoadEditAccessInformation();

    });

    $('#btnsavecompanyinfo').on('click', function(){

        var address = $('#txtcompanyaddress').val();
        var telnumber = $('#txtcompanytelnumber').val();
        var faxnumber = $('#txtcompanyfaxnumber').val();

        if(address==""){
            toastr.error("Please input the company address.", '', { positionClass: 'toast-top-center' });
            $('#txtcompanyaddress').focus();
        }
        else if(telnumber==""){
            toastr.error("Please input the company telephone number.", '', { positionClass: 'toast-top-center' });
            $('#txtcompanytelnumber').focus();
        }
        else if(faxnumber==""){
            toastr.error("Please input the company fax number.", '', { positionClass: 'toast-top-center' });
            $('#txtcompanyfaxnumber').focus();
        }
        else{
            
            $.ajax({
                url: '{{ url("api/settings/savecompanyinfo") }}',
                type: 'post',
                data: {
                    address: address,
                    telnumber: telnumber,
                    faxnumber: faxnumber
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        LoadCompanyInfo();

                    }

                }
            });

        }

    });

    $('#btnsavetaxclassification').on('click', function(){

        var vatexempt = $('#txtvatexempt').val();
        var vatablesales = $('#txtvatablesales').val();
        var zerorated = $('#txtzerorated').val();

        if(vatexempt==""){
            toastr.error("Please input a vat exempt.", '', { positionClass: 'toast-top-center' });
            $('#txtvatexempt').focus();
        }
        else if(vatablesales==""){
            toastr.error("Please input a vatable sales.", '', { positionClass: 'toast-top-center' });
            $('#txtvatablesales').focus();
        }
        else if(zerorated==""){
            toastr.error("Please input a zero rated.", '', { positionClass: 'toast-top-center' });
            $('#txtzerorated').focus();
        }
        else{

            $.ajax({
                url: '{{ url("api/settings/savetaxclassification") }}',
                type: 'post',
                data: {
                    vatexempt: vatexempt,
                    vatablesales: vatablesales,
                    zerorated: zerorated,
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        LoadSettings();

                    }

                }
            });

        }
        
    });

    $('#btnsaveraw').on('click', function(){

        var overrunpercentage = $('#txtoverrunpercent').val();

        if(overrunpercentage==""){
            toastr.error("Please input a overrun percentage.", '', { positionClass: 'toast-top-center' });
            $('#txtoverrunpercent').focus();
        }
        else{

            $.ajax({
                url: '{{ url("api/settings/saverawmaterial") }}',
                type: 'post',
                data: {
                    overrunpercentage: overrunpercentage
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        LoadRawMaterialSettings();

                    }

                }
            });

        }

    });

    $('#btnaddeditaccess').on('click', function(){

        LoadUserGroup();
        $('#neweditaccess').modal('toggle');

    });

    $('#btnsaveeditaccess').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save Edit Access Information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveEditAccess();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    $(document).on('click', '#btnupdateeditaccess', function(){

        seditaccessid = $(this).val();
        LoadEditAccesProfile(seditaccessid)
        $('#updateeditaccess').modal('toggle');

    });

    $('#btnueditaccess').on('click', function(){

        $.confirm({
              title: 'Update',
              content: 'Update Edit Access Information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        UpdateEditAccess();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    $(document).on('click', '#btndeleteeditaccess', function(){

        var id = $(this).val();

        $.confirm({
              title: 'Delete',
              content: 'Delete Edit Access Information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        DeleteEditAccess(id);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
        });

    });

    function DeleteEditAccess(id){

        $.ajax({
            url: '{{ url("api/settings/deleteeditaccess") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadEditAccessInformation();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

    function UpdateEditAccess(){

        var ugroupid = $('#cmbueditaccessusergroup').val();

        //Validation
        if(ugroupid==""){

            toastr.error('Please select a user group.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/settings/updateeditaccess") }}',
                type: 'post',
                data: {
                    id: seditaccessid,
                    ugroupid: ugroupid
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadEditAccessInformation();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#updateeditaccess .close').click();

                    }

                }
            });

        }

    }

    function LoadEditAccesProfile(id){

        $.ajax({
            url: '{{ url("api/settings/loadeditaccessprofile") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#cmbueditaccessusergroup').find('option').remove();
                $('#cmbueditaccessusergroup').append(response.content);
                $('#cmbueditaccessusergroup').select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function SaveEditAccess(){

        var ugroupid = $('#cmbneditaccessusergroup').val();

        //Validation
        if(ugroupid==""){

            toastr.error('Please select a user group.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/settings/saveeditaccess") }}',
                type: 'post',
                data: {
                    ugroupid: ugroupid
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadEditAccessInformation();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#neweditaccess .close').click();

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    }

    function LoadCompanyInfo(){

        $.ajax({
            url: '{{ url("api/settings/loadcompanyinfo") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtcompanyaddress').val(response.data.address);
                $('#txtcompanytelnumber').val(response.data.telnumber);
                $('#txtcompanyfaxnumber').val(response.data.faxnumber);

            }
        });

    }

    function LoadSettings(){

        $.ajax({
            url: '{{ url("api/settings/loadsettings") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtvatexempt').val(response.data.vatexempt);
                $('#txtvatablesales').val(response.data.vatablesales);
                $('#txtzerorated').val(response.data.zerorated);

            }
        });

    }

    function LoadRawMaterialSettings(){

        $.ajax({
            url: '{{ url("api/settings/loadrawmaterialsettings") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtoverrunpercent').val(response.data.overrunpercentage);

            }
        });

    }

    function LoadUserGroup(){

        $.ajax({
            url: '{{ url("api/settings/loadusergroup") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbneditaccessusergroup').find('option').remove();
                $('#cmbneditaccessusergroup').append(response.content);
                $('#cmbneditaccessusergroup').select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadEditAccessInformation(){

        tbleditaccess = $('#tbleditaccess').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/settings/loadeditaccessinformation") }}',
          },
          columns : [
              {data: 'id', name: 'id'},
              {data: 'usergroup', name: 'usergroup'},
              {data: 'panel', name: 'panel'},
          ]
        });

    }

    function ReloadEditAccessInformation(){

        tbleditaccess.ajax.reload();

    }

  </script>  

@endsection