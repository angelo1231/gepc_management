@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Quality Assurance</b></h3>
        </div>
        <div class="box-body">

          {{-- <div class="col-md-3">
              <label for="cmbcustomer">Customer</label>
              <select id="cmbcustomer" name="cmbcustomer" class="form-control">
              </select>
          </div> --}}

          <div class="col-md-12">
              {{-- <button id="btnnewcoc" name="btnnewcoc" class="btn btn-flat btn-success" style="float:right; margin: 24px 10px 24px; 0px;"><i class="fa fa-plus"></i> New COC</button> --}}
              @if(in_array(69, $access))
                <button id="btnqarejecttype" name="btnqarejecttype" class="btn btn-flat btn-primary" style="float:right; margin: 24px 0px 0px; 0px;"><i class="fa fa-tasks"></i> Reject Type Information </button>
              @endif
          </div>

          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#forfi"><strong>For Final Inspection</strong></a></li>
              <li><a data-toggle="tab" href="#fiinfo"><strong>Final Inspection Information</strong></a></li>
          </ul>
          
          <div class="tab-content">
              <div id="forfi" class="tab-pane fade in active">
                
                <br>
                <br>
                <div id="divforfinalinspection" class="col-md-12">
                  <table id="tblforcoc" class="table">
                      <thead>
                        <tr>
                          <th>Job Order #</th>
                          <th>Item Description</th>
                          <th>Target Output</th>
                          <th>Balance</th>
                        </tr>
                      </thead>
                  </table>
                </div>

              </div>
              <div id="fiinfo" class="tab-pane fade">
                
                <br>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txtfrom">From</label>
                    <input type="text" id="txtfrom" class="form-control" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txtto">To</label>
                    <input type="text" id="txtto" class="form-control" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <button id="btnsearchfiinfo" name="btnsearchfiinfo" class="btn btn-block btn-success">Search</button>
                  </div>
                </div>

                <div id="divforfinalinspectioninfo" class="col-md-12">
                  <br>
                  <table id="tblfiinfo" class="table">
                      <thead>
                        <tr>
                          <th>Final Inspection #</th>
                          <th>Job Order #</th>
                          <th>Output Quantity</th>
                          <th>Customer</th>
                          <th>Confirm By</th>
                          <th>Confirm Date</th>
                        </tr>
                      </thead>
                  </table>
                </div>

              </div>
          </div>

          

          {{-- <div class="col-md-12">
            <table id="tblcoc" class="table">
              <thead>
                  <tr>
                      <th>JO#</th>
                      <th>Item Description</th>
                      <th>Target Output</th>
                      <th>Inside/Outside</th>
                      <th>Issued By</th>
                      <th>Created Date</th>
                      <th>Created Time</th>
                      <th>Customer</th>
                      <th></th>
                  </tr>
              </thead>
            </table>
          </div> --}}


        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblforcoc;
    var tblfiinfo;
    var scustomer;
    var datebegin = moment().format("YYYY-MM-01");
    var dateend = moment().format("YYYY-MM-") + moment().daysInMonth();

    $(document).ready(function(){

      // SetSelect2();
      //Set
      SetDataTable();
      $('#txtfrom').val(datebegin);
      $('#txtto').val(dateend);
      SetDatePicker();

      //Load
      LoadCOCInformation();
      LoadFinalInspectionInformation();

    });

    $('#btnqarejecttype').on('click', function(){

      window.location = "{{ url('qainfo/rejecttype') }}";

    });

    $('#btnsearchfiinfo').on('click', function(){

      LoadFinalInspectionInformation();

    });

    function LoadCOCInformation(customer){

      tblforcoc.destroy();
      tblforcoc = $('#tblforcoc').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/qualityassurance/cocinformations") }}'
        },
        columns : [
            {data: 'jonumber', name: 'jonumber'},
            {data: 'itemdescription', name: 'itemdescription'},
            {data: 'targetoutput', name: 'targetoutput'},
            {data: 'balance', name: 'balance'}
        ]
      });

    }

    function LoadFinalInspectionInformation(){

      var from = $('#txtfrom').val();
      var to = $('#txtto').val();

      tblfiinfo.destroy();
      tblfiinfo = $('#tblfiinfo').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            data: {
              from: from,
              to: to
            },
            url: '{{ url("api/qualityassurance/loadfinalinspectioninformation") }}'
        },
        columns : [
            {data: 'finumber', name: 'finumber'},
            {data: 'jonumber', name: 'jonumber'},
            {data: 'qty', name: 'qty'},
            {data: 'customer', name: 'customer'},
            {data: 'validateby', name: 'validateby'},
            {data: 'confirmdate', name: 'confirmdate'}
        ]
      });

    }

    function SetDataTable(){

      tblforcoc = $('#tblforcoc').DataTable({
          autoWidth: false,
          ordering: false,
      });
      
      tblfiinfo = $('#tblfiinfo').DataTable({
          autoWidth: false,
          ordering: false,
      });
    
    }

    function ReloadCOCInformation(){

      tblforcoc.ajax.reload();

    }


    function ReloadFinalInspectionInformation(){

      tblfiinfo.ajax.reload();

    }

    function SetDatePicker(){

      $("#txtfrom").datepicker({ 
          dateFormat: 'yy-mm-dd' 
      });

      $("#txtto").datepicker({ 
          dateFormat: 'yy-mm-dd' 
      });

    }


  </script>  

@endsection