@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> New User Information</b></h3>
          <a href="{{ url('/user') }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#userinfo">User Information</a></li>
                </ul>
                  
                <div class="tab-content">
                    {{-- User Information --}}
                    <div id="userinfo" class="tab-pane fade in active">
                      
                        <div class="col-md-12 row">
                            <div class="col-md-6">
                                <br>
                                <div class="form-group">
                                    <label for="txtfirstname">Firstname</label>
                                    <input id="txtfirstname" name="txtfirstname" type="text" class="form-control" placeholder="Firstname">
                                </div>
                                <div class="form-group">
                                    <label for="txtmi">MI (Optional)</label>
                                    <input id="txtmi" name="txtmi" type="text" class="form-control" placeholder="Middle Initial">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <br>
                                <div class="form-group">
                                    <label for="txtlastname">Lastname</label>
                                    <input id="txtlastname" name="txtlastname" type="text" class="form-control" placeholder="Lastname">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="txtusername">Username</label>
                                    <input id="txtusername" name="txtusername" type="text" class="form-control" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="cmbusertype">Usertype</label>
                                    <select class="form-control" id="cmbusertype">
                                        {{-- <option value="Administrator">Administrator</option>
                                        <option value="Accounting">Accounting</option>
                                        <option value="Purchasing">Purchasing</option>  
                                        <option value="Planning">Planning</option>
                                        <option value="Production">Production</option>
                                        <option value="Production Operator">Production Operator</option>
                                        <option value="Raw Materials">Raw Materials</option>
                                        <option value="Finish Goods">Finish Goods</option>
                                        <option value="Sales">Sales</option>
                                        <option value="Quality Assurance">Quality Assurance</option> --}}
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="txtpassword">Password</label>
                                    <input id="txtpassword" name="txtpassword" type="password" class="form-control" placeholder="Password">
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12 row">
                            <button id="btnsaveuser" name="btnsaveuser" class="btn btn-flat btn-success" style="float: right;">Save Information</button>
                        </div>


                    </div>

                </div>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    $(document).ready(function(){

        //Load
        LoadSelect2();
        LoadUserGroup();

    });
    
    $('#btnsaveuser').on('click', function(){

        SaveUserInformation();

    });

    function SaveUserInformation(){

        var firstname = $('#txtfirstname').val();
        var lastname = $('#txtlastname').val();
        var mi = $('#txtmi').val();
        var username = $('#txtusername').val();
        var password = $('#txtpassword').val();
        var usertype = $('#cmbusertype').val();

        if(firstname==""){
            toastr.error('Please input the firstname.', '', { positionClass: 'toast-top-center' });
            $('#txtfirstname').focus();
        }
        else if(lastname==""){
            toastr.error('Please input the lastname.', '', { positionClass: 'toast-top-center' });
            $('#txtlastname').focus();
        }
        else if(username==""){
            toastr.error('Please input the username.', '', { positionClass: 'toast-top-center' });
            $('#txtusername').focus();
        }
        else if(password==""){
            toastr.error('Please input the password.', '', { positionClass: 'toast-top-center' });
            $('#txtpassword').focus();
        }
        else if(usertype==""){
            toastr.error('Please select a usertype.', '', { positionClass: 'toast-top-center' });
            $('#cmbusertype').focus();
        }
        else{

            $.ajax({
                url: '{{ url("api/user/registeruser") }}',
                type: 'post',
                data: {
                    firstname: firstname,
                    lastname: lastname,
                    mi: mi,
                    username: username,
                    password: password,
                    usertype: usertype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        window.location = '{{ url("/user") }}';

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                        $('#txtusername').val('');
                        $('#txtusername').focus();

                    }

                }
            });

        }

    }

    function LoadUserGroup(){

        $.ajax({
            url: '{{ url("api/user/loadusergroup") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbusertype').find('option').remove();
                $('#cmbusertype').append('<option value="">Please select a usertype</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbusertype').append('<option value="'+ response.data[i]["ugroupid"] +'">'+ response.data[i]["usergroup"] +'</option>');
                }

            }
        });

    }

    function LoadSelect2(){

        $('#cmbusertype').select2({
            theme: 'bootstrap'
        });

    }

  </script>  

@endsection