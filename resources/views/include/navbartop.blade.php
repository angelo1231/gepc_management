<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GEPC</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b style="font-size: 13px;">Good Earth Packaging Corp.</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $url = url(Storage::url(Auth::user()->image)); ?>?<?php echo \Carbon\Carbon::now()->toDateTimeString(); ?>"  class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->lastname }}, {{ Auth::user()->firstname }} {{ Auth::user()->mi }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $url = url(Storage::url(Auth::user()->image)); ?>?<?php echo \Carbon\Carbon::now()->toDateTimeString(); ?>" class="img-circle" alt="User Image">
                <p>
                  <strong id="navuser">{{ Auth::user()->lastname }}, {{ Auth::user()->firstname }} {{ Auth::user()->mi }} - </strong>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <button id="btnprofile" name="btnprofile" type="button" class="btn btn-default btn-flat">Profile</button>
                </div>
                <div class="pull-right">
                  <button id="btnlogout" name="btnlogout" type="button" class="btn btn-default btn-flat">Logout</button>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
<!--           <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>  -->
        </ul>
      </div>
    </nav>
  </header>

  <script>

      GetUsertype()
      function GetUsertype(){

        $.ajax({
          url: '{{ url("api/user/getusertype") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            $('#navuser').append(response.usertype);

          }
        });

      }

      $('#btnlogout').on('click', function(){

        $.ajax({
          url: '{{ url("api/login/logoutuser") }}',
          type: 'post',
          success: function(response){
              window.location = "{{ url('/login') }}";
          }
        });

      });

      $('#btnprofile').on('click', function(){

        window.location = "{{ url('/profile') }}";

      });

  </script>