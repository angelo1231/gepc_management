<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="auth" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>AUTHENTICATION</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

                <div class="form-group has-feedback">
                  <label for="txtauthusername">Username</label>
                  <input type="text" id="txtauthusername" name="txtauthusername" class="form-control" placeholder="Username" autocomplete="off">
                  <i class="fa fa-user form-control-feedback" style="font-size: 17px;"></i>
                </div>

                <div class="form-group has-feedback">
                  <label for="txtauthpassword">Password</label>
                  <input type="password" id="txtauthpassword" name="txtauthpassword" class="form-control" placeholder="Password">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnauthlogin" name="btnauthlogin" type="button" class="btn btn-primary btn-block btn-flat">Login</button>
                </div>
            </div>
        </div>
    </div>
</div>