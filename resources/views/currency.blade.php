@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Currency Information</b></h3>
        </div>
        <div class="box-body">
          <div class="col-md-9">
          </div>
          <div class="col-md-3" style="margin-bottom: 15px;">
              <button id="btnnewcurrency" name="btnnewcurrency" class="btn btn-success btn-block btn-flat"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Currency</strong></button>
          </div> 
          <div class="col-md-12">
              <table id="tblcurrency" class="table">
                  <thead>
                      <tr>
                          <th>Currency</th>
                          <th>Rate</th>
                          <th></th>
                      </tr>
                  </thead>
              </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modals --}}
  @include('modal.currency.newcurrency')
  @include('modal.currency.updatecurrency')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>


      var tblcurrency;
      $(document).ready(function() {

          tblcurrency = $('#tblcurrency').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: '{{ url("api/currency/currencyinformation") }}',
            columns : [
                {data: 'currency', name: 'currency'},
                {data: 'rate', name: 'rate'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
          });

      });

      $('#btnnewcurrency').on('click', function(){

        @if(in_array(29, $access))

          ClearNewCurrency();
          $('#modalnewcurrency').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });


      $('#btnsave').on('click', function(){

        var currency = $('#txtncurrency').val();
        var rate = $('#txtnrate').val();

        if(currency==""){
           toastr.error("Please input the currency.", '', { positionClass: 'toast-top-center' });
        }
        else if(rate==""){
          toastr.error("Please input the rate of the currency.", '', { positionClass: 'toast-top-center' });
        }
        else{

          $.ajax({
            url: '{{ url("api/currency/newcurrency") }}',
            type: 'post',
            data: {currency: currency, rate: rate},
            dataType: 'json',
            success: function(response){

              if (response.success){

                  $("#modalnewcurrency .close").click();
                  ReloadCurrency();
                  toastr.success(response.message, '', { positionClass: 'toast-top-center' });

              }
              else{

                $('#txtncurrency').val('');
                $('#txtncurrency').focus();
                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }

            }
          });

        }

      });

      $(document).on('click', '#btnedit', function(){

        @if(in_array(30, $access))

          var id = $(this).attr("value");
          ClearUpdate();

          $.ajax({
            url: '{{ url("api/currency/currencyprofile") }}',
            type: 'get',
            data: {id: id},
            dataType: 'json',
            success: function(response){

              $('#txtucurrency').val(response.currency);
              $('#txturate').val(response.rate);
              $('#btnupdate').val(id);

            }
          });

          $('#modalupdatecurrency').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnupdate').on('click', function(){

        var id = $(this).attr("value");
        var currency = $('#txtucurrency').val();
        var rate = $('#txturate').val();

        if(currency==""){
           toastr.error("Please input the currency.", '', { positionClass: 'toast-top-center' });
        }
        else if(rate==""){
          toastr.error("Please input the rate of the currency.", '', { positionClass: 'toast-top-center' });
        }
        else{

          $.ajax({
            url: '{{ url("api/currency/updatecurrency") }}',
            type: 'post',
            data: {id: id, currency: currency, rate: rate},
            dataType: 'json',
            success: function(response){

               $("#modalupdatecurrency .close").click();
               ReloadCurrency();
               toastr.success(response.message, '', { positionClass: 'toast-top-center' });

            }
          });

        }

      });


      function ClearNewCurrency(){

        $('#txtncurrency').val('');
        $('#txtnrate').val('');

      }

      function ClearUpdate(){

        $('#txtucurrency').val('');
        $('#txturate').val('');

      }

      function ReloadCurrency(){
        tblcurrency.ajax.reload();
      }


  </script>  

@endsection