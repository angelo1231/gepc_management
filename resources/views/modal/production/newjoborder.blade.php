<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newjoborderprod" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong id="pinfo">New Job Order</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="cmbjornumber">Job Order Request Number</label>
                            <select name="cmbjornumber" id="cmbjornumber" class="form-control">
                            </select>
                        </div>

                    </div>

                    <div id="divjocontent" class="col-md-12" style="display: none;">

                        <hr>
                        <div class="col-md-6">
                            <div class="row">

                                <div class="form-group">
                                    <label for="txtjonum">Job Order Number</label>
                                    <input type="text" id="txtjonum" name="txtjonum" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtdeldatefrom">Delivery Date From</label>
                                    <input id="txtdeldatefrom" name="txtdeldatefrom" class="form-control" placeholder="Delivery Date From" type="text">
                                </div>
            
                                <div class="form-group">
                                    <label for="txtdeldateto">Delivery Date To</label>
                                    <input id="txtdeldateto" name="txtdeldateto" class="form-control" placeholder="Delivery Date To" type="text">
                                </div>

                                <div class="form-group">
                                    <label for="txtstyle">Style</label>
                                    <input id="txtstyle" name="txtstyle" class="form-control" placeholder="Style" type="text">
                                </div>
            
                                <div class="form-group">
                                    <label for="txtinkcolor">Ink Color</label>
                                    <input id="txtinkcolor" name="txtinkcolor" class="form-control" placeholder="Ink Color" type="text">
                                </div>
            
            
                                <div class="form-group">
                                    <label for="txtspecialinstruction">Special Instruction</label>
                                    <textarea class="form-control" rows="5" id="txtspecialinstruction" name="txtspecialinstruction" placeholder="Special Instruction"></textarea>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="">

                                <div class="form-group">
                                    <label for="txtjocustomer">Customer</label>
                                    <input type="text" id="txtjocustomer" name="txtjocustomer" class="form-control" readonly>
                                </div>
                                
                                <div class="form-group">
                                    <label for="txtjoponumber">Purchase Order Number</label>
                                    <input type="text" id="txtjoponumber" name="txtjoponumber" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtjoitem">Item</label>
                                    <input type="text" id="txtjoitem" name="txtjoitem" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtmrnumber">Material Request Number</label>
                                    <input type="text" id="txtmrnumber" name="txtmrnumber" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtjotargetoutput">Target Output</label>
                                    <input type="text" id="txtjotargetoutput" name="txtjotargetoutput" class="form-control" readonly>
                                </div>

                            </div>
                        </div>
                            
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                        <div class="col-md-12">
                            <button id="btncreate" name="btncreate" type="button" class="btn btn-success btn-flat">Create</button>
                            <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Close</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>