<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notif_Model extends Model
{
    
    public static function NotifPlanningMR($prid){

        $result = DB::table('purchaserequest')
        ->select(
            'prnumber'
        )
        ->where('prid', '=', $prid)
        ->get();

        return $result;

    }

}
