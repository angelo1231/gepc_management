@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><b>Profile</b></h3>
        </div>
        <div class="box-body">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-12">
                  <p style="text-align: center; margin: 20px auto;">
                    <img src="<?php echo $url = url(Storage::url(Auth::user()->image)); ?>?<?php echo \Carbon\Carbon::now()->toDateTimeString(); ?>" class="img-circle" width="250px" height="250px" alt="User Image">
                  </p>
                  <p style="text-align: center; margin: 10px auto;">
                    <button id="btnimage" name="btnimage" type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#modaluploadimage"><i class="fa fa-picture-o" style="font-size: 12px;"></i> Upload Picture</button>
                  </p>
              </div>
              <div class="col-md-12">
                  <p style="text-align: center; margin: 20px auto;">
                    <img src="<?php echo $url = asset('esign') . '/' . Auth::user()->esign; ?>?<?php echo \Carbon\Carbon::now()->toDateTimeString(); ?>" class="img-circle" width="250px" height="250px" alt="User Image">
                  </p>
                  <p style="text-align: center; margin: 10px auto;">
                    <button id="btnesign" name="btnesign" type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#uploadesign"><i class="fa fa-picture-o" style="font-size: 12px;"></i> Upload Signature</button>
                  </p>
              </div>
            </div>

          </div>
          <div class="col-md-6">
              
            <form id="frmprofile" name="frmprofile" action="{{ url('api/profile/updateprofile') }}" method="POST">
           
                <div class="form-group has-feedback">
                    <label for="txtfirstname">Firstname</label>
                    <input id="txtfirstname" name="txtfirstname" type="text" class="form-control" placeholder="Firstname" value="{{ Auth::user()->firstname }}">
                </div>
                <div class="form-group has-feedback">
                    <label for="txtlastname">Lastname</label>
                    <input id="txtlastname" name="txtlastname" type="text" class="form-control" placeholder="Lastname" value="{{ Auth::user()->lastname }}">
                </div>
                <div class="form-group has-feedback">
                    <label for="txtmi">MI (Optional)</label>
                    <input id="txtmi" name="txtmi" type="text" class="form-control" placeholder="Middle Initial" value="{{ Auth::user()->mi }}">
                </div>
                <div class="form-group has-feedback">
                    <label for="txtusername">Username</label>
                    <input id="txtusername" name="txtusername" type="text" class="form-control" placeholder="Username" value="{{ Auth::user()->username }}" readonly>
                </div>
                <div class="form-group has-feedback">
                    <label for="txtusertype">Usertype</label>
                    <input id="txtusertype" name="txtusertype" type="text" class="form-control" placeholder="Usertype" value="{{ $usertype }}" readonly>
                </div>

            </form>

          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
            <button id="btnrestore" name="btnrestore" type="button" class="btn btn-primary btn-flat" style="float: right; margin-left: 5px;"><i class="fa fa-refresh"></i></button>
            <button id="btnchangepassword" name="btnchangepassword" type="button" class="btn btn-primary btn-flat" style="float: right; margin-left: 5px;" data-toggle="modal" data-target="#modalchange"><i class="fa fa-lock" style="font-size: 12px;"></i> Change Password</button>
            <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-flat" style="float: right;"><i class="fa fa-pencil" style="font-size: 12px;"></i> Update</button>
            
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  <!-- Modal -->
  @include('modal.profile.uploadimage')
  @include('modal.profile.uploadesign')
  @include('modal.profile.changepassword')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    $(document).ready(function() {
     
        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
          toastr.success(msg, '', { positionClass: 'toast-top-center' });
        }

    });

    $('#btnupdate').on('click', function(){

        $.confirm({
              title: 'Update',
              content: 'Update your profile information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){
                          
                          var firstname = $('#txtfirstname').val();
                          var lastname = $('#txtlastname').val();
                          var mi = $('#txtmi').val();
                          if(firstname==""){
                            toastr.error('Please input the firstname.', '', { positionClass: 'toast-top-center' });
                          }
                          else if(lastname==""){
                            toastr.error('Please input the lastname.', '', { positionClass: 'toast-top-center' });
                          }
                          else{

                            $.ajax({
                              url: '{{ url("api/profile/updateinformation") }}',
                              type: 'post',
                              data: {firstname: firstname, lastname: lastname, mi: mi},
                              dataType: 'json',
                              success: function(response){

                                if(response.success){
                                  document.location.reload(false);
                                }

                              }
                            });

                          }

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        Restore();

                      }
                  } 
              }
          });

    });

    $('#btnupload').on('click', function(){
      
      var image = $('#image').val();
      if(image==""){
          toastr.error("Please select a image.", '', { positionClass: 'toast-top-center' });
      }
      else{

          var frm = document.getElementById("frmupload");
          var fd = new FormData(frm);

          $.ajax({
            url: '{{ url("api/profile/uploadimage") }}',
            type: 'post',
            data: fd,
            contentType: false,      
            cache: false,             
            processData:false, 
            dataType: 'json',
            success: function(response){

              if(response.success){
                document.location.reload(false);
              }

            }
          });

      }

    });

    $('#btnrestore').on('click', function(){
        Restore();
    });


    $('#btnchangepassword').on('click', function(){

      ClearChangePassword();

    });

    $('#btnchange').on('click', function(){

        var oldpassword = $('#txtoldpassword').val();
        var newpassword = $('#txtnewpassword').val();
        var confirmpassword = $('#txtconfirmpassword').val();

        if(oldpassword==""){

          toastr.error('Please input old password.', '', { positionClass: 'toast-top-center' });

        }
        else if(newpassword==""){

          toastr.error('Please input the new password.', '', { positionClass: 'toast-top-center' });

        }
        else if(confirmpassword==""){

          toastr.error('Please confirm the password.', '', { positionClass: 'toast-top-center' });

        }
        else{

          $.ajax({

            url: '{{ url("api/profile/changepassword") }}',
            type: 'post',
            data: {
              oldpassword: oldpassword, 
              newpassword: newpassword, 
              confirmpassword: confirmpassword
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                  $("#modalchange .close").click();
                  toastr.success(response.message, '', { positionClass: 'toast-top-center' });

              }
              else{

                if(response.type=="oldpassword"){
                  $('#txtoldpassword').val('');
                  $('#txtoldpassword').focus();
                }
                else if(response.type=="matchpassword"){
                  $('#txtconfirmpassword').val('');
                  $('#txtconfirmpassword').focus();
                }
                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }

            }

          });

        }

    });

    $('#btnuploadesign').on('click', function(){

      var image = $('#esign').val();
      if(image==""){
          toastr.error("Please select a image.", '', { positionClass: 'toast-top-center' });
      }
      else{

          var form = new FormData();
          form.append("file", $('#esign')[0].files[0]);

          $.ajax({
            url: '{{ url("api/profile/uploadesign") }}',
            type: 'post',
            data: form,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){

              if(response.success){
                document.location.reload(false);
              }

            }
          });

      }

    });


    function ClearChangePassword(){

      $('#txtoldpassword').val('');
      $('#txtnewpassword').val('');
      $('#txtconfirmpassword').val('');

    }

    function Restore(){

        $('#txtfirstname').val('{{ Auth::user()->firstname }}');
        $('#txtlastname').val('{{ Auth::user()->lastname }}');
        $('#txtmi').val('{{ Auth::user()->mi }}');

    }

  </script>  

@endsection