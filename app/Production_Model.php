<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Production_Model extends Model
{
   

    public static function LoadMSTNumber(){

        $result = DB::table('productiontags')
        ->select(
            'productiontags.qafiid',
            'joborders.jonumber',
            'productiontags.mstnumber',
            'qafinalinspectioninformation.outputqty',
            'jobordermonitoringaudititems.rejectqty',
            DB::raw("CONCAT(users.lastname, ', ', users.firstname) AS 'issuedby'"),
            DB::raw("DATE_FORMAT(productiontags.created_at, '%Y-%m-%d %r') AS 'created_at'")   
        )
        ->join('qafinalinspectioninformation', 'qafinalinspectioninformation.qafiid', '=', 'productiontags.qafiid')
        ->join('jobordermonitoringaudititems', 'jobordermonitoringaudititems.id', '=', 'qafinalinspectioninformation.auditid')
        ->join('joborders', 'joborders.joid', '=', 'qafinalinspectioninformation.joid')
        ->join('users', 'users.id', '=', 'qafinalinspectioninformation.issuedby')
        ->where('productiontags.flag', '=', 0)
        ->get();

        return $result;

    }

    public static function UpdateMSTStatus($qafiid){

        DB::table('productiontags')
        ->where('qafiid', '=', $qafiid)
        ->update([
            "flag"=>1
        ]);

    }

    public static function GetFGInfo($qafiid){

        $result = DB::table('productiontags')
        ->select(
            'joborders.jonumber',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'finishgoods.packingstd',
            'customers.customer'
        )
        ->join('qafinalinspectioninformation', 'qafinalinspectioninformation.qafiid', '=', 'productiontags.qafiid')
        ->join('joborders', 'joborders.joid', '=', 'qafinalinspectioninformation.joid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('customers', 'customers.cid', '=', 'joborders.cid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('productiontags.qafiid', '=', $qafiid)
        ->first();

        return $result;

    }

}
