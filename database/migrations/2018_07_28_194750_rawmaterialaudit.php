<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rawmaterialaudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmaterialaudit', function (Blueprint $table) {
            $table->increments('auditid');
            $table->integer('rmid');
            $table->text('ponumber')->nullable();
            $table->text('drnumber')->nullable();
            $table->text('requisitionnumber')->nullable();
            $table->integer('qty');
            $table->integer('balance');
            $table->text('issuedby')->nullable();
            $table->longtext('issuedby')->nullable();
            $table->date('issueddate');
            $table->time('issuedtime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmaterialaudit');
    }
}
