@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Group Process</b></h3>
        </div>
        <div class="box-body">

            <div class="col-md-12">

              <button id="btnnewgroupprocess" name="btnnewgroupprocess" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Process Code</button>
                
            </div>

            <div class="col-md-12">

                <table id="tblgroupprocess" class="table">
                    <thead>
                        <tr>
                            <th>Process Code</th>
                            <th>Process</th>
                            <th>Style</th>
                            <th>Ink Color</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->

        {{-- Modal --}}
        @include('modal.process.newgroupprocess')
        @include('modal.process.updategroupprocess')

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblgroupprocess;
    var sprocesscode;

    //Variables New Process Group
    var norder = 1;
    var nitemcount = 1;
    var nitemarray = new Array();

    //Variables Update Process Group
    var uorder = 1;
    var uitemcount = 1;
    var uitemarray = new Array();
    var ditemarray = new Array();


    $(document).ready(function(){

      LoadGroupProcess();

    });

    $('#btnnewgroupprocess').on('click', function(){

      @if(in_array(20, $access))

        ClearNewInformation();
        LoadProcess($('#cmbnprocess1').attr('id'));
        $('#newgroupprocess').modal('toggle');

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $('#txtnprocesscode').on('keyup', function(){

      $(this).val($(this).val().toUpperCase());

    });

    $('#btnnadd').on('click', function(){

      AddNewProcess();

    });

    $(document).on('click', '#btnndelete', function(){

      var itemcount = $(this).val();
      DeleteNewProcess(itemcount);
      ResetNewOrdering();

    });

    $('#btnsave').on('click', function(){

      SaveGroupProcessInformation();

    });

    $(document).on('click', '#btnedit', function(){

      @if(in_array(21, $access))

        sprocesscode = $(this).val();
        ClearUpdateInformation();
        LoadGroupProcessInformation(sprocesscode);
        $('#updategroupprocess').modal('toggle');

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $(document).on('click', '#btnuremove', function(){

      var itemcount = $(this).val();
      DeleteUpdateProcess(itemcount);
      ResetUpdateOrdering();

    });

    $('#btunadd').on('click', function(){

      uitemarray.push(uitemcount);

      $('#uprocesscontent').append('<tr id="update'+uitemcount+'"><td style="vertical-align: middle;"><input type="text" id="txtuid'+uitemcount+'" name="txtuid[]" value="0" hidden="true"><input type="text" id="txtuorder'+uitemcount+'" name="txtuorder[]" class="form-control" value="'+ uorder +'" style="width: 35px;" readonly></td><td><select name="cmbuprocess[]" id="cmbuprocess'+uitemcount+'" class="form-control" style="width: 250px;"></select></td><td><button id="btnuremove" name="btnuremove" class="btn btn-flat btn-danger" value="'+uitemcount+'"><i class="fa fa-trash"></i></button></td></tr>');

      LoadProcess("cmbuprocess"+uitemcount);

      uitemcount += 1;
      uorder += 1;

    });

    $('#btndelete').on('click', function(){

        $.ajax({
          url: '{{ url("api/groupprocess/deletegroupprocess") }}',
          type: 'post',
          data: {
            processcode: sprocesscode
          },
          dataType: 'json',
          success: function(response){

            if(response.success){

              $('#updategroupprocess .close').click();
              toastr.success(response.message, '', { positionClass: 'toast-top-center' });
              ReloadGroupProcess();

            }

          }
        });

    });

    $('#btnupdate').on('click', function(){

      var processid = $('[name="txtuid[]"]').serializeArray();
      var style = $('#txtustyle').val();
      var inkcolor = $('#txtuinkcolor').val();
      var ordering = $('[name="txtuorder[]"]').serializeArray();
      var process = $('[name="cmbuprocess[]"]').serializeArray();

      //Validation
      if(style==""){
        toastr.error('Please input the style of the process.', '', { positionClass: 'toast-top-center' });
        $('#txtustyle').focus();
      }
      else if(inkcolor==""){
        toastr.error('Please input the ink color of the process.', '', { positionClass: 'toast-top-center' });
        $('#txtuinkcolor').focus();
      }
      else if(process.length==0){
        toastr.error('Please select a process for the process code.', '', { positionClass: 'toast-top-center' });
      }
      else{

        $.ajax({
          url: '{{ url("api/groupprocess/updategroupprocessinformation") }}',
          type: 'post',
          data: {
            processcode: sprocesscode,
            processid: processid,
            style: style,
            inkcolor: inkcolor,
            ordering: ordering,
            process: process,
            pdelete: ditemarray
          },
          dataType: 'json',
          success: function(response){

            if(response.success){

              $('#updategroupprocess .close').click();
              toastr.success(response.message, '', { positionClass: 'toast-top-center' });
              ReloadGroupProcess();

            }

          }
        });

      }




    });

    function LoadGroupProcessInformation(processcode){

      $.ajax({
        url: '{{ url("api/groupprocess/loadgroupprocessinformation") }}',
        type: 'get',
        data: {
          processcode: processcode
        },
        dataType: 'json',
        success: function(response){

          $('#txtuprocesscode').val(response.groupprocess[0]["processcode"]);
          $('#txtustyle').val(response.groupprocess[0]["style"]);
          $('#txtuinkcolor').val(response.groupprocess[0]["inkcolor"]);

          for(var i=0;i<response.groupprocess.length;i++){

            uitemarray.push(uitemcount);

            if(i==0){
              $('#uprocesscontent').append('<tr id="update'+uitemcount+'"><td style="vertical-align: middle;"><input type="text" id="txtuid'+uitemcount+'" name="txtuid[]" value="'+ response.groupprocess[i]["id"] +'" hidden="true"><input type="text" id="txtuorder'+uitemcount+'" name="txtuorder[]" class="form-control" value="'+ response.groupprocess[i]["processorder"] +'" style="width: 35px;" readonly></td><td><select name="cmbuprocess[]" id="cmbuprocess'+uitemcount+'" class="form-control" style="width: 250px;"></select></td><td></td></tr>');
            }
            else{
              $('#uprocesscontent').append('<tr id="update'+uitemcount+'"><td style="vertical-align: middle;"><input type="text" id="txtuid'+uitemcount+'" name="txtuid[]" value="'+ response.groupprocess[i]["id"] +'" hidden="true"><input type="text" id="txtuorder'+uitemcount+'" name="txtuorder[]" class="form-control" value="'+ response.groupprocess[i]["processorder"] +'" style="width: 35px;" readonly></td><td><select name="cmbuprocess[]" id="cmbuprocess'+uitemcount+'" class="form-control" style="width: 250px;"></select></td><td><button id="btnuremove" name="btnuremove" class="btn btn-flat btn-danger" value="'+uitemcount+'"><i class="fa fa-trash"></i></button></td></tr>');
            }


            SetGroupProcessInfo("cmbuprocess"+uitemcount, response.groupprocess[i]["processid"]);

            uitemcount += 1;
            uorder += 1;

          }

        }
      });

    }

    function SetGroupProcessInfo(id, processid){

     $.ajax({
        url: '{{ url("api/groupprocess/loadprocess") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

            $('#'+id).find('option').remove();
            $('#'+id).append('<option value="" disabled selected>Select a Process</option>');
            for (var i = 0; i < response.data.length; i++) {
                $('#'+id).append('<option value="'+  response.data[i]["pid"] +'">'+  response.data[i]["process"] +'</option>');
            }

            $('#'+id).select2({
              theme: 'bootstrap'
            });

        },
        complete: function(){

          $('#'+id).val(processid).trigger('change');

        }
      });

    }

    function SaveGroupProcessInformation(){

        var processcode = $('#txtnprocesscode').val();
        var style = $('#txtnstyle').val();
        var inkcolor = $('#txtninkcolor').val();
        var dataorder = $('[name="txtnorder[]"]').serializeArray();
        var dataprocess = $('[name="cmbnprocess[]"]').serializeArray();

        //Validation
        if(processcode==""){
          toastr.error('Please input the process code.', '', { positionClass: 'toast-top-center' });
          $('#txtnprocesscode').focus();
        }
        else if(style==""){
          toastr.error('Please input the style of the process.', '', { positionClass: 'toast-top-center' });
          $('#txtnstyle').focus();
        }
        else if(inkcolor==""){
          toastr.error('Please input the ink color of the process.', '', { positionClass: 'toast-top-center' });
          $('#txtninkcolor').focus();
        }
        else if(dataprocess.length==0){
          toastr.error('Please select a process for the process code.', '', { positionClass: 'toast-top-center' });
        }
        else{

          $.ajax({
            url: '{{ url("api/groupprocess/savegroupprocess") }}',
            type: 'post',
            data: {
              processcode: processcode,
              style: style,
              inkcolor: inkcolor,
              dataorder: dataorder,
              dataprocess: dataprocess
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                ReloadGroupProcess();
                $('#newgroupprocess .close').click();

              }
              else{

                toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                $('#txtnprocesscode').val('');
                $('#txtnprocesscode').focus();


              }

            }
          });

        }




    }

    function AddNewProcess(){

      nitemcount += 1;
      norder += 1;

      nitemarray.push(nitemcount);
      
      $('#nprocesscontent').append('<tr id="new'+ nitemcount +'"><td style="vertical-align: middle;"><input type="text" id="txtnorder'+ nitemcount +'" name="txtnorder[]" class="form-control" value="'+ norder +'" style="width: 35px;" readonly></td><td><select name="cmbnprocess[]" id="cmbnprocess'+ nitemcount +'" class="form-control" style="width: 250px;"></select></td><td><button id="btnndelete" name="btnndelete" class="btn btn-flat btn-danger" value="'+ nitemcount +'"><i class="fa fa-trash"></i></button></td></tr>');
      
      LoadProcess('cmbnprocess'+ nitemcount);

    }

    function DeleteNewProcess(itemcount){

      norder -= 1;
      $('#new'+itemcount).remove();

    }

    function DeleteUpdateProcess(itemcount){

      var id = $('#txtuid'+itemcount).val();
      uorder -= 1;
      if(id!=0){
        ditemarray.push(id);
      }
      $('#update'+itemcount).remove();

    }

    function ClearNewInformation(){

      $('#txtnprocesscode').val('');
      $('#txtnstyle').val('');
      $('#txtninkcolor').val('');

      for(var i=0;i<nitemarray.length;i++){
          $('#new'+nitemarray[i]).remove();
      }
      norder = 1;
      nitemcount = 1;
      nitemarray = [];

    }

    function ClearUpdateInformation(){

      $('#txtuprocesscode').val('');
      $('#txtustyle').val('');
      $('#txtuinkcolor').val('');

      for(var i=0;i<uitemarray.length;i++){
          $('#update'+uitemarray[i]).remove();
      }
      uorder = 1;
      uitemcount = 1;
      uitemarray = [];
      ditemarray = [];

    }

    function LoadProcess(id){

      $.ajax({
        url: '{{ url("api/groupprocess/loadprocess") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

            $('#'+id).find('option').remove();
            $('#'+id).append('<option value="" disabled selected>Select a Process</option>');
            for (var i = 0; i < response.data.length; i++) {
                $('#'+id).append('<option value="'+  response.data[i]["pid"] +'">'+  response.data[i]["process"] +'</option>');
            }

            $('#'+id).select2({
              theme: 'bootstrap'
            });

        }
      });

    }

    function ResetNewOrdering(){

      var count = 2;

      for(var i=0;i<nitemarray.length;i++){

        var elementExists = document.getElementById('txtnorder'+nitemarray[i]);

        if(elementExists!=null){
          $('#txtnorder'+nitemarray[i]).val(count);
          count ++;
        }

      }

    }

    function ResetUpdateOrdering(){

      var count = 1;

      for(var i=0;i<uitemarray.length;i++){

        var elementExists = document.getElementById('txtuorder'+uitemarray[i]);

        if(elementExists!=null){
          $('#txtuorder'+uitemarray[i]).val(count);
          count ++;
        }

      }

    }

    function LoadGroupProcess(){

      tblgroupprocess = $('#tblgroupprocess').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: '{{ url("api/groupprocess/loadgroupprocess") }}',
          columns : [
              {data: 'processcode', name: 'processcode'},
              {data: 'process', name: 'process'},
              {data: 'style', name: 'style'},
              {data: 'inkcolor', name: 'inkcolor'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
      });

    }

    function ReloadGroupProcess(){

      tblgroupprocess.ajax.reload();

    }

  </script>  

@endsection