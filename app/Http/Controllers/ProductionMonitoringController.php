<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductionMonitoring_Model;
use Auth;
use App\Http\Controllers\UserController;

class ProductionMonitoringController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        $access = UserController::UserGroupAccess();

        $process = ProductionMonitoring_Model::LoadProcess();

        return view('productionmonitoring')
        ->with('process', $process)
        ->with('access', $access);
        
    }

    function LoadProcess(){

        $process = ProductionMonitoring_Model::LoadProcess();

        return json_encode([
            "data"=>$process
        ]);

    }

    function GetProcessJO(Request $request){

        
        if($request->search!=null){

            $processjo = ProductionMonitoring_Model::GetProcessJOSearch($request);

            return json_encode([
                "data"=>$processjo
            ]);
            
        }
        else{

            $processjo = ProductionMonitoring_Model::GetProcessJO($request);

            return json_encode([
                "data"=>$processjo
            ]);

        }

    }

    function GetMonitoringInformation(Request $request){

        $monitoring = ProductionMonitoring_Model::GetMonitoringInformation($request->mid);

        return json_encode([
            "jonumber"=>$monitoring->jonumber,
            "process"=>$monitoring->process
        ]);

    }

    function SaveOperationInformation(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
        $balance = $request->qty + $request->rejectqty;

        //Validation
        $validation = ProductionMonitoring_Model::ValidateBalance($request->mid, $request->pid, $balance);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Invalid input your over the balance of the operation."
            ]);

        }
        else{

            $auditid = ProductionMonitoring_Model::GetAuditID($request->mid, $request->pid);
            $processcode = ProductionMonitoring_Model::GetJobOrderFGIDProcess($request->mid);
            $process = ProductionMonitoring_Model::GetProcess($processcode);
    
            ProductionMonitoring_Model::UpdateProcessTargetOutputBalance($request->mid, $request->pid, $balance);

            //Next Process
            for($i=0;$i<count($process);$i++){

                if($process[$i]->processid==$request->pid){
    
                    if($i+1<count($process)){
    
                        ProductionMonitoring_Model::UpdateCurrentProcess($request->mid, $process[$i+1]->processid);
                        ProductionMonitoring_Model::UpdateMonitoringAudit($request->mid, $process[$i+1]->processid, $request->qty);
    
                    }
                    else{
    
                        ProductionMonitoring_Model::UpdateCurrentProcess($request->mid, $process[0]->processid);
    
                    }
    
                    break;
                }
    
            }
    
            $auditid = ProductionMonitoring_Model::SaveOperationInformation($auditid, $issuedby, $request->qty, $request->rejectqty, $request->rejecttype);
    
            return json_encode([
                "success"=>true,
                "auditid"=>$auditid,
                "message"=>"Success save operation information"
            ]);

        }

    }

    function LoadRejectType(){

        $content = "";
        $rejecttype = ProductionMonitoring_Model::LoadRejectType();

        $content .= '
            <option value="0">N/A</option>
        ';
        foreach($rejecttype as $val){

            $content .= '
                <option value="'. $val->id .'">'. $val->reject .'</option>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

}
