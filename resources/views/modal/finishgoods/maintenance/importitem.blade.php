<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modalimportitem" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>IMPORT</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-3">

                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="cmbimportcustomer">Customer</label>
                            <select name="cmbimportcustomer" id="cmbimportcustomer" class="form-control"></select>
                        </div>

                        <div class="form-group">
                            <label for="file">Select a file</label>
                            <input type="file" id="file" name="file" accept=".xls, .xlsx, .csv">
                        </div>
                    </div>
                    <div class="col-md-3">

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-6">

                </div>
                <div class="col-md-6">
                    <button id="btnimportdata" name="btnimportdata" type="button" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Save</button>
                    <button id="btnimporttemp" name="btnimporttemp" type="button" class="btn btn-info btn-flat"><i class="fa fa-download"></i> Download Template</button>
                </div>
            </div>
        </div>
    </div>
</div>