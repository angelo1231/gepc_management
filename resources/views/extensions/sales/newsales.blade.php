@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> New Sales Information</b></h3>
          <a href="{{ url("/sales") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-6">

                <div class="form-group">
                    <label for="cmbcustomer">Customer</label>
                    <select id="cmbcustomer" name="cmbcustomer" class="form-control">
                    </select>
                </div>

                <div class="form-group">
                    <label for="txtdeliverynumber">Delivery #</label>
                    <input id="txtdeliverynumber" name="txtdeliverynumber" type="text" class="form-control" placeholder="Delivery Number">
                </div>

                <div class="form-group">
                    <label for="txtdeldate">Delivery Date</label>
                    <input id="txtdeldate" name="txtdeldate" type="text" class="form-control" placeholder="Delivery Date">
                </div>

            </div>


            <div class="col-md-12">

                <table id="tblitems" class="table" style="margin-top: 10px;">
                    <thead>
                        <tr>
                            <th style="width: 200px;">PO #</th>
                            <th style="width: 200px;">Item</th>
                            <th>Item Description</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th><button id="btnadd" name="btnadd" class="btn btn-flat btn-primary" disabled><i class="fa fa-plus"></i></button></th>
                        </tr>
                    </thead>
                    <tbody id="deliverycontent">
                        <tr>
                            <td>
                                <select id="cmbpo1" name="cmbpo[]" class="form-control" disabled></select>
                            </td>
                            <td>
                                <select id="cmbitem1" name="cmbitem[]" class="form-control" disabled></select>
                            </td>
                            <td>
                                <textarea id="txtitemdescription1" name="txtitemdescription[]" class="form-control" rows="4" readonly placeholder="Item Description"></textarea>
                            </td>
                            <td>
                                <input id="txtprice1" name="txtprice[]" class="form-control" type="text" placeholder="Price" readonly>
                            </td>
                            <td>
                                <input id="txtqty1" name="txtqty[]" class="form-control" type="number" placeholder="Qty" readonly>
                            </td>
                            <td>
                                <input id="txttotal1" name="txttotal[]" class="form-control" type="text" placeholder="Total" readonly>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>        
                </table>
                <label id="lblgrandtotal" style="float: right; margin-right: 65px;">Grand Total: 0</label>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
            <button id="btnsavedr" name="btnsavedr" class="btn btn-primary btn-flat" style="float:right;"><i class="fa fa-floppy-o" style="font-size: 12px;"></i><strong> Save Information</strong></button>
                
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var itemcount = 1;
      var itemarray = new Array();
      var scid;
      var grandtotal;

      $(document).ready(function(){

        LoadCustomers();
        SetDatePicker();

      });

      $('#cmbcustomer').on('change', function(){

        var attr = $('#cmbpo1').attr('id');
        scid = $(this).val();
        LoadCustomerPO(attr, scid);
        $('#btnadd').prop('disabled',false);

      });

      $('#btnadd').on('click', function(){

        itemcount += 1;
        itemarray.push(itemcount);

        $('#deliverycontent').append('<tr id="po'+ itemcount +'"><td><select id="cmbpo'+ itemcount +'" name="cmbpo[]" class="form-control"></select></td><td><select id="cmbitem'+ itemcount +'" name="cmbitem[]" class="form-control" disabled></select></td><td><textarea id="txtitemdescription'+ itemcount +'" name="txtitemdescription[]" class="form-control" rows="4" readonly placeholder="Item Description"></textarea></td><td><input id="txtprice'+ itemcount +'" name="txtprice[]" class="form-control" type="text" placeholder="Price" readonly></td><td><input id="txtqty'+ itemcount +'" name="txtqty[]" class="form-control" type="number" placeholder="Qty" readonly></td><td><input id="txttotal'+ itemcount +'" name="txttotal[]" class="form-control" type="text" placeholder="Total" readonly></td><td><button id="btnremove" name="btnremove" class="btn btn-flat btn-info" value="'+ itemcount +'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadCustomerPO($('#cmbpo'+itemcount).attr('id'), scid);

      });

      $(document).on('click','#btnremove', function(){

        var id = $(this).val();
        var position = $.inArray(parseInt(id), itemarray);

        if(~position){
            itemarray.splice(position, 1);
            document.getElementById("po"+id).remove();
            GrandTotal();
        }

      });

      $(document).on('change', '[name="cmbpo[]"]', function(){

        var id = $(this).attr('id');
        var poid = $(this).val();

        split_string_id = id.split(/(\d+)/);

        LoadPOItems(split_string_id[1], poid);
        
      });

      $(document).on('change', '[name="cmbitem[]"]', function(){

        var id = $(this).attr('id');
        var itemid = $(this).val();

        split_string_id = id.split(/(\d+)/);

        LoadItemInformation(split_string_id[1], itemid);

      });

      $(document).on('keyup', '[name="txtqty[]"]', function(){

        var id = $(this).attr('id');
        split_string_id = id.split(/(\d+)/);

        var itemid = $('#cmbitem'+split_string_id[1]).val();
        var qty = $(this).val();
        Currency(split_string_id[1], itemid, qty);

      });

      $('#btnsavedr').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this delivery information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveDeliveryInformation();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

      });

      function SaveDeliveryInformation(){

          var validateblank = false;
          var validatenegative = false;
          var drnumber = $('#txtdeliverynumber').val();
          var deldate = $('#txtdeldate').val();
          var datapo = $('[name="cmbpo[]"]').serializeArray();
          var dataitem = $('[name="cmbitem[]"]').serializeArray();
          var dataqty = $('[name="txtqty[]"]').serializeArray();
          var dataprice = $('[name="txtprice[]"]').serializeArray();
          var datatotal = $('[name="txttotal[]"]').serializeArray();

          //Validation
          for(var i=0;i<dataqty.length;i++){
            
            if(dataqty[i].value==""){
                validateblank = true;
                break;
            }

            if(dataqty[i].value<=0){
                validatenegative = true;
                break;
            }

          }


          if(scid==null){

            toastr.error("Please select a customer.", '', { positionClass: 'toast-top-center' });

          } 
          else if(drnumber==""){

            toastr.error("Please input the delivery number.", '', { positionClass: 'toast-top-center' });

          }
          else if(deldate==""){

            toastr.error("Please input the delivery date.", '', { positionClass: 'toast-top-center' });

          }
          else{

            if(!validateblank){

                if(!validatenegative){

                    $.ajax({
                        url: '{{ url("api/sales/validatedeliverynumber") }}',
                        type: 'get',
                        data: {
                            drnumber: drnumber
                        },
                        dataType: 'json',
                        success: function(response){

                            if(response.success){

                                toastr.error("Delivery number already exist", '', { positionClass: 'toast-top-center' });
                                $('#txtdeliverynumber').val('');

                            }
                            else{


                                $.ajax({
                                    url: '{{ url("api/sales/savedeliveryinformation") }}',
                                    type: 'post',
                                    data: {
                                        cid: scid,
                                        drnumber: drnumber,
                                        deldate: deldate,
                                        datapo: datapo,
                                        dataitem: dataitem,
                                        dataqty: dataqty,
                                        dataprice: dataprice,
                                        datatotal: datatotal,
                                        grandtotal: grandtotal
                                    },
                                    dataType: 'json',
                                    beforeSend: function(){
                                        H5_loading.show();
                                    },
                                    success: function(response){

                                        if(response.success){

                                            H5_loading.hide();
                                            window.location = "{{ url('/sales') }}";

                                        }
                                        else{

                                            H5_loading.hide();
                                            toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                                        }

                                    }
                                });


                            }

                        }
                    });

                }
                else{

                    toastr.error("Please dont input negative numbers or equal to zero quantity.", '', { positionClass: 'toast-top-center' });

                }

            }
            else{

                toastr.error("Please dont leave any blank information.", '', { positionClass: 'toast-top-center' });

            }

            


          }
    
      }

      function Currency(id, itemid, qty){

        $.ajax({
            url: '{{ url("api/sales/currency") }}',
            type: 'get',
            data: {
                itemid: itemid,
                qty: qty
            },
            dataType: 'json',
            success: function(response){

                $('#txttotal'+id).val(response.total);

            },
            complete: function(){

                GrandTotal();

            }
        });

      }

      function GrandTotal(){

        var datatotal = $('[name="txttotal[]"]').serializeArray();
       
        grandtotal = 0.00;
        for(var i=0;i<datatotal.length;i++){

            if(datatotal[i].value!=""){
                grandtotal = grandtotal + parseFloat(datatotal[i].value);
            }
            
        }

        $('#lblgrandtotal').text('Grand Total: ' + grandtotal);

      }

      function LoadItemInformation(id, data){

        var datapo = $('[name="cmbpo[]"]').serializeArray();
        var dataitem = $('[name="cmbitem[]"]').serializeArray();
        var poid = $('#cmbpo'+id).val();
        var item = $('#cmbitem'+id).val();
        var validateitems = false;
        var count = 0;

        //Validation
        for(var i=0;i<datapo.length;i++){

            if(datapo[i].value==poid && dataitem[i].value==item){
                count ++;
            }
            if(count>=2){
                validateitems = true;
                break;
            }

        }

        if(validateitems){
            toastr.error("You have already pick this item in this PO.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/sales/loaditeminformation") }}',
                type: 'get',
                data: {
                    itemid: data
                },
                dataType: 'json',
                success: function(response){

                    $('#txtitemdescription'+id).val(response.idsize + " " + response.odsize);
                    $('#txtprice'+id).val(response.price);

                },
                complete: function(){

                    $('#txtqty'+id).prop('readonly', false);
                    $('#txtqty'+id).focus();

                }
            });

        }
        

      }

      function LoadPOItems(id, data){
        
        $.ajax({
            url: '{{ url("api/sales/getpoitems") }}',
            type: 'get',
            data: {
                poid: data
            },
            dataType: 'json',
            success: function(response){

                $('#cmbitem'+id).find('option').remove();
                $('#cmbitem'+id).append('<option value="" disabled selected>Select a Item</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#cmbitem'+id).append('<option value="'+  response.data[i]["itemid"] +'">'+  response.data[i]["finishgood"] +'</option>');
                }

                $('#cmbitem'+id).select2({
                    theme: 'bootstrap'
                })

            },
            complete: function(){
                $('#cmbitem'+id).prop('disabled', false);
            }
        });

      }

      function LoadCustomerPO(id, data){

        $.ajax({
            url: '{{ url("api/sales/getpocustomer") }}',
            type: 'get',
            data: {
                id: data
            },
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a PO #</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["poid"] +'">'+  response.data[i]["ponumber"] +'</option>');
                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                })

            },
            complete: function(){
                $('#'+id).prop('disabled', false);
            }
        });

      }

      function LoadCustomers(){

          $.ajax({
              url: "{{ url('api/sales/newpurchaseorder/loadcustomer') }}",
              type: 'get',
              dataType: 'json',
              success: function(response){

                    $('#cmbcustomer').find('option').remove();
                    $('#cmbcustomer').append('<option value="" disabled selected>Select a Customer</option>');
                    for (var i = 0; i < response.data.length; i++) {
                        $('#cmbcustomer').append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                    }

                    $('#cmbcustomer').select2({
                        theme: 'bootstrap'
                    })

              }
          });

      }

      function SetDatePicker(){

        $("#txtdeldate").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

      }

  </script>  

@endsection