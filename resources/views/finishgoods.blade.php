@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Finished Good Information</b></h3>
        </div>
        <div class="box-body">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="cmbcustomer">Customer</label>
                    <select class="form-control" id="cmbcustomer">
                    </select>
                </div>
            </div>
            <div class="col-md-9">
            <br>

                <button id="btnnewitem" name="btnnewitem" class="btn btn-success btn-flat" style="float:right; margin: 7px 0px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Item</strong></button>

                <button id="btnimport" name="btnimport" class="btn btn-primary btn-flat" style="float:right; margin: 7px 10px;"><i class="fa fa-sign-in" style="font-size:12px;"></i> <strong>Import</strong></button>

                <button id="btnprocess" name="btnprocess" class="btn btn-primary btn-flat" style="float:right; margin: 7px 0px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Process</strong></button>
                
            </div>
            <div class="col-md-12">
                <table id="tblfinishgoods" class="table">
                    <thead>
                        <tr>
                            <th>Item Code</th>
                            <th>Partnumber</th>
                            <th>Partname</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Process Code</th>
                            <th>Packing STD</th>
                            <th id="headercurrency">Currency</th>
                            <th id="headerprice">Price</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  @include('modal.finishgoods.maintenance.newitem')
  @include('modal.finishgoods.maintenance.updateitem')
  @include('modal.finishgoods.maintenance.importitem')
  @include('modal.finishgoods.maintenance.uploaditemattachment')
  

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblfinishgoods;
      var sfgid;

      $(document).ready(function() {

            LoadCustomer($('#cmbcustomer').attr('id'));
            tblfinishgoods = $('#tblfinishgoods').DataTable({
                autoWidth: false,
                ordering: false,
            });

            // tblfinishgoods.columns(6).visible(false);
            // tblfinishgoods.columns(7).visible(false);    

            LoadSelect2();

      });

      $("#cmbcustomer").on('change', function(){

            var customer =  $("#cmbcustomer").val();
            tblfinishgoods.destroy();
            tblfinishgoods = $('#tblfinishgoods').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/finishgoods/finishgoodsinformation") }}',
                data: {customer: customer},
            },
            columns : [
                {data: 'itemcode', name: 'itemcode'},
                {data: 'partnumber', name: 'partnumber'},
                {data: 'partname', name: 'partname'},
                {data: 'description', name: 'description'},
                {data: 'size', name: 'size'},
                {data: 'processcode', name: 'processcode'},
                {data: 'packingstd', name: 'packingstd'},
                {data: 'currency', name: 'currency'},
                {data: 'price', name: 'price'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
            });

            // tblfinishgoods.columns(6).visible(false);
            // tblfinishgoods.columns(7).visible(false);    

      });

      $('#btnnewitem').on('click', function(){

        @if(in_array(14, $access))

            $('#txtnprocesstip').text('Please Select A Process');

            ClearNewItem();

            $('#lblncurrency').show();
            $('#cmbncurrency').next(".select2-container").show();
            $('#lblnprice').show();
            $('#txtnprice').show();

            $('#modalnewitem').modal('toggle');

            // $('#lblncurrency').hide();
            // $('#cmbncurrency').next(".select2-container").hide();
            // $('#lblnprice').hide();
            // $('#txtnprice').hide();

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#cmbnprocess').on('change', function(){

        var processcode = $('#cmbnprocess').val();
        LoadProcessTip(processcode, "txtnprocesstip", "new");

      });      

      $('#btnsave').on('click', function(){

        var itemcode = $('#txtnitemcode').val();
        var partnumber =  $('#txtnpartnumber').val();
        var partname = $('#txtnpartname').val();
        var description = $('#txtndescription').val();
        var cid = $('#cmbncustomer').val();
        var processcode = $('#cmbnprocess').val();
        var idwidth = $('#txtnidwidth').val();
        var idlength = $('#txtnidlength').val();
        var idheight = $('#txtnidheight').val();
        var odwidth =  $('#txtnodwidth').val();
        var odlength = $('#txtnodlength').val();
        var odheight = $('#txtnodheight').val();
        var currencyid = $('#cmbncurrency').val();
        var price = $('#txtnprice').val();
        var packingstd = $('#txtnpackingstd').val();
        var file = $('#nattachment').val();

        if(itemcode==""){
            toastr.error("Please input the item code of the item.", '', { positionClass: 'toast-top-center' });
            $('#txtnitemcode').focus();
        }
        else if(partnumber==""){
            toastr.error("Please input the partnumber of the item.", '', { positionClass: 'toast-top-center' });
            $('#txtnpartnumber').focus();   
        }
        else if(partname==""){
            toastr.error("Please input the partname of the item.", '', { positionClass: 'toast-top-center' });
            $('#txtnpartname').focus();
        }
        else if(cid==""){
            toastr.error("Please select the customer of the item.", '', { positionClass: 'toast-top-center' });
            $('#cmbncustomer').focus();
        }
        else if(processcode==""){
            toastr.error("Please select the process of the item.", '', { positionClass: 'toast-top-center' });
            $('#cmbnprocess').focus();
        }
        else{

            $.ajax({
                url: '{{ url("api/finishgoods/newitem") }}',
                type: 'post',
                data: {
                    itemcode: itemcode,
                    partnumber: partnumber,  
                    partname: partname, 
                    description: description,
                    cid: cid,
                    processcode: processcode,
                    idwidth: idwidth,
                    idlength: idlength,
                    idheight: idheight,
                    odwidth: odwidth,
                    odlength: odlength,
                    odheight: odheight,
                    currencyid: currencyid,
                    price: price,
                    packingstd: packingstd
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $("#modalnewitem .close").click();
                        ReloadFinishGoods();

                        if(file!=""){

                            UploadAttachment(response.fgid);

                        }

                    }

                }
            });

        }


      });

      $(document).on('click', '#btnedit', async function(){

        @if(in_array(15, $access))

            var id = $(this).val();

            ClearUpdateItem();
            $('#lblucurrency').show();
            $('#cmbucurrency').next(".select2-container").show();
            $('#lbluprice').show();
            $('#txtuprice').show();

            // $('#lblucurrency').hide();
            // $('#cmbucurrency').next(".select2-container").hide();
            // $('#lbluprice').hide();
            // $('#txtuprice').hide();

            $.ajax({
                url: '{{ url("api/finishgoods/finishgoodsprofile") }}',
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function(response){

                    LoadInformation($('#cmbucustomer').attr('id'), $('#cmbuprocess').attr('id'), $('#cmbucurrency').attr('id'), response);
                    $('#btnupdate').val(id);

                }
            });

            $('#modalupdateitem').modal('toggle');

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnupdate').on('click', function(){

        var fgid = $(this).val();
        var itemcode = $('#txtuitemcode').val();
        var partnumber =  $('#txtupartnumber').val();
        var partname = $('#txtupartname').val();
        var description = $('#txtudescription').val();
        var cid = $('#cmbucustomer').val();
        var processcode = $('#cmbuprocess').val();
        var idwidth = $('#txtuidwidth').val();
        var idlength = $('#txtuidlength').val();
        var idheight = $('#txtuidheight').val();
        var odwidth =  $('#txtuodwidth').val();
        var odlength = $('#txtuodlength').val();
        var odheight = $('#txtuodheight').val();
        var currencyid = $('#cmbucurrency').val();
        var price = $('#txtuprice').val();
        var packingstd = $('#txtupackingstd').val();
        var file = $('#uattachment').val();

        if(itemcode==""){
            toastr.error("Please input the item code of the item.", '', { positionClass: 'toast-top-center' });
            $('#txtuitemcode').focus();
        }
        else if(partnumber==""){
            toastr.error("Please input the partnumber of the item.", '', { positionClass: 'toast-top-center' });
            $('#txtupartnumber').focus();
        }
        else if(partname==""){
            toastr.error("Please input the partname of the item.", '', { positionClass: 'toast-top-center' });
            $('#txtupartname').focus();
        }
        else if(cid==""){
            toastr.error("Please select the customer of the item.", '', { positionClass: 'toast-top-center' });
            $('#cmbucustomer').focus();
        }
        else if(processcode==""){
            toastr.error("Please select the process of the item.", '', { positionClass: 'toast-top-center' });
            $('#cmbuprocess').focus();
        }
        else{

            $.ajax({
                url: '{{ url("api/finishgoods/updateitem") }}',
                type: 'post',
                data: {
                    fgid: fgid,
                    itemcode: itemcode,
                    partnumber: partnumber,  
                    partname: partname, 
                    description: description,
                    cid: cid,
                    processcode: processcode,
                    idwidth: idwidth,
                    idlength: idlength,
                    idheight: idheight,
                    odwidth: odwidth,
                    odlength: odlength,
                    odheight: odheight,
                    currencyid: currencyid,
                    price: price,
                    packingstd: packingstd,
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $("#modalupdateitem .close").click();
                        ReloadFinishGoods();

                        if(file!=""){

                            UploadAttachment(response.fgid);

                        }

                    }

                }
            });

        }

      });

      $('#cmbuprocess').on('change', function(){

        var processcode = $('#cmbuprocess').val();
        LoadProcessTip(processcode, "txtuprocesstip", "update");

      });

      $('#btnimport').on('click', function(){

        $('#modalimportitem').modal('toggle');
        LoadCustomer('cmbimportcustomer');

      });

      $('#btnimporttemp').on('click', function(){

        window.open("{{ url('api/finishgoods/downloadexceltemp') }}", '_blank');

      });

      $('#btnimportdata').on('click', function(){

        $.confirm({
              title: 'Import',
              content: 'Import this selected file?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        ImportFGData();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

      });

      $('#nattachment').on('change', function(event){

        var file = $(this).val();
        if(file!=""){

            var tmppath = URL.createObjectURL(event.target.files[0]);
        
            $('#imgnattachment').attr('src', tmppath);

        }
        else{

            $('#imgnattachment').attr('src', '{{ asset("images/noimgattachment.png") }}')

        }

      });

      $(document).on('click', '#btnuploadattachment', function(){

        @if(in_array(15, $access))

            ClearUpload();
            sfgid = $(this).val();
            $('#modaluploadattachment').modal('toggle');

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#uattachment').on('change', function(event){

        var file = $(this).val();
        if(file!=""){

            var tmppath = URL.createObjectURL(event.target.files[0]);
        
            $('#imguploadattachment').attr('src', tmppath);

        }
        else{

            $('#imguploadattachment').attr('src', '{{ asset("images/noimgattachment.png") }}')

        }

      });

      $('#btnuupload').on('click', function(){

        UpdateAttachment(sfgid);

      });

      $('#btnprocess').on('click', function(){

        @if(in_array(16, $access))

            window.location = "{{ url('/process') }}";

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      function UploadAttachment(fgid){

        var form = new FormData();
        form.append("fgid", fgid);
        form.append("attachment", $("#nattachment")[0].files[0]);

        $.ajax({
            url: '{{ url("api/finishgoods/uploadattachment") }}',
            type: 'post',
            data: form,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){



            }
        });

      }

      function UpdateAttachment(fgid){

        var form = new FormData();
        form.append("fgid", fgid);
        form.append("attachment", $("#uattachment")[0].files[0]);

        $.ajax({
            url: '{{ url("api/finishgoods/updateattachment") }}',
            type: 'post',
            data: form,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function(response){

                if(response.success){

                    $('#modaluploadattachment .close').click();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

      }

      function ImportFGData(){

        var customer = $('#cmbimportcustomer').val();
        var file = $('#file').val();

        //Validation
        if(customer==""){
            toastr.error("Please select a customer.", '', { positionClass: 'toast-top-center' });
            $('#cmbimportcustomer').focus();
        }
        else if(file==""){
            toastr.error("Please select a file to import.", '', { positionClass: 'toast-top-center' });
        }
        else{

            var form = new FormData(); 
            form.append("cid", customer);
            form.append("file", $("#file")[0].files[0]);

            $.ajax({
                url: '{{ url("api/finishgoods/importcustomerfg") }}',
                type: 'post',
                data: form,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){

                    if(response.success){

                        ReloadFinishGoods();
                        $('#modalimportitem .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

      }

     function LoadInformation(customer, process, currency, data){

        $.ajax({
              url: '{{ url("api/finishgoods/loadcustomer") }}',
              type: 'get',
              dataType: 'json',
              beforeSend: function() {
                H5_loading.show();
              },
              success: function(response){

                $('#'+customer).find('option').remove();
                $('#'+customer).append('<option value="" disabled selected>Select a Customer</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+customer).append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                }

                $.ajax({
                    url: '{{ url("api/finishgoods/loadprocess") }}',
                    type: 'get',
                    dataType: 'json',
                    success: function(response){

                        $('#'+process).find('option').remove();
                        $('#'+process).append('<option value="" disabled selected>Select a Process</option>');
                        for (var i = 0; i < response.data.length; i++) {
                            $('#'+process).append('<option value="'+  response.data[i]["processcode"] +'">'+  response.data[i]["processcode"] +'</option>');
                        }

                        $.ajax({
                            url: '{{ url("api/finishgoods/loadcurrency") }}',
                            type: 'get',
                            dataType: 'json',
                            success: function(response){

                                $('#'+currency).find('option').remove();
                                $('#'+currency).append('<option value="" disabled selected>Select a Currency</option>');
                                for (var i = 0; i < response.data.length; i++) {
                                    $('#'+currency).append('<option value="'+  response.data[i]["currencyid"] +'">'+  response.data[i]["currency"] +'</option>');
                                }

                                FinishGoodProfile(data);

                            }
                        });

                    }
                });

              },
              complete: function(){
                H5_loading.hide();
              }
          });

     }

      function FinishGoodProfile(data){

        $('#txtuitemcode').val(data.itemcode);
        $('#txtupartnumber').val(data.partnumber);
        $('#txtupartname').val(data.partname);
        $('#txtudescription').val(data.description);
        $('#txtuidwidth').val(data.idwidth);
        $('#txtuidlength').val(data.idlength);
        $('#txtuidheight').val(data.idheight);
        $('#txtuodwidth').val(data.odwidth);
        $('#txtuodlength').val(data.odlength);
        $('#txtuodheight').val(data.odheight);
        $('#cmbucustomer').val(data.customer).trigger('change');
        $('#cmbuprocess').val(data.process).trigger('change');
        $('#cmbucurrency').val(data.currency).trigger('change');
        $('#txtuprice').val(data.price);
        $('#txtupackingstd').val(data.packingstd);

        if(data.attachment===null){
            $('#imguattachment').attr('src', '{{ asset("images/noimgattachment.png") }}');
        }
        else{
            $('#imguattachment').attr('src', '{{ asset("drawing") }}/'+data.attachment);
        }
        

        LoadProcessTip(data.process, "txtuprocesstip", "update");

      }

    function ClearNewItem(){

        $('#txtnitemcode').val('');
        $('#txtnpartnumber').val('');
        $('#txtnpartname').val('');
        $('#txtndescription').val('');
        LoadCustomer($('#cmbncustomer').attr('id'));
        LoadProcess($('#cmbnprocess').attr('id'));
        LoadCurrency($('#cmbncurrency').attr('id'));
        $('#txtnidwidth').val('');
        $('#txtnidlength').val('');
        $('#txtnidheight').val('');
        $('#txtnodwidth').val('');
        $('#txtnodlength').val('');
        $('#txtnodheight').val('');
        $('#txtnprice').val('');
        $('#txtnstyle').val('');
        $('#txtninkcolor').val('');
        $('#nattachment').val('');

    }

    function ClearUpdateItem(){

      $('#txtuitemcode').val('');
      $('#txtupartnumber').val('');
      $('#txtupartname').val('');
      $('#txtudescription').val('');
      LoadCustomer($('#cmbucustomer').attr('id'));
      LoadProcess($('#cmbuprocess').attr('id'));
      LoadCurrency($('#cmbucurrency').attr('id'));
      $('#txtuidwidth').val('');
      $('#txtuidlength').val('');
      $('#txtuidheight').val('');
      $('#txtuodwidth').val('');
      $('#txtuodlength').val('');
      $('#txtuodheight').val('');
      $('#txtuprice').val('');
      $('#txtupackingstd').val('');
      $('#txtustyle').val('');
      $('#txtuinkcolor').val('');
      $('#imguattachment').attr('src', '');

    }

    function ClearUpload(){

        $('#uattachment').val('');
        $('#imguploadattachment').attr('src', '{{ asset("images/noimgattachment.png") }}')

    }

    function LoadCustomer(id){
          
          $.ajax({
              url: '{{ url("api/finishgoods/loadcustomer") }}',
              type: 'get',
              dataType: 'json',
              success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Customer</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                }

              }
          });

      }

    function LoadProcess(id){
          
        $.ajax({
              url: '{{ url("api/finishgoods/loadprocess") }}',
              type: 'get',
              dataType: 'json',
              success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Process</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["processcode"] +'">'+  response.data[i]["processcode"] +'</option>');
                }

              }
          });
          
      }

    function LoadCurrency(id){

          $.ajax({
              url: '{{ url("api/finishgoods/loadcurrency") }}',
              type: 'get',
              dataType: 'json',
              success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Currency</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["currencyid"] +'">'+  response.data[i]["currency"] +'</option>');
                }

              }
          });

      }

      function LoadProcessTip(processcode, id, type){

        $.ajax({
            url: '{{ url("api/finishgoods/loadcodeprocess") }}',
            type: 'get',
            data: {
                processcode: processcode
            },
            dataType: 'json',
            success: function(response){

                var tooltip = "";
                var count;
                $('#'+id).text('');

                if(type=="new"){
                    $('#txtnstyle').val(response.style);
                    $('#txtninkcolor').val(response.inkcolor);
                }
                else{
                    $('#txtustyle').val(response.style);
                    $('#txtuinkcolor').val(response.inkcolor);
                }

                for(var i=0;i<response.data.length;i++){

                    count = i+1;
                    tooltip = tooltip + count.toString() + "." + response.data[i]["process"] + "\n";

                }
                $('#'+id).text(tooltip);
            }
        });

      }

      function LoadSelect2(){

        $('#cmbcustomer').select2({
            theme: 'bootstrap'
        });

        $('#cmbncustomer').select2({
            theme: 'bootstrap'
        });

        $('#cmbnprocess').select2({
            theme: 'bootstrap'
        });

        $('#cmbncurrency').select2({
            theme: 'bootstrap'
        });

        $('#cmbucustomer').select2({
            theme: 'bootstrap'
        });

        $('#cmbuprocess').select2({
            theme: 'bootstrap'
        });

        $('#cmbucurrency').select2({
            theme: 'bootstrap'
        });

        $('#cmbimportcustomer').select2({
            theme: 'bootstrap'
        });

      }

      function ReloadFinishGoods(){

          var customer = $('#cmbcustomer').val();
          if(customer!=null){
            tblfinishgoods.ajax.reload();
          }

      }

  </script>  

@endsection