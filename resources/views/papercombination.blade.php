@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Paper Combination</b></h3>
        </div>
        <div class="box-body">

          <div class="col-md-12">

            <div class="col-md-12">
                <button id="btnewpapercombination" name="btnewpapercombination" class="btn btn-success btn-flat" style="float: right;"><i class="fa fa-plus"></i> New Paper Combination</button>
            </div>

            <div class="col-md-12">
                <br>
                <table id="tblpapercombination" class="table">
                    <thead>
                        <tr>
                            <th>RM Paper Combination</th>
                            <th>Flute</th>
                            <th>Board Pound</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modal --}}
  @include('modal.papercombination.newpapercombination')
  @include('modal.papercombination.updatepapercombination')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblpapercombination;
    var spapercomid;
    var papercomdata;

    $(document).ready(function() {
    
        //Load
        LoadPaperCombination();

        //Set
        SetSelect2();

    });

    $('#btnewpapercombination').on('click', function(){

        ClearNewPaperCombination();
        LoadFlute("cmbnflute", "New");
        $('#newpapercombination').modal('toggle');

    });

    $('#btnsavepapercombination').on('click', function(){

        SavePaperCombination();

    });

    $('#cmbnflute').on('change', function(){

        var flute = $(this).val();

        LoadBoardPound("cmbnboardpound", "New", flute);

    });

    $(document).on('click', '#btnupdate', function(){

        spapercomid = $(this).val();

        $('#updatepapercombination').modal('toggle');

        LoadPaperCombinationInfo(spapercomid);

    });

    $('#btnupdatepapercombination').on('click', function(){

        UpdatePaperCombination();

    });

    $('#cmbuflute').on('change', function(){

        var flute = $(this).val();

        LoadBoardPound("cmbuboardpound", "New", flute);

    });

    function UpdatePaperCombination(){

        var rmpapercombination = $('#txturmpapercombination').val();
        var flute = $('#cmbuflute').val();
        var boardpound = $('#cmbuboardpound').val();

        if(rmpapercombination==""){

            toastr.error("Please input RM paper combination.", '', { positionClass: 'toast-top-center' });

        }
        else if(flute==""){

            toastr.error("Please select Flute.", '', { positionClass: 'toast-top-center' });


        }
        else if(boardpound==""){

            toastr.error("Please select boardpound.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/papercombination/updatepapercombination") }}',
                type: 'post',
                data: {
                    papercomid: spapercomid,
                    rmpapercombination: rmpapercombination,
                    flute: flute,
                    boardpound: boardpound
                },
                dataType: 'json',
                // statusCode: {
                //     422 : function(code){
                //         toastr.error('Paper combination information already exist.', '', { positionClass: 'toast-top-center' });
                //     }
                // },
                success: function(response){

                    if(response.success){

                        ReloadPaperCombination();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#updatepapercombination .close').click();

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    }

    function LoadPaperCombinationInfo(id){

        $.ajax({
            url: '{{ url("api/papercombination/loadpapercombinationinfo") }}',
            type: 'get',
            data: {
                papercomid: id
            },
            dataType: 'json',
            beforeSend: function(){

                $("#cmbuflute").select2('destroy');
                $("#cmbuboardpound").select2('destroy'); 

            },
            success: function(response){

                papercomdata = response;
                $('#txturmpapercombination').val(response.rmpapercombination);
                LoadFlute('cmbuflute', 'Update');
                LoadBoardPound('cmbuboardpound', 'Update', response.flute);

            }
        });

    }

    function SavePaperCombination(){

        var rmpapercombination = $('#txtnrmpapercombination').val();
        var flute = $('#cmbnflute').val();
        var boardpound = $('#cmbnboardpound').val();

        if(rmpapercombination==""){

           toastr.error("Please input RM paper combination.", '', { positionClass: 'toast-top-center' });

        }
        else if(flute==""){

           toastr.error("Please select Flute.", '', { positionClass: 'toast-top-center' });


        }
        else if(boardpound==""){

           toastr.error("Please select boardpound.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/papercombination/savepapercombination") }}',
                type: 'post',
                data: {
                    rmpapercombination: rmpapercombination,
                    flute: flute,
                    boardpound: boardpound
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadPaperCombination();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newpapercombination .close').click();

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    }

    function LoadFlute(id, action){

        $.ajax({
            url: '{{ url("api/papercombination/loadflute") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Flute</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["flute"] +'">'+  response.data[i]["flute"] +'</option>');
                }

            },
            complete: function(){

                if(action=="Update"){
                    $('#'+id).val(papercomdata.flute);
                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadBoardPound(id, action, flute){

        $.ajax({
            url: '{{ url("api/papercombination/loadboardpound") }}',
            type: 'get',
            data: {
                flute: flute
            },
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Board Pound</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["boardpound"] +'">'+  response.data[i]["boardpound"] +'</option>');
                }

            },
            complete: function(){

                if(action=="Update"){
                    $('#'+id).val(papercomdata.boardpound);
                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadPaperCombination(){

        tblpapercombination = $('#tblpapercombination').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: '{{ url("api/papercombination/loadpapercombination") }}',
            columns : [
                {data: 'rmpapercombination', name: 'rmpapercombination'},
                {data: 'flute', name: 'flute'},
                {data: 'boardpound', name: 'boardpound'},
                {data: 'createdat', name: 'createdat'},
                {data: 'updatedat', name: 'updatedat'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

    }

    function ReloadPaperCombination(){

        tblpapercombination.ajax.reload();

    }

    function ClearNewPaperCombination(){

        $('#txtnrmpapercombination').val('');

    }

    function SetSelect2(){

        $("#cmbuflute").select2();
        $("#cmbuboardpound").select2(); 

    }

  </script>  

@endsection