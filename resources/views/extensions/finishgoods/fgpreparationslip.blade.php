@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Preparation Slip</b></h3>
          <a href="{{ url("/fginventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

          <div class="col-md-12">

            <div class="col-md-12 row">
                <table id="tblpreparationslip" class="table">
                    <thead>
                        <tr class="header">
                            <th>Slip #</th>
                            <th>Total Quantity</th>
                            <th>Issued By</th>
                            <th>Created Date</th>
                            <th>Customer</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div> 

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
            

          </div> 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modal --}}
  @include('modal.finishgoods.inventory.fgpreparationslipinfo')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblpreparationslip;
      var spreparationid;

      $(document).ready(function(){

        //Load
        LoadPreparationSlipInformation();

      });

      $(document).on('click', '#btnpreparationinfo', function(){

        spreparationid = $(this).val();

        $('#fgpreparationslipinfo').modal('toggle');
        LoadPreparationSlipItems(spreparationid);

      });

      $('#btnapprovepreparationslip').on('click', function(){

        $.confirm({
            title: 'Approve',
            content: 'Approve this preparation slip information?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){

                        Approved(spreparationid);

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                      
                    }
                } 
            }
        });

      });

      $('#btndisapprovepreparationslip').on('click', function(){

        $.confirm({
            title: 'Dispprove',
            content: 'Disapprove this preparation slip information?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){

                        Disapproved(spreparationid);

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                      
                    }
                } 
            }
        });

      });

      function Approved(preparationid){

        $.ajax({
            url: '{{ url("api/finishgoodsinv/approvepreparationslip") }}',
            type: 'post',
            data: {
                preparationid: preparationid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#fgpreparationslipinfo .close').click();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadPreparationSlipInformation();

                }

            }
        });

      }

      function Disapproved(preparationid){

        var remarks = $('#txtremarks').val();

        if(remarks==""){

            toastr.error('Please input the remarks.', '', { positionClass: 'toast-top-center' });
            $('#txtremarks').focus();

        }
        else{
        
            $.ajax({
                url: '{{ url("api/finishgoodsinv/disapprovepreparationslip") }}',
                type: 'post',
                data: {
                    preparationid: preparationid,
                    remarks: remarks
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#fgpreparationslipinfo .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadPreparationSlipInformation();

                    }

                }
            });

        }


      }

      function LoadPreparationSlipItems(preparationid){

        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadpreparationslipitems") }}',
            type: 'get',
            data: {
                preparationid: preparationid
            },
            dataType: 'json',
            success: function(response){

                //Set
                $('#lblinformation').text('');
                $('#lblinformation').text(response.preparationnumber);

                $('#tblpreparationslipitemscontent').find('tr').remove();
                for(var i=0;i<response.preparationitems.length;i++){

                    $('#tblpreparationslipitemscontent').append('<tr><td>'+ response.preparationitems[i]["partnumber"] +'</td> <td>'+ response.preparationitems[i]["partname"] +'</td> <td>'+ response.preparationitems[i]["description"] +'</td> <td>ID Size: '+ response.preparationitems[i]["idsize"] +' <br>OD_Size: '+ response.preparationitems[i]["odsize"] +'</td> <td>'+ response.preparationitems[i]["qty"] +'</td> </tr>');

                }

            }
        });

      }

      function LoadPreparationSlipInformation(){

        tblpreparationslip = $('#tblpreparationslip').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/finishgoodsinv/loadpreparationslipinformation") }}'
            },
            columns : [
                {data: 'psnumber', name: 'psnumber'},
                {data: 'totalqty', name: 'totalqty'},
                {data: 'issuedby', name: 'issuedby'},
                {data: 'createdat', name: 'createdat'},
                {data: 'customer', name: 'customer'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

      }

      function ReloadPreparationSlipInformation(){

        tblpreparationslip.ajax.reload();

      }

      //Socket Function
      socket.on('reloadfgpreparationslip', function(){

        ReloadPreparationSlipInformation();

      });

      
  </script>  

@endsection