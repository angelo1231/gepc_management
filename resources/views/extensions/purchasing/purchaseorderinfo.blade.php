@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-file"></i><b> PO# {{ $info[0]->ponumber }}</b></h3>
          <a href="{{ url("/purchasing") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                    <article class="col-md-6">
                            <div class="form-group">
                                
                                    <label for="txtsupplier">Supplier</label>
                                    <input id="txtsupplier" name="txtsupplier" type="text" class="form-control" placeholder="Supplier" style="width: 350px;" value="{{ $info[0]->supplier }}" readonly>

                            </div>
                            {{-- <div class="form-group">
                                
                                    <label for="txtaddress">Address</label>
                                    <input id="txtaddress" name="txtaddress" type="text" class="form-control" placeholder="Address" value="{{ $info[0]->address }}" readonly>
                             
                            </div> --}}
                            <div class="form-group">
                        
                                    <label for="txtattn">Attention To</label>
                                    <input id="txtattn" name="txtattn" type="text" class="form-control" placeholder="Attn" value="{{ $info[0]->attentionto }}" readonly>
                             
                            </div>
                            <div class="form-group">
                        
                                    <label for="txtdelto">Deliver To</label>
                                    <input id="txtdelto" name="txtdelto" type="text" class="form-control" placeholder="Deliver To" value="{{ $info[0]->deliveryto }}" readonly>
                
                            </div>  
                    </article>
                    <article class="col-md-6">
                            <div class="form-group">
                        
                                    <label for="txtdelto">Delivery Date</label>
                                    <input id="txtdeldate" name="txtdeldate" type="text" class="form-control" placeholder="Delivery Date" style="width: 320px;" value="{{ $deliverydates }}" readonly>
                
                            </div>  
                            <div class="form-group">
                                
                                    <label for="txttin">TIN</label>
                                    <input id="txttin" name="txttin" type="text" class="form-control" placeholder="TIN" style="width: 320px;" value="{{ $info[0]->tin }}" readonly>
                
                            </div>
                            <div class="form-group">
                                
                                    <label for="txtterms">Terms</label>
                                    <input id="txtterms" name="txtterms" type="text" class="form-control" placeholder="Terms" style="width: 320px;" value="{{ $info[0]->terms }}" readonly>
                
                            </div>   
                    </article>
                    <article class="col-md-12">
                            <table id="tblitems" class="table table-hover" style="margin-top: 10px;">
                                <tr>
                                    <th>Item</th>
                                    <th>Description</th>
                                    <th>Flute</th>
                                    <th>Board Pound</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                    <th>Delivery</th>
                                    <th>Deliverd Qty</th>
                                    <th>Total Good</th>
                                    <th>Total Reject</th>
                                    <th>Balance</th>                                    
                                    <th></th>
                                </tr>
                                <tbody id="tblitemcontent">
                                    @foreach($items as $item)
                                    <?php
    
                                        $deliverycontent = "";
                                        $size = "";
                                        if($item->category=="Raw Material"){
    
                                            if($item->widthsize!=""){
                                                $size = $size . $item->width . "." . $item->widthsize;
                                            }else{
                                                $size = $size . $item->width;
                                            }
    
                                            if($item->lengthsize!=""){
                                                $size = $size . " x " . $item->length . "." . $item->lengthsize;
                                            }else{
                                                $size = $size . " x " . $item->length;
                                            }
    
                                        }
                                        else if($item->category=="Other"){
    
                                            $size = $item->name;
    
                                        }
                                        
                                        $delinfo = explode(',', $item->deliveryinfo);

                                        foreach($delinfo as $del){

                                            $deliverycontent .= $del . "<br>";

                                        }

                                    ?>
                                    <tr>
                                        <td>{{ $size }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->flute }}</td>
                                        <td>{{ $item->boardpound }}</td>
                                        <td>{{ number_format($item->price, 2) }}</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>{{ number_format($item->total, 2) }}</td>
                                        <td><?php echo $deliverycontent;  ?></td>
                                        <td>{{ $item->totaldelqty }}</td>
                                        <td>{{ $item->delqty }}</td>
                                        <td>{{ $item->totalreject }}</td>
                                        <td>{{ $item->balance }}</td>
                                        <td>
                                            @if($info[0]->hasapproved=="False")
                                                <button id="btnedit" name="btnedit" class="btn btn-flat btn-info" value="{{ $item->itemid }}"><i class="fa fa-edit"></i></button>                                                
                                            @endif
                                        </td>
                                    </tr>    
                                    @endforeach
                                </tbody>
                                
                            </table>
                            <label for="txtdelprice" style="float: left; margin-top: 5px; margin-left: 10px;">Delivery Charge </label>
                            <input id="txtdelprice" name="txtdelprice" type="number" class="form-control" placeholder="Terms" value="{{ number_format($info[0]->deliverycharge, 2) }}" style="width: 150px; float: left; margin-left: 10px;" readonly>
        
                            <label id="lblgrandtotal" style="float: right; margin-right: 72px;">
                                Grand Total: {{ number_format($info[0]->grandtotal, 2) }}
                            </label>
                    </article>
                    <article class="col-md-12">
                        <div id="divapprove">
                           
                            @if($info[0]->hasapproved=="False")
                                <button id="btnapprove" name="btnapprove" class="btn btn-flat btn-primary" style="float: right; margin-top: 15px;">Approve</button>
                            @endif

                        </div>
                    </article>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

  {{-- Modal --}}
  @include('modal.purchasing.editpurchaseitem')

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var poid = '{{ $poid }}';
    var sitemid;
    var authinfo = [];
    var auth = 0;

    $('#btnapprove').on('click', function(){

        $.confirm({
            title: 'Approve',
            content: 'Approve this purchase order?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){

                      ApprovePO();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                      

                    }
                } 
            }
        });

    });

    $(document).on('click', '[name="btnedit"]', function(){

      sitemid = $(this).val();

      if(auth==0){
        ClearAuth();
        $('#auth').modal('toggle');
      }
      else{
        GetItemInformation(sitemid);
        $('#editpurchaseitem').modal('toggle');
      }

    });

    $(document).on('click', '#btnauthlogin', function(){

        var validation = AuthLogin();

        validation.done(function(data){

            if(data.success){

                authinfo = {
                    id: data.id,
                    name: data.name
                };

                $('#auth').modal('toggle');
                setTimeout(function(){ 
                
                  auth = 1;
                  GetItemInformation(sitemid);
                  $('#editpurchaseitem').modal('toggle');

                }, 500);

            }

        });

    });

    $('#btnsaveeditinfo').on('click', function(){

        $.confirm({
            title: 'Update',
            content: 'Update this purchase order item?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){

                        UpdateItemInformation();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                      

                    }
                } 
            }
        });

    });

    function UpdateItemInformation(){

        var price = $('#txtnewprice').val();
        var qty = $('#txtnewqty').val();

        $.ajax({
            url: '{{ url("api/purchasing/updateiteminformation") }}',
            type: 'post',
            data: {
                poid: poid,
                itemid: sitemid,
                price: price,
                qty: qty
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#editpurchaseitem .close').click();
                    ReloadPOItemInformation();
                    $('#lblgrandtotal').text('');
                    $('#lblgrandtotal').text('Grand Total: ' + response.grandtotal);

                }

            }
        });

    }

    function ReloadPOItemInformation(){

        $.ajax({
            url: '{{ url("api/purchasing/reloadpoiteminformation") }}',
            type: 'get',
            data: {
                poid: poid
            },
            dataType: 'json',
            success: function(response){

                $('#tblitemcontent').find('tr').remove();
                $('#tblitemcontent').append(response.content);

            }
        });

    }

    function GetItemInformation(itemid){

        $.ajax({
            url: '{{ url("api/purchasing/getiteminformation") }}',
            type: 'get',
            data: {
                itemid: itemid
            },
            dataType: 'json',
            success: function(response){

                $('#txtnewprice').val(response.price);
                $('#txtnewqty').val(response.qty);

            }
        });

    }   

    function ApprovePO(){

        $.ajax({
            url: '{{ url("api/purchasing/approvepo") }}',
            type: 'post',
            data: {
                poid: poid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#divapprove').hide();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    ReloadPOItemInformation();
                    POConvertPDF();

                }
                else{

                    toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

    function POConvertPDF(){

        $.ajax({
            url: '{{ url("api/purchasing/poconvertpdf") }}',
            type: 'post',
            data: {
                poid: poid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    SendPOEmail();

                }

            }
        });

    }

    function SendPOEmail(){

        $.ajax({
            url: '{{ url("api/purchasing/sendpoemail") }}',
            type: 'post',
            data: {
                poid: poid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    

                }

            }
        });

    }

  </script>  

@endsection