@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Process Information</b></h3>
          <a href="{{ url("/finishgoods") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-12" style="margin-bottom: 15px;">

                <button id="btngroupprocess" name="btngroupprocess" class="btn btn-flat btn-primary" style="float: right;"><i class="fa fa-cog"></i> Group Process</button>

                <button id="btnnewprocess" name="btnnewprocess" class="btn btn-success btn-flat" style="float: right; margin-right: 10px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Process</strong></button>     
            </div> 
          <div class="col-md-12">
              <table id="tblprocess" class="table">
                  <thead>
                      <tr>
                          <th>Process Id</th>
                          <th>Process</th>
                          <th>For Final Inspection</th>
                          <th></th>
                      </tr>
                  </thead>
              </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modals --}}
  @include('modal.process.newprocess');
  @include('modal.process.updateprocess');

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblprocess;


      $(document).ready(function() {

        //Load
        LoadProcess();

        //Set
        $('#txtnforcoc').select2({
          theme: 'bootstrap'
        });

        $('#txtuforcoc').select2({
          theme: 'bootstrap'
        });


      });

      $('#btnnewprocess').on('click', function(){

        @if(in_array(17, $access))

            ClearNewProcess();
            $('#modalnewprocess').modal('toggle');

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnsave').on('click', function(){

        var process = $('#txtnprocess').val();
        var forcoc = $('#txtnforcoc').val();

        if(process==""){
            toastr.error('Please input the process name.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/process/saveprocess") }}',
                type: 'post',
                data: {
                    process: process,
                    forcoc: forcoc
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $("#modalnewprocess .close").click()
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadProcess();

                    }
                    else{

                        $('#txtnprocess').val('');
                        $('#txtnprocess').focus();
                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

      });

      $(document).on('click', '#btnedit', function(){

        @if(in_array(18, $access))

            var pid = $(this).val();
            ClearUpdateProcess();

            $.ajax({
                url: '{{ url("api/process/processprofile") }}',
                type: 'get',
                data: {
                    pid: pid
                },
                dataType: 'json',
                success: function(response){

                    $('#txtuprocess').val(response.process);
                    $('#txtuforcoc').val(response.forcoc).trigger('change');
                    $('#btnupdate').val(pid);

                }
            });

            $('#modalupdateprocess').modal('toggle');

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnupdate').on('click', function(){

        var pid = $('#btnupdate').val();
        var process = $('#txtuprocess').val();
        var forcoc = $('#txtuforcoc').val();

        if(process==""){
            toastr.error('Please input the process name.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/process/updateprocess") }}',
                type: 'post',
                data: {
                    pid: pid,
                    process: process,
                    forcoc: forcoc
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $("#modalupdateprocess .close").click()
                        ClearUpdateProcess();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadProcess();

                    }

                }
            });

        }

      });

      $('#btngroupprocess').on('click', function(){

        @if(in_array(19, $access))

            window.location = "{{ url('process/groupprocess') }}";

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif


      });

      function ClearNewProcess(){

        $('#txtnprocess').val('');
        $('#txtnforcoc').val(0).trigger('change');

      }

      function LoadProcess(){

          tblprocess = $('#tblprocess').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: '{{ url("api/process/processinformation") }}',
            columns : [
                {data: 'id', name: 'id'},
                {data: 'process', name: 'process'},
                {data: 'forfinalinspection', name: 'forfinalinspection'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
          });

      }

      function ClearUpdateProcess(){

        $('#txtuprocess').val('');

      }

      function ReloadProcess(){
          tblprocess.ajax.reload();
      }

  </script>  

@endsection