<div class="col-md-12 row">
<table id="tblaudit" class="table">
        <thead>
            <tr>
              <th>Partnumber</th>
              <th>Partname</th>
              <th>Description</th>
              <th>ID Size</th>
              <th>OD Size</th>
              <th>JO#</th>
              <th>DR#</th>
              <th>Quantity</th>
              <th>Balance</th>
              <th>Issued By</th>
              <th>Issued Date</th>
              <th>Issued Time</th>
            </tr>
        </thead>
        <tbody>
            @foreach($audit as $data)
                <tr>
                    <th>{{ $data->partnumber }}</th>
                    <th>{{ $data->partname }}</th>
                    <th>{{ $data->description }}</th>
                    <th>{{ $data->idsize }}</th>
                    <th>{{ $data->odsize }}</th>
                    <th>{{ $data->jonumber }}</th>
                    <th>{{ $data->drnumber }}</th>
                    <th>{{ $data->qty }}</th>
                    <th>{{ $data->balance }}</th>
                    <th>{{ $data->issuedby }}</th>
                    <th>{{ $data->issueddate }}</th>
                    <th>{{ $data->issuedtime }}</th>
                </tr>
            @endforeach
        </tbody>
</table>
</div>
<script>

    $(document).ready(function(){

        $('#tblaudit').DataTable({
            autoWidth: false,
            ordering: false,
        });
           
    });

</script>