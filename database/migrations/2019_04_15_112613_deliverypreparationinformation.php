<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Deliverypreparationinformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliverypreparationsinformation', function (Blueprint $table) {
            $table->increments('preparationid');
            $table->string('preparationnumber', 60);
            $table->integer('cid');
            $table->text('issuedby');
            $table->text('approvedby');
            $table->datetime('approveddate');
            $table->text('disapprovedby');
            $table->datetime('disapproveddate');
            $table->longtext('remarks');
            $table->text('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliverypreparationsinformation');        
    }
}
