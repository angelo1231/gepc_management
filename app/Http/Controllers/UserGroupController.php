<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserGroup_Model;
use Auth;
use DataTables;
use Illuminate\Support\Collection;
use App\Http\Controllers\UserController;

class UserGroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index(){

        $access = UserController::UserGroupAccess();

        return view('usergroup')
        ->with('access', $access);

    }

    function SaveUserGroup(Request $request){

        $validation = UserGroup_Model::ValidateUserGroup($request->usergroup);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"User Group information already exist."
            ]);

        }
        else{

            UserGroup_Model::SaveUserGroup($request->usergroup);

            return json_encode([
                "success"=>true,
                "message"=>"User Group information has been save."
            ]);

        }

    }

    function LoadUserGroupInformation(){

        $usergroup = UserGroup_Model::LoadUserGroupInformation();
        $data = array();

        for($i=0;$i<count($usergroup);$i++){

            $obj = new \stdClass;

            $obj->id = $usergroup[$i]->ugroupid;
            $obj->usergroup = $usergroup[$i]->usergroup;
            $obj->access = $usergroup[$i]->access;
            $obj->panel = '<button id="btnaddaccess" name="btnaddaccess" class="btn btn-flat btn-info" value="'. $usergroup[$i]->ugroupid .'" title="Add Access"><i class="fa fa-users"></i></button>';

            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns(['panel'])
        ->make(true);

    }

    function LoadAccess(){

        $access = UserGroup_Model::LoadAccess();

        return json_encode([
            "data"=>$access
        ]);

    }

    function SaveUserGroupAccess(Request $request){

        $sql = "INSERT INTO useraccess (ugroupid, accessid, created_at) VALUES ";
        if(isset($request->access)){

            for($i=0;$i<count($request->access);$i++){
    
                if($i+1>=count($request->access)){
                    $sql .= "('". $request->ugroupid ."', '". $request->access[$i] ."', NOW());";
                }
                else{
                    $sql .= "('". $request->ugroupid ."', '". $request->access[$i] ."', NOW()),";
                }
    
            }

        }

        //Delete User Group Access
        UserGroup_Model::DeleteUserGroupAccess($request->ugroupid);

        //Save-Update User Group Access
        if(isset($request->access)){
            UserGroup_Model::SaveUserGroupAccess($sql);
        }

        return json_encode([
            "success"=>true,
            "message"=>"User group acess has been save."
        ]);

    }

    function GetUserGroupAccess(Request $request){

        $access = array();
        $dataaccess = UserGroup_Model::GetUserGroupAccess($request->ugroupid);

        if(count($dataaccess)!=0){
            $access = explode(',', $dataaccess[0]->access);
        }
  
        return json_encode([
            "access"=>$access
        ]);

    }

}
