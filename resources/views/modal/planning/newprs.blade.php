<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newprs" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>NEW PURCHASE REQUEST</strong></label>
              <button id="btnicancelprx" name="btnicancelprx" type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <table class="table table-bordered" style="width: 100%">
                            <tr>
                                <th style="vertical-align: middle; width: 30%;">Purchase Request #</th>
                                <th colspan="3">
                                    <input id="txtiprnumber" name="txtiprnumber" class="form-control" placeholder="Purchase Request Number" type="text" readonly>
                                </th>
                            </tr>
                            <tr>
                                <th style="vertical-align: top;">Reason</th>
                                <th colspan="3">
                                    <textarea class="form-control" rows="3" id="txtireason" name="txtireason" placeholder="Your Reason For Request"></textarea>
                                </th>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle;">Item</th>
                                <th style="vertical-align: middle;">Quantity</th>
                                <th style="vertical-align: middle;">Delivery Info</th>
                                <th style="vertical-align: middle; text-align: center;">
                                    <button id="btinewprsoitem" name="btinewprsoitem" class="btn btn-flat btn-success"><i class="fa fa-plus"></i></button>
                                </th>
                            </tr>
                            <tbody id="tblnewprcontent" style="border-top: none;">
                                <tr>
                                    <td colspan="4" style="text-align: center;">No Item</td>
                                </tr>
                            </tbody>
                            <tbody id="tblnewprsave" style="border-top: none;">
                                {{-- Button Here --}}
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btnisavepr" name="btnisavepr" type="button" class="btn btn-primary btn-flat">Request</button>
                        <button id="btnicancelpr" name="btnicancelpr" type="button" class="btn btn-danger btn-flat">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>