<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings_Model;
use Auth;
use App\Http\Controllers\UserController;
use DataTables;
use Illuminate\Support\Collection;

class SettingsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){
    
        $access = UserController::UserGroupAccess();

        return view('settings')
        ->with('access', $access);
        
    }

    function LoadSettings(){

        $settings = Settings_Model::LoadSettings();

        return json_encode([
            "data"=>$settings
        ]);

    }

    function SaveTaxClassification(Request $request){

        Settings_Model::SaveTaxClassification($request);

        return json_encode([
            "success"=>true,
            "message"=>"Tax Classification information has been update."
        ]);

    }

    function LoadCompanyInfo(){

        $settings = Settings_Model::LoadCompanyInfo();

        return json_encode([
            "data"=>$settings
        ]);

    }

    function SaveCompanyInfo(Request $request){

        Settings_Model::SaveCompanyInfo($request);

        return json_encode([
            "success"=>true,
            "message"=>"Company information has been update."
        ]);

    }

    static function GetCompanyInfo(){

        $companyinfo = Settings_Model::GetCompanyInfo();

        return $companyinfo;

    }

    function LoadRawMaterialSettings(){

        $settings = Settings_Model::LoadRawMaterialSettings();

        return json_encode([
            "data"=>$settings
        ]);

    }

    function SaveRawMaterial(Request $request){

        Settings_Model::SaveRawMaterial($request);

        return json_encode([
            "success"=>true,
            "message"=>"Raw Material Setting has been update."
        ]);

    }

    function LoadUserGroup(){

        $content = "";
        
        $usergroupinfo = Settings_Model::LoadUserGroup();

        $content .= '<option value="" disabled selected>Select a User Group</option>';
        foreach($usergroupinfo as $val){

            $content .= '<option value="'. $val->ugroupid .'">'. $val->usergroup .'</option>';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function SaveEditAccess(Request $request){

        //Validation
        $validation = Settings_Model::ValidateEditAccess($request->ugroupid);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"User group access already exist."
            ]);

        }
        else{

            Settings_Model::SaveEditAccess($request->ugroupid);

            return json_encode([
                "success"=>true,
                "message"=>"Edit access information has been save."
            ]);

        }

    }

    function LoadEditAccessInformation(){

        $editaccessinfo = Settings_Model::LoadEditAccessInformation();
        
        $data = array();
        for($i=0;$i<count($editaccessinfo);$i++){
            
            $obj = new \stdClass;

            $obj->id = $editaccessinfo[$i]->id;
            $obj->usergroup = $editaccessinfo[$i]->usergroup;
            $obj->panel = '
                <button id="btnupdateeditaccess" class="btn btn-flat btn-info" value="'. $editaccessinfo[$i]->id .'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button> 
                <button id="btndeleteeditaccess" class="btn btn-flat btn-danger" value="'. $editaccessinfo[$i]->id .'"><i class="fa fa-trash" aria-hidden="true"></i></button>
            ';
            
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns([
            'panel'
        ])
        ->make(true);

    }

    function LoadEditAccessProfile(Request $request){

        $content = "";
        
        $usergroupinfo = Settings_Model::LoadUserGroup();
        $editaccessinfo = Settings_Model::LoadEditAccessProfile($request->id);

        $content .= '<option value="" disabled selected>Select a User Group</option>';
        foreach($usergroupinfo as $val){

            if($editaccessinfo->ugroupid==$val->ugroupid){
                $content .= '<option value="'. $val->ugroupid .'" selected>'. $val->usergroup .'</option>';
            }
            else{
                $content .= '<option value="'. $val->ugroupid .'">'. $val->usergroup .'</option>';
            }

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function UpdateEditAccess(Request $request){

        Settings_Model::UpdateEditAccess($request->id, $request->ugroupid);

        return json_encode([
            "success"=>true,
            "message"=>"Successfully update edit access information."
        ]);

    }

    function DeleteEditAccess(Request $request){

        Settings_Model::DeleteEditAccess($request->id);

        return json_encode([
            "success"=>true,
            "message"=>"Successfully delete edit access information."
        ]);

    }

}
