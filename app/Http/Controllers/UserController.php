<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use DataTables;
use App\User_Model;
use Illuminate\Support\Collection;
use App\User;
use Illuminate\Support\Facades\Hash;
use Response;
use Illuminate\Support\Str;
use Session;
use App\Http\Controllers\UserController;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index($action=null, $view=null, $id=null){

        $access = UserController::UserGroupAccess();

        if($view==null && $action==null){

            return view('users')
            ->with('access', $access);

        }
        else{

            if($action=="new"){

                return view('extensions.users.'.$view)
                ->with('access', $access);

            }
            else if($action=="update"){

                //Validation
                $validation = User_Model::ValidateUserRestriction($id);

                if($validation){

                    User_Model::AddUserRestriction($id);

                }

                $userinfo = User_Model::UserProfile($id);

                return view('extensions.users.'.$view)
                ->with('userinfo', $userinfo)
                ->with('uid', $id)
                ->with('access', $access);


            }

        }

    }

    function UserInformation(){

        $id = Auth::user()->id;
        $users = User_Model::UserInformation($id);
        $data = array();
        for($i=0;$i<count($users);$i++){
            $obj = new \stdClass;
            $obj->firstname = $users[$i]->firstname;
            $obj->lastname = $users[$i]->lastname;
            $obj->mi = $users[$i]->mi;
            $obj->username = $users[$i]->username;
            $obj->usertype = $users[$i]->usertype;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $users[$i]->id .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button> 
                           <button id="btnchangepassword" name="btnchangepassword" class="btn btn-success btn-flat" title="Change Password" value="'. $users[$i]->id .'" style="width: 40px;"><i class="fa fa-key"></i></button>
                           <button id="btnreset" name="btnreset" class="btn btn-danger btn-flat" title="Reset Password" value="'. $users[$i]->id .'" style="width: 40px;"><i class="fa fa-refresh"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function RegisterUser(Request $request){

        $username_count = User_Model::ValidateUsername($request->username);
   
        if($username_count == 0){

            $request['password'] = bcrypt($request->password);
            User_Model::RegisterUser($request);

            Session::flash('message', "User account has been register.");
            return json_encode(["success"=>true]);
            
        }
        else{

            return json_encode(["success"=>false, "message"=>'Username alrady exist.']); 
            
        }


    }

    function UpdateProfile(Request $request){

        User_Model::UpdateProfile($request);
        
        Session::flash('message', "User account has been update.");
        return json_encode([
            "success"=>true
        ]);

    }

    function ChangePassword(Request $request){

        $id = $request->id;
        $oldpassword = User_Model::OldPassword($id);

        if(Hash::check($request->oldpassword, $oldpassword)){
           
            if($request->newpassword==$request->confirmpassword){

                $user = User::find($id);
                $user->password = bcrypt($request->newpassword);
                $user->save();

                return json_encode(['success'=>true, 'message'=>'Account password has been change.']);

            }
            else{
                return json_encode(['success'=>false, 'message'=>'New password do not match confirm password.', 'type'=>'matchpassword']);
            }

        }
        else{
            return json_encode(['success'=>false, 'message'=>'Old password do not match.', 'type'=>'oldpassword']);
        }

    }


    function ResetPassword(Request $request){

        $id = $request->id;
        $password = Str::random(10);

        $user = User::find($id);
        $filename = $user->lastname . " " . $user->firstname . " " . $user->mi . ".txt";
        $user->password = bcrypt($password);
        $user->save();

        return json_encode(["success"=>true, "filename"=>$filename, "password"=>$password]);

    }

    function LoadUserGroup(){

        $usergroup = User_Model::LoadUserGroup();

        return json_encode([
            "data"=>$usergroup
        ]);

    }

    function GetUsertype(){

        $usertype = User_Model::GetUsertype(Auth::user()->usertype);

        return json_encode([
            "usertype"=>$usertype
        ]);

    }

    static function UserGroupAccess(){

        $access = array();
        $dataaccess = User_Model::UserGroupAccess(Auth::user()->usertype);

        if(count($dataaccess)!=0){
            $access = explode(',', $dataaccess[0]->access);
        }

        return $access;

    }


}
