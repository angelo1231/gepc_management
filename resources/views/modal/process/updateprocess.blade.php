<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modalupdateprocess" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>PROCESS INFORMATION</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtuprocess">Process</label>
                            <input type="text" id="txtuprocess" name="txtuprocess" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtuforcoc">For Final Inspection</label>
                            <select name="txtuforcoc" id="txtuforcoc" class="form-control">
                              <option value="0">Disabled</option>
                              <option value="1">Enabled</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-block btn-flat" style="margin-top: 10px;">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>