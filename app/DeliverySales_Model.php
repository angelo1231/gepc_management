<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class DeliverySales_Model extends Model
{
    
    public static function LoadCustomer(){

        $result = DB::table('customers')
        ->select(
            'cid',
            'customer'
        )
        ->get();

        return $result;

    }

    public static function LoadDeliverySalesInformation($data){

        $result = DB::table('invoiceinformation')
        ->select(
            DB::raw("DATE_FORMAT(invoiceinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
            'invoiceinformation.invoicenumber',
            'deliveryinformation.drnumber',
            DB::raw("GROUP_CONCAT(deliveryitems.description SEPARATOR '<br>') AS 'customeritem'"),
            DB::raw("GROUP_CONCAT(deliveryitems.qty SEPARATOR '<br>') AS 'qty'"),
            DB::raw("GROUP_CONCAT(deliveryitems.price SEPARATOR '<br>') AS 'unitprice'"),
            DB::raw("GROUP_CONCAT(deliveryitems.total SEPARATOR '<br>') AS 'total'")
        )
        ->join('deliveryinformation', 'deliveryinformation.did', '=', 'invoiceinformation.did')
        ->join(DB::raw("
            (SELECT
            deliveryinformationitems.itemid,
            deliveryinformationitems.did,
            finishgoods.description,
            deliveryinformationitems.qty,
            deliveryinformationitems.price,
            deliveryinformationitems.total
            FROM deliveryinformationitems
            INNER JOIN finishgoods ON finishgoods.fgid=deliveryinformationitems.fgid) deliveryitems
        "), 'deliveryitems.did', '=', 'deliveryinformation.did')
        ->where('invoiceinformation.cid', '=', $data->cid)
        ->groupBy('invoiceinformation.invoiceid')
        ->get();

        return $result;

    }

    public static function LoadSalesSummary($data){

        $result = DB::table('invoiceinformation')
        ->select(
            DB::raw("DATE_FORMAT(invoiceinformation.created_at, '%Y-%m-%d') AS 'created_at'"),
            'invoiceinformation.invoicenumber',
            'deliveryinformation.drnumber',
            'customers.customer',
            'deliveryinformation.grandtotal'
        )
        ->join('customers', 'customers.cid', '=', 'invoiceinformation.cid')
        ->join('deliveryinformation', 'deliveryinformation.did', '=', 'invoiceinformation.did')
        ->where('invoiceinformation.cid', '=', $data->cid)
        ->get();

        return $result;

    }

    public static function LoadSalesPerCustomer($data){

        $result = DB::table('invoiceinformation')
        ->select(
            'customers.customer',
            DB::raw("SUM(deliveryinformation.grandtotal) AS 'grandtotal'")
        )
        ->join('customers', 'customers.cid', '=', 'invoiceinformation.cid')
        ->join('deliveryinformation', 'deliveryinformation.did', '=', 'invoiceinformation.did')
        ->whereRaw("DATE_FORMAT(invoiceinformation.created_at, '%Y-%m-%d') BETWEEN '". $data->datefrom ."' AND '". $data->dateto ."'")
        ->groupBy('invoiceinformation.cid')
        ->get();

        return $result;

    }


}
