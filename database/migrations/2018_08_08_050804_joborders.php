<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Joborders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joborders', function (Blueprint $table) {
            $table->increments('joid');
            $table->string('jorequestnumber', '60')->unique();
            $table->string('jonumber', '60')->unique();
            $table->integer('poid');
            $table->integer('itemid');
            $table->integer('requestid');
            $table->integer('rmstock');
            $table->text('style')->nullable();
            $table->text('inkcolor')->nullable();
            $table->text('specialinstruction')->nullable();
            $table->integer('targetoutput');
            $table->integer('qtyreceive')->default(0);
            $table->date('deliverydatefrom')->nullable();
            $table->date('deliverydateto')->nullable();
            $table->integer('cid');
            $table->text('requestby');
            $table->datetime('requestdate')->nullable();
            $table->text('issuedby')->nullable();
            $table->text('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joborders');
    }
}
