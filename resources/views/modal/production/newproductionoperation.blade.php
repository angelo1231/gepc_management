<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newproductionoperation" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>New Operation Information</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txtnjonumber">Job Order Number</label>
                            <select name="txtnjonumber" id="txtnjonumber" class="form-control"></select>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnncreate" name="btnncreate" type="button" class="btn btn-primary btn-flat btn-block">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>