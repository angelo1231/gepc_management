<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Qafinalinspectionvisual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qafinalinspectionvisual', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qafiid');
            $table->integer('rejecttypeid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qafinalinspectionvisual');
    }
}
