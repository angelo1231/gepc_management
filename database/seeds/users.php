<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $result = DB::table('users')->insertGetId([
            'firstname' => 'Angelo Gabriel',
            'lastname' => 'Del Mundo',
            'mi' => 'B',
            'username' => 'Administrator',
            'password' => bcrypt('password'),
            'usertype' => 'Administrator',
            'image' => 'noimage.jpg',
            'created_at' => DB::raw("NOW()")
        ]);

        DB::table('usersrestrictions')
        ->insert([
            "uid"=>$result,
            "purchasing"=>"1",
            "planning"=>"1",
            "rawmaterial"=>"1",
            "finishgoods"=>"1",
            "delivery"=>"1",
            "qualityassurance"=>"1",
            "production"=>"1",
            "accounting"=>"1",
            "created_at"=>DB::raw("NOW()")
        ]);

    }
}
