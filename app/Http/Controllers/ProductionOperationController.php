<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductionOperation_Model;
use Auth;
use DataTables;
use Illuminate\Support\Collection;
use Session;
use App\Http\Controllers\UserController;

class ProductionOperationController extends Controller
{
    
    //Variables
    private $tableschema;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }

    function index($view=null, $id=null){

        $access = UserController::UserGroupAccess();

        if($view!=null && $id!=null){

            $operationinfo = ProductionOperation_Model::GetOperationInfo($id);

            return view('extensions.production.'.$view)
            ->with('operationinfo', $operationinfo)
            ->with('access', $access);

        }
        else{

            return view('productionoperation')
            ->with('access', $access);

        }

    }

    function LoadJobOrder(){

        $joborder = ProductionOperation_Model::LoadJobOrder();

        return json_encode([
            "data"=>$joborder
        ]);

    }

    function SaveOperation(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        //Validation
        $validation = ProductionOperation_Model::ValidateMonitorJO($request);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Operation Information Is Already Open."
            ]);

        }
        else{

            //Get
            $processcode = ProductionOperation_Model::GetJOFGInformation($request);
            $itemprocess = ProductionOperation_Model::GetFirstItemProcess($processcode);
            $targetoutput = ProductionOperation_Model::GetJOTargetOutput($request->joid);

            //Save
            $mid = ProductionOperation_Model::SaveOperation($request, $issuedby, $itemprocess[0]->processid);

            for($i=0;$i<count($itemprocess);$i++){

                ProductionOperation_Model::SaveOperationProcess($mid, $itemprocess[$i]->processid, $i, $targetoutput);

            }

            return json_encode([
                "success"=>true,
                "message"=>"Operation Information Has Been Save."
            ]);

        }

    }

    function LoadOperationInformation(){

        $operationinfo = ProductionOperation_Model::LoadOperationInformation();

        $data = array();
        for($i=0;$i<count($operationinfo);$i++){
            
            $obj = new \stdClass;

            $obj->id = $operationinfo[$i]->mid;
            $obj->jonumber = $operationinfo[$i]->jonumber;
            $obj->targetoutput = $operationinfo[$i]->targetoutput;
            $obj->panel = '<button id="btnoperationprofile" name="btnoperationprofile" class="btn btn-flat btn-info" value="'.$operationinfo[$i]->mid.'" title="Information"><i class="fa fa-info"></i></button>';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadOperationInfo(Request $request){

        $operationinfo = ProductionOperation_Model::LoadOperationInfo($request->mid);

        $data = array();
        for($i=0;$i<count($operationinfo);$i++){
            
            $obj = new \stdClass;

            $obj->process = $operationinfo[$i]->process;
            $obj->operator = $operationinfo[$i]->operator;
            $obj->qty = $operationinfo[$i]->qty;
            $obj->qtyreject = $operationinfo[$i]->rejectqty;

            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->make(true);

    }

    function LoadJobOrderProcess(Request $request){

        $processcode = ProductionOperation_Model::GetJOFinishGoodsInformation($request);

        $process = ProductionOperation_Model::LoadJobOrderProcess($request->mid, $processcode->processcode);

        $data = array();
        for($i=0;$i<count($process);$i++){
            
            $obj = new \stdClass;

            $obj->process = $process[$i]->process;
            $obj->panel = "<button id='btnprocessinfo' name='btnprocessinfo' class='btn btn-flat btn-info' value='". $process[$i]->auditid ."' data-id='". $process[$i]->pid ."' title='Process Information'><i class='fa fa-info'></i></button>";
            
            $obj->processcount = $process[$i]->processcount;
            
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function GetProcessCode(Request $request){

        $processcode = ProductionOperation_Model::GetJOFGInformation($request);

        return json_encode([
            "processcode"=>$processcode
        ]);

    }

    function GetGroupProcess(Request $request){

        $groupprocess = ProductionOperation_Model::GetGroupProcess($request->processcode);

        return json_encode([
            "data"=>$groupprocess
        ]);

    }

    function SaveProcessInformation(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        ProductionOperation_Model::SaveProcessInformation($request, $issuedby);

        return json_encode([
            "success"=>true,
            "message"=>"Process information has been save."
        ]);

    }

    function LoadProcessInfo(Request $request){

        $process = ProductionOperation_Model::LoadProcessInfo($request);

        return json_encode([
            "process"=>$process->process
        ]);

    }

    function LoadJobOrderProcessInfo(Request $request){

        $processdata = ProductionOperation_Model::LoadJobOrderProcessInfo($request);

        return json_encode([
            "data"=>$processdata
        ]);

    }

    function LoadMonitoringProcess(Request $request){

        $process = ProductionOperation_Model::LoadMonitoringProcess($request->mid);

        return json_encode([
            "data"=>$process
        ]);

    }

    function CheckCurrentProcess(Request $request){

        $validation = ProductionOperation_Model::CheckCurrentProcess($request);

        if($validation){

            return json_encode([
                "success"=>true
            ]);

        }
        else{

            return json_encode([
                "success"=>false
            ]);

        }

    }

    function ChangeCurrentProcess(Request $request){

        ProductionOperation_Model::ChangeCurrentProcess($request);

        return json_encode([
            "success"=>true,
            "message"=>"Process has been change."
        ]);

    }

    function LoadJobOrderForRequest(){

        $joborder = ProductionOperation_Model::LoadJobOrderForRequest();

        return json_encode([
            "data"=>$joborder
        ]);


    }

    function UpdateCreateJO(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        ProductionOperation_Model::UpdateCreateJO($request, $this->tableschema, $issuedby);

        return json_encode([
            "success"=>true,
            "message"=>"Job order has been create."
        ]);

    }

    function LoadJORequestInformation(Request $request){

        //TODO HERE

        $joinfo = ProductionOperation_Model::LoadJORequestInformation($request->joid);

        return json_encode([
            "customer"=>$joinfo->customer,
            "ponumber"=>$joinfo->ponumber,
            "description"=>$joinfo->description,
            "mrinumber"=>$joinfo->mrinumber,
            "targetoutput"=>$joinfo->targetoutput,
            "specialinstruction"=>$joinfo->specialinstruction
        ]);

    }

}
