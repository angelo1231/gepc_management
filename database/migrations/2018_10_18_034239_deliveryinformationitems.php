<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Deliveryinformationitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveryinformationitems', function (Blueprint $table) {
            $table->increments('itemid');
            $table->integer('did');
            $table->integer('poid');
            $table->integer('pocusitemid');
            $table->integer('fgid');
            $table->integer('currencyid');
            $table->decimal('rate');
            $table->integer('qty');
            $table->decimal('price');
            $table->decimal('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveryinformationitems');
    }
}
