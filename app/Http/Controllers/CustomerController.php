<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Seassion;
use App\Customer_Model;
use Illuminate\Support\Collection;
use DataTables;
use App\Http\Controllers\UserController;

class CustomerController extends Controller
{
	
    public function __construct()
    {
        $this->middleware('auth');
    }

	function index(){

        $access = UserController::UserGroupAccess();

		return view('customers')
        ->with('access', $access);

	}

	function CustomerInformation(){

		$customers = Customer_Model::CustomerInformation();
        $data = array();
        for($i=0;$i<count($customers);$i++){
			$obj = new \stdClass;
            $obj->cid = $customers[$i]->cid;			
			$obj->supplier = $customers[$i]->customer;
			$obj->contactperson = $customers[$i]->contactperson;
			$obj->contactnumber = $customers[$i]->contactnumber;
			$obj->email = $customers[$i]->email;
            $obj->address = $customers[$i]->address;
            $obj->tin = $customers[$i]->tin;
			$obj->terms = $customers[$i]->terms;
			$obj->tax = $customers[$i]->taxclassification;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $customers[$i]->cid .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);
		
	}

	function SaveCustomer(Request $request){

		//Disabled Customer Validation
		//$customer_count = Customer_Model::ValidateCustomer($request->customer);

		// if($customer_count == 0){
			
		// 	Customer_Model::NewCustomer($request);

		// 	return json_encode(["success"=>true, "message"=>"Customer has been added."]);

		// }
		// else{
		// 	return json_encode(["success"=>false, "message"=>"Customer already exist."]);
		// }

		Customer_Model::NewCustomer($request);

		return json_encode(["success"=>true, "message"=>"Customer has been added."]);

	}

	function CustomerProfile(Request $request){

		$data = Customer_Model::CustomerProfile($request->cid);

		return json_encode([
			"customer"=>$data[0]->customer,
			"contactperson"=>$data[0]->contactperson,
			"contactnumber"=>$data[0]->contactnumber,
			"email"=>$data[0]->email, 
			"address"=>$data[0]->address, 
			"tin"=>$data[0]->tin, 
			"terms"=>$data[0]->terms,
			"tax"=>$data[0]->taxclassification
		]);

	}

	function UpdateCustomer(Request $request){

		Customer_Model::UpdateCustomer($request);

		return json_encode(["message"=>"Customer information has been update."]);

	}

}
