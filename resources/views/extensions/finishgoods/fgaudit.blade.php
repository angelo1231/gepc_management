@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Finish Goods Audit Information</b></h3>
          <a href="{{ url("/fginventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-12 row">

                    <div class="col-md-2">
                        <div class="form-group">
                                
                            <label for="txtdatefrom">Date From</label>
                            <input id="txtdatefrom" name="txtdatefrom" type="text" class="form-control" placeholder="Date From">

                        </div>
                    </div>
 
                    <div class="col-md-2">
                        <div class="form-group">

                                <label for="txtdateto">Date To</label>
                                <input id="txtdateto" name="txtdateto" type="text" class="form-control" placeholder="Date To">
        
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">

                            <br>
                            <button id="btngenerate" name="btngenerate" type="button" class="btn btn-success btn-flat" style="margin-top: 5px;">Generate</button>
        
                        </div>
                    </div>
 
            </div>
            <div class="col-md-12" id="content" style="margin-top: 10px;">

            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    $(document).ready(function(){

        $("#txtdatefrom").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

         $("#txtdateto").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        $('#content').hide();
        LoadStartEndDate();

    });

    $('#btngenerate').on('click', function(){

        var datefrom = $('#txtdatefrom').val();
        var dateto = $('#txtdateto').val();

        $.ajax({
            url: '{{ url("api/finishgoodsinv/generateaudit") }}',
            type: 'get',
            data: {
                datefrom: datefrom,
                dateto: dateto
            },
            beforeSend: function(){

                $('#content').hide();

            },
            success: function(response){

                $('#content').html(response);
                

            },
            complete: function(){

                $('#content').fadeIn("slow");

            }
        });

    });

    function LoadStartEndDate(){

        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadstartenddate") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtdatefrom').val(response.datefrom);
                $('#txtdateto').val(response.dateto);

            }
        });

    }

  </script>  

@endsection