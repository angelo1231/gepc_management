<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Qafinalinspectionmeasuring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qafinalinspectionmeasuring', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qafiid');
            $table->integer('metertape')->default(0);
            $table->integer('caliper')->default(0);
            $table->integer('meterstick')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qafinalinspectionmeasuring');
    }
}
