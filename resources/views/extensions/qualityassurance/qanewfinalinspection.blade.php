@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 id="qaheader" class="box-title"><i class="fa fa-file"></i> <b> New Final Inspection</b></h3>
          <a href="{{ url('/qualityassurance') }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                <h3><strong>Job Order #: {{ $joinformation->jonumber }}</strong></h3>
                <h3><strong>Target Output: {{ $jomonitoringinfo->targetoutput }}</strong></h3>
                <h3><strong>Balance: {{ $jomonitoringinfo->balance }}</strong></h3>
                <br>
                <div class="form-group">
                    <label for="txtqty">Quantity</label>
                    <input type="number" class="form-control" id="txtqty" name="txtqty" value="0">
                </div>
                <div class="form-group">
                    <label for="txtrejectqty">Reject Quantity</label>
                    <input type="number" class="form-control" id="txtrejectqty" name="txtrejectqty" value="0">
                </div>
                <div class="form-group">
                    <label for="cmbrejecttype">Reject Type</label>
                    <select name="cmbrejecttype" id="cmbrejecttype" class="form-control">
                        <option value="0">N/A</option>
                        @foreach ($rejecttype as $val)
                            <option value="{{ $val->id }}">{{ $val->reject }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="form-group col-md-12" style="border: 2px solid red; border-radius: 5px; border-color: lightgrey;">
                    <h4 style="margin-left: 10px;"><strong>Measuring Tool</strong></h4>
                    <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" id="chkmt" value="1">Meter Tape</label>
                    <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" id="chkcal" value="1">Caliper</label>
                    <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" id="chkms" value="1">Meter Stick</label> 
                </div>
                
                <div class="form-group col-md-12" style="border: 2px solid red; border-radius: 5px; border-color: lightgrey;">
                    <h4 style="margin-left: 10px;"><strong>Visual Check Legend</strong></h4>
                    @foreach ($rejecttype as $val)
                    <div class="col-md-4 row">
                        <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" name="chkvisual[]" value="{{ $val->id }}">{{ $val->rejectcode }}: {{ $val->reject }}</label>
                    </div>
                    @endforeach
                </div>
                
                <div class="form-group">
                    <label for="">Others</label>
                    <input type="text" id="txtothers" name="txtothers" class="form-control" placeholder="Others">
                </div>

            </div>

            <div class="col-md-12">
                <table id="tbldimension" class="table table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="4" style="text-align: center;">Dimension Check Legend</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 33%; text-align: center; vertical-align: middle;"><strong>Requirements</strong></td>
                        <td style="width: 33%;"><label class="radio-inline"><input type="radio" name="optsize" value="ID"><strong>Inside</strong></label></td>
                        <td style="width: 33%;" colspan="2"><label class="radio-inline"><input type="radio" name="optsize" value="OD" checked><strong>Outside</strong></label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; vertical-align: middle;"><strong>Length</strong></td☻>
                        <td style="text-align: center; vertical-align: middle;"><strong>Width</strong></td>
                        <td style="text-align: center; vertical-align: middle;" colspan="2"><strong>Height</strong></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" id="txtlength" style="text-align: center;" readonly></td>
                        <td><input type="text" class="form-control" id="txtwidth" style="text-align: center;" readonly></td>
                        <td colspan="2"><input type="text" class="form-control" id="txtheight" style="text-align: center;" readonly></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" placeholder="Length" style="text-align: center;" id="txtlength1" name="txtlength[]" value="0" data-id="1"></td>
                        <td><input type="text" class="form-control" placeholder="Width"  style="text-align: center;" id="txtwidth1"  name="txtwidth[]"  value="0" data-id="1"></td>
                        <td>
                            <input type="text" class="form-control" placeholder="Height" style="text-align: center;" id="txtheight1" name="txtheight[]" value="0" data-id="1">
                            <input type="hidden" id="txtremarks1" name="txtremarks[]" value="0">
                        </td>
                        <td id="tdcheck1" style="width: 5%; text-align: center; vertical-align: middle;">
                            <i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" placeholder="Length" style="text-align: center;" id="txtlength2" name="txtlength[]" value="0" data-id="2"></td>
                        <td><input type="text" class="form-control" placeholder="Width"  style="text-align: center;" id="txtwidth2"  name="txtwidth[]"  value="0" data-id="2"></td>
                        <td>
                            <input type="text" class="form-control" placeholder="Height" style="text-align: center;" id="txtheight2" name="txtheight[]" value="0" data-id="2">
                            <input type="hidden" id="txtremarks2" name="txtremarks[]" value="0">
                        </td>
                        <td id="tdcheck2" style="width: 5%; text-align: center; vertical-align: middle;">
                            <i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" placeholder="Length" style="text-align: center;" id="txtlength3" name="txtlength[]" value="0" data-id="3"></td>
                        <td><input type="text" class="form-control" placeholder="Width"  style="text-align: center;" id="txtwidth3"  name="txtwidth[]"  value="0" data-id="3"></td>
                        <td>
                            <input type="text" class="form-control" placeholder="Height" style="text-align: center;" id="txtheight3" name="txtheight[]" value="0" data-id="3">
                            <input type="hidden" id="txtremarks3" name="txtremarks[]" value="0">
                        </td>
                        <td id="tdcheck3" style="width: 5%; text-align: center; vertical-align: middle;">
                            <i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" placeholder="Length" style="text-align: center;" id="txtlength4" name="txtlength[]" value="0" data-id="4"></td>
                        <td><input type="text" class="form-control" placeholder="Width"  style="text-align: center;" id="txtwidth4"  name="txtwidth[]"  value="0" data-id="4"></td>
                        <td>
                            <input type="text" class="form-control" placeholder="Height" style="text-align: center;" id="txtheight4" name="txtheight[]" value="0" data-id="4">
                            <input type="hidden" id="txtremarks4" name="txtremarks[]" value="0">
                        </td>
                        <td id="tdcheck4" style="width: 5%; text-align: center; vertical-align: middle;">
                            <i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" placeholder="Length" style="text-align: center;" id="txtlength5" name="txtlength[]" value="0" data-id="5"></td>
                        <td><input type="text" class="form-control" placeholder="Width"  style="text-align: center;" id="txtwidth5"  name="txtwidth[]"  value="0" data-id="5"></td>
                        <td>
                            <input type="text" class="form-control" placeholder="Height" style="text-align: center;" id="txtheight5" name="txtheight[]" value="0" data-id="5">
                            <input type="hidden" id="txtremarks5" name="txtremarks[]" value="0">
                        </td>
                        <td id="tdcheck5" style="width: 5%; text-align: center; vertical-align: middle;">
                            <i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Flute Type: </strong> 
                            <select name="cmbflute" id="cmbflute" class="form-control">
                                <?php $flute = explode(',', $joinformation->flute);  ?>
                                @for ($i = 0; $i < count($flute); $i++)
                                    <option value="{{ $flute[$i] }}">{{ $flute[$i] }}</option>
                                @endfor
                            </select>
                        </td>
                        <td><strong>Thickness</strong> <input type="text" class="form-control" id="txtthickness" placeholder="Thickness"></td>
                        <td colspan="2"><strong>Tolerance</strong> <input type="text" class="form-control" id="txttolerance" value="0"></td>
                    </tr>
                </tbody>
                </table>

                <button id="btnsavefi" name="btnsavefi" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
              {{-- <label>Target Output:</label> --}}
          </div>          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  {{-- @include('modal.qualityassurance.newcocqty')
  @include('modal.qualityassurance.cocitemdimension') --}}

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

<script>

    //Variables
    var joid = '{{ $joid }}';
    var mid = '{{ $mid }}';
    var pid = '{{ $pid }}';
    
    //Measuring Tool
    var mt = 0;
    var cal = 0;
    var ms = 0;

    $(document).ready(function(){

        //Set
        $('#cmbrejecttype').select2({
            theme: 'bootstrap'
        });
        $('#cmbflute').select2({
            theme: 'bootstrap'
        });
        SetSize();

    });

    $('[name="optsize"]').on('change', function(){

        SetSize();

    });

    $('#btnsavefi').on('click', function(){

        $.confirm({
            title: 'Save',
            content: 'Save this final inspection information?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-info',
                    keys: ['enter'],
                    action: function(){

                        SaveFI();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                        

                    }
                } 
            }
        });

    });

    $('[name="txtlength[]"]').on('click', function(){

        $(this).select();

    });

    $('[name="txtwidth[]"]').on('click', function(){

        $(this).select();

    });

    $('[name="txtheight[]"]').on('click', function(){

        $(this).select();

    });

    $('[name="txtlength[]"]').on('keyup', function(){

        var id = $(this).data('id');

        Remarks(id);

    });

    $('[name="txtwidth[]"]').on('keyup', function(){

        var id = $(this).data('id');

        Remarks(id);

    });

    $('[name="txtheight[]"]').on('keyup', function(){

        var id = $(this).data('id');

        Remarks(id);

    });

    $('#txttolerance').on('keyup', function(){

        Tolerance();

    });

    function SaveFI(){

        var operation = SaveProcessMonitoring();

        operation.then(function(data){

            if(data.success){

                ValMeasureTool();
                var outputqty = $('#txtqty').val();
                var visual = $('[name="chkvisual[]"]').serializeArray();
                var dimensionlength = $('[name="txtlength[]"]').serializeArray();
                var dimensionwidth = $('[name="txtwidth[]"]').serializeArray();
                var dimensionheight = $('[name="txtheight[]"]').serializeArray();
                var others = $('#txtothers').val();
                var insideoutside = $("input[name='optsize']:checked").val();
                var flute = $('#cmbflute').val();
                var thickness = $('#txtthickness').val();
                var tolerance = $('#txttolerance').val();
                var remarks = $('[name="txtremarks[]"]').serializeArray();

                $.ajax({
                    url: '{{ url("api/qualityassurance/savefinalinspection") }}',
                    type: 'post',
                    data: {
                        joid: joid,
                        mid: mid,
                        pid: pid,
                        outputqty: outputqty,
                        mt: mt,
                        cal: cal,
                        ms: ms,
                        visual: visual,
                        dimensionlength: dimensionlength,
                        dimensionwidth: dimensionwidth,
                        dimensionheight: dimensionheight,
                        remarks: remarks,
                        others: others,
                        insideoutside: insideoutside,
                        flute: flute,
                        thickness: thickness,
                        tolerance: tolerance,
                        auditid: data.auditid
                    },
                    dataType: 'json',
                    success: function(response){

                        if(response.success){
                            window.location = '{{ url("/qualityassurance") }}';
                        }
                        else{
                            toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                        }

                    },
                    complete: function(){
                        H5_loading.hide();
                    }
                });

            }
            else{

              toastr.error(data.message, '', { positionClass: 'toast-top-center' });
              H5_loading.hide();

            }

        });

    }

    function SaveProcessMonitoring() {
        
        var qty = $('#txtqty').val();
        var rejectqty = $('#txtrejectqty').val();
        var rejecttype = $('#cmbrejecttype').val();

        return $.ajax({
          url: '{{ url("api/productionmonitoring/saveoperationinformation") }}',
          type: 'post',
          data: {
            pid: pid,
            mid: mid,
            qty: qty,
            rejectqty: rejectqty,
            rejecttype: rejecttype
          },
          dataType: 'json',
          beforeSend: function(){

            H5_loading.show();

          }
        });

    }

    function ValMeasureTool() {
        
        if($("#chkmt").prop('checked')){
            mt = 1;
        }
        else{
            mt = 0
        }
        
        if($("#chkcal").prop('checked')){
            cal = 1;
        }
        else{
            cal = 0
        }

        if($("#chkms").prop('checked')){
            ms = 1;
        }
        else{
            ms = 0
        }


    }

    function Tolerance(){

        var counter = 1;
        var dimensionlength = $('[name="txtlength[]"]').serializeArray();

        for(var i=0;i<dimensionlength.length;i++){

            Remarks(counter);
            counter += 1;

        }

    }

    function Remarks(id){

        var bollength = false;
        var bolwidth = false;
        var bolheight = false;
        var length = $('#txtlength').val();
        var width = $('#txtwidth').val();
        var height = $('#txtheight').val();
        var tolerance = $('#txttolerance').val();
        var vallength = $('#txtlength'+id).val();
        var valwidth = $('#txtwidth'+id).val();
        var valheight = $('#txtheight'+id).val();

        if(vallength <= parseFloat(tolerance) + parseFloat(length) && vallength >= parseFloat(length) - parseFloat(tolerance)){
            bollength = true;
        }
        else{
            bollength = false;
        }

        if(valwidth <= parseFloat(tolerance) + parseFloat(width) && valwidth >= parseFloat(width) - parseFloat(tolerance)){
            bolwidth = true;
        }
        else{
            bolwidth = false;
        }

        if(valheight <= parseFloat(tolerance) + parseFloat(height) && valheight >= parseFloat(height) - parseFloat(tolerance)){
            bolheight = true;
        }
        else{
            bolheight = false;
        }

        if(bollength==true && bolwidth==true && bolheight==true){

            $('#txtremarks'+id).val('1');
            $('#tdcheck'+id).empty('');
            $('#tdcheck'+id).append('<i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>');

        }else{

            $('#txtremarks'+id).val('0');
            $('#tdcheck'+id).empty('');
            $('#tdcheck'+id).append('<i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>');

        }

    }

    function SetSize(){

        var value = $("input[name='optsize']:checked").val();

        if(value=="OD"){

            $('#txtlength').val('{{ $OD_length }}');
            $('#txtwidth').val('{{ $OD_width }}');
            $('#txtheight').val('{{ $OD_height }}');

        }
        else{

            $('#txtlength').val('{{ $ID_length }}');
            $('#txtwidth').val('{{ $ID_width }}');
            $('#txtheight').val('{{ $ID_height }}');

        }

    }

</script>  

@endsection