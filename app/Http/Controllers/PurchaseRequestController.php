<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest_Model;
use Auth;
use DataTables;
use Illuminate\Support\Collection;

class PurchaseRequestController extends Controller
{

    private $tableschema;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }
    
    function GenPurchaseRequestNumber(){

        $batch = PurchaseRequest_Model::GenPurchaseRequestNumber($this->tableschema);

        return json_encode([
            "batch"=>$batch
        ]);

    }

    function LoadRawMaterials(){

        $rawmaterials = PurchaseRequest_Model::LoadRawMaterials();

        return json_encode([
            "data"=>$rawmaterials
        ]);

    }

    function SavePR(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
        PurchaseRequest_Model::SavePR($request, $issuedby, $this->tableschema);

        return json_encode([
            "success"=>true,
            "message"=>"Purchase request has been save."
        ]);

    }

    function LoadPurchaseRequestInformation(){

        $prequest = PurchaseRequest_Model::LoadPurchaseRequestInformation("Planning", null);
        
        $data = array();

        for($i=0;$i<count($prequest);$i++){
            
            $obj = new \stdClass;

            if($prequest[$i]->status=="Done"){
                $obj->status = '<a href="#"><i class="fa fa-check"></i></a>';
            }
            else{
                $obj->status = '';
            }

            $obj->prnumber = $prequest[$i]->prnumber;
            $obj->itemdescription = $prequest[$i]->description;
            $obj->reqqty = $prequest[$i]->qtyrequired;
            $obj->reason = $prequest[$i]->reason;
            $obj->issuedby = $prequest[$i]->issuedby;    
            $obj->issueddate = $prequest[$i]->issueddate;
            $obj->issuedtime = $prequest[$i]->issuedtime;

            if($prequest[$i]->status=="Done"){
                $obj->panel = '';
            }
            else{
                $obj->panel = '<button id="btnremove" name="btnremove" class="btn btn-danger btn-flat" title="Remove" value="'. $prequest[$i]->prid .'" style="width: 40px;"><i class="fa fa-trash"></i>';
            }
            
            $data[] = $obj;

        }
        
        $info = new Collection($data);

        return Datatables::of($info)->rawColumns([
            'panel', 
            'status',
            'itemdescription',
            'reqqty'
        ])->make(true);

    }

    function LoadPurchaseRequestInfoPurchasing(){

        $purchaserequest = PurchaseRequest_Model::LoadPurchaseRequestInformation("Purchasing");

        $data = array();

        for($i=0;$i<count($purchaserequest);$i++){
            
            $obj = new \stdClass;

            $obj->prnumber = $purchaserequest[$i]->prnumber;
            $obj->itemdescription = $purchaserequest[$i]->description;
            $obj->reqqty = $purchaserequest[$i]->qtyrequired;
            $obj->reason = $purchaserequest[$i]->reason;
            $obj->issuedby = $purchaserequest[$i]->issuedby;    
            $obj->issueddate = $purchaserequest[$i]->issueddate;
            $obj->issuedtime = $purchaserequest[$i]->issuedtime;
            $obj->panel = '<div class="checkbox"><label><input id="chkitem'.$purchaserequest[$i]->prid.'" name="chkitem[]" type="checkbox" value="'.$purchaserequest[$i]->prid.'"></label></div>';
            
            $data[] = $obj;

        }
        
        $info = new Collection($data);

        return Datatables::of($info)
        ->rawColumns([
            'panel',
            'itemdescription',
            'reqqty'
        ])->make(true);

    }

    function UpdatePurchaseRequestInfoPurchasing(Request $request){

        PurchaseRequest_Model::UpdatePurchaseRequestInfoPurchasing($request->id);

        return json_encode([
            "success"=>true,
            "message"=>"Purchase request has been approve."
        ]);

    }

    function LoadSuppliers(){

        $suppliers = PurchaseRequest_Model::LoadSuppliers();

        return json_encode([
            "data"=>$suppliers
        ]);

    }

    function LoadFlutePaperCombination(Request $request){

        $papercombination = PurchaseRequest_Model::LoadFlutePaperCombination($request->flute, $request->boardpound);

        return json_encode([
            "data"=>$papercombination
        ]);

    }

    function LoadPaperComPrice(Request $request){

        $papercomprice = PurchaseRequest_Model::LoadPaperComPrice($request->papercomid);

        if(count($papercomprice)!=0){

            return json_encode([
                "price"=>$papercomprice[0]->price
            ]);

        }
        else{

            return json_encode([
                "price"=>0
            ]);

        }

    }

}
