<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\DeliverySales_Model;
use Session;
use DataTables;
use Illuminate\Support\Collection;
use App\Http\Controllers\UserController;

class DeliverySalesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        $access = UserController::UserGroupAccess();

        return view('deliverysales')
        ->with('access', $access);
                   
    }

    function LoadCustomer(){

        $content = "";
        $customer = DeliverySales_Model::LoadCustomer();

        $content .= '<option value="">Select a Customer</option>';
        foreach($customer as $val){

            $content .= '<option value="'. $val->cid .'">'. $val->customer .'</option>';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function LoadDeliverySalesInformation(Request $request){

        $deliverysales = DeliverySales_Model::LoadDeliverySalesInformation($request);

        $data = array();

        foreach($deliverysales as $val){

            $obj = new \stdClass;

            $obj->date = $val->created_at;
            $obj->invoicenumber = $val->invoicenumber;
            $obj->drnumber = $val->drnumber;
            $obj->customeritem = $val->customeritem;
            $obj->qty = $val->qty;
            $obj->unitprice = $val->unitprice;
            $obj->total = $val->total;
            $obj->panel = '';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns(['panel', 'customeritem', 'qty', 'unitprice', 'total'])
        ->make(true);

    }

    function LoadSalesSummary(Request $request){

        $salessummary = DeliverySales_Model::LoadSalesSummary($request);

        $data = array();

        foreach($salessummary as $val){

            $obj = new \stdClass;

            $obj->date = $val->created_at;
            $obj->invoicenumber = $val->invoicenumber;
            $obj->drnumber = $val->drnumber;
            $obj->customer = $val->customer;
            $obj->total = $val->grandtotal;
            $obj->panel = '';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns(['panel'])
        ->make(true);

    }

    function LoadSalesPerCustomer(Request $request){

        $salescustomer = DeliverySales_Model::LoadSalesPerCustomer($request);

        $data = array();

        foreach($salescustomer as $val){

            $obj = new \stdClass;

            $obj->customer = $val->customer;
            $obj->total = $val->grandtotal;
            $obj->panel = '';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns(['panel'])
        ->make(true);


    }

}
