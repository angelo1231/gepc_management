<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newpapercombination" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>NEW PAPER COMBINATION</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">
    
                        <div class="form-group">
                            <label for="txtnrmpapercombination">RM Paper Combination</label>
                            <input type="text" id="txtnrmpapercombination" name="txtnrmpapercombination" class="form-control" placeholder="RM Paper Combination">
                        </div>

                        <div class="form-group">
                            <label for="cmbnflute">Flute</label>
                            <select id="cmbnflute" name="cmbnflute" class="form-control">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="cmbnboardpound">Board Pound</label>
                            <select id="cmbnboardpound" name="cmbnboardpound" class="form-control">
                            </select>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btnsavepapercombination" name="btnsavepapercombination" class="btn btn-success">Save Information</button>
                        <button data-dismiss="modal" class="btn btn-danger">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>