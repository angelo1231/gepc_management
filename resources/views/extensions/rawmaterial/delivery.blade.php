@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Delivery Informations</b></h3>
          <a href="{{ url("/rminventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="cmbponumber">Purchase Order Number</label>
                    <select class="form-control" id="cmbponumber" style="width: 320px;">
                    </select>
                </div>

            </div>
            <div class="col-md-12">

                <div class="col-md-6">
                    <div class="row">
                        <div class="form-group">
                            <label for="txtsupplier">Supplier</label>
                            <input type="text" id="txtsupplier" name="txtsupplier" class="form-control" readonly>
                        </div>
                    </div>
                </div>

                <table id="tblpoitems" class="table" style="font-size: 12px;">
                    <thead>
                        <tr class="header">
                            <th>Description</th>
                            <th>Size</th>
                            <th>Flute</th>
                            <th>Board Pound</th>
                            <th>Quantity</th>
                            <th>Deliverd Quantity</th>
                            <th>
                                Delivery Quantity
                            </th>
                            <th>
                                Reject
                            </th>
                        </tr>
                    </thead>
                    <tbody id="tblpoitemscontent">
                        {{-- Content Here --}}
                    </tbody>
                </table>
            </div> 
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

            <div class="col-md-12">
                <button id="btnsavepoinformation" name="btnsavepoinformation" class="btn btn-primary btn-flat" style="float:right;" disabled><i class="fa fa-floppy-o" style="font-size: 12px;"></i><strong> Save Information</strong></button>
            </div>

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {{-- Modal --}}
  @include('modal.rawmaterials.inventory.drdelivery')

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>


        $(document).ready(function() {
     
            LoadPO($('#cmbponumber').attr('id'));
            LoadSelect2();

        });

        $('#cmbponumber').on('change', function(){

            var poid = $(this).val();
            
            $.ajax({
                url: '{{ url("api/rawmaterialinv/loadporawmaterial") }}',
                type: 'get',
                data: {poid: poid},
                dataType: 'json',
                success: function(response){

                    $('#txtsupplier').val(response.posupplier);
                    $('#tblpoitemscontent').find('tr').remove();
                    $('#tblpoitemscontent').append(response.content);
                    $('#btnsavepoinformation').attr('disabled', false);

                }
            });

        });

        $('#btnsavepoinformation').on('click', function(){

            $('#drdelivery').modal('toggle');

        });

        $('#btnsavedelivery').on('click', function(){

            $.confirm({
              title: 'Save',
              content: 'Save this delivery information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveDelivery();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

        });

        function SaveDelivery(){

            //Validation Variables
            var validateblank = false;
            var validatenegative = false;

            //Variables
            var poid = $('#cmbponumber').val();
            var ponumber = $('#cmbponumber option:selected').text();
            var itemid = $('[name="txtitemid[]"]').serializeArray();
            var rmid = $('[name="txtrmid[]"').serializeArray();
            var delqty = $('[name="txtdelqty[]"]').serializeArray();
            var deliveryqty = $('[name="txtdeliveryqty[]"]').serializeArray();
            var deliveryqtyreject = $('[name="txtdeliveryqtyreject[]"]').serializeArray();
            var drnumber = $('#txtdeliverynumber').val();

            for(var i=0;i<deliveryqty.length;i++){
            
                if(deliveryqty[i].value==""){
                    validateblank = true;
                    break;
                }

                if(deliveryqty[i].value<0){
                    validatenegative = true;
                    break;
                }

            }

            if(drnumber==""){
                toastr.error("Please input the delivery number.", '', { positionClass: 'toast-top-center' });
            }
            else{

                if(!validateblank){

                    if(!validatenegative){

                        $.ajax({
                            url: '{{ url("api/rawmaterialinv/savepodelivery") }}',
                            type: 'post',
                            data: {
                                poid: poid,
                                ponumber: ponumber,
                                itemid: itemid,
                                rmid: rmid,
                                deliveryqty: deliveryqty,
                                deliveryqtyreject: deliveryqtyreject,
                                drnumber: drnumber
                            },
                            dataType: 'json',
                            beforeSend: function(){
                                H5_loading.show();
                            },
                            success: function(response){

                                if(response.success){

                                    //Socket Imit
                                    socket.emit('rminvinformation');
                                    window.location = "{{ url('/rminventory') }}";

                                }
                                else{

                                    toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                                }

                            },
                            complete: function(){
                                H5_loading.hide();
                            }
                        });

                    }
                    else{
                        toastr.error("Please dont input negative numbers or equal to zero quantity.", '', { positionClass: 'toast-top-center' });
                    }

                }
                else{
                    toastr.error("Please dont leave any blank information.", '', { positionClass: 'toast-top-center' });
                }

            }

        }

        function LoadPO(id){

            $.ajax({
                url: '{{ url("api/rawmaterialinv/loadpo") }}',
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $('#'+id).find('option').remove();
                    $('#'+id).append('<option value="" disabled selected>Select a Purchase Order Number</option>');
                    for (var i = 0; i < response.data.length; i++) {
                        $('#'+id).append('<option value="'+  response.data[i]["poid"] +'">'+  response.data[i]["ponumber"] +'</option>');
                    }

                }
            });

        }

        function LoadSelect2(){

            $('#cmbponumber').select2({
                theme: 'bootstrap'
            });

        }

  </script>  

@endsection