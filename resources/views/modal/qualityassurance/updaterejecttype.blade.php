<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="updaterejecttype" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>UPDATE REJECT TYPE</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txturejectcode">Reject Code</label>
                            <input type="text" id="txturejectcode" name="txturejectcode" class="form-control" placeholder="Reject Code">
                        </div>
                        <div class="form-group">
                            <label for="txtureject">Reject Type</label>
                            <input type="text" id="txtureject" name="txtureject" class="form-control" placeholder="Reject Type">
                        </div>
   
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnupdaterejecttype" name="btnupdaterejecttype" type="button" class="btn btn-primary btn-flat btn-block">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>