<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use DataTables;
use Illuminate\Support\Collection;
use App\Production_Model;
use App\Http\Controllers\UserController;

class ProductionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index($view=null, $id=null){
 
        $access = UserController::UserGroupAccess();

        if($view!=null && $id!=null){

            $fginfo = Production_Model::GetFGInfo($id);

            return view('extensions.production.'.$view)
            ->with('fginfo', $fginfo)
            ->with('access', $access);
    
        }
        else{
    
            return view('production')
            ->with('access', $access);
    
        }


    }

    function LoadMSTNumber(){

        $mstnumbers = Production_Model::LoadMSTNumber();

        $data = array();
        foreach($mstnumbers as $val){

            $obj = new \stdClass;

            $obj->jonumber = $val->jonumber;
            $obj->mstnumber = $val->mstnumber;
            $obj->qty = $val->outputqty;
            $obj->reject = $val->rejectqty;
            $obj->issuedby = $val->issuedby;
            $obj->issueddate = $val->created_at;    
            $obj->panel = '<button id="btnprint" name="btnprint" class="btn btn-flat btn-primary" title="Print" value="'. $val->qafiid .'"><i class="fa fa-print"></i></button>';
            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);


    }

    function UpdateMSTStatus(Request $request){

        Production_Model::UpdateMSTStatus($request->qafiid);

        return json_encode([
            "success"=>true
        ]);

    }

}
