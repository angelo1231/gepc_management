<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notif_Model;

class NotifController extends Controller
{
    
    function NotifPlanningMR(Request $request){

        $prdata = Notif_Model::NotifPlanningMR($request->prid);

        return json_encode([
            "prdata"=>$prdata
        ]);

    }

}
