<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->text('firstname');
            $table->text('lastname');
            $table->text('mi')->nullable();
            $table->string('username', '60')->unique();
            $table->string('password', '60');
            $table->integer('usertype');
            $table->string('image', '100')->default('noimage.jpg');
            $table->string('esign', '100')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
