<!-- Middle Modal -->
<style>
.modal {
   text-align: center;
   padding: 0!important;
}
    
.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}
    
.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.ui-autocomplete  {
    z-index: 1100 !important;
}

</style>   

<!-- Modal -->
<div class="modal fade" id="drdelivery"  role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  <label id="lblinformation"><strong>Delivery Number</strong></label> 
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="col-md-12">

                            <div class="form-group">
                                <input id="txtdeliverynumber" name="txtdeliverynumber" type="text" class="form-control" placeholder="Delivery Number">
                            </div>
                      
                        </div>

                    </div>
          
                </div>
                
                <div class="modal-footer">
                    <button id="btnsavedelivery" name="btnsavedelivery" class="btn btn-flat btn-success">Save</button>
                    <button class="btn btn-flat btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
       
</div>
    