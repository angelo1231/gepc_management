<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('vatexempt', 10, 2)->default(0);
            $table->decimal('vatablesales', 10, 2)->default(0);
            $table->decimal('zerorated', 10, 2)->default(0);
            $table->string('address', 60)->nullable();
            $table->string('telnumber', 60)->nullable();
            $table->string('faxnumber', 60)->nullable();
            $table->decimal('overrunpercentage', 10, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
