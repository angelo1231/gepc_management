<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Settings_Model extends Model
{
    
    public static function LoadSettings(){

        $result = DB::table('settings')
        ->select(
            'vatexempt',
            'vatablesales',
            'zerorated'
        )
        ->where('id', '=', 1)
        ->first();

        return $result;

    }

    public static function SaveTaxClassification($data){

        DB::table('settings')
        ->where('id', '=', 1)
        ->update([
            "vatexempt"=>$data->vatexempt,
            "vatablesales"=>$data->vatablesales,
            "zerorated"=>$data->zerorated
        ]);

    }

    public static function LoadCompanyInfo(){

        $result = DB::table('settings')
        ->select(
            'address',
            'telnumber',
            'faxnumber'
        )
        ->where('id', '=', 1)
        ->first();

        return $result;

    }

    public static function SaveCompanyInfo($data){

        $result = DB::table('settings')
        ->where('id', '=', 1)
        ->update([
            'address'=>$data->address,
            'telnumber'=>$data->telnumber,
            'faxnumber'=>$data->faxnumber
        ]);

        return $result;

    }

    public static function GetCompanyInfo(){

        $result = DB::table('settings')
        ->select(
            'address',
            'telnumber',
            'faxnumber'
        )
        ->where('id', '=', 1)
        ->first();

        return $result;

    }

    public static function LoadRawMaterialSettings(){

        $result = DB::table('settings')
        ->select(
            'overrunpercentage'
        )
        ->where('id', '=', 1)
        ->first();

        return $result;

    }

    public static function SaveRawMaterial($data){

        $result = DB::table('settings')
        ->where('id', '=', 1)
        ->update([
            'overrunpercentage'=>$data->overrunpercentage
        ]);

        return $result;

    }

    public static function LoadUserGroup(){

        $result = DB::table('usergroups')
        ->select(
            'ugroupid',
            'usergroup'
        )
        ->get();

        return $result;

    }

    public static function ValidateEditAccess($ugroupid){

        $result = DB::table('settingseditaccess')
        ->select(
            DB::raw("COUNT(*) AS 'ugroupcount'")
        )
        ->where('ugroupid', '=', $ugroupid)
        ->first();

        if($result->ugroupcount!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveEditAccess($ugroupid){

        DB::table('settingseditaccess')
        ->insert([
            "ugroupid"=>$ugroupid,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadEditAccessInformation(){

        $result = DB::table('settingseditaccess')
        ->select(
            'settingseditaccess.id',
            'usergroups.usergroup'
        )
        ->join('usergroups', 'usergroups.ugroupid', '=', 'settingseditaccess.ugroupid')
        ->get();

        return $result;

    }

    public static function LoadEditAccessProfile($id){

        $result = DB::table('settingseditaccess')
        ->select(
            'ugroupid'
        )
        ->where('id', '=', $id)
        ->first();

        return $result;

    }

    public static function UpdateEditAccess($id, $ugroupid){

        DB::table('settingseditaccess')
        ->where('id', '=', $id)
        ->update([
            "ugroupid"=>$ugroupid,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteEditAccess($id){

        DB::table('settingseditaccess')
        ->where('id', '=', $id)
        ->delete();

    }

}
