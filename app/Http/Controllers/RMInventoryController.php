<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use App\RawMaterials_Model;
use DataTables;
use Illuminate\Support\Collection;
use Excel;
use App\Exports\RawMaterials\RawMaterialExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\UserController;

class RMInventoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null, $action=null, $id=null){

        $access = UserController::UserGroupAccess();

        if($view==null){

            return view('rawmaterialinventory')
            ->with('access', $access);

        }
        else{

            return view('extensions.rawmaterial.'.$view)
            ->with('access', $access);   
           
        }
                   
       
    }

    function PrintRM($sort=null){

        $rmdata = RawMaterials_Model::RawMaterialInformation($sort);
        return view('extensions.rawmaterial.rawmaterialprint')
        ->with('rmdata', $rmdata);

    }

    function LoadPO(){

        $po = RawMaterials_Model::LoadPO();
        return json_encode(["data"=>$po]);

    }

    function LoadSupplier(){

        $supplier = RawMaterials_Model::LoadSupplier();
        return json_encode(["data"=>$supplier]);

    }

    function RawMaterialInformation(Request $request){

        $access = UserController::UserGroupAccess();

        $rawmaterials = RawMaterials_Model::RawMaterialInformation($request->sort);

        $data = array();
        for($i=0;$i<count($rawmaterials);$i++){
            
            $obj = new \stdClass;

            $size = "";
            if($rawmaterials[$i]->category=="Raw Material" || $rawmaterials[$i]->category=="Excess Board"){

                if($rawmaterials[$i]->widthsize!=""){
                    $size = $size . $rawmaterials[$i]->width . "." . $rawmaterials[$i]->widthsize;
                }
                else{
                    $size = $size . $rawmaterials[$i]->width;
                }
    
                if($rawmaterials[$i]->lengthsize!=""){
                    $size = $size . " x " . $rawmaterials[$i]->length . "." . $rawmaterials[$i]->lengthsize;
                }
                else{
                    $size = $size . " x " . $rawmaterials[$i]->length;
                }

            }

            $obj->name = $rawmaterials[$i]->name;
            $obj->description = $rawmaterials[$i]->description;
            $obj->size = $size;
            $obj->flute = $rawmaterials[$i]->flute;
            $obj->boardpound = $rawmaterials[$i]->boardpound;
            $obj->currency = $rawmaterials[$i]->currency;    
            $obj->price = $rawmaterials[$i]->price;
            $obj->stocks = $rawmaterials[$i]->stockonhand;
            $obj->category = $rawmaterials[$i]->category;
            // $obj->panel = '<button id="btndelivery" name="btndelivery" class="btn btn-info btn-flat" title="Delivery" value="'. $rawmaterials[$i]->rmid .'" style="width: 40px;"><i class="fa fa-truck"></i></button><button id="btnrmauditinfo" name="btnrmauditinfo" class="btn btn-info btn-flat" title="Audit Trail" value="'. $rawmaterials[$i]->rmid .'" style="width: 40px;" data-toggle="modal" data-target="#modalaudit"><i class="fa fa-info-circle"></i></button>';

            if(in_array(67, $access)){

                $obj->panel = '<button id="btnrmauditinfo" name="btnrmauditinfo" class="btn btn-info btn-flat" title="Audit Trail" value="'. $rawmaterials[$i]->rmid .'" style="width: 40px;" data-toggle="modal" data-target="#modalaudit"><i class="fa fa-info-circle"></i></button> <button id="btnupdatestock" name="btnupdatestock" class="btn btn-flat btn-info" value="'. $rawmaterials[$i]->rmid .'" title="Update Stock"><i class="fa fa-list-ul"></i></button>';

            }
            else{

                $obj->panel = '<button id="btnrmauditinfo" name="btnrmauditinfo" class="btn btn-info btn-flat" title="Audit Trail" value="'. $rawmaterials[$i]->rmid .'" style="width: 40px;" data-toggle="modal" data-target="#modalaudit"><i class="fa fa-info-circle"></i></button>';

            }
                           
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'size'])->make(true);

    }

    function LoadPONumberIndividual(Request $request){

       $ponumbers = RawMaterials_Model::LoadPONumberIndividual($request->rmid);
       return json_encode(["data"=>$ponumbers]);

    }

    function LoadPORawMaterialProfile(Request $request){

        $poitems = RawMaterials_Model::LoadPORawMaterialProfile($request->rmid, $request->poid);
        return json_encode([
            "itemid"=>$poitems[0]->itemid,
            "name"=>$poitems[0]->name,
            "description"=>$poitems[0]->description,
            "width"=>$poitems[0]->width,
            "widthsize"=>$poitems[0]->widthsize,
            "length"=>$poitems[0]->length,
            "lengthsize"=>$poitems[0]->lengthsize,
            "flute"=>$poitems[0]->flute,
            "qty"=>$poitems[0]->qty,
            "boardpound"=>$poitems[0]->boardpound,
            "delqty"=>$poitems[0]->delqty,
            "category"=>$poitems[0]->category
        ]);

    }

    function SaveDeliveryInformationIndividual(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
        $stockonhand = RawMaterials_Model::GetRMStockOnHand($request->rmid);
        $delqty = RawMaterials_Model::GetPOItemDeliverdQty($request->itemid);
        
        $stockonhand += $request->deliverdqty;
        $delqty += $request->deliverdqty;
        RawMaterials_Model::UpdateRMStockOnHand($request->rmid, $stockonhand);
        RawMaterials_Model::UpdatePOItem($request->itemid, $delqty);
        RawMaterials_Model::SaveRMAudit($request->rmid, $request->ponumber, "", $request->deliverdqty, $stockonhand, $issuedby);
        
        $validate = $this->ValidatePOItems($request->poid);
        if($validate){

            RawMaterials_Model::UpdatePORemarks($request->poid);

        }

        return json_encode(["success"=>true, "message"=>"Delivery information has been save."]);

    }

    function ValidatePOItems($poid){

        $validate = true;
        $data = RawMaterials_Model::ValidatePOItems($poid);
    
        foreach($data as $val){

            if($val->quantity_check=="No"){
                $validate = false;
                break;
            }

        }

        return $validate;
        
    }

    function LoadPORawMaterial(Request $request){

      $content = "";
      $posupplier = RawMaterials_Model::LoadPOSupplier($request->poid);
      $poitems = RawMaterials_Model::LoadPORawMaterial($request->poid);

      foreach($poitems as $val){

        $size = "";

        if($val->category=="Raw Material"){

            if($val->widthsize!=null){
                $size = $size . $val->width . "." . $val->widthsize;
            } 
            else{
                $size = $size . $val->width;
            }

            if($val->lengthsize!=null){
                $size = $size . " x " . $val->length . "." . $val->lengthsize;
            }
            else{
                $size = $size . " x " . $val->length;
            }

        }

        $content .= '
        <tr class="items">
            <td style="vertical-align: middle;">
                <input type="hidden" id="txtitemid" name="txtitemid[]" value="'. $val->itemid .'" />
                <input type="hidden" id="txtrmid" name="txtrmid[]" value="'. $val->rmid .'" />
                '. $val->description .'
            </td>
            <td style="vertical-align: middle;">'. $size .'</td>
            <td style="vertical-align: middle;">'. $val->flute .'</td>
            <td style="vertical-align: middle;">'. $val->boardpound .'</td>
            <td style="vertical-align: middle;">'. $val->qty .'</td>
            <td><input type="text" id="txtdelqty" name="txtdelqty[]" value="'. $val->delqty .'" class="form-control" readonly/></td>
            <td>
                <input id="txtdeliveryqty" name="txtdeliveryqty[]" type="number" class="form-control" value="0"/>
            </td>
            <td>
                <input id="txtdeliveryqtyreject" name="txtdeliveryqtyreject[]" type="number" class="form-control" value="0"/>
            </td>
        </tr>
        ';

      }
      
      return json_encode([
          "content"=>$content,
          "posupplier"=>$posupplier
      ]);

    }

    function SavePODelivery(Request $request){

       $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
       $overrun = false;
       $overrundata = array();

       //Validation
       for($v=0;$v<count($request->itemid);$v++){

           $overrunqty = RawMaterials_Model::GetOverRunQtyItem($request->itemid[$v]["value"]);

           if($overrunqty<$request->deliveryqty[$v]["value"]){
               $overrun = true;
               $overrundata[] = array(
                    $request->itemid[$v]["value"]
               );
           }

       }

       if(!$overrun){
           
            for($i=0;$i<count($request->itemid);$i++){

                if($request->deliveryqty[$i]["value"]!=0){

                    $totalqty = $request->deliveryqty[$i]["value"] - $request->deliveryqtyreject[$i]["value"];
                    $stockonhand = RawMaterials_Model::GetRMStockOnHand($request->rmid[$i]["value"]);
                    $delqty = RawMaterials_Model::GetPOItemDeliverdQty($request->itemid[$i]["value"]);

                    $stockonhand += $totalqty;
                    $delqty += $totalqty;

                    RawMaterials_Model::UpdateRMStockOnHand($request->rmid[$i]["value"], $stockonhand);
                    RawMaterials_Model::UpdatePOItem($request->itemid[$i]["value"], $delqty);
                    RawMaterials_Model::SaveRMAudit($request->rmid[$i]["value"], $request->ponumber, $request->drnumber, "", $totalqty, $stockonhand, $issuedby);
                    RawMaterials_Model::SavePOItemAudit($request->itemid[$i]["value"], $request->drnumber, $request->deliveryqty[$i]["value"], $request->deliveryqtyreject[$i]["value"], $totalqty);

                }
                    
            }

            $validate = $this->ValidatePOItems($request->poid);
            if($validate){

                RawMaterials_Model::UpdatePORemarks($request->poid);

            }

            Session::flash('message', "Delivery information has been save.");
            return json_encode([
                "success"=>true
            ]);

       }
       else{

            $content = "";
            $data = RawMaterials_Model::GetOverRunItemInfo($overrundata);

            for($i=0;$i<count($data);$i++){

                if($i+1<count($data)){
                    $content .= $data[$i]->item . ", ";
                }
                else{
                    $content .= $data[$i]->item;                    
                }

            }

            return json_encode([
                "success"=>false,
                "message"=>"This following items is over the limit " . $content . "."
            ]);

       }

    }

    function DownloadExcel($sort=null){

        $rmdata = RawMaterials_Model::RawMaterialInformation($sort);

        $rmdata_array = array();

        foreach($rmdata as $data){

            $size = "";
            $stockonhand = "";
            if($data->widthsize!=""){
                $size = $size . $data->width . "." . $data->widthsize;
            }
            else{
                $size = $size . $data->width;
            }

            if($data->lengthsize!=""){
                $size = $size . " x " . $data->length . "." . $data->lengthsize;
            }
            else{
                $size = $size . " x " . $data->length;
            }

            if($data->stockonhand=="0"){
                $stockonhand = "0";
            }
            else{
                $stockonhand = $data->stockonhand;
            }

            $rmdata_array[] = array(
                'Size'=>$size,
                'Description'=>$data->description,
                'Flute'=>$data->flute,
                'Boardpound'=>$data->boardpound,
                'Currency'=>$data->currency,
                'Price'=>$data->price,
                'Stocks'=>$stockonhand
            );

        }

        return Excel::download(new RawMaterialExport($rmdata_array), "RM-" . $sort . ".xlsx");

    }

    function LoadStartEndDate(){

        $start = Carbon::now()->startOfMonth()->format('Y-m-d');
        $end = Carbon::now()->format('Y-m-d');

        return json_encode(["datefrom"=>$start, "dateto"=>$end]);

    }

    function LoadAuditInformation(Request $request){

        $audit = RawMaterials_Model::LoadRMAuditInformation($request->datefrom, $request->dateto);

        return View::make('extensions.rawmaterial.views.rmaudittable')->with('audit', $audit)->render();

    }

    function LoadAuditRMInformation(Request $request){
        
        $audit = RawMaterials_Model::LoadAuditRMInformation($request->id, $request->datefrom, $request->dateto);

        return View::make('extensions.rawmaterial.views.rmauditmodaltable')->with('audit', $audit)->render();

    }

    function RMInformation(Request $request){

        $rawmaterial = RawMaterials_Model::RawMaterialsProfile($request->id);

        $size = "";
        if($rawmaterial[0]->category=="Raw Material"){

            if($rawmaterial[0]->widthsize!=""){
                $size = $rawmaterial[0]->width . "." . $rawmaterial[0]->widthsize;
            }
            else{
                $size = $rawmaterial[0]->width;
            }
            
            if($rawmaterial[0]->lengthsize!=""){
                $size = $size . " x " . $rawmaterial[0]->length . "." . $rawmaterial[0]->lengthsize; 
            }
            else{
                $size = $size . " x " . $rawmaterial[0]->length;
            }

        }
        
        return json_encode([
            "name"=>$rawmaterial[0]->name,
            "description"=>$rawmaterial[0]->description,
            "size"=>$size,
            "flute"=>$rawmaterial[0]->flute,
            "boardpound"=>$rawmaterial[0]->boardpound,
            "category"=>$rawmaterial[0]->category
        ]);

    }

    function LoadRMStockOnHand(Request $request){

        $rminfo = RawMaterials_Model::LoadRMStockOnHand($request->rmid);

        return json_encode([
            "stockonhand"=>$rminfo->stockonhand,
            "description"=>$rminfo->description
        ]);

    }

    function SaveRMStockOnHand(Request $request){

       $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

       RawMaterials_Model::SaveRMStockOnHand($request->rmid, $request->qty);
       RawMaterials_Model::SaveRMAuditStock($request->rmid, $request->qty, $issuedby);

        return json_encode([
            "success"=>true,
            "message"=>"Raw material stock information has been save"
        ]);

    }

    function LoadMRCount(){

        $mrcount = RawMaterials_Model::LoadMRCount();

        return json_encode([
            "mrcount"=>$mrcount
        ]);

    }

}
