<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use App\RawMaterials_Model;
use DataTables;
use Illuminate\Support\Collection;
use App\Http\Controllers\UserController;
use App\Exports\RawMaterials\RMDownloadTemp;
use App\Imports\RawMaterials\RawMaterialsImport;
use Excel;

class RMFileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index(){

        $access = UserController::UserGroupAccess();

        return view('rawmaterials')
        ->with('access', $access);

    }

    function LoadSupplier(){

        $supplier = RawMaterials_Model::LoadSupplier();
        return json_encode(["data"=>$supplier]);

    }

    function LoadCurrency(){

        $currency = RawMaterials_Model::LoadCurrency();
        return json_encode(["data"=>$currency]);

    }

    function LoadFlute(){
       
        $flute = RawMaterials_Model::LoadFlute();
        return json_encode(["data"=>$flute]);

    }

    function LoadBoardPound(Request $request){

        $boardpound = RawMaterials_Model::LoadBoardPound($request->flute);
        return json_encode(["data"=>$boardpound]);

    }

    function RawMaterialInformation(Request $request){

        $rawmaterials = RawMaterials_Model::RawMaterialInformation($request->sort);

        $data = array();
        for($i=0;$i<count($rawmaterials);$i++){
            
            $obj = new \stdClass;

            $size = "";
            if($rawmaterials[$i]->category=="Raw Material"){

                if($rawmaterials[$i]->widthsize!=""){
                    $size = $size . $rawmaterials[$i]->width . "." . $rawmaterials[$i]->widthsize;
                }
                else{
                    $size = $size . $rawmaterials[$i]->width;
                }
    
                if($rawmaterials[$i]->lengthsize!=""){
                    $size = $size . " x " . $rawmaterials[$i]->length . "." . $rawmaterials[$i]->lengthsize;
                }
                else{
                    $size = $size . " x " . $rawmaterials[$i]->length;
                }

            }

            $obj->name = $rawmaterials[$i]->name;
            $obj->description = $rawmaterials[$i]->description;
            $obj->size = $size;
            $obj->flute = $rawmaterials[$i]->flute;
            $obj->boardpound = $rawmaterials[$i]->boardpound;
            $obj->currency = $rawmaterials[$i]->currency;    
            $obj->price = $rawmaterials[$i]->price;
            $obj->category = $rawmaterials[$i]->category;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $rawmaterials[$i]->rmid .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'size'])->make(true);


    }

    function RMNewItem(Request $request){
        
        $rawmaterial_count = RawMaterials_Model::ValidateRawMaterial($request, $request->category);

        if($rawmaterial_count==0){

            RawMaterials_Model::RMNewItem($request);
            return json_encode([
                "success"=>true,
                "message"=>"material information has been save."
            ]);

        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"material information already exist."
            ]);

        }

    }

    function RawMaterialsProfile(Request $request){

        $data = RawMaterials_Model::RawMaterialsProfile($request->id);

        return json_encode([
            "name"=>$data[0]->name,
            "description"=>$data[0]->description,
            "width"=>$data[0]->width,
            "widthsize"=>$data[0]->widthsize,
            "length"=>$data[0]->length,
            "lengthsize"=>$data[0]->lengthsize,
            "flute"=>$data[0]->flute,
            "boardpound"=>$data[0]->boardpound,
            "price"=>$data[0]->price,
            "currencyid"=>$data[0]->currencyid,
            "category"=>$data[0]->category
        ]);

    }

    function RMUpdateItem(Request $request){

        RawMaterials_Model::RMUpdateItem($request);
        
        return json_encode([
            "success"=>true, 
            "message"=>"Item information has been update."
        ]);

    }

    function DownloadExcelTemp(){

        return Excel::download(new RMDownloadTemp(), "rmtemp.xlsx");

    }

    function ImportRawMaterials(Request $request){

        $file = $request->file('file');

        Excel::import(new RawMaterialsImport(), $file);

        return json_encode([
            "success"=>true,
            "message"=>"You have successfully imported the data"
        ]);

    }
 
}
