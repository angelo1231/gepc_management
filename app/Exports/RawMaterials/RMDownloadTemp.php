<?php

namespace App\Exports\RawMaterials;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RMDownloadTemp implements WithHeadings
{
    
    use Exportable;

    public function headings(): array
    {
        return [
            'name',
            'description',
            'width',
            'widthsize',
            'length',
            'lengthsize',
            'flute',
            'boardpound',
            'price',
            'currency',
            'category'
        ];
    }


}
