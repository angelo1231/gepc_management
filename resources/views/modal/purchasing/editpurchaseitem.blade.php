<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="editpurchaseitem" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>EDIT INFORMATION</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                      <div class="form-group">
                        <label for="txtnewprice">Price</label>
                        <input type="number" id="txtnewprice" name="txtnewprice" class="form-control" placeholder="Price">
                      </div>

                      <div class="form-group">
                          <label for="txtnewqty">Quantity</label>
                          <input type="number" id="txtnewqty" name="txtnewqty" class="form-control" placeholder="Quantity">
                      </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnsaveeditinfo" name="btnsaveeditinfo" type="button" class="btn btn-primary btn-flat">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>