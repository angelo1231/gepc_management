<table style="border: 2px solid black; font-size: 12px; margin-top: 10px; margin-left: 20px; float: left;">
    <tr>
        <td colspan="6"><img src="{{ asset('images/GEPC_LOGO_RPT.jpg') }}" style="float: left; width: 90px; margin: 5px auto;"/> <label style="float: right; margin: 8px 10px;">Product Tag</label></td>
    </tr>
    <tr>
        <td colspan="3" style="border: 2px solid black; width: 130px;">
            Partnumber:
            <br>
            <div>{{ $fginfo->partnumber }}</div>
        </td>
        <td colspan="3" style="border: 2px solid black; vertical-align: text-top;">
            Production Date:
        </td>
    </tr>
    <tr>
        <td colspan="3" rowspan="2" style="border: 2px solid black; vertical-align: text-top;">
            MPN:
            <br>
            <div>{{ $fginfo->partname }}</div>
        </td>
        <td colspan="3" style="border: 2px solid black;">
            Description:
            <br>
            <div>{{ $fginfo->description }}</div>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: 2px solid black;">
            Customer:
            <br>
            <div>{{ $fginfo->customer }}</div>
        </td>
    </tr>
    <tr>
        <td colspan="3" rowspan="2" style="border: 2px solid black; vertical-align: text-top;">
            Quantity:
            <br>
            <div>{{ $fginfo->packingstd }}</div>
        </td>
        <td colspan="3" style="border: 2px solid black;">
            Doc Ref:
            <br>
            <div>{{ $fginfo->jonumber }}</div>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: 2px solid black;">
            Remarks:
            <br>
            <div>REMARKS</div>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: 2px solid black; height: 40px; vertical-align: text-top;">
            Delivery Date:
        </td>
        <td style="border: 2px solid black; vertical-align: text-top;">
            Warehouse:
        </td>
        <td style="border: 2px solid black; width: 50px; vertical-align: text-top;">
            QA:
        </td>
        <td style="border: 2px solid black; vertical-align: text-top;">
            FIFO Code:
        </td>
    </tr>
    <tr>
        <td colspan="6"><label style="float: right;">FO</label></td>
    </tr>
</table>