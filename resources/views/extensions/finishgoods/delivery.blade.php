@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Delivery</b></h3>
          <a href="{{ url("/fginventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

          <div class="col-md-12">

            <div class="form-group col-md-3 row">
              <label for="cmbdrnumber">Delivery Number</label>
              <select name="cmbdrnumber" id="cmbdrnumber" class="form-control"></select>
            </div>

            <div class="col-md-12 row">
                <table id="tbldeliveryitems" class="table">
                    <thead>
                        <tr class="header">
                            <th>PO #</th>
                            <th>Item Description</th>
                            <th>Item Size</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div> 

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
            
            <button id="btnvalidate" name="btnvalidate" class="btn btn-flat btn-primary" style="float: right;" disabled><i class="fa fa-save"></i> Validate</button>

          </div> 
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var itemcount = 1;

      $(document).ready(function(){

        LoadDRNumber();

      });

      $('#cmbdrnumber').on('change', function(){

        var did = $(this).val();
        $('#items').remove();
        LoadDRItems(did);
        $('#btnvalidate').prop('disabled', false);

      });

      $('#btnvalidate').on('click', function(){

        var did = $('#cmbdrnumber').val();
        var datapo = $('[name="txtponumber[]"]').serializeArray();
        var datacusitemid = $('[name="txtcusitemid[]"]').serializeArray();
        var dataqty = $('[name="txtqty[]"]').serializeArray();

        $.ajax({
          url: '{{ url("api/finishgoodsinv/validatedelivery") }}',
          type: 'post',
          data: {
            did: did,
            datapo: datapo,
            datacusitemid: datacusitemid,
            dataqty: dataqty
          },
          dataType: 'json',
          beforeSend: function(){
            H5_loading.show();
          },
          complete: function(){

            H5_loading.hide();
            
            //Socket Emit
            socket.emit('fginvinformation');

            window.location = "{{ url('/fginventory') }}";

          }
        });

      });

      function LoadDRItems(did){

        $.ajax({
          url: '{{ url("api/finishgoodsinv/loaddritems") }}',
          type: 'get',
          data: {
            did: did
          },
          dataType: 'json',
          success: function(response){

            for(var i = 0; i < response.data.length; i++){
            
              $('#tbldeliveryitems').append('<tr id="items"><td><input type="text" id="txtponumber" name="txtponumber[]" class="form-control" value="'+ response.data[i]["ponumber"] +'" readonly></td><td><input type="hidden" id="txtcusitemid" name="txtcusitemid[]" class="form-control" value="'+ response.data[i]["pocusitemid"] +'"><input type="text" id="txtitemdescription" name="txtitemdescription[]" class="form-control" value="'+ response.data[i]["itemdescription"] +'" readonly></td><td><textarea name="txtitemsize[]" id="txtitemsize" class="form-control" rows="3" readonly>ID Size: '+ response.data[i]["idsize"] +' OD Size: '+ response.data[i]["odsize"] +'</textarea></td><td><input type="number" id="txtqty" name="txtqty[]" value="'+ response.data[i]["qty"] +'" class="form-control"></td></tr>');
            

            }
        
          }
        });

      }

      function LoadDRNumber(){

        $.ajax({
          url: '{{ url("api/finishgoodsinv/loaddrnumbers") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

              $('#cmbdrnumber').find('option').remove();
              $('#cmbdrnumber').append('<option value="" disabled selected>Select a Delivery Number</option>');
              for (var i = 0; i < response.data.length; i++) {
                  $('#cmbdrnumber').append('<option value="'+  response.data[i]["did"] +'">'+  response.data[i]["drnumber"] +'</option>');
              }

              $('#cmbdrnumber').select2({
                  theme: 'bootstrap'
              })

          }
        });

      }

      
  </script>  

@endsection