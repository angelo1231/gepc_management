<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Auth_Model extends Model
{
    
    public static function LoadAuthUserInfo($username){

        $result = DB::table('users')
        ->select(
            'id',
            DB::raw("CONCAT(lastname, ', ', firstname, ' ', mi) AS 'name'"),
            'password',
            'usertype'
        )
        ->where('username', '=', $username)
        ->get();

        return $result;

    }

    public static function AuthLogin($usertype){

        $result = DB::table('settingseditaccess')
        ->select(
            DB::raw("COUNT(*) AS 'editaccesscount'")
        )
        ->where('ugroupid', '=', $usertype)
        ->first();

        if($result->editaccesscount!=0){

            return true;

        }
        else{

            return false;

        }

    }

}
