<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Planning_Model extends Model
{
    
    public static function LoadCustomers(){

        $result = DB::table('customers')
        ->select('cid', 'customer')
        ->get();

        return $result;

    }

    public static function LoadCustomerItems($cid){

        if($cid=="All"){

            $result = DB::table('finishgoods')
            ->select(
                'fgid', 
                DB::raw("description AS 'item'")
            )
            ->get();
    
            return $result;

        }
        else{

            $result = DB::table('finishgoods')
            ->select(
                'fgid', 
                DB::raw("description AS 'item'")
            )
            ->where('cid', '=', $cid)
            ->get();
    
            return $result;

        }

    }

    public static function LoadRawMaterials(){

        $result = DB::table('rawmaterials')
        ->select(
            'rmid',
            DB::raw("IF(category!='Raw Material', CONCAT(name, ' ', description), CONCAT(IF(widthsize!='', CONCAT(width,'.',widthsize), width), ' x ', IF(lengthsize!='', CONCAT(length, '.', lengthsize), length), ' ', flute, ' ', boardpound)) AS description")
        )
        ->where('category', '=', 'Raw Material')
        ->get();

        return $result;

    }

    public static function SaveRequestMaterial($mrinumber, $cid, $fgid, $reason, $issuedby, $prid){

        $result = DB::table('materialrequest')
        ->insertGetID([
            "mrinumber"=>$mrinumber,
            "cid"=>$cid,
            "fgid"=>$fgid,
            "reason"=>$reason,
            "issuedby"=>$issuedby,
            "status"=>"Pending",
            "created_at"=>DB::raw("NOW()"),
            "prid"=>$prid
        ]);

        return $result;

    }

    public static function SaveRequestMaterialItem($mrid, $rmid, $reqqty, $remarks){

        $result = DB::table('materialrequestitems')
        ->insertGetId([
            "mrid"=>$mrid,
            "rmid"=>$rmid,
            "qtyrequired"=>$reqqty,
            "remarks"=>$remarks,
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function LoadCustomerPO($customer){

        if($customer=="All"){

            $result = DB::select(DB::raw("
                SELECT
                *
                FROM (
                    SELECT
                    pocustomerinformationitems.itemid,
                    pocustomerinformation.ponumber,
                    finishgoods.description,
                    pocustomerinformationitems.qty,
                    IFNULL((
                        SELECT
                        GROUP_CONCAT(salesnumber SEPARATOR '<br>')
                        FROM pocustomerinformationitemdelivery
                        WHERE itemid=pocustomerinformationitems.itemid
                        AND `status`='Open' AND deleted_at IS NULL
                        GROUP BY itemid
                    ), 'None') AS 'salesnumber',
                    IFNULL((
                        SELECT
                        GROUP_CONCAT(deliveryqty SEPARATOR '<br>')
                        FROM pocustomerinformationitemdelivery
                        WHERE itemid=pocustomerinformationitems.itemid
                        AND `status`='Open' AND deleted_at IS NULL
                        GROUP BY itemid
                    ), 'None') AS 'deliveryqty',
                    IFNULL((
                        SELECT
                        GROUP_CONCAT(deliverydate SEPARATOR '<br>')
                        FROM pocustomerinformationitemdelivery
                        WHERE itemid=pocustomerinformationitems.itemid
                        AND `status`='Open' AND deleted_at IS NULL
                        GROUP BY itemid
                    ), 'None') AS 'deliverydate',
                    pocustomerinformation.cid,
                    customers.customer
                    FROM pocustomerinformationitems
                    INNER JOIN pocustomerinformation ON pocustomerinformation.poid=pocustomerinformationitems.poid
                    INNER JOIN finishgoods ON finishgoods.fgid=pocustomerinformationitems.fgid
                    INNER JOIN customers ON customers.cid=pocustomerinformation.cid 
                ) AS planningdata  
            "));

            return $result;

        }
        else{

            $result = DB::select(DB::raw("
                SELECT
                *
                FROM (
                    SELECT
                    pocustomerinformationitems.itemid,
                    pocustomerinformation.ponumber,
                    finishgoods.description,
                    pocustomerinformationitems.qty,
                    IFNULL((
                        SELECT
                        GROUP_CONCAT(salesnumber SEPARATOR '<br>')
                        FROM pocustomerinformationitemdelivery
                        WHERE itemid=pocustomerinformationitems.itemid
                        AND `status`='Open' AND deleted_at IS NULL
                        GROUP BY itemid
                    ), 'None') AS 'salesnumber',
                    IFNULL((
                        SELECT
                        GROUP_CONCAT(deliveryqty SEPARATOR '<br>')
                        FROM pocustomerinformationitemdelivery
                        WHERE itemid=pocustomerinformationitems.itemid
                        AND `status`='Open' AND deleted_at IS NULL
                        GROUP BY itemid
                    ), 'None') AS 'deliveryqty',
                    IFNULL((
                        SELECT
                        GROUP_CONCAT(deliverydate SEPARATOR '<br>')
                        FROM pocustomerinformationitemdelivery
                        WHERE itemid=pocustomerinformationitems.itemid
                        AND `status`='Open' AND deleted_at IS NULL
                        GROUP BY itemid
                    ), 'None') AS 'deliverydate',
                    pocustomerinformation.cid,
                    customers.customer
                    FROM pocustomerinformationitems
                    INNER JOIN pocustomerinformation ON pocustomerinformation.poid=pocustomerinformationitems.poid
                    INNER JOIN finishgoods ON finishgoods.fgid=pocustomerinformationitems.fgid
                    INNER JOIN customers ON customers.cid=pocustomerinformation.cid
                ) AS planningdata
                WHERE planningdata.salesnumber!='None' AND planningdata.cid='". $customer ."'
            "));

            return $result;

        }

    }

    public static function CustomerPOProfile($poid){

        $result = DB::table('pocustomerinformation')
        ->select(
            'customers.customer',
            'pocustomerinformation.ponumber',
            'pocustomerinformation.remarks',
            'pocustomerinformation.deliverydate'
        )
        ->leftjoin('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
        ->where('pocustomerinformation.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function CustomerPOItemsProfile($poid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'pocustomerinformationitems.itemid',
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'finishgoods.ID_length',
            'finishgoods.ID_width',
            'finishgoods.ID_height',
            'finishgoods.OD_length',
            'finishgoods.OD_width',
            'finishgoods.OD_height',
            'pocustomerinformationitems.qty',
            'pocustomerinformationitems.delqty'
        )
        ->leftjoin('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->where('pocustomerinformationitems.poid', '=', $poid)
        ->get();

        return $result;

    }

    public static function GenJONumber($tableschema){

        $result = DB::select(DB::raw("
            SELECT CONCAT('JO', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) AS 'batch' FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'joborders'
        "));

        return $result[0]->batch;

    }

    public static function Padding($batch){

        $result = DB::select(DB::raw('SELECT LPAD('.$batch.', 4, "0") AS pad'));
        
        return $result[0]->pad;

    }

    public static function LoadMaterialRequestInfo($cid){

        if($cid=="All"){

            $result = DB::table('materialrequest')
            ->select(
                'requestid',
                'mrinumber'
            )
            ->where('status', '=', 'JO Creation')
            ->get();
            

            return $result;

        }
        else{
            
            $result = DB::table('materialrequest')
            ->select(
                'requestid',
                'mrinumber'
            )
            ->where('status', '=', 'JO Creation')
            ->where('cid', '=', $cid)
            ->get();
            

            return $result;

        }

    }

    public static function LoadJOPOItems($poid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'pocustomerinformationitems.itemid',
            DB::raw("finishgoods.description AS finishgood")
        )
        ->where('pocustomerinformationitems.poid', '=', $poid)
        ->leftjoin('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->get();

        return $result;

    }

    public static function GetRMMaterialRequest($requestid){

        $result = DB::table('materialrequestitems')
        ->select(
            'rmid'
        )
        ->where('mrid', '=', $requestid)
        ->get();

        return $result;
        
    }

    public static function GetRMStock($rmid){

        // $result = DB::table('materialrequest')
        // ->select(
        //     DB::raw("SUM(rawmaterials.stockonhand) AS 'stockonhand'")
        // )
        // ->leftjoin('rawmaterials', function($join){
        //     $join->whereRaw('FIND_IN_SET(rawmaterials.rmid, materialrequest.rmid)');
        // })
        // ->where('materialrequest.requestid', '=', $requestid)
        // ->get();

        $result = DB::table('rawmaterials')
        ->select(
            'stockonhand'
        )
        ->where('rmid', '=', $rmid)
        ->first();

        return $result->stockonhand;

    }

    public static function SaveJobOrderRMInfo($joid, $rmid, $stockonhand){

        DB::table('joborderrminfo')
        ->insert([
            "joid"=>$joid,
            "rmid"=>$rmid,
            "rmstockonhand"=>$stockonhand,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function CreateJobOrder($materialrequest, $targetoutput, $jorequestnumber, $issuedby, $poid, $itemid, $cid){

        $result = DB::table('joborders')
        ->insertGetId([
            "jorequestnumber"=>$jorequestnumber,
            "poid"=>$poid,
            "itemid"=>$itemid,
            "requestid"=>$materialrequest,
            "targetoutput"=>$targetoutput,
            "cid"=>$cid,
            "requestby"=>$issuedby,
            "requestdate"=>DB::raw("NOW()"),
            "status"=>"For Request",
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function UpdateMaterialRequest($requestid){

        DB::table('materialrequest')
        ->where('requestid', '=', $requestid)
        ->update([
            "status"=>"Done",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadJobOrderInformation($customer){

        if($customer=="All"){

            $result = DB::table('joborders')
            ->select(
                'joborders.joid',
                'joborders.status',
                'joborders.jorequestnumber',
                'joborders.jonumber',
                'pocustomerinformation.ponumber',
                DB::raw("finishgoods.description AS 'finishgood'"),
                DB::raw("
                    (
                        SELECT
                        GROUP_CONCAT(CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound) SEPARATOR '<br>')
                        FROM joborderrminfo
                        INNER JOIN rawmaterials ON rawmaterials.rmid=joborderrminfo.rmid
                        WHERE joborderrminfo.joid=joborders.joid
                        GROUP BY joborderrminfo.joid
                    ) AS 'rawmaterial'
                "),
                'joborders.targetoutput',
                DB::raw("CONCAT(DATE_FORMAT(joborders.deliverydatefrom, '%M %d'), ' to ', DATE_FORMAT(joborders.deliverydateto, '%M %d of %Y')) AS 'deliverydate'"),
                DB::raw("DATE_FORMAT(joborders.created_at, '%Y-%m-%d') AS 'issueddate'"),
                'joborders.issuedby',
                'joborders.qtyreceive',
                'customers.customer',
                DB::raw("DATE_FORMAT(joborders.requestdate, '%Y-%m-%d') AS 'requestdate'"),
                'joborders.requestby'
            )
            ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'joborders.poid')
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
            ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
            ->join('customers', 'customers.cid', '=', 'joborders.cid')
            ->orderBy('joborders.joid', 'DESC')
            ->get();


        }
        else{

            $result = DB::table('joborders')
            ->select(
                'joborders.joid',
                'joborders.status',
                'joborders.jonumber',
                'pocustomerinformation.ponumber',
                DB::raw("finishgoods.description AS 'finishgood'"),
                DB::raw("
                    (
                        SELECT
                        GROUP_CONCAT(CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound) SEPARATOR '<br>')
                        FROM joborderrminfo
                        INNER JOIN rawmaterials ON rawmaterials.rmid=joborderrminfo.rmid
                        WHERE joborderrminfo.joid=joborders.joid
                        GROUP BY joborderrminfo.joid
                    ) AS 'rawmaterial'
                "),
                'joborders.targetoutput',
                DB::raw("CONCAT(DATE_FORMAT(joborders.deliverydatefrom, '%M %d'), ' to ', DATE_FORMAT(joborders.deliverydateto, '%M %d of %Y')) AS 'deliverydate'"),
                DB::raw("DATE_FORMAT(joborders.created_at, '%Y-%m-%d') AS 'issueddate'"),
                'joborders.issuedby',
                'joborders.qtyreceive',
                'customers.customer',
                DB::raw("DATE_FORMAT(joborders.requestdate, '%Y-%m-%d') AS 'requestdate'"),
                'joborders.requestby'
            )
            ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'joborders.poid')
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
            ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
            ->join('customers', 'customers.cid', '=', 'joborders.cid')
            ->where('joborders.cid', '=', $customer)
            ->orderBy('joborders.joid', 'DESC')
            ->get();


        }

        
        return $result;

    }

    public static function LoadCustomerPOInformation($joid){

        $result = DB::table('joborders')
        ->select(
            'customers.customer',
            'joborders.jonumber',
            'pocustomerinformation.ponumber',
            'pocustomerinformationitems.qty AS poqty',
            DB::raw("CONCAT(DATE_FORMAT(joborders.deliverydatefrom, '%M %d'), ' to ', DATE_FORMAT(joborders.deliverydateto, '%M %d of %Y')) AS 'deliverydate'"),
            DB::raw("DATE_FORMAT(joborders.created_at, '%Y-%m-%d') AS 'issueddate'"),
            'finishgoods.partnumber',
            'finishgoods.partname',
            'finishgoods.description',
            'finishgoods.OD_length',
            'finishgoods.OD_width',
            'finishgoods.OD_height',
            'finishgoods.packingstd',
            'finishgoods.stockonhand',
            DB::raw("GROUP_CONCAT(CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length)) SEPARATOR ',') AS 'rawmaterial'"),
            'rawmaterials.flute',
            'rawmaterials.boardpound',
            DB::raw("GROUP_CONCAT(materialrequestitems.qtyissued SEPARATOR ',') AS 'qtyissued'"),
            DB::raw("GROUP_CONCAT(materialrequestitems.balance SEPARATOR ',') AS 'balance'"),
            DB::raw("GROUP_CONCAT(materialrequestitems.drnumber SEPARATOR ',') AS 'drnumber'"),
            'materialrequest.issuedby',
            'joborders.targetoutput',
            'joborders.specialinstruction',
            'joborders.style',
            'joborders.inkcolor',
            'finishgoods.processcode'
        )
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'joborders.poid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'joborders.itemid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join('materialrequest', 'materialrequest.requestid', '=', 'joborders.requestid')
        ->join('materialrequestitems', 'materialrequestitems.mrid', '=', 'materialrequest.requestid')
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'materialrequestitems.rmid')
        ->join('customers', 'customers.cid', '=', 'joborders.cid')
        ->where('joborders.joid', '=', $joid)
        ->groupBy('joborders.joid')
        ->get();

        return $result;

    }

    public static function GetFGProcess($joid){

        $result = DB::table('jobordermonitoring')
        ->select(
            'process.process',
            'joaudititemsdatetime.created_at',
            'joaudititemsqty.qty',
            'joaudititemsrejectqty.rejectqty',
            'joaudititemsrejecttype.rejecttype'
        )
        ->join('jobordermonitoringaudit', 'jobordermonitoringaudit.mid', '=', 'jobordermonitoring.mid')
        ->leftjoin(DB::raw("(
            SELECT
            auditid,
            IFNULL(GROUP_CONCAT(DATE_FORMAT(jobordermonitoringaudititems.created_at, '%Y-%m-%d %h:%i %p') SEPARATOR '<br>'), '') AS 'created_at'
            FROM jobordermonitoringaudititems
            GROUP BY auditid
        ) joaudititemsdatetime"), 'joaudititemsdatetime.auditid', '=', 'jobordermonitoringaudit.auditid')
        ->leftjoin(DB::raw("(
            SELECT
            auditid,
            IFNULL(GROUP_CONCAT(jobordermonitoringaudititems.qty SEPARATOR '<br>'), '') AS 'qty'
            FROM jobordermonitoringaudititems
            GROUP BY auditid
        ) joaudititemsqty"), 'joaudititemsqty.auditid', '=', 'jobordermonitoringaudit.auditid')
        ->leftjoin(DB::raw("(
            SELECT
            auditid,
            IFNULL(GROUP_CONCAT(jobordermonitoringaudititems.rejectqty SEPARATOR '<br>'), '') AS 'rejectqty'
            FROM jobordermonitoringaudititems
            GROUP BY auditid
        ) joaudititemsrejectqty"), 'joaudititemsrejectqty.auditid', '=', 'jobordermonitoringaudit.auditid')
        ->leftjoin(DB::raw("(
            SELECT
            jobordermonitoringaudititems.auditid,
            IFNULL(GROUP_CONCAT(IFNULL(qarejecttype.rejectcode, '-') SEPARATOR '<br>'), '') AS 'rejecttype'
            FROM jobordermonitoringaudititems
            LEFT JOIN qarejecttype ON qarejecttype.id=jobordermonitoringaudititems.rejectid
            GROUP BY auditid
        ) joaudititemsrejecttype"), 'joaudititemsrejecttype.auditid', '=', 'jobordermonitoringaudit.auditid')
        ->join('process', 'process.pid', '=', 'jobordermonitoringaudit.pid')
        ->where('jobordermonitoring.joid', '=', $joid)
        ->groupBy('jobordermonitoringaudit.auditid')
        ->get();

        return $result;

    }

    public static function GetProcess(){

        $result = DB::table('process')
        ->select(
            '*'
        )
        ->get();

        return $result;

    }

    public static function LoadJOSalesOrder($itemid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'delid',
            'salesnumber',
            'deliveryqty',
            'deliverydate'
        )
        ->where('itemid', '=', $itemid)
        ->where('status', '!=', 'Close')
        ->whereRaw("delid NOT IN (SELECT delid FROM joborderssoinfo)")
        ->get();

        return $result;

    }

    public static function GetDeliveryQtySalesOrder($delid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'deliveryqty'
        )
        ->whereIn('delid', $delid)
        ->get();

        return $result;

    }

    public static function LoadItemSONumber($itemid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'delid',
            'salesnumber',
            'deliveryqty',
            'deliverydate'
        )
        ->where('itemid', '=', $itemid)
        ->where('status', '=', 'Open')
        ->whereNull('deleted_at')
        ->get();

        return $result;

    }

    public static function LoadQuantityReqSO($delid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'deliveryqty'
        )
        ->whereIn('delid', $delid)
        ->get();

        return $result;

    }

    public static function SavePR($data){

        DB::table('purchaserequest')
        ->where('prid', '=', $data->prid)
        ->update([
            "reason"=>$data->reason,
            "status"=>"Pending"
        ]);
        
    }

    public static function UpdateSO($prid, $delid){

        DB::table('pocustomerinformationitemdelivery')
        ->whereIn('delid', $delid)
        ->update([
            'prid'=>$prid,
            'status'=>'Close',
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateRawMaterialDataPR($prid){

        $result = DB::table('purchaserequestitems')
        ->select(
            'itemid'
        )
        ->where('prid', '=', $prid)
        ->get();

        if(count($result)==0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function GetPurchaseRequestItems($prid){

        $result = DB::table('purchaserequestitems')
        ->select(
            'itemid'
        )
        ->where('prid', '=', $prid)
        ->get();

        return $result;

    }

    public static function ValidatePurchaseRequestItemDelivery($itemid){

        $result = DB::table('purchaserequestitemsdelivery')
        ->select(
            'id'
        )
        ->where('itemid', '=', $itemid)
        ->get();

        if(count($result)==0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SaveJobOrderSOInfo($joid, $delid){

        DB::table('joborderssoinfo')
        ->insert([
            "joid"=>$joid,
            "delid"=>$delid
        ]);

    }

    public static function LoadSOItemInformation($itemid){

        $result = DB::table('pocustomerinformationitemdelivery')
        ->select(
            'delid',
            'salesnumber',
            'deliveryqty',
            'deliverydate'
        )
        ->where('itemid', '=', $itemid)
        ->where('status', '=', 'Open')
        ->get();

        return $result;

    }

    public static function UpdateCustomerSOInformation($delid){

        DB::table('pocustomerinformationitemdelivery')
        ->where('delid', '=', $delid)
        ->update([
            "deleted_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteSOInformation($delid, $reason, $authid, $authname, $userid, $issuedby){

        DB::table('pocustomerinformationitemdeliverydeleted')
        ->insert([
            "delid"=>$delid,
            "reason"=>$reason,
            "authuid"=>$authid,
            "authname"=>$authname,
            "deluid"=>$userid,
            "delname"=>$issuedby,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetFGIDCustomer($fgid){

        $result = DB::table('finishgoods')
        ->select(
            'cid'
        )
        ->where('fgid', '=', $fgid)
        ->first();

        return $result->cid;

    }

    public static function LoadRawMaterialsExcess(){

        $result = DB::table('rawmaterials')
        ->select(
            'rmid',
            DB::raw("IF(category!='Excess Board', CONCAT(name, ' ', description), CONCAT(IF(widthsize!='', CONCAT(width,'.',widthsize), width), ' x ', IF(lengthsize!='', CONCAT(length, '.', lengthsize), length), ' ', flute, ' ', boardpound)) AS description")
        )
        ->where('category', '=', 'Excess Board')
        ->get();

        return $result;

    }

    public static function SaveExcessBoardItem($mritemid, $rmid, $qty){

        DB::table('materialrequestitemsexcess')
        ->insert([
            "mritemid"=>$mritemid,
            "excessrmid"=>$rmid,
            "qtyexcessboard"=>$qty,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function SaveNewExccessBoard($rmid, $width, $widthsize, $length, $lengthsize, $description){

        $rmiddata = DB::table('rawmaterials')
        ->select(
            'flute',
            'boardpound'
        )
        ->where('rmid', '=', $rmid)
        ->first();

        $result = DB::table('rawmaterials')
        ->insertGetId([
            "description"=>$description,
            "width"=>$width,
            "widthsize"=>$widthsize,
            "length"=>$length,
            "lengthsize"=>$lengthsize,
            "flute"=>$rmiddata->flute,
            "boardpound"=>$rmiddata->boardpound,
            "price"=>"0",
            "stockonhand"=>"0",
            "currencyid"=>"1",
            "category"=>"Excess Board",
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function ValidateExcessBoard($description){

        $result = DB::table('rawmaterials')
        ->select(
            DB::raw("COUNT(*) AS 'excesscount'")
        )
        ->where('description', '=', $description)
        ->first();

        if($result->excesscount!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function GetExcessBoardRMID($description){

        $result = DB::table('rawmaterials')
        ->select(
            'rmid'
        )
        ->where('description', '=', $description)
        ->first();

        return $result->rmid;

    }

    public static function CreateNewPRS($issuedby, $tableschema){

        $result = DB::table('purchaserequest')
        ->insertGetId([
            "prnumber"=>DB::raw("(SELECT CONCAT('PR', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'purchaserequest')"),
            "issuedby"=>$issuedby,
            "status"=>"",
            "created_at"=>DB::raw("NOW()")
        ]);

        return $result;

    }

    public static function GetPRNumber($prid){

        $result = DB::table('purchaserequest')
        ->select(
           "prnumber" 
        )
        ->where('prid', '=', $prid)
        ->first();

        return $result->prnumber;

    }

    public static function DeleteItemInfoDelivery($prid){

        DB::table('purchaserequestitemsdelivery')
        ->whereRaw("itemid IN (SELECT itemid FROM purchaserequestitems WHERE prid='". $prid ."')")
        ->delete();

    }

    public static function DeletePRInfo($prid){

        DB::table('purchaserequest')
        ->where('prid', '=', $prid)
        ->delete();

    }

    public static function DeletePRItem($prid){

        DB::table('purchaserequestitems')
        ->where('prid', '=', $prid)
        ->delete();

    }

    public static function SaveNewPRSItem($prid, $rmid, $qtyrequired){

        DB::table('purchaserequestitems')
        ->insert([
            "prid"=>$prid,
            "rmid"=>$rmid,
            "qtyrequired"=>$qtyrequired,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetPRItemInfo($prid){

        $result = DB::table('purchaserequestitems')
        ->select(
            'purchaserequestitems.itemid',
            DB::raw("CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound) AS 'rawmaterial'"),
            'purchaserequestitems.qtyrequired'
        )
        ->where('prid', '=', $prid)
        ->join('rawmaterials', 'rawmaterials.rmid', '=', 'purchaserequestitems.rmid')
        ->get();

        return $result;

    }

    public static function DeletePRItemIndi($itemid){

        DB::table('purchaserequestitems')
        ->where('itemid', '=', $itemid)
        ->delete();

    }

    public static function GetDelItemInfo($itemid){

        $result = DB::table('purchaserequestitemsdelivery')
        ->select(
            'id',
            'deliverydate',
            'deliveryqty'
        )
        ->where('itemid', '=', $itemid)
        ->get();

        return $result;

    }

    public static function SaveDelPRItemInfo($itemid, $deldate, $delqty){

        DB::table('purchaserequestitemsdelivery')
        ->insert([
            "itemid"=>$itemid,
            "deliverydate"=>$deldate,
            "deliveryqty"=>$delqty,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteDelPRItemIndi($itemid){

        DB::table('purchaserequestitemsdelivery')
        ->where('itemid', '=', $itemid)
        ->delete();

    }

    public static function UpdateSOPurchaseRequest($prid){

        DB::table('pocustomerinformationitemdelivery')
        ->where('prid', '=', $prid)
        ->update([
            "prid"=>0,
            "status"=>"Open",
            "updated_at"=>null
        ]);

    }

    public static function DeleteDelDeliveryInfo($id){

        DB::table('purchaserequestitemsdelivery')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function LoadPurchaseRequestInformation($customer){

        if($customer=="All"){

            $result = DB::table('purchaserequest')
            ->select(
                'purchaserequest.prid',
                'purchaserequest.prnumber',
                'pocustomerinformation.ponumber'
            )
            ->join(DB::raw("
                (SELECT * FROM pocustomerinformationitemdelivery GROUP BY prid) pocustomeritem
            "), 'pocustomeritem.prid', '=', 'purchaserequest.prid')
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomeritem.itemid')
            ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
            ->join('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
            ->where('purchaserequest.status', '=', 'Done')
            ->whereRaw("purchaserequest.prid NOT IN (SELECT prid FROM materialrequest)")
            ->get();

            return $result;

        }
        else {

            $result = DB::table('purchaserequest')
            ->select(
                'purchaserequest.prid',
                'purchaserequest.prnumber',
                'pocustomerinformation.ponumber'
            )
            ->join(DB::raw("
                (SELECT * FROM pocustomerinformationitemdelivery GROUP BY prid) pocustomeritem
            "), 'pocustomeritem.prid', '=', 'purchaserequest.prid')
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomeritem.itemid')
            ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
            ->join('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
            ->where('purchaserequest.status', '=', 'Done')
            ->where('pocustomerinformation.cid', '=', $customer)
            ->whereRaw("purchaserequest.prid NOT IN (SELECT prid FROM materialrequest)")
            ->get();

            return $result;

        }

    }

    public static function LoadPurchaseRequestData($prid){

        $result = DB::table('purchaserequest')
        ->select(
            'pocustomerinformationitems.fgid'
        )
        ->join(DB::raw("
            (SELECT * FROM pocustomerinformationitemdelivery GROUP BY prid) pocustomeritem
        "), 'pocustomeritem.prid', '=', 'purchaserequest.prid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomeritem.itemid')
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
        ->join('customers', 'customers.cid', '=', 'pocustomerinformation.cid')
        ->where('purchaserequest.prid', '=', $prid)
        ->first();

        return $result;

    }

    public static function LoadPurchaseRequestItemData($prid){

        $result = DB::table('purchaserequestitems')
        ->select(
            'rmid'
        )
        ->where('prid', '=', $prid)
        ->get();

        return $result;

    }

    public static function LoadMaterialRequestData($prid, $requestid){

        if($prid!=0){

            $result = DB::table('pocustomerinformationitemdelivery')
            ->select(
                'pocustomerinformation.poid',
                'pocustomerinformation.ponumber',
                'pocustomerinformationitems.itemid',
                'finishgoods.description',
                DB::raw("SUM(pocustomerinformationitemdelivery.deliveryqty) AS 'targetoutput'"),
                'pocustomerinformation.cid'
            )
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomerinformationitemdelivery.itemid')
            ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
            ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
            ->where('pocustomerinformationitemdelivery.prid', '=', $prid)
            ->groupBy('pocustomerinformationitemdelivery.prid')
            ->first();
    
            return $result;

        }
        else{

            $result = DB::table('pocustomerinformationitemdelivery')
            ->select(
                'pocustomerinformation.poid',
                'pocustomerinformation.ponumber',
                'pocustomerinformationitems.itemid',
                'finishgoods.description',
                DB::raw("SUM(pocustomerinformationitemdelivery.deliveryqty) AS 'targetoutput'"),
                'pocustomerinformation.cid'
            )
            ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomerinformationitemdelivery.itemid')
            ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
            ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
            ->where('pocustomerinformationitemdelivery.mrid', '=', $requestid)
            ->groupBy('pocustomerinformationitemdelivery.prid')
            ->first();
    
            return $result;

        }

    }

    public static function LoadCustomerFGSalesOrder($fgid){

        $result = DB::table('pocustomerinformationitems')
        ->select(
            'pocustomerinformationitemdelivery.delid',
            'pocustomerinformation.ponumber',
            'pocustomerinformationitemdelivery.salesnumber',
            'pocustomerinformationitemdelivery.deliveryqty',
            'pocustomerinformationitemdelivery.deliverydate'
        )
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
        ->join('pocustomerinformationitemdelivery', function($join){
            $join->on('pocustomerinformationitemdelivery.itemid', '=', 'pocustomerinformationitems.itemid')
            ->where('pocustomerinformationitemdelivery.status', '=', 'Open');
        })
        ->where('pocustomerinformationitems.fgid', '=', $fgid)
        ->get();

        return $result;

    }

    public static function LoadRawMaterialWithExcess(){

        $result = DB::table('rawmaterials')
        ->select(
            'rmid',
            DB::raw("CONCAT(IF(widthsize!='', CONCAT(width,'.',widthsize), width), ' x ', IF(lengthsize!='', CONCAT(length, '.', lengthsize), length), ' ', flute, ' ', boardpound) AS description")
        )
        // ->where('category', '=', 'Raw Material') 
        ->orWhere('category', '=', 'Excess Board')
        ->get();

        return $result;

    }

    public static function UpdateCustomerSOInfo($mrid, $sodata){


        DB::table('pocustomerinformationitemdelivery')
        ->whereIn('delid', $sodata)
        ->update([
            "mrid"=>$mrid,
            "status"=>"Close",
            "updated_at"=>DB::raw("NOW()")
        ]);


    }

    public static function GetMaterialRequestPRID($requestid){

        $result = DB::table('materialrequest')
        ->select(
            'prid'
        )
        ->where('requestid', '=', $requestid)
        ->first();

        return $result->prid;

    }

    public static function LoadMRForJobOrderCreation(){


        $union = DB::table('materialrequest')
        ->select(
            'materialrequest.requestid',
            'materialrequest.mrinumber',
            'pocustomerinformation.ponumber',
            'finishgoods.description',
            'pocustomerinformationitemdelivery.deliveryqty',
            'customers.customer'
        )
        ->join('pocustomerinformationitemdelivery', 'pocustomerinformationitemdelivery.mrid', '=', 'materialrequest.requestid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomerinformationitemdelivery.itemid')
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join('customers', 'customers.cid', '=', 'finishgoods.cid')
        ->where('materialrequest.status', '=', 'JO Creation')
        ->where('materialrequest.prid', '=', 0);

        $result = DB::table('materialrequest')
        ->select(
            'materialrequest.requestid',
            'materialrequest.mrinumber',
            'pocustomerinformation.ponumber',
            'finishgoods.description',
            'pocustomerinformationitemdelivery.deliveryqty',
            'customers.customer'
        )
        ->join('pocustomerinformationitemdelivery', 'pocustomerinformationitemdelivery.prid', '=', 'materialrequest.prid')
        ->join('pocustomerinformationitems', 'pocustomerinformationitems.itemid', '=', 'pocustomerinformationitemdelivery.itemid')
        ->join('pocustomerinformation', 'pocustomerinformation.poid', '=', 'pocustomerinformationitems.poid')
        ->join('finishgoods', 'finishgoods.fgid', '=', 'pocustomerinformationitems.fgid')
        ->join('customers', 'customers.cid', '=', 'finishgoods.cid')
        ->where('materialrequest.status', '=', 'JO Creation')
        ->where('materialrequest.prid', '!=', 0)
        ->union($union)
        ->get();

        return $result;


    }

}
