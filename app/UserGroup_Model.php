<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserGroup_Model extends Model
{
    
    public static function ValidateUserGroup($usergroup){

        $result = DB::table('usergroups')
        ->select(
            DB::raw("COUNT(*) AS 'usergroup_count'")
        )
        ->where('usergroup', '=', $usergroup)
        ->first();

        if($result->usergroup_count!=0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SaveUserGroup($usergroup){

        DB::table('usergroups')
        ->insert([
            'usergroup'=>$usergroup,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function LoadUserGroupInformation(){

        $result = DB::table('usergroups as group')
        ->select(
            'group.ugroupid',
            'group.usergroup',
            DB::raw("(
                SELECT GROUP_CONCAT(access.access) 
                FROM useraccess
                INNER JOIN access ON access.accessid=useraccess.accessid
                WHERE useraccess.ugroupid=group.ugroupid
                GROUP BY useraccess.ugroupid
            ) AS 'access'")
        )
        ->get();

        return $result;

    }

    public static function LoadAccess(){

        $result = DB::table('access')
        ->select(
            'accessid',
            'access'
        )
        ->get();

        return $result;

    }

    public static function DeleteUserGroupAccess($ugroupid){

        DB::table('useraccess')
        ->where('ugroupid', '=', $ugroupid)
        ->delete();

    }

    public static function SaveUserGroupAccess($sql){

        DB::select(DB::raw($sql));

    }

    public static function GetUserGroupAccess($ugroupid){

        $result = DB::table('useraccess')
        ->select(
            DB::raw("GROUP_CONCAT(accessid) AS 'access'")
        )
        ->where('ugroupid', '=', $ugroupid)
        ->groupBy('ugroupid')
        ->get();

        return $result;

    }


}
