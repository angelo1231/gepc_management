<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Main_Model extends Model
{
    
    public static function LoadPOForApproval(){

        $result = DB::table('poinformation')
        ->select(
            'poid',
            'ponumber',
            'issuedby',
            'created_at'
        )
        ->whereRaw("approvedby IS NULL")
        ->get();

        return $result;

    }

}
