@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Finished Goods Inventory</b></h3>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="cmbcustomer">Customer</label>
                        <select class="form-control" id="cmbcustomer">
                        </select>
                    </div>
                </div>
                <div class="col-md-9">
                    <br>

                    <button id="btnfgaudit" name="btnfgaudit" class="btn btn-flat btn-primary" style="float: right;"><i class="fa fa-info-circle"></i> Audit Information</button>

                    <button id="btndel" name="btndel" class="btn btn-flat btn-primary" style="float: right; margin-right: 10px;"><i class="fa fa-tasks"></i> Delivery</button>

                    <button id="btnpreparationslip" name="btnpreparationslip" class="btn btn-flat btn-primary" style="float: right; margin-right: 10px;"><i class="fa fa-tasks"></i> Preparation Slip</button>

                    <button id="btnproduction" name="btnproduction" class="btn btn-flat btn-primary" style="float: right; margin-right: 10px;"><i class="fa fa-tasks"></i> Production</button>

                </div>

                <div class="col-md-12">
                    <table id="tblfinishgoods" class="table">
                        <thead>
                            <tr>
                                <th>Partnumber</th>
                                <th>Partname</th>
                                <th>Description</th>
                                <th>Size</th>
                                <th>Packing STD</th>
                                <th>Currency</th>
                                <th>Price</th>
                                <th>Stocks</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">

          <div class="col-md-12">
            <button id="btnexcel" name="btnexcel" class="btn btn-flat btn-info" style="float: right;" disabled><i class="fa fa-file-excel-o"></i> Excel</button>
            <button id="btnprint" name="btnprint" class="btn btn-flat btn-info" style="float: right; margin-right: 10px;" disabled><i class="fa fa-print"></i> Print</button>
          </div>

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  @include('modal.finishgoods.inventory.finishgoodsproduction')
  @include('modal.finishgoods.inventory.individualaudit')
  @include('modal.finishgoods.inventory.fgupdatestocks')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblfinishgoods;
      var sfgid;

      $(document).ready(function(){

        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
          toastr.success(msg, '', { positionClass: 'toast-top-center' });
        }
        
        SetSelect2();
        SetDatatable();
        SetDatePicker();
        LoadCountPS();
        LoadCustomers($('#cmbcustomer').attr('id'));
         
      });

      $('#cmbcustomer').on('change', function(){

          var customer = $(this).val();
          LoadFinishGoodsInformation(customer);
          $('#btnexcel').prop('disabled', false);
          $('#btnprint').prop('disabled', false);

      });

      $('#btnfgaudit').on('click', function(){

        window.location = "{{ url('/fginventory/fgaudit') }}";

      });

      $('#btndel').on('click', function(){

        @if(in_array(46, $access))

            window.location = "{{ url('/fginventory/delivery') }}";

        @else
        
            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnproduction').on('click', function(){

        @if(in_array(45, $access))

            window.location = "{{ url('/fginventory/production') }}";

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnpreparationslip').on('click', function(){

        window.location = "{{ url('/fginventory/fgpreparationslip') }}";

      });

      $(document).on('click', '#btndelivery', function(){

        @if(in_array(46, $access))

            

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $(document).on('click', '#btnaudittrail', function(){

        var fgid = $(this).val();
        $('#btngenerate').val(fgid);

        $('#content').hide();
        LoadStartEndDate();

        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadfginfo") }}',
            type: 'get',
            data: {
                fgid: fgid
            },
            dataType: 'json',
            success: function(response){

                $('#lblinformation').text('');
                $('#lblinformation').text(response.partnumber + ' ' + response.partname + ' ' + response.description);

            }
        });

      });

      $('#btngenerate').on('click', function(){

        var fgid = $(this).val();
        var from = $('#txtdatefrom').val();
        var to = $('#txtdateto').val();
       
        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadfgauditinformation") }}',
            type: 'get',
            data: {
                fgid: fgid,
                from: from,
                to: to
            },
            beforeSend: function(){
                $('#content').hide();
            },
            success: function(response){

                $('#content').html(response);

            },
            complete: function(){
                $('#content').fadeIn("slow");
            }
        });

      });

      $(document).on('click', '#btninventory', function(){

        @if(in_array(45, $access))

            var fgid = $(this).val();
            $('#btnpsave').val(fgid);

            LoadFGJONumber(fgid);
            
            $.ajax({
                url: '{{ url("api/finishgoodsinv/loadfginfo") }}',
                type: 'get',
                data: {
                    fgid: fgid
                },
                dataType: 'json',
                success: function(response){

                    $('#lblpinformation').text('');
                    $('#lblpinformation').text(response.partnumber + ' ' + response.partname + ' ' + response.description);
                    $('#finishgoodproduction').modal('toggle');

                }
            });

        @else
        
            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnpsave').on('click', function(){

        var fgid = $(this).val();
        var jonumber = $('#cmbpjonumber').val();
        var qty = $('#txtpqty').val();

        if(jonumber==null){
            toastr.error('Please select a job order number.', '', { positionClass: 'toast-top-center' });
        }
        else if(qty==""){
            toastr.error('Please input a quantity.', '', { positionClass: 'toast-top-center' });
        }
        else if(qty < 0){
            toastr.error("Please input a non negative number in the deliverd quantity.", '', { positionClass: 'toast-top-center' });
        }
        else if(qty==0){
            toastr.error("Please input a greater than zero in the deliverd quantity.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/finishgoodsinv/savefgindividualproduction") }}',
                type: 'post',
                data: {
                    fgid: fgid,
                    jonumber: jonumber,
                    qty: qty
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#finishgoodproduction .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadFinishGoodsInformation();

                    }

                }
            });

        }

      });

      $('#btnprint').on('click', function(){

        var customerid = $('#cmbcustomer').val();
        window.open("{{ url('/fginventory/finishgoodprint/printfg') }}/"+customerid, '_blank');

      });

      $('#btnexcel').on('click', function(){

        var customerid = $('#cmbcustomer').val();
        window.open("{{ url('api/finishgoodsinv/downloadexcel') }}/"+customerid, '_blank');

      });

      $(document).on('click', '#btnupdatestocks', function(){

        sfgid = $(this).val();

        ClearUpdateStocks();
        LoadFGStockOnHand(sfgid);
        $('#fgupdatestocks').modal('toggle');

      });

      $('#btnsaveupdatestocks').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this stock on hand information of the item?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveStockOnHandInformation(sfgid);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

      });

      function SaveStockOnHandInformation(fgid){

        var qty = $('#txtuqtystock').val();
        var remarks = $('#txturemarksstock').val();

        //Validation
        if(qty==""){

            toastr.error("Please input a quantity.", '', { positionClass: 'toast-top-center' });


        }
        else if(remarks==""){

            toastr.error("Please input the remarks.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/finishgoodsinv/savestockonhandinformation") }}',
                type: 'post',
                data: {
                    fgid: fgid,
                    qty: qty,
                    remarks: remarks
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#fgupdatestocks .close').click();
                        ReloadFinishGoodsInformation();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });


                    }

                }
            });

        }

      }

      function LoadFGStockOnHand(fgid){

        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadfgstockonhand") }}',
            type: 'get',
            data: {
                fgid: fgid
            },
            dataType: 'json',
            success: function(response){

                $('#lblinformationstock').text('');
                $('#lblinformationstock').text(response.description);
                $('#txtuqtystock').val(response.stockonhand);

            }
        });

      }

      function LoadFGJONumber(fgid){

        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadfgjo") }}',
            type: 'get',
            data: {
                fgid: fgid
            },
            dataType: 'json',
            success: function(response){

                $('#cmbpjonumber').find('option').remove();
                $('#cmbpjonumber').append('<option value="" disabled selected>Select a Job Order Number</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#cmbpjonumber').append('<option value="'+  response.data[i]["joid"] +'">'+  response.data[i]["jonumber"] +'</option>');
                }

                $('#cmbpjonumber').select2({
                    theme: 'bootstrap'
                });

            }
        });

      }

      function LoadStartEndDate(){

        $.ajax({
            url: '{{ url("api/finishgoodsinv/loadstartenddate") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtdatefrom').val(response.datefrom);
                $('#txtdateto').val(response.dateto);

            }
        });

      }

      function LoadFinishGoodsInformation(customer){

          tblfinishgoods.destroy();
          tblfinishgoods = $('#tblfinishgoods').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/finishgoodsinv/loadfinishgoodsinformation") }}',
                data: {customer: customer},
            },
            columns : [
                {data: 'partnumber', name: 'partnumber'},
                {data: 'partname', name: 'partname'},
                {data: 'description', name: 'description'},
                {data: 'size', name: 'size'},
                {data: 'packingstd', name: 'packingstd'},
                {data: 'currency', name: 'currency'},
                {data: 'price', name: 'price'},
                {data: 'stockonhand', name: 'stockonhand'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
          });

            // tblfinishgoods.columns(5).visible(false);
            // tblfinishgoods.columns(6).visible(false);    
          

      }

      function LoadCustomers(id){

          $.ajax({
              url: '{{ url("api/finishgoodsinv/loadcustomers") }}',
              type: 'get',
              dataType: 'json',
              success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Customer</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                }

              }
          });

      }

      function SetDatatable(){

          tblfinishgoods = $('#tblfinishgoods').DataTable({
              autoWidth: false,
              ordering: false,
          });

            // tblfinishgoods.columns(5).visible(false);
            // tblfinishgoods.columns(6).visible(false);    

      }

      function SetSelect2(){

        $('#cmbcustomer').select2({
          theme: 'bootstrap'
        });

      }

      function SetDatePicker(){

        $("#txtdatefrom").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

         $("#txtdateto").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

      }

      function ReloadFinishGoodsInformation(){

        var customer = $('#cmbcustomer').val();
        if(customer!=null){
            tblfinishgoods.ajax.reload();
        }

      }

      function ClearUpdateStocks(){

        $('#txtuqtystock').val('');
        $('#txturemarksstock').val('');

      }

      function LoadCountPS(){

        $.ajax({
          url: '{{ url("api/finishgoodsinv/loadcountps") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            if(response.prcount!=0){

              $('#btnpreparationslip').text('');
              $('#btnpreparationslip').append('<i class="fa fa-tasks" style="font-size:12px;"></i> Preparation Slip <span id="prcount" class="dot">'+ response.pscount +'</span>');

            }
            else{

              $('#btnpreparationslip').text('');
              $('#btnpreparationslip').append('<i class="fa fa-tasks" style="font-size:12px;"></i> Preparation Slip');

            }

          }
        });

      }

    //Socket functions
    socket.on('reloadfginvinformation', function(){

        ReloadFinishGoodsInformation();

    });

    socket.on('reloadfgpreparationslip', function(){

      LoadCountPS();

    });

  </script>  

@endsection