<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="updategroupprocess" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>UPDATE PROCESS</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtuprocesscode">Process Code</label>
                            <input type="text" name="txtuprocesscode" id="txtuprocesscode" class="form-control" placeholder="Process Code">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtustyle">Style</label>
                            <input type="text" name="txtustyle" id="txtustyle" class="form-control" placeholder="Style">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtuinkcolor">Ink Color</label>
                            <input type="text" name="txtuinkcolor" id="txtuinkcolor" class="form-control" placeholder="Ink Color">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <table id="tbluprocess" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Process</th>
                                    <th class="text-right"><button id="btunadd" name="btunadd" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button></th>
                                </tr>
                            </thead>
                           
                            <tbody id="uprocesscontent">
                                <tr>
                                    {{-- <td style="vertical-align: middle;"><input type="text" id="txtuorder1" name="txtuorder[]" class="form-control" value="1" style="width: 35px;" readonly></td>
                                    <td><select name="cmbuprocess[]" id="cmbuprocess1" class="form-control" style="width: 250px;"></select></td>
                                    <td></td> --}}
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    
                    <button type="button" class="btn btn-danger btn-flat" style="margin-top: 10px; float: right;" data-dismiss="modal">Close</button>

                    <button id="btndelete" name="btndelete" type="button" class="btn btn-danger btn-flat" style="margin-top: 10px; float: right; margin-right: 10px;">Delete</button>

                    <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-flat" style="margin-top: 10px; float: right; margin-right: 10px;">Update</button>
                    
                </div>
            </div>
        </div>
    </div>
</div>