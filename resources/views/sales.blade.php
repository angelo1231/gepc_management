@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Sales Information</b></h3>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                {{-- <div class="form-group col-md-4 row">
                    <label for="cmbcustomer">Customer</label>
                    <select name="cmbcustomer" id="cmbcustomer" class="form-control"></select>
                </div> --}}
                <div style="margin-top: 27px;">

                    <button id="btnnewpreparationslip" name="btnnewpreparationslip" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-plus"></i> New Preparation Slip</button>

                    {{-- <button id="btnnewsales" name="btnnewsales" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-plus"></i> New Sales Information</button> --}}

                    <button id="btncustomerpo" name="btncustomerpo" class="btn btn-flat btn-primary" style="float: right; margin-right: 10px; margin-bottom: 20px;"><i class="fa fa-tasks"></i> Purchase Order</button>

                </div>
                
                
            </div>

            <div class="col-md-12">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#drinfo">Delivery Information</a></li>
                    <li><a data-toggle="tab" href="#psinfo">Preparation Slip Information</a></li>
                </ul>

                <div class="tab-content">
                    <div id="drinfo" class="tab-pane fade in active">

                        <br>
                        <div class="col-md-6">
                            <label for="cmbcustomer">Customer</label>
                            <select name="cmbcustomer" id="cmbcustomer" class="form-control"></select>
                        </div>
                        
                        
                        <div class="col-md-12">
                            <br>
                            <table id="tbldelivery" class="table">
                                <thead>
                                    <tr>
                                        <th>DR #</th>
                                        <th>Total Quantity</th>
                                        <th>Grand Total</th>
                                        <th>Delivery Date</th>
                                        <th>Issued By</th>
                                        <th>Created Date</th>
                                        <th>Customer</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div id="psinfo" class="tab-pane fade">

                        <br>
                        <div class="col-md-6">
                            <label for="cmbcustomerslip">Customer</label>
                            <select name="cmbcustomerslip" id="cmbcustomerslip" class="form-control"></select>
                        </div>
                        
                        
                        <div class="col-md-12">
                            <br>
                            <table id="tblpreparationslip" class="table">
                                <thead>
                                    <tr>
                                        <th>Slip #</th>
                                        <th>Total Quantity</th>
                                        <th>Issued By</th>
                                        <th>Created Date</th>
                                        <th>Customer</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modal --}}
  @include('modal.sales.newinvoice')
  @include('modal.sales.createdr')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tbldelivery;
      var sdid;
      var scid;
      var spscid;
      var tblpreparationslip;
      var spsid;

      $(document).ready(function() {
     
        var msg = "{{ Session::get('message') }}";
        if(msg!=""){

            toastr.success(msg, '', { positionClass: 'toast-top-center' });
            
        }

        //Set Datepicker
        $("#txtdeliverydate").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        //Load
        LoadCustomers();
        SetDatatables();

     });

     $('#cmbcustomer').on('change', function(){

        scid = $(this).val();
        LoadDeliveryInformation(scid);

     });

     $('#cmbcustomerslip').on('change', function(){

        spscid = $(this).val();
        LoadPreparationSlipInformation(spscid);

     });

     $(document).on('click', '#btncreateinvoice', function(){

        sdid = $(this).val();
        $('#newinvoice').modal('toggle');

     });

     $(document).on('click', '#btndownload', function(){

        var did = $(this).val();
        window.open("{{ url('api/sales/downloadexcel') }}/"+did, '_blank');

     });

     $('#btnsaveinvoice').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Create invoice?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        CreateInvoice();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

     });

     $('#btnnewsales').on('click', function(){

        @if(in_array(48, $access))

            window.location = "{{ url('/sales/newsales') }}";

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

     });

     $('#btncustomerpo').on('click', function(){

        @if(in_array(49, $access))

            window.location = "{{ url('/sales/cuspurchaseorder') }}";

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif


     });

     $('#btnnewpreparationslip').on('click', function(){

        window.location = "{{ url('/sales/newsalespreparationslip') }}";        

     });

     $(document).on('click', '#btncreatedr', function(){

        spreparationid = $(this).val();
        $('#createdr').modal('toggle');


     });

     $('#btncreate').on('click', function(){

        $.confirm({
              title: 'Create Delivery Information',
              content: 'Create Delivery Reciepe?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        CreateDR(spreparationid);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

     });

     function CreateDR(preparationid){

        var deliverynumber = $('#txtdeliverynumber').val();
        var deliverydate = $('#txtdeliverydate').val();

        //Validation
        if(deliverynumber==""){

            toastr.error("Please input the delivery number.", '', { positionClass: 'toast-top-center' });
            $('#txtdeliverynumber').focus();

        }
        else if(deliverydate==""){

            toastr.error("Please select the delivery date.", '', { positionClass: 'toast-top-center' });
            $('#txtdeliverydate').focus();

        }
        else{

            $.ajax({
                url: '{{ url("api/sales/createdr") }}',
                type: 'post',
                data: {
                    preparationid: preparationid,
                    deliverynumber: deliverynumber,
                    deliverydate: deliverydate
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#createdr .close').click();
                        LoadDeliveryInformation(scid);
                        LoadPreparationSlipInformation(spscid);

                    }

                }
            });

        }

     }

     function CreateInvoice(){

        var invoicenumber = $('#txtninvoicenumber').val();

        //Validation
        if(invoicenumber==""){

            toastr.error("Please input the invoice number.", '', { positionClass: 'toast-top-center' });
            $('#txtninvoicenumber').focus();

        }
        else{

            $.ajax({
                url: '{{ url("api/sales/createinvoice") }}',
                type: 'post',
                data: {
                    did: sdid,
                    invoicenumber: invoicenumber
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadDeliveryInformation();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newinvoice .close').click();

                        //Socket Emit
                        socket.emit('deliverysalesinformation');

                    }

                }
            });

        }


     }

     function LoadDeliveryInformation(cid){

        tbldelivery.destroy();
        tbldelivery = $('#tbldelivery').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/sales/loaddeliveryinformation") }}',
                data: {
                    cid: cid
                },
            },
            columns : [
                {data: 'drnumber', name: 'drnumber'},
                {data: 'totalqty', name: 'totalqty'},
                {data: 'grandtotal', name: 'grandtotal'},
                {data: 'deldate', name: 'deldate'},
                {data: 'issuedby', name: 'issuedby'},
                {data: 'createdat', name: 'createdat'},
                {data: 'customer', name: 'customer'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

     }

     function LoadPreparationSlipInformation(cid){

        tblpreparationslip.destroy();
        tblpreparationslip = $('#tblpreparationslip').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/sales/loadpreparationslipinformation") }}',
                data: {
                    cid: cid
                },
            },
            columns : [
                {data: 'psnumber', name: 'psnumber'},
                {data: 'totalqty', name: 'totalqty'},
                {data: 'issuedby', name: 'issuedby'},
                {data: 'createdat', name: 'createdat'},
                {data: 'customer', name: 'customer'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

     }

     function LoadCustomers(){

            $.ajax({
                url: "{{ url('api/sales/newpurchaseorder/loadcustomer') }}",
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $('#cmbcustomer').find('option').remove();
                    $('#cmbcustomer').append('<option value="" disabled selected>Select a Customer</option>');
                    $('#cmbcustomer').append('<option value="All">All</option>');
                    for (var i = 0; i < response.data.length; i++) {
                        $('#cmbcustomer').append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                    }

                    $('#cmbcustomerslip').find('option').remove();
                    $('#cmbcustomerslip').append('<option value="" disabled selected>Select a Customer</option>');
                    $('#cmbcustomerslip').append('<option value="All">All</option>');
                    for (var i = 0; i < response.data.length; i++) {
                        $('#cmbcustomerslip').append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                    }

                    $('#cmbcustomer').select2({
                        theme: 'bootstrap'
                    });

                    $('#cmbcustomerslip').select2({
                        theme: 'bootstrap'
                    });

                },
                complete: function(){

                    $('#cmbcustomer').val("All").trigger("change");
                    $('#cmbcustomerslip').val("All").trigger("change");

                }
            });

    }

    function SetDatatables(){

      tbldelivery = $('#tbldelivery').DataTable({
          autoWidth: false,
          ordering: false,
      });

      tblpreparationslip = $('#tblpreparationslip').DataTable({
          autoWidth: false,
          ordering: false,
      }); 
 
    }

    function ReloadDeliveryInformation(){

        tbldelivery.ajax.reload();

    }



  </script>  

@endsection