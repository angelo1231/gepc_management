<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('settings')->insert([
            'vatexempt' => '.20',
            'vatablesales' => '.30',
            'zerorated' => '0'
        ]);

    }
}
