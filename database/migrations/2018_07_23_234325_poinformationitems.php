<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Poinformationitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poinformationitems', function (Blueprint $table) {
            $table->increments('itemid');
            $table->integer('poid');
            $table->integer('pritemid');
            $table->integer('rmid');
            $table->integer('currencyid');
            $table->text('rate');
            $table->integer('qty');
            $table->integer('papercomid');
            $table->integer('papercompriceid');
            $table->decimal('price', 10, 2);
            $table->decimal('total', 10, 2);
            $table->integer('delqty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poinformationitems');
    }
}
