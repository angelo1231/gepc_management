@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Purchase Order Information</b></h3>
          <a href="{{ url("/sales") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-3" style="margin-bottom: 15px;">
                <label for="cmbcustomer">Customer</label>
                <select id="cmbcustomer" name="cmbcustomer" class="form-control">
                </select>
            </div>
            <div class="col-md-9">
               
                <button id="btnnewpurchaseorder" name="btnnewpurchaseorder" class="btn btn-flat btn-success" style="float: right; margin-bottom: 15px; margin-top: 23px;"><i class="fa fa-plus"></i> New Purchase Order</button>

                <button id="btnexcel" name="btnexcel" class="btn btn-flat btn-info" style="float: right; margin-bottom: 15px; margin-top: 23px; margin-right: 10px;"><i class="fa fa-file-excel-o"></i> Excel</button>

                   
            </div>
            
            <div class="col-md-12">

                <table id="tblcustomerpo" class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Customer</th>
                            <th>PO #</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>Served</th>
                            <th>Unserved</th>
                            <th>Price</th>
                            <th>PO Bal Amount</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblcustomerpo;
    var scustomer;

    $(document).ready(function(){

        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
          toastr.success(msg, '', { positionClass: 'toast-top-center' });
        }

        tblcustomerpo = $('#tblcustomerpo').DataTable({
            autoWidth: false,
            ordering: false,
        });

        LoadCustomer($('#cmbcustomer').attr('id'));
       
    });

    $('#cmbcustomer').on('change', function(){

        scustomer = $(this).val();
        LoadCustomerPO();

    });

    function LoadCustomer(id){

        $.ajax({
            url: '{{ url("api/sales/cuspurchaseorder/loadcustomer") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Customer</option>');
                $('#'+id).append('<option value="All">All</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["cid"] +'">'+  response.data[i]["customer"] +'</option>');
                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){
                
                $('#'+id).val("All").trigger("change");
                
            }
        });

    }

    $('#btnnewpurchaseorder').on('click', function(){

        @if(in_array(50, $access))

            window.location = "{{ url('/sales/newcuspurchaseorder') }}";

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });
    
        @endif

    });

    function LoadCustomerPO(){

        tblcustomerpo.destroy();
        tblcustomerpo = $('#tblcustomerpo').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/sales/cuspurchaseorder/loadcustomerpo") }}',
                data: {
                    customer: scustomer
                },
            },
            columns : [
                {data: 'status', name: 'status'},
                {data: 'customer', name: 'customer'},
                {data: 'PO', name: 'PO'},
                {data: 'item', name: 'item'},
                {data: 'qty', name: 'qty'},
                {data: 'served', name: 'served'},
                {data: 'unserved', name: 'unserved'},
                {data: 'price', name: 'price'},
                {data: 'pobalamount', name: 'pobalamount'},
                {data: 'panel', name: 'panel', width: '5%'},
            ]
        });

    }

    function ReloadCustomerPO(){

        if(scustomer!=null){
            tblcustomerpo.ajax.reload();
        }

    }

  </script>  

@endsection