<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Deliveryinformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveryinformation', function (Blueprint $table) {
            $table->increments('did');
            $table->string('sinumber', 60);
            $table->string('drnumber', 60);
            $table->decimal('grandtotal');
            $table->integer('cid');
            $table->text('taxclassification');
            $table->decimal('tax', 10, 2);
            $table->date('deliverydate');
            $table->text('status');
            $table->text('issuedby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveryinformation');
    }
}
