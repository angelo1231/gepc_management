@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Delivery Sales Information</b></h3>
        </div>
        <div class="box-body">

          <div class="col-md-12">

            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#salesdetails">Sales Details</a></li>
              <li><a data-toggle="tab" href="#salessummary">Sales Summary</a></li>
              <li><a data-toggle="tab" href="#salespercustomer">Sales Per Customer</a></li>
            </ul>

            <div class="tab-content">

                <div id="salesdetails" class="tab-pane fade in active">

                    <br>
                    <div class="form-group col-md-4">
                        <label for="cmbcustomersalesdetails">Customer</label>
                        <select name="cmbcustomersalesdetails" id="cmbcustomersalesdetails" class="form-control"></select>
                    </div>

                    <div class="col-md-12">

                        <table id="tblsalesdetails" class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Invoice #</th>
                                    <th>DR #</th>
                                    <th>Customer Item</th>
                                    <th>Qty</th>
                                    <th>Unit Price</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                          </table>

                    </div>

                </div>

                <div id="salessummary" class="tab-pane fade">

                    <br>
                    <div class="form-group col-md-4">
                        <label for="cmbcustomersalessummary">Customer</label>
                        <select name="cmbcustomersalessummary" id="cmbcustomersalessummary" class="form-control"></select>
                    </div>

                    <div class="col-md-12">

                        <table id="tblsalessummary" class="table">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Invoice #</th>
                                    <th>DR #</th>
                                    <th>Customer</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                          </table>

                    </div>

                </div>

                <div id="salespercustomer" class="tab-pane fade">

                  <br>
                  <div class="form-group col-md-4">
                      <label for="txtdtfrom">From</label>
                      <input type="text" id="txtdtfrom" name="txtdtfrom" class="form-control" placeholder="From">
                  </div>

                  <div class="form-group col-md-4">
                      <label for="txtdtto">To</label>
                      <input type="text" id="txtdtto" name="txtdtto" class="form-control" placeholder="To">
                  </div>

                  <div class="form-group col-md-4">
                      <button type="button" class="btn btn-flat btn-primary" id="btngenerate" name="btngenerate" style="margin-top: 24px;">Generate</button>
                  </div>

                  <div class="col-md-12">

                      <table id="tblsalespercustomer" class="table">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                      </table>

                  </div>
                  
                </div>

            </div>

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblsalesdetails;
    var tblsalessummary;
    var tblsalespercustomer;

    $(document).ready(function(){

      //Load
      LoadCustomer();

      //Set
      SetDataTable();
      SetDatePicker();

    });


    function LoadCustomer(){

      $.ajax({
        url: '{{ url("api/deliverysales/loadcustomer") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

          //Sales Details
          $('#cmbcustomersalesdetails').find('option').remove();
          $('#cmbcustomersalesdetails').append(response.content);

          //Sales Summary
          $('#cmbcustomersalessummary').find('option').remove();
          $('#cmbcustomersalessummary').append(response.content);


        },
        complete: function(){

          $('#cmbcustomersalesdetails').select2({
              theme: 'bootstrap'
          });

          $('#cmbcustomersalessummary').select2({
              theme: 'bootstrap'
          });

        }
      });

    }

    $('#cmbcustomersalesdetails').on('change', function(){

      var cid = $(this).val();
      LoadDeliverySalesInformation(cid);

    });

    $('#cmbcustomersalessummary').on('change', function(){

      var cid = $(this).val();
      LoadSalesSummary(cid);

    });

    $('#btngenerate').on('click', function(){

      var datefrom = $('#txtdtfrom').val();
      var dateto = $('#txtdtto').val();

      if(datefrom==""){

        toastr.error("Please select a date from.", '', { positionClass: 'toast-top-center' });

      }
      else if(dateto==""){

        toastr.error("Please select a date to.", '', { positionClass: 'toast-top-center' });

      }
      else{

        LoadSalesPerCustomer(datefrom, dateto);

      }


    });

    function LoadSalesPerCustomer(from, to){

      tblsalespercustomer.destroy();
      tblsalespercustomer = $('#tblsalespercustomer').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/deliverysales/loadsalespercustomer") }}',
              data: {
                  datefrom: from,
                  dateto: to
              },
          },
          columns : [
              {data: 'customer', name: 'customer'},
              {data: 'total', name: 'total'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
      });

    }

    function LoadDeliverySalesInformation(cid){

      tblsalesdetails.destroy();
      tblsalesdetails = $('#tblsalesdetails').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/deliverysales/loaddeliverysalesinformation") }}',
              data: {
                  cid: cid
              },
          },
          columns : [
              {data: 'date', name: 'date'},
              {data: 'invoicenumber', name: 'invoicenumber'},
              {data: 'drnumber', name: 'drnumber'},
              {data: 'customeritem', name: 'customeritem'},
              {data: 'qty', name: 'qty'},
              {data: 'unitprice', name: 'unitprice'},
              {data: 'total', name: 'total'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
      });

    }

    function LoadSalesSummary(cid){

      tblsalessummary.destroy();
      tblsalessummary = $('#tblsalessummary').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/deliverysales/loadsalessummary") }}',
              data: {
                  cid: cid
              },
          },
          columns : [
              {data: 'date', name: 'date'},
              {data: 'invoicenumber', name: 'invoicenumber'},
              {data: 'drnumber', name: 'drnumber'},
              {data: 'customer', name: 'customer'},
              {data: 'total', name: 'total'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
      });


    }

    function ReloadDeliverySalesInformation(){

      tblsalesdetails.ajax.reload();

    }

    function ReloadSalesSummary(){

      tblsalessummary.ajax.reload();

    }

    function ReloadSalesPerCustomer(){

      tblsalespercustomer.ajax.reload();

    }

    function SetDataTable(){

      tblsalesdetails = $('#tblsalesdetails').DataTable({
          autoWidth: false,
          ordering: false,
      });

      tblsalessummary = $('#tblsalessummary').DataTable({
          autoWidth: false,
          ordering: false,
      });

      tblsalespercustomer = $('#tblsalespercustomer').DataTable({
          autoWidth: false,
          ordering: false,
      });

    }

    function SetDatePicker(){

      $("#txtdtfrom").datepicker({ 
          dateFormat: 'yy-mm-dd' 
      });

      $("#txtdtto").datepicker({ 
          dateFormat: 'yy-mm-dd' 
      });   

    }

    //Socket functions
    socket.on('reloaddeliverysalesinformation', function(){

      ReloadDeliverySalesInformation();

    });

  </script>  

@endsection