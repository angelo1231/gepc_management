<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use Illuminate\Support\Collection;
use App\RMExcess_Model;

class RMExcessController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }

    function LoadExcessInformation(){

        $excessrawmaterials = RMExcess_Model::LoadExcessInformation();

        $data = array();
        for($i=0;$i<count($excessrawmaterials);$i++){
            
            $obj = new \stdClass;

            $obj->rawmaterial = $excessrawmaterials[$i]->rmexcess;
            $obj->qty = $excessrawmaterials[$i]->qtyexcessboard;
            $obj->panel = '<button id="btnreceive" name="btnreceive" class="btn btn-flat btn-success" value="'. $excessrawmaterials[$i]->id .'"><i class="fa fa-plus" aria-hidden="true"></i></button>';
                           
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)
        ->rawColumns([
            'panel'
        ])
        ->make(true);

    }

    function ReceiveExcess(Request $request){

       $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

       //Get
       $excessqty = RMExcess_Model::GetExcessQty($request->id);
       $stockonhand = RMExcess_Model::GetRMStockOnHand($request->id);

       //Set
       $stockonhand += $excessqty;    

       //Update
       RMExcess_Model::UpdateRMStockOnHand($request->id, $stockonhand);
       RMExcess_Model::SaveRMAudit($request->id, $excessqty, $stockonhand, $issuedby);
       RMExcess_Model::UpdateExcessInformation($request->id);
       
       return json_encode([
            "success"=>true,
            "message"=>"You have receive excess board quantity."
        ]);

    }

}
