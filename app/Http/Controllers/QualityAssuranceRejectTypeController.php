<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use Illuminate\Support\Collection;
use App\Http\Controllers\UserController;
use App\QualityAssuranceRejectType_Model;

class QualityAssuranceRejectTypeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        $access = UserController::UserGroupAccess();

        return view('extensions.qualityassurance.qarejectinfo')
        ->with('access', $access);

    }

    function LoadRejectType(){

        $rejectinfo = QualityAssuranceRejectType_Model::LoadRejectType();

        $data = array();
        for($i=0;$i<count($rejectinfo);$i++){
            
            $obj = new \stdClass;

            $obj->id = $rejectinfo[$i]->id;
            $obj->code = $rejectinfo[$i]->rejectcode;
            $obj->rejecttype = $rejectinfo[$i]->reject;
            $obj->createdby = $rejectinfo[$i]->createdby;
            $obj->createddate = $rejectinfo[$i]->created_at;
            $obj->panel = '<button id="btnurejecttype" name="btnurejecttype" class="btn btn-flat btn-info" value="'. $rejectinfo[$i]->id .'"><i class="fa fa-edit"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveRejectType(Request $request){

        //Validation
        $valrejecttype = QualityAssuranceRejectType_Model::ValRejectType($request->rejecttype);

        if($valrejecttype){

            return json_encode([
                "success"=>false,
                "message"=>"Reject type already exist."
            ]);

        }
        else{

            $uid = Auth::user()->id;

            QualityAssuranceRejectType_Model::SaveRejectType($request->rejecttype, $request->rejectcode, $uid);

            return json_encode([
                "success"=>true,
                "message"=>"Reject type has been save."
            ]);

        }

    }

    function LoadRejectTypeInfo(Request $request){

        $rejecttypeinfo = QualityAssuranceRejectType_Model::LoadRejectTypeInfo($request->id);

        return json_encode([
            "rejectcode"=>$rejecttypeinfo->rejectcode,
            "rejecttype"=>$rejecttypeinfo->reject
        ]);

    }

    function UpdateRejectType(Request $request){

        QualityAssuranceRejectType_Model::UpdateRejectType($request->id, $request->rejecttype, $request->rejectcode);

        return json_encode([
            "success"=>true,
            "message"=>"Reject type has been update."
        ]);

    }

}
