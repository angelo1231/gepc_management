<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Delivery_Model;
use Session;
use DataTables;
use Illuminate\Support\Collection;
use App\Exports\Delivery\DeliveryExport;
use Excel;
use App\Http\Controllers\UserController;

class DeliveryController extends Controller
{

    //Variables
    private $tableschema;

    public function __construct()
    {
        $this->middleware('auth');
        $this->tableschema = env('DB_DATABASE', 'db_accounting_management');
    }
    
    function index($view=null, $action=null, $id=null){

        $access = UserController::UserGroupAccess();        

        if($view==null){

            return view('sales')
            ->with('access', $access);

    
        }
        else{
    
            if($action!=null && $id!=null){
                    
                $pocustomerid = Delivery_Model::GetPOCustomerID($id);
                $poinformation = Delivery_Model::CustomerPOProfile($id);
                $poinformationitems = Delivery_Model::CustomerPOItemsProfile($id);

                return view('extensions.sales.'.$view)
                ->with('poid', $id)
                ->with('cid', $pocustomerid)
                ->with('poinformation', $poinformation)
                ->with('poinformationitems', $poinformationitems)
                ->with('access', $access);

    
            }
            else{

                return view('extensions.sales.'.$view)
                ->with('access', $access);

            }
    
        }

    }

    function LoadCustomer(){

        $customer = Delivery_Model::LoadCustomer();

        return json_encode([
            "data"=>$customer
        ]);

    }

    function LoadCustomerItems(Request $request){

        $finishgoods = Delivery_Model::LoadCustomerItems($request->customer);

        return json_encode([
            "data"=>$finishgoods
        ]);

    }

    function GetFGProfile(Request $request){

        $fgprofile = Delivery_Model::GetFGProfile($request->fgid);

        return json_encode([
            "partnumber"=>$fgprofile[0]->partnumber,
            "partname"=>$fgprofile[0]->partname,
            "description"=>$fgprofile[0]->description,
            "idsize"=>"I.D W: " .$fgprofile[0]->ID_width . " L: " .$fgprofile[0]->ID_length . " H: " .$fgprofile[0]->ID_height,
            "odsize"=>"O.D W: " .$fgprofile[0]->OD_width . " L: " .$fgprofile[0]->OD_length . " H: " .$fgprofile[0]->OD_height,
            "price"=>$fgprofile[0]->price
        ]);

    }

    function GetCurrency(Request $request){

        $currency = Delivery_Model::GetCurrency($request->fgid);
      
        $total = ($currency[0]->rate * $currency[0]->price) * $request->qty;

        return json_encode([
            "total"=>$total
        ]);

    }

    function SavePurchaseOrder(Request $request){

        $validation = Delivery_Model::ValidatePurchaseOrderNumber($request->ponumber);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Purchase order number already exist."
            ]);

        }
        else{

            $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;
            $poid = Delivery_Model::SaveCustomerPOInformation($request->ponumber, $request->cid, $request->grandtotal, $request->remarks, $request->deliverydate, $issuedby, $request->isopenpo);

            for($y=0;$y<count($request->fgid);$y++){

                $data = Delivery_Model::Currency($request->fgid[$y]["value"]);

                Delivery_Model::SaveCustomerPOInformationItems($poid, $request->fgid[$y]["value"], $data[0]->currencyid, $data[0]->rate, $request->qty[$y]["value"], $data[0]->price, $request->total[$y]["value"]);
                
            }

            Session::flash('message', "Purchase information has been save.");
            return json_encode([
                "success"=>true
            ]);

        }

        

    }

    function LoadCustomerPO(Request $request){

        $po = Delivery_Model::LoadCustomerPO($request->customer);
        
        $data = array();
        for($i=0;$i<count($po);$i++){
            
            $obj = new \stdClass;

            if($po[$i]->status=="Close"){
                $obj->status = '<i class="fa fa-check"></i>';   
            }
            else{
                $obj->status = '';   
            }

            $obj->customer = $po[$i]->customer;
            $obj->PO = $po[$i]->ponumber;
            $obj->item = $po[$i]->item;
            $obj->qty = $po[$i]->qty;
            $obj->served = $po[$i]->served;
            $obj->unserved = $po[$i]->unserved;
            $obj->price = $po[$i]->price;
            $obj->pobalamount = $po[$i]->pobalamount;

            $obj->panel = '<a href="'. url("/sales/cuspurchaseorderinfo/info/". $po[$i]->poid) .'"><button id="btninfo" name="btninfo" class="btn btn-flat btn-info"><i class="fa fa-info"></i></button></a>';
            $data[] = $obj;


        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns([
            'panel', 
            'status',
            'item',
            'qty',
            'served',
            'unserved',
            'price',
            'pobalamount'
        ])->make(true);

    }

    function GetPOCustomer(Request $request){

        $customerpo = Delivery_Model::GetPOCustomer($request->id);

        return json_encode([
            "data"=>$customerpo
        ]);

    }

    function GetPOItems(Request $request){

        $poitems = Delivery_Model::GetPOItems($request->poid);

        return json_encode([
            "data"=>$poitems
        ]);

    }

    function LoadItemInformation(Request $request){

        $finishgood = Delivery_Model::LoadItemInformation($request->itemid);

        return json_encode([
            "finishgood"=>$finishgood->finishgood,
            "idsize"=>$finishgood->idsize,
            "odsize"=>$finishgood->odsize,
            "price"=>$finishgood->price,
            "stockonhand"=>$finishgood->stockonhand
        ]);

    }

    function Currency(Request $request){

        $currency = Delivery_Model::LoadCurrency($request->itemid);

        $total = ($currency->rate * $currency->price) * $request->qty;

        return json_encode([
            "total"=>$total
        ]);

    }

    function SaveDeliveryInformation(Request $request){

        $validate = false;
        $itemid = 0;
        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        //Validation
        for($v=0;$v<count($request->dataitem);$v++){

            $validate = Delivery_Model::ValidateStockOnHand($request->dataitem[$v]["value"], $request->dataqty[$v]["value"]);
            if($validate){
                $itemid = $request->dataitem[$v]["value"];
                break;
            }

        }

        if($validate){

            $fgdata = Delivery_Model::GetFGInformation($itemid);
            $message = "Insufficient stocks on " . $fgdata->finishgood . ". Current stockonhand: " . $fgdata->stockonhand;


            return json_encode([
                "success"=>false,
                "message"=>$message
            ]);

        }
        else{

            $did = Delivery_Model::SaveDeliveryInformation($request->drnumber, $request->deldate, $request->grandtotal, $request->cid, $issuedby);

            for($i=0;$i<count($request->datapo);$i++){

                Delivery_Model::SaveDeliveryInformationItems($did, $request->datapo[$i]["value"], $request->dataitem[$i]["value"], $request->dataqty[$i]["value"], $request->dataprice[$i]["value"], $request->datatotal[$i]["value"]);
    
            }
    
            Session::flash('message', "Delivery information has been save.");
            return json_encode([
                "success"=>true,
                "message"=>""
            ]);

        }
        
    }

    function LoadDeliveryInformation(Request $request){

        $delivery = Delivery_Model::LoadDeliveryInformation($request);

        $data = array();
        for($i=0;$i<count($delivery);$i++){
            
            $obj = new \stdClass;

            $obj->drnumber = $delivery[$i]->drnumber;
            $obj->totalqty = $delivery[$i]->totalqty;
            $obj->grandtotal = $delivery[$i]->grandtotal;
            $obj->deldate = $delivery[$i]->deliverydate;
            $obj->issuedby = $delivery[$i]->issuedby;
            $obj->createdat = $delivery[$i]->created_at;
            $obj->customer = $delivery[$i]->customer;
            $obj->panel = '<button id="btndownload" name="btndownload" class="btn btn-flat btn-info" title="Download" value="'. $delivery[$i]->did .'"><i class="fa fa-download"></i></button> <button id="btncreateinvoice" name="btncreateinvoice" class="btn btn-flat btn-info" title="Create Invoice" value="'. $delivery[$i]->did .'"><i class="fa fa-file-o"></i></button>';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);
        

    }

    function ValidateDeliveryNumber(Request $request){

        $validate = Delivery_Model::ValidateDeliveryNumber($request->drnumber);

        return json_encode([
            "success"=>$validate
        ]);

    }

    function DownloadExcel($did){

        $dritems = Delivery_Model::GetDeliveryItemInformation($did);

        $drdata_array = array();

        foreach($dritems as $data){

            $drdata_array[] = array(
                'Delivery Number'=>$data->drnumber,
                'Purchase Order Number'=>$data->ponumber,
                'Partnumber'=>$data->partnumber,
                'Partname'=>$data->partname,
                'Description'=>$data->description,
                'Qty'=>$data->qty,
                'Price'=>$data->price,
                'Total'=>$data->total
            );

        }

        $drnumber = $dritems[0]->drnumber;
        return Excel::download(new DeliveryExport($drdata_array), $drnumber . ".xlsx");

    }

    function CreateInvoice(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        Delivery_Model::CreateInvoice($request, $issuedby);
        Delivery_Model::CloseDeliveryInformation($request);

        return json_encode([
            "success"=>true,
            "message"=>"Invoice information has been save."
        ]);


    }

    function SaveSalesOrderDelivery(Request $request){

        //Validation
        $validation = Delivery_Model::ValidateQuantitySO($request->itemid, $request->deliveryqty);
        
        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"You have exceed on the item delivery quantity."
            ]);

        }
        else{

            $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

            Delivery_Model::SaveSalesOrderDelivery($request->deliveryqty, $request->deliverydate, $request->itemid, $issuedby, $this->tableschema);
    
            return json_encode([
                "success"=>true,
                "message"=>"Sales Order information has been save."
            ]);

        }

    }

    function LoadItemDeliveryInformation(Request $request){

        $itemdelivery = Delivery_Model::LoadItemDeliveryInformation($request->itemid);

        return json_encode([
            "data"=>$itemdelivery
        ]);

    }

    function AddOpenPOItems(Request $request){

        //Validation
        $validation = Delivery_Model::ValidatePOItem($request->poid, $request->fgid);
        
        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Item information already exist.",
            ]);

        }
        else{

            //Add Item To PO
            $data = Delivery_Model::Currency($request->fgid);
            Delivery_Model::SaveCustomerPOInformationItems($request->poid, $request->fgid, $data[0]->currencyid, $data[0]->rate, $request->qty, $request->price, $request->total);

            //Update PO Information
            $grandtotal = Delivery_Model::UpdatePOInformation($request->poid, $request->total);

            return json_encode([
                "success"=>true,
                "message"=>"Customer purchase order information has been updated.",
                "grandtotal"=>$grandtotal
            ]);

        }



    }

    function ReloadPOItems(Request $request){

        $content = "";
        $poinformationitems = Delivery_Model::CustomerPOItemsProfile($request->poid);

        foreach($poinformationitems as $val){

            $content .= '
            <tr><td>'. $val->partnumber .'</td> <td>'. $val->partname .'</td> <td>'. $val->description .'</td> <td>I.D: W: '. $val->ID_width .' L: '. $val->ID_length .' H: '. $val->ID_height .'<br> O.D: W: '. $val->OD_width .' L: '. $val->OD_length .' H: '. $val->OD_height .'</td> <td>'. number_format($val->price, 2) .'</td> <td>'. $val->qty .'</td> <td>'. number_format($val->total, 2) .'</td> <td>'. $val->delqty .'</td> <td><button id="btndelivery" name="btndelivery" value="'. $val->itemid .'" class="btn btn-flat btn-info"><i class="fa fa-truck" aria-hidden="true"></i></button> <button id="btneditqty" name="btneditqty" class="btn btn-flat btn-info" value="'. $val->itemid .'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </button></td> </tr>
            ';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function SavePreparationSlipInformation(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        $preparationid = Delivery_Model::SavePreparationSlipInformation($request->cid, $issuedby, $this->tableschema);

        for($i=0;$i<count($request->datapo);$i++){

            Delivery_Model::SavePreparationSlipInformationItems($preparationid, $request->datapo[$i]["value"], $request->dataitem[$i]["value"], $request->dataqty[$i]["value"]);
    
        }
    
        Session::flash('message', "Preparation slip information has been save.");
        return json_encode([
            "success"=>true,
            "message"=>""
        ]);

    }

    function LoadPreparationSlipInformation(Request $request){

        $preparationslip = Delivery_Model::LoadPreparationSlipInformation($request);

        $data = array();
        for($i=0;$i<count($preparationslip);$i++){
            
            $obj = new \stdClass;

            $obj->psnumber = $preparationslip[$i]->preparationnumber;
            $obj->totalqty = $preparationslip[$i]->totalqty;
            $obj->issuedby = $preparationslip[$i]->issuedby;
            $obj->createdat = $preparationslip[$i]->created_at;
            $obj->customer = $preparationslip[$i]->customer;
            if($preparationslip[$i]->status=="Open"){
                $obj->panel = '';
            }
            else if($preparationslip[$i]->status=="Approved"){
                $obj->panel = '<button id="btncreatedr" name="btncreatedr" class="btn btn-flat btn-info" title="Create Delivery" value="'. $preparationslip[$i]->preparationid .'"><i class="fa fa-file-o"></i></button>';
            }
            else{
                $obj->panel = '<button id="btninfo" name="btninfo" class="btn btn-flat btn-info" title="Information" value="'. $preparationslip[$i]->preparationid .'"><i class="fa fa-info"></i></button>';   
            }
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function CreateDR(Request $request){

        $issuedby = Auth::user()->lastname . ", " . Auth::user()->firstname . " " . Auth::user()->mi;

        //Get Preparation Info
        $preparationinfo = Delivery_Model::GetPreparationInfo($request->preparationid);
        $preparationitems = Delivery_Model::GetPreparationItems($request->preparationid);

        //Save Delivery Information
        $did = Delivery_Model::CreateDRInfo($preparationinfo, $request->deliverynumber, $request->deliverydate, $issuedby);

        //Save Delivery Items
        foreach($preparationitems as $val){

            Delivery_Model::CreateDRItems($did, $val->poid, $val->pocusitemid, $val->fgid, $val->currencyid, $val->rate, $val->qty, $val->price, $val->total);

        }

        //Update Preparation Slip
        Delivery_Model::UpdatePreparationSlip($request->preparationid);

        return json_encode([
            "success"=>true,
            "message"=>"Delivery information has been save."
        ]);

    }

    function LoadDelIDDeliveryInfo(Request $request){

        $deliveyrqty = Delivery_Model::LoadDelIDDeliveryInfo($request->delid);

        return json_encode([
            "deliveryqty"=>$deliveyrqty
        ]);

    }

    function UpdateDelQtyInfo(Request $request){

        //Validation
        $validation = Delivery_Model::ValidateUpdateQuantitySO($request->delid, $request->deliveryqty, $request->itemid);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"You have exceed on the item delivery quantity."
            ]);

        }
        else{

            Delivery_Model::UpdateDelQtyInfo($request->delid, $request->deliveryqty);

            return json_encode([
                "success"=>true,
                "message"=>"Sales Order information has been update."
            ]);

        }

    }

    function UpdateQuantityItem(Request $request){

        Delivery_Model::UpdateQuantityItem($request->itemid, $request->qty);
        
        return json_encode([
            "success"=>true,
            "message"=>"Quantity information has been update."
        ]);

    }


}
