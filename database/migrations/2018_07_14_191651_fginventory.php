<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fginventory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finishgoods', function (Blueprint $table) {
            $table->increments('fgid');
            $table->string('itemcode','255');
            $table->string('partnumber')->nullable();
            $table->string('partname')->nullable();
            $table->longtext('description')->nullable();
            $table->text('ID_length')->nullable();
            $table->text('ID_width')->nullable();
            $table->text('ID_height')->nullable();
            $table->text('OD_length')->nullable();
            $table->text('OD_width')->nullable();
            $table->text('OD_height')->nullable();
            $table->text('packingstd')->nullable();
            $table->integer('currencyid')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->integer('stockonhand')->nullable();
            $table->integer('cid');
            $table->string('processcode', 11);
            $table->string('attachment', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finishgoods');
    }
}
