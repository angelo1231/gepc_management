@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 id="qaheader" class="box-title"><i class="fa fa-file"></i> <b> Final Inspection: {{ $qafinalinfo->finalinspectionnumber }}</b></h3>
          <a href="{{ url('/qualityassurance') }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-12">

                {{-- <h3><strong>Job Order #: {{ $joinformation->jonumber }}</strong></h3>
                <h3><strong>Target Output: {{ $jomonitoringinfo->targetoutput }}</strong></h3>
                <h3><strong>Balance: {{ $jomonitoringinfo->balance }}</strong></h3> --}}
                <br>
                <div class="form-group">
                  <label for="txtjonumber">JO #</label>
                  <input type="text" class="form-control" id="txtjonumber" name="txtjonumber" value="{{ $joinformation->jonumber }}" readonly>
                </div>
                <div class="form-group">
                  <label for="txtpartnumber">Partnumber</label>
                  <input type="text" class="form-control" id="txtpartnumber" name="txtpartnumber" value="{{ $joinformation->partnumber }}" readonly>
                </div>
                <div class="form-group">
                  <label for="txtitemdescription">Item Description</label>
                  <input type="text" class="form-control" id="txtitemdescription" name="txtitemdescription" value="{{ $joinformation->description }}" readonly>
                </div>
                <div class="form-group">
                    <label for="txtqty">Output Quantity</label>
                    <input type="text" class="form-control" id="txtqty" name="txtqty" value="{{ $qafinalinfo->outputqty }}" readonly>
                </div>
                
                <div class="form-group col-md-12" style="border: 2px solid red; border-radius: 5px; border-color: lightgrey;">
                    <h4 style="margin-left: 10px;"><strong>Measuring Tool</strong></h4>
                    <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" id="chkmt" @if($qafinalmeasure->metertape==1) checked @endif disabled>Meter Tape</label>
                    <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" id="chkcal" @if($qafinalmeasure->caliper==1) checked @endif disabled>Caliper</label>
                    <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" id="chkms" @if($qafinalmeasure->meterstick==1) checked @endif disabled>Meter Stick</label> 
                </div>
                
                <div class="form-group col-md-12" style="border: 2px solid red; border-radius: 5px; border-color: lightgrey;">
                    <h4 style="margin-left: 10px;"><strong>Visual Check Legend</strong></h4>
                    @foreach ($rejecttype as $val)
                      <?php $visual = false; ?>
                      @foreach ($qafinalvisual as $item)
                          
                          @if ($val->id==$item->rejecttypeid)
                            <div class="col-md-4 row">
                                <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" name="chkvisual[]" value="{{ $val->id }}" disabled checked>{{ $val->rejectcode }}: {{ $val->reject }}</label>
                            </div>
                            <?php $visual = true; ?>
                            @break
                          @endif

                      @endforeach

                    @if (!$visual)
                      <div class="col-md-4 row">
                          <label class="checkbox-inline" style="margin-left: 10px; margin-bottom: 10px;"><input type="checkbox" name="chkvisual[]" value="{{ $val->id }}" disabled>{{ $val->rejectcode }}: {{ $val->reject }}</label>
                      </div>
                    @endif
                    
                    @endforeach
                </div>
                
                <div class="form-group">
                    <label for="">Others</label>  
                    <input type="text" id="txtothers" name="txtothers" class="form-control" placeholder="Others" value="{{ $qafinalothers->others }}" readonly>
                </div>

            </div>

            <div class="col-md-12">
                <table id="tbldimension" class="table table-bordered" style="width: 100%;">
                <thead>
                    <tr>
                        <th colspan="4" style="text-align: center;">Dimension Check Legend</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 33%; text-align: center; vertical-align: middle;"><strong>Requirements</strong></td>
                        <td style="width: 33%;"><label class="radio-inline"><input type="radio" name="optsize" value="ID" @if($qafinalinfo->insideoutside=="ID") checked @endif><strong>Inside</strong></label></td>
                        <td style="width: 33%;" colspan="2"><label class="radio-inline"><input type="radio" name="optsize" value="OD" @if($qafinalinfo->insideoutside=="OD") checked @endif><strong>Outside</strong></label></td>
                    </tr>
                    <tr>
                        <td style="text-align: center; vertical-align: middle;"><strong>Length</strong></td☻>
                        <td style="text-align: center; vertical-align: middle;"><strong>Width</strong></td>
                        <td style="text-align: center; vertical-align: middle;" colspan="2"><strong>Height</strong></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" id="txtlength" style="text-align: center;" readonly></td>
                        <td><input type="text" class="form-control" id="txtwidth" style="text-align: center;" readonly></td>
                        <td colspan="2"><input type="text" class="form-control" id="txtheight" style="text-align: center;" readonly></td>
                    </tr>
                    <?php  $counter = 1; ?>
                    @foreach ($qafinaldimension as $val)
                    <tr>
                        <td><input type="text" class="form-control" placeholder="Length" style="text-align: center;" id="txtlength{{ $counter }}" name="txtlength[]" value="{{ $val->length }}" readonly></td>
                        <td><input type="text" class="form-control" placeholder="Width"  style="text-align: center;" id="txtwidth{{ $counter }}"  name="txtwidth[]"  value="{{ $val->width }}" readonly></td>
                        <td>
                            <input type="text" class="form-control" placeholder="Height" style="text-align: center;" id="txtheight{{ $counter }}" name="txtheight[]" value="{{ $val->height }}" readonly>
                        </td>
                        <td id="tdcheck{{ $counter }}" style="width: 5%; text-align: center; vertical-align: middle;">
                            @if ($val->remarks!=0)
                              <i class="fa fa-check" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                            @else
                              <i class="fa fa-times" aria-hidden="true" style="text-align: center; vertical-align: middle;"></i>
                            @endif
                        </td>
                    </tr>
                    <?php $counter += 1; ?>
                    @endforeach
                    <tr>
                        <td><strong>Flute Type: </strong> 
                            <input type="text" id="txtflute" class="form-control" value="{{ $qafinalinfo->flute }}" readonly>
                        </td>
                        <td><strong>Thickness</strong> <input type="text" class="form-control" id="txtthickness" placeholder="Thickness" value="{{ $qafinalinfo->thickness }}" readonly></td>
                        <td colspan="2"><strong>Tolerance</strong> <input type="text" class="form-control" id="txttolerance" value="{{ $qafinalinfo->tolerance }}" readonly></td>
                    </tr>
                </tbody>
                </table>

                  <button id="btnconfirm" name="btnconfirm" class="btn btn-flat btn-success" @if($qafinalinfo->confirmby==0) style="float: right;" @else style="float: right; display: none;" @endif><i class="fa fa-check" aria-hidden="true"></i> Confirm</button>
                  <button id="btnprint" name="btnprint" class="btn btn-flat btn-primary" @if($qafinalinfo->confirmby!=0) style="float: right;" @else style="float: right; display: none" @endif ><i class="fa fa-print" aria-hidden="true" ></i> Print</button>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12">
              {{-- <label>Target Output:</label> --}}
          </div>          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  {{-- @include('modal.qualityassurance.newcocqty')
  @include('modal.qualityassurance.cocitemdimension') --}}

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

<script>

  //Variables
  var qafiid = '{{ $qafiid }}';

  $(document).ready(function(){

    //Set
    SetSize();

  });

  $('#btnconfirm').on('click', function(){

    @if(in_array(72, $access))

      $.confirm({
          title: 'Confirm',
          content: 'Confirm this final inspection information?',
          type: 'blue',
          buttons: {   
              ok: {
                  text: "Yes",
                  btnClass: 'btn-info',
                  keys: ['enter'],
                  action: function(){

                    Confirm();

                  }
              },
              cancel: {
                  text: "No",
                  btnClass: 'btn-info',
                  action: function(){
                      
                    

                  }
              } 
          }
      });

    @else

      toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

    @endif

  });

  $('#btnprint').on('click', function(){

    // window.open('', _blank)
    window.open('{{ url("/qualityassurance/qafinalinspectionprint/printfi") }}/'+ qafiid +'', "", "width=1000,height=900");

  });

  function Confirm(){

    $.ajax({
      url: '{{ url("api/qualityassurance/confirmfinalinspection") }}',
      type: 'post',
      data: {
        qafiid: qafiid
      },
      dataType: 'json',
      success: function(response){

        if(response.success){

          $('#btnconfirm').hide();
          $('#btnprint').show();
          toastr.success(response.message, '', { positionClass: 'toast-top-center' });

        }

      }
    });

  }

  function SetSize(){

    var value = $("input[name='optsize']:checked").val();

    if(value=="OD"){

        $('#txtlength').val('{{ $OD_length }}');
        $('#txtwidth').val('{{ $OD_width }}');
        $('#txtheight').val('{{ $OD_height }}');

    }
    else{

        $('#txtlength').val('{{ $ID_length }}');
        $('#txtwidth').val('{{ $ID_width }}');
        $('#txtheight').val('{{ $ID_height }}');

    }

  }

</script>  

@endsection