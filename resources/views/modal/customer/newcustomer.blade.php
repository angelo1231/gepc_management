<!-- Middle Modal -->
<style>
        .modal {
          text-align: center;
          padding: 0!important;
        }
        
        .modal:before {
          content: '';
          display: inline-block;
          height: 100%;
          vertical-align: middle;
          margin-right: -4px;
        }
        
        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
        }
        </style>   
        <!-- Modal -->
        <div class="modal fade" id="modalnewcustomer" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <strong>NEW CUSTOMER</strong>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                    
                            <div class="form-group">
                                    <label for="txtncustomer">Supplier</label>
                                    <input id="txtncustomer" name="txtncustomer" type="text" class="form-control" placeholder="Customer">
                            </div>
                            <div class="form-group">
                                <label for="txtncontactperson">Contact Person</label>
                                <input id="txtncontactperson" name="txtncontactperson" type="text" class="form-control" placeholder="Contact Person">
                            </div>
                            <div class="form-group">
                                    <label for="txtncontactnumber">Contact Number</label>
                                    <input id="txtncontactnumber" name="txtncontactnumber" type="text" class="form-control" placeholder="Contact Number">
                            </div>
                            <div class="form-group">
                                    <label for="txtnemail">Email</label>
                                    <input id="txtnemail" name="txtnemail" type="text" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                    <label for="txtnaddress">Address</label>
                                    <input id="txtnaddress" name="txtnaddress" type="text" class="form-control" placeholder="Address">
                            </div>
                            <div class="form-group">
                                    <label for="txtntin">TIN (Optional)</label>
                                    <input id="txtntin" name="txtntin" type="text" class="form-control" placeholder="TIN">
                            </div>
                            <div class="form-group">
                                    <label for="txtnterms">Terms (Optional)</label>
                                    <input id="txtnterms" name="txtnterms" type="text" class="form-control" placeholder="Terms">
                            </div>
                            <div class="form-group">
                                <label for="cmbntax">Tax Classification</label>
                                <select name="cmbntax" id="cmbntax" class="form-control">
                                  <option value="Vat Exempt">Vat Exempt</option>
                                  <option value="Vatable Sales">Vatable Sales</option>
                                  <option value="Zero Rated">Zero Rated</option>
                                </select>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-9">
        
                        </div>
                        <div class="col-md-3">
                            <button id="btnsave" name="btnsave" type="button" class="btn btn-primary btn-block btn-flat">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>