<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Purchaserequestitemsdelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaserequestitemsdelivery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('itemid');
            $table->datetime('deliverydate');
            $table->integer('deliveryqty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaserequestitemsdelivery');                        
    }
}
