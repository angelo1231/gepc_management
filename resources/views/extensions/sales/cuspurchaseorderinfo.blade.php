@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 id="lblheader" class="box-title"><i class="fa fa-th-list"></i><b> PO# {{ $poinformation[0]->ponumber }}</b></h3>
          <a href="{{ url('/sales/cuspurchaseorder') }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

             <div class="col-md-3">

                <div class="form-group">
                    <label for="txtcustomer">Customer</label>
                    <input id="txtcustomer" name="txtcustomer" class="form-control" type="text" placeholder="Customer" value="{{ $poinformation[0]->customer }}" readonly>
                </div>

                {{-- <div class="form-group">
                    <label for="txtdeldate">Delivery Date</label>
                    <input id="txtdeldate" name="txtdeldate" type="text" class="form-control" placeholder="Delivery Date" value="{{ $poinformation[0]->deliverydate }}" readonly>
                </div> --}}

            </div>
            <div class="col-md-9">

                <div class="form-group">
                    <label for="txtremarks">Remarks</label>
                    <textarea id="txtremarks" name="txtremarks" class="form-control" rows="4" style="width: 400px;" readonly>{{ $poinformation[0]->remarks }}</textarea>
                </div>

            </div>

            <div class="col-md-12">

                    <table id="tblitems" class="table table-hover" style="margin-top: 10px;">
                      <thead>
                          <tr>
                            <th>Partnumber</th>
                            <th>Partname</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Total</th>
                            <th>Delivery Qty</th>
                            @if($poinformation[0]->is_openpo==0)
                            <th></th>
                            @else
                            <th><button id="btnadditem" name="btnadditem" class="btn btn-flat btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
                            @endif
                        </tr>
                      </thead>
                      <tbody id="tblitemscontent">
                        @foreach ($poinformationitems as $val)
                            <tr>
                                <td>{{ $val->partnumber }}</td>
                                <td>{{ $val->partname }}</td>
                                <td>{{ $val->description }}</td>
                                <td>
                                    I.D: W:  {{ $val->ID_width }} L: {{ $val->ID_length }} H: {{ $val->ID_height }}
                                    <br>
                                    O.D: W:  {{ $val->OD_width }} L: {{ $val->OD_length }} H: {{ $val->OD_height }}
                                </td>
                                <td>{{ number_format($val->price, 2) }}</td>
                                <td>{{ $val->qty }}</td>
                                <td>{{ number_format($val->total, 2) }}</td>
                                <td>{{ $val->delqty }}</td>
                                <td><button id="btndelivery" name="btndelivery" value="{{ $val->itemid }}" class="btn btn-flat btn-info"><i class="fa fa-truck" aria-hidden="true"></i></button> <button id="btneditqty" name="btneditqty" class="btn btn-flat btn-info" value="{{ $val->itemid }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </button></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>

                    <label id="lblgrandtotal" style="float: right; margin-right: 72px;">
                        Grand Total: {{ number_format($poinformation[0]->grandtotal, 2) }}
                    </label>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

    @include('modal.sales.cusitemdelivery')
    @include('modal.sales.cusadditem')
    @include('modal.sales.cuseditqtyitem')
    

  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var spoid = '{{ $poid }}';
    var scid = '{{ $cid }}';
    var sitemid;
    var sfgid;
    var sdelid;
    var authinfo = [];

    $(document).ready(function(){

      //Set
      $("#txtadeldate").datepicker({ 
          dateFormat: 'yy-mm-dd' 
      });

    });

    $(document).on('click', '#btndelivery', function(){

      sitemid = $(this).val();
      SetDefault();
      LoadItemDeliveryInformation(sitemid);
      $('#cusitemdelivery').modal('toggle');

    });

    $('#btnadddeliverysales').on('click', function(){

      ClearNewSalesDelivery();
      
      $('#divtblitemdelivery').hide();
      $('#divadddeliverysales').fadeIn();

      $('#btnclosedelivery').hide();
      
    });

    $('#btnclose').on('click', function(){

      $('#divadddeliverysales').hide();
      $('#divtblitemdelivery').fadeIn();

      $('#btnclosedelivery').show();

    });

    $('#btnasave').on('click', function(){

      var deliveryqty = $('#txtadelqty').val();
      var deliverydate = $('#txtadeldate').val();

      $.confirm({
          title: 'Save',
          content: 'Save this delivery information of this item?',
          type: 'blue',
          buttons: {   
              ok: {
                  text: "Yes",
                  btnClass: 'btn-info',
                  keys: ['enter'],
                  action: function(){

                    SaveSalesOrderDelivery(deliveryqty, deliverydate, sitemid);

                  }
              },
              cancel: {
                  text: "No",
                  btnClass: 'btn-info',
                  action: function(){
                      
                    

                  }
              } 
          }
      });

    });

    $('#btnadditem').on('click', function(){

      ClearNewPOItem();
      LoadCustomerItems(scid);
      $('#cusadditem').modal('toggle');

    });

    $('#cmbitem').on('change', function(){

      sfgid = $(this).val();

      LoadFGProfile(sfgid);
      $('#txtquantity').attr('disabled', false);

    });

    $('#txtquantity').on('keyup', function(){

      var qty = $(this).val();

      GetCurrency(sfgid, qty);

    });

    $('#btnaddcusitem').on('click', function(){

      var price = $('#txtprice').val();
      var qty = $('#txtquantity').val();
      var total = $('#txttotal').val();

      $.confirm({
          title: 'Add',
          content: 'Add this item information in this purchase order?',
          type: 'blue',
          buttons: {   
              ok: {
                  text: "Yes",
                  btnClass: 'btn-info',
                  keys: ['enter'],
                  action: function(){

                    AddPOItem(spoid, sfgid, price, qty, total);

                  }
              },
              cancel: {
                  text: "No",
                  btnClass: 'btn-info',
                  action: function(){
                      
                    

                  }
              } 
          }
      });

    });

    $(document).on('click', '#btneditdelinfo', function(){

      sdelid = $(this).val();
      
      LoadDelIDDeliveryInfo(sdelid);

      $('#divtblitemdelivery').hide();
      $('#diveditdeliverysales').fadeIn();

      $('#btnclosedelivery').hide();

    });

    $('#btneclose').on('click', function(){

      $('#diveditdeliverysales').hide();
      $('#divtblitemdelivery').fadeIn();

      $('#btnclosedelivery').show();

    });

    $('#btnesave').on('click', function(){

      var deliveryqty =  $('#txtedelqty').val();

      $.confirm({
          title: 'Update',
          content: 'Update this item information?',
          type: 'blue',
          buttons: {   
              ok: {
                  text: "Yes",
                  btnClass: 'btn-info',
                  keys: ['enter'],
                  action: function(){

                    UpdateDelQtyInfo(sdelid, deliveryqty, sitemid);

                  }
              },
              cancel: {
                  text: "No",
                  btnClass: 'btn-info',
                  action: function(){
                      
                    

                  }
              } 
          }
      });

    });

    $(document).on('click', '#btneditqty', function(){

      sitemid = $(this).val();

      ClearAuth();
      $('#auth').modal('toggle');

    });

    $(document).on('click', '#btnauthlogin', function(){

        var validation = AuthLogin();

        validation.done(function(data){

            if(data.success){

                authinfo = {
                    id: data.id,
                    name: data.name
                };

                $('#auth').modal('toggle');
                setTimeout(function(){ 

                  $('#cuseditqtyitem').modal('toggle');

                }, 500);

            }

        });

    });

    $('#btnsaveeditqty').on('click', function(){

      $.confirm({
          title: 'Update',
          content: 'Update this quantity information of this item?',
          type: 'blue',
          buttons: {   
              ok: {
                  text: "Yes",
                  btnClass: 'btn-info',
                  keys: ['enter'],
                  action: function(){

                    UpdateQuantityItem();

                  }
              },
              cancel: {
                  text: "No",
                  btnClass: 'btn-info',
                  action: function(){
                      
                    

                  }
              } 
          }
      });

    });

    function UpdateQuantityItem(){

      var qty = $('#txtnewqty').val();

      $.ajax({
        url: '{{ url("api/sales/cuspurchaseorderinfo/updatequantityitem") }}',
        type: 'post',
        data: {
          itemid: sitemid,
          qty: qty,
          authinfo: authinfo
        },
        dataType: 'json',
        success: function(response){

          if(response.success){

            $('#cuseditqtyitem').modal('toggle');
            ReloadPOItems(spoid);

          }

        }
      });

    }

    function UpdateDelQtyInfo(delid, deliveryqty, itemid){

      $.ajax({
        url: "{{ url('api/sales/cuspurchaseorderinfo/updatedelqtyinfo') }}",
        type: 'post',
        data: {
          delid: delid,
          deliveryqty: deliveryqty,
          itemid: itemid
        },
        dataType: 'json',
        success: function(response){

          if(response.success){

            toastr.success(response.message, '', { positionClass: 'toast-top-center' });
            SetDefault("Update Item");            
            LoadItemDeliveryInformation(itemid);

          }
          else{

            toastr.error(response.message, '', { positionClass: 'toast-top-center' });

          }

        }
      });

    }

    function LoadDelIDDeliveryInfo(delid){

      $.ajax({
        url: "{{ url('api/sales/cuspurchaseorderinfo/loaddeliddeliveryinfo') }}",
        type: 'get',
        data: {
          delid: delid
        },
        dataType: 'json',
        success: function(response){

          $('#txtedelqty').val(response.deliveryqty);

        }
      });

    }

    function AddPOItem(poid, fgid, price, qty, total){

      //Validation
      if(qty<=0 || qty==""){
        toastr.error("Please input a valid quantity.", '', { positionClass: 'toast-top-center' });
      }
      else{

        $.ajax({
          url: '{{ url("api/sales/cuspurchaseorderinfo/addopenpoitems") }}',
          type: 'post',
          data: {
            poid: poid,
            fgid: fgid,
            price: price,
            qty: qty,
            total: total
          },
          dataType: 'json',
          success: function(response){

            if(response.success){

              $('#cusadditem .close').click();
              toastr.success(response.message, '', { positionClass: 'toast-top-center' });
              $('#lblgrandtotal').text('');
              $('#lblgrandtotal').text('Grand Total: '+ response.grandtotal);
              ReloadPOItems(poid);

            }
            else{

              toastr.error(response.message, '', { positionClass: 'toast-top-center' });

            }

          }
        });

      }

    }

    function GetCurrency(fgid, qty){

      $.ajax({
            url: '{{ url("api/sales/cuspurchaseorderinfo/getcurrency") }}',
            type: 'get',
            data: {
                fgid: fgid,
                qty: qty
            },
            dataType: 'json',
            success: function(response){

                $('#txttotal').val(response.total);

            }
        });

    }

    function LoadFGProfile(fgid){

      $.ajax({
        url: '{{ url("api/sales/cuspurchaseorderinfo/getfgprofile") }}',
        type: 'get',
        data: {
          fgid: fgid
        },
        dataType: 'json',
        success: function(response){

          $('#txtpartnumber').val(response.partnumber);
          $('#txtpartname').val(response.partname);
          $('#txtdescription').val(response.description);
          $('#txtsize').val(response.idsize + ' ' + response.odsize);
          $('#txtprice').val(response.price);

        }
      });


    }

    function SaveSalesOrderDelivery(deliveryqty, deliverydate, itemid){

      $.ajax({
        url: '{{ url("api/sales/cuspurchaseorderinfo/savesalesorderdelivery") }}',
        type: 'post',
        data: {
          deliveryqty: deliveryqty,
          deliverydate: deliverydate,
          itemid: itemid
        },
        dataType: 'json',
        success: function(response){

          if(response.success){

            toastr.success(response.message, '', { positionClass: 'toast-top-center' });
            SetDefault("Add Item");            
            LoadItemDeliveryInformation(itemid);

          }
          else{

            toastr.error(response.message, '', { positionClass: 'toast-top-center' });

          } 

        }
      });

    }

    function SetDefault(action){

      if(action=="Add Item"){

        $('#divadddeliverysales').hide();
        $('#divtblitemdelivery').show();

        $('#btnclosedelivery').show();

      }
      else if(action=="Update Item"){

        $('#diveditdeliverysales').hide();
        $('#divtblitemdelivery').fadeIn();

        $('#btnclosedelivery').show();

      }

    }

    function ClearNewSalesDelivery(){

      $('#txtadelqty').val('');
      $('#txtadeldate').val('');

    }

    function LoadCustomerItems(customer){

      $.ajax({
        url: '{{ url("api/sales/cuspurchaseorderinfo/loadcustomeritems") }}',
        type: 'get',
        data: {
          customer: customer
        },
        dataType: 'json',
        success: function(response){

          $('#cmbitem').find('option').remove();
          $('#cmbitem').append('<option value="" disabled selected>Select a item</option>');
          for (var i = 0; i < response.data.length; i++) {
              $('#cmbitem').append('<option value="'+  response.data[i]["fgid"] +'">'+  response.data[i]["item"] +'</option>');
          }

        },
        complete: function(){

          $('#cmbitem').select2({
              theme: 'bootstrap',
              dropdownParent: $('#cusadditem')
          })

        }
      });

    }

    function LoadItemDeliveryInformation(itemid){

      $.ajax({
        url: '{{ url("api/sales/cuspurchaseorderinfo/loaditemdeliveryinformation") }}',
        type: 'get',
        data: {
          itemid: itemid
        },
        dataType: 'json',
        success: function(response){

          $('#itemdeliverycontent').find('tr').remove();
          for(var i=0;i<response.data.length;i++){

            if(response.data[i]["status"]!="Close"){

              $('#itemdeliverycontent').append('<tr><td></td> <td>'+ response.data[i]["salesnumber"] +'</td> <td>'+ response.data[i]["deliveryqty"] +'</td> <td>'+ response.data[i]["deliverydate"] +'</td> <td>'+ response.data[i]["created_at"] +'</td> <td><button id="btneditdelinfo" name="btneditdelinfo" class="btn btn-flat btn-primary" title="Edit Info" value="'+ response.data[i]["delid"] +'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td></tr>');

            }
            else{

              $('#itemdeliverycontent').append('<tr><td></td> <td>'+ response.data[i]["salesnumber"] +'</td> <td>'+ response.data[i]["deliveryqty"] +'</td> <td>'+ response.data[i]["deliverydate"] +'</td> <td>'+ response.data[i]["created_at"] +'</td> <td></td></tr>');

            }

          }

        }
      });

    }

    function ClearNewPOItem(){

      $('#txtpartnumber').val('');
      $('#txtpartname').val('');
      $('#txtdescription').val('');
      $('#txtsize').val('');
      $('#txtprice').val('');
      $('#txtquantity').val('');
      $('#txttotal').val('');
      $('#txtquantity').attr('disabled', true);

    }

    function ReloadPOItems(poid){

      $.ajax({
        url: '{{ url("api/sales/cuspurchaseorderinfo/reloadpoitems") }}',
        type: 'get',
        data: {
          poid: poid
        },
        dataType: 'json',
        success: function(response){

          $('#tblitemscontent').find('tr').remove();
          $('#tblitemscontent').append(response.content);

        }
      });

    }

  </script>  

@endsection