<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modaluploadattachment" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>Upload Attachment</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                    <label for="uattachment">Drawing Attachment</label>
                    <input type="file" name="uattachment" id="uattachment">
                    <img src="{{ asset('images/noimgattachment.png') }}" id="imguploadattachment" style="margin: 10px 14%;" width="400px">

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnuupload" name="btnuupload" type="button" class="btn btn-primary btn-block btn-flat">Upload</button>
                </div>
            </div>
        </div>
    </div>
</div>