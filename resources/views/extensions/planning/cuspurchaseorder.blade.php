@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Sales Order Information</b></h3>
          <a href="{{ url("/planning") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#soforrequest">For Request</a></li>
                {{-- <li><a data-toggle="tab" href="#soinfo">Sales Order</a></li> --}}
                @if(in_array(36, $access))
                    <li><a data-toggle="tab" href="#prinfo">Purchase Request</a></li>
                @endif
            </ul>

            <div class="tab-content">
                <div id="soforrequest" class="tab-pane fade in active">

                    <br>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="cmbcustomer">Customer</label>
                            <select id="cmbcustomer" name="cmbcustomer" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">

                        <table id="tblcustomerpo" class="table">
                            <thead>
                                <tr>
                                    <th>Purchase Order Number</th>
                                    <th>Item Description</th>
                                    <th>Quantity</th>
                                    <th>Sales Order Number</th>
                                    <th>Delivery Quantity</th>
                                    <th>Delivery Date</th>
                                    <th>Customer</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>

                    </div>

                </div>
                {{-- <div id="soinfo" class="tab-pane fade">



                </div> --}}
                @if(in_array(36, $access))

                    <div id="prinfo" class="tab-pane fade">

                        {{-- <div class="col-md-12">
                            <br>
                            <button id="btnnewprs" name="btnnewprs" class="btn btn-success btn-flat" style="float: right;"><i class="fa fa-plus"></i> New Purchase Request</button>
                            <br>
                            <br>
                            <br>
                        </div> --}}

                        <div class="col-md-12">
                            <br>
                            <br>
                            <table id="tblpurchaserequest" class="table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>PR #</th>
                                    <th>Item Description</th>
                                    <th>Required Quantity</th>
                                    <th>Reason</th>
                                    <th>Issued By</th>
                                    <th>Issued Date</th>
                                    <th>Issued Time</th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                    </div>

                @endif
            </div>

            {{-- Modal --}}
            @include('modal.planning.purchaserequestitem')
            @include('modal.planning.delso')
            @include('modal.planning.newprs')
            @include('modal.planning.delpritems')

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblcustomerpo;
    var tblpurchaserequest;
    var scustomer;
    var sitemid;
    var authinfo = [];
    var prsoitemcount = 0;
    var prsoitemsavecount = 0;
    var cprid;
    var sdelitemid;
    var praction = "";


    $(document).ready(function(){

        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
          toastr.success(msg, '', { positionClass: 'toast-top-center' });
        }

        //Set
        tblcustomerpo = $('#tblcustomerpo').DataTable({
            autoWidth: false,
            ordering: false,
        });
        $("#txtdelpritemdeldate").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });
        
        //Load
        LoadCustomer($('#cmbcustomer').attr('id'));
        LoadPurchaseRequestInformation();
       
    });

    $('#cmbcustomer').on('change', function(){

        scustomer = $(this).val();
        LoadCustomerPO();

    });

    $(document).on('click', '#btnprrequest', function(){

        sitemid = $(this).val();

        @if(in_array(37, $access))

            $.confirm({
                title: 'Create',
                content: 'Create a purchase request?',
                type: 'blue',
                buttons: {   
                    ok: {
                        text: "Yes",
                        btnClass: 'btn-success',
                        keys: ['enter'],
                        action: function(){

                            praction = "Individual PRSSO";

                            //Reset
                            prsoitemcount = 0;
                            $('#tblnewprsocontent').find('tr').remove();
                            $('#tblnewprsocontent').append('<tr><td colspan="4" style="text-align: center;">No Item</td></tr>');
                            $('#tblnewprsosave').find('tr').remove();
                            //Create
                            CreatePRInformation();
                            //Load
                            LoadItemSONumber(sitemid);

                            //Toggle
                            $('#purchaserequestitem').modal('toggle');

                        }
                    },
                    cancel: {
                        text: "No",
                        btnClass: 'btn-info',
                        action: function(){
                            
                            

                        }
                    } 
                }
            });

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

    });

    $(document).on('change', '[name="chkdelid[]"]', function(){

        var sodata = $('[name="chkdelid[]"]').serializeArray();

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/loadquantityreqso") }}',
            type: 'get',
            data: {
                sodata: sodata,
            },
            dataType: 'json',
            success: function(response){

                var reqqty = 0;

                for(var i=0;i<response.sodelqty.length;i++){

                    reqqty += response.sodelqty[i]["deliveryqty"];

                }

                $('#txtnqtyrequired').val(reqqty);

            }
        });

    });

    $('#btnnewprsoitem').on('click', function(){

        if(prsoitemcount==0){

            prsoitemcount += 1;

            if(prsoitemcount!=0 && prsoitemsavecount==0){
                $('#tblnewprsocontent').find('tr').remove();                
            }
            $('#tblnewprsocontent').append('<tr id="prso'+ prsoitemcount +'"> <td><select name="cmbnrawmaterial[]" id="cmbnrawmaterial'+ prsoitemcount +'" class="form-control" style="width: 250px;"></select></td> <td><input id="txtnqtyrequired'+ prsoitemcount +'" name="txtnqtyrequired[]" class="form-control" placeholder="Quantity Required" type="number"></td> <td></td> <td style="text-align: center;"><button id="btnremoveprso" name="btnremoveprso" class="btn btn-flat btn-danger" value="'+ prsoitemcount +'"><i class="fa fa-trash"></i></button></td> </tr>');
            $('#tblnewprsosave').append('<tr> <td colspan="3"></td> <td style="text-align: center;"><button id="btnsaveprsosave" name="btnsaveprsosave" class="btn btn-info btn-flat">Save</button></td> </tr>');

            LoadRawMaterials('cmbnrawmaterial'+prsoitemcount);

        }
        else{

            prsoitemcount += 1;
            $('#tblnewprsocontent').append('<tr id="prso'+ prsoitemcount +'"> <td><select name="cmbnrawmaterial[]" id="cmbnrawmaterial'+ prsoitemcount +'" class="form-control" style="width: 250px;"></select></td> <td><input id="txtnqtyrequired'+ prsoitemcount +'" name="txtnqtyrequired[]" class="form-control" placeholder="Quantity Required" type="number"></td> <td></td> <td style="text-align: center;"><button id="btnremoveprso" name="btnremoveprso" class="btn btn-flat btn-danger" value="'+ prsoitemcount +'"><i class="fa fa-trash"></i></button></td> </tr>');

            LoadRawMaterials('cmbnrawmaterial'+prsoitemcount);            

        }        

    });

    $(document).on('click', '#btnremoveprso', function(){

        var id = $(this).val();

        $('#prso'+id).remove();
        prsoitemcount -= 1;

        if(prsoitemcount==0){

            if(prsoitemsavecount==0){
                if(praction=="Individual PRSSO"){
                    $('#tblnewprsocontent').append('<tr><td colspan="4" style="text-align: center;">No Item</td></tr>');
                }
                else{
                    $('#tblnewprcontent').append('<tr><td colspan="4" style="text-align: center;">No Item</td></tr>');
                }
            }

            if(praction=="Individual PRSSO"){
                $('#tblnewprsosave').find('tr').remove();
            }
            else{
                $('#tblnewprsave').find('tr').remove();
            }

        }

    });

    $('#btnsavepr').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this purchase request information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-success',
                      keys: ['enter'],
                      action: function(){

                        SavePurchaseRequest();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
        });

    });

    $(document).on('click', '#btnremove', function(){

        var id = $(this).val();
        $.confirm({
                title: 'Delete',
                content: 'Delete this purchase request information?',
                type: 'blue',
                buttons: {   
                    ok: {
                        text: "Yes",
                        btnClass: 'btn-danger',
                        keys: ['enter'],
                        action: function(){

                            RemovePRSInformation(id);

                        }
                    },
                    cancel: {
                        text: "No",
                        btnClass: 'btn-info',
                        action: function(){
                            
                        

                        }
                    } 
                }
        });

    });

    $(document).on('click', '#btndelso', function(){

        sitemid = $(this).val();

        ClearAuth();
        $('#auth').modal('toggle');

    });

    $(document).on('click', '#btnauthlogin', function(){

        var validation = AuthLogin();

        validation.done(function(data){

            if(data.success){

                authinfo = {
                    id: data.id,
                    name: data.name
                };

                $('#auth').modal('toggle');
                setTimeout(function(){ 

                    LoadSOItemInformation();
                    $('#delso').modal('toggle');

                }, 500);

            }

        });

    });

    $('#btndeleteso').on('click', function(){

        $.confirm({
              title: 'Delete',
              content: 'Delete this sales order information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-danger',
                      keys: ['enter'],
                      action: function(){

                        DeleteSOInformation();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
        });

    });

    $('#btnnewprs').on('click', function(){

        @if(in_array(37, $access))

            $.confirm({
                title: 'Create',
                content: 'Create a purchase request?',
                type: 'blue',
                buttons: {   
                    ok: {
                        text: "Yes",
                        btnClass: 'btn-success',
                        keys: ['enter'],
                        action: function(){

                            praction = "Individual PRS";

                            //Reset
                            prsoitemcount = 0;
                            $('#tblnewprcontent').find('tr').remove();
                            $('#tblnewprcontent').append('<tr><td colspan="4" style="text-align: center;">No Item</td></tr>');
                            $('#tblnewprsave').find('tr').remove();

                            //Create
                            CreateIPRInformation();

                            //Toggle
                            $('#newprs').modal('toggle');

                        }
                    },
                    cancel: {
                        text: "No",
                        btnClass: 'btn-info',
                        action: function(){
                            
                            

                        }
                    } 
                }
            });

        @else
            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });
        @endif

    });

    $('#btnisavepr').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this purchase request information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-success',
                      keys: ['enter'],
                      action: function(){

                        SavePurchaseRequestIndividual();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
        });

    });

    $('#btncancelprsoitemx').on('click', function(){

        $.confirm({
            title: 'Cancel',
            content: 'Cancel this purchase request?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-success',
                    keys: ['enter'],
                    action: function(){

                        CancelPRSInformation();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                        

                    }
                } 
            }
        });

    });

    $('#btncancelprsoitem').on('click', function(){

        $.confirm({
            title: 'Cancel',
            content: 'Cancel this purchase request?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-success',
                    keys: ['enter'],
                    action: function(){

                        CancelPRSInformation();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                        

                    }
                } 
            }
        });        

    });
    
    $(document).on('click', '#btnsaveprsosave', function(){

        SavePRItems();

    });

    $(document).on('click', '[name="btnremoveprsoitem"]', function(){

        var itemid = $(this).val();
        DeletePRItem(itemid);

    });

    $(document).on('click', '[name="btndelprsoitem"]', function(){

        sdelitemid = $(this).val();

        ClearDelPRItemsInfo();
        $('#delpritems').modal('toggle');

    });

    $('#btnsavedelpritem').on('click', function(){

        SaveDelPRItemsInfo();

    });

    $(document).on('click', '[name="btndeldeliveryinfo"]', function(){

        var id = $(this).val();
        var itemid = $(this).data("itemid");

        DeleteDelDeliveryInfo(id, itemid);

    });

    $('#btnicancelpr').on('click', function(){

        $.confirm({
            title: 'Cancel',
            content: 'Cancel this purchase request?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-success',
                    keys: ['enter'],
                    action: function(){

                        CancelIPRSInformation();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                        

                    }
                } 
            }
        });    

    });

    $('#btnicancelprx').on('click', function(){

        $.confirm({
            title: 'Cancel',
            content: 'Cancel this purchase request?',
            type: 'blue',
            buttons: {   
                ok: {
                    text: "Yes",
                    btnClass: 'btn-success',
                    keys: ['enter'],
                    action: function(){

                        CancelIPRSInformation();

                    }
                },
                cancel: {
                    text: "No",
                    btnClass: 'btn-info',
                    action: function(){
                        
                        

                    }
                } 
            }
        });   

    });

    $('#btinewprsoitem').on('click', function(){

        if(prsoitemcount==0){

            prsoitemcount += 1;

            if(prsoitemcount!=0 && prsoitemsavecount==0){
                $('#tblnewprcontent').find('tr').remove();                
            }
            $('#tblnewprcontent').append('<tr id="prso'+ prsoitemcount +'"> <td><select name="cmbnrawmaterial[]" id="cmbnrawmaterial'+ prsoitemcount +'" class="form-control" style="width: 250px;"></select></td> <td><input id="txtnqtyrequired'+ prsoitemcount +'" name="txtnqtyrequired[]" class="form-control" placeholder="Quantity Required" type="number"></td> <td></td> <td style="text-align: center;"><button id="btnremoveprso" name="btnremoveprso" class="btn btn-flat btn-danger" value="'+ prsoitemcount +'"><i class="fa fa-trash"></i></button></td> </tr>');
            $('#tblnewprsave').append('<tr> <td colspan="3"></td> <td style="text-align: center;"><button id="btnsaveprsosave" name="btnsaveprsosave" class="btn btn-info btn-flat">Save</button></td> </tr>');

            LoadRawMaterials('cmbnrawmaterial'+prsoitemcount);

        }
        else{

            prsoitemcount += 1;
            $('#tblnewprcontent').append('<tr id="prso'+ prsoitemcount +'"> <td><select name="cmbnrawmaterial[]" id="cmbnrawmaterial'+ prsoitemcount +'" class="form-control" style="width: 250px;"></select></td> <td><input id="txtnqtyrequired'+ prsoitemcount +'" name="txtnqtyrequired[]" class="form-control" placeholder="Quantity Required" type="number"></td> <td></td> <td style="text-align: center;"><button id="btnremoveprso" name="btnremoveprso" class="btn btn-flat btn-danger" value="'+ prsoitemcount +'"><i class="fa fa-trash"></i></button></td> </tr>');

            LoadRawMaterials('cmbnrawmaterial'+prsoitemcount);            

        }

    });

    function CreateIPRInformation(){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/createnewprs") }}',
            type: 'post',
            dataType: 'json',
            success: function(response){

                cprid = response.prid;
                $('#txtiprnumber').val(response.prnumber);          

            }
        });

    }

    function CancelIPRSInformation(){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/cancelnewprs") }}',
            type: 'post',
            data: {
                prid: cprid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#newprs').modal('toggle');
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

    function DeleteDelDeliveryInfo(id, itemid){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/deletedeldeliveryinfo") }}',
            type: 'post',
            data: {
                itemid: itemid,
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#deliteminfo'+ itemid).text('');
                    $('#deliteminfo'+ itemid).append(response.content);

                }

            }
        });

    }

    function SaveDelPRItemsInfo(){

        var deldate = $('#txtdelpritemdeldate').val();
        var delqty = $('#txtdelpritemdelqty').val();

        //Validation
        if(deldate==""){
            toastr.error("Please select a delivery date.", '', { positionClass: 'toast-top-center' });
        }
        else if(delqty<0 || delqty==0 || delqty==""){
            toastr.error("Please input a delivery quantity.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/planning/cuspurchaseorder/savedelpriteminfo") }}',
                type: 'post',
                data: {
                    itemid: sdelitemid,
                    deldate: deldate,
                    delqty: delqty
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#deliteminfo'+ sdelitemid).text('');
                        $('#deliteminfo'+ sdelitemid).append(response.content);
                        $('#delpritems').modal('toggle');                        

                    }

                }
            });

        }

    }

    function ClearDelPRItemsInfo(){

        $('#txtdelpritemdeldate').val('');
        $('#txtdelpritemdelqty').val('');

    }

    function DeletePRItem(itemid){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/deletepritem") }}',
            type: 'post',
            data: {
                prid: cprid,
                itemid: itemid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#prsosaveitem'+itemid).remove();
                    prsoitemsavecount = response.prsoitemsavecount;

                    if(prsoitemcount==0 && prsoitemsavecount!=0){
                        if(praction=="Individual PRSSO"){
                            $('#tblnewprsosave').find('tr').remove();
                        }
                        else{
                            $('#tblnewprsave').find('tr').remove();
                        }
                    }
                    else if(prsoitemsavecount==0 && prsoitemcount==0){
                        if(praction=="Individual PRSSO"){
                            $('#tblnewprsocontent').append('<tr><td colspan="4" style="text-align: center;">No Item</td></tr>');
                        }
                        else{
                            $('#tblnewprcontent').append('<tr><td colspan="4" style="text-align: center;">No Item</td></tr>');
                        }
                    }


                }

            }
        });

    }

    function SavePRItems(){

        var rmid = $('[name="cmbnrawmaterial[]"]').serializeArray();
        var qtyrequired = $('[name="txtnqtyrequired[]"]').serializeArray();
        var hasrmid = true;
        var hasqtyreq = true;

        if(rmid.length!=0){

            //Validation
            for(var i=0;i<rmid.length;i++){

                if(rmid[i]["value"]==""){
                    hasrmid = false;
                }

                if(qtyrequired[i]["value"]=="" || qtyrequired[i]["value"]<0 || qtyrequired[i]["value"]==0){
                    hasqtyreq = false;
                }

            }

            if(!hasrmid){
                toastr.error('Please select a raw material info.', '', { positionClass: 'toast-top-center' }); 
            }
            else if(!hasqtyreq){
                toastr.error('Please input a required quantity.', '', { positionClass: 'toast-top-center' });
            }
            else{

                $.ajax({
                    url: '{{ url("api/planning/cuspurchaseorder/savenewprsitem") }}',
                    type: 'post',
                    data: {
                        prid: cprid,
                        rmid: rmid,
                        qtyrequired: qtyrequired
                    },
                    dataType: 'json',
                    success: function(response){

                        if(response.success){

                            if(praction=="Individual PRSSO"){
                                prsoitemcount = 0;
                                $('#tblnewprsocontent').find('tr').remove();
                                $('#tblnewprsosave').find('tr').remove();
                                $('#tblnewprsocontent').append(response.content);
                                prsoitemsavecount = response.prsoitemsavecount;
                            }
                            else{
                                prsoitemcount = 0;
                                $('#tblnewprcontent').find('tr').remove();
                                $('#tblnewprsave').find('tr').remove();
                                $('#tblnewprcontent').append(response.content);
                                prsoitemsavecount = response.prsoitemsavecount;
                            }


                        }

                    }
                });

            }

        }
        else{

            toastr.error('Please select a raw material info.', '', { positionClass: 'toast-top-center' });

        }

    }

    function RemovePRSInformation(prid){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/cancelnewprs") }}',
            type: 'post',
            data: {
                prid: prid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadPurchaseRequestInformation();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    //Socket Emit
                    socket.emit('planningpr');

                }

            }
        });

    }

    function CancelPRSInformation(){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/cancelnewprs") }}',
            type: 'post',
            data: {
                prid: cprid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#purchaserequestitem').modal('toggle');
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

    function CreatePRInformation(){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/createnewprs") }}',
            type: 'post',
            dataType: 'json',
            success: function(response){

                cprid = response.prid;
                $('#txtnprnumber').val(response.prnumber);          

            }
        });

    }

    function DeleteSOInformation(){

        var sodata = $('[name="chkddelid[]"]').serializeArray();
        var reason = $('#txtdelreason').val();

        if(reason==""){

            toastr.error("Please input your reason.", '', { positionClass: 'toast-top-center' });

        }
        else if(sodata.length==0){

            toastr.error("Please select a sales order info.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/planning/cuspurchaseorder/deletesoinformation") }}',
                type: 'post',
                data: {
                    sodata: sodata,
                    reason: reason,
                    authinfo: authinfo
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadCustomerPO();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#delso .close').click();

                    }

                }
            });
            

        }
        

    }

    function LoadSOItemInformation(){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/loadsoiteminformation") }}',
            type: 'get',
            data: {
                itemid: sitemid
            },
            dataType: 'json',
            success: function(response){

                $('#tblplanningsocontent').find('tr').remove();
                $('#tblplanningsocontent').append(response.content);

            }
        })

    }


    function SavePurchaseRequest(){

        var reason = $('#txtnreason').val();
        var sodata = $('[name="chkdelid[]"]').serializeArray();

        if(reason==""){
            toastr.error("Please input the reason for request.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/planning/cuspurchaseorder/savepurchaserequest") }}',
                type: 'post',
                data: {
                    prid: cprid,
                    reason: reason,
                    sodata: sodata
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $("#purchaserequestitem").modal('toggle');
                        LoadCustomerPO();
                        ReloadPurchaseRequestInformation();

                        //Socket Emit
                        socket.emit('planningpr');

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    }

    function SavePurchaseRequestIndividual(){

        var reason = $('#txtireason').val();

        if(reason==""){
            toastr.error("Please input the reason for request.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/planning/cuspurchaseorder/savepurchaserequestindividual") }}',
                type: 'post',
                data: {
                    prid: cprid,
                    reason: reason
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $("#newprs").modal('toggle');
                        // LoadCustomerPO();
                        ReloadPurchaseRequestInformation();

                        //Socket Emit
                        socket.emit('planningpr');

                    }

                }
            });

        }

    }

    function LoadRawMaterials(id){

        $.ajax({
            url: '{{ url("api/purchaserequest/loadrawmaterials") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="" disabled selected>Select a Item</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["rmid"] +'">'+  response.data[i]["description"] +'</option>');
                }

            },
            complete: function(){

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function GenPurchaseRequestNumber(id){

        $.ajax({
            url: '{{ url("api/purchaserequest/genpurchaserequestnumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).val(response.batch);

            }
        });

    }

    function LoadItemSONumber(itemid){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/loaditemsonumber") }}',
            type: 'get',
            data: {
                itemid: itemid
            },
            dataType: 'json',
            success: function(response){

                $('#tblitemsonumbercontent').find('tr').remove();
                for(var i=0;i<response.salesorderinfo.length;i++){
                    
                    $('#tblitemsonumbercontent').append('<tr> <td><div class="checkbox"><label><input id="chkdelid" name="chkdelid[]" type="checkbox" value="'+ response.salesorderinfo[i]["delid"] +'"></label></div></td> <td style="vertical-align: middle;">'+ response.salesorderinfo[i]["salesnumber"] +'</td> <td style="vertical-align: middle;">'+ response.salesorderinfo[i]["deliveryqty"] +'</td> <td style="vertical-align: middle;">'+ response.salesorderinfo[i]["deliverydate"] +'</td> </tr>');

                }

            }
        });

    }

    function LoadCustomer(id){

        $.ajax({
            url: '{{ url("api/planning/cuspurchaseorder/loadcustomers") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append(response.content);

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){

                $('#'+id).val("All").trigger("change");

            }
        });

    }

    function LoadCustomerPO(){

        tblcustomerpo.destroy();
        tblcustomerpo = $('#tblcustomerpo').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/planning/cuspurchaseorder/loadcustomerpo") }}',
                data: {
                    customer: scustomer
                },
            },
            columns : [
                {data: 'ponumber', name: 'ponumber'},
                {data: 'itemdescription', name: 'itemdescription'},
                {data: 'qty', name: 'qty'},
                {data: 'sonumber', name: 'sonumber'},
                {data: 'deliveryqty', name: 'deliveryqty'},
                {data: 'deliverydate', name: 'deliverydate'},
                {data: 'customer', name: 'customer'},
                {data: 'panel', name: 'panel', width: '5%'},
            ]
        });

        if(scustomer=="All"){
            tblcustomerpo.columns(6).visible(true); 
        }
        else{
            tblcustomerpo.columns(6).visible(false); 
        }

    }

    function LoadPurchaseRequestInformation(){

        tblpurchaserequest = $('#tblpurchaserequest').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/purchaserequest/loadpurchaserequestinformation") }}',
            },
            columns : [
                {data: 'status', name: 'status'},
                {data: 'prnumber', name: 'prnumber'},
                {data: 'itemdescription', name: 'itemdescription'},
                {data: 'reqqty', name: 'reqqty'},
                {data: 'reason', name: 'reason'},
                {data: 'issuedby', name: 'issuedby'},
                {data: 'issueddate', name: 'issueddate'},
                {data: 'issuedtime', name: 'issuedtime'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

    }

    function ReloadCustomerPO(){

        if(scustomer!=null){
            tblcustomerpo.ajax.reload();
        }

    }
    
    function ReloadPurchaseRequestInformation(){

        tblpurchaserequest.ajax.reload();

    }

    //Socket functions
    socket.on('cuspurchaseorder', function(){

        ReloadCustomerPO();

    });

    socket.on('reloadprplanning', function(){
      ReloadPurchaseRequestInformation();
    });

  </script>  

@endsection