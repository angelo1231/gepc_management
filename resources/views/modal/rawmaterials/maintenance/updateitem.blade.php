<!-- Middle Modal -->
<style>
    .modal {
      text-align: center;
      padding: 0!important;
    }
    
    .modal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }
    
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }

    .ui-autocomplete  {
        z-index: 1100 !important;
    }
    </style>   
    <!-- Modal -->
    <div class="modal fade" id="modalupdateitem" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <strong>RAW MATERIAL INFORMATION</strong>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="container-fluid">
                        <div class="col-md-6">
                                <div class="col-md-12" id="uname">
                                        <div class="form-group">
                                                <label for="txtuname">Name</label>
                                                <input id="txtuname" name="txtuname" type="text" class="form-control" placeholder="Name">
                                        </div>
                                </div>
                                <div class="col-md-12" id="udescription">
                                        <div class="form-group">
                                                <label for="txtudescription">Description</label>
                                                <textarea id="txtudescription" name="txtudescription" type="text" class="form-control" rows="4" placeholder="Description"></textarea>
                                        </div>
                                </div>
                                <div class="col-md-6" id="uwidth">
                                        <div class="form-group">
                                                <label for="txtuwidth">Width</label>
                                                <input id="txtuwidth" name="txtuwidth" type="text" class="form-control" placeholder="Width">
                                        </div>
                                </div>
                                <div class="col-md-6" id="uwidthsize">
                                        <div class="form-group">
                                                <label for="txtuwidthsize"><br></label>
                                                <input id="txtuwidthsize" name="txtuwidthsize" type="text" class="form-control">
                                        </div>
                                </div>
                                <div class="col-md-6" id="ulength">
                                        <div class="form-group">
                                                <label for="txtulength">Length</label>
                                                <input id="txtulength" name="txtulength" type="text" class="form-control" placeholder="Length">
                                        </div>
                                </div>
                                <div class="col-md-6" id="ulengthsize">
                                        <div class="form-group">
                                                <label for="txtulengthsize"><br></label>
                                                <input id="txtulengthsize" name="txtulengthsize" type="text" class="form-control">
                                        </div>
                                </div>
  
                        </div>
                        <div class="col-md-6">
                                <div class="col-md-12" id="uflute">
                                        <div class="form-group">
                                                <label for="txtuflute">Flute</label>
                                                <input id="txtuflute" name="txtuflute" type="text" class="form-control" placeholder="Flute">
                                        </div>
                                </div>
                                <div class="col-md-12" id="uboardpound">
                                        <div class="form-group">
                                                <label for="txtuboardpound">Board Pound</label>
                                                <input id="txtuboardpound" name="txtuboardpound" type="text" class="form-control" placeholder="Board Pound">
                                        </div>
                                </div>
                                <div class="col-md-6" id="ucurrency">
                                        <div class="form-group">
                                                <label id="lblucurrency" for="cmbucurrency">Currency</label>
                                                <select class="form-control" id="cmbucurrency">
                                                </select>
                                        </div>
                                </div>
                                <div class="col-md-6" id="uprice">
                                        <div class="form-group">
                                                <label id="lbluprice" for="txtuprice">Price</label>
                                                <input id="txtuprice" name="txtuprice" type="number" class="form-control" placeholder="Price">
                                        </div>
                                </div>
                                <div class="col-md-12">
                                        <div class="form-group">
                                                <label for="cmbucategory">Category</label>
                                                <select class="form-control" id="cmbucategory">
                                                        <option value="" disabled selected>Selec a Category</option>
                                                        <option value="Raw Material">Raw Material</option>
                                                        <option value="Other">Other</option>
                                                        <option value="Fix Asset">Fix Asset</option>
                                                </select>
                                        </div>
                                </div>

                        </div>
                  </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-9">
    
                    </div>
                    <div class="col-md-3">
                        <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-block btn-flat">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>