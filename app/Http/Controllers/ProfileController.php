<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Profile_Model;
use Illuminate\Support\Facades\Storage;
use Image; 
use Session;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\UserController;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index(){

        $access = UserController::UserGroupAccess();
        $usertype = Profile_Model::GetUsertype(Auth::user()->usertype);

        return view('profile')
        ->with('usertype', $usertype)
        ->with('access', $access);

    }

    function UploadImage(Request $request){

        if($request->hasFile('image')){
            
            //Storage::putFile('public/images', $request->file('image'));
            $request->image->storeAs('public', Auth::user()->id.".jpg");
            Profile_Model::UploadImage(Auth::user()->id, Auth::user()->id.".jpg");

            Session::flash('message', "Image has been upload");
            return json_encode([
                "success"=>true
            ]);

        }


    }

    function UpdateInformation(Request $request){

        Profile_Model::UpdateInformation(Auth::user()->id, $request);
        Auth::user()->firstname = $request->firstname;
        Auth::user()->lastname = $request->lastname;
        Auth::user()->mi = $request->mi;
        Session::flash('message', "Profile information has been update.");
        return json_encode(["success"=>true]);

    }

    function ChangePassword(Request $request){

        $oldpassword = Profile_Model::OldPassword(Auth::user()->id);

        if(Hash::check($request->oldpassword, $oldpassword)){
           
            if($request->newpassword==$request->confirmpassword){

                $user = User::find(Auth::user()->id);
                $user->password = bcrypt($request->newpassword);
                $user->save();

                return json_encode(['success'=>true, 'message'=>'Account password has been change.']);

            }
            else{
                return json_encode(['success'=>false, 'message'=>'New password do not match confirm password.', 'type'=>'matchpassword']);
            }

        }
        else{
            return json_encode(['success'=>false, 'message'=>'Old password do not match.', 'type'=>'oldpassword']);
        }

    }

    function UploadESign(Request $request){

        $userid = Auth::user()->id;

        //Set Data
        $file = $request->file('file');
        $filename = rand() . '.' . $file->getClientOriginalExtension();

        //Upload Attachment
        $file->move(public_path('esign'), $filename);

        //File Permission
        chmod(public_path('esign/'.$filename), 0777);

        //Update User
        Profile_Model::UpdateUserESign($userid, $filename);

        Session::flash('message', "Signature has been upload");
        return json_encode([
            "success"=>true
        ]);


    }



}
