<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modalnewprocess" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>NEW PROCESS</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtnprocess">Process</label>
                            <input type="text" name="txtnprocess" id="txtnprocess" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtnforcoc">For Final Inspection</label>
                            <select name="txtnforcoc" id="txtnforcoc" class="form-control">
                              <option value="0">Disabled</option>
                              <option value="1">Enabled</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnsave" name="btnsave" type="button" class="btn btn-primary btn-block" style="margin-top: 10px;">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>