@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 id="lblheader" class="box-title"><i class="fa fa-th-list"></i><b> PO# {{ $poinformation[0]->ponumber }}</b></h3>
          <a href="{{ url("/planning/cuspurchaseorder") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

             <div class="col-md-3">

                <div class="form-group">
                    <label for="txtcustomer">Customer</label>
                    <input id="txtcustomer" name="txtcustomer" class="form-control" type="text" placeholder="Customer" value="{{ $poinformation[0]->customer }}" readonly>
                </div>

                <div class="form-group">
                    <label for="txtdeldate">Delivery Date</label>
                    <input id="txtdeldate" name="txtdeldate" type="text" class="form-control" placeholder="Delivery Date" value="{{ $poinformation[0]->deliverydate }}" readonly>
                </div>

            </div>
            <div class="col-md-9">

                <div class="form-group">
                    <label for="txtremarks">Remarks</label>
                    <textarea id="txtremarks" name="txtremarks" class="form-control" rows="4" style="width: 400px;" readonly>{{ $poinformation[0]->remarks }}</textarea>
                </div>

            </div>

            <div class="col-md-12">

                    <table id="tblitems" class="table table-hover" style="margin-top: 10px;">
                        <tr>
                            <th>Partnumber</th>
                            <th>Partname</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Qty</th>
                            <th>Delivery Qty</th>
                            <th></th>
                        </tr>
                        @foreach ($poinformationitems as $val)
                            <tr>
                                <td>{{ $val->partnumber }}</td>
                                <td>{{ $val->partname }}</td>
                                <td>{{ $val->description }}</td>
                                <td>
                                    I.D: W:  {{ $val->ID_width }} L: {{ $val->ID_length }} H: {{ $val->ID_height }}
                                    <br>
                                    O.D: W:  {{ $val->OD_width }} L: {{ $val->OD_length }} H: {{ $val->OD_height }}
                                </td>
                                <td>{{ $val->qty }}</td>
                                <td>{{ $val->delqty }}</td>
                                <td><button id="btncreatejo" name="btncreatejo" class="btn btn-flat btn-success" title="Create Job Order"><i class="fa fa-file-o"></i></button></td>
                            </tr>
                        @endforeach
                    </table>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>



  </script>  

@endsection