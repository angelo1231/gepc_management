<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use DataTables;
use App\Process_Model;
use Illuminate\Support\Collection;
use App\Http\Controllers\UserController;

class ProcessController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index($view=null){

        $access = UserController::UserGroupAccess();
            
        if($view!=null){

            return view('extensions.process.'.$view)
            ->with('access', $access);

        }
        else{

            return view('process')
            ->with('access', $access);

        }

    }

    function ProcessInformation(){

        $process = Process_Model::ProcessInformation();
        $data = array();

        for($i=0;$i<count($process);$i++){
            $obj = new \stdClass;
            
            $obj->id = $process[$i]->pid;
            $obj->process = $process[$i]->process;

            if($process[$i]->forcoc!=0){

                $obj->forfinalinspection = '<i class="fa fa-check" aria-hidden="true"></i>';

            }
            else{

                $obj->forfinalinspection = '<i class="fa fa-times" aria-hidden="true"></i>';

            }

            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $process[$i]->pid .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button>';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'forfinalinspection'])->make(true);

    }

    function SaveProcess(Request $request){

        $process_count = Process_Model::ValidateProcess($request->process);

        if($process_count == 0){
            Process_Model::SaveProcess($request);
            return json_encode(["success"=>true, "message"=>"Process information has been save."]);
        }
        else{
            return json_encode(["success"=>false, "message"=>"Process code already exist."]);
        }   

    }

    function ProcessProfile(Request $request){

        $data = Process_Model::ProcessProfile($request->pid);
        
        return json_encode([
            "process"=>$data[0]->process, 
            "forcoc"=>$data[0]->forcoc
        ]);

    }

    function UpdateProcess(Request $request){

        Process_Model::UpdateProcess($request);

        return json_encode(["success"=>true, "message"=>"Process information has been update."]);

    }

    function LoadProcess(){

        $process = Process_Model::LoadProcess();

        return json_encode([
            "data"=>$process
        ]);

    }

    function SaveGroupProcess(Request $request){

        $validation = Process_Model::ValidateGroupProcess($request->processcode);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Process code already exist."
            ]);

        }
        else{

            for($i=0;$i<count($request->dataprocess);$i++){

                Process_Model::SaveGroupProcess($request->processcode, $request->dataorder[$i]["value"], $request->dataprocess[$i]["value"]);

            }

            Process_Model::SaveProcessStyle($request->processcode, $request->style, $request->inkcolor);

            return json_encode([
                "success"=>true,
                "message"=>"Process code information has been save."
            ]);


        }

    }

    function LoadGroupProcess(){

        $process;
        $groupprocess = Process_Model::GetGroupProcess();

        $data = array();

        for($i=0;$i<count($groupprocess);$i++){
            
            $process = "";
            $processdata = Process_Model::GetOrderGroupProcess($groupprocess[$i]->processcode);
            for($y=0;$y<count($processdata);$y++){

                if($y+1<count($processdata)){

                    $process = $process . $processdata[$y]->process . ", ";

                }
                else{

                    $process = $process . $processdata[$y]->process;

                }
                
            }

            $obj = new \stdClass;
            
            $obj->processcode = $groupprocess[$i]->processcode;
            $obj->process = $process;
            $obj->style = $groupprocess[$i]->style;
            $obj->inkcolor = $groupprocess[$i]->inkcolor;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-flat btn-primary" value="'.$groupprocess[$i]->processcode.'"><i class="fa fa-edit"></i></button>';
            $data[] = $obj;

        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadGroupProcessInformation(Request $request){

        $groupprocess = Process_Model::LoadGroupProcessInformation($request->processcode);

        return json_encode([
            "groupprocess"=>$groupprocess
        ]);

    }

    function DeleteGroupProcess(Request $request){

        Process_Model::DeleteGroupProcess($request->processcode);

        return json_encode([
            "success"=>true,
            "message"=>"Process code has been deleted."
        ]);

    }

    function UpdateGroupProcessInformation(Request $request){

        for($i=0;$i<count($request->processid);$i++){

            if($request->processid[$i]["value"]!=0){ //Update

                Process_Model::UpdateProcessInfo($request->processid[$i]["value"], $request->ordering[$i]["value"], $request->process[$i]["value"]);

            }
            else{ //Insert

                Process_Model::SaveGroupProcess($request->processcode, $request->ordering[$i]["value"], $request->process[$i]["value"]);

            }

        }

        //Delete
        if(isset($request->pdelete)){

            for($i=0;$i<count($request->pdelete);$i++){

                Process_Model::DeleteProcessInfo($request->pdelete[$i]);

            }

        }

        //Validation For Process Style
        $validation = Process_Model::ValidateProcessStyle($request->processcode);

        if($validation){ //Update Process Style

            Process_Model::UpdateProcessStyle($request->processcode, $request->style, $request->inkcolor);

        }   
        else{ //Insert Process Style

            Process_Model::SaveProcessStyle($request->processcode, $request->style, $request->inkcolor);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Group process information has been update."
        ]);

    }

}
