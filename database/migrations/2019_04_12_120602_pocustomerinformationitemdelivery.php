<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pocustomerinformationitemdelivery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pocustomerinformationitemdelivery', function (Blueprint $table) {
            $table->increments('delid');
            $table->string('salesnumber', 60);
            $table->integer('itemid');
            $table->integer('deliveryqty');
            $table->date('deliverydate');
            $table->integer('prid')->default(0);
            $table->integer('mrid')->default(0);
            $table->text('status');
            $table->timestamps();
            $table->datetime('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pocustomerinformationitemdelivery');        
    }
}
