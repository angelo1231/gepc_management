<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PurchaseRequest_Model extends Model
{
    
    public static function GenPurchaseRequestNumber($tableschema){

        $result = DB::select(DB::raw("
            SELECT CONCAT('PR', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) AS 'batch' FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'purchaserequest'
        "));

        return $result[0]->batch;

    }

    public static function Padding($batch){

        $result = DB::select(DB::raw('SELECT LPAD('.$batch.', 4, "0") AS pad'));
        
        return $result[0]->pad;

    }

    public static function LoadRawMaterials(){

        $result = DB::table('rawmaterials')
        ->select(
            'rmid',
            DB::raw("IF(category!='Raw Material', CONCAT(name, ' ', description), CONCAT(IF(widthsize!='', CONCAT(width,'.',widthsize), width), ' x ', IF(lengthsize!='', CONCAT(length, '.', lengthsize), length), ' ', flute, ' ', boardpound)) AS description")
        )
        ->where('category', '!=', 'Excess Board')
        ->get();

        return $result;

    }

    public static function SavePR($data, $issuedby, $tableschema){

        DB::table('purchaserequest')
        ->insert([
            "prnumber"=>DB::raw("(SELECT CONCAT('PR', DATE_FORMAT(NOW(), '%Y%m%d'),LPAD(AUTO_INCREMENT, 4, '0')) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '".$tableschema."' AND TABLE_NAME = 'purchaserequest')"),
            "rmid"=>$data->rmid,
            "qtyrequired"=>$data->reqqty,
            "reason"=>$data->reason,
            "issuedby"=>$issuedby,
            "status"=>"Pending",
            "created_at"=>DB::raw("NOW()")
        ]);
        
    }

    public static function LoadPurchaseRequestInformation($action){

        if($action=="Planning"){

            $result = DB::table('purchaserequest')
            ->select(
                'purchaserequest.prid',
                'purchaserequest.prnumber',
                DB::raw("GROUP_CONCAT(IF(rawmaterials.category!='Raw Material', CONCAT(rawmaterials.name, ' ', rawmaterials.description), CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound)) SEPARATOR ' <br>') AS 'description'"),
                DB::raw("GROUP_CONCAT(purchaserequestitems.qtyrequired SEPARATOR ' <br>') AS 'qtyrequired'"),
                'purchaserequest.reason',
                'purchaserequest.issuedby',
                DB::raw("DATE_FORMAT(purchaserequest.created_at, '%Y-%m-%d') AS 'issueddate'"),
                DB::raw("DATE_FORMAT(purchaserequest.created_at, '%h:%i %p') AS 'issuedtime'"),
                'purchaserequest.status'
            )
            ->join('purchaserequestitems', 'purchaserequestitems.prid', '=', 'purchaserequest.prid')
            ->join('rawmaterials', 'rawmaterials.rmid', '=', 'purchaserequestitems.rmid')
            ->groupBy('purchaserequest.prid')
            ->orderBy('purchaserequest.prid', 'DESC')
            ->get();

            return $result;

        }
        else if($action=="Purchasing"){

            $result = DB::table('purchaserequest')
            ->select(
                'purchaserequest.prid',
                'purchaserequest.prnumber',
                DB::raw("GROUP_CONCAT(IF(rawmaterials.category!='Raw Material', CONCAT(rawmaterials.name, ' ', rawmaterials.description), CONCAT(IF(rawmaterials.widthsize!='', CONCAT(rawmaterials.width,'.',rawmaterials.widthsize), rawmaterials.width), ' x ', IF(rawmaterials.lengthsize!='', CONCAT(rawmaterials.length, '.', rawmaterials.lengthsize), rawmaterials.length), ' ', rawmaterials.flute, ' ', rawmaterials.boardpound)) SEPARATOR ' <br>') AS 'description'"),
                DB::raw("GROUP_CONCAT(purchaserequestitems.qtyrequired SEPARATOR ' <br>') AS 'qtyrequired'"),
                'purchaserequest.reason',
                'purchaserequest.issuedby',
                DB::raw("DATE_FORMAT(purchaserequest.created_at, '%Y-%m-%d') AS 'issueddate'"),
                DB::raw("DATE_FORMAT(purchaserequest.created_at, '%h:%i %p') AS 'issuedtime'"),
                'purchaserequest.status'
            )
            ->join('purchaserequestitems', 'purchaserequestitems.prid', '=', 'purchaserequest.prid')
            ->join('rawmaterials', 'rawmaterials.rmid', '=', 'purchaserequestitems.rmid')
            ->where('purchaserequest.status', '=', 'Pending')
            ->groupBy('purchaserequest.prid')
            ->orderBy('purchaserequest.prid', 'DESC')
            ->get();

            return $result;

        }


    }

    public static function DeletePurchaseRequestInfo($id){

        DB::table('purchaserequest')
        ->where('prid', '=', $id)
        ->delete();

    }

    public static function UpdatePurchaseRequestInfoPurchasing($id){

        DB::table('purchaserequest')
        ->where('prid', '=', $id)
        ->update([
            "status"=>"Done",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadSuppliers(){

        $result = DB::table('suppliers')
        ->select(
            'sid',
            'supplier'
        )
        ->get();

        return $result;

    }

    public static function LoadFlutePaperCombination($flute, $boardpound){

        $result = DB::table('papercombination')
        ->select(
            'papercomid',
            'rmpapercombination'
        )
        ->where('flute', '=', $flute)
        ->where('boardpound', '=', $boardpound)
        ->get();

        return $result;

    }

    public static function LoadPaperComPrice($papercomid){

        $result = DB::table('papercombinationprice')
        ->select(
            'price'
        )
        ->whereNull('deleted_at')
        ->get();

        return $result;

    }


}
