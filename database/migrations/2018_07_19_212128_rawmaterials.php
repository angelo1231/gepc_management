<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rawmaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmaterials', function (Blueprint $table) {
            $table->increments('rmid');
            $table->string('itemcode','255');
            $table->text('name')->nullable();
            $table->longtext('description')->nullable();
            $table->text('width')->nullable();
            $table->text('widthsize')->nullable();
            $table->text('length')->nullable();
            $table->text('lengthsize')->nullable();
            $table->text('flute')->nullable();
            $table->text('boardpound')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->integer('stockonhand')->default(0);
            $table->integer('currencyid')->nullable();
            $table->text('category')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmaterials');
    }
}
