<!-- Middle Modal -->
<style>
    .modal {
       text-align: center;
       padding: 0!important;
    }
        
    .modal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }
        
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
    
    /* .ui-autocomplete  {
        z-index: 1100 !important;
    } */
    
    </style>   
    
    <!-- Modal -->
    <div class="modal fade" id="cusadditem"  role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
    
                    <div class="modal-header">
                      <label id="lblinformation"><strong>Add Item</strong></label> 
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
      
                    <div class="modal-body">
                        <div class="container-fluid">
    
                            <div class="col-md-12">
    
                                <div class="form-group">
                                    <label for="cmbitem">Item</label>
                                    <select name="cmbitem" id="cmbitem" class="form-control"></select>
                                </div>

                                <div class="form-group">
                                    <label for="txtpartnumber">Partnumber</label>
                                    <input type="text" id="txtpartnumber" name="txtpartnumber" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtpartname">Partname</label>
                                    <input type="text" id="txtpartname" name="txtpartname" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtdescription">Description</label>
                                    <input type="text" id="txtdescription" name="txtdescription" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtsize">Size</label>
                                    <textarea rows="4" id="txtsize" name="txtsize" class="form-control" readonly></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="txtprice">Price</label>
                                    <input type="text" id="txtprice" name="txtprice" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="txtquantity">Quantity</label>
                                    <input type="number" id="txtquantity" name="txtquantity" class="form-control" disabled>
                                </div>

                                <div class="form-group">
                                    <label for="txttotal">Total</label>
                                    <input type="text" id="txttotal" name="txttotal" class="form-control" readonly>
                                </div>
                          
                            </div>
    
                        </div>
              
                    </div>
                    
                    <div class="modal-footer">

                        <div class="container-fluid">

                            <div class="col-md-12">
                                    <button id="btnaddcusitem" name="btnaddcusitem" class="btn btn-flat btn-success">Add</button>
                                    <button class="btn btn-flat btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>

                    </div>
    
                </div>
            </div>
           
    </div>
        