@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Excess</b></h3>
          <a href="{{ url("/rminventory") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                <br>
                <table id="tblexcess" class="table">
                    <thead>
                        <tr>
                            <th>Excess Material</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  {{-- @include('modal.materialrequest.approvemri') --}}

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblexcess;

    $(document).ready(function(){

        //Load
        LoadExcessInformation();

    });

    $(document).on('click', '#btnreceive', function(){

        var id = $(this).val();

        $.confirm({
              title: 'Receive',
              content: 'Receive this excess board?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        ReceiveExcess(id);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    function ReceiveExcess(id){

        $.ajax({
            url: '{{ url("api/rmexcess/receiveexcess") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadExcessInformation();

                }   

            }
        });

    }

    function LoadExcessInformation(){

        tblexcess = $('#tblexcess').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/rmexcess/loadexcessinformation") }}',
        },
        columns : [
            {data: 'rawmaterial', name: 'rawmaterial'},
            {data: 'qty', name: 'qty'},
            {data: 'panel', name: 'panel', width: '12%'},
        ]
      });

    }

    function ReloadExcessInformation(){

        tblexcess.ajax.reload();

    }

  </script>  

@endsection