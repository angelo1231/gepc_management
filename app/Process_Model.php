<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Process_Model extends Model
{
    
    public static function ProcessInformation(){

        $result = DB::table('process')
        ->select('*')
        ->get();

        return $result;

    }

    public static function SaveProcess($data){

        DB::table('process')
        ->insert([
            'process'=>$data->process,
            'forcoc'=>$data->forcoc,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateProcess($process){

        $result = DB::table('process')
        ->select(DB::raw('COUNT(*) AS process_count'))
        ->where('process', '=', $process)
        ->get();

        return $result[0]->process_count;

    }

    public static function ProcessProfile($pid){

        $result = DB::table('process')
        ->select('*')
        ->where('pid', '=', $pid)
        ->get();

        return $result;

    }

    public static function UpdateProcess($data){

        DB::table('process')
        ->where('pid', '=', $data->pid)
        ->update([
            "process"=>$data->process,
            "forcoc"=>$data->forcoc,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadProcess(){

        $result = DB::table('process')
        ->select(
            'pid',
            'process'
        )
        ->get();

        return $result;

    }

    public static function ValidateGroupProcess($processcode){

        $result = DB::table("processcode")
        ->select(
            DB::raw("COUNT(DISTINCT processcode) AS 'processcode_count'")
        )
        ->where('processcode', '=', $processcode)
        ->first();

        if($result->processcode_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveGroupProcess($processcode, $order, $processid){

        DB::table('processcode')
        ->insert([
            "processcode"=>$processcode,
            "processorder"=>$order,
            "processid"=>$processid
        ]);

    }

    public static function SaveProcessStyle($processcode, $style, $inkcolor){

        DB::table('processstyle')
        ->insert([
            "processcode"=>$processcode,
            "style"=>$style,
            "inkcolor"=>$inkcolor
        ]);

    }

    public static function GetGroupProcess(){

        $result = DB::table('processcode')
        ->select(
            'processcode.processcode',
            DB::raw("MAX(processstyle.style) AS 'style'"),
            DB::raw("MAX(processstyle.inkcolor) AS 'inkcolor'")
        )
        ->leftjoin('processstyle', 'processstyle.processcode', '=', 'processcode.processcode')    
        ->groupBy('processcode.processcode')
        ->get();

        return $result;

    }

    public static function GetOrderGroupProcess($processcode){

        $result = DB::table('processcode')
        ->select(
            'process.process'
        )
        ->join('process', 'process.pid', '=', 'processcode.processid')
        ->where('processcode.processcode', '=', $processcode)
        ->orderBy('processcode.processorder', 'ASC')
        ->get();

        return $result;

    }

    public static function LoadGroupProcessInformation($processcode){

        $result = DB::table('processcode')
        ->select(
            'processcode.*',
            'processstyle.style',
            'processstyle.inkcolor'
        )
        ->leftjoin('processstyle', 'processstyle.processcode', '=', 'processcode.processcode')
        ->where('processcode.processcode', '=', $processcode)
        ->orderBy('processcode.processorder')
        ->get();

        return $result;

    }

    public static function DeleteGroupProcess($processcode){

        DB::table('processcode')
        ->where('processcode', '=', $processcode)
        ->delete();

    }

    public static function DeleteProcessInfo($id){

        DB::table('processcode')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function UpdateProcessInfo($id, $order, $processid){

        DB::table('processcode')
        ->where('id', '=', $id)
        ->update([
            "processorder"=>$order,
            "processid"=>$processid
        ]);

    }

    public static function ValidateProcessStyle($processcode){

        $result = DB::table('processstyle')
        ->select(
            DB::raw("COUNT(*) AS 'processcode_count'")
        )
        ->where('processcode', '=', $processcode)
        ->get();

        if($result[0]->processcode_count!=0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function UpdateProcessStyle($processcode, $style, $inkcolor){

        DB::table('processstyle')
        ->where('processcode', '=', $processcode)
        ->update([
            "style"=>$style,
            "inkcolor"=>$inkcolor
        ]);

    }

}
