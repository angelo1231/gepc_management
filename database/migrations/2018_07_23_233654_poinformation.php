<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Poinformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poinformation', function (Blueprint $table) {
            $table->increments('poid');
            $table->string('ponumber', '60')->unique();
            $table->text('attentionto');
            $table->text('deliveryto');
            $table->decimal('grandtotal', 10, 2);
            $table->decimal('deliverycharge', 10, 2)->default(0);
            $table->integer('sid');
            $table->text('remarks');
            $table->longtext('note')->nullable();
            $table->integer('iuid', 11);
            $table->text('issuedby');
            $table->integer('auid', 11)->default(0);
            $table->text('approvedby');
            $table->datetime('approveddate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poinformation');
    }
}
