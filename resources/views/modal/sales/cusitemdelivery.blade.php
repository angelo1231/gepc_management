<!-- Middle Modal -->
<style>
.modal {
   text-align: center;
   padding: 0!important;
}
    
.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}
    
.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

.ui-autocomplete  {
    z-index: 1100 !important;
}

</style>   

<!-- Modal -->
<div class="modal fade" id="cusitemdelivery"  role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  <label id="lblinformation"><strong>Delivery Information</strong></label> 
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
  
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="col-md-12">

                            <div id="divtblitemdelivery">
                                <table id="tblitemdelivery" class="table">
                                    <thead>
                                      <tr>
                                        <th></th>
                                        <th>Sales Order #</th>
                                        <th>Delivery Quantity</th>
                                        <th>Delivery Date</th>
                                        {{-- <th>Created By</th> --}}
                                        <th>Created Date</th>
                                        <th><button id="btnadddeliverysales" name="btnadddeliverysales" class="btn btn-flat btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
                                      </tr>
                                    </thead>
                                    <tbody id="itemdeliverycontent">
                                    </tbody>
                                </table>
                            </div>
                            <div id="divadddeliverysales" style="display: none;">

                                <div class="form-group">
                                    <label for="txtadelqty">Delivery Quantity</label>
                                    <input type="text" id="txtadelqty" name="txtadelqty" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="txtadeldate">Delivery Date</label>
                                    <input type="text" id="txtadeldate" name="txtadeldate" class="form-control">
                                </div>
                                <button id="btnclose" name="btnclose" class="btn btn-flat btn-danger" style="float: right;">Close</button>
                                <button id="btnasave" name="btnasave" class="btn btn-flat btn-success" style="float: right; margin-right: 10px;">Save Delivery</button>

                            </div>
                            <div id="diveditdeliverysales" style="display: none;">

                              <div class="form-group">
                                  <label for="txtedelqty">Delivery Quantity</label>
                                  <input type="text" id="txtedelqty" name="txtedelqty" class="form-control">
                              </div>

                              <button id="btneclose" name="btneclose" class="btn btn-flat btn-danger" style="float: right;">Close</button>
                              <button id="btnesave" name="btnesave" class="btn btn-flat btn-success" style="float: right; margin-right: 10px;">Update Delivery</button>

                            </div>
                      
                        </div>

                    </div>
          
                </div>
                
                <div class="modal-footer">
                    <button id="btnclosedelivery" name="btnclosedelivery" class="btn btn-flat btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
       
</div>
    