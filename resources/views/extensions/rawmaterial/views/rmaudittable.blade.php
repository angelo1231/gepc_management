<div class="col-md-12 row">
<table id="tblaudit" class="table">
        <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Size</th>
              <th>Flute</th>
              <th>Board Pound</th>
              <th>Purchase Order Number</th>
              <th>Material Requisition Number</th>
              <th>Quantity</th>
              <th>Balance</th>
              <th>Issued By</th>
              <th>Issued Date</th>
              <th>Issued Time</th>
            </tr>
        </thead>
        <tbody>
            @foreach($audit as $data)
            <?php

                $size = "";
            
                if($data->category=="Raw Material"){

                    if($data->widthsize!=""){
                        $size = $size . $data->width . "." . $data->widthsize;
                    }
                    else{
                        $size = $size . $data->width;
                    }

                    if($data->lengthsize!=""){
                        $size = $size . " x " . $data->length . "." . $data->lengthsize;
                    }
                    else{
                        $size = $size . " x " . $data->length;
                    }

                }

                

            ?>
                <tr>
                    <th>{{ $data->name }}</th>
                    <th>{{ $data->description }}</th>
                    <th>{{ $size }}</th>
                    <th>{{ $data->flute }}</th>
                    <th>{{ $data->boardpound }}</th>
                    <th>{{ $data->ponumber }}</th>
                    <th>{{ $data->requisitionnumber }}</th>
                    <th>{{ $data->qty }}</th>
                    <th>{{ $data->balance }}</th>
                    <th>{{ $data->issuedby }}</th>
                    <th>{{ $data->issueddate }}</th>
                    <th>{{ $data->issuedtime }}</th>
                </tr>
            @endforeach
        </tbody>
</table>
</div>
<script>

    $(document).ready(function(){

        $('#tblaudit').DataTable({
            autoWidth: false,
            ordering: false,
        });
           
    });

</script>