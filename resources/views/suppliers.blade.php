@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Supplier Information</b></h3>
        </div>
        <div class="box-body">
            <div class="col-md-9">
            </div>
            <div class="col-md-3" style="margin-bottom: 15px;">
                <button id="btnnewsupplier" name="btnnewsupplier" class="btn btn-success btn-block btn-flat"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Supplier</strong></button>
            </div>   
            <div class="col-md-12">
            <table id="tblsuppliers" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Supplier</th>
                        <th>Contact Person</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>TIN</th>
                        <th>Terms</th>
                        <th>Tax Classification</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modals --}}
  @include('modal.supplier.newsupplier')
  @include('modal.supplier.updatesupplier')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tblsuppliers;

      $(document).ready(function() {

          tblsuppliers = $('#tblsuppliers').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: '{{ url("api/supplier/supplierinformation") }}',
            columns : [
                {data: 'sid', name: 'sid'},
                {data: 'supplier', name: 'supplier'},
                {data: 'contactperson', name: 'contactperson'},
                {data: 'contactnumber', name: 'contactnumber'},
                {data: 'email', name: 'email'},
                {data: 'address', name: 'address'},
                {data: 'tin', name: 'tin'},
                {data: 'terms', name: 'terms'},
                {data: 'tax', name: 'tax'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
          });

          SetSelect2();

      });


      $('#btnnewsupplier').on('click', function(){

        @if(in_array(23, $access))

          ClearNewSupplier();
          $('#modalnewsupplier').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnsave').on('click', function(){

        var supplier = $('#txtnsupplier').val();
        var contactperson = $('#txtncontactperson').val();
        var contactnumber = $('#txtncontactnumber').val();
        var email = $('#txtnemail').val();
        var address =  $('#txtnaddress').val();
        var tin = $('#txtntin').val();
        var terms = $('#txtnterms').val();
        var tax = $('#cmbntax').val();

        if(supplier==""){
          toastr.error('Please input the supplier name.', '', { positionClass: 'toast-top-center' });
          $('#txtnsupplier').focus();
        }
        else if(contactperson==""){
          toastr.error('Please input the contact person of the supplier.', '', { positionClass: 'toast-top-center' });
          $('#txtncontactperson').focus();
        }
        else if(contactnumber==""){
          toastr.error('Please input the contact number of the supplier.', '', { positionClass: 'toast-top-center' });
          $('#txtncontactnumber').focus();
        }
        else if(email==""){
          toastr.error('Please input the email of the supplier.', '', { positionClass: 'toast-top-center' });
          $('#txtnemail').focus();
        }
        else if(address==""){
          toastr.error('Please input the supplier address.', '', { positionClass: 'toast-top-center' });
          $('#txtnaddress').focus();
        }
        else{

          $.ajax({
            url: '{{ url("api/supplier/newsupplier") }}',
            type: 'post',
            data: {
              supplier: supplier, 
              contactperson: contactperson,
              contactnumber: contactnumber,
              email: email,
              address: address, 
              tin: tin, 
              terms: terms,
              tax: tax
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                $("#modalnewsupplier .close").click();
                ReloadSuppliers();
                toastr.success(response.message, '', { positionClass: 'toast-top-center' });

              }
              else{

                $('#txtnsupplier').val('');
                $('#txtnsupplier').focus();
                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }


            }
          });

        }

      });

      $(document).on('click', '#btnedit', function(){

        @if(in_array(24, $access))

          var sid = $(this).attr("value");
          ClearUpdate();
          $.ajax({
            url: '{{ url("api/supplier/supplierprofile") }}',
            type: 'get',
            data: {sid: sid},
            dataType: 'json',
            success: function(response){

              $('#txtusupplier').val(response.supplier);
              $('#txtucontactperson').val(response.contactperson);
              $('#txtucontactnumber').val(response.contactnumber);
              $('#txtuemail').val(response.email);
              $('#txtuaddress').val(response.address);
              $('#txtutin').val(response.tin);
              $('#txtuterms').val(response.terms);
              $('#cmbutax').val(response.tax).trigger('change');
              $('#btnupdate').val(sid);

            }
          });

          $("#modalupdatesupplier").modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnupdate').on('click', function(){

        var sid =$('#btnupdate').val();
        var supplier = $('#txtusupplier').val();
        var contactperson = $('#txtucontactperson').val();
        var contactnumber = $('#txtucontactnumber').val();
        var email = $('#txtuemail').val();
        var address =  $('#txtuaddress').val();
        var tin = $('#txtutin').val();
        var terms = $('#txtuterms').val();
        var tax = $('#cmbutax').val();

        if(supplier==""){
          toastr.error('Please input the supplier name.', '', { positionClass: 'toast-top-center' });

        }
        else if(contactperson==""){
          toastr.error('Please input the contact person of the supplier.', '', { positionClass: 'toast-top-center' });
          $('#txtucontactperson').focus();
        }
        else if(contactnumber==""){
          toastr.error('Please input the contact number of the supplier.', '', { positionClass: 'toast-top-center' });
          $('#txtucontactnumber').focus();
        }
        else if(email==""){
          toastr.error('Please input the email of the supplier.', '', { positionClass: 'toast-top-center' });
          $('#txtuemail').focus();
        }
        else if(address==""){
          toastr.error('Please input the supplier address.', '', { positionClass: 'toast-top-center' });
        }
        else{

          $.ajax({
            url: '{{ url("api/supplier/updatesupplier") }}',
            type: 'post',
            data: {
              sid: sid, 
              supplier: supplier,
              contactperson: contactperson,
              contactnumber: contactnumber,
              email: email, 
              address: address, 
              tin: tin, 
              terms: terms,
              tax: tax
            },
            dataType: 'json',
            success: function(response){

              $("#modalupdatesupplier .close").click();
              toastr.success(response.message, '', { positionClass: 'toast-top-center' });
              ReloadSuppliers();

            }
          });

        }

      });

      function ClearNewSupplier(){

        $('#txtnsupplier').val('');
        $('#txtncontactperson').val('');
        $('#txtncontactnumber').val('');
        $('#txtnemail').val('');
        $('#txtnaddress').val('');
        $('#txtntin').val('');
        $('#txtnterms').val('');

      }

      function ClearUpdate(){

        $('#txtusupplier').val('');
        $('#txtucontactperson').val('');
        $('#txtucontactnumber').val('');
        $('#txtuemail').val('');
        $('#txtuaddress').val('');
        $('#txtutin').val('');
        $('#txtuterms').val('');

      }

      function SetSelect2(){

        $('#cmbntax').select2({
          theme: 'bootstrap'
        });

        $('#cmbutax').select2({
          theme: 'bootstrap'
        });

      }

      function ReloadSuppliers(){
        tblsuppliers.ajax.reload();
      }

  </script>  

@endsection