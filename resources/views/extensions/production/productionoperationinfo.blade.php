@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Monitor Id: {{ $operationinfo->mid }} Job Order Number: {{ $operationinfo->jonumber }}</b></h3>
          <a href="{{ url("/planning") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

            <div class="col-md-12">
                <h3>Process Information</h3>
                <div class="col-md-6 row">
                    <div class="form-group">
                        <label for="cmbprocess">Process (Current)</label>
                        <select class="form-control" id="cmbprocess" name="cmbprocess">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="txtqty">Quantity</label>
                        <input id="txtqty" name="txtqty" type="text" class="form-control" placeholder="Quantity">
                    </div>
                    <div class="form-group">
                        <label for="txtrejectqty">Reject Quantity</label>
                        <input id="txtrejectqty" name="txtrejectqty" type="text" class="form-control" placeholder="Reject Quantity">
                    </div>
                    <button id="btnsaveprocessinfo" name="btnsaveprocessinfo" class="btn btn-success btn-flat" style="float: right;">Save Information</button>
                </div>

            </div>

            <div class="col-md-12">

                <h3>Process Data</h3>
                <table id="tbloperationinfo" class="table">
                    <thead>
                        <tr>
                            <th>Process</th>
                            <th>Process Count</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  @include('modal.production.productionprocessinfo')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tbloperationinfo;
    var mid = '{{ $operationinfo->mid }}';
    var joid = '{{ $operationinfo->joid }}';
    var processcode;
    var groupprocess;
    var currentpid = '{{ $operationinfo->currentpid }}';

    $(document).ready(function(){

        //Load
        LoadJobOrderProcess();
        LoadMonitoringProcess();

        //Get
        GetProcessCode();

    });

    $('#cmbprocess').on('change', function(){

        var pid = $(this).val();
        var auditid = $(this).find(':selected').data('id');
        
        $.ajax({
            url: '{{ url("api/productionoperationinfo/checkcurrentprocess") }}',
            type: 'get',
            data: {
                mid: mid,
                pid: pid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $.confirm({
                        title: 'Save',
                        content: 'Change the current process?',
                        type: 'blue',
                        buttons: {   
                            ok: {
                                text: "Yes",
                                btnClass: 'btn-info',
                                keys: ['enter'],
                                action: function(){

                                    ChangeCurrentProcess(mid, pid, auditid);
                                    
                                }
                            },
                            cancel: {
                                text: "No",
                                btnClass: 'btn-info',
                                action: function(){
                                    
                                    $('#cmbprocess').val(currentpid).trigger("change");

                                }
                            } 
                        }
                    });

                }

            }
        });


    });

    $('#btnsaveprocessinfo').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveProcessInformation();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
        });

    });

    $(document).on('click', '#btnprocessinfo', function(){

        var pid = $(this).data('id');
        var auditid = $(this).val();

        $.ajax({
            url: '{{ url("api/productionoperationinfo/loadprocessinfo") }}',
            type: 'get',
            data: {
                pid: pid,
                auditid: auditid
            },
            dataType: 'json',
            success: function(response){

                $('#pinfo').text(response.process + ' Information.');
                $('#productionprocessinfo').modal('toggle');
                LoadJobOrderProcessInfo(auditid);

            }
        });

    });

    function LoadJobOrderProcessInfo(auditid){

        $.ajax({
            url: '{{ url("api/productionoperationinfo/loadjoborderprocessinfo") }}',
            type: 'get',
            data: {
                auditid: auditid
            },
            dataType: 'json',
            success: function(response){

                $('#processinfocontent').find('tr').remove();

                if(response.data.length!=0){

                    for(var i=0;i<response.data.length;i++){

                        $('#processinfocontent').append('<tr><td>'+ response.data[i]["operator"] +'</td><td>'+ response.data[i]["qty"] +'</td><td>'+ response.data[i]["rejectqty"] +'</td><td>'+ response.data[i]["date_created"] +'</td></tr>');

                    }

                }
                else{

                    $('#processinfocontent').append('<tr><td colspan="5" style="text-align: center;"><strong>No Data</strong></td></tr>');

                }

            }
        });

    }

    function SaveProcessInformation(){

        var qty = $('#txtqty').val();
        var qtyreject = $('#txtrejectqty').val();
        var auditid = $('#cmbprocess').find(':selected').data('id');

        //Validation
        if(qty==""){

            toastr.error('Please input a quantity', '', { positionClass: 'toast-top-center' });
            $('#txtquantity').val('0');
            $('#txtquantity').focus();

        }
        else if(qty<="0"){

            toastr.error('Please input a quantity greater than zero.', '', { positionClass: 'toast-top-center' });
            $('#txtquantity').focus();

        }
        else{

            if(qtyreject==""){
                qtyreject = 0;
            }

            $.ajax({
                url: '{{ url("api/productionoperationinfo/saveprocessinformation") }}',
                type: 'post',
                data: {
                    auditid: auditid,
                    qty: qty,
                    qtyreject: qtyreject
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        RelaodOperationInfo();
                        ClearNewProductionInfo();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                        //Socket Emit
                        socket.emit('monitoringjo');

                    }


                }
            });

        }

    }

    function ClearNewProductionInfo(){

        $('#txtqty').val('');
        $('#txtrejectqty').val('');

    }

    function GetGroupProcess(){

        $.ajax({
            url: '{{ url("api/productionoperationinfo/getgroupprocess") }}',
            type: 'get',
            data: {
                processcode: processcode
            },
            dataType: 'json',
            success: function(response){

                groupprocess = response.data;

            }
        });

    }

    function GetProcessCode(){

        $.ajax({
            url: '{{ url("api/productionoperationinfo/getprocesscode") }}',
            type: 'get',
            data: {
                joid: joid
            },
            dataType: 'json',
            success: function(response){

                processcode = response.processcode;
                GetGroupProcess();

            }
        });

    }

    function LoadMonitoringProcess(){

        $.ajax({
            url: '{{ url("api/productionoperationinfo/loadmonitoringprocess") }}',
            type: 'get',
            data: {
                mid: mid
            },
            dataType: 'json',
            success: function(response){

                $('#cmbprocess').find('option').remove();
                for(var i=0;i<response.data.length;i++){

                    $('#cmbprocess').append('<option value="'+ response.data[i]["pid"] +'" data-id="'+ response.data[i]["auditid"] +'">'+ response.data[i]["process"] +'</option>');

                }
                $('#cmbprocess').val(currentpid);
                $('#cmbprocess').select2({
                    theme: "bootstrap"
                });

            }
        });

    }

    function ChangeCurrentProcess(mid, pid, auditid){

        $.ajax({
            url: '{{ url("api/productionoperationinfo/changecurrentprocess") }}',
            type: 'post',
            data: {
                mid: mid,
                pid: pid,
                auditid: auditid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    currentpid = pid;
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    //Socket Emit
                    socket.emit('monitoringjo');

                }

            }
        });

    }

    function LoadJobOrderProcess(){

        tbloperationinfo = $('#tbloperationinfo').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/productionoperationinfo/loadjoborderprocess") }}',
                data: {
                    mid: mid,
                    joid: joid
                }
            },
            columns : [
                {data: 'process', name: 'process'},
                {data: 'processcount', name: 'processcount'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

    }

    function RelaodOperationInfo(){

        tbloperationinfo.ajax.reload();

    }


  </script>  

@endsection