@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 id="qaheader" class="box-title"><i class="fa fa-th-list"></i> <b>Reject Type</b></h3>
          <a href="{{ url('/qualityassurance') }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">

          <div class="col-md-12">
              <button id="btnnewrejecttype" name="btnnewrejecttype" class="btn btn-flat btn-success" style="float:right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Reject Type</button>
          </div>

          <div class="col-md-12">
            <table id="tblrejecttype" class="table">
              <thead>
                  <tr>
                      <th>#</th>
                      <th>Code</th>
                      <th>Reject Type</th>
                      <th>Created By</th>
                      <th>Created Date</th>
                      <th></th>
                  </tr>
              </thead>
            </table>
          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  @include('modal.qualityassurance.newrejecttype')
  @include('modal.qualityassurance.updaterejecttype')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblrejecttype;
    var sid;

    $(document).ready(function(){

        //Load
        LoadRejectType();

    });

    $('#btnnewrejecttype').on('click', function(){

      @if(in_array(70, $access))

        ClearNewRejectType();
        $('#newrejecttype').modal('toggle');

      @else

        toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

      @endif

    });

    $('#btnsaverejecttype').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save this reject type information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveRejectType();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    $(document).on('click', '#btnurejecttype', function(){

        @if(in_array(71, $access))

            sid = $(this).val();

            ClearUpdateRejectType();
            LoadRejectTypeInfo(sid);
            $('#updaterejecttype').modal('toggle');

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

    });

    $('#btnupdaterejecttype').on('click', function(){

        $.confirm({
              title: 'Update',
              content: 'Update this reject type information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        UpdateRejectType();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    function UpdateRejectType(){

        var rejectcode = $('#txturejectcode').val();
        var rejecttype = $('#txtureject').val();

        if(rejectcode==""){

            toastr.error("Please input the reject code.", '', { positionClass: 'toast-top-center' });

        }
        else if(rejecttype==""){

            toastr.error("Please input the reject type.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/qainfo/rejecttype/updaterejecttype") }}',
                type: 'post',
                data: {
                    id: sid,
                    rejectcode: rejectcode,
                    rejecttype: rejecttype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadRejectType();
                        $('#updaterejecttype').modal('toggle');

                    }

                }
            });

        }

    }

    function LoadRejectTypeInfo(id){

        $.ajax({
            url: '{{ url("api/qainfo/rejecttype/loadrejecttypeinfo") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#txturejectcode').val(response.rejectcode);
                $('#txtureject').val(response.rejecttype);

            }
        });

    }

    function SaveRejectType(){

        var rejectcode = $('#txtnrejectcode').val();
        var rejecttype = $('#txtnreject').val();

        if(rejectcode==""){

            toastr.error("Please input the reject code.", '', { positionClass: 'toast-top-center' });

        }
        else if(rejecttype==""){

            toastr.error("Please input the reject type.", '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/qainfo/rejecttype/saverejecttype") }}',
                type: 'post',
                data: {
                    rejectcode: rejectcode,
                    rejecttype: rejecttype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadRejectType();
                        $('#newrejecttype').modal('toggle');

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });


                    }

                }
            });

        }

    }

    function LoadRejectType(){

        tblrejecttype = $('#tblrejecttype').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ordering: false,
        ajax: {
            type: 'get',
            url: '{{ url("api/qainfo/rejecttype/loadrejecttype") }}'
        },
        columns : [
            {data: 'id', name: 'id'},
            {data: 'code', name: 'code'},
            {data: 'rejecttype', name: 'rejecttype'},
            {data: 'createdby', name: 'createdby'},
            {data: 'createddate', name: 'createddate'},
            {data: 'panel', name: 'panel', width: '12%'},
        ]
      });

    }

    function ReloadRejectType() {
        
        tblrejecttype.ajax.reload();

    }

    function ClearNewRejectType(){

       $('#txtnrejectcode').val('');
       $('#txtnreject').val('');

    }

    function ClearUpdateRejectType(){

       $('#txturejectcode').val('');
       $('#txtureject').val('');

    }


  </script>  

@endsection