<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newpurchaserequest" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>NEW PURCHASE REQUEST</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txtnprnumber">Purchase Request #</label>
                            <input id="txtnprnumber" name="txtnprnumber" class="form-control" placeholder="Purchase Request Number" type="text" readonly>
                        </div>

                        <div class="form-group">
                            <label for="cmbnrawmaterial">Item</label>
                            <select name="cmbnrawmaterial" id="cmbnrawmaterial" class="form-control">
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="txtnqtyrequired">Quantity Required</label>
                            <input id="txtnqtyrequired" name="txtnqtyrequired" class="form-control" placeholder="Quantity Required" type="number">
                        </div>

                        <div class="form-group">
                            <label for="txtnreason">Reason</label>
                            <textarea class="form-control" rows="5" id="txtnreason" name="txtnreason" placeholder="Your Reason For Request"></textarea>
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <button id="btnsavepr" name="btnsavepr" type="button" class="btn btn-primary btn-flat">Save Information</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>