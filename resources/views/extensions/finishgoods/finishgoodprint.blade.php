<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Good Earth Packaging Corp.</title>

        <link rel="stylesheet" href="{{ asset('css/print.css') }}">

    </head>

<body>


    <page size="A4">    
        <table style="width:100%; border: 2px solid black;">
            <tr>
                <td colspan="7" style="border: 2px solid black;"><h2 style="text-align: center;">{{ $fgdata[0]->customer }} Inventory Report</h2></td>
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;">Partnumber</td>
                <td style="border: 2px solid black; text-align: center;">Partname</td>
                <td style="border: 2px solid black; text-align: center;">Description</td>
                <td style="border: 2px solid black; text-align: center;">Size</td>
                <td style="border: 2px solid black; text-align: center;">Currency</td>
                <td style="border: 2px solid black; text-align: center;">Price</td>
                <td style="border: 2px solid black; text-align: center;">Stocks</td>
            </tr>
            @foreach($fgdata as $data)
            <?php

                $idsize = "";
                $odsize = "";

                $idsize = "IDSize: W: " . $data->ID_width . " L: " . $data->ID_length . " H: " . $data->ID_height;
                $odsize = "ODSize: W: " . $data->OD_width . " L: " . $data->OD_length . " H: " . $data->OD_height

            ?>
            <tr>
                <td style="border: 2px solid black; text-align: center;">{{ $data->partnumber }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->partname }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->description }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $idsize }} {{ $odsize }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->currency }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->price }}</td>
                <td style="border: 2px solid black; text-align: center;">{{ $data->stockonhand }}</td>
            </tr>
            @endforeach
        </table>
    </page>


</body>
</html>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

  
