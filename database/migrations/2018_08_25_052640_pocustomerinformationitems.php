<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pocustomerinformationitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pocustomerinformationitems', function (Blueprint $table) {
            $table->increments('itemid');
            $table->integer('poid');
            $table->integer('fgid');
            $table->integer('currencyid');
            $table->decimal('rate', 10, 2);
            $table->decimal('price', 10, 2);
            $table->integer('qty');
            $table->decimal('total', 10, 2);
            $table->integer('delqty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pocustomerinformationitems');
    }
}
