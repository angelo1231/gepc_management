<!doctype html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Good Earth Packaging Corp.</title>

        {{-- CSS --}}
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">

        {{-- JS --}}
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/print.css') }}">

    </head>



<body>


    <page size="A4">    

        <table style="border: 2px solid black;">
            <tr>
                <td style="text-align: center;" colspan="5">
                    <img src="{{ asset('images/GEPC_LOGO_RPT.jpg') }}" style="margin-bottom: -17px; width: 250px;"/>
                    <h4>Blk 25 Lot 29-F Monserrat St. Villa De Toledo Subd. Brgy Balibago Sta Rosa Laguna<br>
                    Tel. No.: (049) 534-9581 * Fax No.: (049) 534-4439
                    </h4>
                    <h1>Purchase Order</h1>
                </td>

            </tr>
            <tr>

                <td colspan="4">
                </td>
                <td style="float: right; border-top: 2px solid black; border-left: 2px solid black; border-bottom: 2px solid black; border-right: 1px solid black; width: 220px;">
                   PO# {{ $info[0]->ponumber }}
                </td>
                
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>      
            </tr>
            <tr>
                <td style="border-top: 2px solid black; border-left: 2px solid black; border-bottom: 2px solid black; border-right: 1px solid black; width: 300px;" colspan="4">
                    TO:  {{ $info[0]->supplier }}
                </td>
                <td style="float: right; width: 300px; border-top: 2px solid black; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                    Date: {{ date('d-M-y', strtotime($info[0]->created_at)) }}
                </td>       
            </tr>
             <tr>
                <td style="border-top: 2px solid black; border-left: 2px solid black; border-bottom: 2px solid black; border-right: 1px solid black; width: 300px;" colspan="4">
                    ATTN: {{ $info[0]->attentionto }}
                </td>
                <td style="float: right; width: 300px; border-top: 1px solid black; border-left: 1px solid black; border-bottom: 1px solid black; border-right: 1px solid black;">
                    Terms: {{ $info[0]->terms }} days
                </td>       
            </tr>
            <tr>
                <td colspan="4">
                   
                </td>
                <td style="float: right; width: 300px; border-top: 1px solid black; border-left: 2px solid black; border-right: 1px solid black;">
                    {{-- Delivery: {{ $deliverydates }} --}}
                </td>       
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>      
            </tr>
            <tr>
                <td style="border: 2px solid black; width: 300px;" colspan="5">
                    Deliver To: {{ $info[0]->deliveryto }}
                </td>      
            </tr>
            <tr>
                <td colspan="5">
                    <br>
                </td>      
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><strong>ITEM #</strong></td>
                <td style="border: 2px solid black; text-align: center; width: 10000px;"><strong>DESCRIPTION</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>QUANTITY</strong></td>   
                <td style="border: 2px solid black; text-align: center; width: 5000px;"><strong>UNIT PRICE</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>TOTAL COST</strong></td>       
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            <?php $itemcount = 1; ?>
            @foreach($items as $item)
            <?php 

                $size = "";
                if($item->category=="Raw Material"){

                    if($item->widthsize!=""){
                        $size = $size . $item->width . "." . $item->widthsize;
                    }else{
                        $size = $size . $item->width;
                    }

                    if($item->lengthsize!=""){
                        $size = $size . " x " . $item->length . "." . $item->lengthsize;
                    }else{
                        $size = $size . " x " . $item->length;
                    }

                }
                else if($item->category=="Other"){

                    $size = $item->name;

                }
                

            ?>
            <tr>
                    <td style="border: 2px solid black; text-align: center;"><strong>{{ $itemcount }}</strong></td>
                    <td style="border: 2px solid black; text-align: center;">
                        <strong>
                            {{ $size }} {{ $item->flute }} {{ $item->boardpound }} <br>
                            <?php $deldates = ""; ?>
                            @foreach($deliverydates as $del)
                                @if($item->itemid==$del->itemid)
                                    <?php
                                        $deldates .= $del->deliverydate . ' ';
                                    ?>
                                @endif
                            @endforeach
                            @if($deldates=="")
                            <span style="font-size: 10px;">Delivery: TBA</span>
                            @else
                            <span style="font-size: 10px;">Delivery: {{ $deldates }}</span>
                            @endif
                            @if($item->rmpapercombination!="")
                            <br>
                            {{ $item->rmpapercombination }}
                            @endif
                        </strong>
                    </td>   
                    <td style="border: 2px solid black; text-align: center;"><strong>{{ $item->qty }}</strong></td>   
                    <td style="border: 2px solid black; text-align: center;">
                        <strong>
                            {{ $item->price }}
                        </strong>
                    </td>   
                    <td style="border: 2px solid black; text-align: center;"><strong>{{ number_format($item->total, 2) }}</strong></td>       
            </tr>
            <?php $itemcount += 1; ?>
            @endforeach 
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            @if($info[0]->deliverycharge!=0)
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"><strong>Delivery Charge</strong></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>{{ number_format($info[0]->deliverycharge, 2) }}</strong></td>       
            </tr>
            @else
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            @endif
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>       
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;"><br></td>
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>Total Amount</strong></td>   
                <td style="border: 2px solid black; text-align: center;"><strong>{{ number_format($info[0]->grandtotal, 2) }}</strong></td>       
            </tr>
            <tr>
                <td colspan="5">
                    <p>Note:</p>
                    <p>1. Please acknowledge receipt and acceptance of the above order(s) by signing the space below. Kindly fac back within 24 hours to # (049) 534-9581 the signed copy for our file. Should no acknowledgement is received after the said time, it is understood that the details in this Purchase Order is official and binding.</p>
                    <p>2. Unless otherwise previously agreed, all deliveries shall be inspected by our Incoming Quality Assurance personnel using Mil-Std 105 at 1.0% AQL Single Sampling for Normal Inspection.</p>
                    <p>3. This Purchase Order is electronically signed. Signature is not required.</p>
                </td>
            </tr>
            <tr>
                <td style="border: 2px solid black; text-align: center;" colspan="3">
                    <img src="{{ asset('esign') }}/{{ $info[0]->issuedesign }}" alt="" width="70">
                    <br>Prepared by: {{ $info[0]->issuedby }}
                </td>
                <td style="border: 2px solid black; text-align: center;" colspan="2">
                    @if($info[0]->approvedesign!="noesignimage.png")
                    <img src="{{ asset('esign') }}/{{ $info[0]->approvedesign }}" alt="" width="70">
                    @endif
                    <br>Approved by: {{ $info[0]->approvedby }}
                </td>  
            </tr>
            <tr>
                <td colspan="5">
                    <p>Order is accepted and confirmed with conditions appearing hereof:</p>
                </td>
            </tr>
            <tr>
                <td style="border-top: 2px solid black; text-align: center; width: 300px;" colspan="2">Name</td>
                <td style="border-top: 2px solid black; text-align: center; width: 300px;" >Signature</td>   
                <td style="border-top: 2px solid black; text-align: center; width: 300px;">Position</td>   
                <td style="border-top: 2px solid black; text-align: center; width: 100px;" >Date</td>    
            </tr>
            <tr>
                <td colspan="5"><br></td>       
            </tr>
        </table>
        <p style="float: right; margin-top: -1px; font-size: 12px;">FO-PUR-02-00</p>
        
    </page>


</body>
</html>

<script type="text/javascript">
    window.onload = function() { window.print(); }
</script>

  
