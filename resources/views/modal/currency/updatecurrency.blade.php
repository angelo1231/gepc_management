<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="modalupdatecurrency" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>CURRENCY INFORMATION</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                    <div class="form-group">
                            <label for="txtucurrency">Currency</label>
                            <input id="txtucurrency" name="txtucurrency" type="text" class="form-control" placeholder="Currency">
                    </div>
                    <div class="form-group">
                            <label for="txturate">Rate</label>
                            <input id="txturate" name="txturate" type="number" class="form-control" placeholder="Rate">
                    </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-block btn-flat">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>