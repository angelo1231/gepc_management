<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="uploadesign" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>UPLOAD SIGNATRURE</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="frmesign" name="frmesign" enctype="multipart/form-data">
                    <div class="form-group">
                            <input id="esign" name="esign" type="file" class="form-control-file" accept="image/png">
                    </div>
            </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-9">

                </div>
                <div class="col-md-3">
                    <button id="btnuploadesign" name="btnuploadesign" type="button" class="btn btn-primary btn-block btn-flat">Upload</button>
                </div>
            </div>
        </div>
    </div>
</div>