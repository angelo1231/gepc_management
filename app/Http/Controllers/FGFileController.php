<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Session;
use App\FinishGoods_Model;
use DataTables;
use Illuminate\Support\Collection;
use App\Exports\FinishGoods\FGDownloadTemp;
use App\Imports\FinishGoods\FinishGoodsImport;
use Excel;
use App\Http\Controllers\UserController;

class FGFileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    function index(){

        $access = UserController::UserGroupAccess();

        return view('finishgoods')
        ->with('access', $access);
            
    }

    function FinishGoodsInformation(Request $request){

        $finishgoods = FinishGoods_Model::FinishGoodsInformation($request->customer);

        $data = array();
        for($i=0;$i<count($finishgoods);$i++){
            $obj = new \stdClass;
            $obj->itemcode = $finishgoods[$i]->itemcode;
            $obj->partnumber = $finishgoods[$i]->partnumber;
            $obj->partname = $finishgoods[$i]->partname;
            $obj->description = $finishgoods[$i]->description;
            $obj->size = '<p><strong>I.D</strong></p>
                          <p>W:'. $finishgoods[$i]->ID_width .' L:'. $finishgoods[$i]->ID_length .' H:'.  $finishgoods[$i]->ID_height .'</p>
                          <p><strong>O.D</strong></p>
                          <p>W:'. $finishgoods[$i]->OD_width .' L:'. $finishgoods[$i]->OD_length .' H:'.  $finishgoods[$i]->OD_height .'</p>';
            $obj->processcode = $finishgoods[$i]->processcode;
            $obj->packingstd = $finishgoods[$i]->packingstd;
            $obj->currency = $finishgoods[$i]->currency;
            $obj->price = $finishgoods[$i]->price;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'. $finishgoods[$i]->fgid .'" style="width: 40px;"><i class="fa fa-pencil-square-o"></i></button> <button id="btnuploadattachment" name="btnuploadattachment" class="btn btn-success btn-flat" title="Upload Attachment" value="'. $finishgoods[$i]->fgid .'" style="width: 40px;"><i class="fa fa-upload"></i></button>';
            $data[] = $obj;
        }
        
        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'size'])->make(true);

    }

    function FGNewItem(Request $request){

        $fgid = FinishGoods_Model::FGNewItem($request);

        return json_encode([
            "success"=>true, 
            "message"=>"Item information has been addded.",
            "fgid"=>$fgid
        ]);

    }

    function FinishGoodsProfile(Request $request){

        $data = FinishGoods_Model::FinishGoodsProfile($request->id);

        return json_encode([
            "itemcode"=>$data[0]->itemcode,
            "partnumber"=>$data[0]->partnumber,  
            "partname"=>$data[0]->partname, 
            "description"=>$data[0]->description,
            "customer"=>$data[0]->cid,
            "process"=>$data[0]->processcode,
            "idwidth"=>$data[0]->ID_width,
            "idlength"=>$data[0]->ID_length,
            "idheight"=>$data[0]->ID_height,
            "odwidth"=>$data[0]->OD_width,
            "odlength"=>$data[0]->OD_length,
            "odheight"=>$data[0]->OD_height,
            "currency"=>$data[0]->currencyid,
            "price"=>$data[0]->price,
            "packingstd"=>$data[0]->packingstd,
            "attachment"=>$data[0]->attachment
        ]);

    }

    function FGUpdateItem(Request $request){

        FinishGoods_Model::FGUpdateItem($request);
        return json_encode(["success"=>true, "message"=>"Item information has been update."]);

    }

    function LoadCustomer(){

        $customer = FinishGoods_Model::LoadCustomers();
        return json_encode(["data"=>$customer]);

    }

    function LoadProcess(){

        $process = FinishGoods_Model::LoadProcess();
        return json_encode(["data"=>$process]);

    }

    function LoadCurrency(){

        $currency = FinishGoods_Model::LoadCurrency();
        return json_encode(["data"=>$currency]);

    }

    function LoadCodeProcess(Request $request){

        $processdata = FinishGoods_Model::LoadCodeProcess($request);
        $processtype = FinishGoods_Model::GetProcessType($request->processcode);

        return json_encode([
            "data"=>$processdata,
            "style"=>$processtype[0]->style,
            "inkcolor"=>$processtype[0]->inkcolor
        ]);

    }

    function DownloadExcelTemp(){

        return Excel::download(new FGDownloadTemp(), "fgtemp.xlsx");

    }

    function ImportCustomerFG(Request $request){

        $file = $request->file('file');

        Excel::import(new FinishGoodsImport($request->cid), $file);

        return json_encode([
            "success"=>true,
            "message"=>"You have successfully imported the data"
        ]);

    }

    function UploadAttachment(Request $request){

        //Set Data
        $file = $request->file('attachment');
        $filename = $request->fgid . '.' . $file->getClientOriginalExtension();

        //Upload Attachment
        $file->move(public_path('drawing'), $filename);

        //Update FG
        FinishGoods_Model::UpdateFGAttachment($request->fgid, $filename);

        return json_encode([
            "success"=>true
        ]);

    }

    function UpdateAttachment(Request $request){

        //Set Data
        $file = $request->file('attachment');
        $filename = $request->fgid . '.' . $file->getClientOriginalExtension();

        //Delete Old File
        if(file_exists(public_path('drawing').'/'.$filename)){
            unlink(public_path('drawing').'/'.$filename);
        }
        
        //Upload Attachment
        $file->move(public_path('drawing'), $filename);

        //Update FG
        FinishGoods_Model::UpdateFGAttachment($request->fgid, $filename);

        return json_encode([
            "success"=>true,
            "message"=>"You have successfully upload the attachment."
        ]);

    }

}
