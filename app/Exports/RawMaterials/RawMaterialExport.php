<?php

namespace App\Exports\RawMaterials;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RawMaterialExport implements FromCollection, WithHeadings
{
    use Exportable;
    private $data;

    public function __construct($data)
    {
         $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'Size',
            'Description',
            'Flute',
            'Boardpound',
            'Currency',
            'Price',
            'Stocks'
        ];
    }

}
