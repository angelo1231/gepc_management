<!-- Middle Modal -->
<style>
        .modal {
          text-align: center;
          padding: 0!important;
        }
        
        .modal:before {
          content: '';
          display: inline-block;
          height: 100%;
          vertical-align: middle;
          margin-right: -4px;
        }
        
        .modal-dialog {
          display: inline-block;
          text-align: left;
          vertical-align: middle;
          /* width: 1300px; */
        }
        </style>   
        <!-- Modal -->
        <div class="modal fade" id="modalnewrequest"  role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <strong>NEW MATERIAL REQUEST</strong>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                    <div class="container-fluid">
                            <div class="col-md-12">
                              <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">MRI# </th>
                                  <td colspan="5"><input id="txtnmrinumber" name="txtnmrinumber" type="text" class="form-control" placeholder="Material Request Number" readonly></td>
                                </tr>
                                <tr>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Customer </th>
                                  <td colspan="5"><select id="cmbncustomer" name="cmbncustomer" class="form-control"></select></td>
                                </tr>
                                <tr>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Purchase Request</th>
                                  <td colspan="5"><select id="cmbpurchaserequest" name="cmbpurchaserequest" class="form-control"></select></td>
                                </tr>
                                <tr>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Item To Produced</th>
                                  <td colspan="5"><select id="cmbnfgitems" name="cmbnfgitems" class="form-control"></select></td>
                                </tr>
                                <tr>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Reason</th>
                                  <td colspan="5"><textarea class="form-control" rows="3" id="txtnreason" name="txtnreason" placeholder="Your Reason For Request"></textarea></td>
                                </tr>
                                <tr>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Raw Materials Needed</th>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Required Qty</th>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Remarks</th>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Excess Boards</th>
                                  <th style="width: 20%; text-align: center; vertical-align: middle;">Qty Of Excess Board</th>
                                  <th style="width: 10%; text-align: center; vertical-align: middle;"><button id="btnaddmritem" name="btnaddmritem" class="btn btn-flat btn-success"><i class="fa fa-plus"></i></button></th>
                                </tr>
                                <tbody id="nmritems" style="border: 0px;">
                                    <tr>
                                      <td><select id="cmbnrawitems1" name="cmbnrawitems[]" class="form-control"></select></td>
                                      <td><input id="txtnrequiredqty1" name="txtnrequiredqty[]" type="number" class="form-control" placeholder="0"></td>
                                      <td><textarea class="form-control" rows="2" id="txtnremarks1" name="txtnremarks[]" placeholder="Remarks"></textarea></td>
                                      <td><select id="txtnexcessboard1" name="txtnexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td>
                                      <td><select id="txtnqtyexcessboard1" name="txtnqtyexcessboard[]" class="form-control" multiple="multiple" style="width: 150px;"></select></td>
                                      <td></td>
                                    </tr>
                                </tbody>
                              </table>
                            </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-9">
        
                        </div>
                        <div class="col-md-3">
                            <button id="btnrequest" name="btnrequest" type="button" class="btn btn-primary btn-block btn-flat">Request</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>