<?php

namespace App\Imports\RawMaterials;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\RawMaterials_Model;

class RawMaterialsImport implements ToModel, WithHeadingRow
{


    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        RawMaterials_Model::ImportRawMaterials($row['name'], $row['description'], $row['width'], $row['widthsize'], $row['length'], $row['lengthsize'], $row['flute'], $row['boardpound'], $row['price'], $row['currency'], $row['category']);


    }

}
