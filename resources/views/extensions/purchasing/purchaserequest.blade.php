@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Purchase Request</b></h3>
          <a href="{{ url("/purchasing") }}" style="float: right;"><i class="fa fa-backward"></i></a>
        </div>
        <div class="box-body">
            <div class="col-md-12">

                <br>
                <div class="col-md-12">
                    <table id="tblprinfo" class="table">
                        <thead>
                            <tr>
                                <th>PR#</th>
                                <th>Item Description</th>
                                <th>Required Quantity</th>
                                <th>Reason</th>
                                <th>Issued By</th>
                                <th>Issued Date</th>
                                <th>Issued Time</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

            <div class="col-md-12">

                    <div class="col-md-12">
                            <button id="btncreatepo" name="btncreatepo" class="btn btn-flat btn-success" style="float: right;">Create Purchase Order</button>
                    </div>

            </div>
            
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->

  {{-- Modal --}}
  @include('modal.purchasing.autonewpurchaseorder')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

    //Variables
    var tblprinfo;
    var sprnumdata;

    $(document).ready(function(){

        //Set
        SetDatePicker();

        //Load
        LoadPurhcaseRequestInformation();

    });

    $('#btncreatepo').on('click', function(){

        //Clear
        ClearCreatePO();

        //Validation
        sprnumdata = $('[name="chkitem[]"]').serializeArray();

        if(sprnumdata.length==0){

            toastr.error('Please select a item.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $('#tblitemscontent').find('tr').remove();
            for(var i=0;i<sprnumdata.length;i++){
                LoadSelectedItems(sprnumdata[i]["value"]);
            }

            LoadSupplier();
            GenPONumber();
            $('#autonewpurchaseorder').modal('toggle');


        }

        
    });

    $('#btnccreatepo').on('click', function(){

        var sid = $('#cmbcsupplier').val();
        var attn = $('#txtcattentionto').val();
        var delto = $('#txtcdeliverto').val();
        var delcharge = $('#txtcdeliverycharge').val();
        var note = $('#txtcnote').val();
        var pritemid = $('[name="txtpritemid[]"]').serializeArray();
        var pritemprice = $('[name="txtprice[]"]').serializeArray();
        var papercom = $('[name="cmbpapercom[]"]').serializeArray();

        if(attn==""){
            toastr.error('Please select input attention to.', '', { positionClass: 'toast-top-center' });
        }
        else if(delto==""){
            toastr.error('Please select input the delivery to.', '', { positionClass: 'toast-top-center' });
        }
        else if(sid==""){
            toastr.error('Please select a supplier.', '', { positionClass: 'toast-top-center' });
        }
        else{


            $.ajax({
                url: '{{ url("api/purchasing/autonewpurchaseorder") }}',
                type: 'post',
                data: {
                    sid: sid,
                    pritemid: pritemid,
                    price: pritemprice,
                    attn: attn,
                    delto: delto,
                    delcharge: delcharge,
                    papercom: papercom,
                    note: note
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#autonewpurchaseorder .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadPurchaseRequest();

                        //Socket Emit
                        socket.emit('purchasingpr');
                        socket.emit('planningnotification', { prid: response.data });

                    }

                }
            });

        }

    });

    $(document).on('click', '#btndone', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/purchaserequest/updatepurchaserequestinfopurchasing") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    LoadPurhcaseRequestInformation();

                    //Socket Emit
                    socket.emit('purchasingpr');

                }

            }
        });

    });

    $('#cmbsupplier').on('change', function(){

        var sid = $(this).val();
        LoadPurhcaseRequestInformation(sid);

    });

    function LoadPaperComPrice(id, papercomid){

        $.ajax({
            url: '{{ url("api/purchaserequest/loadpapercomprice") }}',
            type: 'get',
            data: {
                papercomid: papercomid
            },
            dataType: 'json',
            success: function(response){

                $('#'+id).val(response.price);

            },
            complete: function(){

                $('#'+id).prop('readonly', false);

            }
        });

    }

    function LoadSelectedItems(prid){

        var flute;
        var boardpound;

        $.ajax({
            url: '{{ url("api/purchasing/loadselecteditem") }}',
            type: 'get',
            data: {
                prid: prid
            },
            dataType: 'json',
            success: function(response){

                //TODO HERE

                for(var i=0;i<response.pritem.length;i++){

                    $('#tblitemscontent').append('<tr><td><input name="txtpritemid[]" type="hidden" value="'+ response.pritem[i]['itemid'] +'">'+ response.pritem[i]['prnumber'] +'</td> <td>'+ response.pritem[i]['description'] +'</td> <td>'+ response.pritem[i]['qtyrequired'] +'</td> <td><input id="txtprice" name="txtprice[]" type="number" value="'+ response.pritem[i]['price'] +'" class="form-control"></td> <td><select id="cmbpapercom'+ response.pritem[i]['itemid'] +'" name="cmbpapercom[]" class="form-control" data-id="'+ prid +'"></select></td> </tr>');

                    LoadFlutePaperCombination('cmbpapercom'+response.pritem[i]['itemid'], response.pritem[i]['flute'], response.pritem[i]['boardpound']);

                }

            }
        });

    }

    function LoadFlutePaperCombination(id, flute, boardpound){

        $.ajax({
            url: '{{ url("api/purchaserequest/loadflutepapercombination") }}',
            type: 'get',
            data: {
                flute: flute,
                boardpound: boardpound
            },
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="0">Select a Paper Combination</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#'+id).append('<option value="'+  response.data[i]["papercomid"] +'">'+  response.data[i]["rmpapercombination"] +'</option>');
                }

            },
            complete: function(){

                $('#'+id).select2({
                    theme: 'bootstrap',
                    width: '200'
                });

            }
        });

    }

    function GenPONumber(){

        $.ajax({
            url: "{{ url('api/purchasing/genponumber') }}",
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtcponumber').val(response.batch);
                
            }
        });

    }

    function LoadSupplier(){

        $.ajax({
            url: '{{ url("api/purchaserequest/loadsuppliers") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbcsupplier').find('option').remove();
                $('#cmbcsupplier').append('<option value="" disabled selected>Select a Supplier</option>');
                for (var i = 0; i < response.data.length; i++) {
                    $('#cmbcsupplier').append('<option value="'+  response.data[i]["sid"] +'">'+  response.data[i]["supplier"] +'</option>');
                }

            },
            complete: function(){

                $('#cmbcsupplier').select2({
                    theme: "bootstrap"
                });

            }
        });

    }

    function LoadPurhcaseRequestInformation(){

        tblprinfo = $('#tblprinfo').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/purchaserequest/loadpurchaserequestinfopurchasing") }}'
            },
            columns : [
                {data: 'prnumber', name: 'prnumber'},
                {data: 'itemdescription', name: 'itemdescription'},
                {data: 'reqqty', name: 'reqqty'},
                {data: 'reason', name: 'reason'},
                {data: 'issuedby', name: 'issuedby'},
                {data: 'issueddate', name: 'issueddate'},
                {data: 'issuedtime', name: 'issuedtime'},
                {data: 'panel', name: 'panel', width: '12%'},
            ]
        });

    }


    function ReloadPurchaseRequest(){

        tblprinfo.ajax.reload();

    }

    function SetDatePicker(){

        $("#txtcdeliverydate").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

    }

    function ClearCreatePO(){

        $('#txtcsupplier').val('');
        $('#txtcponumber').val('');
        $('#txtcattentionto').val('');
        $('#txtcdeliverto').val('');
        $('#txtcdeliverycharge').val('0');
        $('#txtcdeliverydate').val('');

    }

    //Socket Function
    socket.on('reloadprpurchasing', function(){
        ReloadPurchaseRequest();
    });

  </script>  

@endsection