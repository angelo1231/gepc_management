<!-- Middle Modal -->
<style>
    .modal {
      text-align: center;
      padding: 0!important;
    }
    
    .modal:before {
      content: '';
      display: inline-block;
      height: 100%;
      vertical-align: middle;
      margin-right: -4px;
    }
    
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
    </style>   
    <!-- Modal -->
    <div class="modal fade" id="modalupdateitem" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <strong>FINISH GOOD INFORMATION</strong>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="container-fluid">
                    <div class="col-md-6">
                            <div class="form-group">
                                    <label for="txtuitemcode">Item Code</label>
                                    <input id="txtuitemcode" name="txtuitemcode" type="text" class="form-control" placeholder="Item Code">
                            </div>
                            <div class="form-group">
                                    <label for="txtupartnumber">Partnumber</label>
                                    <input id="txtupartnumber" name="txtupartnumber" type="text" class="form-control" placeholder="Partnumber">
                            </div>
                            <div class="form-group">
                                    <label for="txtupartname">Partname</label>
                                    <input id="txtupartname" name="txtupartname" type="text" class="form-control" placeholder="Partname">
                            </div>
                            <div class="form-group">
                                    <label for="txtudescription">Description</label>
                                    <input id="txtudescription" name="txtudescription" type="text" class="form-control" placeholder="Description">
                            </div>
                            <div class="form-group">
                                <label for="cmbucustomer">Customer</label>
                                <select class="form-control" id="cmbucustomer">
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="col-md-11 row">
                                    <label for="cmbuprocess">Process</label>
                                    <select class="form-control" id="cmbuprocess">
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <i style="margin-top: 33px; font-size: 20px;" class="tooltips fa fa-info-circle">
                                        <span id="txtuprocesstip" class="tooltiptext" style="font-size: 12px;"></span>
                                    </i>
                                </div>
                            </div>

                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group">
                                    <label for="txtupackingstd">Packing STD</label>
                                    <input id="txtupackingstd" name="txtupackingstd" type="text" class="form-control" placeholder="Packing Standard">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                    <label for="txtustyle">Style</label>
                                    <input id="txtustyle" name="txtustyle" type="text" class="form-control" placeholder="Style" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                    <label for="txtuinkcolor">Ink Color</label>
                                    <input id="txtuinkcolor" name="txtuinkcolor" type="text" class="form-control" placeholder="Ink Color" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label>I.D Size</label>
                                    <input id="txtuidwidth" name="txtuidwidth" type="text" class="form-control" placeholder="Width">
                                    <br>
                                    <input id="txtuidlength" name="txtuidlength" type="text" class="form-control" placeholder="Length">
                                    <br>
                                    <input id="txtuidheight" name="txtuidheight" type="text" class="form-control" placeholder="Height">
                            </div>

                            <div class="form-group">
                                <label id="lblucurrency" for="cmbucurrency">Currency</label>
                                <select class="form-control" id="cmbucurrency" name="cmbucurrency">
                                </select>
                                
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>O.D Size</label>
                                <input id="txtuodwidth" name="txtuodwidth" type="text" class="form-control" placeholder="Width">
                                <br>
                                <input id="txtuodlength" name="txtuodlength" type="text" class="form-control" placeholder="Length">
                                <br>
                                <input id="txtuodheight" name="txtuodheight" type="text" class="form-control" placeholder="Height">
                                
                                <div class="form-group" style="margin-top: 15px;">
                                    <label id="lbluprice" for="txtuprice">Price</label>
                                    <input id="txtuprice" name="txtuprice" type="number" class="form-control" placeholder="Price">
                                </div>
                        </div>
                        </div>

                    </div>

                    <div class="col-md-12">
                        <label for="imguattachment">Attachment</label>
                        <img src="" id="imguattachment" style="margin: 10px 24%;" width="400px">
                    </div>

                  </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-9">
    
                    </div>
                    <div class="col-md-3">
                        <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary btn-block btn-flat">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>