<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="newgroupprocess" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <strong>NEW PROCESS</strong>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtnprocesscode">Process Code</label>
                            <input type="text" name="txtnprocesscode" id="txtnprocesscode" class="form-control" placeholder="Process Code">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtnstyle">Style</label>
                            <input type="text" name="txtnstyle" id="txtnstyle" class="form-control" placeholder="Style">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="txtninkcolor">Ink Color</label>
                            <input type="text" name="txtninkcolor" id="txtninkcolor" class="form-control" placeholder="Ink Color">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <table id="tblnprocess" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Process</th>
                                    <th class="text-right"><button id="btnnadd" name="btnnadd" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button></th>
                                </tr>
                            </thead>
                           
                            <tbody id="nprocesscontent">
                                <tr>
                                    <td style="vertical-align: middle;"><input type="text" id="txtnorder1" name="txtnorder[]" class="form-control" value="1" style="width: 35px;" readonly></td>
                                    <td><select name="cmbnprocess[]" id="cmbnprocess1" class="form-control" style="width: 250px;"></select></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button type="button" class="btn btn-danger btn-flat" style="margin-top: 10px; float: right;" data-dismiss="modal">Close</button>

                    <button id="btnsave" name="btnsave" type="button" class="btn btn-primary btn-flat" style="margin-top: 10px; float: right; margin-right: 10px;">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>