@extends('layout.index')

@section('body')
<body class="hold-transition skin-purple sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  @include('include.navbartop')
  <!-- =============================================== -->

  @include('include.navbarsidel')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    @include('include.contentheader')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-th-list"></i><b> Planning</b></h3>
        </div>
        <div class="box-body">

          <div class="col-md-12">

            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#joinfo">Job Order Information</a></li>
              <li><a data-toggle="tab" href="#mrinfo">Job Order Creation</a></li>
              <li><a data-toggle="tab" href="#jooperation">Operations</a></li>
            </ul>

            <div class="tab-content">

                <div id="joinfo" class="tab-pane fade in active">

                  <br>
                  <div class="col-md-3">
                      <div class="form-group">
                          <label for="cmbcustomers">Customers</label>
                          <select id="cmbcustomers" name="cmbcustomers" class="form-control">
                          </select>
                      </div>
                    </div>
                    <div class="col-md-9">
        
                        <button id="btnnewjo" name="btnnewjo" class="btn btn-success btn-flat" style="float:right; margin: 26px 0px;"><i class="fa fa-plus" style="font-size:12px;"></i> <strong>New Request Job Order</strong></button>
        
                        <button id="btnmr" name="btnmr" class="btn btn-primary btn-flat" style="float:right; margin: 26px 10px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Material Request</strong></button>
        
                        <button id="btncustomerpo" name="btncustomerpo" class="btn btn-primary btn-flat" style="float:right; margin: 26px 0px;"><i class="fa fa-tasks" style="font-size:12px;"></i> <strong>Sales Order</strong></button>
                        
                    </div>
                    <div class="col-md-12">
                      <table class="table" id="tbljo">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Job Order Request #</th>
                            <th>Job Order #</th>
                            <th>Purchase Order #</th>
                            <th>Item Description</th>
                            <th>Material Description</th>
                            <th>Target Output</th>
                            {{-- <th>Delivery Date</th> --}}
                            <th>Request Date</th>
                            <th>Request By</th>
                            <th>Quantity Receive</th>
                            <th>Customer</th>
                            <th></th>
                          </tr>
                        </thead>
                      </table>
                    </div>

                </div>

                <div id="mrinfo" class="tab-pane fade">

                  <br>
                  <div class="col-md-12">

                    <table id="tblmrcreation" class="table">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Material Request #</th>
                                <th>Purchase Order #</th>
                                <th>Purchase Order Item</th>
                                <th>Target Output</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>

                  </div>

                </div>  

                <div id="jooperation" class="tab-pane fade">

                  <br>
                  <div class="col-md-12">

                      <button id="btnnewoperation" name="btnnewoperation" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Operation</button>

                      <button id="btnrequestjo" name="btnrequestjo" class="btn btn-flat btn-primary" style="float: right; margin-bottom: 10px; margin-right: 10px;"><i class="fa fa-file"></i> New Job Order</button>
      
                  </div>

                  <div class="col-md-12">

                      <table id="tbloperations" class="table">
                          <thead>
                              <tr>
                                  <th>Id</th>
                                  <th>Job Order #</th>
                                  <th>Target Output</th>
                                  <th></th>
                              </tr>
                          </thead>
                      </table>

                  </div>


                </div>

            </div>

          </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('include.footer')

  <!-- =============================================== -->
  {{-- Modal --}}
  @include('modal.planning.newjoborder')
  @include('modal.production.newproductionoperation')
  @include('modal.production.newjoborder')

  {{-- @include('include.navbarsider') --}}

</div>
<!-- ./wrapper -->

</body>
@endsection

@section('script')

  <script>

      //Variables
      var tbljo;
      var tbloperations;
      var tblmrcreation;
      var scustomer;
      var sjorequestnumber;

      $(document).ready(function(){

        //Load
        LoadSelect2();
        LoadCustomers($('#cmbcustomers').attr('id'));
        LoadOperationInformation();
        LoadMRForJobOrderCreation();

        tbljo = $('#tbljo').DataTable({
            autoWidth: false,
            ordering: false,
        });

        //Set DatePicker
        $("#txtdeldatefrom").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });

        $("#txtdeldateto").datepicker({ 
            dateFormat: 'yy-mm-dd' 
        });
        
        // tbljo.columns(10).visible(false);

      });

      $('#btnnewjo').on('click', function(){

        @if(in_array(35, $access))

          ClearNewJO();
          LoadJOCustomer($('#cmbncustomer').attr('id'));
          $('#newjoborder').modal('toggle');

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif  

      });

      $('#btnmr').on('click', function(){

        @if(in_array(38, $access))

          window.location = "{{ url('/planning/materialrequest') }}";

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btncustomerpo').on('click', function(){

        @if(in_array(40, $access))

          window.location = "{{ url('/planning/cuspurchaseorder') }}";

        @else

          toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#cmbncustomer').on('change', function(){

          var id = $('#cmbnmaterialrequest').attr('id');
          var customer = $(this).val();
          
          //Load
          LoadMaterialRequestInfo(id, customer);

      });

      $('#cmbnmaterialrequest').on('change', function(){

          var requestid = $(this).val();

          LoadMaterialRequestData(requestid);

      });

      $('#btncreatejo').on('click', function(){

          $.confirm({
              title: 'Save',
              content: 'Save this job order information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                          CreateJO();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

      });

      $('#cmbcustomers').on('change', function(){

        scustomer = $(this).val();
        LoadJobOrderInformation(scustomer);

      });

      function LoadMaterialRequestData(requestid){

        $.ajax({
          url: '{{ url("api/planning/loadmaterialrequestdata") }}',
          type: 'get',
          data: {
            requestid: requestid
          },
          dataType: 'json',
          success: function(response){
            
              $('#txtnponumber').val(response.ponumber);
              $('#txtnpoitem').val(response.poitem);
              $('#txtntargetoutput').val(response.targetoutput);

          }
        });

      }

      function CreateJO(){

        var customer = $('#cmbncustomer').val();
        var materialrequest = $('#cmbnmaterialrequest').val();
        var targetoutput = $('#txtntargetoutput').val();

        //Validation
        if(customer==null){
          toastr.error("Please select a customer.", '', { positionClass: 'toast-top-center' });
        }
        else if(materialrequest==null){
          toastr.error("Please select a material request number.", '', { positionClass: 'toast-top-center' });
        }
        else if(targetoutput==0){
          toastr.error("Please input a number is greater than zero.", '', { positionClass: 'toast-top-center' });
        }
        else if(targetoutput < 0){
            toastr.error("Please input a non negative number in the delivery charge.", '', { positionClass: 'toast-top-center' });
        }
        else{

          $.ajax({
            url: '{{ url("api/planning/createjoborder") }}',
            type: 'post',
            data: {
              customer: customer,
              materialrequest: materialrequest,
              targetoutput: targetoutput
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                LoadJobOrderInformation(scustomer);
                $('#newjoborder .close').click();

                //Socket
                socket.emit('productionjorequestnotif', { jorequestnumber: response.jorequestnumber });

              }
              else{

                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }

            }
          });

        }
    
      }


      function LoadMaterialRequestInfo(id, customer){

          $.ajax({
            url: '{{ url("api/planning/loadmaterialrequestinfo") }}',
            type: 'get',
            data: {
              customer: customer
            },
            dataType: 'json',
            success: function(response){

              $('#'+id).find('option').remove();
              $('#'+id).append(response.content);

            },
            complete: function(){

              $('#'+id).select2({
                theme: 'bootstrap'
              });

            }
          });

      }

      function LoadJOCustomer(id){

        $.ajax({
          url: '{{ url("api/planning/loadcustomers") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            $('#'+id).find('option').remove();
            $('#'+id).append(response.content);

          },
          complete: function(){

            $('#'+id).select2({
              theme: 'bootstrap'
            });

          }
        });

      }

      function LoadCustomers(id){

        $.ajax({
          url: '{{ url("api/planning/loadcustomers") }}',
          type: 'get',
          dataType: 'json',
          success: function(response){

            $('#'+id).find('option').remove();
            $('#'+id).append(response.content);

          },
          complete: function(){

            $('#'+id).val("All").trigger("change");

          }
        });

      }

      function LoadJobOrderInformation(customer){

        tbljo.destroy();
        tbljo = $('#tbljo').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/planning/loadjoborderinformation") }}',
              data: {
                customer: customer
              },
          },
          columns : [
              {data: 'status', name: 'status'},
              {data: 'jorequestnumber', name: 'jorequestnumber'},
              {data: 'jonumber', name: 'jonumber'},
              {data: 'ponumber', name: 'ponumber'},
              {data: 'finishgood', name: 'finishgood'},
              {data: 'rawmaterial', name: 'rawmaterial'},
              {data: 'targetoutput', name: 'targetoutput'},
              // {data: 'deliverydate', name: 'deliverydate'},
              {data: 'requestdate', name: 'requestdate'},
              {data: 'requestby', name: 'requestby'},
              {data: 'qtyreceive', name: 'qtyreceive'},
              {data: 'customer', name: 'category'},
              {data: 'panel', name: 'panel'},
          ]
        });

        // if(customer=="All"){
        //   tbljo.columns(10).visible(true);
        // }
        // else{
        //   tbljo.columns(10).visible(false);
        // }

      }

      function ClearNewJO(){

        $('#txtnjonumber').val('');
        $('#cmbnmaterialrequest').find('option').remove();
        $('#txtntargetoutput').val('');
        $('#txtnponumber').val('');
        $('#txtnpoitem').val('');

      }

      function LoadSelect2(){

        $('#cmbcustomers').select2({
          theme: 'bootstrap'
        });

      }

      //Planning Operation

      $(document).on('click', '#btnoperationprofile', function(){

        @if(in_array(60, $access))

            var mid = $(this).val();
            window.location = "{{ url('/productionoperation/productionoperationinfo') }}/" + mid;

        @else

            toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

        @endif

      });

      $('#btnrequestjo').on('click', function(){

        $('#divjocontent').hide();
        $('#newjoborderprod').modal('toggle');
        LoadJobOrderForRequest();
        GenJONumber();
        ClearNewJobOrder();

      });

      $('#cmbjornumber').on('change', function(){

        sjorequestnumber = $(this).val();

        LoadJORequestInformation(sjorequestnumber);
        $('#divjocontent').fadeIn();

      });

      $('#btncreate').on('click', function(){

        $.confirm({
              title: 'Create',
              content: 'Create this job order information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        Create();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

      });

      $('#btnnewoperation').on('click', function(){

          @if(in_array(59, $access))

              $('#newproductionoperation').modal('toggle');
              LoadJobOrder();

          @else

              toastr.error("You dont have any access here please contact your system administrator.", '', { positionClass: 'toast-top-center' });

          @endif


      });

      $('#btnncreate').on('click', function(){

          $.confirm({
                title: 'Save',
                content: 'Save this information?',
                type: 'blue',
                buttons: {   
                    ok: {
                        text: "Yes",
                        btnClass: 'btn-info',
                        keys: ['enter'],
                        action: function(){

                          SaveOperation();

                        }
                    },
                    cancel: {
                        text: "No",
                        btnClass: 'btn-info',
                        action: function(){
                            
                          

                        }
                    } 
                }
            });

      });

      $(document).on('click', '#btncrtjo', function(){

        var materialrequest = $(this).val();
        var targetoutput = $(this).data('output');

        $.confirm({
                title: 'Request',
                content: 'Request this job order information?',
                type: 'blue',
                buttons: {   
                    ok: {
                        text: "Yes",
                        btnClass: 'btn-info',
                        keys: ['enter'],
                        action: function(){

                          CreateForMRJO(materialrequest, targetoutput);

                        }
                    },
                    cancel: {
                        text: "No",
                        btnClass: 'btn-info',
                        action: function(){
                            
                          

                        }
                    } 
                }
            });


      });

      function CreateForMRJO(materialrequest, targetoutput){

        $.ajax({
            url: '{{ url("api/planning/createjoborder") }}',
            type: 'post',
            data: {
              materialrequest: materialrequest,
              targetoutput: targetoutput
            },
            dataType: 'json',
            success: function(response){

              if(response.success){

                toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                LoadJobOrderInformation(scustomer);
                ReloadMRForJobOrderCreation();

                //Socket
                socket.emit('productionjorequestnotif', { jorequestnumber: response.jorequestnumber });

              }
              else{

                toastr.error(response.message, '', { positionClass: 'toast-top-center' });

              }

            }
          });

      }

      function SaveOperation(){

          var joid = $('#txtnjonumber').val();
          
          if(joid==null || joid==""){

              toastr.error("Please select a job order number.", '', { positionClass: 'toast-top-center' });
              $('#txtnjonumber').focus();

          }
          else{

              $.ajax({
                  url: '{{ url("api/productionoperation/saveoperation") }}',
                  type: 'post',
                  data: {
                      joid: joid
                  },
                  dataType: 'json',
                  success: function(response){

                      if(response.success){

                          toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                          $('#newproductionoperation .close').click();
                          ReloadOperationInformation();

                          //Socket Emit
                          socket.emit('monitoringjo');

                      }
                      else{

                          toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                      }

                  }
              });

          }

      }

      function LoadJobOrder(){

            $.ajax({
                url: '{{ url("api/productionoperation/loadjoborder") }}',
                type: 'get',
                dataType: 'json',
                success: function(response){

                    $('#txtnjonumber').find('option').remove();
                    $('#txtnjonumber').append('<option value="" disabled selected>Select a Job Order Number</option>');
                    for(var i=0;i<response.data.length;i++){

                        $('#txtnjonumber').append('<option value="'+response.data[i]["joid"]+'">'+response.data[i]["jonumber"]+'</option>');

                    } 

                },
                complete: function(){

                  $('#txtnjonumber').select2({
                      theme: 'bootstrap'
                  }); 

                }
            });

      }

      function Create(){

          var joid = $('#cmbjornumber').val();
          var style = $('#txtstyle').val();
          var inkcolor = $('#txtinkcolor').val();
          var specialinstruction = $('#txtspecialinstruction').val();
          var deldatefrom = $('#txtdeldatefrom').val();
          var deldateto = $('#txtdeldateto').val();

          if(inkcolor==""){
            $('#txtinkcolor').val('None');
            inkcolor = "None";
          }

          //Validation
          if(style==""){
            toastr.error("Please input the style.", '', { positionClass: 'toast-top-center' });
          }
          else if(deldatefrom==""){
              toastr.error("Please select a delivery date from.", '', { positionClass: 'toast-top-center' });
          }
          else if(deldateto==""){
              toastr.error("Please select delivery date to.", '', { positionClass: 'toast-top-center' });
          }
          else{

              $.ajax({
                  url: '{{ url("api/productionoperation/updatecreatejo") }}',
                  type: 'post',
                  data: {
                      joid: joid,
                      style: style,
                      inkcolor: inkcolor,
                      specialinstruction: specialinstruction,
                      deldatefrom: deldatefrom,
                      deldateto: deldateto
                  },
                  dataType: 'json',
                  success: function(response){

                      if(response.success){

                          toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                          $('#newjoborderprod .close').click();

                      }

                  }
              });

          }

      }

      function LoadJORequestInformation(jorequestnumber){

        $.ajax({
            url: '{{ url("api/productionoperation/loadjorequestinformation") }}',
            type: 'get',
            data: {
                joid: jorequestnumber
            },
            dataType: 'json',
            success: function(response){

                $('#txtjocustomer').val(response.customer);
                $('#txtjoponumber').val(response.ponumber);
                $('#txtjoitem').val(response.description);
                $('#txtmrnumber').val(response.mrinumber);
                $('#txtjotargetoutput').val(response.targetoutput);
                $('#txtspecialinstruction').val(response.specialinstruction);

            }
        });

      }

      function LoadJobOrderForRequest(){

          $.ajax({
              url: '{{ url("api/productionoperation/loadjoborderforrequest") }}',
              type: 'get',
              dataType: 'json',
              success: function(response){

                  $('#cmbjornumber').find('option').remove();
                  $('#cmbjornumber').append('<option value="" disabled selected>Select a Job Order Request Number</option>');
                  for(var i=0;i<response.data.length;i++){

                      $('#cmbjornumber').append('<option value="'+response.data[i]["joid"]+'">'+response.data[i]["jorequestnumber"]+'</option>');

                  }

                  $('#cmbjornumber').select2({
                      theme: 'bootstrap'
                  });  

              }
          });

      }

      function GenJONumber(){

        $.ajax({
            url: '{{ url("api/planning/genjonumber") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#txtjonum').val(response.batch);

            }
        });

      }

      function ClearNewJobOrder(){

        $('#txtstyle').val('');
        $('#txtinkcolor').val('');
        $('#txtspecialinstruction').val('');
        $('#txtdeldatefrom').val('');
        $('#txtdeldateto').val('');

      }

      function LoadMRForJobOrderCreation(){

        tblmrcreation = $('#tblmrcreation').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/planning/loadmrforjobordercreation") }}'
          },
          columns : [
              {data: 'customer', name: 'customer'},
              {data: 'materialnumber', name: 'materialnumber'},
              {data: 'ponumber', name: 'ponumber'},
              {data: 'poitem', name: 'poitem'},
              {data: 'targetoutput', name: 'targetoutput'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
        });

      }

      function LoadOperationInformation(){

        tbloperations = $('#tbloperations').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ordering: false,
          ajax: {
              type: 'get',
              url: '{{ url("api/productionoperation/loadoperationinformation") }}'
          },
          columns : [
              {data: 'id', name: 'id'},
              {data: 'jonumber', name: 'jonumber'},
              {data: 'targetoutput', name: 'targetoutput'},
              {data: 'panel', name: 'panel', width: '12%'},
          ]
        });

      }

      function ReloadOperationInformation(){

        tbloperations.ajax.reload();

      }

      function ReloadMRForJobOrderCreation(){

        tblmrcreation.ajax.reload();

      }

  </script>  

@endsection