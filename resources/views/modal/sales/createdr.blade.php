<!-- Middle Modal -->
<style>
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>   
<!-- Modal -->
<div class="modal fade" id="createdr" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <label><strong>Delivery Number</strong></label>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            
                <div class="container-fluid">

                    <div class="col-md-12">

                        <div class="form-group">
                            <label for="txtdeliverynumber">Delivery Number</label>
                            <input type="text" id="txtdeliverynumber" name="txtdeliverynumber" class="form-control" placeholder="Delivery Number">
                        </div>

                        <div class="form-group">
                            <label for="txtdeliverydate">Delivery Date</label>
                            <input type="text" id="txtdeliverydate" name="txtdeliverydate" class="form-control" placeholder="Delivery Date">
                        </div>

                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                        <div class="col-md-12">
                                <button id="btncreate" name="btncreate" type="button" class="btn btn-primary btn-flat">Create Delivery Reciepe</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>